package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 选择充值方式
 * Created by Administrator on 2016/1/14.
 */
@EActivity(R.layout.activity_select_recharge_way)
public class SelectRechargeWayActivity extends BaseActy{
    protected static final String TAG = "SelectRechargeWayActivity";

    @ViewById(R.id.asrw_iv_alipay_select)
    ImageView alipaySelectIV;
    @ViewById(R.id.asrw_iv_wechat_select)
    ImageView wechatSelectIV;
    @ViewById(R.id.asrw_iv_unionpay_select)
    ImageView unionpaySelectIV;

    //记录选择充值方式
    private String currentRechargeWay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.asrw_tb_title, "选择付款方式");
        Intent intent = getIntent();
        currentRechargeWay = intent.getExtras().getString("rechargeWay");
        bindView();
        notifySelectIV();
    }

    private void bindView(){
        findViewById(R.id.asrw_rl_alipay_type).setOnClickListener(this);
        findViewById(R.id.asrw_rl_wechat_type).setOnClickListener(this);
        findViewById(R.id.asrw_rl_unionpay_type).setOnClickListener(this);
    }

    /**
     *更新选中的钩方法
     */
    private void notifySelectIV(){
        if (("1").equals(currentRechargeWay)){
            alipaySelectIV.setVisibility(View.VISIBLE);
            wechatSelectIV.setVisibility(View.GONE);
            unionpaySelectIV.setVisibility(View.GONE);
        }
        else if(("2").equals(currentRechargeWay)){
            alipaySelectIV.setVisibility(View.GONE);
            wechatSelectIV.setVisibility(View.VISIBLE);
            unionpaySelectIV.setVisibility(View.GONE);
        }
        else if(("3").equals(currentRechargeWay)){
            alipaySelectIV.setVisibility(View.GONE);
            wechatSelectIV.setVisibility(View.GONE);
            unionpaySelectIV.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 选中后关闭页面，跳转上个页面且传值
     */
    private void finishAndSetResult(){
        Intent intentResult = new Intent();
        intentResult.putExtra("rechargeWay", currentRechargeWay);
        setResult(RESULT_OK, intentResult);
        finish();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.asrw_rl_alipay_type:
                currentRechargeWay = "1";
                notifySelectIV();
                finishAndSetResult();
                break;
            case R.id.asrw_rl_wechat_type:
                currentRechargeWay = "2";
                notifySelectIV();
                finishAndSetResult();
                break;
            case R.id.asrw_rl_unionpay_type:
                currentRechargeWay = "3";
                notifySelectIV();
                finishAndSetResult();
                break;
        }
    }
}
