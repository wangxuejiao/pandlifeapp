package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.GoodsItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.RecommendGoodsItem;

/**
 * Created by play on 2016/1/6.
 */
public class ShoppingCartItemView extends AbsRelativeLayout {

    private ImageView imageIV;
    private TextView goodsNameTV;
    private TextView sumTV;
    private ImageView addIV;
    private ImageView subtractIV;
    private EditText goodsNumberET;

    private int goodsNumber = 1;

    public ShoppingCartItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ShoppingCartItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        imageIV = (ImageView)findViewById(R.id.isc_iv_image);
        goodsNameTV = (TextView) findViewById(R.id.isc_tv_goods_name);
        sumTV = (TextView) findViewById(R.id.isc_tv_sum);
        addIV = (ImageView)findViewById(R.id.isc_iv_add);
        subtractIV = (ImageView)findViewById(R.id.isc_iv_subtract);
        goodsNumberET = (EditText) findViewById(R.id.isc_et_goods_number);
    }

    @Override
    public void setObject(final Item item, final int position, final ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);
        if (item instanceof GoodsItem) {
            GoodsItem goodsItem = (GoodsItem) item;
            goodsNameTV.setText(goodsItem.getName());
            sumTV.setText("￥" + goodsItem.getPrice());
            goodsNumberET.setText(goodsNumber + "");

            addIV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    goodsNumber++;
                    goodsNumberET.setText(goodsNumber + "");
                    v.setTag(goodsNumber);
                    onViewClickListener.onViewClick(v, position);
                }
            });
            subtractIV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (goodsNumber > 0) {
                        goodsNumber--;
                    } else {
                        goodsNumber = 0;
                    }
                    goodsNumberET.setText(goodsNumber + "");
                    v.setTag(goodsNumber);
                    onViewClickListener.onViewClick(v, position);
                }
            });
        }else if (item instanceof RecommendGoodsItem){
            
        }

    }
}
