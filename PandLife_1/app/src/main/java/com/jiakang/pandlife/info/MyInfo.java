package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.item.UserItem;

import org.androidannotations.annotations.App;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 5.个人信息获取接口
 *
 * @author ww
 *
 */
public class MyInfo extends BaseAbsInfo {

    private static final String TAG = "MyInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private UserItem mUserItem;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=User&a=Myinfo" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                mUserItem = BaseInfo.gson.fromJson(jsonObject.get("data").toString(), UserItem.class);
                // 保存用户实体
                PandLifeApp.getInstance().setUserItem(mUserItem);
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserItem getUserItem() {
        return mUserItem;
    }

    public void setUserItem(UserItem userItem) {
        mUserItem = userItem;
    }

    
}
