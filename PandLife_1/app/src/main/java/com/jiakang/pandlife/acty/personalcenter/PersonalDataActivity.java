package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.db.AreaDBManage;
import com.jiakang.pandlife.info.MyInfo;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.JKLog;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

/**
 * 个人资料
 */
@EActivity(R.layout.activity_personal_data)
public class PersonalDataActivity extends BaseActy {
	protected final String TAG = "PersonalDataActivity";

	@ViewById(R.id.apda_iv_head)
	ImageView headerIV;
	@ViewById(R.id.apda_tv_nick_name_content)
	TextView nickNameTV;
	@ViewById(R.id.apda_tv_real_name_content)
	TextView realNameTV;
	@ViewById(R.id.apda_tv_identity_card_content)
	TextView identityCardTV;
	@ViewById(R.id.apda_tv_birthday_content)
	TextView birthDayTV;
	@ViewById(R.id.apda_tv_phone_content)
	TextView telephoneTV;
	@ViewById(R.id.apda_tv_home_address_content)
	TextView addressTV;

	private ApiManager apiManager;
	private AreaDBManage areaDBManage;

	private UserItem mUserItem;
	


	/** 获取我的信息接口 */
	private MyInfo mMyInfo = new MyInfo();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


	}

	@AfterViews
	protected void initVar() {

		initView();
		bindView();
	}
	
	private void initView() {
		initTitleBar(R.id.apda_tb_title, "个人资料", null, "修改");
		
	}

	private void bindView(){
		mUserItem = PandLifeApp.getInstance().getUserItem();
		if (mUserItem != null){
			if (!TextUtils.isEmpty(mUserItem.getHead())){
				ImageLoaderUtil.displayImage(mUserItem.getHead(), headerIV, R.mipmap.default_image);
//				BaseImageDownloader.getStreamFromOtherSource();
			}
			if (!TextUtils.isEmpty(mUserItem.getNick())){
				nickNameTV.setText(mUserItem.getNick());
			}
			String userNameStr = mUserItem.getRealname();
			String telephoneStr = mUserItem.getMobile();


			realNameTV.setText(userNameStr);
			identityCardTV.setText(mUserItem.getCard_no());
			birthDayTV.setText(mUserItem.getBirthday());
			telephoneTV.setText(telephoneStr);
			addressTV.setText(mUserItem.getAddress());
		}else{
			ApiManager apiManager = ApiManager.getInstance();
			apiManager.request(mMyInfo, new AbsOnRequestListener(mContext){
				@Override
				public void onRequestSuccess(int result, JSONObject jsonObject) {
					super.onRequestSuccess(result, jsonObject);
					try {
						mUserItem = mMyInfo.getUserItem();
						if (mUserItem == null){
							CustomToast.showToast(mContext, "获取信息失败，请稍后重试");
							return;
						}
						bindView();
					}catch (Exception e){
						e.printStackTrace();
					}
				}
			});
		}

	}
	
	@Override  
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        super.onActivityResult(requestCode, resultCode, data);  
        try {
        	if(requestCode == Constant.StaticCode.REQUEST_CHANGED_DATA && resultCode == RESULT_OK) {
				setResult(RESULT_OK);
				ApiManager apiManager = ApiManager.getInstance();
				apiManager.request(mMyInfo, new AbsOnRequestListener(mContext) {
					@Override
					public void onRequestSuccess(int result, JSONObject jsonObject) {
						super.onRequestSuccess(result, jsonObject);
						try {
							mUserItem = mMyInfo.getUserItem();
							if (mUserItem == null) {
								CustomToast.showToast(mContext, "获取信息失败，请稍后重试");
								return;
							}
							bindView();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
            }
		} catch (Exception e) {
			JKLog.e(TAG, "onActivityResult-------------------------------->" + e);
		}
    }
	
	@Override
	public void onClick(View v){
		switch(v.getId()){
		case R.id.it_ibn_left://返回
			PersonalDataActivity.this.finish();
			break;
		case R.id.it_btn_right://进入修改界面
			Intent intentChangedData = new Intent(mContext, ChangePersonalDataActivity_.class);
			startActivityForResult(intentChangedData, Constant.StaticCode.REQUEST_CHANGED_DATA);
			break;
		}
	}
}
