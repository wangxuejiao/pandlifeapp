package com.jiakang.pandlife.item;

/**
 * Created by play on 2016/1/14.
 */
public class QiNiuToken {


    /**
     * data : {"qiniutoken":"W-noG7WUocaTRGBnm5b7n5Vv4aJzYoiNqfirDyAS:Ds8q-hXFjP_65hglw3cNHQLTIgc=:eyJzY29wZSI6InhpYW9uaXUiLCJkZWFkbGluZSI6MTQ4ODc1NTAxMiwicmV0dXJuQm9keSI6IntcIm5hbWVcIjokKGtleSksXCJoYXNoXCI6JChldGFnKSxcInNpemVcIjogJChmc2l6ZSksXCJ3XCI6JChpbWFnZUluZm8ud2lkdGgpLFwiaFwiOiQoaW1hZ2VJbmZvLmhlaWdodCksXCJ0eXBlXCI6ICQobWltZVR5cGUpfSJ9"}
     * status : 1
     * info : 成功
     */
    private DataEntityQiNiuToken data;
    private int status;
    private String info;

    public void setData(DataEntityQiNiuToken data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public DataEntityQiNiuToken getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }

    public class DataEntityQiNiuToken {
        /*
         * qiniutoken : W-noG7WUocaTRGBnm5b7n5Vv4aJzYoiNqfirDyAS:Ds8q-hXFjP_65hglw3cNHQLTIgc=:eyJzY29wZSI6InhpYW9uaXUiLCJkZWFkbGluZSI6MTQ4ODc1NTAxMiwicmV0dXJuQm9keSI6IntcIm5hbWVcIjokKGtleSksXCJoYXNoXCI6JChldGFnKSxcInNpemVcIjogJChmc2l6ZSksXCJ3XCI6JChpbWFnZUluZm8ud2lkdGgpLFwiaFwiOiQoaW1hZ2VJbmZvLmhlaWdodCksXCJ0eXBlXCI6ICQobWltZVR5cGUpfSJ9
         */
        private String qiniutoken;

        public void setQiniutoken(String qiniutoken) {
            this.qiniutoken = qiniutoken;
        }

        public String getQiniutoken() {
            return qiniutoken;
        }
    }
}
