package com.jiakang.pandlife.info;

/**
 * 1.团购商品列表接口
 * Created by Administrator on 2015/12/8.
 */


import android.text.TextUtils;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.GroupPurchaseDetailItem;
import com.jiakang.pandlife.item.GroupPurchaseItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 2.团购商品详情接口
 *
 * @author ww
 *
 */
public class GetTuanDetailInfo extends BaseAbsInfo {

    private static final String TAG = "GetTuanDetailInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;
    /** 产品id	Int(10) */
    private int id;

    private GroupPurchaseDetailItem mGroupPurchaseDetailItem;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Tuan&a=Gettuandetial" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("id", id);

        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                mGroupPurchaseDetailItem = BaseInfo.gson.fromJson(jsonObject.get("data").toString(), GroupPurchaseDetailItem.class);
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public GroupPurchaseDetailItem getmGroupPurchaseDetailItem() {
        return mGroupPurchaseDetailItem;
    }

    public void setmGroupPurchaseDetailItem(GroupPurchaseDetailItem mGroupPurchaseDetailItem) {
        this.mGroupPurchaseDetailItem = mGroupPurchaseDetailItem;
    }
}
