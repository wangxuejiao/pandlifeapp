package com.jiakang.pandlife.acty.register;

import android.os.Bundle;
import android.view.View;


import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

/**
 * 用户协议
 */
@EActivity(R.layout.activity_user_agreement)
public class UserAgreementActivity extends BaseActy {

	private static final String TAG = "UserAgreementActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_user_agreement);

//		initVar();
//		bindView();

	}

//	private void bindView() {
//
//	}

	@AfterViews
	protected void initVar() {
		initTitleBar(R.id.aua_tb_title, "用户协议");

	}


	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.it_ibn_left://返回
			UserAgreementActivity.this.finish();
		default:
			break;
		}
		super.onClick(v);
	}

}
