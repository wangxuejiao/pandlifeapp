package com.jiakang.pandlife.db;


import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.jiakang.pandlife.annotation.utils.CursorUtil;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.CityItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.ProvinceItem;
import com.jiakang.pandlife.utils.JKLog;

import java.util.ArrayList;
import java.util.List;


public class AreaDBManage {
	static String[] nString = null;
	static AreaDBHelper dbHelper = null;

	public AreaDBManage(Context context) {
		if (dbHelper == null)
			dbHelper = AreaDBHelper.getInstance(context);
	}

	/**
	 * 关闭地区数据库
	 * 
	 * @param context
	 */
	public void closeDB() {
		dbHelper.close();
	}

	/**
	 * 查询所有省
	 * 
	 * @return
	 */
	// @Deprecated
	// public String[] queryAllProvince() {
	// try {
	// Cursor cur = dbHelper.getDatabase().rawQuery("select * from province",
	// null);
	// int i = 0;
	// int myProId = 0;
	// if (cur != null) {// 游标不为空
	// // 返回给定名称的列的基于0开始的index，如果该属性列不存在则返回-1
	// // 通过它们的index来检索属性值
	// int numColumn = cur.getColumnIndex("province");
	// int numProId = cur.getColumnIndex("provinceID");
	// int count = cur.getCount();
	// nString = new String[count];
	// String proIds = "0";
	//
	// if (cur.moveToFirst()) { // cur.moveToFirst()让游标指向第一行，如果游标指向第一行，则返回true
	// do {
	// nString[i] = cur.getString(numColumn);// 获得当前行该属性的值
	// proIds = cur.getString(numProId);
	// if (null != BaseInfo.mPersonalInfo.getProvince_code()
	// && BaseInfo.mPersonalInfo.getProvince_code().equals(proIds)) {
	// myProId = i;
	// }
	// i++;
	// } while (cur.moveToNext());
	// }
	// }
	// if (cur != null) {
	// cur.close();
	// }
	// if (nString.length > 0) {
	// String chang = "";
	// chang = nString[0];
	// nString[0] = nString[myProId];
	// nString[myProId] = chang;
	// }
	// return nString;
	// } catch (Exception e) {
	// e.printStackTrace();
	// return new String[] { "" };
	// }
	// }

	/**
	 * 查询所有籍贯的省
	 * 
	 * @param sql
	 * @param column
	 * @param proId
	 * @param context
	 * @return
	 */
	// @Deprecated
	// public String[] queryAllHomeProvince(String sql, String column, String
	// proId) {
	// try {
	// Cursor cur = dbHelper.getDatabase().rawQuery(sql, null);
	// int i = 0;
	// int myProId = 0;
	// if (cur != null) {// 游标不为空
	// // 返回给定名称的列的基于0开始的index，如果该属性列不存在则返回-1
	// // 通过它们的index来检索属性值
	// int numColumn = cur.getColumnIndex(column);
	// int numProId = cur.getColumnIndex(proId);
	// int count = cur.getCount();
	// nString = new String[count];
	// String proIds = "0";
	// if (cur.moveToFirst()) { // cur.moveToFirst()让游标指向第一行，如果游标指向第一行，则返回true
	// do {
	// nString[i] = cur.getString(numColumn);// 获得当前行该属性的值
	// proIds = cur.getString(numProId);
	// if (null != BaseInfo.mPersonalInfo.getNativeplace()
	// && BaseInfo.mPersonalInfo.getNativeplace().equals(proIds)) {
	// myProId = i;
	// }
	// i++;
	// } while (cur.moveToNext());
	// }
	// }
	// if (cur != null) {
	// cur.close();
	// }
	// if (nString.length > 0) {
	// String chang = "";
	// chang = nString[0];
	// nString[0] = nString[myProId];
	// nString[myProId] = chang;
	// }
	// return nString;
	// } catch (Exception e) {
	// e.printStackTrace();
	// return new String[] { "" };
	// }
	//
	// }

	/**
	 * 查询一个省中的所有市
	 * 
	 * "select * from city where fatherID=" + provinceID, "city", "cityID"
	 * 
	 * @param sql
	 * @param column
	 * @param cityId
	 * @param context
	 * @return
	 */
	// @Deprecated
	// public String[] queryAllCityInProvince(String sql, String column, String
	// cityId) {
	// try {
	// Cursor cur = dbHelper.getDatabase().rawQuery(sql, null);
	// int i = 0;
	// int myCityId = 0;
	// if (cur != null) {// 游标不为空
	// // 返回给定名称的列的基于0开始的index，如果该属性列不存在则返回-1
	// // 通过它们的index来检索属性值
	// int numColumn = cur.getColumnIndex(column);
	// int numProId = cur.getColumnIndex(cityId);
	// int count = cur.getCount();
	// nString = new String[count];
	// String proIds = "0";
	//
	// if (cur.moveToFirst()) { // cur.moveToFirst()让游标指向第一行，如果游标指向第一行，则返回true
	// do {
	// nString[i] = cur.getString(numColumn);// 获得当前行该属性的值
	// proIds = cur.getString(numProId);
	// if (null != BaseInfo.mPersonalInfo.getCity_code()
	// && BaseInfo.mPersonalInfo.getCity_code().equals(proIds)) {
	// myCityId = i;
	// }
	// i++;
	// } while (cur.moveToNext());
	// }
	// }
	// if (cur != null) {
	// cur.close();
	// }
	// if (nString.length > 0) {
	// String chang = "";
	// chang = nString[0];
	// nString[0] = nString[myCityId];
	// nString[myCityId] = chang;
	// }
	// return nString;
	// } catch (Exception e) {
	// e.printStackTrace();
	// return new String[] { "" };
	// }
	//
	// }

	/**
	 * 查询一个市中的所有区 "select * from area where fatherID = 'unitCityID'
	 * 
	 * @param unitCityID
	 *            : IData.unitCityID,
	 * @return
	 */
	// @Deprecated
	// public String[] queryAllAreaInCity(String unitCityID) {
	// try {
	// Cursor cur =
	// dbHelper.getDatabase().rawQuery("select * from area where fatherID=" +
	// unitCityID, null);
	// int i = 0;
	// int myAreaId = 0;
	// if (cur != null) {// 游标不为空
	// // 返回给定名称的列的基于0开始的index，如果该属性列不存在则返回-1
	// // 通过它们的index来检索属性值
	// int numColumn = cur.getColumnIndex("area");
	// int numProId = cur.getColumnIndex("areaID");
	// int count = cur.getCount();
	// nString = new String[count];
	// String proIds = "0";
	// if (cur.moveToFirst()) { // cur.moveToFirst()让游标指向第一行，如果游标指向第一行，则返回true
	// do {
	// nString[i] = cur.getString(numColumn);// 获得当前行该属性的值
	// proIds = cur.getString(numProId);
	// if (null != BaseInfo.mPersonalInfo.getArea_code()
	// && BaseInfo.mPersonalInfo.getArea_code().equals(proIds)) {
	// myAreaId = i;
	// }
	// i++;
	// } while (cur.moveToNext());
	// }
	// }
	// if (cur != null) {
	// cur.close();
	// }
	// if (nString.length > 0) {
	// String chang = "";
	// chang = nString[0];
	// nString[0] = nString[myAreaId];
	// nString[myAreaId] = chang;
	// }
	// return nString;
	// } catch (Exception e) {
	// e.printStackTrace();
	// return new String[] { "" };
	// }
	// }

	/**
	 * sql查询，根据column生成数组
	 * 
	 * @param sql
	 * @param column
	 * @param context
	 * @return String[] column数组
	 */
	@Deprecated
	public String[] queryString(String sql, String column) {
		try {
			Cursor cur = dbHelper.getDatabase().rawQuery(sql, null);
			int i = 0;
			if (cur != null) {// 游标不为空
								// 返回给定名称的列的基于0开始的index，如果该属性列不存在则返回-1
								// 通过它们的index来检索属性值
				int numColumn = cur.getColumnIndex(column);
				int count = cur.getCount();
				nString = new String[count];
				if (cur.moveToFirst()) { // cur.moveToFirst()让游标指向第一行，如果游标指向第一行，则返回true
					do {
						nString[i] = cur.getString(numColumn);// 获得当前行该属性的值
						i++;
					} while (cur.moveToNext());
				}
			}
			if (cur != null) {
				cur.close();
			}
			return nString;
		} catch (Exception e) {
			e.printStackTrace();
			return new String[] { "" };
		}

	}

	/**
	 * --------------------------------以上全部被弃用-------------------------------
	 * ----------------------------管理层主要面向对象的业务逻辑-------------------------
	 * ----------------------------国家、省、城市、地区-------------------------
	 */

	/**
	 * 根据cityCode查找市
	 * 
	 * @param cityId
	 * @return 城市名字
	 */
	public CityItem getCity(int cityId) {
		CityItem cityItem;
		Cursor cur = dbHelper.getDatabase().rawQuery("select * from t_area where id = '" + cityId + "'", null);
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			cityItem = CursorUtil.fromJson(cur, CityItem.class);
			cur.close();
			if (cityItem != null)
				return cityItem;
		}
		return null;
	}

	/**
	 * 根据cityName查找市
	 * 
	 * @param cityName
	 * @return
	 */
	public CityItem getCityByName(String cityName) {
		CityItem cityItem;
		Cursor cur = dbHelper.getDatabase().rawQuery("select * from t_area where name like '" + cityName + "'  ", null);
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			cityItem = CursorUtil.fromJson(cur, CityItem.class);
			cur.close();
			if (cityItem != null)
				return cityItem;
		}
		return null;
	}

	/**
	 * 根据provinceID查找省
	 * 
	 * @param provinceId
	 * @return
	 */
	public ProvinceItem getProvince(int provinceId) {
		ProvinceItem provinceItem;
		Cursor cur = dbHelper.getDatabase().rawQuery("select * from t_area where id = '" + provinceId + "'", null);
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			provinceItem = CursorUtil.fromJson(cur, ProvinceItem.class);
			cur.close();
			if (provinceItem != null)
				return provinceItem;
		}
		return null;
	}

	/**
	 * 根据provideName查找省
	 * 
	 * @param provinceName
	 * @return
	 */
	public ProvinceItem getProvinceByName(String provinceName) {
		ProvinceItem provinceItem;
		Cursor cur = dbHelper.getDatabase().rawQuery("select * from t_area where name like '" + provinceName + "'   ", null);
		if (cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			provinceItem = CursorUtil.fromJson(cur, ProvinceItem.class);
			cur.close();
			if (provinceItem != null)
				return provinceItem;
		}
		return null;
	}

	/**
	 * 根据countryCode查找国家
	 * 
	 * @param countryCode
	 * @return
	 */
//	public CountryItem getCountry(String countryCode) {
//		CountryItem countryItem;
//		Cursor cur = dbHelper.getDatabase().rawQuery("select * from country where id = '" + countryCode + "'", null);
//		if (cur != null && cur.getCount() > 0) {
//			cur.moveToFirst();
//			countryItem = CursorUtil.fromJson(cur, CountryItem.class);
//			cur.close();
//			if (countryItem != null)
//				return countryItem;
//		}
//		return null;
//	}

	/**
	 * 查询所有国家 含有index和preSortName的CountryItem
	 * 
	 * @param sql
	 * @return
	 */
//	public List<Item> getAllCountry() {
//		return getAllCountry("select * from country order by sortName ");
//
//	}

	/**
	 * 根据sql筛选国家，目前只有根据输入国家名称模糊查询用到
	 * 
	 * @param sql
	 * @return
	 */
//	public List<Item> getAllCountry(String sql) {
//		List<Item> itemList = new ArrayList<Item>();
//		Cursor cursor = dbHelper.getDatabase().rawQuery(sql, null);
//		if (cursor != null && cursor.getCount() > 0) {
//			// int index = 0;
//			String preSortName = "";
//			while (cursor.moveToNext()) {
//				CountryItem countryItem = CursorUtil.fromJson(cursor, CountryItem.class);
//				if (countryItem != null) {
//					countryItem.setPreSortName(preSortName);
//					preSortName = countryItem.getSortName();
//					itemList.add(countryItem);
//				}
//
//			}
//		}
//		JKLog.i("AreaDBManage", "getAllCountry方法中-------->：cursor.getCount()为：" + cursor.getCount());
//		cursor.close();
//		return itemList;
//	}

	/**
	 * 查询所有省
	 * 
	 * @return
	 */
	public List<Item> getAllProvince() {
		return getAllProvince("sortProvince");
	}

	/**
	 * 查询所有省
	 * 
	 * @return
	 */
	public List<Item> getAllProvince(String orderBy) {
		return getProvincesBySql("select * from t_area where parentid = '0'");
	}

	/**
	 * 根据sql筛选省，目前只有根据输入省名称模糊查询用到
	 * 
	 * @param sql
	 * @return
	 */
	public List<Item> getProvincesBySql(String sql) {
		List<Item> itemList = new ArrayList<Item>();
		Cursor cursor = dbHelper.getDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			String preSortName = "";
			while (cursor.moveToNext()) {
				ProvinceItem provinceItem = CursorUtil.fromJson(cursor, ProvinceItem.class);
				if (provinceItem != null) {
//					provinceItem.setPreSortName(preSortName);
//					preSortName = provinceItem.getSortProvince();
					itemList.add(provinceItem);
				}

			}
		}
		JKLog.i("AreaDBManage", "getAllProvince方法中-------->：cursor.getCount()为：" + cursor.getCount());
		cursor.close();
		return itemList;
	}

	/**
	 * 查询某省所有城市
	 * 
	 * @return
	 */
	public List<Item> getAllCity(int provinceID) {
		return getAllCity(provinceID, "sortCity");
	}

	public List<Item> getAllCity(int provinceID, String orderBy) {
		return getCitysBySql("select * from t_area where parentid = '" + provinceID + "'");
	}

	/**
	 * 根据sql筛选城市，目前只有根据输入城市名称模糊查询用到
	 * 
	 * @param sql
	 * @return
	 */
	public List<Item> getCitysBySql(String sql) {
		List<Item> itemList = new ArrayList<Item>();
		Cursor cursor = dbHelper.getDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			String preSortName = "";
			while (cursor.moveToNext()) {
				CityItem cityItem = CursorUtil.fromJson(cursor, CityItem.class);
				if (cityItem != null) {
//					cityItem.setPreSortName(preSortName);
//					preSortName = cityItem.getSortCity();
					itemList.add(cityItem);
				}
			}
		}
		JKLog.i("AreaDBManage", "getAllCity方法中-------->：cursor.getCount()为：" + cursor.getCount());
		cursor.close();
		return itemList;
	}

	/**
	 * 查询某城市所有区
	 * 
	 * @return
	 */
	public List<Item> getAllArea(int cityID) {
		List<Item> itemList = new ArrayList<Item>();
		Cursor cursor = dbHelper.getDatabase().rawQuery("select * from t_area where parentid = '" + cityID + "'", null);
		if (cursor != null && cursor.getCount() > 0) {
			String preSortName = "";
			while (cursor.moveToNext()) {
				AreaItem areaItem = CursorUtil.fromJson(cursor, AreaItem.class);
				if (areaItem != null) {
//					areaItem.setPreSortName(preSortName);
//					preSortName = areaItem.getSortArea();
					itemList.add(areaItem);
				}
			}
		}
		JKLog.i("AreaDBManage", "getAllArea方法中-------->：cursor.getCount()为：" + cursor.getCount());
		cursor.close();
		return itemList;
	}

}
