package com.jiakang.pandlife.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.config.ConfigSharedPreferences;

import java.util.ArrayList;
import java.util.List;


/**
 * @ClassName: AutoCompleteTextViewUtil
 * @author:hlu
 * @Date：2013-4-17
 * @version V1.0.0
 * @Description: AutoCompleteTextView 实现的自动匹配的实现
 */
public class AutoCompleteTextViewUtil {

	/**
	 * 默认spf文件名
	 */
	// private static String spfName = "AutoCompleteTextView";
	// private static SharedPreferences spf;

	/**
	 * 初始化AutoCompleteTextView，最多显示5项提示，使 AutoCompleteTextView在一开始获得焦点时自动提示
	 * 
	 * @param field
	 *            保存在sharedPreference中的字段名
	 */
	public static void showAutoComplete(final AutoCompleteTextView autoTv) {
		int id = autoTv.getId();
		if (id == View.NO_ID)
			id = 0x99;
		// id作为引所值
		final String field = String.valueOf(id);
		autoTv.setDropDownHeight(350);
		autoTv.setThreshold(1);
		// autoTv.setCompletionHint("最近的5条记录");
		autoTv.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				autoTv.setAdapter(null);
				SharedPreferences autoCompleteTextSpf = PandLifeApp.getInstance().getConfigSpfByName(ConfigSharedPreferences.NAME_AUTOCOMPLETE);
				String longhistory = autoCompleteTextSpf.getString(field, "");// ab,abc,abcd,addd
				String[] hisArrays = longhistory.split(",");
				// // 只保留最近的50条的记录
				if (hisArrays.length > 50) {
					String[] newArrays = new String[50];
					System.arraycopy(hisArrays, 0, newArrays, 0, 50);
					MyArrayAdapter<String> arrAdapter = new MyArrayAdapter<String>(autoTv.getContext(), android.R.layout.simple_dropdown_item_1line, newArrays);
					autoTv.setAdapter(arrAdapter);
				} else {
					MyArrayAdapter<String> arrAdapter = new MyArrayAdapter<String>(autoTv.getContext(), android.R.layout.simple_dropdown_item_1line, hisArrays);
					autoTv.setAdapter(arrAdapter);
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.toString().length() > 0 && autoTv.getAdapter().getCount() > 1) {
					try {
						autoTv.showDropDown();
					} catch (Exception e) {
					}
				}

			}
		});
	}

	/**
	 * 把指定AutoCompleteTextView中内容保存到sharedPreference中指定的字符段
	 * 
	 * @param field
	 *            保存在sharedPreference中的字段名
	 */
	public static void saveHistory(AutoCompleteTextView autoTv) {
		int id = autoTv.getId();
		if (id == View.NO_ID)
			id = 0x99;
		final String field = String.valueOf(id);
		String text = autoTv.getText().toString();
		SharedPreferences autoCompleteTextSpf = PandLifeApp.getInstance().getConfigSpfByName(ConfigSharedPreferences.NAME_AUTOCOMPLETE);
		String longhistory = autoCompleteTextSpf.getString(field, "");// ab,abc,abcd,addd
		if (!longhistory.contains(text + ",")) {
			StringBuilder sb = new StringBuilder(longhistory);
			if (longhistory.length() > 0)
				sb.insert(0, text + ",");
			else
				sb.insert(0, text);
			autoCompleteTextSpf.edit().putString(field, sb.toString()).commit();
		}
	}

	/**
	 * ---------------------------------邮箱后缀提示----------------------------------
	 * ---
	 * 
	 * <pre>
	 * 网易@163.com邮箱：　   pop.163.com; 　　           smtp.163.com 
	 * 网易@yeah.net邮箱：    pop.yeah.net; 　　          smtp.yeah.net 
	 * 网易@netease.com邮箱： pop.netease.com; smtp.netease.com 
	 * 网易@126.com邮箱：     pop.126.com       　        smtp.126.com 
	 * 搜狐@sohu.com：        pop3.sohu.com；             smtp.sohu.com 
	 * 信网@eyou.com：        pop3.eyou.com；             mx.eyou.com 
	 * 新飞网@tom.com：　     pop.tom.com；               smtp.tom.com 
	 * 亿唐@etang.com：　     pop.free.etang.com          smtp.free.etang.com 
	 * 21世纪@21cn.com： 　   pop.21cn.com； 　　　　　   smtp.21cn.com
	 * </pre>
	 */
	// private static String[] stringArray = { "@qq.com", "@163.com",
	// "@126.com", "@sina.com", "@taobao.com" };
	private static String[] stringArray = { "@qq.com", "@163.com", "@126.com", "@sohu.com", "@21cn.com" };

	/**
	 * 构造邮箱后缀提示编辑框
	 * 
	 * @param autoview
	 */
	public static void showEndWithEmail(final AutoCompleteTextView autoview) {
		Context ctx = autoview.getContext();
		final MyAdatper adapter = new MyAdatper(ctx);
		autoview.setAdapter(adapter);
		// default=2
		autoview.setThreshold(1);
		autoview.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				JKLog.i("AutoCompleteTextViewUtil", "afterTextChanged方法中-------->：s为：" + s);
				String input = s.toString();
				adapter.mList.clear();
				if (input.contains("@")) {
					input = input.substring(0, input.indexOf("@"));
					for (int i = 0; i < stringArray.length; ++i) {
						adapter.mList.add(input + stringArray[i]);
					}
					adapter.notifyDataSetChanged();
					try {
						autoview.showDropDown();
					} catch (Exception e) {
					}

				}

			}
		});

	}

	private static class MyArrayAdapter<String> extends ArrayAdapter<String> {

		public MyArrayAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
		}

		public MyArrayAdapter(Context context, int textViewResourceId, String[] objects) {
			super(context, textViewResourceId, objects);
		}

		public MyArrayAdapter(Context context, int resource, int textViewResourceId, List<String> objects) {
			super(context, resource, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
		}

		public MyArrayAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
			super(context, resource, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
		}

		public MyArrayAdapter(Context context, int resource, int textViewResourceId) {
			super(context, resource, textViewResourceId);
			// TODO Auto-generated constructor stub
		}

		public MyArrayAdapter(Context context, int textViewResourceId, List<String> objects) {
			super(context, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
		}

	}

	private static class MyAdatper extends BaseAdapter implements Filterable {

		List<String> mList;
		private Context mContext;
		private MyFilter mFilter;

		public MyAdatper(Context context) {
			mContext = context;
			mList = new ArrayList<String>();
		}

		@Override
		public int getCount() {
			return mList == null ? 0 : mList.size();
		}

		@Override
		public Object getItem(int position) {
			return mList == null ? null : mList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				TextView tv = new TextView(mContext);
				tv.setPadding(10, 10, 10, 10);
				tv.setTextColor(Color.BLACK);
				tv.setTextSize(20);
				convertView = tv;
			}
			TextView txt = (TextView) convertView;
			txt.setText(mList.get(position));
			return txt;
		}

		@Override
		public Filter getFilter() {
			if (mFilter == null) {
				mFilter = new MyFilter();
			}
			return mFilter;
		}

		private class MyFilter extends Filter {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				JKLog.i("", "AutoCompleteTextViewUtil.MyAdatper.MyFilter中：constraint为：" + constraint);
				FilterResults results = new FilterResults();
				if (mList == null) {
					mList = new ArrayList<String>();
				}
				results.values = mList;
				results.count = mList.size();
				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				if (results.count > 0) {
					notifyDataSetChanged();
				} else {
					try {
						notifyDataSetInvalidated();
					} catch (Exception e) {
						JKLog.e("AutoCompleteTextViewUtil", "publishResults方法中-------->：e为：" + e);
					}

				}
			}

		}

	}

}