package com.jiakang.pandlife.utils;

import java.security.MessageDigest;

/**
 * Class<code>MD5.java</code>
 */
public class MD5 {

	public static String getMD5(byte[] word) throws Exception {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(word);
		byte[] desKey = md.digest();
		String mdv = ByteFormatUtils.byte2hex(desKey);
		return mdv;
	}

	public static String toMD5(byte[] source) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(source);
			StringBuffer buf = new StringBuffer();
			for (byte b : md.digest())
				buf.append(String.format("%02x", b & 0xff));
			return buf.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}