package com.jiakang.pandlife.info;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author cwang
 * 
 */
public interface BaseInterfaceInfo {
	/** 请求成功code */
	String CODE_SUCCESS = "0";
	/** 请求失败code */
	String CODE_FAIL = "1";
	/** 请求超时code */
	String CODE_TIMEOUT = "2";

	/**
	 * 请求字段
	 */
	String requestStr();

	/**
	 * 请求有效期
	 */
	int requestDate();

	/**
	 * 请求地址
	 */
	String requestUrl();

	/**
	 * 请求参数封装
	 */
	JSONObject requestParams();

	/**
	 * 请求结果解析
	 */
	void responseData(JSONObject jsonObject);

	/**
	 * 请求结果，0成功，1失败
	 */
	String requestResult();

	String getMessage();

	void getFromDb(JSONArray jsonArray);

	void setRequestResult(String result);



}
