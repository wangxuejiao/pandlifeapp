package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.DeliveryItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.List;
import java.util.regex.Pattern;

/**
 * 选择地区
 */
@EActivity(R.layout.activity_select_single_data)
public class SelectSingleDataActivity extends BaseActy {
    protected static final String TAG = "SelectSingleDataActivity";

    @ViewById(R.id.assd_lv_single_data)
    ListView dataLV;

    private String data;

    private List<DeliveryItem> mDeliveryItems;
    private ItemAdapter mItemAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar() {
        Intent intent = getIntent();
        String title = intent.getExtras().getString("title");
        data = intent.getExtras().getString("data");
        mDeliveryItems = (List<DeliveryItem>) intent.getSerializableExtra("list");
        initTitleBar(R.id.assd_tb_title, title);
        bindView();
    }

    private void bindView() {

        if(!TextUtils.isEmpty(data)){
            //以","号拆分字符串
            String pattern=",";
            Pattern pat = Pattern.compile(pattern);
            String s = data.substring(1, data.lastIndexOf(']'));
            final String[] goodsKindArr = pat.split(s);
            ArrayAdapter arrayAdapter = new ArrayAdapter<String>(mContext,R.layout.item_array_select_left, goodsKindArr);
            dataLV.setAdapter(arrayAdapter);
            dataLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    ListView listView = (ListView) parent;
                    String selectGoodsKindStr = (String) listView.getItemAtPosition(position);
                    Intent intent = new Intent();
                    intent.putExtra("data", selectGoodsKindStr);
                    setResult(RESULT_OK, intent);
                    finish();
                }

            });
        }
        else if (mDeliveryItems.size() != 0){
            mItemAdapter = new ItemAdapter(mContext);
            mItemAdapter.setItems((List)mDeliveryItems);
            dataLV.setAdapter(mItemAdapter);
            dataLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ListView listViewe = (ListView)parent;
                    DeliveryItem deliveryItem = (DeliveryItem) listViewe.getItemAtPosition(position);
                    Intent intent = new Intent();
                    intent.putExtra("deliveryItem", deliveryItem);
                    deliveryItem.setTag("select");
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });
        }

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
