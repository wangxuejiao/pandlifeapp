package com.jiakang.pandlife.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.annotation.utils.SqlBuilder;
import com.jiakang.pandlife.item.AdvertisementItem;
import com.jiakang.pandlife.item.ConsumptionDetailItem;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.GroupPurchaseItem;
import com.jiakang.pandlife.item.JpushNotificationItem;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.item.NearbyMerchantItem;
import com.jiakang.pandlife.item.PostDetail;
import com.jiakang.pandlife.item.RedPackageItem;
import com.jiakang.pandlife.item.ReplayItem;
import com.jiakang.pandlife.item.SendBagItem;
import com.jiakang.pandlife.utils.JKLog;

import cn.jpush.android.api.PushNotificationBuilder;


/**
 * 默认就在数据库里创建5张表
 * 
 * @FriendItem
 */
public class DataCacheDBHelper extends SQLiteOpenHelper {
	private static final String TAG = "DataCacheDBHelper";

	private String dbName;// 数据库名称

	
	private static final int version = 40;// 数据库版本

	/** 不登录下广告缓存表 */
	public final static String TAB_ADVERTISEMENT_CACHE = "tab_advertisement_cache";
//	/** 热门城市缓存表 */
//	public final static String TAB_HOTCITY_CACHE = "tab_hotcity_cache";
//	/** 热门职位缓存表 */
//	public final static String TAB_HOTJOB_CACHE = "tab_hotjob_cache";
	/** 团购列表缓存表 */
	public final static String TAB_GROUPPURCHASE_CACHE = "tab_grouppurchase_cache";
	/** 我的包裹缓存表 */
	public final static String TAB_MYEXPRESS_CACHE = "tab_myexpress_cache";
	/** 附近商家缓存表 */
	public final static String TAB_NEARBY_MERCHANT_CACHE = "tab_nearby_merchant_cache";
	/** 收支明细缓存表 */
	public final static String TAB_CONSUMPTION_DETAIL_CACHE = "tab_consumption_detail_cache";
	/** 我的红包缓存表 */
	public final static String TAB_MY_REDPACKAGE_CACHE = "tab_my_redpackage_cache";
	/** 我的订单缓存表 */
	public final static String TAB_MYINDENT_CACHE = "tab_myindent_cache";
	/** 我的寄件缓存表 */
	public final static String TAB_MY_SENDEXPRESS_CACHE = "tab_my_sendexpress_cache";

	/**帖子详情缓存表**/
	public final static String TAB_POST_DETAIL_CACHE = "tab_post_detail_cache";

	/**
	 * 极光推送缓存
	 */
	public final static String TAB_JPUSH_NOTIFICATION_CACHE = "tab_jpush_notification_cache";

	/**
	 * 回复列表缓存表
	 */
	public static final String TAB_REPLY_CACHE = "tab_reply_cache";
	
	public DataCacheDBHelper(Context context, String name) {
		super(context, name, null, version);
		dbName = name;
		JKLog.e(TAG, "创建数据库----------------------------------->" + name);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		this.mDefaultWritableDatabase = db;
		JKLog.i("DBOpenHelper", "onCreate------->数据库创建了----");

		// 不登录下广告缓存表
		db.execSQL(SqlBuilder.getCreatTableSQL(AdvertisementItem.class));
		// 团购列表缓存表
		db.execSQL(SqlBuilder.getCreatTableSQL(GroupPurchaseItem.class));
		// 我的包裹缓存表
		db.execSQL(SqlBuilder.getCreatTableSQL(ExpressListItem.class));
		// 附近商家缓存表
		db.execSQL(SqlBuilder.getCreatTableSQL(NearbyMerchantItem.class));
		// 收支明细缓存表
		db.execSQL(SqlBuilder.getCreatTableSQL(ConsumptionDetailItem.class));
		// 我的红包缓存表
		db.execSQL(SqlBuilder.getCreatTableSQL(RedPackageItem.class));
		// 我的订单缓存表
		db.execSQL(SqlBuilder.getCreatTableSQL(MyIndentItem.class));
		// 我的寄件缓存表
		db.execSQL(SqlBuilder.getCreatTableSQL(SendBagItem.class));

		db.execSQL(SqlBuilder.getCreatTableSQL(PostDetail.DataEntityPostDetail.class));

		db.execSQL(SqlBuilder.getCreatTableSQL(ReplayItem.class));
		//极光推送
		db.execSQL(SqlBuilder.getCreatTableSQL(JpushNotificationItem.class));

		// 热门城市缓存表
//		db.execSQL(SqlBuilder.getCreatTableSQL(HotCityItem.class));
		// 热门职位缓存表
//		db.execSQL(SqlBuilder.getCreatTableSQL(HotJobItem.class));

	}


	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		this.mDefaultWritableDatabase = db;
		JKLog.e("DBOpenHelper", "onUpgrade-----> 数据库更新删除表了----oldVersion:" + oldVersion + ";---newVersion:" + newVersion + ";--Dbname:" + dbName);

		switch (oldVersion) {
		case 75:

		case 99:

			break;
		default:
			break;
		}

		// deleteIndex(db, "idx_cachemsg");
		onCreate(db);
	}

	/**
	 * 删除引所值
	 * 
	 * @param db
	 * @param index
	 * @return void
	 */
	public void deleteIndex(SQLiteDatabase db, String index) {
		try {
			db.execSQL("drop index " + index);
		} catch (Exception e) {
			JKLog.e(TAG, "deleteIndex方法中-------->：e为：" + e);
		}
	}
	
	
	private SQLiteDatabase mDefaultWritableDatabase;
	@Override
	public SQLiteDatabase getWritableDatabase() {
		if(mDefaultWritableDatabase != null){
			return mDefaultWritableDatabase;
		} else {
			return super.getWritableDatabase();
		}
	}

}