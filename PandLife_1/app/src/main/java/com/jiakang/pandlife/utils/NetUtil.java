package com.jiakang.pandlife.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

/**
 * @author：xuwangwang E-mail: 541765907@qq.com
 * @date：2014年7月25日 上午11:31:59
 * @version 1.0 String Tag = "NetUtil中：";
 */
public class NetUtil {

	/** 没有网络 */
	public static final int NETWORKTYPE_INVALID = 0;
	/** wap网络 */
	public static final int NETWORKTYPE_WAP = 1;
	/** 2G网络 */
	public static final int NETWORKTYPE_2G = 2;
	/** 3G和3G以上网络，或统称为快速网络 */
	public static final int NETWORKTYPE_3G = 3;
	/** wifi网络 */
	public static final int NETWORKTYPE_WIFI = 4;

	private static boolean isFastMobileNetwork(Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		switch (telephonyManager.getNetworkType()) {
		case TelephonyManager.NETWORK_TYPE_1xRTT:
			return false; // ~ 50-100 kbps
		case TelephonyManager.NETWORK_TYPE_CDMA:
			return false; // ~ 14-64 kbps
		case TelephonyManager.NETWORK_TYPE_EDGE:
			return false; // ~ 50-100 kbps
		case TelephonyManager.NETWORK_TYPE_EVDO_0:
			return true; // ~ 400-1000 kbps
		case TelephonyManager.NETWORK_TYPE_EVDO_A:
			return true; // ~ 600-1400 kbps
		case TelephonyManager.NETWORK_TYPE_GPRS:
			return false; // ~ 100 kbps
		case TelephonyManager.NETWORK_TYPE_HSDPA:
			return true; // ~ 2-14 Mbps
		case TelephonyManager.NETWORK_TYPE_HSPA:
			return true; // ~ 700-1700 kbps
		case TelephonyManager.NETWORK_TYPE_HSUPA:
			return true; // ~ 1-23 Mbps
		case TelephonyManager.NETWORK_TYPE_UMTS:
			return true; // ~ 400-7000 kbps
		case TelephonyManager.NETWORK_TYPE_EHRPD:
			return true; // ~ 1-2 Mbps
		case TelephonyManager.NETWORK_TYPE_EVDO_B:
			return true; // ~ 5 Mbps
		case TelephonyManager.NETWORK_TYPE_HSPAP:
			return true; // ~ 10-20 Mbps
		case TelephonyManager.NETWORK_TYPE_IDEN:
			return false; // ~25 kbps
		case TelephonyManager.NETWORK_TYPE_LTE:
			return true; // ~ 10+ Mbps
		case TelephonyManager.NETWORK_TYPE_UNKNOWN:
			return false;
		default:
			return false;
		}
	}

	/**
	 * 获取网络状态，wifi,wap,2g,3g.
	 * 
	 * @param context
	 *            上下文
	 * @return int 网络状态 {@link #NETWORKTYPE_2G},{@link #NETWORKTYPE_3G},          *
	 *         {@link #NETWORKTYPE_INVALID},{@link #NETWORKTYPE_WAP}*
	 *         <p>
	 *         {@link #NETWORKTYPE_WIFI}
	 */

	public static int getNetWorkType(Context context) {
		int mNetWorkType = NETWORKTYPE_INVALID;

		ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = manager.getActiveNetworkInfo();

		if (networkInfo != null && networkInfo.isConnected()) {
			String type = networkInfo.getTypeName();

			if (type.equalsIgnoreCase("WIFI")) {
				mNetWorkType = NETWORKTYPE_WIFI;
			} else if (type.equalsIgnoreCase("MOBILE")) {
				String proxyHost = android.net.Proxy.getDefaultHost();

				mNetWorkType = TextUtils.isEmpty(proxyHost) ? (isFastMobileNetwork(context) ? NETWORKTYPE_3G : NETWORKTYPE_2G) : NETWORKTYPE_WAP;
			}
		} else {
			mNetWorkType = NETWORKTYPE_INVALID;
		}

		return mNetWorkType;
	}

	/**
	 * IMSI共有15位，其结构如下：
		MCC+MNC+MIN
		MCC：Mobile Country Code，移动国家码，共3位，中国为460;
		MNC:Mobile Network Code，移动网络码，共2位，电信03，移动02/00，联通GSM 01，一个典型的IMSI号码为460030912121001;
		MIN共有10位，其结构如下：
		09+M0M1M2M3+ABCD
		其中的M0M1M2M3和MDN号码中的H0H1H2H3可存在对应关系，ABCD四位为自由分配。
		可以看出IMSI在MIN号码前加了MCC，可以区别出每个用户的来自的国家，因此可以实现国际漫游。在同一个国家内，如果有多个CDMA运营商，可以通过MNC来进行区别.

		功能 说明
		getCellLocation（） 返回的单元格位置的装置
		ACCESS_COARSE_LOCATION或ACCESS_FINE_LOCATION
		getDeviceId（） 返回的IMEI / MEID的设备。 如果该设备是GSM设备
		然后IMEI号将被退回，如果该设备是一个CDMA设备然后MEID
		将被退回
		READ_PHONE_STATE
		getLine1Number（） 返回设备的电话号码（MSISDN号码）
		READ_PHONE_STATE
		getNetworkOperatorName（） 返回注册的网络运营商的名字
		getNetworkOperator（） 返回的MCC +跨国公司的注册网络运营商
		getNetworkCountryIso（） 返回注册的网络运营商的国家代码
		getSimCountryIso（） 返回SIM卡运营商的国家代码
		READ_PHONE_STATE
		getSimOperator（） 返回SIM卡运营商的单个核细胞数+冶
		READ_PHONE_STATE
		getSimOperatorName（） 返回SIM卡运营商的名字
		READ_PHONE_STATE
		getSimSerialNumber（） 返回SIM卡的序列号
		READ_PHONE_STATE
		getNetworkType（） 返回网络设备可用的类型。 这将是
		下列其中一个值：
		TelephonyManager.NETWORK_TYPE_UNKNOWN

		TelephonyManager.NETWORK_TYPE_GPRS

		TelephonyManager.NETWORK_TYPE_EDGE

		TelephonyManager.NETWORK_TYPE_UMTS

		READ_PHONE_STATE

		getPhoneType（） 返回设备的类型。 这将是以下值之一：
		TelephonyManager.PHONE_TYPE_NONE

		TelephonyManager.PHONE_TYPE_GSM

		TelephonyManager.PHONE_TYPE_CDMA


		READ_PHONE_STATE

		getSubscriberId（） 返回用户识别码（的IMSI）的设备
		READ_PHONE_STATE
		getNeighboringCellInfo（） 返回NeighboringCellInfo类代表名单
		相邻小区的信息，如果可用，否则将
		返回null
		ACCESS_COARSE_UPDATES
	 * @param context
	 * @return void
	 */
	public static void getYunyinshang(Context context){
		TelephonyManager tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		// 电信03，移动02，联通GSM 01
		String simOperator = tel.getSimOperator();
		Log.e("", "ddd方法中-------->：simOperator为：" + simOperator);
		// simOperator()为：46001
		
		Log.e("", "ddd方法中-------->：getDeviceId()为：" + tel.getDeviceId());
		// getDeviceId()为：860312026876993
		
		Log.e("", "ddd方法中-------->：getLine1Number()为：" + tel.getLine1Number());
		// getLine1Number()为：
		
		Log.e("", "ddd方法中-------->：getNetworkOperatorName()为：" + tel.getNetworkOperatorName());
		// getNetworkOperatorName()为：China Unicom
		// getNetworkOperatorName()为：CHINA MOBILE
		// 电信的无

		
		Log.e("", "ddd方法中-------->：getNetworkOperator()为：" + tel.getNetworkOperator());
		// getNetworkOperator()为：46000 移动
		// getNetworkOperator()为：46001 联通
		// getNetworkOperator()为：46003 电信

		Log.e("", "ddd方法中-------->：getSimCountryIso()为：" + tel.getSimCountryIso());
		// getSimCountryIso()为：cn

		Log.e("", "ddd方法中-------->：getSimOperator()为：" + tel.getSimOperator());
		// getSimOperator()为：46001

		
		Log.e("", "ddd方法中-------->：getSimOperatorName()为：" + tel.getSimOperatorName());
		// getSimOperatorName()为：
		
		Log.e("", "ddd方法中-------->：getSimSerialNumber()为：" + tel.getSimSerialNumber());
		// getSimSerialNumber()为：89860113011300060177

		Log.e("", "ddd方法中-------->：getNetworkType()为：" + tel.getNetworkType());
		// getNetworkType()为：2
	}
	
	
	
	
	
	
	
	
	/** 移动 */
	public static final int YYS_YIDONG = 1;
	/** 联通 */
	public static final int YYS_LIANTONG = 2;
	/** 电信 */
	public static final int YYS_DIANXIN = 3;
	/** 未知*/
	public static final int YYS_UNKNOWN = 4;
	
	/**
	 * 匹配移动、联通、电信
	 * @param phone_number
	 * @return int type
	 */
	public static int matchesPhoneNumber(String phone_number) { 

		String cm = "^((13[4-9])|(147)|(15[0-2,7-9])|(18[2-3,7-8]))\\d{8}$"; 
		String cu = "^((13[0-2])|(145)|(15[5-6])|(186))\\d{8}$"; 
		String ct = "^((133)|(153)|(18[0,9]))\\d{8}$"; 

		int flag = 0; 
		if (phone_number.matches(cm)) { 
			flag = 1; 
		} else if (phone_number.matches(cu)) { 
			flag = 2; 
		} else if (phone_number.matches(ct)) { 
			flag = 3; 
		} else { 
			flag = 4; 
		} 
		return flag; 

	} 

	/**
	 * 通过手机SIM卡获取运营商
	 * // getNetworkOperator()为：46000/46002移动
	 * // getNetworkOperator()为：46001 联通
	 * // getNetworkOperator()为：46003 电信
	 *  
	 * @param mContext
	 * @return String
	 */
	public static int getYunyingshang(Context mContext){
		TelephonyManager tel = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
		String kind = tel.getNetworkOperator();
		if (kind.contains("46000")|| kind.contains("46002")) {
			return YYS_YIDONG;
		}else if (kind.contains("46001")) {
			return YYS_LIANTONG;
		}else if (kind.contains("46003")) {
			return YYS_DIANXIN;
		}else {
			return YYS_UNKNOWN;
		}
	}
	/**
	 * 通过手机号获取运营商
	 * @param x
	 * @return void
	 */
	public static String getYunyingshang(int type){
		String result = "未知";
		switch(type){
		case 1 : 
			result = "移动";
			System.out.println("移动"); 
			break; 
		case 2: 
			result = "联通 ";
			System.out.println("联通 "); 
			break; 
		case 3 : 
			result = "电信";
			System.out.println("电信"); 
			break; 
		case 4: 
			System.out.println("输入有误"); 
			break; 
		default:
			System.out.println("输入有误"); 
			break;
		}
		return result;

	}
	
	/**
	 * 获取手机ip
	 * @return
	 * @return String
	 */
	public static String getPhoneIp() {
        try {  
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {  
                NetworkInterface intf = en.nextElement();  
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {  
                    InetAddress inetAddress = enumIpAddr.nextElement();  
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {  
                    //if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet6Address) {  
                        return inetAddress.getHostAddress().toString();  
                    }  
                }  
            }  
        } catch (Exception e) {  
        }  
        return ""; 
    }
	
	
	
	
	
}
