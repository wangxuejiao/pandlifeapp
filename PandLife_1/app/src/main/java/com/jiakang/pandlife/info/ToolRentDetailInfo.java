package com.jiakang.pandlife.info;

import android.util.Log;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.item.ToolRentDetail;
import com.jiakang.pandlife.utils.DataCache;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by play on 2016/1/12.
 */
public class ToolRentDetailInfo extends BaseAbsInfo {

    private int id;
    private String address;
    private int backTime;

    private ToolRentDetail toolRentDetail;
    private ToolRentDetail.DataEntity toolRentDetailEntity;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ToolRentDetail getToolRentDetail() {
        return toolRentDetail;
    }

    public void setToolRentDetail(ToolRentDetail toolRentDetail) {
        this.toolRentDetail = toolRentDetail;
    }

    public ToolRentDetail.DataEntity getToolRentDetailEntity() {
        return toolRentDetailEntity;
    }

    public void setToolRentDetailEntity(ToolRentDetail.DataEntity toolRentDetailEntity) {
        this.toolRentDetailEntity = toolRentDetailEntity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getBackTime() {
        return backTime;
    }

    public void setBackTime(int backTime) {
        this.backTime = backTime;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Server&a=Leasedetailed&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("id",id);
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        Log.i(TAG,"---->JSONObject:" + jsonObject.toString());
        toolRentDetail = BaseInfo.gson.fromJson(jsonObject.toString(), ToolRentDetail.class);
        toolRentDetailEntity = toolRentDetail.getData();
        DataCache dataCache = DataCache.get(PandLifeApp.getInstance());
        String key = "toolRentDetailEntity" + toolRentDetailEntity.getId();
        dataCache.put(key,jsonObject,10 * 60);
    }
}
