package com.jiakang.pandlife.info.nearmerchase;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.info.BaseAbsInfo;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.GoodsKindItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 2.商家分类获取接口
 *
 * @author ww
 *
 */
public class GetCategoryInfo extends BaseAbsInfo {

    private static final String TAG = "GetCategoryInfo";

    /** 商家id	Int(10) */
    private int sid;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;
    /** 页面索引	Int(10) */
    private int pageIndex;
    /** 分页大小	Int(10) */
    private int pageSize;

    private List<GoodsKindItem> allItems = new ArrayList<GoodsKindItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Trader&a=Getcategory" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("sid", sid);
            json.put("pageIndex", pageIndex);
            json.put("pageSize", pageSize);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                GoodsKindItem goodsKindItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    goodsKindItem = BaseInfo.gson.fromJson(itemStr, GoodsKindItem.class);
                    goodsKindItem.setLayout(R.layout.item_select_single_center);
                    // 入库
//                    goodsKindItem.insert();
                    allItems.add(goodsKindItem);
                }

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<GoodsKindItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<GoodsKindItem> allItems) {
        this.allItems = allItems;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
