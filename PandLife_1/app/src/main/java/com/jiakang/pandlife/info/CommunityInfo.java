package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.CommunityItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取小区接口
 *
 * @author ww
 *
 */
public class CommunityInfo extends BaseAbsInfo {

    private static final String TAG = "CommunityInfo";

    /** 省份编码的code值 	Int(10) */
    private int province_id;
    /** 城市编码的code值	Int(10) */
    private int city_id;
    /** 地区编码的code值	Int(10) */
    private int zone_id;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<CommunityItem> allItems = new ArrayList<CommunityItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Address&a=Community" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("province_id", province_id);
            json.put("city_id", city_id);
            json.put("zone_id", zone_id);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                CommunityItem communityItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    communityItem = BaseInfo.gson.fromJson(itemStr, CommunityItem.class);
                    communityItem.setLayout(R.layout.item_select_single_left);
                    // 入库
//                    recommendRecruitItem.insert();
                    allItems.add(communityItem);
                }

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<CommunityItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<CommunityItem> allItems) {
        this.allItems = allItems;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getZone_id() {
        return zone_id;
    }

    public void setZone_id(int zone_id) {
        this.zone_id = zone_id;
    }
}
