package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.GetDefaultSenderAddressInfo;
import com.jiakang.pandlife.info.GetSenderListInfo;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.ProvinceItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的地址簿
 * Created by Administrator on 2016/1/8.
 */
@EActivity(R.layout.activity_my_address_book)
public class MyAddressBookActivity extends BaseActy {
    protected static final String TAG = "MyAddressBookActivity";


    @ViewById(R.id.amab_lv_address)
    ListView addressLV;
    @ViewById(R.id.amab_btn_add_address)
    Button addAddressBN;

    private ItemAdapter addressItemAdapter;


    /**地址类型（收件人/发件人）*/
    public static final String ADDRESS_TYPE = "address_type";
    /**发件人地址*/
    public static final String SENDER_ADDRESS = "sender_address";
    /**收件人地址*/
    public static final String RECIPIENTS_ADDRESS = "recipients_address";

    /**获取我的默认地址接口*/
    private GetDefaultSenderAddressInfo mGetDefaultSenderAddressInfo = new GetDefaultSenderAddressInfo();
    /**获取我的地址接口*/
    private GetSenderListInfo mGetSenderListInfo = new GetSenderListInfo();

    //记录默认地址的id，将获取的所有地址中去除这个默认地址
    private int defaultAddressId;

    private List<AddressItem> allItems = new ArrayList<AddressItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.amab_tb_title, "我的地址");

        initView();
        bindInfo();
    }

    private void initView(){
        addAddressBN.setOnClickListener(this);
        addressItemAdapter = new ItemAdapter(mContext);
        addressLV.setAdapter(addressItemAdapter);
    }

    private void bindInfo(){
        addressItemAdapter.clear();
        addressItemAdapter.notifyDataSetChanged();

        ApiManager apiManagerDefault = ApiManager.getInstance();
        apiManagerDefault.request(mGetDefaultSenderAddressInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        defaultAddressId = mGetDefaultSenderAddressInfo.getAddressItem().getId();
                        addressItemAdapter.add(mGetDefaultSenderAddressInfo.getAddressItem());

                        //防止获取默认地址比获取列表慢，就将列表获取放在之后
                        ApiManager apiManagerList = ApiManager.getInstance();
                        apiManagerList.request(mGetSenderListInfo, new AbsOnRequestListener(mContext) {
                            @Override
                            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                                super.onRequestSuccess(stat, jsonObject);
                                try {
                                    if (stat == 1) {

                                        if (mGetSenderListInfo.getAllItems().size() == 0) {
                                            return;
                                        }
                                        for (int i = 0; i < mGetSenderListInfo.getAllItems().size(); i++) {
                                            if (defaultAddressId != mGetSenderListInfo.getAllItems().get(i).getId()) {
                                                addressItemAdapter.add(mGetSenderListInfo.getAllItems().get(i));
                                            }
                                        }
//                                      addressItemAdapter.addItems(1,(List) mGetSenderListInfo.getAllItems());
                                        addressItemAdapter.notifyDataSetChanged();

                                        addressLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                ListView listView = (ListView) parent;
                                                AddressItem addressItem = (AddressItem) listView.getItemAtPosition(position);
                                                Intent intentAddressRedact = new Intent(MyAddressBookActivity.this, AddressDetailActivity_.class);
                                                intentAddressRedact.putExtra("addressItem", addressItem);
                                                intentAddressRedact.putExtra(ADDRESS_TYPE, SENDER_ADDRESS);
                                                startActivityForResult(intentAddressRedact, Constant.StaticCode.REQUEST_REDACT_ADDRESS);
                                            }
                                        });
                                    } else {
                                    }
                                } catch (Exception e) {

                                }
                            }
                        });
                    } else {
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onRequestFail(int result, JSONObject jsonObject) {
                super.onRequestFail(result, jsonObject);
                //防止获取默认地址失败（还未设置默认地址），所以这里也做一个请求
                ApiManager apiManagerList = ApiManager.getInstance();
                apiManagerList.request(mGetSenderListInfo, new AbsOnRequestListener(mContext) {
                    @Override
                    public void onRequestSuccess(int stat, JSONObject jsonObject) {
                        super.onRequestSuccess(stat, jsonObject);
                        try {
                            if (stat == 1) {

                                if (mGetSenderListInfo.getAllItems().size() == 0) {
                                    return;
                                }
                                for (int i = 0; i < mGetSenderListInfo.getAllItems().size(); i++) {
                                    if (defaultAddressId != mGetSenderListInfo.getAllItems().get(i).getId()) {
                                        addressItemAdapter.add(mGetSenderListInfo.getAllItems().get(i));
                                    }
                                }
//                                      addressItemAdapter.addItems(1,(List) mGetSenderListInfo.getAllItems());
                                addressItemAdapter.notifyDataSetChanged();

                                addressLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                        ListView listView = (ListView) parent;
                                        AddressItem addressItem = (AddressItem) listView.getItemAtPosition(position);
                                        Intent intentAddressRedact = new Intent(MyAddressBookActivity.this, AddressDetailActivity_.class);
                                        intentAddressRedact.putExtra("addressItem", addressItem);
                                        intentAddressRedact.putExtra(ADDRESS_TYPE, SENDER_ADDRESS);
                                        startActivityForResult(intentAddressRedact, Constant.StaticCode.REQUEST_REDACT_ADDRESS);
                                    }
                                });
                            } else {
                            }
                        } catch (Exception e) {

                        }
                    }
                });
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_REDACT_ADDRESS) && (resultCode == RESULT_CANCELED)){
            bindInfo();
        }
        if ((requestCode == Constant.StaticCode.REQUEST_SENDER_ADDRESS) && (resultCode == RESULT_OK)){
            bindInfo();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.amab_btn_add_address:
                Intent intentAddAddress = new Intent(this, AddressAdditionActivity_.class);
                intentAddAddress.putExtra(ADDRESS_TYPE, SENDER_ADDRESS);
                startActivityForResult(intentAddAddress, Constant.StaticCode.REQUEST_SENDER_ADDRESS);
                break;
        }
    }
}
