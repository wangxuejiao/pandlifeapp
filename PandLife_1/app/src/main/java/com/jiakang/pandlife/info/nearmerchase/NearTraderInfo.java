package com.jiakang.pandlife.info.nearmerchase;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.info.BaseAbsInfo;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.item.NearbyMerchantItem;
import com.jiakang.pandlife.utils.JKLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 1.附近商家接口
 *
 * @author ww
 *
 */
public class NearTraderInfo extends BaseAbsInfo {

    private static final String TAG = "NearTraderInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<NearbyMerchantItem> allItems = new ArrayList<NearbyMerchantItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Trader&a=Neartrader" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                NearbyMerchantItem nearbyMerchantItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    nearbyMerchantItem = BaseInfo.gson.fromJson(itemStr, NearbyMerchantItem.class);
                    // 入库
                    nearbyMerchantItem.insert();
                    allItems.add(nearbyMerchantItem);
                }
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<NearbyMerchantItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<NearbyMerchantItem> allItems) {
        this.allItems = allItems;
    }
}
