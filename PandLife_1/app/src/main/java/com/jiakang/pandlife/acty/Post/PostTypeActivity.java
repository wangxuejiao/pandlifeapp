package com.jiakang.pandlife.acty.Post;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.PostTypeInfo;
import com.jiakang.pandlife.item.PostTypeItem;

import org.json.JSONObject;

import java.util.List;

public class PostTypeActivity extends BaseActy {

    private static final String TAG = "PostTypeActivity";
    private ItemAdapter mItemAdapter;
    private ListView lvPostType;

    private String mNamePostType; //记录选中的帖子类型
    private int mCurrentClickPosition; //记录当前点击的是哪一个条目
    private int mPostType;
    private PostTypeInfo mPostTypeInfo = new PostTypeInfo();
    private List<PostTypeItem.DataEntityPostType> mDataEntityPostTypeList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_type);
        initView();
        initData();


    }

    private void initView() {

        initTitleBar(R.id.al_tb_title, "帖子类型", null, "确定");
        lvPostType = (ListView) findViewById(R.id.lv_post_type);
        lvPostType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mCurrentClickPosition = position;

                for (int i = 0; i < mDataEntityPostTypeList.size(); i++) {

                    View viewItem = lvPostType.getChildAt(i);
                    ImageView ivArrowForward = (ImageView) viewItem.findViewById(R.id.iv_arrow_select);
                    TextView tvNamePostType = (TextView) viewItem.findViewById(R.id.tv_name_post_type);
                    if (i == mCurrentClickPosition) {

                        ivArrowForward.setVisibility(View.VISIBLE);
                        mNamePostType = tvNamePostType.getText().toString();
                        mDataEntityPostTypeList.get(position).setChecked(true);
                        mPostType = Integer.parseInt(mDataEntityPostTypeList.get(position).getId());
                    } else {
                        ivArrowForward.setVisibility(View.INVISIBLE);
                        mDataEntityPostTypeList.get(position).setChecked(false);
                    }
                }
            }
        });
    }

    private void initData() {

        mItemAdapter = new ItemAdapter(mContext);
        lvPostType.setAdapter(mItemAdapter);
        onPostTypeRequest();

        mCurrentClickPosition = getIntent().getExtras().getInt("currentClickPosition");
    }

    /**
     * 帖子类型请求
     */
    private void onPostTypeRequest() {

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mPostTypeInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mDataEntityPostTypeList = mPostTypeInfo.getDataEntityPostTypeList();
                mItemAdapter.addItems((List) mDataEntityPostTypeList);
                mItemAdapter.notifyDataSetChanged();


                if (-1 != mCurrentClickPosition){
                    mDataEntityPostTypeList.get(mCurrentClickPosition).setChecked(true);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_btn_right:

                Intent intent = new Intent(mContext,PostActivity_.class);
                intent.putExtra("namePostType",mNamePostType);
                intent.putExtra("currentClickPosition", mCurrentClickPosition);
                intent.putExtra("postType",mPostType);
                setResult(RESULT_OK, intent);
                finish();
                break;
        }
    }
}
