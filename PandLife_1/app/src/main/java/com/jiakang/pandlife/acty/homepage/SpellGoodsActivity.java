package com.jiakang.pandlife.acty.homepage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jiakang.pandlife.R;

import org.androidannotations.annotations.EActivity;

/**
 * 小区拼好货
 */
@EActivity(R.layout.activity_spell_goods)
public class SpellGoodsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
