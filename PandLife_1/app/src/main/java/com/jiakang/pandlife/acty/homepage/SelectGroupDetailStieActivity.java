package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.register.SelectSiteActivity_;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.CommunityItem;
import com.jiakang.pandlife.item.GroupDetailStationItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 * 团购详情中选择站点
 */
@EActivity(R.layout.activity_select_group_detail_stie)
public class SelectGroupDetailStieActivity extends BaseActy {
    protected static final String TAG = "SelectGroupDetailStieActivity";

    @ViewById(R.id.asgds_lv_stie)
    ListView siteLV;

    /** 详情中站点列表 */
    private List<GroupDetailStationItem> allItems = new ArrayList<GroupDetailStationItem>();

    private ItemAdapter mItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.asgds_tb_title, "选择站点");

        bindView();
    }

    private void bindView(){
        mItemAdapter = new ItemAdapter(mContext);
        siteLV.setAdapter(mItemAdapter);

        final Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        ArrayList list = bundle.getParcelableArrayList("list");
        if ((list != null) && (list.size() != 0)){
            GroupDetailStationItem groupDetailStationItem;
            for (int i=0; i < list.size(); i ++){
                groupDetailStationItem = (GroupDetailStationItem)list.get(i);
                allItems.add(groupDetailStationItem);
            }
            mItemAdapter.clear();
            mItemAdapter.addItems((List) allItems);
            mItemAdapter.notifyDataSetChanged();
            siteLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ListView listView = (ListView) parent;
                    GroupDetailStationItem groupDetailStationItem = (GroupDetailStationItem) listView.getItemAtPosition(position);
                    Intent intentSite = new Intent();
                    intentSite.putExtra("groupDetailStationItem", groupDetailStationItem);
                    setResult(RESULT_OK, intentSite);
                    finish();
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
