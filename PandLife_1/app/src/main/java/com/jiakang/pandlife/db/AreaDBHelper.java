package com.jiakang.pandlife.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.jiakang.pandlife.utils.JKLog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;


/** 省份、城市、地点数据库；主要从Assets中写入不变的数据库 */
public class AreaDBHelper {
	private final int BUFFER_SIZE = 8192;
	private static Context ctx;
//	private static String dir = "/src/main/assets";
	private static String dir = "/data/data/com.jiakang.pandlife/databases/";
	private static String dbName = "jiakang.db";
	/** 版本更新值 */
	private static int newVersion = 20130526;
	private static AreaDBHelper areaDBHelper;
	private SQLiteDatabase database;

	public SQLiteDatabase getDatabase() {
		return database;
	}

	/**
	 * 单例模式
	 * 
	 * @param context
	 * @return
	 */
	public synchronized static AreaDBHelper getInstance(Context context) {
		ctx = context;
		if (areaDBHelper == null)
			areaDBHelper = new AreaDBHelper(context);
		return areaDBHelper;
	}

	/**
	 * 私有构造方法
	 * 
	 * @param context
	 */
	private AreaDBHelper(Context context) {
		JKLog.e("AreaDBHelper", "AreaDBHelper中：AreaDBHelper方法-------------->");
		// 目录
		File dirFile = new File(dir);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		// db文件
		File dbFile = new File(dir + dbName);
		if (!dbFile.exists()) {// 数据库文件不存在
			// 写入数据库
			writeDBFile(dbFile);
		} else {
			database = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
			int oldVersion = database.getVersion();
			JKLog.e("AreaDBHelper", "AreaDBHelper方法中-------->：oldVersion：" + oldVersion + ";--newVersion：" + newVersion);
			if (newVersion > oldVersion) {
				// 强写、覆盖文件数据库
				writeDBFile(dbFile);
			}
		}

	}



	/**
	 * 写入数据库文件<br>
	 * 有则强写、覆盖文件数据库<br>
	 * 
	 * @param dbFile
	 */
	private void writeDBFile(final File dbFile) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JKLog.e("AreaDBHelper", "AreaDBHelper.writeDBFile写数据库文件-------------->");

					InputStream is = ctx.getAssets().open(dbName);
					FileOutputStream fos = new FileOutputStream(dbFile);

					byte[] buffer = new byte[BUFFER_SIZE];
					int count = 0;
					while ((count = is.read(buffer)) > 0) {
						fos.write(buffer, 0, count);
					}
					is.close();
					fos.flush();
					fos.getFD().sync();
					fos.close();
					database = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
					database.setVersion(newVersion);
				} catch (Exception e) {
					JKLog.e("AreaDBHelper", "数据库写入失败-------->：e为：" + e);
					// IData.isDatabase = false;
				}
			}
		}).start();

	}

	public void close() {
		database.close();
	}

}
