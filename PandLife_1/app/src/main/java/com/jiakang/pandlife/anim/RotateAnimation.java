package com.jiakang.pandlife.anim;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;

/**
 * @author：xuwangwang E-mail: 541765907@qq.com
 * @date：2012-3-28
 * @version 1.0
 */
public class RotateAnimation extends Animation {
	private int centerX;
	private int centerY;
	private float degrees;
	private int duration;
	private Interpolator interpolator = null;
	private Camera camera = new Camera();

	public RotateAnimation(int centerX, int centerY, float degrees, int duration) {
		// this(centerX, centerY, duration, new
		// DecelerateInterpolator());//开始快，后减慢；
		this(centerX, centerY, degrees, duration, new LinearInterpolator());// 一直匀速；
	}

	/**
	 * 默认是开始快，后减慢； setInterpolator(new LinearInterpolator());//一直匀速变化
	 * setInterpolator(new AccelerateInterpolator());//开始慢，后加速；
	 * setInterpolator(new AccelerateDecelerateInterpolator());//开始、结束慢，中间快；
	 * setInterpolator(new CycleInterpolator(1));//循环变化1次，速度按照正弦曲线改变；
	 * setInterpolator(new DecelerateInterpolator());//开始快，后减慢；
	 */
	public RotateAnimation(int centerX, int centerY, float degrees, int duration, Interpolator interpolator) {
		this.centerX = centerX;
		this.centerY = centerY;
		this.degrees = degrees;
		this.duration = duration;
		this.interpolator = interpolator;

	}

	/**
	 * Animation的抽象方法
	 */
	@Override
	public void initialize(int width, int height, int parentWidth, int parentHeight) {
		super.initialize(width, height, parentWidth, parentHeight);
		// 设置动画的持续时间
		setDuration(duration);
		// 设置动画结束后效果保留
		setFillAfter(true);
		// 控制变化速度；
		// setInterpolator(new LinearInterpolator());//一直匀速变化
		// setInterpolator(new AccelerateInterpolator());//开始慢，后加速；
		// setInterpolator(new AccelerateDecelerateInterpolator());//开始、结束慢，中间快；
		// setInterpolator(new CycleInterpolator(1));//循环变化1次，速度按照正弦曲线改变；
		// setInterpolator(new DecelerateInterpolator());//开始快，后减慢；
		setInterpolator(interpolator);
	}

	/**
	 * Animation的抽象方法
	 * 
	 * 该方法的interpolatedTime代表了抽象的动画持续时间，不管动画实际持续时间多长，
	 * interpolatedTime参数总是从0（动画开始时）～1（动画结束时） Transformation参数代表了对目标组件所做的变.
	 */
	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		// System.out.println("interpolatedTime为：" + interpolatedTime);

		camera.save();
		// 控制偏移： 根据interpolatedTime时间来控制X、Y、Z上的偏移
		// camera.translate(100.0f - 100.0f * interpolatedTime,
		// 150.0f * interpolatedTime - 150.0f, //因为y轴方向向上
		// 80.0f - 80.0f * interpolatedTime);//因为z轴方向向里
		// camera.translate(0,
		// 0, //因为y轴方向向上
		// 580.0f * interpolatedTime - 580.0f); //因为z轴方向向里
		// camera.translate(centerX*2 - centerX*2 * interpolatedTime,
		// 0, //因为y轴方向向上
		// 0); //因为z轴方向向里

		// 设置根据interpolatedTime时间在X柚上旋转不同角度
		// camera.rotateX((360 * interpolatedTime));
		// // 设置根据interpolatedTime时间在Y柚上旋转不同角度。
		camera.rotateY(degrees * (interpolatedTime));
		// // 设置根据interpolatedTime时间在z柚上旋转不同角度
		// camera.rotateZ((360 * interpolatedTime));

		// 获取Transformation参数的Matrix对象
		Matrix matrix = t.getMatrix();
		// 将came所做的变化应用到Matrix上
		camera.getMatrix(matrix);
		matrix.preTranslate(-centerX, -centerY);
		matrix.postTranslate(centerX, centerY);
		camera.restore();
	}
}