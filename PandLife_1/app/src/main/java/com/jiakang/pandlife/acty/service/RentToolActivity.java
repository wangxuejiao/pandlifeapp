package com.jiakang.pandlife.acty.service;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.RentTooInfo;
import com.jiakang.pandlife.utils.DateUtils;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.qqtheme.framework.picker.DatePicker;

@EActivity(R.layout.activity_rent_tool)
public class RentToolActivity extends BaseActy {


    @ViewById(R.id.iv_rent_tool)
    ImageView ivRentTool;
    @ViewById(R.id.tv_title_tool_rent)
    TextView tvTitle;
    @ViewById(R.id.tv_add_title_tool_rent)
    TextView tvAddTitle;
    @ViewById(R.id.tv_deposit_tool_rent)
    TextView tvDeposit;
    @ViewById(R.id.tv_available_count_rent)
    TextView tvAvailableCount;
    @ViewById(R.id.tv_count_rent_tool)
    TextView tvRentCount;
    @ViewById(R.id.iv_subtract_rent_tool)
    ImageView ivSubtract;
    @ViewById(R.id.iv_add_rent_tool)
    ImageView ivAdd;
    @ViewById(R.id.tv_back_time_tool_rent_detail)
    TextView tvBackTime;

    private String title;
    private String addTitle;
    private String deposit;
    private String availableCount;
    private String id;
    private String imgUrl;

    private int count = 1; //租借数量默认为0件

    //租借工具类
    private RentTooInfo mRentToolInfo = new RentTooInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rent_tool);
        intData();
    }


    @AfterViews
    protected void afterCreateView() {

        initTitleBar(R.id.al_tb_title, "租借工具", null, null);

        tvTitle.setText(title);
        tvAddTitle.setText(addTitle);
        tvDeposit.setText("押金：￥" + deposit + "元");
        tvAvailableCount.setText("共" + availableCount + "件可借");
        ImageLoaderUtil.displayImage(imgUrl, ivRentTool, R.mipmap.default_image);

    }

    private void intData() {

        Bundle bundle = getIntent().getExtras();
        imgUrl = bundle.getString("imgUrl");
        title = bundle.getString("title");
        addTitle = bundle.getString("addTitle");
        deposit = bundle.getString("deposit");
        availableCount = bundle.getString("availableCount");
        id = bundle.getString("id");

    }

    /**
     * 选择归还时间
     */
    public void onYearMonthDayPicker() {

        //获取当前的年月日
        Calendar cal = Calendar.getInstance();
        final int dayCurrent = cal.get(Calendar.DATE);
        final int monthCurrent = cal.get(Calendar.MONTH) + 1;
        final int yearCurrent = cal.get(Calendar.YEAR);

        final DatePicker picker = new DatePicker(this);
        picker.setRange(2016, 2025);
        picker.setSelectedItem(dayCurrent,monthCurrent,yearCurrent);
        picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
            @Override
            public void onDatePicked(String year, String month, String day) {

                String dateStr = year + "-" + month + "-" + day;
                if(!DateUtils.compareDate(dateStr)){
                    Toast.makeText(mContext,"归还时间不可以在今天之前！",Toast.LENGTH_SHORT).show();
                    tvBackTime.setText(yearCurrent + "-" + monthCurrent  + "-" + dayCurrent);
                    picker.setSelectedItem(dayCurrent,monthCurrent,yearCurrent);
                    return;
                }
                Date date = null;
                try {
                    tvBackTime.setText(dateStr);
                    date = new SimpleDateFormat("yyyy-MM-dd").parse(year + "-" + month + "-" + day);
                    mRentToolInfo.setBackTime((int) (date.getTime() / 1000));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        picker.show();
    }

    /**
     * 租借请求
     */
    private void toolRentRequest() {

        if(tvBackTime.getText().toString().equals("请选择时间")){
            Toast.makeText(mContext,"请选择时间",Toast.LENGTH_SHORT).show();
            return;
        }
        mRentToolInfo.setLid(Integer.parseInt(id));
        mRentToolInfo.setCount(count);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mRentToolInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                Toast.makeText(mContext, "租借成功", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Click(R.id.iv_subtract_rent_tool)
    protected void subTractClick(){

       if(count > 0){
           count = count - 1;
           tvRentCount.setText(count + "");
       }

    }
    @Click(R.id.iv_add_rent_tool)
    protected void addRentClick(){

        if(count <  Integer.parseInt(availableCount)){
            count = count + 1;
            tvRentCount.setText(count + "");
        }
    }

    @Click(R.id.rl_back_time_rent_detail)
    protected void backTimeClick() {
        onYearMonthDayPicker();
    }

    @Click(R.id.btn_rent_apply)
    protected void rentToolApply() {

        toolRentRequest();
    }


}
