package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.utils.DataCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的团购订单详细接口
 *
 * @author ww
 *
 */
public class MyTuanOrderDetailInfo extends BaseAbsInfo {

    private static final String TAG = "MyTuanOrderDetailInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;
    /** 订单编号id	Int(10) */
    private int id;

    private List<MyIndentItem> allItems = new ArrayList<MyIndentItem>();

    private MyIndentItem mMyIndentItem;

//    /** 因为服务器给的详情中没有id或者其他唯一标示符，所以将字段返回至activity界面保存 */

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=User&a=Mytuanorderdetial" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("id", id);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                mMyIndentItem = BaseInfo.gson.fromJson(jsonObject.get("data").toString(), MyIndentItem.class);


//                allItems.clear();
//                JSONArray taskJA = jsonObject.getJSONArray("data");
//                String itemStr;
//                MyIndentItem myIndentItem;
//                for(int i=0;i<taskJA.length();i++){
//                    itemStr = taskJA.getJSONObject(i).toString();
//                    myIndentItem = BaseInfo.gson.fromJson(itemStr, MyIndentItem.class);
//                    myIndentItem.setOrder_type("1");
//                    allItems.add(myIndentItem);
//                }

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<MyIndentItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<MyIndentItem> allItems) {
        this.allItems = allItems;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MyIndentItem getMyIndentItem() {
        return mMyIndentItem;
    }

    public void setMyIndentItem(MyIndentItem myIndentItem) {
        mMyIndentItem = myIndentItem;
    }
}
