package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.PublicKuaiDiInfo;
import com.jiakang.pandlife.item.SendBagItem;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.CustomListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.List;

/**
 * 查看物流
 */
@EActivity(R.layout.activity_check_logistics)
public class CheckLogisticsActivity extends BaseActy {
    protected static final String TAG = "CheckLogisticsActivity";

    @ViewById(R.id.acl_iv_logistics_logo)
    ImageView logisticsLogoIV;
    @ViewById(R.id.acl_tv_mailno)
    TextView mailnoTV;
    @ViewById(R.id.acl_tv_source)
    TextView sourceTV;
    @ViewById(R.id.acl_lv_logistics)
    CustomListView logisticsLV;

    private ItemAdapter mItemAdapter;

    private SendBagItem mSendBagItem;

    /** 获取邮寄快递物流信息接口 */
    private PublicKuaiDiInfo mPublicKuaiDiInfo = new PublicKuaiDiInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.acl_tb_title, "物流追踪");

        bindView();
        bindInfo();
    }

    private void bindView(){
        Intent intent = getIntent();
        mSendBagItem = (SendBagItem)intent.getExtras().get("sendBagItem");

        mailnoTV.setText("运单号：" + mSendBagItem.getMailno());
        sourceTV.setText("承运来源：" + mSendBagItem.getDelivery_name());

        mItemAdapter = new ItemAdapter(mContext);
        logisticsLV.setAdapter(mItemAdapter);
    }

    private void bindInfo(){
        mPublicKuaiDiInfo.setCode(mSendBagItem.getDelivery_id() + "");
        mPublicKuaiDiInfo.setPostid(mSendBagItem.getMailno());
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mPublicKuaiDiInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    mItemAdapter.addItems((List)mPublicKuaiDiInfo.getAllItems());
                    mItemAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    JKLog.e(TAG, "onRequestSuccess方法中-------->：");
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
