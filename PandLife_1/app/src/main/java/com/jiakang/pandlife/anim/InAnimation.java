package com.jiakang.pandlife.anim;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;

/**
 * @author��xuwangwang E-mail: 541765907@qq.com
 * @date��2012-3-28
 * @version 1.0
 */
public class InAnimation extends Animation {
	private int centerX;
	private int centerY;
	// ���嶯���ĳ����¼�
	private int duration;
	private Interpolator interpolator = null;
	private Camera camera = new Camera();

	public InAnimation(int centerX, int centerY, int duration) {
		// this(centerX, centerY, duration, new
		// DecelerateInterpolator());//��ʼ�죬�����
		this(centerX, centerY, duration, new LinearInterpolator());// һֱ���٣�
	}

	/**
	 * Ĭ���ǿ�ʼ�죬����� setInterpolator(new LinearInterpolator());//һֱ���ٱ仯
	 * setInterpolator(new AccelerateInterpolator());//��ʼ�����٣�
	 * setInterpolator(new AccelerateDecelerateInterpolator());//��ʼ���������м�죻
	 * setInterpolator(new CycleInterpolator(1));//ѭ���仯1�Σ��ٶȰ����������߸ı䣻
	 * setInterpolator(new DecelerateInterpolator());//��ʼ�죬�����
	 */
	public InAnimation(int centerX, int centerY, int duration, Interpolator interpolator) {
		this.centerX = centerX;
		this.centerY = centerY;
		this.duration = duration;
		this.interpolator = interpolator;

	}

	// Animation�ĳ��󷽷�
	public void initialize(int width, int height, int parentWidth, int parentHeight) {
		super.initialize(width, height, parentWidth, parentHeight);
		// ���ö����ĳ���ʱ��
		setDuration(duration);
		// ���ö��������Ч����
		setFillAfter(true);
		// ���Ʊ仯�ٶȣ�
		// setInterpolator(new LinearInterpolator());//һֱ���ٱ仯
		// setInterpolator(new AccelerateInterpolator());//��ʼ�����٣�
		// setInterpolator(new
		// AccelerateDecelerateInterpolator());//��ʼ���������м�죻
		// setInterpolator(new CycleInterpolator(1));//ѭ���仯1�Σ��ٶȰ����������߸ı䣻
		// setInterpolator(new DecelerateInterpolator());//��ʼ�죬�����
		setInterpolator(interpolator);
	}

	/*
	 * Animation�ĳ��󷽷�
	 * 
	 * �÷�����interpolatedTime����˳���Ķ�������ʱ�䣬���ܶ���ʵ�ʳ���ʱ��೤��
	 * interpolatedTime�������Ǵ�0��������ʼʱ����1����������ʱ��
	 * Transformation�������˶�Ŀ����������ı�.
	 */
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		// System.out.println("interpolatedTimeΪ��" + interpolatedTime);

		camera.save();
		// ����ƫ�ƣ� ���interpolatedTimeʱ��������X��Y��Z�ϵ�ƫ��
		// camera.translate(100.0f - 100.0f * interpolatedTime,
		// 150.0f * interpolatedTime - 150.0f, //��Ϊy�᷽������
		// 80.0f - 80.0f * interpolatedTime);//��Ϊz�᷽������
		// camera.translate(0,
		// 0, //��Ϊy�᷽������
		// 580.0f * interpolatedTime - 580.0f); //��Ϊz�᷽������
		camera.translate(centerX * 2 - centerX * 2 * interpolatedTime, 0, // ��Ϊy�᷽������
				0); // ��Ϊz�᷽������

		// ���ø��interpolatedTimeʱ����X������ת��ͬ�Ƕ�
		// camera.rotateX((360 * interpolatedTime));
		// // ���ø��interpolatedTimeʱ����Y������ת��ͬ�Ƕȡ�
		camera.rotateY(360 * (interpolatedTime));
		// // ���ø��interpolatedTimeʱ����z������ת��ͬ�Ƕ�
		// camera.rotateZ((360 * interpolatedTime));

		// ��ȡTransformation�����Matrix����
		Matrix matrix = t.getMatrix();
		// ��came�����ı仯Ӧ�õ�Matrix��
		camera.getMatrix(matrix);
		matrix.preTranslate(-centerX, -centerY);
		matrix.postTranslate(centerX, centerY);
		camera.restore();
	}
}