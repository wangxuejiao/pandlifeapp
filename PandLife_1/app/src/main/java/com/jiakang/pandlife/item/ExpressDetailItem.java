package com.jiakang.pandlife.item;

import java.util.ArrayList;
import java.util.List;

/**
 * 寄件详情中实体
 * Created by Administrator on 2016/1/14.
 */
public class ExpressDetailItem extends Item{

    /** 寄件记录id	Int(10) */
    private int id;
    /** 用户id	Int(10) */
    private int mid;
    /** 收件人地址id	Int(10) */
    private int sid;
    /** 寄件人地址id	Int(10) */
    private int jid;
    /** 快递id	Int(10) */
    private int delivery_id;
    /** 快递名称	Varchar(32) */
    private String delivery_name;
    /** 快递单号	Varchar(32) */
    private String mailno;
    /** 站点id	Int(10) */
    private int station;
    /** 付费类型（到付：d，寄付：f）	Varchar(32) */
    private String paytype;
    /** 物品类型	Varchar(32) */
    private String bag_kind;
    /** 数量	Int(10) */
    private int nums;
    /** 保价费用	Float(10，2) */
    private float supvalue;
    /** 备注信息	Varchar(255) */
    private String remarks;
    /** 是否正常（正常：y   取消：n）	Varchar(32) */
    private String is_enable;
    /** 是否处理（是：y，否：n）	Varchar(32) */
    private String bag_type;
    /** 寄件提交时间	Varchar(32) */
    private String createtime;
    /** 寄件处理时间	Varchar(32) */
    private String updatetime;
    /** 收件人信息结果集 */
//    private List<RecipientsInfoItem> sinfo = new ArrayList<RecipientsInfoItem>();
    private RecipientsInfoItem sinfo;
    /** 寄件人信息结果集 */
//    private List<SenderInfoItem> jinfo = new ArrayList<SenderInfoItem>();
    private SenderInfoItem jinfo;

    private int layout;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    /** 收件人信息结果实体 */
    public class RecipientsInfoItem{
        /** 姓名	Varchar(32) */
        private String name;
        /** 省份code	Int(10) */
        private int province_id;
        /** 省份名称	Varchar(32) */
        private String province;
        /** 城市code	Int(10) */
        private int city_id;
        /** 城市名称	Varchar(32) */
        private String city;
        /** 县/区code	Int(10) */
        private int zone_id;
        /** 县/区名称	Varchar(32) */
        private String zone;
        /** 详细地址	Varchar(32) */
        private String address;
        /** 手机号码	Varchar(11) */
        private String phone;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getProvince_id() {
            return province_id;
        }

        public void setProvince_id(int province_id) {
            this.province_id = province_id;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public int getCity_id() {
            return city_id;
        }

        public void setCity_id(int city_id) {
            this.city_id = city_id;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public int getZone_id() {
            return zone_id;
        }

        public void setZone_id(int zone_id) {
            this.zone_id = zone_id;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }
    /** 发件人信息结果实体 */
    public class SenderInfoItem{
        /** 姓名	Varchar(32) */
        private String name;
        /** 省份code	Int(10) */
        private int province_id;
        /** 省份名称	Varchar(32) */
        private String province;
        /** 城市code	Int(10) */
        private int city_id;
        /** 城市名称	Varchar(32) */
        private String city;
        /** 县/区code	Int(10) */
        private int zone_id;
        /** 县/区名称	Varchar(32) */
        private String zone;
        /** 详细地址	Varchar(32) */
        private String address;
        /** 手机号码	Varchar(11) */
        private String phone;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getProvince_id() {
            return province_id;
        }

        public void setProvince_id(int province_id) {
            this.province_id = province_id;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public int getCity_id() {
            return city_id;
        }

        public void setCity_id(int city_id) {
            this.city_id = city_id;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public int getZone_id() {
            return zone_id;
        }

        public void setZone_id(int zone_id) {
            this.zone_id = zone_id;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getJid() {
        return jid;
    }

    public void setJid(int jid) {
        this.jid = jid;
    }

    public int getDelivery_id() {
        return delivery_id;
    }

    public void setDelivery_id(int delivery_id) {
        this.delivery_id = delivery_id;
    }

    public String getDelivery_name() {
        return delivery_name;
    }

    public void setDelivery_name(String delivery_name) {
        this.delivery_name = delivery_name;
    }

    public String getMailno() {
        return mailno;
    }

    public void setMailno(String mailno) {
        this.mailno = mailno;
    }

    public int getStation() {
        return station;
    }

    public void setStation(int station) {
        this.station = station;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public String getBag_kind() {
        return bag_kind;
    }

    public void setBag_kind(String bag_kind) {
        this.bag_kind = bag_kind;
    }

    public int getNums() {
        return nums;
    }

    public void setNums(int nums) {
        this.nums = nums;
    }

    public float getSupvalue() {
        return supvalue;
    }

    public void setSupvalue(float supvalue) {
        this.supvalue = supvalue;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBag_type() {
        return bag_type;
    }

    public String getIs_enable() {
        return is_enable;
    }

    public void setIs_enable(String is_enable) {
        this.is_enable = is_enable;
    }

    public void setBag_type(String bag_type) {
        this.bag_type = bag_type;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

//    public List<RecipientsInfoItem> getSinfo() {
//        return sinfo;
//    }
//
//    public void setSinfo(List<RecipientsInfoItem> sinfo) {
//        this.sinfo = sinfo;
//    }
//
//    public List<SenderInfoItem> getJinfo() {
//        return jinfo;
//    }
//
//    public void setJinfo(List<SenderInfoItem> jinfo) {
//        this.jinfo = jinfo;
//    }

    public RecipientsInfoItem getSinfo() {
        return sinfo;
    }

    public void setSinfo(RecipientsInfoItem sinfo) {
        this.sinfo = sinfo;
    }

    public SenderInfoItem getJinfo() {
        return jinfo;
    }

    public void setJinfo(SenderInfoItem jinfo) {
        this.jinfo = jinfo;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }
}
