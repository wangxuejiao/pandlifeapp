package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.CommunityItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 28.根据经纬度获取附近小区信息
 *
 * @author ww
 *
 */
public class NearCommunityInfo extends BaseAbsInfo {

    private static final String TAG = "NearCommunityInfo";

    /** 经度	varchar(100) */
    private String longitude;
    /** 纬度	varchar(100) */
    private String latitude;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<CommunityItem> allItems = new ArrayList<CommunityItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Address&a=Nearcommunity" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("longitude", longitude);
            json.put("latitude", latitude);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                CommunityItem communityItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    communityItem = BaseInfo.gson.fromJson(itemStr, CommunityItem.class);
                    communityItem.setLayout(R.layout.item_select_single_left);
                    allItems.add(communityItem);
                }

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<CommunityItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<CommunityItem> allItems) {
        this.allItems = allItems;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
