package com.jiakang.pandlife.info;

/**
 * 1.团购商品列表接口
 * Created by Administrator on 2015/12/8.
 */


import android.text.TextUtils;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.GroupPurchaseItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 4.团购明日商品列表接口
 *
 * @author ww
 *
 */
public class TomorrowTuanInfo extends BaseAbsInfo {

    private static final String TAG = "TomorrowTuanInfo";

    /** 分页-当前页码 */
    private int pageIndex;
    /** 分页-每页数目 */
    private int pageSize;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<GroupPurchaseItem> allItems = new ArrayList<GroupPurchaseItem>();
    private String totalPage;
    private String pageIndexStr;


    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Tuan&a=Tomorrowtuan" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("pageSize", BaseInfo.pageSize);
            json.put("pageIndex",pageIndex);
            json.put("token", "ff0a6c74e7b4d08b659f9e2737a4ac5c");
//            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                GroupPurchaseItem groupPurchaseItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    groupPurchaseItem = BaseInfo.gson.fromJson(itemStr, GroupPurchaseItem.class);
                    allItems.add(groupPurchaseItem);
                }

                JSONObject taskJO = (JSONObject)jsonObject.get("page");
                pageIndexStr = taskJO.getString("pageIndex");
                if (!TextUtils.isEmpty(pageIndexStr))
                    pageIndex = Integer.parseInt(pageIndexStr);
                totalPage = taskJO.getString("totalPage");
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<GroupPurchaseItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<GroupPurchaseItem> allItems) {
        this.allItems = allItems;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public String getPageIndexStr() {
        return pageIndexStr;
    }

    public void setPageIndexStr(String pageIndexStr) {
        this.pageIndexStr = pageIndexStr;
    }
}
