package com.jiakang.pandlife.annotation.info;

import android.util.Log;

import com.jiakang.pandlife.annotation.utils.ClassUtils;
import com.jiakang.pandlife.annotation.utils.FieldUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;


public class TableInfo {

	private String className;
	private String tableName;
	private Id id;
	public final HashMap<String, TableField> propertyMap = new HashMap<String, TableField>();
	// 在对实体进行数据库操作的时候查询是否已经有表了，只需查询一遍，用此标示
	private boolean checkDatabese;
	private static final HashMap<String, TableInfo> tableInfoMap = new HashMap<String, TableInfo>();

	private TableInfo() {
	}

	public static TableInfo get(Class<?> clazz) {
		if (clazz == null)
			throw new RuntimeException("table info get error,because the clazz is null");

		TableInfo tableInfo = tableInfoMap.get(clazz.getName());
		if (tableInfo == null) {
			tableInfo = new TableInfo();

			tableInfo.setTableName(ClassUtils.getTableName(clazz));
			tableInfo.setClassName(clazz.getName());

			Field idField = ClassUtils.getPrimaryKeyField(clazz);
			if (idField != null) {
				Id id = new Id();
				id.setColumn(FieldUtils.getPropertyColumnByField(idField));
				id.setFieldName(idField.getName());
				id.setSet(FieldUtils.getFieldSetMethod(clazz, idField));
				id.setGet(FieldUtils.getFieldGetMethod(clazz, idField));
				id.setDataType(idField.getType());

				tableInfo.setId(id);
			} else {
				throw new RuntimeException("the class[" + clazz + "]'s idField is null , \n you can define _id,id property or use annotation @id to solution this exception");
			}

			List<TableField> pList = ClassUtils.getPropertyList(clazz);
			if (pList != null) {
				for (TableField p : pList) {
					if (p != null)
						tableInfo.propertyMap.put(p.getColumn(), p);
				}
			}

			tableInfoMap.put(clazz.getName(), tableInfo);
		}

		if (tableInfo == null)
			Log.e("", "the class[" + clazz + "]'s table is null");

		return tableInfo;
	}

	public static TableInfo get(String className) {
		try {
			return get(Class.forName(className));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Id getId() {
		return id;
	}

	public void setId(Id id) {
		this.id = id;
	}

	public boolean isCheckDatabese() {
		return checkDatabese;
	}

	public void setCheckDatabese(boolean checkDatabese) {
		this.checkDatabese = checkDatabese;
	}

}
