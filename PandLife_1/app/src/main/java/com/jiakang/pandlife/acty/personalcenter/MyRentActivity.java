package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.GetMyLeaseListInfo;
import com.jiakang.pandlife.item.MyLeaseItem;
import com.jiakang.pandlife.utils.CustomToast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的租借
 */
@EActivity(R.layout.activity_my_rent)
public class MyRentActivity extends BaseActy {
    protected static final String TAG = "MyRentActivity";

    @ViewById(R.id.amr_rgp_label)
    RadioGroup labelRGP;
    @ViewById(R.id.amr_rbn_no_dispose)
    RadioButton noDisposeRBN;
    @ViewById(R.id.amr_rbn_already_dispose)
    RadioButton alreadyDisposeRBN;
    @ViewById(R.id.amr_lv_my_rent)
    ListView myRentLV;
    @ViewById(R.id.amr_rl_no_data)
    RelativeLayout noDataRL;

    private ItemAdapter mItemAdapter;

    /** 获取我的租借接口 */
    private GetMyLeaseListInfo mGetMyLeaseListInfo = new GetMyLeaseListInfo();

    private List<MyLeaseItem> noDisposeItems = new ArrayList<MyLeaseItem>();
    private List<MyLeaseItem> alreadyDisposeItems = new ArrayList<MyLeaseItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.amr_tb_title, "我的租借");

        bindView();
        bindInfo();
    }

    private void bindView(){
        mItemAdapter = new ItemAdapter(mContext);
        myRentLV.setAdapter(mItemAdapter);

        labelRGP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.amr_rbn_no_dispose:
                        mItemAdapter.clear();
                        mItemAdapter.addItems((List) noDisposeItems);
                        mItemAdapter.notifyDataSetChanged();
                        noDisposeRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        alreadyDisposeRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        break;
                    case R.id.amr_rbn_already_dispose:
                        mItemAdapter.clear();
                        mItemAdapter.addItems((List) alreadyDisposeItems);
                        mItemAdapter.notifyDataSetChanged();
                        alreadyDisposeRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        noDisposeRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        break;
                }
            }
        });

        myRentLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listView = (ListView)parent;
                MyLeaseItem myLeaseItem = (MyLeaseItem)listView.getItemAtPosition(position);
                Intent intentRentDetail = new Intent(mContext, MyRentDetailActivity_.class);
                intentRentDetail.putExtra("myLeaseItem", myLeaseItem);
                startActivity(intentRentDetail);
            }
        });
    }

    private void bindInfo(){
        noDisposeItems.clear();
        alreadyDisposeItems.clear();
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mGetMyLeaseListInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try{
                    if (mGetMyLeaseListInfo.getAllItems().size() == 0){
                        CustomToast.showToast(mContext, "您暂无租借信息");
                        myRentLV.setVisibility(View.GONE);
                        noDataRL.setVisibility(View.VISIBLE);
                        return;
                    }
                    for (int i=0; i < mGetMyLeaseListInfo.getAllItems().size(); i ++){
                        String is_back = mGetMyLeaseListInfo.getAllItems().get(i).getIs_back();
                        if (("y").equals(is_back)){
                            alreadyDisposeItems.add(mGetMyLeaseListInfo.getAllItems().get(i));
                        }else {
                            noDisposeItems.add(mGetMyLeaseListInfo.getAllItems().get(i));
                        }
                    }
                    mItemAdapter.addItems((List)noDisposeItems);
                    mItemAdapter.notifyDataSetChanged();
                }catch (Exception e){

                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
