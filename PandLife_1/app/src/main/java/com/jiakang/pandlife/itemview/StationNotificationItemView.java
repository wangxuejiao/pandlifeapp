package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.StationNotificationItem;
import com.jiakang.pandlife.utils.DateUtils;
import com.jiakang.pandlife.utils.Util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by play on 2016/1/4.
 */
public class StationNotificationItemView extends AbsLinearLayout{
    private String TAG = "StationNotificationItemView";
	private TextView tvTitle;
    private TextView tvDate;
	private TextView tvContent;

	public StationNotificationItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public StationNotificationItemView(Context context) {
		super(context);
	}

	@Override
	public void findViewsByIds() {
		tvTitle = (TextView) findViewById(R.id.tv_title_station_notification);
		tvDate = (TextView) findViewById(R.id.tv_date_station_notification);
		tvContent = (TextView) findViewById(R.id.tv_content_station_notification);

	}

	@Override
	public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
		super.setObject(item, position, onViewClickListener);

		StationNotificationItem.DataEntity dataEntity = (StationNotificationItem.DataEntity) item;
		tvTitle.setText(dataEntity.getTitle());
		Long tempDate = Long.parseLong(dataEntity.getCreatetime()) * 1000 ;
		tvDate.setText(DateUtils.getDateStringyyyyMMddHH(tempDate));
		tvContent.setText(dataEntity.getContent());

    }

}
