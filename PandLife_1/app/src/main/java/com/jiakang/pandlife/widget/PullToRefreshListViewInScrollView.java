package com.jiakang.pandlife.widget;

import android.content.Context;
import android.util.AttributeSet;


/**
 * @author CQ
 * @date：2014-4-18 下午4:21:06
 * @version 1.0
 */
public class PullToRefreshListViewInScrollView extends PullToRefreshListView {

	public PullToRefreshListViewInScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public PullToRefreshListViewInScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);

		super.onMeasure(widthMeasureSpec, expandSpec);
	}

}
