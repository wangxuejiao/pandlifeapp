package com.jiakang.pandlife.info;

import com.google.gson.Gson;
import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.NearbyStationItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by play on 2016/1/12.
 */
public class NearbyStationInfo extends BaseAbsInfo {

    private String longitude;
    private String latitude;

    private NearbyStationItem nearbyStationItem;
    private List<NearbyStationItem.DataEntity> dataEntityList = new ArrayList<>();

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public NearbyStationItem getNearbyStationItem() {
        return nearbyStationItem;
    }

    public void setNearbyStationItem(NearbyStationItem nearbyStationItem) {
        nearbyStationItem = nearbyStationItem;
    }

    public List<NearbyStationItem.DataEntity> getDataEntitieList() {
        return dataEntityList;
    }

    public void setDataEntitieList(List<NearbyStationItem.DataEntity> dataEntitieList) {
        dataEntityList = dataEntitieList;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=ExpressUsers&a=Nearstation&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("longitude",longitude);
            json.put("latitude",latitude);
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        nearbyStationItem = BaseInfo.gson.fromJson(jsonObject.toString(), NearbyStationItem.class);
        dataEntityList.clear();
        dataEntityList = nearbyStationItem.getData();
        for (int i = 0; i < dataEntityList.size(); i++) {

            dataEntityList.get(i).setLayout(R.layout.item_nearby_station);
        }
    }
}
