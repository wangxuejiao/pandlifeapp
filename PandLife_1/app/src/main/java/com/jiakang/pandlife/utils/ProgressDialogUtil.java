package com.jiakang.pandlife.utils;

import android.content.Context;

import com.jiakang.pandlife.widget.ProgressDialog;


public class ProgressDialogUtil {
	// private static CustomProgressDialog progressDialog;
	private Context context;
	private static ProgressDialog progressDialog;

	public ProgressDialogUtil(Context context) {
		this.context = context;
	}

	public static void startProgressBar(Context mContext, String message) {
		try {
			if (null != progressDialog && progressDialog.isShowing()) {
				progressDialog.setMsg(message);
			} else {
				progressDialog = new ProgressDialog(mContext, message);
				progressDialog.show();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void stopProgressBar() {
		try {
			if (null != progressDialog && progressDialog.isShowing()) {
				progressDialog.hide();
				progressDialog.dismiss();
				progressDialog.cancel();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean isShowing() {
		if (null == progressDialog) {
			return false;
		} else if (progressDialog.isShowing()) {
			return true;
		} else {
			return false;
		}
	}

	public static void setCancelable(boolean bo) {
		if (bo == true) {
			progressDialog.setCancelable(true);
		} else {
			progressDialog.setCancelable(false);
		}
	}
}
