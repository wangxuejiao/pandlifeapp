package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.AreaInfo;
import com.jiakang.pandlife.info.CommunityInfo;
import com.jiakang.pandlife.info.SetCommunityInfo;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.CityItem;
import com.jiakang.pandlife.item.CommunityItem;
import com.jiakang.pandlife.item.ProvinceItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * 选择小区
 */
@EActivity(R.layout.activity_select_community)
public class SelectCommunityActivity extends BaseActy {
    protected static final String TAG = "SelectCommunityActivity";

    @ViewById(R.id.asco_lv_community)
    ListView communityLV;

    private ItemAdapter mItemAdapter;
    /**
     * 选择小区接口
     */
    private CommunityInfo mCommunityInfo = new CommunityInfo();

    private ProvinceItem mProvinceItem;
    private CityItem mCityItem;
    private AreaItem mAreaItem;

    private List<CommunityItem> allItems = new ArrayList<CommunityItem>();

    private String siteOrCommunity = "community_and_site";

    /** 绑定小区接口 */
    private SetCommunityInfo mSetCommunityInfo = new SetCommunityInfo();

    private CommunityItem mCommunityItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar() {
        initTitleBar(R.id.asco_tb_title, "选择小区");

        Intent intent = getIntent();
        siteOrCommunity = intent.getExtras().getString(SelectLocationOrManualActivity.SITE_OR_COMMUNITY);

        bindView();
    }

    private void bindView() {
        mItemAdapter = new ItemAdapter(mContext);
        communityLV.setAdapter(mItemAdapter);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        ArrayList list = bundle.getParcelableArrayList("list");
        if ((list != null) && (list.size() != 0)){
            CommunityItem communityItem;
            for (int i=0; i < list.size(); i ++){
                communityItem = (CommunityItem)list.get(i);
                allItems.add(communityItem);
            }
            mItemAdapter.clear();
            mItemAdapter.addItems((List) allItems);
            mItemAdapter.notifyDataSetChanged();
            communityLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ListView listView = (ListView) parent;
                    mCommunityItem = (CommunityItem) listView.getItemAtPosition(position);
                    if (("site").equals(siteOrCommunity)) {
                        skipSite();
                    }else {
                        String cid = mCommunityItem.getCid();
                        //绑定小区的方法
                        bindCommunity(cid);
                    }
                }
            });
        }else {
            mProvinceItem = (ProvinceItem) intent.getExtras().get("provinceItem");
            mCityItem = (CityItem) intent.getExtras().get("cityItem");
            mAreaItem = (AreaItem) intent.getExtras().get("areaItem");
            bindInfo();
        }
    }

    private void bindInfo() {
        ApiManager apiManager = ApiManager.getInstance();
        mCommunityInfo.setProvince_id(mProvinceItem.getCode());
        mCommunityInfo.setCity_id(mCityItem.getCode());
        mCommunityInfo.setZone_id(mAreaItem.getCode());
        apiManager.request(mCommunityInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        if (mCommunityInfo.getAllItems().size() == 0) {
                            CustomToast.showToast(mContext, "未获取到小区信息，请重新获取");
                            return;
                        }
                        mItemAdapter.addItems((List) mCommunityInfo.getAllItems());
                        mItemAdapter.notifyDataSetChanged();

                        communityLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                ListView listView = (ListView) parent;
                                mCommunityItem = (CommunityItem) listView.getItemAtPosition(position);
                                if (("site").equals(siteOrCommunity)) {
                                    skipSite();
                                }else {
                                    String cid = mCommunityItem.getCid();
                                    //绑定小区的方法
                                    bindCommunity(cid);
                                }
                            }
                        });

                    } else {
                        JKLog.i(TAG, "失败");
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    /**
     * 绑定小区的方法
     */
    private void bindCommunity(String cid){
        //绑定小区的方法，后返回

        if (TextUtils.isEmpty(cid)){
            CustomToast.showToast(mContext, "绑定失败，请重试");
            return;
        }
        mSetCommunityInfo.setCid(Integer.parseInt(cid));
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mSetCommunityInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    if (("community").equals(siteOrCommunity)){
                        CustomToast.showToast(mContext, "绑定成功");
                        setResult(RESULT_OK);
                        finish();
                    }else {
                        skipSite();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestFail(int result, JSONObject jsonObject) {
                super.onRequestFail(result, jsonObject);
                if(getMyApp().getUserItem().getCid() != 0){
                    skipSite();
                }else{
                    CustomToast.showToast(mContext, "绑定失败，请重试");
                }
            }
        });
    }

    /**
     * 跳转站点的方法
     */
    private void skipSite(){
        Intent intentSite = new Intent(mContext, SelectSiteActivity_.class);
        intentSite.putExtra("communityItem", mCommunityItem);
        startActivityForResult(intentSite, Constant.StaticCode.REQUSET_BIND_SITE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUSET_BIND_SITE) && (resultCode == RESULT_OK)){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
