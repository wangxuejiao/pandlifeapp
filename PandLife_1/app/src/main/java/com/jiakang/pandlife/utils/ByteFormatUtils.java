package com.jiakang.pandlife.utils;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class<code>ByteChangeUtils.java</code>
 * 
 * @author WeiQinyu
 * @version 2011-10-14 下午02:45:41
 */

public class ByteFormatUtils {
	// 二进制转换成十六进制的字符串
	public static String byte2hex(byte[] desKey) {

		int i;
		StringBuffer buf = new StringBuffer("");
		for (int offset = 0; offset < desKey.length; offset++) {
			i = desKey[offset];
			if (i < 0)
				i += 256;
			if (i < 16)
				buf.append("0");
			buf.append(Integer.toHexString(i));
		}
		return buf.toString();
	}

	// 二进制转成十六进制
	public static String byte2hex1(byte[] b) throws NoSuchAlgorithmException {

		String hs = "";
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = Integer.toHexString(b[n] & 0xFF);
			if (stmp.length() == 1)
				hs = hs + "0" + stmp;
			else
				hs = hs + stmp;
		}
		return hs;
	}

	// 二进制转成十六进制
	public static String byte2hex2(byte[] b) throws NoSuchAlgorithmException {

		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < b.length; i++) {
			int x = b[i] & 0xFF, h = x >>> 4, l = x & 0x0F;
			buf.append((char) (h + ((h < 10) ? '0' : 'a' - 10)));
			buf.append((char) (l + ((l < 10) ? '0' : 'a' - 10)));
		}
		return buf.toString();
	}

	/**
	 * Convert byte[] to hex
	 * string.这里我们可以将byte转换成int，然后利用Integer.toHexString(int)来转换成16进制字符串。
	 * 
	 * @param src
	 *            byte[] data
	 * 
	 * @return hex string
	 */
	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("");
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString();
	}

	/**
	 * 将十六进制字符串转换为byte数组
	 */
	public static byte[] hexStringToBytes(String hexString) {
		if (hexString == null || hexString.equals("")) {
			return null;
		}
		hexString = hexString.toUpperCase();
		int length = hexString.length() / 2;
		char[] hexChars = hexString.toCharArray();
		byte[] d = new byte[length];
		for (int i = 0; i < length; i++) {
			int pos = i * 2;
			d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
		}
		return d;
	}

	/**
	 * 将char型转换为byte
	 */
	private static byte charToByte(char c) {
		return (byte) "0123456789ABCDEF".indexOf(c);
	}

	/**
	 * 将long型数据转换成dword的byte[]数组
	 * 
	 * @param value
	 * @return
	 */
	public static byte[] longToDword(long value) {
		byte[] data = new byte[4];

		for (int i = 0; i < data.length; i++) {
			data[i] = (byte) (value >> (8 * i));
		}
		return data;
	}

	/**
	 * 将dword的byte[]数组转换成long
	 * 
	 * @param buf
	 * @param index
	 * @return
	 */
	public static long dwordToLong(byte buf[], int index) {

		int b0 = (0x000000FF & ((int) buf[index]));
		int b1 = (0x000000FF & ((int) buf[index + 1]));
		int b2 = (0x000000FF & ((int) buf[index + 2]));
		int b3 = (0x000000FF & ((int) buf[index + 3]));

		long unsignedLong = ((long) (b0 | b1 << 8 | b2 << 16 | b3 << 24)) & 0xFFFFFFFFL;
		return unsignedLong;
	}

	// 字节数组截取
	public static byte[] interceptByte(byte[] data, int index, int len) {

		byte[] rstData = null;
		if (index >= 0 && len > 0) {
			rstData = new byte[len];
			System.arraycopy(data, index, rstData, 0, len);
		}

		return rstData;
	}

	// 字节数组拼接
	public static byte[] appendByte(byte[] data, byte[] data1) {

		int len = data != null ? data.length : 0;
		int len1 = data1 != null ? data1.length : 0;
		byte[] rstData = null;
		try {
			rstData = new byte[len + len1];
			if (len > 0)
				System.arraycopy(data, 0, rstData, 0, len);
			if (len1 > 0)
				System.arraycopy(data1, 0, rstData, len, len1);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rstData;
	}

	// 字节顺序变换
	public static byte[] orderChange(byte[] data) {

		int len = data != null ? data.length : 0;
		byte[] rstData = null;
		try {
			rstData = new byte[len];
			for (int i = 0; i < len; i++) {
				rstData[i] = data[len - 1 - i];
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rstData;
	}

	public static List<byte[]> interceptByteToList(byte[] content) {
		// 文件传送
		List<byte[]> list = new ArrayList<byte[]>();
		int part = 4096;
		if (content != null) {
			int len = content.length;
			/*
			 * 文件内容大于4096个字节 那么将附件内容拆分成多个包，分批传送到客户端
			 */
			if (len > part) {
				int y = len % part;
				int size = (len - y) / part;
				byte[] send;
				for (int k = 0; k < size; k++) {
					// 传输文件内容
					send = ByteFormatUtils.interceptByte(content, k * part, part);
					list.add(send);
				}

				if (y > 0) {
					send = ByteFormatUtils.interceptByte(content, size * part, y);
					list.add(send);

				}
			}
			// 文件内容小于等于4096个字节
			else {
				list.add(content);
			}
		}

		return list;
	}

}
