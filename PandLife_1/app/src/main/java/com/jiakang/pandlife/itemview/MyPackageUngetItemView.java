package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.Item;

/**
 * Created by play on 2016/1/6.
 */
public class MyPackageUngetItemView extends AbsLinearLayout {

    private TextView waybillNumberTV;
    private TextView expressCompanyTV;
    private TextView packageNumberTV;
    private TextView arriveDateTV;

    public MyPackageUngetItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyPackageUngetItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        waybillNumberTV = (TextView) findViewById(R.id.impu_tv_waybill_number_content);
        expressCompanyTV = (TextView) findViewById(R.id.impu_tv_express_company_content);
        packageNumberTV = (TextView) findViewById(R.id.impu_tv_package_number_content);
        arriveDateTV = (TextView) findViewById(R.id.impu_tv_arrive_date_content);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        ExpressListItem expressListItem = (ExpressListItem) item;
        waybillNumberTV.setText(expressListItem.getMailno());
        expressCompanyTV.setText(expressListItem.getCompanyname());
        packageNumberTV.setText(expressListItem.getNumber());
        arriveDateTV.setText(expressListItem.getGmtcreate());

    }
}
