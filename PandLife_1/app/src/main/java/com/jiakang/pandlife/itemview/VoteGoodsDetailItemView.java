package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;

/**
 * Created by play on 2016/1/13.
 */
public class VoteGoodsDetailItemView extends AbsLinearLayout {

    private RadioButton rbVote;
    private ProgressBar pbVote;
    private TextView tvCount;

    public VoteGoodsDetailItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void findViewsByIds() {

        rbVote = (RadioButton) findViewById(R.id.rb_vote);
        pbVote = (ProgressBar) findViewById(R.id.pb_vote);
        tvCount = (TextView) findViewById(R.id.tv_count_vote);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);
    }
}
