package com.jiakang.pandlife;


import com.google.gson.Gson;

/**
 * @author ww
 */
public class BaseInfo {

	static {
		long preTime = System.currentTimeMillis();
//		JKLog.e("BaseInfo", "static方法begin-------->");
//		JKLog.e("BaseInfo", "static方法over-------->time:" + (System.currentTimeMillis() - preTime));
	}

	private static String TAG = "BaseInfo";

	public static String data = "data";

	public static int DATE_MAX = 1000;

	/** 是否有网络 */
	public static boolean hasNet = false;
	/** gson对象 */
	public static Gson gson = new Gson();

	public static String newVersionID = "20140909";

	public static String token;

	public static int pageSize = 20;

}
