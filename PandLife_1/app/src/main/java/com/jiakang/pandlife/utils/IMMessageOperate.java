package com.jiakang.pandlife.utils;

import com.jiakang.pandlife.BaseInfo;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Random;


/**
 * Class<code>MessageOperate.java</code> ���ܽ��ܷ�װ��
 * 
 * @author WeiQinyu
 * @version 2012-05-03 ����03:04:13
 */
public class IMMessageOperate {

	public final static String MATCHERROR = "MATCHERROR";

	/**
	 * ��װ��Ϣ��
	 * 
	 * @param map
	 * @return
	 */
	public byte[] encode(int flag, Map<String, Object> map) {
		try {
			// ����ת����json�����ַ�
			String str = BaseInfo.gson.toJson(map);
			// ������ݣ���UTF-8�ķ�ʽ��ȡ�ֽ�����
			byte[] text = str.getBytes("UTF-8");

			// ��ɶԳƼ��ܵ���Կ
			byte[] secertKey = createKey();

			// ��ȡ���ĵ�MD5ֵ
			String mdvStr = MD5.getMD5(text);

			// �����Ľ��м��ܴ���
			text = DesPlus.encrypt(text, secertKey);

			// ƴ���ֽ����飬8λ��Կ+32λMD5ֵ+����
			byte[] rst = ByteFormatUtils.appendByte(secertKey, mdvStr.getBytes("UTF-8"));
			rst = ByteFormatUtils.appendByte(rst, text);

			// ����Ϣ����ѹ��
			rst = ZLibUtils.compress(rst);

			// ��ȡ��Ϣѹ������ֽ����鳤��
			int zLen = rst.length;
			byte[] zLenB = appendZero(zLen);

			byte[] flagB = Integer.toString(flag).getBytes("UTF-8");

			// ƴ��3λͨ�ű�ʶ��
			rst = ByteFormatUtils.appendByte(flagB, rst);
			// ƴ��8λ��ݳ���
			rst = ByteFormatUtils.appendByte(zLenB, rst);
			return rst;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * ������Ϣ��
	 * 
	 * @param code
	 *            �ͻ��˴�������ѹ�������Ϣ��
	 * @return
	 */
	public String decode(byte[] code) {
		byte[] rst;
		try {
			// ����Ϣ���н�ѹ��
			rst = ZLibUtils.decompress(code);
			// ��ȡ8λ�Գ���Կ
			byte[] secertKey = ByteFormatUtils.interceptByte(rst, 0, 8);
			// ��ȡ����
			rst = ByteFormatUtils.interceptByte(rst, 40, rst.length - 40);
			// ����
			rst = DesPlus.decrypt(rst, secertKey);
			return new String(rst, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// �����Գ���Կ
	private byte[] createKey() throws Exception {

		String keyStr = new String();
		Random rd = new Random();
		for (int i = 0; i < 8; i++) {
			keyStr += rd.nextInt(9);
		}
		return keyStr.getBytes();
	}

	/**
	 * ����ݳ��Ȳ���8λ����ಹ��
	 * 
	 * @param zLen
	 * @return
	 */
	private byte[] appendZero(int zLen) {
		byte[] zLenB = null;
		String zLenStr = Integer.toString(zLen);
		if (zLenStr != null && zLenStr.length() < 8) {
			int i = zLenStr.length();
			StringBuffer sb = new StringBuffer();
			for (; i < 8; i++) {
				sb.append("0");
			}
			zLenStr = sb.append(zLenStr).toString();
		}

		try {
			zLenB = zLenStr.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return zLenB;
	}

}
