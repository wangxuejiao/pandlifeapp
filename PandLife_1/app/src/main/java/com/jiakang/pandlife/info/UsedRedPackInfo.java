package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.RedPackageItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单可用红包接口
 *
 * @author ww
 *
 */
public class UsedRedPackInfo extends BaseAbsInfo {

    private static final String TAG = "UsedRedPackInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;
    /** 商品id	Int(10) */
    private int gid;
    /** 订单总金额	Int(10) */
    private int money;
    /** 商品类型：团购产品为：t */
    private String type;

    private List<RedPackageItem> allItems = new ArrayList<RedPackageItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Redpack&a=Usedredpack" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", "ff0a6c74e7b4d08b659f9e2737a4ac5c");
            json.put("gid", gid);
            json.put("money", money);
            json.put("type", type);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                RedPackageItem redPackageItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    redPackageItem = BaseInfo.gson.fromJson(itemStr, RedPackageItem.class);
                    allItems.add(redPackageItem);
                }

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<RedPackageItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<RedPackageItem> allItems) {
        this.allItems = allItems;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
