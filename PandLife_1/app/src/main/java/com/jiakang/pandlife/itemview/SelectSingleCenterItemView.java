package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.CityItem;
import com.jiakang.pandlife.item.CommunityItem;
import com.jiakang.pandlife.item.DeliveryItem;
import com.jiakang.pandlife.item.GoodsKindItem;
import com.jiakang.pandlife.item.GroupDetailStationItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.ProvinceItem;

/**
 * Created by play on 2016/1/6.
 */
public class SelectSingleCenterItemView extends AbsRelativeLayout {

    private TextView centerTV;
    private ImageView arrowIV;

    public SelectSingleCenterItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectSingleCenterItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        centerTV = (TextView) findViewById(R.id.issc_tv_center);
        arrowIV = (ImageView) findViewById(R.id.issc_iv_arrow);

    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        if (item instanceof GoodsKindItem){
            GoodsKindItem goodsKindItem = (GoodsKindItem) item;
            centerTV.setText(goodsKindItem.getCname());
            if (goodsKindItem.isCheck()){
                centerTV.setBackgroundColor(getResources().getColor(R.color.white));
                arrowIV.setVisibility(View.VISIBLE);
            }else{
                centerTV.setBackgroundColor(getResources().getColor(R.color.gray_bg));
                arrowIV.setVisibility(View.GONE);
            }
        }

    }
}
