package com.jiakang.pandlife.acty;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.db.DataCacheDBHelper;
import com.jiakang.pandlife.db.DataCacheDBManage;
import com.jiakang.pandlife.item.JpushNotificationItem;

import java.util.ArrayList;
import java.util.List;

public class JpushNoticeActivity extends BaseActy {

    private ListView lvJpushNotification;
    private List<JpushNotificationItem> mJpushNotificationItemList = new ArrayList<>();
    private ItemAdapter mItemAdapter;
    DataCacheDBManage mDataCacheDBManage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jpush_notice);

    }

    @Override
    protected void onStart() {
        super.onStart();
        initView();
        initData();
    }

    private void initView() {

        initTitleBar(R.id.al_tb_title, "消息通知",null,"清空");
        lvJpushNotification = (ListView) findViewById(R.id.lv_jpush_notification);
        mItemAdapter = new ItemAdapter(mContext);
        lvJpushNotification.setAdapter(mItemAdapter);
        lvJpushNotification.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                JpushNotificationItem jpushNotificationItem = (JpushNotificationItem) lvJpushNotification.getItemAtPosition(position);

                mDataCacheDBManage.updateJpushNotificationItem(jpushNotificationItem.getId());
                String title = jpushNotificationItem.getTitle();
                String message = jpushNotificationItem.getMessage();
                Long time = jpushNotificationItem.getTime();
                Intent intent = new Intent(JpushNoticeActivity.this,JpushNotificationDetailActivity_.class);
                intent.putExtra("title",title);
                intent.putExtra("message",message);
                intent.putExtra("time",time);
                startActivity(intent);
            }
        });
    }

    private void initData() {

        mDataCacheDBManage = getMyApp().getCacheDataDBManage();
        mJpushNotificationItemList = mDataCacheDBManage.getJpushNotificationList();
        mItemAdapter.addItems((List) mJpushNotificationItemList);
        mItemAdapter.notifyDataSetChanged();
    }

    public static void startJpushNoticeActivity(Context context){

        Intent intent = new Intent(context,JpushNoticeActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){

            case R.id.it_btn_right:
                mDataCacheDBManage.clear(DataCacheDBHelper.TAB_JPUSH_NOTIFICATION_CACHE);
                mJpushNotificationItemList.clear();
                mItemAdapter.addItems((List)mJpushNotificationItemList);
                mItemAdapter.notifyDataSetChanged();
                break;
        }
    }
}
