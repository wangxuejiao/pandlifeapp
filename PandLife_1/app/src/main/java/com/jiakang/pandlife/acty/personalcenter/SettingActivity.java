package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.JpushNoticeActivity;
import com.jiakang.pandlife.acty.LoginActivity_;
import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.utils.FileCache;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import cn.jpush.android.api.JPushInterface;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

/**
 * 设置
 * Created by Administrator on 2016/1/14.
 */
@EActivity(R.layout.activity_setting)
public class SettingActivity extends BaseActy{
    protected static final String TAG = "SettingActivity";

    public static boolean isForeground = false;

    @ViewById(R.id.as_cb_message_notify)
    CheckBox messageNotifyCB;
    @ViewById(R.id.as_tv_about)
    TextView aboutTV;
    @ViewById(R.id.as_tv_clause_privacy)
    TextView clausePrivacyTV;
    @ViewById(R.id.as_tv_share)
    TextView shareTV;
    @ViewById(R.id.as_btn_exit_login)
    Button exitLoginBN;

    private SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isForeground = true;
    }

    @AfterViews
    protected void initVar() {
        initTitleBar(R.id.as_tb_title, "设置");

        bindView();
    }

    private void bindView(){

        if(myAccount.getBoolean(Constant.Spf.MESSAGE_NOTIFY,true)){

            messageNotifyCB.setChecked(true);
        }else {
            messageNotifyCB.setChecked(false);
        }

        aboutTV.setOnClickListener(this);
        clausePrivacyTV.setOnClickListener(this);
        shareTV.setOnClickListener(this);
        exitLoginBN.setOnClickListener(this);

        messageNotifyCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //将消息通知状态值缓存
                final SharedPreferences.Editor editor = myAccount.edit();
                if (isChecked){
                    editor.putBoolean(Constant.Spf.MESSAGE_NOTIFY, true);
                    JPushInterface.resumePush(mContext);
                }else if(!isChecked){
                    editor.putBoolean(Constant.Spf.MESSAGE_NOTIFY, false);
                    JPushInterface.stopPush(mContext);
                }
                editor.commit();
            }
        });
    }



    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.as_tv_about:
                Intent intentAbout = new Intent(mContext, AboutUsActivity_.class);
                startActivity(intentAbout);
                break;
            case R.id.as_tv_clause_privacy:
                Intent intentClausePrivacy = new Intent(mContext, ClausePrivacyActivity_.class);
                startActivity(intentClausePrivacy);
                break;
            case R.id.as_tv_share:

                showShare();

                /*UMImage image = new UMImage(SettingActivity.this,
                        BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));

                new ShareAction(this).setDisplayList(SHARE_MEDIA.SINA, SHARE_MEDIA.QQ, SHARE_MEDIA.QZONE, SHARE_MEDIA.WEIXIN, SHARE_MEDIA.WEIXIN_CIRCLE)
                        .withText("熊猫快收")
                        .withTargetUrl("http://www.baidu.com")
                        .withMedia(image)
                        .setListenerList(umShareListener, umShareListener)
                        .open();*/
                break;
            /*我刚下的熊猫快收App还挺有意思，你看看。
快递信息、小区拼好货、小区二手信息、家庭维修工具通通有哦。
*/
            case R.id.as_btn_exit_login:
//                BaseActy.isOrNotLogin = false;


                //将用户登录token缓存
                final SharedPreferences.Editor editor = myAccount.edit();
                editor.putString(Constant.Spf.TOKEN, "");
                editor.commit();
                BaseInfo.token = "";//情况访问令牌
                // 清空用户实体
//                UserItem mUserItem;
                PandLifeApp.getInstance().setUserItem(null);
                Intent intentLogin = new Intent(mContext, LoginActivity_.class);
                startActivity(intentLogin);
                setResult(RESULT_OK);
                finish();
//                BaseInfo.userId = "";//情况userId
//                mainControlActy.stopService();//关闭即时通讯服务
//                XmppConnectionManager.getInstance().disconnect();//销毁XMPP链接
//                mainControlActy.jumpUnLoginFragment();//跳转到未登录下的我的中心界面
                break;
        }
    }

    private void showShare() {
        ShareSDK.initSDK(this);
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();
// 分享时Notification的图标和文字  2.5.9以后的版本不调用此方法
        //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
        oks.setTitle("熊猫快收");
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        oks.setTitleUrl("http://www.baidu.com");
        // text是分享文本，所有平台都需要这个字段
        oks.setText("我刚下的熊猫快收App还挺有意思，你看看。快递信息、小区拼好货、小区二手信息、家庭维修工具通通有哦。");
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //  oks.setImageUrl("http://7sby7r.com1.z0.glb.clouddn.com/CYSJ_02.jpg");//网络图片rul
        FileCache fileCache = FileCache.getInstance();
        fileCache.writeAssetsImageToSdcard(mContext);
        oks.setImagePath("/data/data/" + mContext.getPackageName() + "/share_image.png");
        // url仅在微信（包括好友和朋友圈）中使用
        //    oks.setUrl("http://sharesdk.cn");
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        //    oks.setComment("我是测试评论文本");
        // site是分享此内容的网站名称，仅在QQ空间使用
        //   oks.setSite(getString(R.string.app_name));
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        //  oks.setSiteUrl("http://sharesdk.cn");
// 启动分享GUI
        oks.show(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onStop() {
        super.onStop();

        isForeground = false;
    }
}
