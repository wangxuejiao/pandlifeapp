package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

/**
 * 我的地址实体
 * Created by Administrator on 2016/1/11.
 */
public class AddressItem extends Item{

    /** 收件人/寄件人id	Int(10) */
    private int id;
    /** 姓名	Varchar(32) */
    private String name;
    /** 省份id	Int(10) */
    private int province_id;
    /** 市id	Int(10) */
    private int city_id;
    /** 区/县id	Int(10) */
    private int zone_id;
    /** 省份名称	Varchar(32) */
    private String province;
    /** 市名称	Varchar(32) */
    private String city;
    /** 区/县名称	Varchar(32) */
    private String zone;
    /** 详细地址	Varchar(32) */
    private String address;
    /** 手机号码	Varchar(11) */
    private String phone;

    /** 是否为默认（收件人/寄件人）的标签 */
    private boolean defaultTag = false;



    private int layout = R.layout.item_my_address;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getZone_id() {
        return zone_id;
    }

    public void setZone_id(int zone_id) {
        this.zone_id = zone_id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public boolean isDefaultTag() {
        return defaultTag;
    }

    public void setDefaultTag(boolean defaultTag) {
        this.defaultTag = defaultTag;
    }
}
