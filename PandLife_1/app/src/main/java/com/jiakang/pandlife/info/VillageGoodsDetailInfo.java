package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.item.VillageGoodsDetail;
import com.jiakang.pandlife.utils.DataCache;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by play on 2016/1/27.
 */
public class VillageGoodsDetailInfo extends BaseAbsInfo {

    private int id;
    private VillageGoodsDetail villageGoodsDetail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public VillageGoodsDetail getVillageGoodsDetail() {
        return villageGoodsDetail;
    }

    public void setVillageGoodsDetail(VillageGoodsDetail villageGoodsDetail) {
        this.villageGoodsDetail = villageGoodsDetail;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Hardgoods&a=View&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("id",id);
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        try {
            JSONObject dataJsonObject = jsonObject.getJSONObject("data");
            villageGoodsDetail = BaseInfo.gson.fromJson(dataJsonObject.toString(),VillageGoodsDetail.class);

            //缓存数据
            DataCache aCache = DataCache.get(PandLifeApp.getInstance());
            String key = "dataEntityGoodsDetail" + villageGoodsDetail.getReplyid();
            aCache.put(key,jsonObject,10 * 60);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
