package com.jiakang.pandlife.acty.Post;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.MainPostSupportInfo;
import com.jiakang.pandlife.info.PostDeleteInfo;
import com.jiakang.pandlife.info.PostDetailInfo;
import com.jiakang.pandlife.info.ReplayCommentPostInfo;
import com.jiakang.pandlife.info.ReplayPostInfo;
import com.jiakang.pandlife.info.ReplyPostSupportInfo;
import com.jiakang.pandlife.item.PostDetail;
import com.jiakang.pandlife.item.ReplayItem;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.DataCache;
import com.jiakang.pandlife.utils.DateUtils;
import com.jiakang.pandlife.utils.DimensionUtil;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.CustomDialog;
import com.jiakang.pandlife.widget.PullToRefreshListView;
import com.jiakang.pandlife.widget.PullToRefreshListViewInScrollView;
import com.jiakang.pandlife.widget.ScrollGridView;
import com.jiakang.pandlife.widget.XScrollView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import me.iwf.photopicker.widget.TouchImageView;

@EActivity(R.layout.activity_post_detail)
public class PostDetailActivity extends BaseActy implements XScrollView.IXScrollViewListener {

    private static final String TAG = "PostDetailActivity";

    @ViewById(R.id.it_ibn_right)
    ImageButton ibtnRight;
    private TextView tvPostType;
    private TextView tvTitle;
    private ImageView ivHead;
    private TextView tvUser;
    private TextView tvDate;
    private TextView tvContent;
    private TextView tvPostSupportCount;
    private TextView tvPostCommentCount;
    private ScrollGridView gvImage;
    private ImageView ivSupportPost; //主贴点赞
    private ImageView ivPost;//回复主贴
    private PullToRefreshListViewInScrollView lvReplay;

    @ViewById(R.id.xscrollview_post_detail)
    XScrollView xscrollView;
    @ViewById(R.id.et_reply_post_detail)
    EditText etReply;
    @ViewById(R.id.tv_reply)
    TextView tvReply;//点击回复

    private ItemAdapter mItemAdapter;

    InputMethodManager mInputMethodManager;

    private int postId; //帖子id
    private String mDeleteFlag = "";
    private List<PostDetail.DataEntityPostDetail.ImglistEntity> mImglistEntities = new ArrayList<>();

    private int replyFlag;//1代表回复主贴，2代表楼层回帖
    ApiManager mApiManager = ApiManager.getInstance();

    DataCache mACache; //数据缓存

    //帖子详情类
    private PostDetailInfo mPostDetailInfo = new PostDetailInfo();
    private PostDetail.DataEntityPostDetail mDataEntityPostDetail;

    private List<ReplayItem> mReplayItemList = new ArrayList<>();

    //回复主贴
    private ReplayPostInfo mReplayPostInfo = new ReplayPostInfo();

    //回复楼层评论
    private ReplayCommentPostInfo mReplayCommentPostInfo = new ReplayCommentPostInfo();

    //主贴点赞
    private MainPostSupportInfo mMainPostSupportInfo = new MainPostSupportInfo();

    //回复帖子点赞
    private ReplyPostSupportInfo mReplyPostSupportInfo = new ReplyPostSupportInfo();

    //删除帖子
    private PostDeleteInfo mPostDeleteInfo = new PostDeleteInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void initView(){

        initTitleBar(R.id.al_tb_title, "帖子详情");


        View scrollview_content = LayoutInflater.from(this).inflate(R.layout.post_deatil_scrollview_content,null);
        if(scrollview_content != null){

            tvPostType = (TextView) scrollview_content.findViewById(R.id.tv_type_post_detail);
            tvTitle = (TextView) scrollview_content.findViewById(R.id.tv_title_post_detail);
            tvDate = (TextView) scrollview_content.findViewById(R.id.tv_date_post_detail);
            ivHead = (ImageView) scrollview_content.findViewById(R.id.iv_head_post_detail);
            tvUser = (TextView) scrollview_content.findViewById(R.id.tv_user_post_detail);
            tvContent = (TextView) scrollview_content.findViewById(R.id.tv_content_post_detail);
            tvPostSupportCount = (TextView) scrollview_content.findViewById(R.id.tv_support_count_post_detail);
            tvPostCommentCount = (TextView) scrollview_content.findViewById(R.id.tv_comment_count_post_detail);
            gvImage = (ScrollGridView) scrollview_content.findViewById(R.id.gv_image_post_detail);
            ivSupportPost = (ImageView) scrollview_content.findViewById(R.id.iv_support_post);
            ivPost = (ImageView) scrollview_content.findViewById(R.id.iv_post);
            lvReplay = (PullToRefreshListViewInScrollView) scrollview_content.findViewById(R.id.lv_replay);
        }

        xscrollView.setView(scrollview_content);

        ibtnRight.setOnClickListener(this);
        xscrollView.setPullRefreshEnable(true);
        xscrollView.setPullLoadEnable(false);
        xscrollView.setIXScrollViewListener(this);
        xscrollView.setRefreshTime(xscrollView.getTime());

        ivSupportPost.setOnClickListener(this);
        ivPost.setOnClickListener(this);
        tvReply.setOnClickListener(this);

        lvReplay.setFocusable(false);
        lvReplay.setFocusableInTouchMode(false);

        mItemAdapter = new ItemAdapter(mContext);
        lvReplay.setAdapter(mItemAdapter);

        mItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {

                switch (view.getId()) {

                    case R.id.iv_support_replay:  //回复帖子点赞

                        ReplayItem replayItem = (ReplayItem) view.getTag();
                        if(replayItem.getIssupport() == 1){
                            Toast.makeText(mContext,"您已点赞，不能重复点赞",Toast.LENGTH_SHORT).show();
                            return;
                        }

                        int id = Integer.parseInt(replayItem.getId());
                        mReplyPostSupportInfo.setForumid(id);
                        mReplyPostSupportInfo.setType("r");
                        supportReplyPostRequest();
                        break;

                    case R.id.iv_comment_replay: //回复楼层帖子

                        mInputMethodManager.toggleSoftInput(0, InputMethodManager.RESULT_SHOWN);

                        replyFlag = 2;
                        int replyid = Integer.parseInt(((ReplayItem) view.getTag()).getId());
                        mReplayCommentPostInfo.setReplyid(replyid);

                        String nick = ((ReplayItem) view.getTag()).getNick();
                        if (TextUtils.isEmpty(nick)) {
                            mReplayCommentPostInfo.setContent("回复匿名用户：");
                        } else {
                            mReplayCommentPostInfo.setContent("回复" + nick + "：");
                        }
                        etReply.setHint(mReplayCommentPostInfo.getContent());
                }
            }
        });
    }

    @AfterViews
    protected void afterCreateView() {

        initView();
        initData();
    }

    private void initData() {

        postId = getIntent().getExtras().getInt("postId");
        mDeleteFlag = getIntent().getExtras().getString("delete");

        //判断缓存是否存在
        mACache = DataCache.get(mContext);
        String key = "dataEntityPostDetail" + String.valueOf(postId);
        JSONObject jsonObject = mACache.getAsJSONObject(key);
        if(jsonObject != null){
            PostDetail postDetail = BaseInfo.gson.fromJson(jsonObject.toString(), PostDetail.class);
            mDataEntityPostDetail = postDetail.getData();
            setView();
        }else {
            postDetailRequest();
        }

        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
    }

    /**
     * 给控件填充数据
     */
    public void setView(){

        if(mDeleteFlag.equals("yes")){//判断是否可用删除帖子
            ibtnRight.setVisibility(View.VISIBLE);
        }else {
            ibtnRight.setVisibility(View.INVISIBLE);
        }

        tvPostType.setText(mDataEntityPostDetail.getTypename());
        tvTitle.setText(mDataEntityPostDetail.getTitle());
        ImageLoaderUtil.displayImageCircleCache(mDataEntityPostDetail.getHead(), ivHead, R.mipmap.ic_head);
        if (TextUtils.isEmpty(mDataEntityPostDetail.getNick())) {
            tvUser.setText("匿名用户");
        } else {
            tvUser.setText(mDataEntityPostDetail.getNick());
        }
        Long tempDate = Long.parseLong(mDataEntityPostDetail.getTime()) * 1000;
        tvDate.setText(DateUtils.getDateStringyyyyMMddHH(tempDate));
        tvContent.setText(mDataEntityPostDetail.getContent());
        tvPostSupportCount.setText(mDataEntityPostDetail.getSupportnum());
        tvPostCommentCount.setText(mDataEntityPostDetail.getReplynum());
        if (mDataEntityPostDetail.getIssupport() == 1) {
            ivSupportPost.setImageResource(R.mipmap.ic_support_press);
        } else {
            ivSupportPost.setImageResource(R.mipmap.ic_support_normal);
        }

        mImglistEntities = mDataEntityPostDetail.getImglist();
        if (null != mImglistEntities && mImglistEntities.size() > 0) {
            gvImage.setAdapter(new PictureGridViewAdapter());
        }

        //回复列表
        mReplayItemList.clear();
        mItemAdapter.clear();
        mReplayItemList = mDataEntityPostDetail.getDialoglist();
        mItemAdapter.addItems((List) mReplayItemList);
        mItemAdapter.notifyDataSetChanged();
    }

    /**
     * 订单详情请求
     */
    private void postDetailRequest() {

        mPostDetailInfo.setId(postId);
        mApiManager.request(mPostDetailInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mDataEntityPostDetail = mPostDetailInfo.getDataEntityPostDetail();
                setView();
            }
        });
    }

    /**
     * 回复主贴
     */
    private void replyPostRequest(){

        mReplayPostInfo.setReplyid(postId);
        if(TextUtils.isEmpty(etReply.getText().toString())){
            Toast.makeText(mContext,"请输入回复内容！",Toast.LENGTH_SHORT).show();
            return;
        }
        mReplayPostInfo.setContent(etReply.getText().toString());

        mApiManager.request(mReplayPostInfo,new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                Toast.makeText(mContext,"回复楼主成功！",Toast.LENGTH_SHORT).show();
                postDetailRequest();
                etReply.setText("");
                etReply.setHint("回复楼主：");
                mReplayPostInfo.setContent("");
                mInputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });
    }

    /**
     * 回复楼层帖子
     */
    private void replyCommentPostRequest(){


        if(TextUtils.isEmpty(etReply.getText())){//用户没有输入内容

            Toast.makeText(mContext,"请输入回复内容！",Toast.LENGTH_SHORT).show();
            return;
        }

        mReplayCommentPostInfo.setContent(mReplayCommentPostInfo.getContent() + etReply.getText().toString());

        mApiManager.request(mReplayCommentPostInfo,new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                postDetailRequest();
                Toast.makeText(mContext,"回复楼层评论成功！",Toast.LENGTH_SHORT).show();
                etReply.setText("");
                etReply.setHint("回复楼主：");
                mReplayCommentPostInfo.setContent("");
                mInputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });
    }

    /**
     * 主帖点赞
     */
    private void supportMainPostRequest(){

        mMainPostSupportInfo.setForumid(postId);
        mMainPostSupportInfo.setType("s");
        mApiManager.request(mMainPostSupportInfo,new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                postDetailRequest();
            }
        });
    }

    /**
     * 回复帖子点赞
     */
    private void supportReplyPostRequest(){

        mApiManager.request(mReplyPostSupportInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                postDetailRequest();
            }
        });
    }

    /**
     * 删除帖子
     */
    private void postDelete(){

        mPostDeleteInfo.setId(postId);
        mApiManager.request(mPostDeleteInfo,new AbsOnRequestListener(mContext){

            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                Toast.makeText(mContext,"帖子删除成功！",Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    public void onRefresh() {

        postDetailRequest();
        xscrollView.finishLoad();
    }

    @Override
    public void onLoadMore() {

    }

    //帖子详情中gridview图片适配器
    class PictureGridViewAdapter extends BaseAdapter {

        @Override
        public int getCount() {

            return mImglistEntities.size();
        }

        @Override
        public Object getItem(int position) {

            return mImglistEntities.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            WindowManager wm = PostDetailActivity.this.getWindowManager();

            int imageSize = (int) ((wm.getDefaultDisplay().getWidth() - DimensionUtil.dip2px
                    (mContext,24)) / 3 );
            TouchImageView imageView;
            if(convertView == null){

                imageView = new TouchImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(imageSize,imageSize));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }else {
                imageView = (TouchImageView) convertView;
            }

            ImageLoaderUtil.displayImage(mImglistEntities.get(position).getImgurl(),imageView,R.mipmap.default_image);
            return imageView;
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

            case R.id.it_ibn_left:
                setResult(RESULT_OK);
                finish();
                break;

            case R.id.it_ibn_right:

                final CustomDialog customDialog = Util.getDialog(mContext, "提示", "确定删除吗？", "确定", "取消");
                customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                        postDelete();
                    }
                });
                customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                    }
                });
                customDialog.show();
                break;
            case R.id.iv_post://回复主贴
                mInputMethodManager.toggleSoftInput(0, InputMethodManager.RESULT_SHOWN);
                replyFlag = 1;
                etReply.setHint("回复楼主：");
                break;

            case R.id.tv_reply: //发送回复
                if(replyFlag == 2){ //回复楼层
                    replyCommentPostRequest();
                }else {
                    replyPostRequest();
                }
                break;

            case R.id.iv_support_post:

                if(mDataEntityPostDetail.getIssupport() == 1){
                    Toast.makeText(mContext,"已点赞，不能重复点赞",Toast.LENGTH_SHORT).show();
                    return;
                }
                supportMainPostRequest();
                break;
        }

    }
}
