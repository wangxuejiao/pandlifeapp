package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 购买成功
 */
@EActivity(R.layout.activity_purchase_success)
public class PurchaseSuccessActivity extends BaseActy {
    protected static final String TAG = "PurchaseSuccessActivity";

    @ViewById(R.id.aps_btn_check_my_bill)
    Button checkMyBillBN;
    @ViewById(R.id.aps_btn_return_homepage)
    Button returnHomepageBN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        checkMyBillBN.setOnClickListener(this);
        returnHomepageBN.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.aps_btn_check_my_bill://点击查看我的订单
//                Intent intentMyBill = new Intent(mContext, MyIndentActivity.class);
//                startActivity(intentMyBill);
                break;
            case R.id.aps_btn_return_homepage://点击返回首页
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }
}
