package com.jiakang.pandlife.utils;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by play on 2016/1/12.
 */
public class DateUtils {


    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
    private static SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd HH:mm");
    public static SimpleDateFormat sdf3 = new SimpleDateFormat("MM月dd日");
    /** "yyyy-MM-dd HH:mm" */
    public static SimpleDateFormat sdf_HH_mm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    /** "yyyy-MM-dd" */
    public static SimpleDateFormat sdf_yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat todayDateFormat = new SimpleDateFormat("HH:mm");
    private static int oneDatesTimes = 24 * 60 * 60 * 1000;
    private static int twoDatesTimes = 2 * oneDatesTimes;


    //========================返还yyyy-MM-dd HH:mm:SS================================
    /**
     * 将long类型的值转换成转换成yyyy-MM-dd HH:mm:SS格式
     * @param date long类型的值
     * @return
     */
    public static String getDateStringyyyyMMddHHmm(Long date){

        Date tempDate = new Date(date);
        return sdf.format(tempDate);
    }

    /**
     * 将String类型的时间，转换成yyyy-MM-dd HH:mm:SS格式
     * @param date String类型值
     * @return
     */
    public static String getDateStringyyyyMMddHHmm(String date){

        Date tempDate = new Date(Long.parseLong(date));
        return date=sdf.format(tempDate);
    }



    //========================返还yyyy-MM-dd HH:mm================================

    /**
     * 将String类型的时间，转换成yyyy-MM-dd HH:mm格式
     * @param date String类型值
     * @return
     */
    public static String getDateStringyyyyMMddHH(Long date){

        Date tempDate = new Date(date);
        return sdf_HH_mm.format(tempDate);
    }


    /**
     * 将String类型的时间，转换成yyyy-MM-dd HH:mm格式
     * @param date String类型值
     * @return
     */
    public static String getDateStringyyyyMMddHH(String date){

        Date tempDate = new Date(Long.parseLong(date));
        return date=sdf.format(tempDate);
    }


    public static String getDateToString(Date date){

        return sdf_yyyy_MM_dd.format(date);
    }






    /*String time="2010-11-20 11:10:10";

    Date date=null;
    SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    date=formatter.parse(time);
    System.out.println(date);*/



    /**
     * 转换成MM月dd日
     * @param date Date类型值
     * @return
     */
    public static String getTime(Date date) {
        String strDate = "";
        strDate = sdf3.format(date);
        return strDate;
    }

    /**
     * 将String类型转换成Date yyyy-MM-dd HH:mm:SS
     * @param time String类型值
     * @return
     */
    public static Date getDateyyyyMMddHHmmSS(String time) {
        return getDate(sdf, time);
    }

    /**
     * 将String转换成 yyyy-MM-dd HH:mm
     * @param time String类型值
     * @return
     */
    public static Date getDateyyyyMMddHHmm(String time) {
        return getDate(sdf_HH_mm, time);
    }

    /**
     * "yyyy-MM-dd HH:mm:SS"
     *
     * @param currentTimeMilli
     * @return String
     */
    public static String getFormatTime(Long currentTimeMilli) {
        try {
            return getIntelligentizeDate(new Date(currentTimeMilli));
        } catch (Exception e) {
            return "";
        }
    }

    public static Date getDate(SimpleDateFormat sdf, String time) {
        try {
            Date date = sdf.parse(time);
            return date;
        } catch (Exception e) {
            return null;
        }
    }



    /**
     * 将Date转换成想要的格式
     * @param format 传入的格式，如：yyyy-MM-dd HH:mm:ss
     * @param date  Date类型值
     * @return
     */
    public static String getTime(String format, Date date) {
        SimpleDateFormat sDateFormat = new SimpleDateFormat(format);
        String dateStr = sDateFormat.format(date);
        return dateStr;
    }

    /**
     * 将new Date()，转换成想要的格式
     * @param format 传入的格式，如：yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String getTime(String format) {
        return getTime(format, new Date());
    }
















    /**
     * 获取智能化时间 今天 HH:mm 昨天 HH:mm HH   MM-dd HH:mm
     * @param time long类型的值
     * @return String
     */
    public static String getIntelligentizeDate(long time) {
        if (time == 0) {
            return "";
        }
        return getIntelligentizeDate(new Date(time));
    }


    /**
     * 智能化获取时间 今天 HH:mm 昨天 HH:mm HH   MM-dd HH:mm
     *
     * @param date Date类型的值
     * @return String
     */
    public static String getIntelligentizeDate(Date date) {
        String resutl;
        resutl = isToday(date);
        if (!TextUtils.isEmpty(resutl)) {
            return resutl;
        }
        resutl = isYesterday(date);
        if (!TextUtils.isEmpty(resutl)) {
            return resutl;
        }
        resutl = otherDate(date);
        if (!TextUtils.isEmpty(resutl)) {
            return resutl;
        }
        return resutl;
    }

    public static String isToday(Date date) {
        String strDate = "";
        Date dateTemp = new Date();
        dateTemp.setHours(0);
        dateTemp.setMinutes(0);
        dateTemp.setSeconds(0);
        if (date.getTime() - dateTemp.getTime() <= oneDatesTimes && date.getTime() - dateTemp.getTime() >= 0) {
            strDate = "今天\t" + todayDateFormat.format(date);
        }
        return strDate;
    }

    public static String isYesterday(Date date) {
        String strDate = "";
        Date dateTemp = new Date();
        dateTemp.setDate(dateTemp.getDate() - 1);
        dateTemp.setHours(0);
        dateTemp.setMinutes(0);
        dateTemp.setSeconds(0);
        if (date.getTime() - dateTemp.getTime() <= twoDatesTimes && date.getTime() - dateTemp.getTime() >= 0) {
            strDate = "昨天\t" + todayDateFormat.format(date);
        }
        return strDate;
    }

    public static String otherDate(Date date) {
        String strDate = "";
        strDate = sdf2.format(date);
        return strDate;
    }


    /**
     * 判断选择的日期是否在今天之后，是返回true
     * @param setDateTime
     * @return
     */
    public static boolean compareDate(String setDateTime){

        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();
        Date setDate = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            setDate = format.parse(setDateTime);
            return setDate.after(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * 判断传入时间是否在现在时间之后，在之后返回true
     * @param format  SimpleDateFormat
     * @param setDateTime 传入的时间String型
     * @return
     */
    public static boolean compareAfterDate(SimpleDateFormat format,String setDateTime){

        Calendar calendar = Calendar.getInstance();
        Date currentDate = calendar.getTime();
        Date setDate = null;
        try {
            setDate = format.parse(setDateTime);
            return setDate.after(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }


}
