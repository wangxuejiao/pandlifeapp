package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 更换昵称、用户名
 */
@EActivity(R.layout.activity_change_user_name)
public class ChangeUserNameActivity extends BaseActy {
    protected static final String TAG = "ChangeUserNameActivity";

    @ViewById(R.id.acun_et_change)
    EditText changeET;
    @ViewById(R.id.acun_iv_clear)
    ImageView clearIV;
    @ViewById(R.id.acun_btn_confirm)
    Button confirmBN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.acun_tb_title, "昵称");

        clearIV.setOnClickListener(this);
        confirmBN.setOnClickListener(this);

        // 如果有改动监听
        changeET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setDeleteAndEnable();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * 控制图片的显示和按钮的可否点击
     */
    public void setDeleteAndEnable() {
        if (changeET.getText().toString().length() == 0) {
            clearIV.setVisibility(View.GONE);
            confirmBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_border_bg));
            confirmBN.setEnabled(false);
        } else {
            clearIV.setVisibility(View.VISIBLE);
            confirmBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_bg_reddish_orange));
            confirmBN.setEnabled(true);
        }
    }

    /**
     * 确定修改昵称的方法
     */
    private void confirmChange(){

        Intent intentUserName = new Intent();
        intentUserName.putExtra("name", changeET.getText().toString());
        setResult(RESULT_OK, intentUserName);
        finish();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.acun_iv_clear:
                changeET.setText("");
                clearIV.setVisibility(View.GONE);
                confirmBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_border_bg));
                confirmBN.setEnabled(false);
                break;
            case R.id.acun_btn_confirm:
                confirmChange();
                break;
        }
    }
}
