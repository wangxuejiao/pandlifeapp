package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.GroupPurchaseDetailItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.MyLeaseItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.Util;

/**
 * Created by play on 2016/1/6.
 */
public class MyRentItemView extends AbsRelativeLayout {

    private ImageView imageIV;
    private TextView titleTV;
    private TextView numberTV;
    private TextView sumTV;
    private TextView dateTV;
    private TextView statusTV;

    public MyRentItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyRentItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        imageIV = (ImageView)findViewById(R.id.imr_iv_icon);
        titleTV = (TextView) findViewById(R.id.imr_tv_title);
        numberTV = (TextView) findViewById(R.id.imr_tv_number);
        sumTV = (TextView) findViewById(R.id.imr_tv_sum);
        dateTV = (TextView) findViewById(R.id.imr_tv_date);
        statusTV = (TextView) findViewById(R.id.imr_tv_status);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);
        MyLeaseItem myLeaseItem = (MyLeaseItem)item;
        ImageLoaderUtil.displayImage(myLeaseItem.getPic(), imageIV);
        titleTV.setText(myLeaseItem.getTitle());
        numberTV.setText("×" + myLeaseItem.getNums());
        sumTV.setText("押金￥" + myLeaseItem.getDeposit());
        long startTime = 0;
        long backTime = 0;
        String startTimeStr = myLeaseItem.getStart_time();
        String backTimeStr = myLeaseItem.getBack_time();
        try {
            startTime = Long.parseLong(startTimeStr) * 1000;
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            backTime = Long.parseLong(backTimeStr) * 1000;
        }catch (Exception e){
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(startTimeStr)){
            dateTV.setText("还：" + Util.getFormatedDateTime("yy-MM-dd", backTime));
        }else if(TextUtils.isEmpty(backTimeStr)){
            dateTV.setText("借：" + Util.getFormatedDateTime("yy-MM-dd", startTime));
        }else{
            dateTV.setText("借：" + Util.getFormatedDateTime("yy-MM-dd", startTime) + "——还：" + Util.getFormatedDateTime("yy-MM-dd", backTime));
        }
        String is_back = myLeaseItem.getIs_back();
        if (("y").equals(is_back)){
            statusTV.setText("已归还");
        }else{
            statusTV.setText("待归还");
        }


    }
}
