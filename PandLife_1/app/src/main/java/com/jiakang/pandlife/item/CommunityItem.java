package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

/**
 * Created by Administrator on 2016/1/12.
 */
public class CommunityItem extends Item{

    /** 小区的id */
    private String cid;
    /** 小区名称 */
    private String name;
    /** 地址经度 */
    private String longitude;
    /** 地址纬度 */
    private String latitude;
    /** 距离 */
    private String juli;

    private int layout = R.layout.item_select_single_left;
    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getJuli() {
        return juli;
    }

    public void setJuli(String juli) {
        this.juli = juli;
    }
}
