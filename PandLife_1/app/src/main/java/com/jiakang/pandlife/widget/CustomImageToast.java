package com.jiakang.pandlife.widget;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jiakang.pandlife.R;

/**
 * Created by play on 2016/1/13.
 */
public class CustomImageToast extends Toast {


    private Context mContext;
    private View view;
    private TextView textView;
    private ImageView imageView;

    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you
     * can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application}
     *                or {@link Activity} object.
     */
    public CustomImageToast(Context context) {
        super(context);
        mContext = context;

        view = View.inflate(mContext, R.layout.view_toast,null);
        textView = (TextView) view.findViewById(R.id.tv_toast);
        imageView = (ImageView) view.findViewById(R.id.iv_toast);
    }

    public void showCustomToast(String text,int imgResId){

        textView.setText(text);
        imageView.setImageResource(imgResId);
        setView(view);
        setGravity(Gravity.CENTER,0,0);
        setDuration(LENGTH_SHORT);
        show();
    }
}
