package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

/**
 * 我的租借列表实体
 * Created by Administrator on 2016/1/14.
 */
public class MyLeaseItem extends Item{

    /** 序号id String */
    private String id;
    /** 租借产品id String */
    private String lid;
    /** 租借产品数量	Int(10) */
    private int nums;
    /** 租借开始时间 String */
    private String start_time;
    /** 是否归还（已还y 未还 n） String */
    private String is_back;
    /** 约定归还时间 String */
    private String back_time;
    /** 实际归还时间 String */
    private String tback_time;
    /** 租借产品名称 String */
    private String title;
    /** 租借产品图片 String */
    private String pic;
    /** 租借押金 String */
    private String deposit;

    private int layout = R.layout.item_my_rent;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLid() {
        return lid;
    }

    public void setLid(String lid) {
        this.lid = lid;
    }

    public int getNums() {
        return nums;
    }

    public void setNums(int nums) {
        this.nums = nums;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getIs_back() {
        return is_back;
    }

    public void setIs_back(String is_back) {
        this.is_back = is_back;
    }

    public String getBack_time() {
        return back_time;
    }

    public void setBack_time(String back_time) {
        this.back_time = back_time;
    }

    public String getTback_time() {
        return tback_time;
    }

    public void setTback_time(String tback_time) {
        this.tback_time = tback_time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }
}
