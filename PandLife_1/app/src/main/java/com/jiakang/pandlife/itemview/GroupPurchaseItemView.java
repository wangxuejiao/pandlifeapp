package com.jiakang.pandlife.itemview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.GroupPurchaseItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.RedPackageItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.utils.SyncImageLoader;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.Anticlockwise;

import org.androidannotations.annotations.EActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by play on 2016/1/6.
 */
@SuppressLint("ShowToast")
public class GroupPurchaseItemView extends AbsRelativeLayout {

    private ImageView pictureIV;
    private TextView openDateTV;
    private TextView titleTV;
    private TextView groupStatusTV;
    private Chronometer timerTV;
//    private TextView secondTV;
    private TextView oldSumTV;
    private TextView nowSumTV;
    private TextView groupNumberTV;
    private Button joinGroupBN;

    private SimpleDateFormat mTimeFormat;
    private long mTime;
    private long mNextTime;

    public GroupPurchaseItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GroupPurchaseItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        pictureIV = (ImageView)findViewById(R.id.igp_iv_picture);
        openDateTV = (TextView) findViewById(R.id.igp_tv_open_group);
        titleTV = (TextView) findViewById(R.id.igp_tv_title);
        groupStatusTV = (TextView) findViewById(R.id.igp_tv_stop_group);
        timerTV = (Chronometer) findViewById(R.id.igp_tv_stop_timer);
//        secondTV = (TextView) findViewById(R.id.igp_tv_stop_second);
        oldSumTV = (TextView) findViewById(R.id.igp_tv_old_sum);
        nowSumTV = (TextView) findViewById(R.id.igp_tv_now_sum);
        groupNumberTV = (TextView) findViewById(R.id.igp_tv_group_number);
        joinGroupBN = (Button) findViewById(R.id.igp_btn_join_group);
    }

    @Override
    public void setObject(final Item item, final int position,final ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        GroupPurchaseItem groupPurchaseItem = (GroupPurchaseItem) item;
        SyncImageLoader.getInstance(PandLifeApp.getInstance().getBaseContext()).loadZoomImage(groupPurchaseItem.getPic(), new SyncImageLoader.OnloadImage() {
            @Override
            public void loadFinish(Bitmap bitmap) {
                pictureIV.setImageBitmap(bitmap);
            }
            @Override
            public void loadFail(boolean hasNet) {
                pictureIV.setImageResource(R.mipmap.public_img_default);
            }
        }, 1000, 200);
//        SyncImageLoader.getInstance(PandLifeApp.getInstance().getBaseContext()).loadZoomImage(groupPurchaseItem.getPic(), new SyncImageLoader.OnloadImage() {
//            @Override
//            public void loadFinish(Bitmap bitmap) {
//                pictureIV.setImageBitmap(bitmap);
//            }
//            @Override
//            public void loadFail(boolean hasNet) {
//                pictureIV.setImageResource(R.mipmap.public_img_default);
//            }
//        }, pictureIV);
        ImageLoaderUtil.displayImage(groupPurchaseItem.getPic(), pictureIV);
        long startTime = Long.parseLong(groupPurchaseItem.getStarttime()) * 1000;
        long endTime = Long.parseLong(groupPurchaseItem.getEndtime()) * 1000;
        openDateTV.setText(Util.getFormatedDateTime("HH:mm", startTime) + "开团");
        titleTV.setText(groupPurchaseItem.getTitle());
        nowSumTV.setText("￥" + groupPurchaseItem.getTprice());
        oldSumTV.setText("￥" + groupPurchaseItem.getPrice());
        groupNumberTV.setText(groupPurchaseItem.getTotal() + "");

        long nowTime = System.currentTimeMillis();
        if (nowTime < startTime) {
            //初始化时间
//          timerTV.setBase((startTime - nowTime) / 1000);
            mNextTime = (startTime - nowTime)/1000;
            // 自动生成的构造函数存根
            mTimeFormat = new SimpleDateFormat("HH:mm:ss");
            timerTV.setOnChronometerTickListener(listener);
            timerTV.start();
            groupStatusTV.setText("距离团购开始");
        }else if ((nowTime > startTime) && (nowTime < endTime)){
            //初始化时间
//          timerTV.setBase((startTime - nowTime) / 1000);
            mNextTime = (endTime - nowTime)/1000;
            // 自动生成的构造函数存根
            mTimeFormat = new SimpleDateFormat("HH:mm:ss");
            timerTV.setOnChronometerTickListener(listener);
            timerTV.start();
            groupStatusTV.setText("距离团购结束");
        }else{
            mNextTime = 0;
            timerTV.setText("00:00:00");
            groupStatusTV.setText("距离团购结束");
            joinGroupBN.setText("团购结束");
        }

//        hourTV.setText(groupPurchaseItem.getEndtime());
//        try {
//            String startTimeStr = groupPurchaseItem.getStarttime();
//            long startTime = Long.parseLong(startTimeStr);
//            long nowTime = SystemClock.currentThreadTimeMillis();
//            CountDownTimer id = new CountDownTimer(startTime - nowTime,1000){
//                @Override
//                    public void onFinish() {
//                    //done
//                }
//                @Override
//                public void onTick(long arg0) {
//                    //每1000毫秒回调的方法
//                    hourTV.setText(Util.getFormatedDateTime("HH:mm:ss", arg0));
//                }
//            }.start();
//        }catch (Exception e){
//            JKLog.e("GroupPurchaseItemView" , "时间转换异常------------------->" + e);
//        }


        joinGroupBN.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setTag(item);
                onViewClickListener.onViewClick(v, position);
            }
        });
    }

    Chronometer.OnChronometerTickListener listener = new Chronometer.OnChronometerTickListener()
    {
        @Override
        public void onChronometerTick(Chronometer chronometer)
        {
            if (mNextTime <= 0) {
                if (mNextTime == 0){
                }
                mNextTime = 0;
                timerTV.setText(mTimeFormat.format(new Date(mNextTime * 1000)));
                return;
            }

            mNextTime--;

            timerTV.setText(mTimeFormat.format(new Date(mNextTime * 1000)));
        }
    };
}
