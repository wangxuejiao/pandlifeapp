package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.item.MyLeaseItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 我的租借中详情
 */
@EActivity(R.layout.activity_my_rent_detail)
public class MyRentDetailActivity extends BaseActy {
    protected static final String TAG = "MyRentDetailActivity";

    @ViewById(R.id.amrd_tv_status)
    TextView statusTV;
    @ViewById(R.id.amrd_tv_date)
    TextView dateTV;
    @ViewById(R.id.amrd_iv_icon)
    ImageView imageIV;
    @ViewById(R.id.amrd_tv_title)
    TextView titleTV;
    @ViewById(R.id.amrd_tv_number)
    TextView numberTV;
    @ViewById(R.id.amrd_tv_sum)
    TextView depositTV;
    @ViewById(R.id.amrd_tv_sum_real)
    TextView realDepositTV;
    @ViewById(R.id.amrd_tv_rent_date_content)
    TextView rentDateTV;
    @ViewById(R.id.amrd_tv_promise_return_date_content)
    TextView promiseReturnDateTV;
    @ViewById(R.id.amrd_rl_real_return_date)
    RelativeLayout realReturnDateRL;
    @ViewById(R.id.amrd_tv_real_return_date_content)
    TextView realReturnDateTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.amrd_tb_title, "详情");

        bindView();
    }

    private void bindView(){
        Intent intent = getIntent();
        MyLeaseItem myLeaseItem = (MyLeaseItem)intent.getExtras().get("myLeaseItem");
        if (myLeaseItem != null){
            ImageLoaderUtil.displayImage(myLeaseItem.getPic(), imageIV);

            long startTime = 0;
            long backTime = 0;
            String startTimeStr = myLeaseItem.getStart_time();
            String backTimeStr = myLeaseItem.getBack_time();
            try {
                startTime = Long.parseLong(startTimeStr) * 1000;
            }catch (Exception e){
                e.printStackTrace();
            }
            try {
                backTime = Long.parseLong(backTimeStr) * 1000;
            }catch (Exception e){
                e.printStackTrace();
            }
            titleTV.setText(myLeaseItem.getTitle());
            numberTV.setText("×" + myLeaseItem.getNums());
            depositTV.setText("押金￥" + myLeaseItem.getDeposit());
            realDepositTV.setText("￥" + myLeaseItem.getDeposit());
            rentDateTV.setText(Util.getFormatedDateTime("yyyy-MM-dd", startTime));
            promiseReturnDateTV.setText(Util.getFormatedDateTime("yyyy-MM-dd", backTime));

            String is_back = myLeaseItem.getIs_back();
            if (("y").equals(is_back)){
                long tbackTime = 0;
                String tbackTimeStr = myLeaseItem.getTback_time();
                try {
                    tbackTime = Long.parseLong(tbackTimeStr) * 1000;
                }catch (Exception e){
                    e.printStackTrace();
                }
                //如果是已归还就有实际归还时间了
                realReturnDateRL.setVisibility(View.VISIBLE);
                statusTV.setText("已归还");
                if (tbackTime == 0){
                    dateTV.setText("");
                    realReturnDateTV.setText("");
                }else {
                    dateTV.setText(Util.getFormatedDateTime("yyyy-MM-dd HH:mm", tbackTime));
                    realReturnDateTV.setText(Util.getFormatedDateTime("yyyy-MM-dd HH:mm", tbackTime));
                }
            }else{
                //如果是已归还就有实际归还时间了
                realReturnDateRL.setVisibility(View.GONE);
                statusTV.setText("待归还");
                dateTV.setText("应还日期：" + Util.getFormatedDateTime("yyyy-MM-dd", backTime));
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
