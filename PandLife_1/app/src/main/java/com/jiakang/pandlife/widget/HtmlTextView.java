package com.jiakang.pandlife.widget;

/** 
 * @author：xuwangwang E-mail: 541765907@qq.com
 * @date：2014年6月20日 上午9:39:35
 * @version 1.0   
 * String Tag = "HtmlTextView中：";
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.jiakang.pandlife.PandLifeApp;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;


/**
 * 支持Html的标签.
 */
public class HtmlTextView extends TextView {

	public HtmlTextView(Context context) {
		super(context);
	}

	public HtmlTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public HtmlTextView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		if (text == null) {
			super.setText("", type);
			return;
		}

		if (isInEditMode()) {
			super.setText(text.toString(), type);
		} else {
			super.setText(Html.fromHtml(text.toString()), type);
		}
	}

	public void setText2(CharSequence text) {
		if (text == null) {
			super.setText("");
			return;
		}

		if (isInEditMode()) {
			super.setText(text.toString());
		} else {
			// super.setText(Html.fromHtml(text.toString(), new
			// ImageGetter(ImageGetter.SOURCE_TYPE_NETWORK), null));
			super.setText(Html.fromHtml(text.toString(), new URLImageParser(this, getContext()), null));
		}
	}

	private class ImageGetter implements Html.ImageGetter {
		static final int SOURCE_TYPE_ASSET = 0;
		static final int SOURCE_TYPE_FILESYSTEM = 1;
		static final int SOURCE_TYPE_NETWORK = 2;

		private int sourceType = SOURCE_TYPE_ASSET;

		public ImageGetter(int sourceType) {
			this.sourceType = sourceType;
		}

		@Override
		public Drawable getDrawable(String source) {
			switch (sourceType) {
			case 0:
				return Drawable.createFromPath(source);
			case 1:
				Log.d("HtmlTextView$ImageGetter.getDrawable(SOURCE_TYPE_FILESYSTEM)", "功能待构建");
				return null;
			case 2:
				Log.d("HtmlTextView$ImageGetter.getDrawable(SOURCE_TYPE_NETWORK)", "功能待构建");
				Bitmap bitmap = PandLifeApp.getInstance().getSyncImageLoader().downloadBitmap(source, -1, -1);
				return new BitmapDrawable(bitmap);
			}
			return null;
		}
	}

	public class URLImageParser implements Html.ImageGetter {
		Context c;
		View container;

		/***
		 * Construct the URLImageParser which will execute AsyncTask and refresh
		 * the container
		 * 
		 * @param t
		 * @param c
		 */
		public URLImageParser(View t, Context c) {
			this.c = c;
			this.container = t;
		}

		public Drawable getDrawable(String source) {
			URLDrawable urlDrawable = new URLDrawable();

			// get the actual source
			ImageGetterAsyncTask asyncTask = new ImageGetterAsyncTask(urlDrawable);

			asyncTask.execute(source);

			// return reference to URLDrawable where I will change with actual
			// image from
			// the src tag
			return urlDrawable;
		}

		public class ImageGetterAsyncTask extends AsyncTask<String, Void, Drawable> {
			URLDrawable urlDrawable;

			public ImageGetterAsyncTask(URLDrawable d) {
				this.urlDrawable = d;
			}

			@Override
			protected Drawable doInBackground(String... params) {
				String source = params[0];
				return fetchDrawable(source);
			}

			@Override
			protected void onPostExecute(Drawable result) {
				// set the correct bound according to the result from HTTP call
				Log.d("height", "" + result.getIntrinsicHeight());
				Log.d("width", "" + result.getIntrinsicWidth());
				urlDrawable.setBounds(0, 0, 0 + result.getIntrinsicWidth(), 0 + result.getIntrinsicHeight());

				// change the reference of the current drawable to the result
				// from the HTTP call
				urlDrawable.drawable = result;

				// redraw the image by invalidating the container
				URLImageParser.this.container.invalidate();
			}

			/***
			 * Get the Drawable from URL
			 * 
			 * @param urlString
			 * @return
			 */
			public Drawable fetchDrawable(String urlString) {
				try {
					URL aURL = new URL(urlString);
					final URLConnection conn = aURL.openConnection();
					conn.connect();
					final BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
					final Bitmap bm = BitmapFactory.decodeStream(bis);
					Drawable drawable = new BitmapDrawable(bm);
					drawable.setBounds(0, 0, bm.getWidth(), bm.getHeight());
					return drawable;
				} catch (Exception e) {
					return null;
				}
			}
		}
	}

}
