package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.CityItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取地区接口
 *
 * @author ww
 *
 */
public class AreaInfo extends BaseAbsInfo {

    private static final String TAG = "AreaInfo";

    /** 地区编码的code值	Int(10) */
    private int id;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<AreaItem> allItems = new ArrayList<AreaItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Address&a=Data" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("id", id);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                AreaItem areaItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    areaItem = BaseInfo.gson.fromJson(itemStr, AreaItem.class);
                    areaItem.setLayout(R.layout.item_select_single_left);
                    // 入库
//                    recommendRecruitItem.insert();
                    allItems.add(areaItem);
                }

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<AreaItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<AreaItem> allItems) {
        this.allItems = allItems;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
