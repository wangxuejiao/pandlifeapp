package com.jiakang.pandlife.utils;

import android.content.Context;
import android.util.Log;

import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.QinNiuTokenInfo;
import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UploadManager;

import org.json.JSONObject;

/**
 * Created by play on 2016/1/14.
 */
public class QiNiuUtil{

    private static final String TAG = "QiNiuUtil";
    //请求七牛token类
    private static QinNiuTokenInfo mQinNiuTokenInfo = new QinNiuTokenInfo();;
    public static String qiNiuToken;

    /**
     * 获取七牛token
     */
    public static String requestQiNiuToken(Context context) {


        ApiManager.getInstance().request(mQinNiuTokenInfo, new AbsOnRequestListener(context) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                qiNiuToken = mQinNiuTokenInfo.getDataEntityQiNiuToken().getQiniutoken();
                Log.i(TAG,"---->七牛Token——qiNiuToken:" + mQinNiuTokenInfo.getDataEntityQiNiuToken().getQiniutoken());
            }

        });
        return qiNiuToken;
    }
    /**
     * 不带配置上传
     * @param filePath
     * @param key  指定上传到七牛服务器文件的名称
     * @param qiNiuToken
     */
    public static void uploadNoConfig(String filePath,String key,String qiNiuToken){


        UploadManager uploadManager = new UploadManager();
        uploadManager.put(filePath, key, qiNiuToken, new UpCompletionHandler() {
            @Override
            public void complete(String key, ResponseInfo info, JSONObject response) {

                Log.i(TAG,"----->七牛responseInfo:" + info);
                Log.i(TAG, "----->七牛response:" + response.toString());
            }
        },null);

    }

}
