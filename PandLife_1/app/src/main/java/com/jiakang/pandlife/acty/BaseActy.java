package com.jiakang.pandlife.acty;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.TitleBar;

import java.util.HashMap;

/**
 * 没有在注册文件中注册的一个Activity
 */
public class BaseActy extends FragmentActivity implements OnClickListener,AMapLocationListener {
	protected final String TAG = "BaseActivity";
	protected Context mContext;
	// public boolean hasNet = false;
	/** 进入此acty是否有动画 */
	protected boolean hasAnim = true;
	protected TitleBar titleBar;
	/** 当前界面是否可见 */
	protected boolean isShow = false;
	/**  */
	protected boolean isDestroy = false;
	/** 广播接收过滤器 */
	private IntentFilter filter = new IntentFilter();

	private AMapLocationClient locationClient = null;
	private AMapLocationClientOption locationOption = null;
	/** 记录经度 */
	double locationLongitude = 0;
	/** 记录纬度 */
	double locationLatitude = 0;

	public static boolean isOrNotLogin;

	private SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

	/** 接收器 */
	private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// String action = intent.getAction();
			// 其他的action回调
			onReceived(context, intent);
		}
	};

	protected Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (!isDestroy) {
				handleMessaged(msg);
			} else {
				JKLog.e(TAG, "handleMessage方法中----->：msg.obj为：" + msg.obj);
			}
			super.handleMessage(msg);
		}

	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		PandLifeApp.addActivity(this);
		isDestroy = false;

		// 子类要注册的广播接受者
		inJectFilter(filter);
		if (filter.countActions() > 0)
			registerReceiver(receiver, filter);

		// 页面2左侧进入，页面1左侧退出
		if (hasAnim)
			overridePendingTransition(R.anim.i_slide_in_left, R.anim.i_slide_out_left);

		// try {
		// GFAgent.setReportUncaughtExceptions(true);
		// } catch (Exception e) {
		// XYLog.e(TAG, "onCreate方法中-------->：e为：" + e);
		// }

		initLocation();

		//是否跳过登录

		if (TextUtils.isEmpty(BaseInfo.token)) {
			String token = myAccount.getString(Constant.Spf.TOKEN, "");
			if (!TextUtils.isEmpty(token)) {
				BaseInfo.token = token;
			}
		}
	}

	/**
	 * 初始化定位方法
	 */
	public void initLocation(){
		//初始化定位对象
		locationClient = new AMapLocationClient(this.getApplicationContext());
		locationOption = new AMapLocationClientOption();
		// 设置定位模式为低功耗模式
		locationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
		// 设置定位监听
		locationClient.setLocationListener(this);
		// 设置为单次定位
		locationOption.setOnceLocation(true);
		locationOption.setNeedAddress(true);
		// 设置定位参数
		locationClient.setLocationOption(locationOption);
	}

	/**
	 * 开始定位的方法
	 */
	public void startLocation(){
		// 启动定位
		locationClient.startLocation();
	}

	public PandLifeApp getMyApp() {
		return (PandLifeApp) super.getApplication();
	}

	@Override
	protected void onPause() {
		super.onPause();
		isShow = false;

		// 机锋统计
		// TCAgent.onPause(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		isShow = true;
		if (!Util.hasNet(mContext, false)) {
			CustomToast.showToast(mContext, "请检查网络", 1000);
		}
		// 机锋统计
		// TCAgent.onResume(this);
	}

	@Override
	protected void onRestoreInstanceState(Bundle bundle) {
		super.onRestoreInstanceState(bundle);
		JKLog.i(TAG, "BaseActivity中：onRestoreInstanceState方法-------------->" + bundle);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		JKLog.i(TAG, "BaseActivity中：onRestoreInstanceState方法-------------->" + outState);
	}

	@Override
	protected void onDestroy() {
		isDestroy = true;

		if (filter.countActions() > 0)
			unregisterReceiver(receiver);

		PandLifeApp.removeActivity(mContext);
		mContext = null;
		filter = null;
		receiver = null;
		titleBar = null;

		mTag = null;
		if (mTags != null) {
			mTags.clear();
			mTags = null;
		}

		if(null != locationClient){
			locationClient.onDestroy();
		}
		super.onDestroy();
		System.gc();
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		super.startActivityForResult(intent, requestCode);
		// // 页面2左侧进入，页面1左侧退出
		// overridePendingTransition(R.anim.i_slide_in_left,
		// R.anim.i_slide_out_left);
	}

	@Override
	public void startActivity(Intent intent) {
		super.startActivity(intent);
	}

	@Override
	public void finish() {
		finish(true);
	}

	public void finish(boolean hasAnim) {
		super.finish();
		// 页面2进入执行in_right，页面1退出执行out_right
		if (hasAnim)
			overridePendingTransition(R.anim.i_slide_in_right, R.anim.i_slide_out_right);
	}

	/**
	 * 注入过滤Action
	 * 
	 * @param filter
	 */
	protected void inJectFilter(IntentFilter filter) {

	}

	/**
	 * 对注入的Actions回调
	 * 
	 * @param context
	 * @param intent
	 */
	public void onReceived(Context context, Intent intent) {
	}

	public void handleMessaged(Message msg) {
	}

	private Object mTag;
	/**
	 * SparseArray性能比HashMap性能高但不能被序列化，有些item标记后无法在acty页面间来回传值
	 * http://blog.csdn.net/xyz_fly/article/details/7931943
	 */
	// private SparseArray<Object> mTags;
	private HashMap<Integer, Object> mTags;

	public Object getTag() {
		return mTag;
	}

	public void setTag(Object tag) {
		mTag = tag;
	}

	public Object getTag(int key) {
		return (mTags == null) ? null : mTags.get(key);
	}

	public void setTag(int key, Object tag) {
		if (mTags == null) {
			// mTags = new SparseArray<Object>();
			mTags = new HashMap<Integer, Object>();
		}
		mTags.put(key, tag);
	}

	protected void initTitleBar(int id) {
		initTitleBar(id, null, null, null);
	}

	protected void initTitleBar(int id, String title) {
		initTitleBar(id, title, null, null);
	}

	protected void initTitleBar(int id, String title, String left, String right) {
		titleBar = (TitleBar) findViewById(id);
		if (!TextUtils.isEmpty(title)) {
			titleBar.titleTV.setText(title);
		} else {
			titleBar.titleTV.setVisibility(View.GONE);
		}

		if (!TextUtils.isEmpty(left)) {
			titleBar.leftBN.setOnClickListener(this);
			titleBar.leftBN.setText(left);
			titleBar.leftBN.setVisibility(View.VISIBLE);

			titleBar.leftIBN.setVisibility(View.GONE);
		} else {
			titleBar.leftBN.setVisibility(View.GONE);
			titleBar.leftIBN.setOnClickListener(this);
		}

		if (!TextUtils.isEmpty(right)) {
			titleBar.rightBN.setText(right);
			titleBar.rightBN.setOnClickListener(this);
		} else {
			titleBar.rightBN.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.it_ibn_left:
				finish();
				break;
			default:
				break;
		}
	}

	private static RequestQueue mRequestQueue;
	/**
	 * 获取Volley的RequestQueue
	 * @return
	 */
	public static RequestQueue getRequestQueue(Context context) {

		if (mRequestQueue == null) {

			if (mRequestQueue == null) {
				mRequestQueue = Volley.newRequestQueue(context);
			}
		}
		return mRequestQueue;
	}

	@Override
	public void onLocationChanged(AMapLocation aMapLocation) {
		//定位回调
		JKLog.i(TAG, aMapLocation.toString());
		locationLongitude = aMapLocation.getLongitude();
		locationLatitude = aMapLocation.getLatitude();
	}

	/**
	 * 放回经度方法
	 */
	public double returnLocationLongitude(){
		if (locationLongitude == 0){
			initLocation();
			startLocation();
		}
		return locationLongitude;
	}

	/**
	 * 放回纬度方法
	 */
	public double returnLocationLatitude(){
		if (locationLatitude == 0){
			initLocation();
			startLocation();
		}
		return locationLatitude;
	}
}