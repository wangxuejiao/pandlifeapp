package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.utils.DataCache;
import com.jiakang.pandlife.utils.JKLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 11.获取首页今日团/明日预告/拼好货图片接口
 *
 * @author ww
 *
 */
public class GetIndexPicInfo extends BaseAbsInfo {

    private static final String TAG = "GetIndexPicInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private String tuanPic;
    private String tuanType;
    private String tTuanPic;
    private String tTuanType;
    private String xqPic;
    private String xqType;

//    private List<MyIndentItem> allItems = new ArrayList<MyIndentItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Index&a=Getindexpic" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                data = (JSONObject)jsonObject.get("data");
                //缓存数据
                DataCache aCache = DataCache.get(PandLifeApp.getInstance());
                String key = "getIndexPic";
                aCache.put(key,data);

                JSONArray tuanJA = data.getJSONArray("tuan_list");
                tuanPic = tuanJA.getJSONObject(0).getString("pic");
                tuanType = tuanJA.getJSONObject(0).getString("type");

                JSONArray tTuanJA = data.getJSONArray("ttuan_list");
                tTuanPic = tTuanJA.getJSONObject(0).getString("pic");
                tTuanType = tTuanJA.getJSONObject(0).getString("type");

                JSONArray xqJA = data.getJSONArray("xq_list");
                xqPic = xqJA.getJSONObject(0).getString("pic");
                xqType = xqJA.getJSONObject(0).getString("type");

//                allItems.clear();
//                JSONArray taskJA = jsonObject.getJSONArray("data");
//                String itemStr;
//                MyIndentItem myIndentItem;
//                for(int i=0;i<taskJA.length();i++){
//                    itemStr = taskJA.getJSONObject(i).toString();
//                    myIndentItem = BaseInfo.gson.fromJson(itemStr, MyIndentItem.class);
//                    myIndentItem.setOrder_type("5");
//                    allItems.add(myIndentItem);
//                }

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getTuanPic() {
        return tuanPic;
    }

    public void setTuanPic(String tuanPic) {
        this.tuanPic = tuanPic;
    }

    public String getTuanType() {
        return tuanType;
    }

    public void setTuanType(String tuanType) {
        this.tuanType = tuanType;
    }

    public String gettTuanPic() {
        return tTuanPic;
    }

    public void settTuanPic(String tTuanPic) {
        this.tTuanPic = tTuanPic;
    }

    public String gettTuanType() {
        return tTuanType;
    }

    public void settTuanType(String tTuanType) {
        this.tTuanType = tTuanType;
    }

    public String getXqPic() {
        return xqPic;
    }

    public void setXqPic(String xqPic) {
        this.xqPic = xqPic;
    }

    public String getXqType() {
        return xqType;
    }

    public void setXqType(String xqType) {
        this.xqType = xqType;
    }

    //    public List<MyIndentItem> getAllItems() {
//        return allItems;
//    }
//
//    public void setAllItems(List<MyIndentItem> allItems) {
//        this.allItems = allItems;
//    }
}
