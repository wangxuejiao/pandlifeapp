package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 *
 * @author ww
 *
 */
public class SendbagInfo extends BaseAbsInfo {

    private static final String TAG = "SendbagInfo";

    /**收件人信息id	Int(10)*/
    private int sid;
    /**站点id	Int(10)*/
    private int station;
    /**寄件人信息id	Int(10)*/
    private int jid;
    /**快递类型id	Int(10)*/
    private int delivery_id;
    /**物品类型	Varchar(32)*/
    private String bag_kind;
    /**包裹数量	Int(10)*/
    private int nums;
    /**付款方式（d：到付；f：寄付）	Varchar(32)*/
    private String paytype;
    /**保价金额	Int(10)*/
    private int supvalue;
    /**备注信息	Varchar(255)*/
    private String remarks;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Express&a=Sendbag" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("sid", sid);
            json.put("station", station);
            json.put("jid", jid);
            json.put("delivery_id", delivery_id);
            json.put("bag_kind", bag_kind);
            json.put("nums", nums);
            json.put("paytype", paytype);
            json.put("supvalue", supvalue);
            json.put("remarks", remarks);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");


            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getStation() {
        return station;
    }

    public void setStation(int station) {
        this.station = station;
    }

    public int getJid() {
        return jid;
    }

    public void setJid(int jid) {
        this.jid = jid;
    }

    public int getDelivery_id() {
        return delivery_id;
    }

    public void setDelivery_id(int delivery_id) {
        this.delivery_id = delivery_id;
    }

    public String getBag_kind() {
        return bag_kind;
    }

    public void setBag_kind(String bag_kind) {
        this.bag_kind = bag_kind;
    }

    public int getNums() {
        return nums;
    }

    public void setNums(int nums) {
        this.nums = nums;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public int getSupvalue() {
        return supvalue;
    }

    public void setSupvalue(int supvalue) {
        this.supvalue = supvalue;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
