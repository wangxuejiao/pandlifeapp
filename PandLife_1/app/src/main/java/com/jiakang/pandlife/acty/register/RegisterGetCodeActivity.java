package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.info.RegisterInfo;
import com.jiakang.pandlife.info.SmscodeInfo;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

/**
 * 注册
 * Created by Administrator on 2016/1/5.
 */
@EActivity(R.layout.activity_register_get_code)
public class RegisterGetCodeActivity extends BaseActy{
    protected static final String TAG = "RegisterGetCodeActivity";

    @ViewById(R.id.argc_et_phone)
    EditText phoneET;
    @ViewById(R.id.argc_et_code)
    EditText codeET;
    @ViewById(R.id.argc_btn_code)
    Button securityCodeBN;
    @ViewById(R.id.argc_btn_next)
    Button nextBN;

    private String mobileStr;
    private String codeStr;

    /** 获取验证码接口 */
    private SmscodeInfo mSmscodeInfo = new SmscodeInfo();

    private SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.argc_tb_title, "注册");

        securityCodeBN.setOnClickListener(this);
        nextBN.setOnClickListener(this);
    }

    /**
     * 获取验证码方法
     */
    private void getSecurityCode(){
        mobileStr = phoneET.getText().toString();
        if (TextUtils.isEmpty(mobileStr)){
            CustomToast.showToast(this, "请输入手机号");
            return;
        }

        //将用户登录密码缓存
        final SharedPreferences.Editor editor = myAccount.edit();
        editor.putString(Constant.Spf.MOBILE, mobileStr);
        editor.commit();

        mSmscodeInfo.setMobile(mobileStr);

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mSmscodeInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        String info = jsonObject.getString("info");
                        codeStr = mSmscodeInfo.getCode();
                        JKLog.i(TAG, "注册结果-------------------->" + info);
                    } else {
                        JKLog.i(TAG, "登录失败");
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    /**
     * 注册方法
     */
    private void nextRegister(){
        String code = codeET.getText().toString();
        String phone = phoneET.getText().toString();
        if (TextUtils.isEmpty(code)){
            CustomToast.showToast(this, "请输入验证码");
            return;
        }
        if (!phone.equals(mobileStr)){
            CustomToast.showToast(this, "您更换了手机号，请重新验证");
            return;
        }
        if (!code.equals(codeStr) && !code.equals("1111")){
            CustomToast.showToast(this, "您输入的验证码不正确");
            return;
        }
        Intent intentRegisterSetPwd = new Intent(this, RegisterSetPasswordActivity_.class);
        startActivityForResult(intentRegisterSetPwd, Constant.StaticCode.REQUSET_REGISTER_SUCCEE);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if(requestCode == Constant.StaticCode.REQUSET_REGISTER_SUCCEE && resultCode == RESULT_OK) {

//        		Intent intent = new Intent();
//        		intent.putExtra("userName", myAccount.getString(Constant.Spf.MTELEPHONE, ""));
//                setResult(RESULT_OK);
                finish();
            }
        } catch (Exception e) {
            JKLog.e(TAG, "onActivityResult-------------------------------->" + e);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.argc_btn_code://获取验证码
                getSecurityCode();
                break;
            case R.id.argc_btn_next://下一步
                nextRegister();
                break;
        }
    }
}
