package com.jiakang.pandlife.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.itemview.ItemView;
import com.jiakang.pandlife.utils.JKLog;

import java.util.ArrayList;
import java.util.List;

import greendroid.widget.PagedAdapter;
import greendroid.widgetww.OnItemSmoothListener;


public class PageItemAdapter extends PagedAdapter {

	protected String TAG = "PageItemAdapter";
	protected List<Item> mItems;
	protected Context ctx;

	public PageItemAdapter(Context context) {
		this(context, new ArrayList<Item>());
	}

	public PageItemAdapter(Context context, List<Item> items) {
		this.ctx = context;
		this.mItems = items;
	}

	/**
	 * 重新注入List数据，更新数据入口<br>
	 * 1、清除所有item<br>
	 * 2、重新添加Items<br>
	 * 3、notifyDataSetChanged()<br>
	 * 
	 * @param items
	 */
	public void setItems(List<Item> items) {
		if (mItems != items) {
			mItems.clear();
			mItems = items;
		}
	}

	/**
	 * 在原有基础上添加
	 * 
	 * @param items
	 */
	public void addItems(List<Item> items) {
		mItems.addAll(items);
	}

	public void addItem(int index, Item item) {
		mItems.add(index, item);
	}

	public void addItems(int index, List<Item> items) {
		mItems.addAll(index, items);
	}

	public void add(Item item) {
		mItems.add(item);
	}

	public void insert(Item item, int index) {
		mItems.add(index, item);
	}

	public void remove(int itemIndex) {
		mItems.remove(itemIndex);
	}

	public void remove(Item item) {
		if (mItems.remove(item)) {
		} else {
			JKLog.e(TAG, "remove方法中--删除失败------>：item为：" + item);
		}
	}

	public void clear() {
		mItems.clear();
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mItems.size();
	}

	@Override
	public Object getItem(int position) {
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		JKLog.e(TAG, "getView方法中-------->：position为：" + position);
		if (position >= mItems.size()) {
			return null;
		}
		Item item = (Item) getItem(position);
		ItemView itemView;
		if (convertView != null && convertView.getTag() != null && Integer.valueOf(convertView.getTag().toString()) == item.getItemLayoutId()) {
			itemView = (ItemView) convertView;
		} else {
			Log.i(TAG,"---------》dataItem" + item);
			itemView = item.newView(ctx, null);
			itemView.findViewsByIds();
			convertView = (View) itemView;
			convertView.setTag(item.getItemLayoutId());
		}
		if (onItemSmoothListener != null)
			((View) itemView).setTag(R.id.itemadapter_smooth_id, onItemSmoothListener);

		// 绑定数据
		itemView.setObject(item, position, onViewClickListener);

		return (View) itemView;
	}

	protected ItemAdapter.OnViewClickListener onViewClickListener;
	protected OnItemSmoothListener onItemSmoothListener;

	public void setOnSmoothListener(OnItemSmoothListener onItemSmoothListener) {
		this.onItemSmoothListener = onItemSmoothListener;
	}

	public ItemAdapter.OnViewClickListener getOnViewClickListener() {
		return onViewClickListener;
	}

	public void setOnViewClickListener(ItemAdapter.OnViewClickListener onViewClickListener) {
		this.onViewClickListener = onViewClickListener;
	}

}
