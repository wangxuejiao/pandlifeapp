package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.RechargeInfo;
import com.jiakang.pandlife.utils.CustomToast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

/**
 * 账户充值
 * Created by Administrator on 2016/1/14.
 */
@EActivity(R.layout.activity_recharge)
public class RechargeActivity extends BaseActy{
    protected static final String TAG = "RechargeActivity";

    @ViewById(R.id.are_iv_pay_icon)
    ImageView payIconIV;
    @ViewById(R.id.are_tv_pay_name)
    TextView payNameTV;
    @ViewById(R.id.are_tv_pay_content)
    TextView payContentTV;
    @ViewById(R.id.are_tv_note)
    TextView payNoteTV;
    @ViewById(R.id.are_et_sum_input)
    EditText sumInputET;
    @ViewById(R.id.are_iv_delete_sum)
    ImageView deleteSumIV;
    @ViewById(R.id.are_btn_next)
    Button nextBN;

    //记录当前充值方式
    private String currentRechargeWay = "1";
    //记录充值请求后返回的订单号
    private String orid;

    /** 充值接口 */
    private RechargeInfo mRechargeInfo = new RechargeInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.are_tb_title, "账户充值");

        bindView();
    }

    private void bindView(){
        findViewById(R.id.are_rl_pay_type).setOnClickListener(this);
        deleteSumIV.setOnClickListener(this);
        nextBN.setOnClickListener(this);

        // 如果有改动监听
        sumInputET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setDeleteAndEnable();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_RECHARGE_WAY) && (resultCode == RESULT_OK)){
            currentRechargeWay = data.getExtras().getString("rechargeWay");
            notifyRechargeWay();
        }
    }

    // 控制图片的显示和按钮的可否点击
    public void setDeleteAndEnable() {
        if (sumInputET.getText().toString().length() == 0) {
            deleteSumIV.setVisibility(View.GONE);
            nextBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_border_bg));
            nextBN.setEnabled(false);
        } else {
            deleteSumIV.setVisibility(View.VISIBLE);
            nextBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_bg_reddish_orange));
            nextBN.setEnabled(true);
        }
    }

    /**
     * 更新当前选择的充值方式
     */
    private void notifyRechargeWay(){
        if(("1").equals(currentRechargeWay)){
            payIconIV.setImageResource(R.mipmap.icon_iv_recharge_alipay);
            payNameTV.setText("支付宝");
            payContentTV.setText("支付宝安全支付");
            payNoteTV.setText("您一次最多可充值1000.00元");
        }else if(("2").equals(currentRechargeWay)){
            payIconIV.setImageResource(R.mipmap.icon_iv_recharge_wechat);
            payNameTV.setText("微信");
            payContentTV.setText("微信安全支付");
            payNoteTV.setText("您一次最多可充值1000.00元");
        }else if(("3").equals(currentRechargeWay)){
            payIconIV.setImageResource(R.mipmap.icon_iv_recharge_unionpay);
            payNameTV.setText("银联");
            payContentTV.setText("银联安全支付");
            payNoteTV.setText("您一次最多可充值1000.00元");
        }
    }

    /**
     * 充值方法
     */
    private void recharge(){
        String sumStr = sumInputET.getText().toString();
        int sum = 0;
        if (!TextUtils.isEmpty(sumStr.trim())){
            sum = Integer.parseInt(sumStr);
        }
        if (sum == 0){
            CustomToast.showToast(mContext, "您输入的金额有误，请重新输入");
            return;
        }
        mRechargeInfo.setMoney(sum);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mRechargeInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    orid = mRechargeInfo.getOrid();
                    //进行ping++对接

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.are_rl_pay_type:
                Intent intentSelectRechargeWay = new Intent(mContext, SelectRechargeWayActivity_.class);
                intentSelectRechargeWay.putExtra("rechargeWay", currentRechargeWay);
                startActivityForResult(intentSelectRechargeWay, Constant.StaticCode.REQUEST_RECHARGE_WAY);
                break;
            case R.id.are_iv_delete_sum:
                sumInputET.setText("");
                deleteSumIV.setVisibility(View.GONE);
                nextBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_border_bg));
                nextBN.setEnabled(false);
                break;
            case R.id.are_btn_next://点击进行充值
                recharge();
                break;
        }
    }
}
