package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.LoginActivity;
import com.jiakang.pandlife.acty.MainActivity;
import com.jiakang.pandlife.acty.MainActivity_;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.info.RegisterInfo;
import com.jiakang.pandlife.info.SmscodeInfo;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

/**
 * 注册
 * Created by Administrator on 2016/1/5.
 */
@EActivity(R.layout.activity_register_set_password)
public class RegisterSetPasswordActivity extends BaseActy{
    protected static final String TAG = "RegisterSetPasswordActivity";

    @ViewById(R.id.arsp_et_inputpwd)
    EditText inputPwdET;
    @ViewById(R.id.arsp_et_reInputpwd)
    EditText reInputPwdET;
    @ViewById(R.id.arsp_btn_finish)
    Button registerBN;

    private String mobileStr;
    private String codeStr;

    /** 获取验证码接口 */
    private SmscodeInfo mSmscodeInfo = new SmscodeInfo();
    /** 注册接口 */
    private RegisterInfo mRegisterInfo = new RegisterInfo();

    private SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.arsp_tb_title, "注册");

        registerBN.setOnClickListener(this);
    }


    /**
     * 注册方法
     */
    private void register(){
        String password1 = inputPwdET.getText().toString();
        String password2 = reInputPwdET.getText().toString();
        if (TextUtils.isEmpty(password1)){
            CustomToast.showToast(this, "请输入密码");
            return;
        }
        if (TextUtils.isEmpty(password2)){
            CustomToast.showToast(this, "请再次输入密码");
            return;
        }
        if(!TextUtils.isEmpty(LoginActivity.webid) && !TextUtils.isEmpty(LoginActivity.webname) && !TextUtils.isEmpty(LoginActivity.type)){
//            if (TextUtils.isEmpty(LoginActivity.webpic)){
//                LoginActivity.webpic = "";
//            }
            mRegisterInfo.setWebid(LoginActivity.webid);
            mRegisterInfo.setWebname(LoginActivity.webname);
            mRegisterInfo.setWebpic(LoginActivity.webpic);
            mRegisterInfo.setType(LoginActivity.type);
        }
        mobileStr = myAccount.getString(Constant.Spf.MOBILE , "");
        mRegisterInfo.setMobile(mobileStr);
        mRegisterInfo.setPassword(password1);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mRegisterInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        CustomToast.showToast(RegisterSetPasswordActivity.this, "注册成功！");
//                        setResult(RESULT_OK);
                        Intent intentSelectLocationOrManual = new Intent(mContext, SelectLocationOrManualActivity_.class);
                        intentSelectLocationOrManual.putExtra(SelectLocationOrManualActivity.SITE_OR_COMMUNITY , "community_and_site");
                        startActivityForResult(intentSelectLocationOrManual, Constant.StaticCode.REQUSET_BIND_SITE);
//                        finish();
                    } else {
                        JKLog.i(TAG, "登录失败");
                    }
                } catch (Exception e) {

                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUSET_BIND_SITE) && (resultCode == RESULT_OK)){

            Intent intentMain = new Intent(mContext, MainActivity_.class);
            startActivity(intentMain);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.arsp_btn_finish://注册
                register();
                break;
        }
    }
}
