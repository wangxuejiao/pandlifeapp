package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.item.UserItem;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 个人信息修改接口
 *
 * @author ww
 *
 */
public class EditMyInfo extends BaseAbsInfo {

    private static final String TAG = "EditMyInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;
    /** 头像地址	Int(10) */
//    private String head;
    /** 站点id	Int(10) */
    private String station_id;
    /** 小区id	Int(10) */
    private String cid;
    /** 昵称	Varchar(32) */
    private String nick;
    /** 真实姓名	Varchar(32) */
    private String realname;
    /** 身份证号	Varchar(32) */
    private String card_no;
    /** 出生日期	Varchar(32) */
    private String birthday;
    /** 家庭住址	Varchar(255) */
    private String address;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=User&a=Editmyinfo" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
//            json.put("head", head);
            json.put("station_id", station_id);
            json.put("cid", cid);
            json.put("nick", nick);
            json.put("realname", realname);
            json.put("card_no", card_no);
            json.put("birthday", birthday);
            json.put("address", address);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

//    public String getHead() {
//        return head;
//    }
//
//    public void setHead(String head) {
//        this.head = head;
//    }

    public String getStation_id() {
        return station_id;
    }

    public void setStation_id(String station_id) {
        this.station_id = station_id;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
