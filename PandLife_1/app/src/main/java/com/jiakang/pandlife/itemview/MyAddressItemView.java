package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.RedPackageItem;

/**
 * Created by play on 2016/1/6.
 */
public class MyAddressItemView extends AbsRelativeLayout {

//    private MyAddressItemView mMyAddressItemView;
    private TextView nameTV;
    private TextView phoneTV;
    private TextView addressTV;

    public MyAddressItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyAddressItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();
//        mMyAddressItemView = (MyAddressItemView)findViewById(R.id.ima_rl_itemview);
        nameTV = (TextView) findViewById(R.id.ima_tv_name);
        phoneTV = (TextView) findViewById(R.id.ima_tv_phone);
        addressTV = (TextView) findViewById(R.id.ima_tv_address);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        if(item instanceof AddressItem){
            AddressItem addressItem = (AddressItem) item;
            nameTV.setText(addressItem.getName());
            phoneTV.setText(addressItem.getPhone());
            addressTV.setText(addressItem.getAddress());

        }
//        else if(item instanceof SenderAddressItem){
//            SenderAddressItem senderAddressItem = (SenderAddressItem) item;
//            nameTV.setText(senderAddressItem.getName());
//            phoneTV.setText(senderAddressItem.getPhone());
//            addressTV.setText(senderAddressItem.getAddress());
//        }
    }
}
