package com.jiakang.pandlife.info.nearmerchase;

/**
 * Created by Administrator on 2015/12/8.
 */


import android.text.TextUtils;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.info.BaseAbsInfo;
import com.jiakang.pandlife.item.RecommendGoodsItem;
import com.jiakang.pandlife.utils.JKLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 3.商家单个分类产品获取接口
 *
 * @author ww
 *
 */
public class OneCateProductInfo extends BaseAbsInfo {

    private static final String TAG = "OneCateProductInfo";

    /** 商家id	Int(10) */
    private int sid;
    /** 产品分类id	Int(10) */
    private int cid;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;
    /** 页面索引	Int(10) */
    private int pageIndex;
    /** 分页大小	Int(10) */
    private int pageSize;

    //分类商品
    private List<RecommendGoodsItem> allItems = new ArrayList<RecommendGoodsItem>();

    private String totalPage;
    private String pageIndexStr;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Trader&a=Onecateproduct" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("sid", sid);
            json.put("cid", cid);
            json.put("pageIndex", pageIndex);
            json.put("pageSize", pageSize);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                RecommendGoodsItem recommendGoodsItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    recommendGoodsItem = BaseInfo.gson.fromJson(itemStr, RecommendGoodsItem.class);
                    // 入库
//                    goodsKindItem.insert();
                    allItems.add(recommendGoodsItem);
                }

                JSONObject taskJO = (JSONObject)jsonObject.get("page");
                pageIndexStr = taskJO.getString("pageIndex");
                if (!TextUtils.isEmpty(pageIndexStr))
                    pageIndex = Integer.parseInt(pageIndexStr);
                totalPage = taskJO.getString("totalPage");

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<RecommendGoodsItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<RecommendGoodsItem> allItems) {
        this.allItems = allItems;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public String getPageIndexStr() {
        return pageIndexStr;
    }

    public void setPageIndexStr(String pageIndexStr) {
        this.pageIndexStr = pageIndexStr;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }
}
