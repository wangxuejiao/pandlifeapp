package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.ConveniencePhoneItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

import java.util.List;

/**
 * Created by play on 2016/1/6.
 */
public class ConveniencePhoneItemView extends AbsLinearLayout {

    private TextView tvTitle;
    private TextView tvTel;
    private ImageView imageView;

    public ConveniencePhoneItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ConveniencePhoneItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        tvTitle = (TextView) findViewById(R.id.tv_name_convenience_phone);
        tvTel = (TextView) findViewById(R.id.tv_phone_convenience_phone);
        imageView = (ImageView) findViewById(R.id.iv_convenience_phone);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        ConveniencePhoneItem.DataEntity dataEntity = (ConveniencePhoneItem.DataEntity) item;
        tvTitle.setText(dataEntity.getTitle());
        tvTel.setText(dataEntity.getTel());
        ImageLoaderUtil.displayImageCircleCache(dataEntity.getIcon(),imageView,R.mipmap.ic_convenience_phone);
    }
}
