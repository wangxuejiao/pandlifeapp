package com.jiakang.pandlife.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore.MediaColumns;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.widget.CustomDialog;
import com.jiakang.pandlife.widget.DragImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.SoftReference;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * 工具包
 * 
 * @author cwang
 */
public class Util {
	// public static boolean netCheckIsShow = false;
	// public static boolean unitCheckIsShow = false;
	// public static boolean perfectInfoIsShow = false;
	// private static final int[] constellationEdgeDay = { 21, 20, 21, 21, 22,
	// 22, 23, 24, 24, 24, 23, 22 };
	// public static final String[] constellationArr = { "水瓶座", "双鱼座", "白羊座",
	// "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座",
	// "天蝎座", "射手座", "魔羯座" };

	// iphone
	private static final int[] constellationEdgeDay = { 21, 21, 22, 22, 23, 24, 24, 24, 23, 22, 21, 20 };
	public static final String[] constellationArr = { "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "魔羯座", "水瓶座", "双鱼座" };
	public static final String[] constellationArrUser = { "未选", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "魔羯座", "水瓶座", "双鱼座" };

	/**
	 * 将uri转化为BitMap
	 * 
	 * @param uri
	 * @param mContext
	 * @return
	 */
	public static Bitmap decodeUriAsBitmap(Uri uri, Context mContext) {
		Bitmap bitmap = null;
		try {
			bitmap = BitmapFactory.decodeStream(mContext.getContentResolver().openInputStream(uri));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		return bitmap;
	}

	/**
	 * 将uri转化为BitMap
	 * 
	 * @param uri
	 * @param mContext
	 * @return
	 */
	public static Bitmap decodeUriAsBitmap(Uri uri, int width, int height) {
		if (null != uri) {
			try {
				// 主动回收内存
				// System.gc();
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inJustDecodeBounds = true;
				JKLog.i("uri-path000000:", uri.getPath());
				BitmapFactory.decodeFile(uri.getPath(), options);
				int scalWidth = options.outWidth / width;
				int scalHeight = options.outHeight / height;
				if (scalWidth <= 0) {
					scalWidth = 1;
				}
				if (scalHeight <= 0) {
					scalHeight = 1;
				}
				options.inSampleSize = scalWidth;
				options.inJustDecodeBounds = false;
				JKLog.i("uri-path111111:", uri.getPath());
				return BitmapFactory.decodeFile(uri.getPath(), options);
			} catch (Exception e) {
				JKLog.i("uri-:", uri.getPath());
				e.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * 动态计算图片载入内存时比例
	 * 
	 * @param options
	 * @param minSideLength
	 * @param maxNumOfPixels
	 */
	private static int computeInitialSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
		double w = options.outWidth;
		double h = options.outHeight;
		int lowerBound = (maxNumOfPixels == -1) ? 1 : (int) Math.ceil(Math.sqrt(w * h / maxNumOfPixels));
		int upperBound = (minSideLength == -1) ? 128 : (int) Math.min(Math.floor(w / minSideLength), Math.floor(h / minSideLength));
		if (upperBound < lowerBound) {
			return lowerBound;
		}
		if ((maxNumOfPixels == -1) && (minSideLength == -1)) {
			return 1;
		} else if (minSideLength == -1) {
			return lowerBound;
		} else {
			return upperBound;
		}
	}


	/**
	 * 判断当前是否有可用网络，如果没有可让用户进入网络设置页面
	 * 
	 * @param mContext
	 *            上下文
	 * @return true 有可用网络，false 没有可用网络
	 */
	public static boolean hasNet(final Context mContext) {
		return hasNet(mContext, true);
	}

	/**
	 * 判断当前是否有可用网络 全局网络状态，受广播控制
	 * 
	 * @param mContext
	 * @param showDlg
	 *            是否提示对话框
	 * @return true 有可用网络，false 没有可用网络
	 */
	public static boolean hasNet(final Context mContext, boolean showDlg) {
		// 如果没有网络，再查一遍确保没网络
		if (!BaseInfo.hasNet) {
			if (hasNetByQuery(mContext)) {
				return true;
			} else {
				if (showDlg)
					showNoNetDlg(mContext);
				// 不显示提示对话框，则直接返回false
				return false;
			}
		} else {
			return true;
		}
	}

	/**
	 * 通过遍历查询是否有网络
	 * 
	 * @param mContext
	 * @return boolean
	 */
	public static boolean hasNetByQuery(Context mContext) {
		ConnectivityManager cwjManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] info = cwjManager.getAllNetworkInfo();
		if (info != null) {
			for (int i = 0; i < info.length; i++) {
				if (info[i].getState() == NetworkInfo.State.CONNECTED) {
					BaseInfo.hasNet = true;
					return true;
				}
			}
		}
		BaseInfo.hasNet = false;
		return false;
	}

	/**
	 * 没用网络
	 * 
	 * @param mContext
	 */
	public static void showNoNetDlg(final Context mContext) {
		// 对话框创建
		final CustomDialog customDialog = Util.getDialog(mContext, "提示", "没有可用的网络，是否对网络进行设置？", "是", "否");
		customDialog.setOnClickListener(R.id.ok, new OnClickListener() {
			@Override
			public void onClick(View v) {
				customDialog.dismiss();
				try {
					if (android.os.Build.VERSION.SDK_INT > 10) {
						// 3.0以上打开设置界面，也可以直接用ACTION_WIRELESS_SETTINGS打开到wifi界面
						mContext.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
					} else {
						mContext.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
					}
				} catch (Exception e) {
					e.printStackTrace();
					CustomToast.showToast(mContext, "打开网络设置失败！", 2000);
				}

			}
		});
		customDialog.setOnClickListener(R.id.cancel, new OnClickListener() {
			@Override
			public void onClick(View v) {
				customDialog.dismiss();
			}
		});
		customDialog.show();
	}

	public static boolean readSIMCard(Context context) {
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);// 取得相关系统服务
		StringBuffer sb = new StringBuffer();
		switch (tm.getSimState()) { // getSimState()取得sim的状态 有下面6中状态
		case TelephonyManager.SIM_STATE_ABSENT:
			sb.append("无卡");
			break;
		case TelephonyManager.SIM_STATE_UNKNOWN:
			sb.append("未知状态");
			break;
		case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
			sb.append("需要NetworkPIN解锁");
			break;
		case TelephonyManager.SIM_STATE_PIN_REQUIRED:
			sb.append("需要PIN解锁");
			break;
		case TelephonyManager.SIM_STATE_PUK_REQUIRED:
			sb.append("需要PUK解锁");
			break;
		case TelephonyManager.SIM_STATE_READY:
			sb.append("良好");
			break;
		}
		if (tm.getSimSerialNumber() == null) {
			sb.append("@无法取得SIM卡号");
			return false;
		}
		if (tm.getNetworkOperator().equals("")) {
			sb.append("@无法取得网络运营商");
			return false;
		}
		if (tm.getNetworkOperatorName().equals("")) {
			sb.append("@无法取得网络运营商名称");
			return false;
		}
		if (tm.getNetworkType() == 0) {
			sb.append("@无法取得网络类型");
			return false;
		}
		return true;
	}

	public static String getDefaultTime(String str) {
		String result = "刚刚";
		if (str.equals("null")) {
			return "无上传时间";
		}
		long currenttime = Calendar.getInstance().getTimeInMillis();
		long oldtime = Long.parseLong(str);
		long time = (currenttime - oldtime) / 3600000;
		if (time >= 1 && time < 24) {
			result = time + "小时前";
		} else if (time >= 24 && time < 24 * 7) {
			result = time / 24 + "天前";
		} else if (time >= (24 * 7) && time < (24 * 30)) {
			result = time / (24 * 7) + "周前";
		} else if (time >= (24 * 30) && time < (24 * 30 * 12)) {
			result = time / (24 * 30) + "月前";
		} else if (time >= (24 * 30 * 12)) {
			result = time / (24 * 30 * 12) + "年前";
		}
		return result;
	}

	/**
	 * 关闭进程,退出本程序
	 * 
	 * @param context
	 */
	// public static void killAll(Context context) {
	// try {
	// // 取消所有通知
	// BaseInfo.mNotificationManager.cancelAll();
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// try {
	// context.stopService(new Intent("im_matchmacke_action_service"));
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// try {
	// // 关闭数据库
	// BaseInfo.dbManager.close();
	// // BaseInfo.dbManager.closeDatabase(null);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// try {
	// XYLog.d("Util", "begin to close database...............");
	// BaseInfo.dbAreaManager.closeDB();
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// logout(context);
	// destroyData(context);
	//
	// // 杀进程
	// android.os.Process.killProcess(android.os.Process.myPid());
	// System.exit(10);
	// }

	public static boolean compareCurrentTime(String time1, String time2) {
		boolean result = false;
		long t1 = Long.parseLong(time1);
		long t2 = Long.parseLong(time2);
		if (t1 > t2) {
			result = true;
		}
		return result;
	}

	/**
	 * 默认位置空
	 * 
	 * @param str
	 * @return
	 */
	public static String getDefaultPlace(String str) {
		if (null == str || str.equals("")) {
			return "";
		}
		return str;
	}

	/**
	 * 默认内容空
	 * 
	 * @param str
	 * @return
	 */
	public static String getDefaultNull(String str) {
		if (null == str || str.equals("")) {
			return "";
		}
		return str;
	}

	/**
	 * 默认数字空
	 * 
	 * @param str
	 * @return
	 */
	public static String getDefaultNumber(String str) {
		if (null == str || str.equals("")) {
			return "";
		}
		return str;
	}

	/**
	 * 默认数字空
	 * 
	 * @param str
	 * @return
	 */
	public static String getDefault0(String str) {
		if (null == str || str.equals("") || str.equals("0")) {
			return "";
		}
		return str;
	}

	/**
	 * 默认数字空
	 * 
	 * @param str
	 * @return
	 */
	public static String getDefault(String str) {
		if (null == str || str.equals("")) {
			return "";
		}
		return str;
	}

	/**
	 * 默认数字空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean checkPetName(String str) {
		if (null == str || str.equals("")) {
			return false;
		}
		return true;
	}

	/**
	 * 默认数字空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean checkInt(int i) {
		if (0 == i) {
			return false;
		}
		return true;
	}

	/**
	 * 远程获取图片
	 * 
	 * @param b
	 * @param degrees
	 * @return
	 */
	public static Bitmap rotate(Bitmap b, int degrees) {
		if (degrees != 0 && b != null) {
			Matrix m = new Matrix();
			m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
			try {
				Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
				if (b != b2) {
					b.recycle(); // Android开发网再次提示Bitmap操作完应该显示的释放
					b = b2;
				}
			} catch (OutOfMemoryError ex) {
				// Android123建议大家如何出现了内存不足异常，最好return 原始的bitmap对象。.
			}
		}
		return b;
	}

	public static boolean bitmapToFile(String path, Bitmap bit) {
		OutputStream outputStream = null;
		try {
			// 写入的文件路径
			outputStream = new FileOutputStream(path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		bit.compress(CompressFormat.JPEG, 100, outputStream);
		try {
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static String dateToStr(String str) {
		long oldtime = Long.parseLong(str);
		Date olDate = new Date(oldtime);
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		String oldDateString = formatter.format(olDate);
		return oldDateString;
	}

	public static Matcher matcherFace(String strFace) {
		String strPattern = "\\[[0-1][0-9][0-9]\\]";
		Pattern p = Pattern.compile(strPattern);
		return p.matcher(strFace);
	}

	/**
	 * 检查密码是否过于简单
	 * 
	 * @param strFace
	 * @return
	 */
	public static boolean checkPassWordIsSimple(String strFace) {
		String strPattern = "^888888$";
		Pattern p = Pattern.compile(strPattern);
		Matcher matcher = p.matcher(strFace);
		boolean find = matcher.find();
		return find;
	}

	/**
	 * 获取字符串长度
	 * 
	 * @param str
	 * @return
	 */
	public static int getStringLen(String str) {
		byte b[];
		try {
			b = str.getBytes("gb2312");
		} catch (UnsupportedEncodingException e) {
			b = str.getBytes();
			e.printStackTrace();
		}
		return b.length;
	}

	public static String urlEncode(String str, String codeName) {
		try {
			return URLEncoder.encode(str, codeName);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean checkEmail(String email) {
		// 验证邮箱的正则表达式
		// String format =
		// "\\p{Alpha}\\w{2,15}[@][a-z0-9]{3,}[.]\\p{Lower}{2,}";
		// String format
		// ="^［a-zA-Z］［\\w\\.-］*［a-zA-Z0-9］@［a-zA-Z0-9］［\\w\\.-］*［a-zA-Z0-9］\\.［a-zA-Z］［a-zA-Z\\.］*［a-zA-Z］$";
		String format = "^([a-zA-Z0-9_\\.-])+@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$";
		// String format =
		// "^［a-zA-Z］［\\w\\.-］*［a-zA-Z0-9］@［a-zA-Z0-9］［\\w\\.-］*［a-zA-Z0-9］\\.［a-zA-Z］［a-zA-Z\\.］*［a-zA-Z］$";
		Pattern p = Pattern.compile(format);
		Matcher m = p.matcher(email);
		return m.matches();
	}

	public static boolean checkPassword(String password) {
		// 验证密码的正则表达式
		// String format =
		// "\\p{Alpha}\\w{2,15}[@][a-z0-9]{3,}[.]\\p{Lower}{2,}";
		if (password.length() < 6) {
			return false;
		} else {
			String format = "^([a-zA-Z0-9_\\.-])";
			Pattern p = Pattern.compile(format);
			Matcher m = p.matcher(password);
			return m.matches();
		}
	}

	/**
	 * 匹配号码是否是11为数字
	 * 
	 * @param phoneNum
	 * @return 不是11位数字，返回false
	 */
	public static boolean checkPhoneNum(String phoneNum) {
		// 验证密码的正则表达式
		// String format =
		// "\\p{Alpha}\\w{2,15}[@][a-z0-9]{3,}[.]\\p{Lower}{2,}";
		// String format = "^((13[0-9])|(15[^4,\\D])|(18[0,2,5-9]))\\d{8}$";
		String format = "^\\d{11}$";// ^\\d{11}$ ^1\\d{10}$
		Pattern p = Pattern.compile(format);
		Matcher m = p.matcher(phoneNum);
		return m.matches();
	}

	/**
	 * 匹配是否为合格姓名
	 *
	 */
	public static boolean checkName(String name){
		String format = "[\u4e00-\u9fa5a-zA-Z]{2,8}";
		Pattern p = Pattern.compile(format);
		Matcher m = p.matcher(name);
		return m.matches();
	}

	/**
	 * @return String 根据时间序列生成ID编号
	 */
	public static String getId() {
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String strTime = sFormat.format(new Date());
		int strMath = 10000 + (int) (Math.random() * (99999 - 10000));
		String str = String.valueOf(strMath);
		String id = strTime + str;
		return id;
	}

	/**
	 * 将毫秒转换成特定格式
	 * @param pattern
	 * @param dateTime
	 * @return
	 */
	public static String getFormatedDateTime(String pattern, long dateTime) {
		SimpleDateFormat sDateFormat = new SimpleDateFormat(pattern);
		return sDateFormat.format(new Date(dateTime + 0));
	}

	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static String getTime() {
		return getTime("yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 获取今天的时间 自定义格式<br>
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static String getTime(String format) {
		return getTime(format, new Date());
	}

	/**
	 * @param format
	 * @param date
	 * @return
	 */
	public static String getTime(String format, Date date) {
		SimpleDateFormat sDateFormat = new SimpleDateFormat(format);
		String dateStr = sDateFormat.format(date);
		return dateStr;
	}

	/**
	 * 获取n月之前或之后的日期
	 * 
	 * @param date
	 * @param nMonth
	 * @param type
	 *            (只能为-1或1)
	 * @return
	 */
	public static Date getDateMonth(Date date, int nMonth, int type) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int nYear = nMonth / 12;
		int month = nMonth % 12;
		calendar.add(Calendar.YEAR, nYear * type);
		Date desDate = calendar.getTime();
		calendar.add(Calendar.MONTH, month * type);
		if (type < 0) {
			while (!calendar.getTime().before(desDate)) {
				calendar.add(Calendar.YEAR, type);
			}
		} else {
			while (!calendar.getTime().after(desDate)) {
				calendar.add(Calendar.YEAR, type);
			}
		}
		return calendar.getTime();
	}

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
	private static SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd HH:mm");
	public static SimpleDateFormat sdf3 = new SimpleDateFormat("MM月dd日");
	/** "yyyy-MM-dd HH:mm" */
	public static SimpleDateFormat sdf_HH_mm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	/** "yyyy-MM-dd" */
	public static SimpleDateFormat sdf_yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat todayDateFormat = new SimpleDateFormat("HH:mm");
	private static int oneDatesTimes = 24 * 60 * 60 * 1000;
	private static int twoDatesTimes = 2 * oneDatesTimes;



	public static String isToday(Date date) {
		String strDate = "";
		Date dateTemp = new Date();
		dateTemp.setHours(0);
		dateTemp.setMinutes(0);
		dateTemp.setSeconds(0);
		if (date.getTime() - dateTemp.getTime() <= oneDatesTimes && date.getTime() - dateTemp.getTime() >= 0) {
			strDate = "今天\t" + todayDateFormat.format(date);
		}
		return strDate;
	}

	public static String isYesterday(Date date) {
		String strDate = "";
		Date dateTemp = new Date();
		dateTemp.setDate(dateTemp.getDate() - 1);
		dateTemp.setHours(0);
		dateTemp.setMinutes(0);
		dateTemp.setSeconds(0);
		if (date.getTime() - dateTemp.getTime() <= twoDatesTimes && date.getTime() - dateTemp.getTime() >= 0) {
			strDate = "昨天\t" + todayDateFormat.format(date);
		}
		return strDate;
	}

	public static String openGroupDate(Date date) {
		String strDate = "";
		strDate = todayDateFormat.format(date);
		return strDate;
	}

	public static String otherDate(Date date) {
		String strDate = "";
		strDate = sdf2.format(date);
		return strDate;
	}

	public static String getCloseTime(Date date) {
		String strDate = "";
		strDate = sdf3.format(date);
		return strDate;
	}

	/**
	 * 得到离报名还有多少时间
	 * 
	 * @param startTime
	 * @param endTime
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static String dateDiff(String startTime, String endTime, String format) throws ParseException {
		// 按照传入的格式生成一个simpledateformate对象
		SimpleDateFormat sd = new SimpleDateFormat(format);
		long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
		long nh = 1000 * 60 * 60;// 一小时的毫秒数
		long nm = 1000 * 60;// 一分钟的毫秒数
		long ns = 1000;// 一秒钟的毫秒数long diff;try {
		// 获得两个时间的毫秒时间差异
		long diff = sd.parse(endTime).getTime() - sd.parse(startTime).getTime();
		long day = diff / nd;// 计算差多少天
		long hour = diff % nd / nh;// 计算差多少小时
		long min = diff % nd % nh / nm;// 计算差多少分钟
		long sec = diff % nd % nh % nm / ns;// 计算差多少秒//输出结果
		String msg = "";

		if (day != 0) {
			msg += day + "天";
		}
		if (hour != 0) {
			msg += hour + "小时";
		}
		if (min != 0) {
			msg += min + "分钟";
		}
		return msg;
	}

	/**
	 * 得到离团购还有多少时间-hour
	 *
	 * @param diffTime
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static String dateDiffGroupPurchase(long diffTime) {
		long nd = 1000 * 24 * 60 * 60;// 一天的毫秒数
		long nh = 1000 * 60 * 60;// 一小时的毫秒数
		long nm = 1000 * 60;// 一分钟的毫秒数
		long ns = 1000;// 一秒钟的毫秒数long diff;try {

		long day = diffTime / nd;// 计算差多少天
		long hour = diffTime % nd / nh;// 计算差多少小时
		long min = diffTime % nd % nh / nm;// 计算差多少分钟
		long sec = diffTime % nd % nh % nm / ns;// 计算差多少秒//输出结果
		String msg = "";

//		if (day != 0) {
//
//			msg += day + ":";
//		}
		if (hour != 0) {
			if (hour <10){
				msg += ("0"+hour+":");
			}else {
				msg += (hour + ":");
			}
		}
		if (min != 0) {
			if (hour <10){
				msg += ("0"+min+":");
			}else {
				msg += (min + ":");
			}
		}
		if (sec != 0){
			if (hour <10){
				msg += ("0"+sec);
			}else {
				msg += sec ;
			}
		}
		return msg;
	}

	public static Date getDate(String time) {
		return getDate(sdf, time);
	}

	public static Date getDateDis(String time) {
		return getDate(sdf_HH_mm, time);
	}

	public static Date getDate(SimpleDateFormat sdf, String time) {
		try {
			Date date = sdf.parse(time);
			return date;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * "yyyy-MM-dd HH:mm:SS"
	 * 
	 * @param currentTimeMilli
	 * @return String
	 */
	public static String getFormatTime(Long currentTimeMilli) {
		try {
			return getIntelligentizeDate(new Date(currentTimeMilli));
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * 
	 * @param time
	 * @return String
	 */
	public static String getFormatTime(String time) {
		String resutl = "";
		if (TextUtils.isEmpty(time))
			return resutl;

		Date date;
		try {
			date = sdf.parse(time);
			resutl = getIntelligentizeDate(date);
		} catch (ParseException e) {
			JKLog.e("Util", "getFormatTime方法中-------->：e为：" + e);
		}
		return resutl;
	}

	/**
	 * 获取智能化时间
	 * @param time
	 * @return String
	 */
	public static String getIntelligentizeDate(long time) {
		if (time == 0) {
			return "";
		}
		return getIntelligentizeDate(new Date(time));
	}
	/**
	 * 智能化获取时间
	 * 
	 * @param date
	 * @return String
	 */
	public static String getIntelligentizeDate(Date date) {
		String resutl;
		resutl = isToday(date);
		if (!TextUtils.isEmpty(resutl)) {
			return resutl;
		}
		resutl = isYesterday(date);
		if (!TextUtils.isEmpty(resutl)) {
			return resutl;
		}
		resutl = otherDate(date);
		if (!TextUtils.isEmpty(resutl)) {
			return resutl;
		}
		return resutl;
	}

	/**
	 * itemView中时间, 过滤空或null情况，优先substring(5, 16)，再formatime<br>
	 * 
	 * @param time
	 * @return
	 */
	public static String getMsgTime(String time) {
		if (!TextUtils.isEmpty(time)) {
			return Util.getFormatTime(time);
		}
		return "";
	}

	public static void ChangColor(String string, TextView v, int end) {
		SpannableStringBuilder style = new SpannableStringBuilder(string);
		style.setSpan(new ForegroundColorSpan(Color.GRAY), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		v.setText(style);
	}

	// public static void remove(String content,String time) {
	// String string = memberList.getPetName();
	// if (IMData.talkTo.size() == 0) {
	// IMData.talkTo.add(memberList);
	// } else {
	// boolean flag = true;
	// for (int i = 0; i < IMData.talkTo.size(); i++) {
	// if (string.equals(((MemberList) IMData.talkTo.get(i))
	// .getPetName())) {
	// flag = false;
	// }
	// }
	// if (flag) {
	// IMData.talkTo.add(memberList);
	// }
	// }
	// }


	/**
	 * 显示dialog
	 * 
	 * @param context
	 * @param resource
	 * @param params
	 * @param clickListen
	 *            如果有匹配params【i]默认设置 监听
	 */
	// public static void showMyDialog(Context context, int resource, String[]
	// params,
	// DialogInterface.OnClickListener[] clickListen) {
	//
	// AlertDialog.Builder build = new AlertDialog.Builder(context);
	// if (resource != 0) {
	// build.setIcon(resource);
	// }
	// if (null != params) {
	// if (null != params[0]) {
	// build.setTitle(params[0]);
	// }
	// if (null != params[1]) {
	// build.setMessage(params[1]);
	// }
	// // params[2]参数默认不为null
	// if (null != clickListen && null != clickListen[0]) {
	// build.setPositiveButton(params[2], clickListen[0]);
	// }
	// if (null != params[3]) {
	// build.setNegativeButton(params[3], clickListen[1]);
	// }
	// build.create().show();
	// }
	// }

	/**
	 * 重载<br>
	 * 带有确认(R.id.ok)和取消(R.id.cancel)按钮的自定义对话框 捕获按钮可以隐藏
	 * 
	 * @param con
	 * @param titleId
	 * @param msgId
	 * @param okStr
	 *            ： 为null则隐藏掉此button（确定）
	 * @param cancelStr
	 *            ： 为null则隐藏掉此button（取消）
	 * @return CustomDialog
	 */
	public static CustomDialog getDialog(Context con, int titleId, int msgId, String okStr, String cancelStr) {
		String title = con.getResources().getString(titleId);
		String msg = con.getResources().getString(msgId);
		return getDialog(con, title, msg, okStr, cancelStr);
	}

	/**
	 * 带有确认(R.id.ok)和取消(R.id.cancel)按钮的自定义对话框 捕获按钮可以隐藏
	 * 
	 * @param con
	 * @param title
	 * @param msg
	 * @param okStr
	 *            ： 为null则隐藏掉此button（确定）
	 * @param cancelStr
	 *            ： 为null则隐藏掉此button（取消）
	 * @return CustomDialog
	 */
	public static CustomDialog getDialog(Context con, String title, String msg, String okStr, String cancelStr, int msgGravity) {
		return new CustomDialog(con, title, msg, okStr, cancelStr, msgGravity);
	}

	public static CustomDialog getDialog(Context con, String title, String msg, String okStr, String cancelStr) {
		return getDialog(con, title, msg, okStr, cancelStr, Gravity.CENTER);
	}


//	private static Dialog imgBigDlg;
//	private static ImageView imageView;
//	private static ProgressBar progressBar;

	public static Dialog getImgBigSeeDlg(final Context mContext) {
		final Dialog imgBigDlg = new Dialog(mContext, R.style.MyDialog);//
		imgBigDlg.setContentView(R.layout.item_preview_picture);

		Window window = imgBigDlg.getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		params.width = RelativeLayout.LayoutParams.FILL_PARENT;
		params.height = RelativeLayout.LayoutParams.FILL_PARENT;
		params.gravity = Gravity.CENTER;
		window.setAttributes(params);

		// 此处可以设置dialog显示的位置
		window.setGravity(Gravity.CENTER);

		// 添加动画
		// window.setWindowAnimations(R.style.ScaledDialog);
		ImageView imageView = (ImageView) imgBigDlg.findViewById(R.id.ipp_iv_image);
		imageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				imgBigDlg.dismiss();
			}
		});
		return imgBigDlg;
		
	}
	
	/**
	 * 显示对话框
	 * @param imgBigDlg
	 * @param source
	 * @param small
	 * @return void
	 */
	public static void showImgBigSeeDlg(final Dialog imgBigDlg, String source, String small){
		final ProgressBar progressBar = (ProgressBar) imgBigDlg.findViewById(R.id.ipp_pb_circle);
		final ImageView imageView = (ImageView) imgBigDlg.findViewById(R.id.ipp_iv_image);
		progressBar.setVisibility(View.VISIBLE);
		SyncImageLoader syncImageLoader = PandLifeApp.getInstance().getSyncImageLoader();
		// 缩略图先上
		syncImageLoader.loadOriginalImage(Util.getUrl(small), new SyncImageLoader.OnloadImage() {
			@Override
			public void loadFinish(Bitmap bit) {
				imageView.setScaleType(ScaleType.CENTER);
				imageView.setImageBitmap(bit);

			}
			@Override
			public void loadFail(boolean hasNet) {
				// progressBar.setVisibility(View.GONE);
			}
		});

		// 大图异步上
		syncImageLoader.loadOriginalImage(Util.getUrl(source), new SyncImageLoader.OnloadImage() {
			@Override
			public void loadFinish(Bitmap b) {
				progressBar.setVisibility(View.GONE);
				imageView.setScaleType(ScaleType.FIT_CENTER);
				imageView.setImageBitmap(b);

			}

			@Override
			public void loadFail(boolean hasNet) {
				progressBar.setVisibility(View.GONE);
				Toast.makeText(imgBigDlg.getContext(), "下载失败！", 1000).show();
			}
		});
		if(imgBigDlg != null && imgBigDlg.isShowing()){
			imgBigDlg.dismiss();
		}
		imgBigDlg.show();
	}
	
	private static int state_height = 0;
	
	public static void showImgBigSeeDlg2(final Activity acty, final Dialog imgBigDlg, String source, String small){
//		final Activity acty = ((Activity)imgBigDlg.getContext());
		WindowManager manager = acty.getWindowManager();
		final int window_width = manager.getDefaultDisplay().getWidth();
		final int window_height = manager.getDefaultDisplay().getHeight();
		state_height = 0;
		final ProgressBar progressBar = (ProgressBar) imgBigDlg.findViewById(R.id.ipp_pb_circle);
		final DragImageView dragImageView = (DragImageView) imgBigDlg.findViewById(R.id.ipp_div_dragImgView);
		imgBigDlg.findViewById(R.id.ipp_pb_circle).setVisibility(View.GONE);
		
		dragImageView.setVisibility(View.VISIBLE);
		progressBar.setVisibility(View.VISIBLE);
		SyncImageLoader syncImageLoader = PandLifeApp.getInstance().getSyncImageLoader();
		// 缩略图先上
		syncImageLoader.loadOriginalImage(Util.getUrl(small), new SyncImageLoader.OnloadImage() {
			@Override
			public void loadFinish(Bitmap bit) {
				dragImageView.setScaleType(ScaleType.CENTER);
				dragImageView.setImageBitmap(bit);
				
			}
			@Override
			public void loadFail(boolean hasNet) {
				// progressBar.setVisibility(View.GONE);
			}
		});
		
		// 大图异步上
		syncImageLoader.loadOriginalImage(Util.getUrl(source), new SyncImageLoader.OnloadImage() {
			@Override
			public void loadFinish(Bitmap curBitmap) {
				progressBar.setVisibility(View.GONE);
				dragImageView.setScaleType(ScaleType.FIT_CENTER);
				dragImageView.setImageBitmap(curBitmap);
				
				Bitmap bmp = scaleBitmap(curBitmap, window_width, window_height);
				dragImageView.setImageBitmap(bmp);
				dragImageView.setmActivity(acty);
				// dragImageView.setId(R.id.pagedview_dragImg_id);
				dragImageView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						imgBigDlg.dismiss();
					}
				});
				// setContentView(dragImageView);
				/** 测量状态栏高度 **/
				ViewTreeObserver viewTreeObserver = dragImageView.getViewTreeObserver();
				viewTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {
						if (state_height == 0) {
							// 获取状况栏高度
							Rect frame = new Rect();
							acty.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
							state_height = frame.top;
							dragImageView.setScreen_H(window_height - state_height);
							dragImageView.setScreen_W(window_width);
						}

					}
				});
				
				
			}
			
			@Override
			public void loadFail(boolean hasNet) {
				progressBar.setVisibility(View.GONE);
				Toast.makeText(imgBigDlg.getContext(), "下载失败！", 1000).show();
			}
		});
		if(imgBigDlg != null && imgBigDlg.isShowing()){
			imgBigDlg.dismiss();
		}
		imgBigDlg.show();
	}
	
	
	/***
	 * 等比例压缩图片
	 * 
	 * @param bitmap
	 * @param screenWidth
	 * @param screenHight
	 * @return
	 */
	public static Bitmap scaleBitmap(Bitmap bitmap, int screenWidth, int screenHight) {
		if (bitmap == null)
			return null;
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		Log.e("jj", "图片宽度" + w + ",screenWidth=" + screenWidth);
		Matrix matrix = new Matrix();
		float scale = (float) screenWidth / w;
		float scale2 = (float) screenHight / h;

		// scale = scale < scale2 ? scale : scale2;

		// 保证图片不变形.
		matrix.postScale(scale, scale);
		// w,h是原图的属性.
		return Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
	}
	

	public static String getUrl(String small) {
		return Constant.downalImageUrl + small;
	}

	/**
	 * 创建圆形图片 不考虑适配 设置固定值 拍照头像过大待设置规则
	 * 
	 * @param bitmap
	 * @return
	 */
	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
		if (null != bitmap) {
			/*
			 * Bitmap bit = null; if(bitmap.getHeight()!=bitmap.getWidth()){ int
			 * value =
			 * bitmap.getWidth()>=bitmap.getHeight()?bitmap.getWidth():bitmap
			 * .getHeight(); bit = bitmap.createScaledBitmap(bitmap, 65, 65,
			 * false); }else{ bit = bitmap; }
			 */
			Bitmap bit = bitmap.createScaledBitmap(bitmap, 65, 65, false);
			Bitmap output = Bitmap.createBitmap(bit.getWidth(), bit.getHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(output);

			final int color = 0xff424242;
			final Paint paint = new Paint();
			// int value =
			// bitmap.getHeight()>=bitmap.getWidth()?bitmap.getWidth():bitmap.getHeight();
			// final Rect rect = new Rect(0, 0, value, value);
			final Rect rect = new Rect(0, 0, bit.getHeight(), bit.getWidth());
			// final Rect rect = new Rect(0, 0, 58, 58);
			final RectF rectF = new RectF(rect);
			// 裁剪半径

			final float roundPx = bit.getHeight() > bit.getWidth() ? bit.getWidth() / 2 - 2 : bit.getHeight() / 2 - 2;
			// final float roundPx = 28;

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

			paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
			canvas.drawBitmap(bit, rect, rect, paint);
			return output;
		}
		return null;
	}


	public static boolean isInputShow(final Context con) {
		// InputMethodManager imm =
		// (InputMethodManager)con.getSystemService(Context.INPUT_METHOD_SERVICE);
		// boolean isOpen=imm.isActive();//isOpen若返回true，则表示输入法打开

		return ((Activity) con).getWindow().getAttributes().softInputMode == WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED;
	}

	public static void closeInput(final Context con) {
		InputMethodManager imm = (InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE);
		// 判断隐藏软键盘是否弹出
		// if (((Activity) con).getWindow().getAttributes().softInputMode ==
		// WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED) {
		// // 隐藏软键盘
		// ((Activity)
		// con).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		// ((Activity) con).getWindow().getAttributes().softInputMode =
		// WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED;
		// }
		// 调用隐藏系统默认的输入法
		// imm.hideSoftInputFromWindow(((Activity)
		// con).getCurrentFocus().getWindowToken(),
		// InputMethodManager.HIDE_NOT_ALWAYS);
		// 强制隐藏键盘
		imm.hideSoftInputFromWindow(((Activity) con).getCurrentFocus().getWindowToken(), 0);
	}

	public static void showInput(final Context con, EditText contentET) {
		InputMethodManager imm = (InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(contentET, 0);
	}

	/**
	 * 显示软键盘
	 * 
	 * @return void
	 */
	public static void showInput(final Context con) {
		Timer timer = new Timer(); // 设置定时器
		timer.schedule(new TimerTask() {
			@Override
			public void run() { // 弹出软键盘的代码
				InputMethodManager imm = (InputMethodManager) con.getSystemService(Context.INPUT_METHOD_SERVICE);
				// imm.showSoftInput(eText, InputMethodManager.RESULT_SHOWN);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
				// imm.toggleSoftInput(0,
				// InputMethodManager.HIDE_NOT_ALWAYS);//隐藏显示交替
			}
		}, 300); // 设置300毫秒的时长
	}

	public static void autoInput(final EditText edit, int delay) {
		edit.setFocusable(true);
		edit.setFocusableInTouchMode(true);
		edit.requestFocus();
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				InputMethodManager inputManager = (InputMethodManager) edit.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.showSoftInput(edit, 0);
			}
		}, delay);
		return;
	}

	/**
	 * 根据日期获取星座
	 * 
	 * @param time
	 * @return
	 */
	// public static String date2Constellation(int month, int day) {
	// if (day <= constellationEdgeDay[month]) {
	// month = month - 1;
	// }
	// if (month >= 0) {
	// return constellationArr[month];
	// }
	// // default to return 魔羯
	// return constellationArr[11];
	// }

	/**
	 * 根据日期获取星座下标
	 * 
	 * @param month
	 * @param day
	 * @return
	 */
	// public static int date2ConstellationPos(int month, int day) {
	// if (day <= constellationEdgeDay[month]) {
	// month = month - 1;
	// }
	// if (month >= 0) {
	// return month;
	// }
	// return 11;
	// }

	public static String getClientUUID(Context mContext) {
		final TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
		String clientUUID = tm.getDeviceId();
		return clientUUID == null ? "" : clientUUID;
	}

	/**
	 * 根据路径获取Drawable
	 * 
	 * @param path
	 * @return
	 */
	public static Drawable getDrawableBySDPath(String path) {
		// SoftReference<Bitmap> bitmap = null;
		// try {
		File file = new File(path);
		if (file.exists()) {
			// 校验文件完整性
			BitmapFactory.Options opts = new BitmapFactory.Options();
			// // opts.inTempStorage = new byte[16 * 1024];
			//
			// opts.inSampleSize = 2;// 1/2
			// InputStream fIn = new FileInputStream(file);
			// bitmap = new SoftReference<Bitmap>(BitmapFactory.decodeStream(
			// fIn, null, opts));
			// if (fIn != null) {
			// fIn.close();
			// }
			opts.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path, opts);

			opts.inSampleSize = computeSampleSize(opts, -1, 128 * 128);
			opts.inJustDecodeBounds = false;
			try {
				Bitmap bmp = BitmapFactory.decodeFile(path, opts);
				if (bmp != null) {
					SoftReference<Bitmap> softBitmap = new SoftReference<Bitmap>(bmp);
					return new BitmapDrawable(softBitmap.get());
					// return new BitmapDrawable(bmp);
				}
			} catch (OutOfMemoryError err) {
				err.printStackTrace();
			}
		}
		// } catch (FileNotFoundException e) {
		// e.printStackTrace();
		// } catch (IOException e) {
		// e.printStackTrace();
		// }

		// if (bitmap != null) {
		// return new BitmapDrawable(bitmap.get());
		// }
		return null;

	}

	public static int computeSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
		int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);

		int roundedSize;
		if (initialSize <= 8) {
			roundedSize = 1;
			while (roundedSize < initialSize) {
				roundedSize <<= 1;
			}
		} else {
			roundedSize = (initialSize + 7) / 8 * 8;
		}

		return roundedSize;
	}

	/**
	 * 根据文件系统的路径得到sdcard路径
	 * file:///mnt/sdcard/dcim/100MEDIA/IMAG0479.jpg-->/mnt/sdcard
	 * /dcim/100MEDIA/IMAG0479.jpg
	 */
	public static String getAbsoluteImgPathByFilePath(String filePath) {
		return "/" + filePath.replace("file:///", "").replace("%20", " ");
	}

	/**
	 * 根据系统图库路径得到sdcard路径 content://media/external/images/media/1
	 * -->/mnt/sdcard/dcim/100MEDIA/IMAG0479.jpg
	 */
	public static String getAbsoluteImgPathByContentPath(String filePath, Context context) {
		Uri uri = Uri.parse(filePath);
		String returnpath = null;
		Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
		if (cursor != null) {
			cursor.moveToFirst();
			// MediaColumns.DATA = "_data"
			returnpath = cursor.getString(cursor.getColumnIndex(MediaColumns.DATA));
			cursor.close();
		}
		return returnpath;
	}

	/**
	 * 获取当前版本号
	 */
	public static String getVersionName(Context context) {
		String versionName = null;
		try {
			versionName = context.getPackageManager().getPackageInfo("com.emww.yilinke", 0).versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return versionName;
	}
	
	public static int getVersionCode(Context context) {
		try {
			return context.getPackageManager().getPackageInfo("com.emww.yilinke", 0).versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}


	/**
	 * create new file
	 * 
	 * @param f
	 * @throws Exception
	 */
	public static void createNewFile(File f) throws Exception {
		if (!f.getParentFile().exists()) {
			f.getParentFile().mkdirs();
		}
		if (!f.exists()) {
			f.createNewFile();
		}
	}

	private static final double EARTH_RADIUS = 6378137;
	private static final double PI = 3.14159265;
	private static final double RAD = Math.PI / 180.0;

	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}

	/**
	 * 根据两点间经纬度坐标（double值），计算两点间距离，单位为米
	 * 
	 * @param lng1
	 * @param lat1
	 * @param lng2
	 * @param lat2
	 * @return
	 */
	// public static double getDistance(double lng1, double lat1, double lng2,
	// double lat2) {
	// double radLat1 = rad(lat1);
	// double radLat2 = rad(lat2);
	// double a = radLat1 - radLat2;
	// double b = rad(lng1) - rad(lng2);
	// double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
	// Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
	// s = s * EARTH_RADIUS;
	// s = Math.round(s * 10000) / 10000;
	// return s;
	// }

	/**
	 * 计算两点间距离（千米）
	 * 
	 * @param lat1
	 * @param lat2
	 * @param lon1
	 * @param lon2
	 * @return
	 * @return double单位是千米
	 */
	public static double getDistatce(double lat1, double lat2, double lon1, double lon2) {
		double R = 6371;
		double distance = 0.0;
		double dLat = (lat2 - lat1) * Math.PI / 180;
		double dLon = (lon2 - lon1) * Math.PI / 180;
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		distance = (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))) * R;
		return distance;
	}

	/**
	 * 
	 * @param provinceId
	 * @param cityId
	 * @return 江苏 南京
	 */
//	public static String getPlace(String provinceId, String cityId) {
//		String provinceName = "";
//		String cityName = "";
//		AreaDBManage areaDBManage = PandLifeApp.getInstance().getAreaDBManage();
//		if (!TextUtils.isEmpty(provinceId) && !"0".equals(provinceId)) {
//			ProvinceItem provinceItem = areaDBManage.getProvince(provinceId);
//			if (provinceItem != null)
//				provinceName = provinceItem.getProvince();
//		}
//		if (!TextUtils.isEmpty(cityId) && !"0".equals(cityId)) {
//			CityItem cityItem = areaDBManage.getCity(cityId);
//			if (cityItem != null)
//				cityName = cityItem.getCity();
//		}
//
//		return provinceName + " " + cityName;
//	}

	/**
	 * 橘黄色显示**中的内容，未找到*不着色
	 * 
	 * @param message
	 * @return CharSequence
	 */
	public static CharSequence getColorStr(String message) {
		return getColorStr(message, Color.parseColor("#ea6e2e"), false);
	}

	/**
	 * 橘黄色显示**中的内容
	 * 
	 * @param message
	 * @param isDye
	 *            true：未找到*则全部着色；false：未找到*不着色
	 * @return CharSequence
	 */
	public static CharSequence getColorStr(String message, boolean isDye) {
		return getColorStr(message, Color.parseColor("#ea6e2e"), isDye);
	}

	/**
	 * 指定色显示**中的内容
	 * 
	 * 用Spannable在TextView中设置超链接、颜色、字体
	 * http://aichixihongshi.iteye.com/blog/1207503
	 * @param message
	 * @param color
	 * @param isDye
	 *            true：未找到*则全部着色；false：未找到*不着色
	 * @return CharSequence
	 */
	public static CharSequence getColorStr(String message, int color, boolean isDye) {
		if (message == null) {
			return null;
		}

		int start = message.indexOf("*");
		int end = message.lastIndexOf("*");
		if (start == -1 && isDye)
			start = 0;
		if (end == -1 && isDye)
			end = message.length() + 1;

		if (start != -1 && end != -1) {
			// String company = message.substring(start, end);
			SpannableString spStr = new SpannableString(message.replaceAll("\\*", ""));// span：跨度
			// spStr.setSpan(new IntentSpan(Color.parseColor("#ea6e2e")), start,
			// end - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			spStr.setSpan(new ForegroundColorSpan(color), start, end - 1, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
			return spStr;
		} else {
			return message;
		}
		// contentTV.setMovementMethod(LinkMovementMethod.getInstance());
	}
	
	/**
	 * 获取带有删除线的字符串
	 * @param message
	 * @return CharSequence
	 */
	public static CharSequence getStrikethroughStr(String message){
		return getStrikethroughStr(message, true);
	}
	
	/**
	 * 获取带有删除线的字符串
	 * @param message
	 * @param isDye true：未找到*则全部删除线；false：未找到*没有删除线
	 * @return CharSequence
	 */
	public static CharSequence getStrikethroughStr(String message, boolean isDye){
		if (message == null) {
			return null;
		}
		int start = message.indexOf("*");
		int end = message.lastIndexOf("*");
		if (start == -1 && isDye)
			start = 0;
		if (end == -1 && isDye)
			end = message.length() + 1;

		if (start != -1 && end != -1) {
			SpannableString spStr = new SpannableString(message.replaceAll("\\*", ""));// span：跨度
			spStr.setSpan(new StrikethroughSpan(), start, end - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			return spStr;
		} else {
			return message;
		}
	}
	

	public static TextView getDeleteTV(Context ctx) {
		int padding = (int) ctx.getResources().getDimension(R.dimen.msg_delete_padding);
		// int padding = (int)dip2px(ctx, 4);
		TextView deleteTV = new TextView(ctx);
		// deleteTV.setId(R.id.ifr_tv_delete);
		deleteTV = new TextView(ctx);
		deleteTV.setVisibility(View.GONE);
		deleteTV.setText("删除");
		deleteTV.setTextColor(Color.WHITE);
		// setTextSize()默认的单位是sp; (int)
		// ctx.getResources().getDimension(R.dimen.msg_delete_size)
		deleteTV.setTextSize(15);
		deleteTV.setBackgroundResource(R.drawable.pub_btn_right);// pub_btn_red
		deleteTV.setPadding(padding, 0, padding, 0);
		deleteTV.setGravity(Gravity.CENTER);
		return deleteTV;
	}

	private static RelativeLayout.LayoutParams rl;

	public static RelativeLayout.LayoutParams getDeleteRL() {
		if (rl == null) {
			rl = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			rl.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			rl.addRule(RelativeLayout.CENTER_VERTICAL);
		}
		return rl;
	}

	public static float dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return dipValue * scale + 0.5f;
	}

	public static float px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return pxValue / scale + 0.5f;
	}

	public static Date getDate2(String time) {
		if (TextUtils.isEmpty(time))
			return new Date();
		try {
			return sdf_yyyy_MM_dd.parse(time);
		} catch (ParseException e) {
		}
		try {
			return sdf_HH_mm.parse(time);
		} catch (ParseException e) {
		}
		return new Date();
	}

	/**
	 * 图片转字节数组
	 * 
	 * @param bmp
	 * @param needRecycle
	 * @return byte[]
	 */
	public static byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
		if (bmp == null) {
			return null;
		}

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		bmp.compress(CompressFormat.PNG, 100, output);
		if (needRecycle) {
			bmp.recycle();
		}

		byte[] result = output.toByteArray();
		try {
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
	
	
	/**
	 * http://www.xxxx.com/xxx.aspx?fileId=112&name=张三
	 * @param queryString
	 * @return Map fileId=112 name=张三
	 */
	public static Map getParamsMap(String queryString) {
		Map paramsMap = new HashMap();
		if (queryString != null && queryString.length() > 0) {
			queryString = queryString.substring(queryString.indexOf("?") + 1);
			int ampersandIndex, lastAmpersandIndex = 0;
			String subStr, param, value;
			String[] paramPair;
			do {
				ampersandIndex = queryString.indexOf('&', lastAmpersandIndex) + 1;
				if (ampersandIndex > 0) {
					subStr = queryString.substring(lastAmpersandIndex, ampersandIndex - 1);
					lastAmpersandIndex = ampersandIndex;
				} else {
					subStr = queryString.substring(lastAmpersandIndex);
				}
				paramPair = subStr.split("=");
				param = paramPair[0];
				value = paramPair.length == 1 ? "" : paramPair[1];
				try {
					value = URLDecoder.decode(value, "UTF-8");
				} catch (UnsupportedEncodingException ignored) {
				}
				paramsMap.put(param, value);
			} while (ampersandIndex > 0);
		}
		return paramsMap;
	}
	
	
	/**
	 * 根据路径获取文件名
	 * @param urlPath
	 * @return
	 */
	public static String getFileNameFromUrlPath(String urlPath) {
		String str = "";
		urlPath = urlPath.trim();
		urlPath = urlPath.replace("\\", "/");
		String[] temp = urlPath.split("/");
		str = temp[temp.length - 1];
		return str;
	}

	/**
	 * 处理图片的一些方法
	 */
	public static final String IMAGE_UNSPECIFIED = "image/*";
	public static final int PhotoResult = 3;
	private static String saveBase = "/sdcard/header/";

	public static File saveFile(Bitmap bm,String savePicPath, String fileName) throws IOException {
		File myCaptureFile = new File(savePicPath, fileName);
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(myCaptureFile));
		bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
		bos.flush();
		bos.close();
		return myCaptureFile;
	}

	/**
	 * 处理图片方法
	 * @param uri
	 * @return
	 */
	public static Intent startPhotoZoom(Uri uri) {
		System.out.println("进入startPhotoZoom(uti)方法uri为：------------------->>>" + uri);
		// 进入startPhotoZoom(uti)方法uri为：------------------->>>content://media/external/images/media/6
		// ？crop：收割，修剪；调用系统中的修剪工具
		Intent intent = new Intent("com.android.camera.action.CROP", null);
		// 在intent上绑定照片数据data
		intent.setDataAndType(uri, IMAGE_UNSPECIFIED);
		// crop：收割，修剪；
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 300);
		intent.putExtra("aspectY", 300);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 300);// 输出图片的宽
		intent.putExtra("outputY", 300);// 输出图片的高
		intent.putExtra("return-data", true);
		return intent;
//		startActivityForResult(intent, PhotoResult);
	}

	/**
	 * 判断是否有SDCard
	 * @return
	 */
	public static boolean hasSDCard(){
		return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	}
	/**
	 * 获取文件名
	 * @return
	 */
	public static String getPicName() {
		String ss = getFolderName("'IMG'_yyyyMMdd_hhmmss", false) + (int) (Math.random() * 100) + ".jpg";
		return ss;
	}

	/**
	 * 获取文件（包括路径）名
	 * @param timeFormat
	 * @return
	 */
	public static String getFolderName(String timeFormat) {
		return getFolderName(timeFormat, true);
	}

	public static String getFolderName(String timeFormat, boolean creatFoler) {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat(timeFormat);// "'IMG'_yyyy-MM-dd_"
		String folerName = dateFormat.format(date);
		File filejia = new File(saveBase, folerName);
		if (!filejia.exists() && creatFoler) {
			filejia.mkdirs();// 按照文件夹路径创建文件夹
		}
		return folerName;
	}
	
	
	private static int widthPixels;
	public static int getScreenWidth(Context ctx) {
		if (widthPixels == 0) {
			// 头部水平填满，计算高度维持原比例
			DisplayMetrics dm = new DisplayMetrics();
			((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(dm);
			widthPixels = dm.widthPixels;
		}
		return widthPixels;
	}


	/**
	 * Json 转成 Map<>
	 * @param jsonStr
	 * @return
	 */
	public static Map<String, String> getMapForJson(String jsonStr){
		JSONObject jsonObject ;
		try {
			jsonObject = new JSONObject(jsonStr);

			Iterator<String> keyIter= jsonObject.keys();
			String key;
			String value ;
			Map<String, String> valueMap = new HashMap<String, String>();
			while (keyIter.hasNext()) {
				key = keyIter.next();
				value = jsonObject.get(key).toString();
				valueMap.put(key, value);
			}
			return valueMap;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 把json 转换为ArrayList 形式
	 * @return
	 */
	public static List<Map<String, String>> getList(String jsonString)
	{
		List<Map<String, String>> list = null;
		try
		{
			JSONArray jsonArray = new JSONArray(jsonString);
			JSONObject jsonObject;
			list = new ArrayList<>();
			for (int i = 0; i < jsonArray.length(); i++)
			{
				jsonObject = jsonArray.getJSONObject(i);
				list.add(Util.getMapForJson(jsonObject.toString()));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}
}