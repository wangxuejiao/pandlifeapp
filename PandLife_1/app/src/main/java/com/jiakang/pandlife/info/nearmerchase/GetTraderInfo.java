package com.jiakang.pandlife.info.nearmerchase;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.info.BaseAbsInfo;
import com.jiakang.pandlife.item.GoodsKindItem;
import com.jiakang.pandlife.item.MerchantInfoItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 4.商家信息获取接口
 *
 * @author ww
 *
 */
public class GetTraderInfo extends BaseAbsInfo {

    private static final String TAG = "GetTraderInfo";

    /** 商家id	Int(10) */
    private int sid;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private MerchantInfoItem mMerchantInfoItem;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Trader&a=Gettraderinfo" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("sid", sid);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                mMerchantInfoItem = BaseInfo.gson.fromJson(jsonObject.get("data").toString(), MerchantInfoItem.class);

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public MerchantInfoItem getMerchantInfoItem() {
        return mMerchantInfoItem;
    }

    public void setMerchantInfoItem(MerchantInfoItem merchantInfoItem) {
        mMerchantInfoItem = merchantInfoItem;
    }
}
