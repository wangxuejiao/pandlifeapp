package com.jiakang.pandlife.acty.share;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.personalcenter.AddressRedactActivity;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.db.AreaDBManage;
import com.jiakang.pandlife.item.Item;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 *选择省市区，通过传值不同做不同操作
 */
@EActivity(R.layout.activity_select_pro_city_area)
public class SelectProCityAreaActivity extends BaseActy {
    protected static final String TAG = "SelectProCityAreaActivity";

    @ViewById(R.id.aspca_lv_listview)
    ListView mListView;

    //上一个页面要求选择什么（省、市、区）
    private String selectType;
    private int keyCode;

    private ItemAdapter mItemAdapter;

    private AreaDBManage mAreaDBManage;

    private List<Item> allItems = new ArrayList<Item>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        Intent intent = getIntent();
        selectType = intent.getExtras().getString(AddressRedactActivity.SELECT_TYPE);
        keyCode = intent.getExtras().getInt(AddressRedactActivity.SELECT_DATA);
        initTitleBar(R.id.aspca_tb_title, selectType);

        mAreaDBManage = PandLifeApp.getInstance().getAreaDBManage();
        mItemAdapter = new ItemAdapter(mContext);
        mListView.setAdapter(mItemAdapter);
        bindView();
    }

    private void bindView(){
        if (("选择省").equals(selectType)){
            allItems =  mAreaDBManage.getAllProvince();
            mItemAdapter.addItems(allItems);
            mItemAdapter.notifyDataSetChanged();
        }
        else if (("选择市").equals(selectType)){
            allItems =  mAreaDBManage.getAllCity(keyCode);
            mItemAdapter.addItems(allItems);
            mItemAdapter.notifyDataSetChanged();
        }
        else if (("选择区").equals(selectType)){
            allItems =  mAreaDBManage.getAllArea(keyCode);
            mItemAdapter.addItems(allItems);
            mItemAdapter.notifyDataSetChanged();
        }

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listView = (ListView)parent;
                Item item = (Item) listView.getItemAtPosition(position);
                Intent intent = new Intent();
                intent.putExtra("item", item);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
