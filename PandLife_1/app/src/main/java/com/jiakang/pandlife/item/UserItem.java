package com.jiakang.pandlife.item;

/**
 * 用户实体
 * Created by Administrator on 2015/12/8.
 */
public class UserItem extends Item{

    /** 头像地址	Varchar(255) */
    private String head;
    /** 站点id	Int(10) */
    private int station_id;
    /** 小区id	Int(10) */
    private int cid;
    /** 昵称	Varchar(255) */
    private String nick;
    /** 手机号	Varchar(11) */
    private String mobile;
    /** 真实姓名	Varchar(255) */
    private String realname;
    /** 身份证号	Varchar(255) */
    private String card_no;
    /** 出生日期	Varchar(255) */
    private String birthday;
    /** 家庭住址	Varchar(255) */
    private String address;
    /** 账户余额 float */
    private float money;

    private int layout;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }
}
