package com.jiakang.pandlife.acty.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.bigkoo.convenientbanner.listener.OnItemClickListener;
import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.MainActivity;
import com.jiakang.pandlife.acty.Post.PostDetailActivity_;
import com.jiakang.pandlife.acty.WebViewActivity;
import com.jiakang.pandlife.acty.express.CaptureActivity;
import com.jiakang.pandlife.acty.express.ExpressDetailActivity;
import com.jiakang.pandlife.acty.express.ScanCodeDeliveryActivity_;
import com.jiakang.pandlife.acty.express.SenderExpressActivity_;
import com.jiakang.pandlife.acty.express.MyPackageActivity_;
import com.jiakang.pandlife.acty.homepage.GroupPurchaseActivity_;
import com.jiakang.pandlife.acty.homepage.GroupPurchaseDetailActivity_;
import com.jiakang.pandlife.acty.homepage.MerchantGoodsListActivity_;
import com.jiakang.pandlife.acty.homepage.SpellGoodsActivity_;
import com.jiakang.pandlife.acty.homepage.TomorrowAdvanceActivity_;
import com.jiakang.pandlife.acty.neighbour.VillageGoodsActivity_;
import com.jiakang.pandlife.acty.register.SelectLocationOrManualActivity;
import com.jiakang.pandlife.acty.register.SelectLocationOrManualActivity_;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.db.DataCacheDBHelper;
import com.jiakang.pandlife.db.DataCacheDBManage;
import com.jiakang.pandlife.info.BannerInfo;
import com.jiakang.pandlife.info.CommunityInfo;
import com.jiakang.pandlife.info.GetIndexPicInfo;
import com.jiakang.pandlife.info.MyCommunityInfo;
import com.jiakang.pandlife.info.nearmerchase.NearTraderInfo;
import com.jiakang.pandlife.item.AdvertisementItem;
import com.jiakang.pandlife.item.BannerItem;
import com.jiakang.pandlife.item.CommunityItem;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.NearbyMerchantItem;
import com.jiakang.pandlife.item.PostDetail;
import com.jiakang.pandlife.itemview.NetworkImageHolderView;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.DataCache;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.Banner;
import com.jiakang.pandlife.widget.ImageBrowsingViewFlipper;
import com.jiakang.pandlife.widget.MarkView;
import com.jiakang.pandlife.widget.PullToRefreshListView;
import com.jiakang.pandlife.widget.PullToRefreshListViewInScrollView;
import com.jiakang.pandlife.widget.TitleBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * 首页Fragment
 */
public class HomePageFragment extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener, OnItemClickListener {
    protected static final String TAG = "HomePageFragment";

    private TitleBar mTitleBar;
    private RelativeLayout viewFlipperRL;
    private ConvenientBanner convenientBanner;
    /** 滑动图片 */
//    private ImageBrowsingViewFlipper viewFlipper;
//    //	private ViewPager viewFlipper;
//    private MarkView markView;
    private List<Drawable> imgs = new ArrayList<Drawable>();

    private RelativeLayout myPackageRL;
    private RelativeLayout expressRL;
    private ImageView groupPurchaseTV;
    private ImageView advanceTV;
    private ImageView goodsTV;
    private PullToRefreshListViewInScrollView nearbyMerchantLV;

    private DataCacheDBManage dataCacheDBManage;

    private ItemAdapter mItemAdapter;
    /** 每页个数，BaseInfo.pageSize = 20 为默认值 */
    private int pageSize = 20;
    /** 第几页 */
    private int pageIndex = 1;

    private final static int GET_NEW_OK = 0x12;
    private final static int GET_MORE_OK = 0x13;
    private final static int IS_GET_NO = 0;
    private final static int IS_GET_NEW = 1;
    private final static int IS_GET_MORE = 2;
    private static int isPullRefresh = 0;
    private boolean noUpLoadFinish = true;

    //banner获取类
    private BannerInfo mBannerInfo = new BannerInfo();
    private List<BannerItem.DataEntityBanner> mDataEntityBannerList;

    /** 获取我的绑定小区接口 */
    private MyCommunityInfo mMyCommunityInfo = new MyCommunityInfo();
    //我绑定的小区实体
    private CommunityItem myCommunityItem;
    /** 获取今日团购、明日预告、小区拼好货的首页图片接口 */
    private GetIndexPicInfo mGetIndexPicInfo = new GetIndexPicInfo();
    /** 附近商家接口 */
    private NearTraderInfo mNearTraderInfo = new NearTraderInfo();

    private List<NearbyMerchantItem> allItems = new ArrayList<NearbyMerchantItem>();

    /** 文件缓存类 */
    private DataCache mACache;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home_page, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initVar();
        initView();
        bindView();
        getMyCommuity();
//        getViewFlipper();
        getGetIndexPic();//获取今日团购、明日预告、小区拼好货的首页图片
        getCacheDataNearbyMerchant();
        bannerRequest();
    }

    @Override
    public void onResume() {
        super.onResume();
        //开始自动翻页
        convenientBanner.startTurning(5000);
    }

    @Override
    public void onPause() {
        super.onPause();
        //停止翻页
        convenientBanner.stopTurning();
    }



    private void initVar(){
        dataCacheDBManage = PandLifeApp.getInstance().getCacheDataDBManage();
    }

    private void initView(){
        mTitleBar = (TitleBar)getView().findViewById(R.id.fhp_tb_title);
        viewFlipperRL = (RelativeLayout)getView().findViewById(R.id.fhp_rl_viewFlipper);
        convenientBanner = (ConvenientBanner) getView().findViewById(R.id.convenientBanner);
//        viewFlipper = (ImageBrowsingViewFlipper)getView().findViewById(R.id.fhp_ibvf_viewflipper);
//        markView = (MarkView)getView().findViewById(R.id.fhp_mv_markView);
        myPackageRL = (RelativeLayout)getView().findViewById(R.id.fhp_rl_my_package);
        expressRL = (RelativeLayout)getView().findViewById(R.id.fhp_rl_express);
        groupPurchaseTV = (ImageView)getView().findViewById(R.id.fhp_tv_group_purchase);
        advanceTV = (ImageView)getView().findViewById(R.id.fhp_tv_advance);
        goodsTV = (ImageView)getView().findViewById(R.id.fhp_tv_goods);
        nearbyMerchantLV = (PullToRefreshListViewInScrollView)getView().findViewById(R.id.fhp_pslv_merchant);

        mItemAdapter = new ItemAdapter(getActivity());
        nearbyMerchantLV.setShowHeaderView(true);
        nearbyMerchantLV.setShowFootView(true);
        nearbyMerchantLV.setAdapter(mItemAdapter);

        //是滑动图片的RelativeLayout获取焦点，从而第一时间显示
        viewFlipperRL.setFocusable(true);
        viewFlipperRL.setFocusableInTouchMode(true);
        viewFlipperRL.requestFocus();
    }

    private void getMyCommuity(){
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mMyCommunityInfo, new AbsOnRequestListener(getActivity()){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try{
                    myCommunityItem = mMyCommunityInfo.getCommunityItem();
                    if (myCommunityItem != null){
                        mTitleBar.titleTV.setText(myCommunityItem.getName());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void bindView(){
        mTitleBar.titleTV.setText("绑定小区");
        mTitleBar.titleTV.setOnClickListener(this);
        mTitleBar.leftBN.setVisibility(View.GONE);
        mTitleBar.leftIBN.setVisibility(View.GONE);
        mTitleBar.rightBN.setVisibility(View.GONE);
        mTitleBar.rightIBN.setVisibility(View.VISIBLE);
        mTitleBar.rightIBN.setImageDrawable(getResources().getDrawable(R.mipmap.icon_iv_scanning));
        mTitleBar.rightIBN.setOnClickListener(this);
//        mTitleBar.rightBN.setText("扫一扫icon");

        myPackageRL.setOnClickListener(this);
        expressRL.setOnClickListener(this);
        groupPurchaseTV.setOnClickListener(this);
        advanceTV.setOnClickListener(this);
        goodsTV.setOnClickListener(this);

        nearbyMerchantLV.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
//							Intent intent = new Intent(mContext, CompanyDetailsActy.class);
//							startActivity(intent);
                if (position == mItemAdapter.getCount() + 1) {
                    return;
                }
                ListView listView = (ListView) parent;
                NearbyMerchantItem nearbyMerchantItem = (NearbyMerchantItem) listView.getItemAtPosition(position);
                Intent intentMerchantGoodsList = new Intent(getActivity(), MerchantGoodsListActivity_.class);
                intentMerchantGoodsList.putExtra("nearbyMerchantItem", nearbyMerchantItem);
                startActivityForResult(intentMerchantGoodsList, 0);

            }
        });
    }

    /**
     * 获取滑动图片
     */
//    private void getViewFlipper(){
//        imgs.clear();
//        Drawable drawable;
//        List<AdvertisementItem> itemList = dataCacheDBManage.getAdvertisementList();
//        if(itemList.size() != 0){
//            for(int i=0; i < itemList.size(); i ++){
//                Bitmap bitmap = BitmapFactory.decodeFile(itemList.get(i).getPicPath());
//                drawable = new BitmapDrawable(bitmap);
//                imgs.add(drawable);
//            }
//        }else{
//            imgs.add(getResources().getDrawable(R.mipmap.public_img_default));
//            imgs.add(getResources().getDrawable(R.mipmap.public_img_default));
//            imgs.add(getResources().getDrawable(R.mipmap.public_img_default));
//            imgs.add(getResources().getDrawable(R.mipmap.public_img_default));
////			imgs[1] =  getResources().getDrawable(R.drawable.main_iv_picture);
////			imgs[2] =  getResources().getDrawable(R.drawable.main_iv_picture);
//        }
//
////		viewFlipper.setVisibility(View.VISIBLE);
//        viewFlipper.setmImgBrowsingMark(this);
//        //设置图片
//        viewFlipper.setImgsDraw(imgs);
//        markView.setMarkCount(imgs.size());
//        //起始位置设置为0
//        markView.setMark(0);
//        // 向左滑动左侧进入的渐变效果（alpha 0.1  -> 1.0）
//        Animation lInAnim = AnimationUtils.loadAnimation(this.getActivity(), R.anim.push_left_in);
//        // 向左滑动右侧滑出的渐变效果（alpha 1.0  -> 0.1）
//        Animation lOutAnim = AnimationUtils.loadAnimation(this.getActivity(), R.anim.push_left_out);
//
//        if(imgs.size() == 1){
////			viewFlipper.startFlipping();
//        }else{
//            viewFlipper.setInAnimation(lInAnim);
//            viewFlipper.setOutAnimation(lOutAnim);
//        }
//        // 设置自动播放功能
//        viewFlipper.setAutoStart(true);
//        if(viewFlipper.isAutoStart() && !viewFlipper.isFlipping()){
//            viewFlipper.startFlipping();
//        }
//    }

    /**
     * 获取今日团购、明日预告、小区拼好货的首页图片方法
     */
    private void getGetIndexPic(){
        //判断缓存是否存在
        mACache = DataCache.get(getActivity());
        String key = "getIndexPic";
        JSONObject jsonObject = mACache.getAsJSONObject(key);
        if(jsonObject != null){
            try {
                JSONArray tuanJA = jsonObject.getJSONArray("tuan_list");
                String tuanPic = tuanJA.getJSONObject(0).getString("pic");
                String tuanType = tuanJA.getJSONObject(0).getString("type");

                JSONArray tTuanJA = jsonObject.getJSONArray("ttuan_list");
                String tTuanPic = tTuanJA.getJSONObject(0).getString("pic");
                String tTuanType = tTuanJA.getJSONObject(0).getString("type");

                JSONArray xqJA = jsonObject.getJSONArray("xq_list");
                String xqPic = xqJA.getJSONObject(0).getString("pic");
                String xqType = xqJA.getJSONObject(0).getString("type");

                ImageLoaderUtil.displayImage(tuanPic, groupPurchaseTV, R.mipmap.default_image);
                ImageLoaderUtil.displayImage(tTuanPic, advanceTV, R.mipmap.default_image);
                ImageLoaderUtil.displayImage(xqPic, goodsTV, R.mipmap.default_image);
            } catch (JSONException e) {
                getGetIndexPicByHttp();
                e.printStackTrace();
            }
        }else {
            getGetIndexPicByHttp();
        }
    }

    /**
     * 通过网络请求获取团购、明日预告、小区拼好货等数据
     */
    private void getGetIndexPicByHttp(){
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mGetIndexPicInfo, new AbsOnRequestListener(getActivity()) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    ImageLoaderUtil.displayImage(mGetIndexPicInfo.getTuanPic(), groupPurchaseTV, R.mipmap.default_image);
                    ImageLoaderUtil.displayImage(mGetIndexPicInfo.gettTuanPic(), advanceTV, R.mipmap.default_image);
                    ImageLoaderUtil.displayImage(mGetIndexPicInfo.getXqPic(), goodsTV, R.mipmap.default_image);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 通过数据库获取附近商家列表信息
     */
    private void getCacheDataNearbyMerchant(){
        allItems = dataCacheDBManage.getGetNearbyMerchantList();
        if (allItems.size() != 0){
            mItemAdapter.clear();
            mItemAdapter.addItems((List) allItems);
            mItemAdapter.notifyDataSetChanged();

            NearbyMerchantItem nearbyMerchantItem = allItems.get(0);
            if ((nearbyMerchantItem.getCurrentTime() + nearbyMerchantItem.getSaveTime()) < System.currentTimeMillis()) {
                //清除数据库
                dataCacheDBManage.clear(DataCacheDBHelper.TAB_NEARBY_MERCHANT_CACHE);
                //获取网络请求数据
                bindInfo(IS_GET_NO);
            } else {
                nearbyMerchantLV.onRefreshComplete();
                nearbyMerchantLV.onMoreComplete(false);
            }
        }else{
            bindInfo(IS_GET_NO);
        }
    }


    /**
     * 通过网络获取附近商家列表信息
     * @param isPullToRefresh
     */
    private void bindInfo(int isPullToRefresh){
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mNearTraderInfo, new AbsOnRequestListener(getActivity()){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    allItems = mNearTraderInfo.getAllItems();
                    if (allItems.size() == 0){
                        CustomToast.showToast(getActivity(), "没有数据");
                        return;
                    }
                    mItemAdapter.clear();
                    mItemAdapter.addItems((List) allItems);
                    mItemAdapter.notifyDataSetChanged();
                    nearbyMerchantLV.onRefreshComplete();
                    nearbyMerchantLV.onMoreComplete(false);

                    nearbyMerchantLV.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {

                        @Override
                        public void onRefresh() {
                            // 线程获取更新数据
                            isPullRefresh = 1;
                            //清除数据库
                            dataCacheDBManage.clear(DataCacheDBHelper.TAB_NEARBY_MERCHANT_CACHE);
                            bindInfo(IS_GET_NEW);

                        }
                    });

                    nearbyMerchantLV.setOnMoreListener(new PullToRefreshListView.OnMoreListener() {

                        @Override
                        public void onMore() {
                            // 线程获取更多数据
                            isPullRefresh = 2;
                            bindInfo(IS_GET_MORE);

                        }
                    });


                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * banner请求
     */
    private void bannerRequest() {

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mBannerInfo, new AbsOnRequestListener(getActivity()) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mDataEntityBannerList = mBannerInfo.getDataEntityBannerList();

                convenientBanner.setPages(new CBViewHolderCreator<NetworkImageHolderView>() {
                    @Override
                    public NetworkImageHolderView createHolder() {
                        return new NetworkImageHolderView();
                    }
                }, mDataEntityBannerList)
                        //设置两个点图片作为翻页指示器，不设置则没有指示器，可以根据自己需求自行配合自己的指示器
                        .setPageIndicator(new int[]{R.mipmap.dot_blur, R.mipmap.dot_focus})
                                //设置指示器的方向
//                .setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.ALIGN_PARENT_RIGHT)
//                .setOnPageChangeListener(this)//监听翻页事件
                        .setOnItemClickListener(HomePageFragment.this);
            }
        });
    }


    @Override
    public void onItemClick(int position) {

        //控制是否循环
//        convenientBanner.setCanLoop(!convenientBanner.isCanLoop());

        BannerItem.DataEntityBanner dataEntityBanner = mDataEntityBannerList.get(position);
        switch (dataEntityBanner.getKind()) {
            case "tuan":
                if (!TextUtils.isEmpty(dataEntityBanner.getId())) {

                    Intent intent = new Intent(getActivity(), GroupPurchaseDetailActivity_.class);
                    intent.putExtra("id", dataEntityBanner.getId());
                    startActivity(intent);
                } else {

                    Intent intent = new Intent(getActivity(), GroupPurchaseActivity_.class);
                    startActivity(intent);
                }
                break;
            case "xqphh":
                if (!TextUtils.isEmpty(dataEntityBanner.getId())) {

                    Intent intentGoodsDetail = new Intent(getActivity(), VillageGoodsActivity_.class);
                    intentGoodsDetail.putExtra("id", dataEntityBanner.getId());
                    startActivity(intentGoodsDetail);
                } else {

                    Intent intentXqphh = new Intent(getActivity(), VillageGoodsActivity_.class);
                    startActivity(intentXqphh);
                }
                break;

            case "linli":
                if(!TextUtils.isEmpty(dataEntityBanner.getId())){

                    Intent intentLinli = new Intent(getActivity(), PostDetailActivity_.class);
                    intentLinli.putExtra("postId", Integer.valueOf(dataEntityBanner.getId()));
                    intentLinli.putExtra("delete","no");
                    startActivity(intentLinli);

                }else {

                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.jumpToNeighbourFragement();
                }
                break;

            case "h5":
                WebViewActivity.startWebView(getActivity(), dataEntityBanner.getUrl());
                break;
            default:
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUSET_BIND_COMMUNITY) && (resultCode == getActivity().RESULT_FIRST_USER)){
            getMyCommuity();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.it_ibn_right:
//                //点击进入扫码取件界面——中间过度页去掉
//                Intent intentScanCodeDelivery = new Intent(getActivity(), ScanCodeDeliveryActivity_.class);
//                startActivityForResult(intentScanCodeDelivery, Constant.StaticCode.REQUEST_SCANNING_BARCODE);
                //打开扫描界面扫描条形码或二维码
                Intent openCameraIntent = new Intent(getActivity(), CaptureActivity.class);
                startActivityForResult(openCameraIntent, Constant.StaticCode.REQUEST_SCANNING_BARCODE);
                break;
            case R.id.it_tv_title:
                Intent intentSelectLocationOrManual = new Intent(getActivity(), SelectLocationOrManualActivity_.class);
                intentSelectLocationOrManual.putExtra(SelectLocationOrManualActivity.SITE_OR_COMMUNITY , "community");
                intentSelectLocationOrManual.putExtra("communityItem", myCommunityItem);
                startActivityForResult(intentSelectLocationOrManual, Constant.StaticCode.REQUSET_BIND_COMMUNITY);
                break;
            case R.id.fhp_rl_my_package:
                Intent intentMyPackage = new Intent(getActivity(), MyPackageActivity_.class);
                startActivity(intentMyPackage);
                break;
            case R.id.fhp_rl_express:
                Intent intentMyExpress = new Intent(getActivity(), SenderExpressActivity_.class);
                startActivity(intentMyExpress);
                break;
            case R.id.fhp_tv_group_purchase:
                Intent intentGroupPurchase = new Intent(getActivity(), GroupPurchaseActivity_.class);
                startActivityForResult(intentGroupPurchase, Constant.StaticCode.REQUEST_GROUP_PURCHASE);
                break;
            case R.id.fhp_tv_advance:
                Intent intentTomorrowAdvance = new Intent(getActivity(), TomorrowAdvanceActivity_.class);
                startActivity(intentTomorrowAdvance);
                break;
            case R.id.fhp_tv_goods:
                Intent intentSpellGoods = new Intent(getActivity(), VillageGoodsActivity_.class);
                startActivity(intentSpellGoods);
                break;
            default:
                break;
        }
    }

//    @Override
//    public MarkView getMarkView() {
//        return markView;
//    }
}
