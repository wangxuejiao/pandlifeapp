package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

import java.util.List;

/**
 * Created by play on 2016/1/18.
 */
public class PostTypeItem {


    /**
     * data : [{"name":"二手闲置","id":"1"},{"name":"小区活动","id":"2"}]
     * status : 1
     * info : 成功
     */
    private List<DataEntityPostType> data;
    private int status;
    private String info;

    public void setData(List<DataEntityPostType> data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<DataEntityPostType> getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }

    public class DataEntityPostType extends Item {
        /**
         * name : 二手闲置
         * id : 1
         */
        private String name;
        private String id;
        private int layout = R.layout.item_post_type;
        private boolean checked; //判断是否被选中

        public void setName(String name) {
            this.name = name;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public String getId() {
            return id;
        }

        public void setLayout(int layout) {
            this.layout = layout;
        }

        public int getLayout() {
            return layout;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        @Override
        public int getItemLayoutId() {
            return layout;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
