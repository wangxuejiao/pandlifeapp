package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 选择支付方式
 * Created by Administrator on 2016/1/14.
 */
@EActivity(R.layout.activity_select_pay_way)
public class SelectPayWayActivity extends BaseActy{
    protected static final String TAG = "SelectPayWayActivity";

    @ViewById(R.id.aspw_tv_indentNO)
    TextView indentNoTV;
    @ViewById(R.id.aspw_tv_sum)
    TextView sumTV;
    @ViewById(R.id.aspw_iv_balance_select)
    ImageView balanceSelectIV;
    @ViewById(R.id.aspw_iv_alipay_select)
    ImageView alipaySelectIV;
    @ViewById(R.id.aspw_iv_wechat_select)
    ImageView wechatSelectIV;
    @ViewById(R.id.aspw_iv_unionpay_select)
    ImageView unionpaySelectIV;
    @ViewById(R.id.aspw_btn_confirm_pay)
    Button confirmPayBN;

    //记录选择充值方式
    private String currentRechargeWay;
    //需要付款的金额
    private float paySum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.aspw_tb_title, "选择支付方式");
        Intent intent = getIntent();
        currentRechargeWay = intent.getExtras().getString("rechargeWay");
        paySum = intent.getExtras().getFloat("paySum");
        bindView();
        notifySelectIV();
    }

    private void bindView(){
        findViewById(R.id.aspw_rl_alipay_type).setOnClickListener(this);
        findViewById(R.id.aspw_rl_wechat_type).setOnClickListener(this);
        findViewById(R.id.aspw_rl_unionpay_type).setOnClickListener(this);
        confirmPayBN.setOnClickListener(this);

        sumTV.setText("￥" + paySum);
        confirmPayBN.setText("确认支付 ￥" + paySum);
    }

    /**
     *更新选中的钩方法
     */
    private void notifySelectIV(){
        if (("0").equals(currentRechargeWay)){
            balanceSelectIV.setVisibility(View.VISIBLE);
            alipaySelectIV.setVisibility(View.GONE);
            wechatSelectIV.setVisibility(View.GONE);
            unionpaySelectIV.setVisibility(View.GONE);
        }if (("1").equals(currentRechargeWay)){
            balanceSelectIV.setVisibility(View.GONE);
            alipaySelectIV.setVisibility(View.VISIBLE);
            wechatSelectIV.setVisibility(View.GONE);
            unionpaySelectIV.setVisibility(View.GONE);
        }
        else if(("2").equals(currentRechargeWay)){
            balanceSelectIV.setVisibility(View.GONE);
            alipaySelectIV.setVisibility(View.GONE);
            wechatSelectIV.setVisibility(View.VISIBLE);
            unionpaySelectIV.setVisibility(View.GONE);
        }
        else if(("3").equals(currentRechargeWay)){
            balanceSelectIV.setVisibility(View.GONE);
            alipaySelectIV.setVisibility(View.GONE);
            wechatSelectIV.setVisibility(View.GONE);
            unionpaySelectIV.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 选中后关闭页面，跳转上个页面且传值
     */
    private void finishAndSetResult(){
        Intent intentResult = new Intent();
        intentResult.putExtra("rechargeWay", currentRechargeWay);
        setResult(RESULT_OK, intentResult);
        finish();
    }

    /**
     * 确认支付方法，进入ping++
     */
    private void confirmPay(){

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.aspw_rl_balance_type:
                currentRechargeWay = "0";
                notifySelectIV();
//                finishAndSetResult();
                break;case R.id.aspw_rl_alipay_type:
                currentRechargeWay = "1";
                notifySelectIV();
//                finishAndSetResult();
                break;
            case R.id.aspw_rl_wechat_type:
                currentRechargeWay = "2";
                notifySelectIV();
//                finishAndSetResult();
                break;
            case R.id.aspw_rl_unionpay_type:
                currentRechargeWay = "3";
                notifySelectIV();
//                finishAndSetResult();
                break;
            case R.id.aspw_btn_confirm_pay:
                confirmPay();
                break;
        }
    }
}
