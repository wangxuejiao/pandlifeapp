package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.DeliveryItem;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.utils.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 5.获取收件人列表接口
 *
 * @author ww
 *
 */
public class GetdeliveryInfo extends BaseAbsInfo {

    private static final String TAG = "GetdeliveryInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<DeliveryItem> allItems = new ArrayList<DeliveryItem>();

    private List<String> delivery_type = new ArrayList<String>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Express&a=Getdelivery" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                data = (JSONObject)jsonObject.get("data");
                JSONArray deliveryType = data.getJSONArray("delivery_type");
                for(int j=0;j<deliveryType.length();j++){
                    delivery_type.add(deliveryType.get(j).toString());
                }
//                        jsonObject.getJSONObject("delivery_type");
//                delivery_type = Util.getList(deliveryType.toString());

                allItems.clear();
                JSONArray taskJA = data.getJSONArray("delivery_list");
                String itemStr;
                DeliveryItem deliveryItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    deliveryItem = BaseInfo.gson.fromJson(itemStr, DeliveryItem.class);
                    allItems.add(deliveryItem);
                }



            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<DeliveryItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<DeliveryItem> allItems) {
        this.allItems = allItems;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getDelivery_type() {
        return delivery_type;
    }

    public void setDelivery_type(List<String> delivery_type) {
        this.delivery_type = delivery_type;
    }
}
