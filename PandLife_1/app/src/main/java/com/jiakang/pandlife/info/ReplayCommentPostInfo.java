package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.ReplayCommentPost;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by play on 2016/1/20.
 */
public class ReplayCommentPostInfo extends BaseAbsInfo{

    private int replyid;
    private String content;
    private ReplayCommentPost replayCommentPost;

    public int getReplyid() {
        return replyid;
    }

    public void setReplyid(int replyid) {
        this.replyid = replyid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ReplayCommentPost getReplayCommentPost() {
        return replayCommentPost;
    }

    public void setReplayCommentPost(ReplayCommentPost replayCommentPost) {
        this.replayCommentPost = replayCommentPost;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Forum&a=ReplyUser&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("replyid",replyid);
            json.put("content",content);
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        try {
            if(jsonObject.getInt("status") == 1){

                replayCommentPost = BaseInfo.gson.fromJson(jsonObject.toString(),ReplayCommentPost.class);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
