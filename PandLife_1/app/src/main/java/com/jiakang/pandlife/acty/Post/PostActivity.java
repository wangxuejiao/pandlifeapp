package com.jiakang.pandlife.acty.Post;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.PostInfo;
import com.jiakang.pandlife.utils.CameraUtil;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.DimensionUtil;
import com.jiakang.pandlife.utils.QiNiuUtil;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.ChoosePhotoPopwindow;
import com.jiakang.pandlife.widget.ScrollGridView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import me.iwf.photopicker.PhotoPickerActivity;
import me.iwf.photopicker.utils.PhotoPickerIntent;
import me.iwf.photopicker.widget.TouchImageView;


@EActivity(R.layout.activity_post)
public class PostActivity extends BaseActy {

    @ViewById(R.id.et_title_post)
    EditText etTitle;
    @ViewById(R.id.et_content_post)
    EditText etContent;
    @ViewById(R.id.tv_type_post)
    TextView tvType;
    @ViewById(R.id.iv_add_image_post)
    ImageView ivAddImage;
    @ViewById(R.id.rl_images_post)
    RelativeLayout rlImagesPost;
    @ViewById(R.id.gv_pick_picture_post)
    ScrollGridView gvPickPicture;//存放选择的图片地址
    @ViewById(R.id.rl_post_type)
    RelativeLayout rlPostType;
    @ViewById(R.id.tv_type_post)
    TextView tvTypePost;

    public static final int TAKE_PHOTO_REQUEST_CODE = 1; //拍照
    public static final int TAKE_PHOTOGRAPH_REQUEST_CODE = 2;//从相册选取
    public static final int CROP_PHOTO_REQUEST_CODE = 3;//裁剪照片
    public static final int POST_TYPE_REQUEST_TYPE = 4;//帖子类型

    PopupWindow popupWindow;
    private List<String> mSelectImageList;
    private int mCurrentClickPosition = -1;//传递给帖子类型，初始化选中的帖子类型
    private int mPostType;//记录帖子的类型
    //发帖类
    private PostInfo mPostInfo = new PostInfo();
    ApiManager mApiManager = ApiManager.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @AfterViews
    protected void afterCreateView() {

        initTitleBar(R.id.al_tb_title, "发帖", null, "发布");
        rlPostType.setOnClickListener(this);
    }

    private void initData() {

        mSelectImageList = new ArrayList<>();
        QiNiuUtil.requestQiNiuToken(mContext);
    }


    private void checkInput() {

        if (TextUtils.isEmpty(etTitle.getText())) {

            Toast.makeText(mContext, "请填写标题", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(etContent.getText())) {

            Toast.makeText(mContext, "请填写内容", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(tvType.getText())) {

            Toast.makeText(mContext, "请选择类型", Toast.LENGTH_SHORT).show();
            return;
        }

        mPostInfo.setTitle(etTitle.getText().toString());
        mPostInfo.setContent(etContent.getText().toString());
        mPostInfo.setType(mPostType);
    }

    private void postRequest() {

        checkInput();

        mApiManager.request(mPostInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                Toast.makeText(mContext, mPostInfo.getPost().getInfo(), Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            }
        });

    }

    @Click(R.id.iv_add_image_post)
    protected void chooseImageClick() {

        showPopWindown();
    }

    private static final String IMAGE_CAMERA = "file://" + Environment.getExternalStorageDirectory() + "/" + System.currentTimeMillis() + ".jpg";
    Uri imageUri = Uri.parse(IMAGE_CAMERA);
    private void showPopWindown() {

        //设置popwindownAcitivity中的位置（底部居中）
        popupWindow = new ChoosePhotoPopwindow(this, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (v.getId()) {

                    case R.id.btn_photograph_popwindow:

                        Log.i(TAG,"----->imageUri:" + imageUri);
                        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                        startActivityForResult(intentCamera, TAKE_PHOTO_REQUEST_CODE);
                        break;

                    case R.id.btn_album_popwindow:

                        PhotoPickerIntent intent = new PhotoPickerIntent(PostActivity.this);
                        intent.setPhotoCount(9);
                        intent.setShowCamera(false);
                        intent.setShowGif(false);
                        startActivityForResult(intent, TAKE_PHOTOGRAPH_REQUEST_CODE);

                        break;

                    default:
                        break;
                }

            }
        });
        popupWindow.showAtLocation(PostActivity.this.findViewById(R.id.ll_post), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }

    class PictureGridViewAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mSelectImageList.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return mSelectImageList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            WindowManager wm = PostActivity.this.getWindowManager();

            int tempSize = (int) ((wm.getDefaultDisplay().getWidth() - DimensionUtil.dip2px(mContext, (float) 20.0)) / 4);
            int imageSize = tempSize - (int)(DimensionUtil.dip2px(mContext, (float) 2.0));
            TouchImageView imageView;
            if (convertView == null) {

                imageView = new TouchImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(imageSize, imageSize));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } else {
                imageView = (TouchImageView) convertView;
            }

            if (position == mSelectImageList.size()) {
                imageView.setImageResource(R.mipmap.default_add_image);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        showPopWindown();
                    }
                });
            } else {
                imageView.setImageBitmap(BitmapFactory.decodeFile(mSelectImageList.get(position)));
            }

            return imageView;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case TAKE_PHOTO_REQUEST_CODE: //拍照

                    CameraUtil.cropImageUri(PostActivity.this,imageUri,1,1,400,400,CROP_PHOTO_REQUEST_CODE);
                    break;

                case TAKE_PHOTOGRAPH_REQUEST_CODE: //从相册选取
                    if (data != null) {
                        rlImagesPost.removeView(ivAddImage);
                        mSelectImageList = data.getStringArrayListExtra(PhotoPickerActivity.KEY_SELECTED_PHOTOS);
                        mPostInfo.getImgList().clear();
                        for (String imgPath : mSelectImageList) {

                            Log.i(TAG,"------>imagpath:" + imgPath);
                            String key = System.currentTimeMillis() + imgPath.substring(imgPath.lastIndexOf("."));
                            mPostInfo.getImgList().add(key);
                            QiNiuUtil.uploadNoConfig(imgPath, key, QiNiuUtil.qiNiuToken);
                        }

                        gvPickPicture.setAdapter(new PictureGridViewAdapter());
                        popupWindow.dismiss();
                    }
                    break;

                case CROP_PHOTO_REQUEST_CODE: //裁剪

                    if(imageUri != null){

                        rlImagesPost.removeView(ivAddImage);
                        String imgPath = imageUri.getPath();
                        String key = System.currentTimeMillis() + imgPath.substring(imgPath.lastIndexOf("."));
                        mPostInfo.getImgList().add(key);
                        QiNiuUtil.uploadNoConfig(imgPath, key, QiNiuUtil.qiNiuToken);
                        mSelectImageList.add(imageUri.getPath());
                        gvPickPicture.setAdapter(new PictureGridViewAdapter());
                        popupWindow.dismiss();
                    }

                    break;

                case POST_TYPE_REQUEST_TYPE: //帖子类型
                    tvTypePost.setText(data.getStringExtra("namePostType"));
                    mCurrentClickPosition = data.getExtras().getInt("currentClickPosition");
                    mPostType = data.getExtras().getInt("postType");
                    break;
            }
        }
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.it_btn_left:
                finish();
                break;

            case R.id.it_btn_right:

                postRequest();
                break;
            case R.id.rl_post_type:
                Intent intent = new Intent(mContext, PostTypeActivity.class);
                intent.putExtra("currentClickPosition", mCurrentClickPosition);
                startActivityForResult(intent, POST_TYPE_REQUEST_TYPE);
                break;
        }
    }
}