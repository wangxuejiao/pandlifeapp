package com.jiakang.pandlife.item;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.annotation.sqlite.Transient;
import com.jiakang.pandlife.db.DataCacheDBHelper;

/**
 * Created by play on 2016/1/31.
 */
@Table(name = DataCacheDBHelper.TAB_JPUSH_NOTIFICATION_CACHE)
public class JpushNotificationItem extends Item {


    private int id;
    private String title;
    private String message;
    private long time = System.currentTimeMillis();
    private String flag = "未读";
    @Transient
    private int layout = R.layout.item_jpush_notification;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    @Override
    public int insert() {
        PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
        return 1;
    }
}
