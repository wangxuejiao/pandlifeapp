package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.PublicKuaiDiItem;
import com.jiakang.pandlife.item.SendBagItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 17.获取邮寄快递物流信息
 *
 * @author ww
 *
 */
public class PublicKuaiDiInfo extends BaseAbsInfo {

    private static final String TAG = "PublicKuaiDiInfo";

    /** taobao_code的值	Varchar(20) */
    private String code;
    /** 运单号码	Varchar(30) */
    private String postid;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<PublicKuaiDiItem> allItems = new ArrayList<PublicKuaiDiItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Express&a=Publickuaidi" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("code", code);
            json.put("postid", postid);
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();

                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                PublicKuaiDiItem publicKuaiDiItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    publicKuaiDiItem = BaseInfo.gson.fromJson(itemStr, PublicKuaiDiItem.class);
                    if (i == 0){
                        publicKuaiDiItem.setOrdinal("first");
                    }else if(i == (taskJA.length()-1)){
                        publicKuaiDiItem.setOrdinal("last");
                    }
                    allItems.add(publicKuaiDiItem);
                }
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public List<PublicKuaiDiItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<PublicKuaiDiItem> allItems) {
        this.allItems = allItems;
    }
}
