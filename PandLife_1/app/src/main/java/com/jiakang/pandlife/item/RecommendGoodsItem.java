package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

/**
 * 推荐商品实体
 * Created by Administrator on 2016/1/29.
 */
public class RecommendGoodsItem extends Item{

    /** 产品id	Int(10) */
    private int id;
    /** 产品标题	Varchar(255) */
    private String title;
    /** 产品图片	Varchar(255) */
    private String pic;
    /** 产品数量	Int(10) */
    private int number;
    /** 原价	Int(10) */
    private float price;
    /** 销售价格	Int(10) */
    private float tprice;
    /** 是否推荐	Varchar */
    private String is_tj;
    /** 销量	Int(10) */
    private int sales;

    /** 添加到购物车中的数量（点中的次数） */
    private int clickNum = 0;

    private int layout = R.layout.item_goods_list;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getTprice() {
        return tprice;
    }

    public void setTprice(float tprice) {
        this.tprice = tprice;
    }

    public String getIs_tj() {
        return is_tj;
    }

    public void setIs_tj(String is_tj) {
        this.is_tj = is_tj;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public int getClickNum() {
        return clickNum;
    }

    public void setClickNum(int clickNum) {
        this.clickNum = clickNum;
    }
}
