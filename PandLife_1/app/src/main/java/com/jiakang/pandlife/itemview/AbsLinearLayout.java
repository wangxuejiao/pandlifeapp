package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;

/**
 * @author：xuwangwang E-mail: 541765907@qq.com
 * @date：2014年3月28日 上午11:20:57
 * @version 1.0 String Tag = "AbsRelativeLayout中：";
 */
public class AbsLinearLayout extends LinearLayout implements ItemView, OnClickListener {

	protected Context ctx;
	protected int position;
	protected ItemAdapter.OnViewClickListener onViewClickListener;

	public AbsLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.ctx = context;
	}

	public AbsLinearLayout(Context context) {
		super(context);
		this.ctx = context;
	}

	@Override
	public void findViewsByIds() {

	}

	@Override
	public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
		if (onViewClickListener != null) {
			this.position = position;
			this.onViewClickListener = onViewClickListener;
		}
	}

	@Override
	public void onClick(View v) {
		if (onViewClickListener != null) {
			onViewClickListener.onViewClick(v, position);
		}
	}
}
