package com.jiakang.pandlife.acty.personalcenter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 账户余额
 * Created by Administrator on 2016/1/14.
 */
@EActivity(R.layout.activity_account_balance)
public class AccountBalanceActivity extends BaseActy{
    protected static final String TAG = "AccountBalanceActivity";

    @ViewById(R.id.aab_btn_usering_detail)
    Button useringDetailBN;
    @ViewById(R.id.aab_tv_account_sum)
    TextView accountSumTV;
    @ViewById(R.id.aab_btn_recharge)
    Button rechargeBN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.aabm_tb_title, "账户余额");

        bindView();
    }

    private void bindView(){
        useringDetailBN.setOnClickListener(this);
        rechargeBN.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.aab_btn_usering_detail:
                Intent intentAccountDetail = new Intent(mContext, AccountDetailActivity_.class);
                startActivity(intentAccountDetail);
                break;
            case R.id.aab_btn_recharge:
                Intent intentRecharge = new Intent(mContext, RechargeActivity_.class);
                startActivity(intentRecharge);
                break;
        }
    }
}
