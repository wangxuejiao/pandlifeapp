package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

/**
 * Created by play on 2016/1/27.
 */
public class VoteItem extends Item {

    /**
     * count : 0
     * id : 1
     * option : 强烈需要
     * totalCount:总投票数（自己添加）
     * check:是否选中 （自己添加）
     */
    private String count;
    private String id;
    private String option;
    private int totalCount;
    private boolean check;
    private int layout = R.layout.item_vote;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getCount() {
        return count;
    }

    public String getId() {
        return id;
    }

    public String getOption() {
        return option;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
