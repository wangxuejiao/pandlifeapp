package com.jiakang.pandlife.widget;

public interface OnRearrangeListener {

	public abstract void onRearrange(int oldIndex, int newIndex);
}
