package com.jiakang.pandlife.info;


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.info.BaseAbsInfo;
import com.jiakang.pandlife.item.PostTypeItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by play on 2016/1/18.
 */
public class PostTypeInfo extends BaseAbsInfo {

    private List<PostTypeItem.DataEntityPostType> dataEntityPostTypeList;

    public List<PostTypeItem.DataEntityPostType> getDataEntityPostTypeList() {
        return dataEntityPostTypeList;
    }

    public void setDataEntityPostTypeList(List<PostTypeItem.DataEntityPostType> dataEntityPostTypeList) {
        this.dataEntityPostTypeList = dataEntityPostTypeList;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Forum&a=Forumtype&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        PostTypeItem postTypeItem = BaseInfo.gson.fromJson(jsonObject.toString(),PostTypeItem.class);
        dataEntityPostTypeList = postTypeItem.getData();
        for (int i = 0; i < dataEntityPostTypeList.size(); i++) {
            dataEntityPostTypeList.get(i).setLayout(R.layout.item_post_type);
        }
    }
}
