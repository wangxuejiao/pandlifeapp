package com.jiakang.pandlife.utils;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.widget.CustomDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * 与WeatherSearch相关，savePath
 */
public class UpdateManager {
	private Context ctx;
	// 提示语
	public String msgUpdate = "     有最新的软件包，赶快下载吧！";
	public String msgDown = "     检测本机未安装此软件，是否下载安装？";
	// 返回的安装包url

	private static String baseDownApkUrl = "http://wang.eezz.cn/apps/";
	private static String updateUrl = baseDownApkUrl + "update_NetNotice.txt";
	public static final String versions = "NetNotice_v1.0.apk";
	public static final String saveBase = "/sdcard/NetNotice/";
	private static String saveFileName = "";
	private static String apkUrl = "";

	private Dialog noticeDialog;
	private Dialog downloadDialog;
	private ProgressDialog checkDialog;
	// private Dialog checkDialog;

	/* 进度条与通知ui刷新的handler和msg常量 */
	private ProgressBar mProgress;
	private static final int DOWN_UPDATE = 1;
	private static final int DOWN_OVER = 2;
	private static final int DOWN_FAIL = 3;
	private static final int CHECK_HAS_NEWAPP = 4;
	private static final int CHECK_NO_NEWAPP = 5;
	private static final int WithOutSD = 7;

	private int progress = 0;
	private boolean interceptFlag = false;
	private boolean showNoUpdateDlg = false;

	// private int netState;

	public UpdateManager(Context ctx) {
		this.ctx = ctx;
	}

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DOWN_UPDATE:
				mProgress.setProgress(progress);
				break;
			case DOWN_OVER:
				// 销毁下载对话框
				downloadDialog.dismiss();
				installApk(saveFileName);
				break;
			case DOWN_FAIL:
				downloadDialog.dismiss();
				interceptFlag = true;
				Toast.makeText(ctx, "更新失败！", 4000).show();
				break;
			case WithOutSD:
				downloadDialog.dismiss();
				Toast.makeText(ctx, "未装载SD卡", 4000).show();
				break;
			case CHECK_HAS_NEWAPP:
				// 销毁检查对话框
				if (checkDialog != null && checkDialog.isShowing()) {
					checkDialog.dismiss();
				}
				apkUrl = baseDownApkUrl + msg.obj.toString();
				// 通知下载
				showNoticeDialog("软件版本更新", msgUpdate, (String) msg.obj, apkUrl);
				break;
			case CHECK_NO_NEWAPP:
				if (checkDialog != null && checkDialog.isShowing()) {
					checkDialog.dismiss();
				}
				// 没有更新下载
				if (showNoUpdateDlg) {
					showNoUpdateDialog();
				}
				break;
			default:
				break;
			}
		};
	};

	/**
	 * 外部检测更新接口：主动检查更新
	 * 
	 * @param showNoUpdateDlg
	 *            有/没有更新都对话框提示
	 * @return void
	 */
	public void checkUpdate(boolean showNoUpdateDlg) {
		if (!Util.hasNet(ctx, false)) {
			return;
		}
		// 显示等待对话框
		showCheckProgressDialog();
		this.showNoUpdateDlg = showNoUpdateDlg;
		new Thread(checkRunnable).start();

	}

	/**
	 * 外部检测更新接口：自动检查更新 有更新才对话框提示，没有不显示提示
	 */
	public void checkUpdate() {
		checkUpdate(false);
	}

	Runnable checkRunnable = new Runnable() {
		@Override
		public void run() {
			String allAppName = null;
			// 读文件
			try {
				URL url = new URL(updateUrl);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.connect();
				// 获取apk长度
				int length = conn.getContentLength();
				InputStream is = conn.getInputStream();
				byte[] c = new byte[500];
				int num = is.read(c);

				allAppName = new String(c, 0, num);
				System.out.println("allAppName为：" + allAppName);

			} catch (Exception e) {
				System.out.println("读文件失败");
				mHandler.sendEmptyMessage(CHECK_NO_NEWAPP);
			}
			if (allAppName == null)
				mHandler.sendEmptyMessage(CHECK_NO_NEWAPP);
			/**
			 * ZangliCalendar_v1.0.apk;ZangliCalendar_v2.0.apk
			 */
			String mStr = allAppName.substring(allAppName.indexOf(versions));
			System.out.println("mStr为：" + mStr);
			if (mStr.indexOf(";") == -1) {
				mHandler.sendEmptyMessage(CHECK_NO_NEWAPP);
			} else {
				String someAppName[] = new String[mStr.length()];
				someAppName = mStr.split(";");

				Message msg = Message.obtain();
				msg.what = CHECK_HAS_NEWAPP;
				msg.obj = someAppName[someAppName.length - 1];

				mHandler.sendMessage(msg);
			}
		}
	};

	// //检查更新对话框
	// private String getCheckUpdateApkName() {}

	private void showCheckProgressDialog() {
		checkDialog = new ProgressDialog(ctx);
		checkDialog.setTitle("正在检查.....");
		// checkDialog.setCancelable(false);//不能用返回键关闭
		checkDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		checkDialog.setIndeterminate(false);// 不显示进度条
		checkDialog.show();
	}

	// 没有更新对话框
	private void showNoUpdateDialog() {
		Builder builder = new Builder(ctx);
		builder.setTitle("软件版本更新");
		builder.setMessage("当前为最新版本了,没有更新版本了！");
		builder.setPositiveButton("知道了", null);
		builder.create().show();
	}

	// 没有更新对话框
	public void showDialog(String title, String msg, String btn1) {
		Builder builder = new Builder(ctx);
		builder.setTitle(title);// "软件版本更新"
		builder.setMessage(msg);// "当前为最新版本了,没有更新版本了！"
		builder.setPositiveButton(btn1, null);// "知道了"
		builder.create().show();
	}

	/**
	 * 
	 * @param saveName
	 *            不要全称：只要文件名，如：NetNotice_v1.0.apk
	 *            会保存在目录/sdcard/NetNotice/NetNotice_v1.0.apk
	 * @param apkUrl
	 *            要ftp地址：http://www.emnet.cn/happy/zangli/NetNotice_v1.0.apk
	 * @return void
	 */
	public void showNoticeDialog(String title, String msg, String saveName, String apkUrl) {
		showNoticeDialog(title, msg, saveName, apkUrl, "下载更新", "一会再说");
	}

	/**
	 * 提醒更新对话框
	 * 
	 * @param saveName
	 *            不要全称：只要文件名，如：NetNotice_v1.0.apk
	 *            会保存在目录/sdcard/NetNotice/NetNotice_v1.0.apk
	 * @param apkUrl
	 *            要ftp地址：http://www.emnet.cn/happy/zangli/NetNotice_v1.0.apk
	 * @param positiveBtn
	 *            积极btn的Text
	 * @param negativeBtn
	 *            消极btn的Text
	 * @return void
	 */
	// public void showNoticeDialog(String title, String msg, final String
	// saveName, final String apkUrl
	// , String positiveBtn, String negativeBtn){
	//
	// AlertDialog.Builder builder = new Builder(ctx);
	// builder.setTitle(title);
	// builder.setMessage(msg);
	// builder.setPositiveButton(positiveBtn, new OnClickListener() {
	// public void onClick(DialogInterface dialog, int which) {
	// dialog.dismiss();
	// showDownloadDialog();
	// saveFileName = saveBase + saveName;
	// //下载apk
	// downloadApk(saveFileName, apkUrl);
	// }
	// });
	// builder.setNegativeButton(negativeBtn, new OnClickListener() {
	// public void onClick(DialogInterface dialog, int which) {
	// dialog.dismiss();
	// }
	// });
	// noticeDialog = builder.create();
	// noticeDialog.show();
	// }

	/**
	 * 取消注册对话框
	 */
	public CustomDialog showNoticeDialog(String title, String msg, final String saveName, final String apkUrl, String positiveBtn, String negativeBtn) {
		final CustomDialog customDialog = Util.getDialog(ctx, title, msg, positiveBtn, negativeBtn);
		customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				customDialog.dismiss();
				showDownloadDialog();
				saveFileName = saveBase + saveName;
				// 下载apk
				downloadApk(saveFileName, apkUrl);
			}
		});
		customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				customDialog.dismiss();
			}
		});
		customDialog.show();
		return customDialog;
	}

	/**
	 * 显示更新进度对话框
	 * 
	 * @return void
	 */
	private void showDownloadDialog() {
		Builder builder = new Builder(ctx);
		builder.setTitle("正在下载...");
		// builder.setTitle("软件版本更新");

		final LayoutInflater inflater = LayoutInflater.from(ctx);
		View v = inflater.inflate(R.layout.progress_dialog, null);
		mProgress = (ProgressBar) v.findViewById(R.id.progress);

		builder.setView(v);
		builder.setNegativeButton("取消", new OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				interceptFlag = true;
			}
		});
		downloadDialog = builder.create();
		downloadDialog.show();
	}

	private class MyRunnable implements Runnable {
		private String savefilename;
		private String apkUrl;

		// public MyRunnable(String savefilename) {
		// this.savefilename = savefilename;
		// }
		public MyRunnable(String savefilename, String apkUrl) {
			this.savefilename = savefilename;
			this.apkUrl = apkUrl;
		}

		public void run() {
			try {
				System.out.println("apkUrl为：" + this.apkUrl);
				URL url = new URL(this.apkUrl);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				// touxiang = new
				// BitmapDrawable(hc.getInputStream());//连接对象hc获取流

				conn.connect();
				// 获取apk长度
				int length = conn.getContentLength();
				InputStream is = conn.getInputStream();

				// String apkFile = saveFileName;
				File ApkFile = new File(this.savefilename);
				// 文件输出流，记得添加写SD卡权限android.permission.WRITE_EXTERNAL_STORAGE
				FileOutputStream fos = new FileOutputStream(ApkFile);

				int count = 0;
				byte buf[] = new byte[1024];

				do {
					// is--->buf--->fos
					int numread = is.read(buf);
					count += numread;
					// 14/677再乘以100=10
					progress = (int) (((float) count / length) * 100);
					// 更新进度
					mHandler.sendEmptyMessage(DOWN_UPDATE);

					if (numread <= 0) {
						// 下载完成通知安装
						mHandler.sendEmptyMessage(DOWN_OVER);
						break;
					}
					fos.write(buf, 0, numread);

				} while (!interceptFlag);// 点击取消就停止下载.

				fos.close();
				is.close();

			} catch (Exception e) {
				System.out.println("下载失败！！e为：" + e);
				mHandler.sendEmptyMessage(DOWN_FAIL);
				// 下载失败！！e为：java.io.FileNotFoundException:
				// /NetNotice_v1.1.apkNetNotice_v1.1.apk (Read-only file system)

			}

		}
	};// apkUrl为：http://www.emnet.cn/happy/zangli/NetNotice_v1.1.apk

	/**
	 * 检查SD卡
	 */
	public boolean hasSDCard() {
		return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	}

	/**
	 * 下载apk
	 * 
	 * @param url
	 */

	private void downloadApk(String saveName, String apkUrl) {
		if (hasSDCard()) {
			File file = new File(saveBase);
			if (!file.exists()) {
				file.mkdir();
			}
			MyRunnable mdownApkRunnable = new MyRunnable(saveName, apkUrl);
			new Thread(mdownApkRunnable).start();
		} else {
			mHandler.sendEmptyMessage(WithOutSD);
		}
	}

	/**
	 * 安装apk
	 * 
	 * @param url
	 */
	private void installApk(String saveName) {
		// 文件对象
		File apkfile = new File(saveName);
		if (!apkfile.exists()) {
			return;
		}
		Intent i = new Intent(Intent.ACTION_VIEW);
		// 绑定数据Data和形式；application/vnd.android.package-archive：后缀在nginx的mime.types中的配置：
		i.setDataAndType(Uri.parse("file://" + apkfile.toString()), "application/vnd.android.package-archive");
		System.out.println("-------------------->>" + Uri.parse("file://" + apkfile.toString()));
		// -------------------->>file:///sdcard/updatedemo/UpdateDemoRelease.apk
		System.out.println("++++++++++++++++>>" + apkfile.toString());
		// ++++++++++++++++>>/sdcard/updatedemo/UpdateDemoRelease.apk
		ctx.startActivity(i);
	}
}
