package com.jiakang.pandlife.config;

import android.content.Context;
import android.content.SharedPreferences;

import com.jiakang.pandlife.PandLifeApp;

import java.util.HashMap;
import java.util.Map;


/**
 * @author：xuwangwang E-mail: 541765907@qq.com
 * @date：2014年5月12日 下午2:42:24
 * @version 1.0 String Tag = "ConfigSPF中：";
 */
public class ConfigSharedPreferences {

	private Map<String, SharedPreferences> spfMap = new HashMap<String, SharedPreferences>();
	private Context ctx;
	private SharedPreferences preferences;
	private static ConfigSharedPreferences configSharedPreferences;

	public final static String NAME_HAFA = "hafa";
	public final static String NAME_BASEDATA = "baseData";
	public final static String NAME_SETTINGS = "settings";
	public final static String NAME_AUTOCOMPLETE = "autoComplete";
	public final static String NAME_CACHE = "cache";
	public final static String NAME_ACCOUNT = "account";

	public static ConfigSharedPreferences getInstance() {
		if (configSharedPreferences == null) {
			configSharedPreferences = new ConfigSharedPreferences();
		}
		return configSharedPreferences;
	}

	public SharedPreferences getConfigSharedPreferences(String spfName) {
		if (spfMap.containsKey(spfName)) {
			return spfMap.get(spfName);
		} else {
			ctx = PandLifeApp.getInstance();
			SharedPreferences preferences = ctx.getSharedPreferences(spfName, Context.MODE_APPEND);
			spfMap.put(spfName, preferences);
			return preferences;
		}
	}

	private ConfigSharedPreferences() {
		// preferences = ctx.getSharedPreferences("MatchMaker",
		// Context.MODE_APPEND);
		// baseDataSpf = ctx.getSharedPreferences("baseData",
		// Context.MODE_APPEND);
		// changeBgSpf = ctx.getSharedPreferences("chatBg",
		// Context.MODE_PRIVATE);
		// settingSpf = ctx.getSharedPreferences("settings",
		// Context.MODE_PRIVATE);
		// autoCompleteTextSpf = ctx.getSharedPreferences("autoComplete",
		// Context.MODE_PRIVATE);
	}

}
