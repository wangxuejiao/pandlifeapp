package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/1/12.
 */
public class GoodsItem extends Item implements Serializable{

    private String name;
    private String content;
    private int number;
    private float price;
    private String pic;

    private int layout = R.layout.item_group_purchase_indent;
    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
