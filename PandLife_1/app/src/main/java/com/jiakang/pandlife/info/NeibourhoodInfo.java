package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.db.DataCacheDBHelper;
import com.jiakang.pandlife.item.NearbyStationItem;
import com.jiakang.pandlife.item.NeibourhoodItem;
import com.jiakang.pandlife.item.NeighborhoodItem;
import com.jiakang.pandlife.utils.DataCache;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by play on 2016/1/18.
 */
public class NeibourhoodInfo extends BaseAbsInfo{

    private int pageIndex;
    private int pageSize;
    private NeibourhoodItem neibourhoodItem;
    private List<NeibourhoodItem.DataEntityNeibourhood> dataEntityNeibourhoodList = new ArrayList<>();

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public NeibourhoodItem getNeibourhoodItem() {
        return neibourhoodItem;
    }

    public void setNeibourhoodItem(NeibourhoodItem neibourhoodItem) {
        this.neibourhoodItem = neibourhoodItem;
    }

    public List<NeibourhoodItem.DataEntityNeibourhood> getDataEntityNeibourhoodList() {
        return dataEntityNeibourhoodList;
    }

    public void setDataEntityNeibourhoodList(List<NeibourhoodItem.DataEntityNeibourhood> dataEntityNeibourhoodList) {
        this.dataEntityNeibourhoodList = dataEntityNeibourhoodList;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Forum&a=Lists&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("pageIndex",pageIndex);
            json.put("pageSize",pageSize);
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        try {
            if(jsonObject.getInt("status") == 1){

                neibourhoodItem = BaseInfo.gson.fromJson(jsonObject.toString(), NeibourhoodItem.class);
                dataEntityNeibourhoodList.clear();
                dataEntityNeibourhoodList = neibourhoodItem.getData();
                for (int i = 0; i < dataEntityNeibourhoodList.size(); i++) {
                    dataEntityNeibourhoodList.get(i).setLayout(R.layout.item_neighbourhood);
                }

                DataCache dataCache = DataCache.get(PandLifeApp.getInstance());
                String key = "neibourhoodItem";
                dataCache.put(key,jsonObject,10 * 60);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
