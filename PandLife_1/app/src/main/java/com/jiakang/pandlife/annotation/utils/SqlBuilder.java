package com.jiakang.pandlife.annotation.utils;

import android.text.TextUtils;

import com.jiakang.pandlife.annotation.info.Id;
import com.jiakang.pandlife.annotation.info.TableField;
import com.jiakang.pandlife.annotation.info.TableInfo;

import java.util.Collection;
import java.util.Map;


public class SqlBuilder {

	private static String getDeleteSqlBytableName(String tableName) {
		return "DELETE FROM " + tableName;
	}

	private static String getSelectSqlByTableName(String tableName) {
		return new StringBuffer("SELECT * FROM ").append(tableName).toString();
	}

	public static String getCreatTableSQL(Class<?> clazz) {
		return getCreatTableSQL(clazz, null);
	}
	
	public static String getCreatTableSQL(Class<?> clazz, String tableName) {
		TableInfo table = TableInfo.get(clazz);
		StringBuffer strSQL = new StringBuffer();
		strSQL.append("CREATE TABLE IF NOT EXISTS ");
		if (TextUtils.isEmpty(tableName)) {
			strSQL.append(table.getTableName());
		}else {
			strSQL.append(tableName);
		}
		strSQL.append(" ( ");

		Id id = table.getId();
		Class<?> primaryClazz = id.getDataType();
		if (primaryClazz == int.class || primaryClazz == Integer.class)
			strSQL.append("[" + id.getColumn() + "]").append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
		else
			strSQL.append("[" + id.getColumn() + "]").append(" TEXT PRIMARY KEY,");

		Collection<TableField> tableFields = table.propertyMap.values();
		for (TableField tableField : tableFields) {
			strSQL.append(" [" + tableField.getColumn());
			strSQL.append("] " + tableField.getType());
			if (tableField.getDefaultValue() != null && tableField.getDefaultValue().length() > 0)
				strSQL.append(" DEFAULT(" + tableField.getDefaultValue() + ")");
			strSQL.append(", ");
		}
		strSQL.deleteCharAt(strSQL.length() - 2);
		strSQL.append(" )");
		return strSQL.toString();
	}
	
	
	

	/**
	 * @param key
	 * @param value
	 * @return eg1: name='afinal' eg2: id=100
	 */
	private static String getPropertyStrSql(String key, Object value) {
		StringBuffer sbSQL = new StringBuffer(key).append("=");
		if (value instanceof String || value instanceof java.util.Date || value instanceof java.sql.Date) {
			sbSQL.append("'").append(value).append("'");
		} else {
			sbSQL.append(value);
		}
		return sbSQL.toString();
	}
	
	
	

//	/**
//	 * 生成sql语句，并且插入数据库 contentValues的键值对，键含有非法字符，如from、to等，使用此方法
//	 * 
//	 * @param table
//	 * @param contentValues
//	 */
//	private static String getInsertSql(String table, Map<String, Object> maps) {
//		return getInsertSql(table, maps, null, false);
//	}
//	
//
//	/**
//	 * 生成sql语句，并且插入数据库 contentValues的键值对，键含有非法字符，如from、to等，使用此方法
//	 * 
//	 * @param table
//	 * @param contentValues
//	 */
//	public static String getInsertSql(String table, Map<String, Object> maps, String compositeKey, boolean orReplace) {
//		StringBuilder sql = new StringBuilder();
//		sql.append("INSERT ");
//		if(orReplace)
//			sql.append("or replace ");
//		sql.append(" INTO ");
//		sql.append(table);
//		sql.append('(');
//
//		Object[] bindArgs = null;
//		int size = (maps != null && maps.size() > 1) ? maps.size()-1 : 0;
//		if (size > 0) {
//			bindArgs = new Object[size];
//			int i = 0;
//			int compositeIndex = -1;
//			for (String colName : maps.keySet()) {
//				// 过滤掉主键
//				if(colName.equals("_id")) continue;
//				sql.append((i > 0) ? "," : "");
//				sql.append("[");
//				if (compositeKey != null && colName.equals(compositeKey))
//					compositeIndex = i;
//				sql.append(colName);
//				sql.append("]");
//				bindArgs[i++] = maps.get(colName);
//			}
//			sql.append(')');
//			sql.append(" VALUES (");
//			for (i = 0; i < size; i++) {
//				if (compositeIndex != i)
//					sql.append((i > 0) ? ", '" : "'");
//				else
//					sql.append(" ,");
//				sql.append(bindArgs[i]);
//				if (compositeIndex != i)
//					sql.append("'");
//			}
//		}
//		sql.append(')');
//		XYLog.i("DataDBManage", "insertData方法中-------->：sql为：" + sql);
//		return sql.toString();
//	}

	
	
	/**
	 * 生成插入sql语句
	 * 
	 * @param table
	 * @param maps
	 * @param compositeKey 复合条件,不加单引号
	 * @param orReplace 
	 * @param hasPrimary 是否包含主键
	 */
	public static String getInsertSql(String table, Map<String, Object> maps) {
		return getInsertSql(table, maps, null, true, true);
	}
	
	/**
	 * 生成插入sql语句
	 * 
	 * @param table
	 * @param maps
	 * @param compositeKey 复合条件,不加单引号
	 * @param orReplace 
	 * @param hasPrimary 是否包含主键
	 */
	public static String getInsertSql(String table, Map<String, Object> maps, String compositeKey, 
			boolean orReplace) {
		return getInsertSql(table, maps, compositeKey, orReplace, false, "");
	}
	/**
	 * 生成插入sql语句
	 * 
	 * @param table
	 * @param maps
	 * @param compositeKey 复合条件,不加单引号
	 * @param orReplace 
	 * @param hasPrimary 是否包含主键
	 */
	public static String getInsertSql(String table, Map<String, Object> maps, String compositeKey, 
			boolean orReplace, boolean hasPrimary) {
		return getInsertSql(table, maps, compositeKey, orReplace, hasPrimary, "_id");
	}
	/**
	 * 生成sql语句
	 * 
	 * @param table
	 * @param maps
	 * @param compositeKey 复合条件,不加单引号
	 * @param orReplace 
	 * @param hasPrimary 是否包含主键(主键int型不加单引号)；生成的sql语句中是否包含主键键值对
	 * @param primaryKey 主键名：如_id
	 */
	public static String getInsertSql(String table, Map<String, Object> maps, String compositeKey, 
			boolean orReplace, boolean hasPrimary, String primaryKey) {
		if (maps == null) 
			return "";
		// 如果maps中没有名为primaryKey的主键，高层一定搞错了，返回""
		if (primaryKey != null && primaryKey.length() > 0 && !maps.containsKey(primaryKey)) 
			return "";
		// 如果主键为0或空
		if (maps.get(primaryKey).equals(0) || maps.get(primaryKey).equals("0") || maps.get(primaryKey).equals("")) 
			hasPrimary = false;
		
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT ");
		if(orReplace)
			sql.append("or replace ");
		sql.append(" INTO ");
		sql.append(table);
		sql.append('(');

		Object[] bindArgs = null;
		int size = (hasPrimary && maps.containsKey(primaryKey)) ? maps.size() : maps.size() - 1;
		if (size > 0) {
			bindArgs = new Object[size];
			int i = 0;
			int _idIndex = 0;
			int compositeIndex = -1;
			for (String colName : maps.keySet()) {
				if (hasPrimary) {
					if(colName.equals(primaryKey)) 
						_idIndex = i;
				}else {
					// 过滤掉主键
					if(colName.equals(primaryKey)) continue;
				}
				sql.append((i > 0) ? "," : "");
				sql.append("[");
				if (compositeKey != null && colName.equals(compositeKey)){
					compositeIndex = i;
					// 复合sql中不替换特殊字符串
					bindArgs[i++] = maps.get(colName);
				}else {
					// 普通字段中替换特殊字符串'
					Object value = maps.get(colName);
					if (value instanceof String) {
						bindArgs[i++] = value.toString().replaceAll("'", "''");
					}else {
						bindArgs[i++] = value;
					}
				}
				sql.append(colName);
				sql.append("]");
			}
			sql.append(')');
			sql.append(" VALUES (");
			for (i = 0; i < size; i++) {
				if (compositeIndex == i || (hasPrimary && _idIndex == i && maps.get(primaryKey) instanceof Integer) ){
					sql.append((i > 0) ? " ," : "");
				}else {
					sql.append((i > 0) ? ", '" : "'");
				}
				sql.append(bindArgs[i]);
				if (compositeIndex == i || (hasPrimary && _idIndex == i && maps.get(primaryKey) instanceof Integer) ){
					
				}else {
					sql.append("'");
				}
			}
			sql.append(')');
		}
//		System.out.println("TestOptAnnotation中：sql为：" + sql );
		return sql.toString();
	}
}
