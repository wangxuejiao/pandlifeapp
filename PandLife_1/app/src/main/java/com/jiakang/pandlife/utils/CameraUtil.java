package com.jiakang.pandlife.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

/**
 * Created by play on 2016/1/11.
 */
public class CameraUtil {

    /**
     * 启动照相机
     * @param activity
     * @param requestCode
     * @param outImageUri
     */
    public static void startCamera(Activity activity,int requestCode,Uri outImageUri){

        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,outImageUri);
        activity.startActivityForResult(intent,requestCode);

    }

    public static void cropImageUri(Activity activity,Uri uri,int aspectX,int aspectY, int outputX,int outputY,int requestCode){

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri,"image/*");
        intent.putExtra("crop", true);
        intent.putExtra("aspectX", aspectX);
        intent.putExtra("aspectY", aspectY);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("scale", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        activity.startActivityForResult(intent, requestCode);
    }
}
