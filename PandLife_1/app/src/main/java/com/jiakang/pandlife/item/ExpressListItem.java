package com.jiakang.pandlife.item;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.annotation.sqlite.Id;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.db.DataCacheDBHelper;

/**
 * Created by Administrator on 2016/1/11.
 */
@Table(name = DataCacheDBHelper.TAB_MYEXPRESS_CACHE)
public class ExpressListItem extends Item{

    /** 数据库id	Int(10) */
    @Id
    private int _id;
    /** 运单号	Varchar(32) */
    private String mailno;
    /** 快递类型	Varchar(32) */
    private String companyname;
    /** 包裹编号	Varchar(32) */
    private String number;
    /** 手机号码	varchar手机号 */
    private String mobile;
    /** 包裹到达时间 &包裹取货时间	Varchar(32) */
    private String gmtcreate;

//    /** 默认的id */
//    private String id;
    /** 包裹状态（未取1，已取2） */
    private String isget = "1";

    /** 插入数据库时的时间 */
    private long currentTime = System.currentTimeMillis();
    /** 保存的时间 */
    private long saveTime = 86400000;

    private int layout = R.layout.item_my_package_unget;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    @Override
    public int insert() {
        PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
        return 1;
    }

    public String getMailno() {
        return mailno;
    }

    public void setMailno(String mailno) {
        this.mailno = mailno;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGmtcreate() {
        return gmtcreate;
    }

    public void setGmtcreate(String gmtcreate) {
        this.gmtcreate = gmtcreate;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public long getSaveTime() {
        return saveTime;
    }

    public void setSaveTime(long saveTime) {
        this.saveTime = saveTime;
    }

    public String getIsget() {
        return isget;
    }

    public void setIsget(String isget) {
        this.isget = isget;
    }
}
