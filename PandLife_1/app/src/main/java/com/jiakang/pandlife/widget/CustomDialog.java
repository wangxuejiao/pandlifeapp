package com.jiakang.pandlife.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.jiakang.pandlife.R;


/**
 * 自定义Dialog
 * 
 * @author ww
 */
public class CustomDialog extends Dialog {

	private static int default_height = 200;
	private static int default_width = 280;
	private int layout;
	private String okStr;
	private String cancelStr;

	public CustomDialog(Context context, String title, String msg, String okStr, String cancelStr) {
		this(context, title, msg, okStr, cancelStr, Gravity.CENTER);
	}

	public CustomDialog(Context context, String title, String msg, String okStr, String cancelStr, int msgGravity) {
		this(context, R.layout.mydialog);
		initDefDlg(title, msg, okStr, cancelStr, msgGravity);
	}

	public CustomDialog(Context context, int layout) {
		this(context, default_width, default_height, layout, R.style.MyDialog);
	}

	public CustomDialog(Context context, int default_width, int default_height, int layout, int style) {
		super(context, style);
		this.layout = layout;

		View view = getLayoutInflater().inflate(layout, null);
		// 透明背景
//		Drawable myDrawable = context.getResources().getDrawable(R.mipmap.dialog_root_bg);
//		myDrawable.setAlpha(150);
//		view.setBackgroundDrawable(myDrawable);
		setContentView(view);

		Window window = getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		float density = getDensity(context);
		params.width = (int) (default_width * density);
		// params.height = (int) (default_height * density);
		// view.measure(0, 0);
		// params.width = view.getMeasuredWidth();
		// params.height = view.getMeasuredHeight();
		params.gravity = Gravity.CENTER;
		window.setAttributes(params);

		// //此处可以设置dialog显示的位置
		// window.setGravity(Gravity.BOTTOM);
		// //添加动画
		// window.setWindowAnimations(R.style.popup_in_out);
	}

	private float getDensity(Context context) {
		Resources resources = context.getResources();
		DisplayMetrics dm = resources.getDisplayMetrics();
		return dm.density;
	}

	/**
	 * 生成默认的对话框{@link R.layout.mydialog}
	 * 
	 * @param title
	 * @param msg
	 * @param okStr
	 * @param cancelStr
	 * @return void
	 */
	private void initDefDlg(String title, String msg, String okStr, String cancelStr, int msgGravity) {
		this.okStr = okStr;
		this.cancelStr = cancelStr;

		TextView titleTV = (TextView) findViewById(R.id.title);
		TextView msgTV = (TextView) findViewById(R.id.message);
		titleTV.setText(title);
		msgTV.setText(msg);
		msgTV.setGravity(msgGravity);

		Button okBtn = (Button) findViewById(R.id.ok);
		Button cancelBtn = (Button) findViewById(R.id.cancel);
		if (okStr != null) {
			okBtn.setText(okStr);
		} else {
			okBtn.setVisibility(View.GONE);
		}

		if (cancelStr != null) {
			cancelBtn.setText(cancelStr);
		} else {
			cancelBtn.setVisibility(View.GONE);
		}

	}

	public void setMsg(String msg) {
		TextView msgTV = (TextView) findViewById(R.id.message);
		msgTV.setText(msg);
	}

	/**
	 * 为指定id控件设置点击监听
	 * 
	 * @param id
	 * @param listener
	 */
	public CustomDialog setOnClickListener(int id, View.OnClickListener listener) {
		View view = findViewById(id);
		if (view != null && listener != null)
			view.setOnClickListener(listener);
		return this;
	}

	// public CustomDialog setOnPositiveClickListener(View.OnClickListener listener)
	// {
	// return setOnClickListener(R.id.ok, listener);
	// }
	//
	// public CustomDialog setOnNegativeClickListener(View.OnClickListener listener)
	// {
	// return setOnClickListener(R.id.cancel, listener);
	// }

}