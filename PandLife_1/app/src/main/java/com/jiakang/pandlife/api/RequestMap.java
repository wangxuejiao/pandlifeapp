package com.jiakang.pandlife.api;

import com.jiakang.pandlife.BaseInfo;

import org.json.JSONObject;

/**
 * @author：xuwangwang E-mail: 541765907@qq.com
 * @date：2014年7月1日 下午2:55:12
 * @version 1.0 String Tag = "RequestMap中：";
 */
public class RequestMap extends JSONObject {

	private String url;

	public RequestMap(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public String getRequestStr(){
		return BaseInfo.data;
	}

	@Override
	public JSONObject put(String name, boolean value) {
		try {
			return super.put(name, value);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public JSONObject put(String name, int value) {
		try {
			return super.put(name, value);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public JSONObject put(String name, double value) {
		try {
			return super.put(name, value);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public JSONObject put(String name, long value) {
		try {
			return super.put(name, value);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public JSONObject put(String name, Object value) {
		try {
			return super.put(name, value);
		} catch (Exception e) {
			return null;
		}
	}
}
