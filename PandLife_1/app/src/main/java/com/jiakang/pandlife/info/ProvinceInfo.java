package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import android.text.TextUtils;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.ProvinceItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取省份接口
 *
 * @author ww
 *
 */
public class ProvinceInfo extends BaseAbsInfo {

    private static final String TAG = "ProvinceInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<ProvinceItem> allItems = new ArrayList<ProvinceItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Address&a=Province" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                ProvinceItem provinceItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    provinceItem = BaseInfo.gson.fromJson(itemStr, ProvinceItem.class);
                    provinceItem.setLayout(R.layout.item_select_single_left);
                    // 入库
//                    recommendRecruitItem.insert();
                    allItems.add(provinceItem);
                }

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<ProvinceItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<ProvinceItem> allItems) {
        this.allItems = allItems;
    }
}
