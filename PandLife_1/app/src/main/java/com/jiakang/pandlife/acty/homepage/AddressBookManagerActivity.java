package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.personalcenter.MyAddressBookActivity_;
import com.jiakang.pandlife.acty.personalcenter.RecipientsAddressBookActivity_;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.GetRecipientsListInfo;
import com.jiakang.pandlife.info.GetSenderListInfo;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.GroupPurchaseItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.List;

/**
 * 收件人地址薄
 */
@EActivity(R.layout.activity_address_book_manager)
public class AddressBookManagerActivity extends BaseActy {
    protected static final String TAG = "AddressBookManagerActivity";

    @ViewById(R.id.aabm_lv_address)
    ListView addressLV;

    private ItemAdapter mItemAdapter;

    /**5.获取寄件人列表接口*/
    private GetSenderListInfo mGetSenderListInfo = new GetSenderListInfo();
    /**5.获取收件人列表接口*/
    private GetRecipientsListInfo mGetRecipientsListInfo = new GetRecipientsListInfo();

    private String sendOrObtain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void intiVar(){
        initTitleBar(R.id.aabm_tb_title, "地址管理", null, "管理");

        bindView();

    }

    private void bindView(){
        mItemAdapter = new ItemAdapter(mContext);
        addressLV.setAdapter(mItemAdapter);

        Intent intent = getIntent();
        sendOrObtain = intent.getExtras().getString("sendOrObtain");
        bindInfo(sendOrObtain);

    }

    private void bindInfo(String sendOrObtain){
        if (("sender").equals(sendOrObtain)){
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mGetSenderListInfo, new AbsOnRequestListener(mContext) {
                @Override
                public void onRequestSuccess(int stat, JSONObject jsonObject) {
                    super.onRequestSuccess(stat, jsonObject);
                    try {
                        if (stat == 1) {
                            mItemAdapter.clear();
                            if (mGetSenderListInfo.getAllItems().size() == 0) {
                                return;
                            }
                            mItemAdapter.addItems((List) mGetSenderListInfo.getAllItems());
                            mItemAdapter.notifyDataSetChanged();

                            addressLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    ListView listView = (ListView) parent;
                                    AddressItem senderAddressItem = (AddressItem) listView.getItemAtPosition(position);
                                    Intent intentSender = new Intent();
                                    intentSender.putExtra("senderAddressItem", senderAddressItem);
                                    setResult(RESULT_OK, intentSender);
                                    finish();
                                }
                            });
                        } else {
                        }
                    } catch (Exception e) {

                    }
                }
            });
        }else {
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mGetRecipientsListInfo, new AbsOnRequestListener(mContext) {
                @Override
                public void onRequestSuccess(int stat, JSONObject jsonObject) {
                    super.onRequestSuccess(stat, jsonObject);
                    try {
                        if (stat == 1) {
                            mItemAdapter.clear();
                            if (mGetRecipientsListInfo.getAllItems().size() == 0) {
                                return;
                            }
                            mItemAdapter.addItems((List) mGetRecipientsListInfo.getAllItems());
                            mItemAdapter.notifyDataSetChanged();

                            addressLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    ListView listView = (ListView) parent;
                                    AddressItem recipientsAddressItem = (AddressItem) listView.getItemAtPosition(position);
                                    Intent intentSender = new Intent();
                                    intentSender.putExtra("recipientsAddressItem", recipientsAddressItem);
                                    setResult(RESULT_OK, intentSender);
                                    finish();
                                }
                            });
                        } else {
                        }
                    } catch (Exception e) {

                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if((requestCode == Constant.StaticCode.REQUEST_SENDER_ADDRESS) && (resultCode == RESULT_CANCELED)){
            bindInfo("sender");
        }
        if((requestCode == Constant.StaticCode.REQUEST_RECEIVER_ADDRESS) && (resultCode == RESULT_CANCELED)){
            bindInfo("obtain");
        }

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.it_btn_right:
                if (("sender").equals(sendOrObtain)){
                    Intent intentMyAddress = new Intent(mContext, MyAddressBookActivity_.class);
                    startActivityForResult(intentMyAddress, Constant.StaticCode.REQUEST_SENDER_ADDRESS);
                }else{
                    Intent intentRecipientsAddress = new Intent(mContext, RecipientsAddressBookActivity_.class);
                    startActivityForResult(intentRecipientsAddress, Constant.StaticCode.REQUEST_RECEIVER_ADDRESS);
                }
                break;
        }
    }
}
