package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.GoodsItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.NearbyMerchantItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

/**
 * Created by play on 2016/1/6.
 */
public class NearbyMerchantItemView extends AbsRelativeLayout {

    private ImageView imageIV;
    private TextView shopNameTV;
    private TextView contentTV;
    private TextView manageKindTV;
    private TextView salesNumberTV;

    public NearbyMerchantItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NearbyMerchantItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        imageIV = (ImageView)findViewById(R.id.inm_iv_image);
        shopNameTV = (TextView) findViewById(R.id.inm_tv_shop_name);
        contentTV = (TextView) findViewById(R.id.inm_tv_content);
        manageKindTV = (TextView) findViewById(R.id.inm_tv_manage_kind);
        salesNumberTV = (TextView) findViewById(R.id.inm_tv_sales_number);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);
        NearbyMerchantItem nearbyMerchantItem = (NearbyMerchantItem)item;
        ImageLoaderUtil.displayImage(nearbyMerchantItem.getHead(), imageIV, R.mipmap.default_image);
        shopNameTV.setText(nearbyMerchantItem.getNick());
        contentTV.setText(nearbyMerchantItem.getOpentime());
        manageKindTV.setText(nearbyMerchantItem.getDescription());
        salesNumberTV.setText("| 月销量"+nearbyMerchantItem.getCount());

    }
}
