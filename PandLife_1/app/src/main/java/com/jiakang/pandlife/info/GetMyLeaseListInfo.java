package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.ExpressDetailItem;
import com.jiakang.pandlife.item.MyLeaseItem;
import com.jiakang.pandlife.item.RedPackageItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 26.我的租借信息列表
 *
 * @author ww
 *
 */
public class GetMyLeaseListInfo extends BaseAbsInfo {

    private static final String TAG = "GetMyLeaseListInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<MyLeaseItem> allItems = new ArrayList<MyLeaseItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Server&a=Getmyleaselist" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
//            json.put("token", "ff0a6c74e7b4d08b659f9e2737a4ac5c");
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                MyLeaseItem myLeaseItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    myLeaseItem = BaseInfo.gson.fromJson(itemStr, MyLeaseItem.class);
                    allItems.add(myLeaseItem);
                }
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<MyLeaseItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<MyLeaseItem> allItems) {
        this.allItems = allItems;
    }
}
