package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.GetTuanDetailInfo;
import com.jiakang.pandlife.item.GroupDetailStationItem;
import com.jiakang.pandlife.item.GroupPurchaseDetailItem;
import com.jiakang.pandlife.item.GroupPurchaseItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.widget.Banner;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 团购详情
 */
@EActivity(R.layout.activity_group_purchase_detail)
public class TomorrowAdvanceDetailActivity extends BaseActy {
    protected static final String TAG = "GroupPurchaseDetailActivity";

    @ViewById(R.id.agpd_iv_head)
    Banner banner;
    @ViewById(R.id.agpd_tv_goods_name)
    TextView goodsNameTV;
    @ViewById(R.id.agpd_tv_join_number)
    TextView joinNumberTV;
    @ViewById(R.id.agpd_tv_stop_group)
    TextView groupStatusTV;
    @ViewById(R.id.agpd_tv_stop_timer)
    Chronometer diffStartDateTV;
    @ViewById(R.id.agpd_tv_my_service_site)
    TextView selectSiteTitleTV;
    @ViewById(R.id.agpd_tv_service_site)
    TextView siteNameTV;
    @ViewById(R.id.agpd_tv_service_site_stock)
    TextView siteStockTV;
    @ViewById(R.id.agpd_tv_now_sum)
    TextView nowSumTV;
    @ViewById(R.id.agpd_tv_old_sum)
    TextView oldSumTV;
    @ViewById(R.id.agpd_tv_goods_number_subtract)
    ImageView goodsNumberSubtractIV;
    @ViewById(R.id.agpd_et_goods_number)
    EditText goodsNumberET;
    @ViewById(R.id.agpd_tv_goods_number_add)
    ImageView goodsNumberAddIV;
    @ViewById(R.id.agpd_tv_introduce)
    TextView introduceTV;
    @ViewById(R.id.agpd_tv_cart_number)
    TextView cartNumberTV;
    @ViewById(R.id.agpd_tv_count_sum)
    TextView countSumTV;
    @ViewById(R.id.agpd_btn_join_group)
    Button joinGroupBN;

    /** 记录商品数目的 */
    private int goodsNumber = 0;

    /** 获取商品详细信息的接口 */
    private GetTuanDetailInfo mGetTuanDetailInfo = new GetTuanDetailInfo();

    /** 商品详情实体 */
    private GroupPurchaseDetailItem mGroupPurchaseDetailItem;

    /** 详情中站点列表 */
    private List<GroupDetailStationItem> allItems = new ArrayList<GroupDetailStationItem>();

    /** 团购详情中已选择的站点 */
    private GroupDetailStationItem mGroupDetailStationItem;

    private SimpleDateFormat mTimeFormat;
    private long mTime;
    private long mNextTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.agpd_tb_title, "团购详情");
        bindView();

    }

    private void bindView(){
        Intent intent = getIntent();
        GroupPurchaseItem groupPurchaseItem = (GroupPurchaseItem)intent.getExtras().get("groupPurchaseItem");
        int id = groupPurchaseItem.getId();
        bindInfo(id);
        banner.setOnClickListener(this);
        int[] imagesRes = {R.mipmap.a, R.mipmap.b, R.mipmap.c,
                R.mipmap.d, R.mipmap.e};
        banner.setImagesRes(imagesRes);


//        selectSiteTV.setOnClickListener(this);
        findViewById(R.id.agpd_rl_my_service_site).setOnClickListener(this);
        goodsNumberSubtractIV.setOnClickListener(this);
        goodsNumberAddIV.setOnClickListener(this);
        joinGroupBN.setOnClickListener(this);


    }

    private void bindInfo(int id){
        mGetTuanDetailInfo.setId(id);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mGetTuanDetailInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try{
                    mGroupPurchaseDetailItem = mGetTuanDetailInfo.getmGroupPurchaseDetailItem();
                    allItems = mGroupPurchaseDetailItem.getStation();

                    goodsNameTV.setText(mGroupPurchaseDetailItem.getTitle());
                    joinNumberTV.setText("未开始");
                    nowSumTV.setText("￥" + mGroupPurchaseDetailItem.getTprice());
                    oldSumTV.setText("￥" + mGroupPurchaseDetailItem.getPrice());
                    introduceTV.setText(mGroupPurchaseDetailItem.getDetailed());
                    notifyCartNumber();//初始化购物车中的商品数量

                    long startTime = Long.parseLong(mGroupPurchaseDetailItem.getStarttime()) * 1000;
                    long endTime = Long.parseLong(mGroupPurchaseDetailItem.getEndtime()) * 1000;
//                  openDateTV.setText(Util.getFormatedDateTime("HH:mm", startTime) + "开团");
                    long nowTime = System.currentTimeMillis();
                    //团购未开始
                    if (nowTime < startTime) {
                        groupStatusTV.setText("距离团购开始");
                        joinGroupBN.setEnabled(false);
                        joinGroupBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_border_bg));
                        mNextTime = (startTime - nowTime)/1000;
                        // 自动生成的构造函数存根
                        mTimeFormat = new SimpleDateFormat("HH:mm:ss");
                        diffStartDateTV.setOnChronometerTickListener(listener);
                        diffStartDateTV.start();
                    }
                    //团购已开始，未结束
                    else if ((nowTime > startTime) && (nowTime < endTime)){
                        joinGroupBN.setEnabled(true);
                        joinGroupBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_yellow_bg));
                        groupStatusTV.setText("距离团购结束");
                        mNextTime = (endTime - nowTime)/1000;
                        // 自动生成的构造函数存根
                        mTimeFormat = new SimpleDateFormat("HH:mm:ss");
                        diffStartDateTV.setOnChronometerTickListener(listener);
                        diffStartDateTV.start();
                    }
                    //团购结束
                    else{
                        groupStatusTV.setText("距离团购结束");
                        mNextTime = 0;
                        diffStartDateTV.setText("00:00:00");
                        joinGroupBN.setEnabled(false);
                        joinGroupBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_border_bg));
                    }

                    /*new CountDownTimer(endTime - nowTime,1000){
                        @Override
                        public void onFinish() {
                            //done
                        }
                        @Override
                        public void onTick(long arg0) {
                            //每1000毫秒回调的方法
                            diffStartDateTV.setText(Util.getFormatedDateTime("HH:mm:ss", arg0));
                        }
                    }.start();*/
                }catch (Exception e){

                }
            }
        });
    }

    /**
     * 更新和显示购物车里商品的数量和价格
     */
    private void notifyCartNumber(){

        String goodsNumberStr = goodsNumberET.getText().toString();
        if(TextUtils.isEmpty(goodsNumberStr) || ("0").equals(goodsNumberStr)){
            cartNumberTV.setVisibility(View.GONE);
            countSumTV.setText("￥0.0");
        }else{
            cartNumberTV.setText(goodsNumberET.getText());
            cartNumberTV.setVisibility(View.VISIBLE);
            int countSum = mGroupPurchaseDetailItem.getTprice() * goodsNumber;
            countSumTV.setText("￥" + countSum + ".0");
        }
    }

    /**
     * 增加数目方法
     */
    private void addGoodsNumber(){
        String siteNumber = siteStockTV.getText().toString();
        if (TextUtils.isEmpty(siteNumber)){
            CustomToast.showToast(mContext, "请先选择站点");
            return;
        }

        String goodsNumberStr = goodsNumberET.getText().toString();
        if(TextUtils.isEmpty(goodsNumberStr)){
            goodsNumber = 1;
            goodsNumberET.setText(goodsNumber + "");
        }else{
            goodsNumber = Integer.parseInt(goodsNumberStr);
            if (goodsNumber == mGroupDetailStationItem.getNumber()) {
                CustomToast.showToast(mContext, "您当前选择已达到库存量");
                return;
            }
            goodsNumber ++;
            goodsNumberET.setText(goodsNumber + "");
        }
//        goodsNumberSubtractIV.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg_reddish_orange));
        goodsNumberSubtractIV.setEnabled(true);
        notifyCartNumber();
    }

    /**
     * 减小数目方法
     */
    private void subtractGoodsNumber(){
        String siteNumber = siteStockTV.getText().toString();
        if (TextUtils.isEmpty(siteNumber)){
            CustomToast.showToast(mContext, "请先选择站点");
            return;
        }

        String goodsNumberStr = goodsNumberET.getText().toString();
        if(TextUtils.isEmpty(goodsNumberStr)){
//            goodsNumberSubtractIV.setBackground(getResources().getDrawable(R.drawable.shape_btn_border_bg));
            goodsNumberSubtractIV.setEnabled(false);
            return;
        }else{
            goodsNumber = Integer.parseInt(goodsNumberStr);
            if (goodsNumber > 0){
                goodsNumber --;
            }else{
                goodsNumber = 0;
            }
            goodsNumberET.setText(goodsNumber+"");
        }
        notifyCartNumber();
    }

    /**
     * 参加团购方法
     */
    private void joinGroup(){
        if (goodsNumber == 0){
            CustomToast.showToast(mContext, "请先添加商品");
            return;
        }
        mGroupPurchaseDetailItem.setTotal(goodsNumber);
        Intent intentGroupPurchaseIndent = new Intent(mContext, GroupPurchaseIndentActivity_.class);
        intentGroupPurchaseIndent.putExtra("groupPurchaseDetailItem", mGroupPurchaseDetailItem);
        startActivityForResult(intentGroupPurchaseIndent, Constant.StaticCode.REQUEST_GROUP_PURCHASE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_GROUP_PURCHASE) && (resultCode == RESULT_OK)){
            setResult(RESULT_OK);
            finish();
        }
        if((requestCode == Constant.StaticCode.REQUEST_SELECT_SITE) && (resultCode == RESULT_OK)){
            mGroupDetailStationItem = (GroupDetailStationItem)data.getExtras().get("groupDetailStationItem");
            selectSiteTitleTV.setText("站点库存");
            siteNameTV.setText(mGroupDetailStationItem.getStation_name());
            siteStockTV.setText("库存" + mGroupDetailStationItem.getNumber() + "件");
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.agpd_rl_my_service_site:
                Intent intentSelectSite = new Intent(mContext, SelectGroupDetailStieActivity_.class);
                intentSelectSite.putExtra("list", (ArrayList)allItems);
                startActivityForResult(intentSelectSite, Constant.StaticCode.REQUEST_SELECT_SITE);
                break;
            case R.id.agpd_tv_goods_number_add://点击增加数目
                addGoodsNumber();
                break;
            case R.id.agpd_tv_goods_number_subtract://点击减小数目
                subtractGoodsNumber();
                break;
            case R.id.agpd_btn_join_group://点击参加团购
                joinGroup();
                break;
        }
    }

    Chronometer.OnChronometerTickListener listener = new Chronometer.OnChronometerTickListener()
    {
        @Override
        public void onChronometerTick(Chronometer chronometer)
        {
            if (mNextTime <= 0) {
                if (mNextTime == 0){
                }
                mNextTime = 0;
                diffStartDateTV.setText(mTimeFormat.format(new Date(mNextTime * 1000)));
                return;
            }

            mNextTime--;

            diffStartDateTV.setText(mTimeFormat.format(new Date(mNextTime * 1000)));
        }
    };
}
