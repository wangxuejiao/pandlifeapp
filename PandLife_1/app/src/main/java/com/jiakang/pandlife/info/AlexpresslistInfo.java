package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import android.text.TextUtils;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.utils.JKLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 1.1我的已取包裹接口
 *
 * @author ww
 *
 */
public class AlexpresslistInfo extends BaseAbsInfo {

    private static final String TAG = "AlexpresslistInfo";

    /** 分页-当前页码 */
    private int pageIndex;
    /** 分页-每页数目 */
    private int pageSize;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<ExpressListItem> allItems = new ArrayList<ExpressListItem>();
    private String totalPage;
    private String pageIndexStr;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Express&a=Alexpresslist" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("pageSize", BaseInfo.pageSize);
            json.put("pageIndex",pageIndex);
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                ExpressListItem expressListItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    expressListItem = BaseInfo.gson.fromJson(itemStr, ExpressListItem.class);
                    expressListItem.setLayout(R.layout.item_my_package_get);
                    expressListItem.setIsget("2");
                    // 入库
                    expressListItem.insert();
                    allItems.add(expressListItem);
                }

                JSONObject taskJO = (JSONObject)jsonObject.get("page");
                pageIndexStr = taskJO.getString("pageIndex");
                if (!TextUtils.isEmpty(pageIndexStr))
                    pageIndex = Integer.parseInt(pageIndexStr);
                totalPage = taskJO.getString("totalPage");
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public List<ExpressListItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<ExpressListItem> allItems) {
        this.allItems = allItems;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public String getPageIndexStr() {
        return pageIndexStr;
    }

    public void setPageIndexStr(String pageIndexStr) {
        this.pageIndexStr = pageIndexStr;
    }
}
