//package com.jiakang.pandlife.acty;
//
//import greendroid.widget.PageIndicator;
//
//import java.text.ParseException;
//
//import org.json.JSONObject;
//
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.graphics.Bitmap;
//import android.os.Bundle;
//import android.view.View;
//import android.view.WindowManager;
//import android.view.animation.AlphaAnimation;
//import android.view.animation.Animation;
//import android.view.animation.Animation.AnimationListener;
//import android.widget.ImageView;
//
//import com.jiakang.pandlife.Constant;
//import com.jiakang.pandlife.R;
//import com.jiakang.pandlife.api.AbsOnRequestListener;
//import com.jiakang.pandlife.api.ApiManager;
//import com.jiakang.pandlife.info.LoginInfo;
//import com.jiakang.pandlife.item.UserItem;
//import com.jiakang.pandlife.utils.CustomToast;
//import com.jiakang.pandlife.utils.JKLog;
//import com.jiakang.pandlife.utils.Util;
//import com.jiakang.pandlife.widget.GuideScrollViewGroup;
//
//
//public class LogoActivity extends BaseActy {
//	private static final String TAG = "LogoActivity";
//
//	/** 记录上次登录时间 */
//	public static String lastLoginTime;
//	/** 是否允许自动登录 */
//	public static boolean allowAutoLogin = false;
//	/** 是否已登录 */
//	public static boolean isLogin = false;
//	/** 获取可变背景等基础信息更新值 */
////	private BaseDataUpdateInfo mBaseDataUpdateInfo;
//	/** 第三方登录管理者 */
////	private  ThridOpenManager thridOpenManager;
//	/** 引导页图片 */
//	private GuideScrollViewGroup fristScrollViewGroup;
//	/** 用户在spf中保存的密码 */
////	private String pass;
//	/** 渐变图片 */
//	private ImageView bgIV;
//	/** 启动页图片 */
//	private ImageView logoStartIV;
//	/** 测试延迟，界面更新延迟效果 */
//	private long beginTime;
//	private long endTime;
//	/** 当前版本号*/
//	private String nowVersionID;
//	private PageIndicator pageIndicator;
//
//	/** 掌缘登录 */
//	private static final int LOGIN_MATCHMAKE = 0x1222;
//	/** 第三方登录 */
//	private final int LOGIN_THIRD = 0x1223;
//
//	private SharedPreferences myAccount;
//	private SharedPreferences baseDataSpf;
//	/** 获取IP接口 */
////	private QueryBaseClientInfo queryBaseClientInfo = new QueryBaseClientInfo();
////	/** 获取广告信息接口 */
////	private QueryAdvertisementInfo queryAdvertisementInfo = new QueryAdvertisementInfo();
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//		JKLog.i("LogoActivity", "onCreate方法-------->");
//		long preTime = System.currentTimeMillis();
//
//		// 取消标题,必须在setContentView（R.layout.logo）之前
//		// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		// 取消状态栏
//		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		setContentView(R.layout.acty_logo);
//
//		JKLog.i("LogoActivity", "LogoActy中取消标题状态栏over-------->" + (System.currentTimeMillis() - preTime));
//
//		getAdvertisement();
//		initVar();
//		allowAutoLogin();
//		getBaseClient();
//		initView();
//
//
//		initImgBg();
//
//		beginTime = System.currentTimeMillis();
//		// 取得上一次运行的版本号与当前版本号对比，不相同进入引导页
//		checkCurVersion();
//		// 更新基础数据
//
//
//		JKLog.i("LogoActivity", "LogoActy中onCreate方法-------->" + (System.currentTimeMillis() - preTime));
//
//
//
//	}
//
//	private void getAdvertisement(){
//		Constant.isHideUpload = true;//设置隐藏请求网络时“加载中”的进度条
//		SharedPreferences baseDataSPF = ConfigSPF.getInstance().getConfigSPF(ConfigSPF.NAME_BASEDATA);
//		// 广告最大id
//		String maxId = baseDataSPF.getString(Constant.Spf.MAX_ADVERTISEMENT_ID, "0");
//		queryAdvertisementInfo.setMaxId(maxId);
//		ApiManager apiManager = ApiManager.getInstance();
////		ProgressDialogUtil.setCancelable(true);
//		apiManager.request(queryAdvertisementInfo, new AbsOnRequestListener(mContext){
//			@Override
//			public void onRequestSuccess(int result, JSONObject jsonObject) {
//				 super.onRequestSuccess(result, jsonObject);
//				try {
//					if(("1").equals(queryAdvertisementInfo.getIsUpdate()) && (0 != queryAdvertisementInfo.getAllItems().size())){
////						viewFlipper.setVisibility(View.GONE);
////						viewFlipper.clearImgsDraw();
////						viewFlipper.removeAllViews();
////						viewFlipper.notifyAll();
////						if(isTopActivity()){
////							Intent intentRefresh = new Intent(mContext, MainControlActy.class);
////							startActivity(intentRefresh);
////						}
////						getViewFlipper();
//						Constant.isHideUpload = false;//设置显示请求网络时“加载中”的进度条
//					}
//				} catch (Exception e) {
//					JKLog.e(TAG, "onRequestSuccess方法中-------->：e为：" + e);
//				}
//			}
//		});
//	}
//
//	private void initVar() {
//		baseDataSpf = MyJobApp.getInstance().getConfigSpfByName(ConfigSPF.NAME_BASEDATA);
//		myAccount = ConfigSPF.getInstance().getConfigSPF(ConfigSPF.NAME_ACCOUNT);
//	}
//
//	@Override
//	protected void onResume() {
//		super.onResume();
//
//	}
//
//	/**
//	 * 初始化界面
//	 */
//	private void initView() {
//		fristScrollViewGroup = (GuideScrollViewGroup) findViewById(R.id.al_gsv_scrollViewGroup);
//		// mScrollLayout默认显示第一页
//		fristScrollViewGroup.setCurrentScreenIndex(0);
//		fristScrollViewGroup.setVisibility(View.GONE);
//		fristScrollViewGroup.setOnScreenChangeListener(new OnScreenChangeListener() {
//			@Override
//			public void onScreenChange(int pageSize, int currentIndex) {
////				pageIndicator.setActiveDot(currentIndex);
//			}
//
//			@Override
//			public void onScreenEnd(int pageSize, int currentIndex) {
////				// 开始登录
////				loginAndGoInTabControl();
//				// 开始动画
//				startAnimation();
//			}
//
//		});
////		pageIndicator = (PageIndicator)findViewById(R.id.al_pi_indicator);
////		pageIndicator.setDotCount(fristScrollViewGroup.getPageSize());
//
//		// 渐变背景动画
//		bgIV = (ImageView) findViewById(R.id.logo_iv_logo);
//		logoStartIV = (ImageView) findViewById(R.id.al_iv_logoImage);
//		JKLog.e(TAG, "bgIV------------------------>"+(bgIV==null));
//		JKLog.e(TAG, "View.VISIBLE-------------------------->"+View.VISIBLE);
//
////		bgIV.setVisibility(View.VISIBLE);
//	}
//
//	/**
//	 * 初始化渐变图片背景
//	 */
//	private void initImgBg() {
//		String urlStr = baseDataSpf.getString(Constant.Spf.WELCOME_BG, "");
//		getMyJobApp().getSyncImageLoader().loadOriginalImage(urlStr, new OnloadImage() {
//
//			@Override
//			public void loadFinish(Bitmap bitmap) {
//				bgIV.setVisibility(View.VISIBLE);
//				bgIV.setImageBitmap(bitmap);
//			}
//
//			@Override
//			public void loadFail(boolean hasNet) {
////				bgIV.setVisibility(View.VISIBLE);
//				JKLog.i(TAG, "LogoActivity.initImgBg()失败loadFail方法-------------->");
//				baseDataSpf.edit().putString("welcomeBg", "0").commit();
//			}
//		});
//	}
//
//	/**
//	 * 取得上一次运行的版本号与当前版本号对比，不相同进入引导页，相同检查是否是登录用户登出操作
//	 */
//	private void checkCurVersion() {
//		String oldVison = baseDataSpf.getString(Spf.OLD_VISON, "0");
//		// 取得当前版本号
//		nowVersionID = Util.getVersionName(LogoActivity.this);
//		baseDataSpf.edit().putString(Spf.OLD_VISON, nowVersionID).commit();
//		if (!oldVison.equals(nowVersionID)) {
//			showGuideView();
////			// 清空基础数据
////			// baseDataSpf.edit().clear().commit();
////			Intent mainIntent = new Intent(mContext, MainControlActy.class);
////			startActivity(mainIntent);
////
////			// 启动IM服务
////			Intent service = new Intent(mContext, IMService.class);
////			service.setAction("im_mywork_action_service");
////			startService(service);
////
////			LogoActivity.this.finish();
//		} else {
////			// 开始登录
////			loginAndGoInTabControl();
//			// 开始动画
//			startAnimation();
//		}
//
//		// 对constring中值赋值
////		/** "http://192.168.1.150:8080/MatchMaker" */
////		Constant.mWebAddress = baseDataSpf.getString(Constant.Spf.HTTP_SERVER, Constant.mWebAddress);
////		/** 图片下载地址 */
////		Constant.downalImageUrl = baseDataSpf.getString(Constant.Spf.FILE_SERVER, Constant.downalImageUrl);
//
//		// 获取基础数据版本号
////		if (Util.hasNet(mContext, false)) {
////			XYLog.e(TAG, "LogoActy类中比对基础数据版本号了-------------->");
////			mBaseDataUpdateInfo = new BaseDataUpdateInfo();
////			httpApi.doActionOnlySuccess(mBaseDataUpdateInfo, mHandler, GET_BASEDATA_UPDATE_OK);
////		}
//
//	}
//
//	/**
//	 * 第一次登录时加入引导页
//	 */
//	private void showGuideView() {
//		fristScrollViewGroup.setVisibility(View.VISIBLE);
//		bgIV.setVisibility(View.GONE);
//	}
//
//	/**
//	 * 是否允许自动登录做判定值
//	 */
//	private void allowAutoLogin(){
//
//		MainControlActy.isExit = myAccount.getBoolean(Constant.Spf.ISEXIT, true);
//		lastLoginTime = myAccount.getString(Constant.Spf.LASTLOGINTIME, null);
//		JKLog.i(TAG, "lastLoginTime----------------------->"+lastLoginTime);
//		String nowTime = Util.getTime();
//		if(lastLoginTime == null){
//			return;
//		}
//		try {
//			boolean isSurpass = Util.dateLastLogin(lastLoginTime, nowTime, "yyyy-MM-dd HH:mm:SS");
//			if((isSurpass == false) && (MainControlActy.isExit == false)){
//				allowAutoLogin = true;
//			}else{
//				allowAutoLogin = false;
//			}
//		} catch (ParseException e1) {
//			JKLog.e(TAG, "allowAutoLogin-------------------------------------------->"+allowAutoLogin);
//		}
//	}
//
//	/**
//	 * 获取IP
//	 */
//	private void getBaseClient(){
//		ApiManager apiManager = ApiManager.getInstance();
//		apiManager.request(queryBaseClientInfo, new AbsOnRequestListener(mContext){
//			@Override
//			public void onRequestSuccess(int result, JSONObject jsonObject) {
//				super.onRequestSuccess(result, jsonObject);
//				try {
//					if(result == 0){
//					}
//				} catch (Exception e) {
//					JKLog.e(TAG, "onRequestSuccess方法中-------->：");
//				}
//			}
//		});
//	}
//
//	/**
//	 * 自动登录操作
//	 *
//	 * @param nameStr
//	 * @param passStr
//	 */
//	private void loginAndGoInTabControl() {
//
//		UserItem userItem = getMyJobApp().getUserItem();
//		String nameStr = myAccount.getString(Constant.Spf.USERNAME, "");
//		String rembPassWord = myAccount.getString(Constant.Spf.PASSWORD, "");
//		if((allowAutoLogin == true) && (userItem != null) && (nameStr != "") && (rembPassWord != "")){
//			autoLogin(nameStr, rembPassWord);
//		}else{
//			isLogin = false;
//			Intent mainIntent = new Intent(mContext, MainControlActy.class);
//			startActivity(mainIntent);
//			LogoActivity.this.finish();
//		}
//
//
//	//		UserItem userItem = MyJobApp.getInstance().getUserItem();
//	//		JKLog.e(TAG, "11111111111111");
//	////		if (userItem == null  || (userItem != null && "1".equals(userItem.getIsExit()))) {
//	////			Intent intent = new Intent(mContext, MainControlActy.class);
//	////			startActivity(intent);
//	////			finish(false);
//	////			return;
//	////		}
//	//
//	//		ApiManager apiManager = ApiManager.getInstance();
//	//		RequestMap requestMap = new RequestMap(ApiManager.URL_login);
//	//		requestMap.put("userName", userItem.getName());
//	//		requestMap.put("passWord", "");
//	//		requestMap.put("loginType", "1");
//	//		apiManager.request(requestMap, new AbsOnRequestListener(mContext) {
//	//			@Override
//	//			public void onRequestSuccess(int result, JSONObject jsonObject) {
//	//				super.onRequestSuccess(result, jsonObject);
//	//				try {
//	//					UserItem userItem = BaseInfo.gson.fromJson(jsonObject.getJSONObject("userItem").toString(), UserItem.class);
//	//					MyJobApp.getInstance().setUserItem(userItem);
//	//
//	//					CustomToast.showToast(mContext, "登陆成功");
//	//
//	//				} catch (Exception e) {
//	//					JKLog.e(TAG, "onRequestSuccess方法中-------->：");
//	//				}
//	//
//	//				Intent intent = new Intent(mContext, MainControlActy.class);
//	//				startActivity(intent);
//	//				finish();
//	//			}
//	//
//	//			@Override
//	//			public void onRequestFail(int result, JSONObject jsonObject) {
//	//				super.onRequestFail(result, jsonObject);
//	//				Intent intent = new Intent(LogoActivity.this, LoginActy.class);
//	//				startActivity(intent);
//	//				finish(false);
//	//			}
//	//
//	//			@Override
//	//			public void onRequestTimeOut(int result, JSONObject jsonObject) {
//	//				super.onRequestTimeOut(result, jsonObject);
//	//				Intent intent = new Intent(LogoActivity.this, LoginActy.class);
//	//				startActivity(intent);
//	//				finish(false);
//	//			}
//	//
//	//
//	//		});
//
//		}
//
//	/**
//	 * 自动登录
//	 * @param nameStr
//	 * @param passStr
//	 */
//	private void autoLogin(String nameStr, String passwordStr){
//
////		boolean exitApp = myAccount.getBoolean("exitApp", false);
//
//		LoginInfo loginInfo = new LoginInfo();
//		loginInfo.setmUserName(nameStr);
//		loginInfo.setmPassword(passwordStr);
//		ApiManager apiManager = ApiManager.getInstance();
//		apiManager.request(loginInfo,new AbsOnRequestListener(mContext){
//			@Override
//			public void onRequestSuccess(int result, JSONObject jsonObject) {
//				super.onRequestSuccess(result, jsonObject);
//				try {
////					UserItem userItem = BaseInfo.gson.fromJson(jsonObject.getJSONObject("userItem").toString(), UserItem.class);
////					MyJobApp.getInstance().setUserItem(userItem);
//					CustomToast.showToast(mContext, "自动登录成功");
//					Intent intent = new Intent(mContext, MainControlActy.class);
//					startActivity(intent);
//					isLogin = true;
//
//					// 启动IM服务
//					Intent service = new Intent(mContext, IMService.class);
//					service.setAction("im_mywork_action_service");
//					startService(service);
//
//				} catch (Exception e) {
//					JKLog.e(TAG, "onRequestSuccess方法中-------->：");
//				}
//
//
//				finish();
//			}
//
//			@Override
//			public void onRequestFail(int result, JSONObject jsonObject) {
//				super.onRequestFail(result, jsonObject);
//				CustomToast.showToast(mContext, "自动登录失败");
//				Intent intent = new Intent(LogoActivity.this, MainControlActy.class);
//				startActivity(intent);
//				isLogin = false;
//				finish(false);
//			}
//
//			@Override
//			public void onRequestTimeOut(int result, JSONObject jsonObject) {
//				super.onRequestTimeOut(result, jsonObject);
//				CustomToast.showToast(mContext, "自动登录失败");
//				Intent intent = new Intent(LogoActivity.this, MainControlActy.class);
//				startActivity(intent);
//				isLogin = false;
//				finish(false);
//			}
//
//		});
//
////		JKLog.e(TAG, "exitApp---------------------------------->"+exitApp);
////		if(exitApp == EXIT_APP){
////			Intent intentAutoLogin = new Intent(mContext, LoginActy.class);
////			intentAutoLogin.putExtra("autoLogin", "true");//传值是否自动登录
////			startActivityForResult(intentAutoLogin, 0);
////			initTitleBar(R.id.am_tb_title, "我的工作", "南京", "退出");
////		}else{
////			initTitleBar(R.id.am_tb_title, "我的工作", "南京", "登录");
////		}
//	}
//
//	private void startAnimation() {
//		JKLog.i("", "LogoActy中：startAnimation方法--------------");
////		fristScrollViewGroup.setVisibility(View.GONE);
////		bgIV.setVisibility(View.VISIBLE);
//		logoStartIV.setVisibility(View.VISIBLE);
//
//		AlphaAnimation aa = new AlphaAnimation(0.8f, 1.0f);
//		aa.setDuration(1000);
//		logoStartIV.startAnimation(aa);
//		aa.setAnimationListener(new AnimationListener() {
//			@Override
//			public void onAnimationStart(Animation animation) {
//
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation animation) {
//
//			}
//
//			@Override
//			public void onAnimationEnd(Animation animation) {
////				Intent mainIntent = new Intent(mContext, MainControlActy.class);
////				startActivity(mainIntent);
////				LogoActivity.this.finish();
//				// 开始登录
//				loginAndGoInTabControl();
//			}
//		});
//
//
//	}
//
//
//}
