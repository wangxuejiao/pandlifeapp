//package com.example.cxl.cxlframe.utils;
//
//import java.util.regex.Pattern;
//
//import android.text.TextUtils;
//
//public class PinyinUtil {
//	// private static Log logger = LogFactory.getLog(StringUtil.class);
//	// 国标码和区位码转换常量
//	static final int GB_SP_DIFF = 160;
//	// 存放国标一级汉字不同读音的起始区位码
//	static final int[] secPosValueList = { 1601, 1637, 1833, 2078, 2274, 2302, 2433, 2594, 2787, 3106, 3212, 3472, 3635, 3722, 3730, 3858, 4027, 4086, 4390, 4558, 4684, 4925, 5249, 5600 };
//	// 存放国标一级汉字不同读音的起始区位码对应读音
//	static final char[] firstLetter = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'w', 'x', 'y', 'z' };
//
//	/**
//	 * 获得字母串的首字母如：如果小写转换成大写<br>
//	 *
//	 * @param str
//	 *            am
//	 * @return A
//	 */
//	public static String getFirstBig(String str) {
//		if (str == null) {
//			return "#";
//		}
//		if (str.trim().length() == 0) {
//			return "#";
//		}
//		char c = str.trim().substring(0, 1).charAt(0);
//		// 正则表达式，判断首字母是否是英文字母
//		Pattern pattern = Pattern.compile("^[A-Za-z]+$");
//		if (pattern.matcher(c + "").matches()) {
//			return (c + "").toUpperCase();
//		} else {
//			return "#";
//		}
//	}
//
//	/**
//	 * 获取一个字符串的拼音码
//	 *
//	 * @param oriStr
//	 *            俺们
//	 * @return am
//	 */
//	// public static String getFirstLetter(String oriStr){
//	// try {
//	// System.out.print(oriStr);
//	// String str = oriStr.toLowerCase();
//	// StringBuffer buffer = new StringBuffer();
//	// char ch;
//	// char[] temp;
//	// for (int i = 0; i < str.length(); i++) { // 依次处理str中每个字符
//	// ch = str.charAt(i);
//	// temp = new char[] { ch };
//	// byte[] uniCode = new String(temp).getBytes("GBK");
//	// if (uniCode[0] < 128 && uniCode[0] > 0) { // 非汉字
//	// buffer.append(temp);
//	// } else {
//	// buffer.append(convert(uniCode));
//	// }
//	// }
//	// return buffer.toString();
//	//
//	// } catch (Exception e) {
//	// return "";
//	// }
//	// }
//
//	/**
//	 * 获取一个汉字的拼音首字母。 GB码两个字节分别减去160，转换成10进制码组合就可以得到区位码
//	 * 例如汉字“你”的GB码是0xC4/0xE3，分别减去0xA0（160）就是0x24/0x43
//	 * 0x24转成10进制就是36，0x43是67，那么它的区位码就是3667，在对照表中读音为‘n’
//	 */
//	private static char convert(byte[] bytes) {
//		char result = '-';
//		int secPosValue = 0;
//		int i;
//		for (i = 0; i < bytes.length; i++) {
//			bytes[i] -= GB_SP_DIFF;
//		}
//		secPosValue = bytes[0] * 100 + bytes[1];
//		for (i = 0; i < 23; i++) {
//			if (secPosValue >= secPosValueList[i] && secPosValue < secPosValueList[i + 1]) {
//				result = firstLetter[i];
//				break;
//			}
//		}
//		return result;
//	}
//
//	/**
//	 * -----------------------需要导入pingyin.jar包------------------
//	 */
//
//	/**
//	 * 得到 全拼
//	 *
//	 * @param src
//	 * @return
//	 */
//	public static String getPinYin(String src) {
//		char[] t1 = null;
//		t1 = src.toCharArray();
//		String[] t2 = new String[t1.length];
//		HanyuPinyinOutputFormat t3 = new HanyuPinyinOutputFormat();
//		t3.setCaseType(HanyuPinyinCaseType.LOWERCASE);
//		t3.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
//		t3.setVCharType(HanyuPinyinVCharType.WITH_V);
//		String t4 = "";
//		int t0 = t1.length;
//		try {
//			for (int i = 0; i < t0; i++) {
//				// 判断是否为汉字字符
//				if (Character.toString(t1[i]).matches("[\\u4E00-\\u9FA5]+")) {
//					t2 = PinyinHelper.toHanyuPinyinStringArray(t1[i], t3);
//					t4 += t2[0];
//				} else {
//					t4 += Character.toString(t1[i]);
//				}
//			}
//			return t4;
//		} catch (BadHanyuPinyinOutputFormatCombination e1) {
//			e1.printStackTrace();
//		}
//		return t4;
//	}
//
//	/**
//	 * 得到中文首字母
//	 *
//	 * @param str
//	 * @return
//	 */
//	public static String getPinYinHeadChar(String str) {
//		if (TextUtils.isEmpty(str))
//			return "";
//
//		String convert = "";
//		for (int j = 0; j < str.length(); j++) {
//			char word = str.charAt(j);
//			String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
//			if (pinyinArray != null) {
//				convert += pinyinArray[0].charAt(0);
//			} else {
//				convert += word;
//			}
//		}
//		return convert;
//	}
//
//	/**
//	 * 将字符串转移为ASCII码
//	 *
//	 * @param cnStr
//	 * @return
//	 */
//	public static String getCnASCII(String cnStr) {
//		StringBuffer strBuf = new StringBuffer();
//		byte[] bGBK = cnStr.getBytes();
//		for (int i = 0; i < bGBK.length; i++) {
//			// System.out.println(Integer.toHexString(bGBK[i]&0xff));
//			strBuf.append(Integer.toHexString(bGBK[i] & 0xff));
//		}
//		return strBuf.toString();
//	}
//
//}
