package com.jiakang.pandlife.acty.service;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.ToolRentInfo;

import com.jiakang.pandlife.item.ToolRentItem;
import com.jiakang.pandlife.widget.PullToRefreshListView;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



@EActivity(R.layout.activity_tool_rent)
public class ToolRentActivity extends BaseActy {

    @ViewById(R.id.lv_tool_rent)
    PullToRefreshListView listView;

    private ItemAdapter mItemAdapter;

    //租借详情
    private ToolRentInfo mToolRentInfo = new ToolRentInfo();
    private List<ToolRentItem.DataEntity> mDataEntityList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
    }

    @AfterViews
    protected void afterCreateView() {

        initTitleBar(R.id.al_tb_title, "工具租借", null, null);
        listView.setAdapter(mItemAdapter);

        listView.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position == mDataEntityList.size() + 1){
                    return;
                }
                Intent intent = new Intent(mContext, ToolDetailActivity_.class);
                intent.putExtra("id", mDataEntityList.get(position - 1).getId());
                startActivity(intent);
            }
        });

        listView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            @Override
            public void onRefresh() {

                toolRentRequest();
            }
        });
    }

    public void initData() {

        mItemAdapter = new ItemAdapter(this);
        toolRentRequest();
    }

    private void toolRentRequest() {

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mToolRentInfo,new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mDataEntityList = mToolRentInfo.getDataEntityList();
                mItemAdapter.clear();
                mItemAdapter.addItems((List)mDataEntityList);
                mItemAdapter.notifyDataSetChanged();
                listView.onRefreshComplete();
            }
        });

    }

}
