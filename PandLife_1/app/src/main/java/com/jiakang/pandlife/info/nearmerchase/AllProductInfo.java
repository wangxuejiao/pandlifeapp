package com.jiakang.pandlife.info.nearmerchase;

/**
 * Created by Administrator on 2015/12/8.
 */


import android.text.TextUtils;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.info.BaseAbsInfo;
import com.jiakang.pandlife.item.GoodsKindItem;
import com.jiakang.pandlife.item.RecommendGoodsItem;
import com.jiakang.pandlife.utils.JKLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 2.商家全部产品、分类接口
 *
 * @author ww
 *
 */
public class AllProductInfo extends BaseAbsInfo {

    private static final String TAG = "AllProductInfo";

    /** 商家id	Int(10) */
    private int sid;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;
    /** 页面索引	Int(10) */
    private int pageIndex;
    /** 分页大小	Int(10) */
    private int pageSize;

    //推荐商品
    private List<RecommendGoodsItem> recommendAllItems = new ArrayList<RecommendGoodsItem>();
    //所有商品
    private List<RecommendGoodsItem> allItems = new ArrayList<RecommendGoodsItem>();

    private String totalPage;
    private String pageIndexStr;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Trader&a=Allproduct" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("sid", sid);
            json.put("pageIndex", pageIndex);
            json.put("pageSize", pageSize);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                data = (JSONObject)jsonObject.get("data");
                allItems.clear();
                JSONArray taskJA = data.getJSONArray("pt");
                String itemStr;
                RecommendGoodsItem recommendGoodsItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    recommendGoodsItem = BaseInfo.gson.fromJson(itemStr, RecommendGoodsItem.class);
                    // 入库
//                    goodsKindItem.insert();
                    allItems.add(recommendGoodsItem);
                }

                JSONObject taskJO = (JSONObject)jsonObject.get("page");
                pageIndexStr = taskJO.getString("pageIndex");
                if (!TextUtils.isEmpty(pageIndexStr))
                    pageIndex = Integer.parseInt(pageIndexStr);
                totalPage = taskJO.getString("totalPage");

                //因后台可能没有字段，所以进行判断
                recommendAllItems.clear();
                if (data.has("tj")){
                    try{
                        JSONArray tjJA = data.getJSONArray("tj");
                        String itemTJStr;
                        RecommendGoodsItem recommendGoodsItemTJ;
                        for(int i=0;i<tjJA.length();i++){
                            itemTJStr = tjJA.getJSONObject(i).toString();
                            recommendGoodsItemTJ = BaseInfo.gson.fromJson(itemTJStr, RecommendGoodsItem.class);
                            // 入库
//                    goodsKindItem.insert();
                            recommendAllItems.add(recommendGoodsItemTJ);
                        }
                    }catch (Exception e){
                        JKLog.e(TAG, "tj解析-------->：e为：" + e);
                    }
                }
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<RecommendGoodsItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<RecommendGoodsItem> allItems) {
        this.allItems = allItems;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<RecommendGoodsItem> getRecommendAllItems() {
        return recommendAllItems;
    }

    public void setRecommendAllItems(List<RecommendGoodsItem> recommendAllItems) {
        this.recommendAllItems = recommendAllItems;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public String getPageIndexStr() {
        return pageIndexStr;
    }

    public void setPageIndexStr(String pageIndexStr) {
        this.pageIndexStr = pageIndexStr;
    }
}
