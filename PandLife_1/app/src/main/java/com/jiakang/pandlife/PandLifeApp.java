package com.jiakang.pandlife;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;

import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.db.AreaDBManage;
import com.jiakang.pandlife.db.DataCacheDBManage;
import com.jiakang.pandlife.db.DataDBManage;
import com.jiakang.pandlife.db.FileDBManage;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.utils.MemoryCache;
import com.jiakang.pandlife.utils.SyncImageLoader;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;


import java.util.ArrayList;
import java.util.List;

import cn.jpush.android.api.JPushInterface;
import cn.sharesdk.framework.ShareSDK;
import greendroid.app.GDApplication;


/**
 *
 */
public class PandLifeApp extends GDApplication {

    private String TAG = "PandLifeApp";
    private static PandLifeApp mInstance;
    private static List<Activity> activityList = new ArrayList<Activity>();

    public static PandLifeApp getInstance() {
        return mInstance;
    }

    @Override
    public Class<?> getHomeActivityClass() {
        return null;
    }

//	//初始化请求队列
//	public static void initRequestQueue(){
//		//初始化 volley
//		VolleyUtil.initialize(mInstance);
//	}

    @Override
    public Intent getMainApplicationIntent() {
        new Intent(Intent.ACTION_VIEW, Uri.parse("http://github.com/cyrilmottier/GreenDroid"));
        return null;
    }

    public static void exitAllActivity(Context ctx) {
        System.out.println("NetNoticeApplication中：共有： " + activityList.size() + " 个Activity被结束掉--------");
        if (activityList.size() > 0) {
            for (Activity ac : activityList) {
                if (null != ac)
                    ac.finish();
            }
            activityList.clear();
        }
    }

    /**
     * 退出应用(默认停止及时服务、杀死应用进程)
     *
     * @param ctx
     */
    public void exitApp(Context ctx) {
        exitApp(ctx, true, true, true);
    }

    /**
     * 退出应用
     *
     * @param ctx
     * @param closeService 是否停止及时服务
     * @param killProcess  是否杀死应用进程
     */
    public void exitApp(Context ctx, boolean closeService, boolean coloseDb, boolean killProcess) {

        // 消除全局系统通知
        if (mNotificationManager != null)
            mNotificationManager.cancelAll();

        // 关闭数据库
        if (coloseDb || (closeService && killProcess)) {
            if ((closeService && killProcess)) {
                if (areaDBManage != null)
                    areaDBManage.closeDB();
            }
        }

        // try {
        // if (closeService) {
        // // 立即关闭会话
        // IMService.getInstance().closeIM(true);
        // // 手动停止service
        // ctx.stopService(new Intent("im_matchmacke_action_service"));
        // }
        // } catch (Exception e) {
        // XYLog.e("", "exitApp方法中-------->：e为：" + e);
        // }

        // 清楚内存中图片
        clearRAMBitmap(ctx, true);
        // 关闭所有Acty
        exitAllActivity(ctx);
        JKLog.i("", "DialogUtil中：共有： " + activityList.size() + " 个Activity被结束掉--------");

        if (killProcess) {
            ActivityManager am = (ActivityManager) getSystemService("activity"); // "activity"
            android.os.Process.killProcess(android.os.Process.myPid());
            am.killBackgroundProcesses(getPackageName()); // API
            // Level至少为8才能使用
            // Android的程序只是让Activity finish()掉,而单纯的finish掉,退出并不完全
            System.exit(0);
            // 所以要用此方法最好加个判断如果是2.2之前的rom就用restartPackage之后的就用killBackgroundProcesse

        }

    }

    /**
     * 清除图片在内存中的软、硬引用
     *
     * @param ctx
     */
    public void clearRAMBitmap(Context ctx, boolean finishSelf) {
        MemoryCache.getInstance().clearCache();
        if (finishSelf)
            ((Activity) ctx).finish();
    }

    public static void addActivity(Activity ac) {
        activityList.add(ac);
    }

    public static void removeActivity(Context activitiy) {
        activityList.remove(activitiy);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        System.out.println("NetNoticeApplication中onConfigurationChanged----------！！！：");
    }

    @Override
    public void onCreate() {
        mInstance = this;
        super.onCreate();

        JPushInterface.setDebugMode(true);    // 设置开启日志,发布时请关闭日志
        JPushInterface.init(this);            // 初始化 JPush

        SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

        if(myAccount.getBoolean(Constant.Spf.MESSAGE_NOTIFY,true)){
            JPushInterface.resumePush(getApplicationContext());
        }else {
            JPushInterface.stopPush(getApplicationContext());
        }

        JPushInterface.setAlias(this, "wxj123", null);//设置别名
        initImageLoader(this);
        System.out.println("NetNoticeApplication中onCreate----------！！！：");

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        System.out.println("NetNoticeApplication整个应用销毁了----------！！！：");
    }

    /**
     * ----------------------------华丽分隔符--------------------------------
     */

    private SyncImageLoader syncImageLoader;

    public SyncImageLoader getSyncImageLoader() {
        if (syncImageLoader == null) {
            syncImageLoader = SyncImageLoader.getInstance(this);
        }
        return syncImageLoader;
    }

    /**
     * 缓存表，不登录也可建表保存
     *
     * @return
     */
    public DataCacheDBManage getCacheDataDBManage() {
        return getDataCacheDBManage("db_pandlife_cache.db");
    }

    private String curDbName = null;
    private DataCacheDBManage dataCacheDBManage;

    public DataCacheDBManage getDataCacheDBManage(String dbName) {
        if (dataCacheDBManage == null) {
            curDbName = dbName;
            dataCacheDBManage = new DataCacheDBManage(this, dbName);
        }
        return dataCacheDBManage;
    }

    private AreaDBManage areaDBManage;

    public AreaDBManage getAreaDBManage() {
        if (areaDBManage == null) {
            areaDBManage = new AreaDBManage(this);
        }
        return areaDBManage;
    }

    private DataDBManage dbManage;

    public DataDBManage getDataDBManage(String dbName) {
        if (dbManage == null) {
            dbManage = new DataDBManage(this, dbName);
        }
        return dbManage;
    }

    private FileDBManage fileDBManage;

    public FileDBManage getFileDBManage() {
        if (fileDBManage == null) {
            fileDBManage = new FileDBManage(this);
        }
        return fileDBManage;
    }

    private NotificationManager mNotificationManager;

    public NotificationManager getNotificationManager() {
        if (this.mNotificationManager == null) {
            this.mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return this.mNotificationManager;
    }

    public SharedPreferences getConfigSpfByName(String spfName) {
        return ConfigSharedPreferences.getInstance().getConfigSharedPreferences(spfName);
    }

    private UserItem userItem;

    public UserItem getUserItem() {
        try {
            if (userItem == null) {
                SharedPreferences baseDataSpf = PandLifeApp.getInstance().getConfigSpfByName(ConfigSharedPreferences.NAME_BASEDATA);
                userItem = BaseInfo.gson.fromJson(baseDataSpf.getString(Constant.Spf.MEMBER, ""), UserItem.class);
            }
        } catch (Exception e) {
            JKLog.e(TAG, "getUserItem方法中-------->：e为：" + e);
        }
        return userItem;
    }

    public void setUserItem(UserItem userItem) {
        SharedPreferences baseDataSpf = PandLifeApp.getInstance().getConfigSpfByName(ConfigSharedPreferences.NAME_BASEDATA);
        baseDataSpf.edit().putString(Constant.Spf.MEMBER, BaseInfo.gson.toJson(userItem)).commit();
        this.userItem = userItem;
    }

//	private static RequestQueue mRequestQueue;
//	/**
//	 * 获取Volley的RequestQueue
//	 * @return
//	 */
//	public  RequestQueue getRequestQueue() {
//
//		if (mRequestQueue == null) {
//
//			if (mRequestQueue == null) {
//				mRequestQueue = Volley.newRequestQueue(this);
//			}
//		}
//		return mRequestQueue;
//	}

    public void initImageLoader(Context context) {
        int maxMemory = (int) Runtime.getRuntime().maxMemory();
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .memoryCache(new LruMemoryCache(maxMemory / 4));

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config.build());
    }


    //友盟各个平台的配置，建议放在全局Application或者程序入口
   /* {
        //微信
        PlatformConfig.setWeixin("wxc2739937484f80a6", "64ba7a1b06cfb2aa27021312839f7626");
        //新浪微博
        PlatformConfig.setSinaWeibo("4273266005", "04b48b094faeb16683c32669824ebdad");
        // QQ和Qzone appid appkey
//        PlatformConfig.setQQZone("1105102036", "joFXAaXqf75g2UnA");
        PlatformConfig.setQQZone("1105130358", "Brk8RKOwg7K05GPw");


    }*/

}
