package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.utils.JKLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 1.我的已完成订单接口
 *
 * @author ww
 *
 */
public class MyOrderInfo extends BaseAbsInfo {

    private static final String TAG = "MyOrderInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<MyIndentItem> allItems = new ArrayList<MyIndentItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=User&a=Myorder" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                MyIndentItem myIndentItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    myIndentItem = BaseInfo.gson.fromJson(itemStr, MyIndentItem.class);
                    myIndentItem.insert();
                    allItems.add(myIndentItem);
                }

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<MyIndentItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<MyIndentItem> allItems) {
        this.allItems = allItems;
    }
}
