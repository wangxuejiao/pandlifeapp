package com.jiakang.pandlife.info;

import android.util.Log;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.Post;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by play on 2016/1/18.
 */
public class PostInfo extends BaseAbsInfo{

    private String title;
    private String content;
    private int type;
    private List<String> imgList = new ArrayList<>();
    private Post post;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<String> getImgList() {
        return imgList;
    }

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post postInfo) {
        this.post = postInfo;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Forum&a=Add&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        StringBuffer stringBuffer = new StringBuffer();
        String imglist = null;
        if(imgList!=null && imgList.size()>0){

            for (int i = 0; i < imgList.size(); i++) {

                stringBuffer = stringBuffer.append(imgList.get(i));
                stringBuffer.append(",");
            }
            imglist = stringBuffer.substring(0,stringBuffer.length() - 1);
        }

        Log.i("asdfadsf","------->stringBuffer:" + stringBuffer);

        JSONObject json = new JSONObject();
        try {
            json.put("title",title);
            json.put("content",content);
            json.put("type",type);
            json.put("imglist",imglist);
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        post = BaseInfo.gson.fromJson(jsonObject.toString(),Post.class);
    }
}
