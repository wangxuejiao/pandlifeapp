package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

import java.util.List;

/**
 * Created by play on 2016/1/5.
 */
public class ToolRentItem {
    /**
     * data : [{"add_title":"让您在旅途中永远不断电","deposit":"100.00","id":"1","pic":"//img.alicdn.com/bao/uploaded/i7/TB1xygEHXXXXXaaXXXXIJbp8pXX_023724.jpg_b.jpg","title":"移动电源","nums":"10"}]
     * status : 1
     * info : 租借列表
     */
    private List<DataEntity> data;
    private int status;
    private String info;


    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }





    public class DataEntity  extends Item{
        /**
         * add_title : 让您在旅途中永远不断电
         * deposit : 100.00
         * id : 1
         * pic : //img.alicdn.com/bao/uploaded/i7/TB1xygEHXXXXXaaXXXXIJbp8pXX_023724.jpg_b.jpg
         * title : 移动电源
         * nums : 10
         */
        private String add_title;
        private String deposit;
        private String id;
        private String pic;
        private String title;
        private String nums;
        private int layout = R.layout.item_tool_rent;

        @Override
        public int getItemLayoutId() {
            return layout;
        }

        public void setLayout(int layout) {
            this.layout = layout;
        }

        public void setAdd_title(String add_title) {
            this.add_title = add_title;
        }

        public void setDeposit(String deposit) {
            this.deposit = deposit;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setNums(String nums) {
            this.nums = nums;
        }

        public String getAdd_title() {
            return add_title;
        }

        public String getDeposit() {
            return deposit;
        }

        public String getId() {
            return id;
        }

        public String getPic() {
            return pic;
        }

        public String getTitle() {
            return title;
        }

        public String getNums() {
            return nums;
        }
    }

}
