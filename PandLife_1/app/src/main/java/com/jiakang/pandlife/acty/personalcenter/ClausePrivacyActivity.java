package com.jiakang.pandlife.acty.personalcenter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

/**
 *隐私政策
 */
@EActivity(R.layout.activity_clause_privacy)
public class ClausePrivacyActivity extends BaseActy {
    protected static final String TAG = "ClausePrivacyActivity";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.acp_tb_title, "隐私政策");
    }
}
