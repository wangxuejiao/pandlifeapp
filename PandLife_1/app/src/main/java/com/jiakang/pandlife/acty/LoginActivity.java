package com.jiakang.pandlife.acty;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.alibaba.sdk.android.AlibabaSDK;
//import com.alibaba.sdk.android.callback.CallbackContext;
//import com.alibaba.sdk.android.callback.InitResultCallback;
//import com.alibaba.sdk.android.login.LoginService;
//import com.alibaba.sdk.android.login.callback.LoginCallback;
//import com.alibaba.sdk.android.session.model.Session;
import com.alibaba.sdk.android.AlibabaSDK;
import com.alibaba.sdk.android.callback.CallbackContext;
import com.alibaba.sdk.android.callback.InitResultCallback;
import com.alibaba.sdk.android.login.LoginService;
import com.alibaba.sdk.android.login.callback.LoginCallback;
import com.alibaba.sdk.android.session.model.Session;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.register.GetPasswdActy_;
import com.jiakang.pandlife.acty.register.OtherLoginBandSelectActivity;
import com.jiakang.pandlife.acty.register.OtherLoginBandSelectActivity_;
import com.jiakang.pandlife.acty.register.RegisterGetCodeActivity_;
import com.jiakang.pandlife.acty.register.SelectLocationOrManualActivity;
import com.jiakang.pandlife.acty.register.SelectLocationOrManualActivity_;
import com.jiakang.pandlife.acty.register.SelectProvinceActivity_;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.db.AreaDBManage;
import com.jiakang.pandlife.info.LoginInfo;
import com.jiakang.pandlife.info.MyInfo;
import com.jiakang.pandlife.info.OtherLoginBandInfo;
import com.jiakang.pandlife.info.PartyLoginInfo;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.JKLog;
import com.mob.tools.utils.UIHandler;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;

@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActy implements AMapLocationListener,PlatformActionListener,Handler.Callback {


    @ViewById(R.id.al_et_account)
    AutoCompleteTextView userNameET;
    @ViewById(R.id.al_et_password)
    EditText passwordET;
    @ViewById(R.id.al_btn_land)
    Button landBN;
    @ViewById(R.id.al_tv_forgetpassword)
    TextView forgetPasswordTV;
    @ViewById(R.id.al_iv_qq)
    ImageView qqIV;
    @ViewById(R.id.al_iv_wechat)
    ImageView wechatIV;
    @ViewById(R.id.al_iv_taobao)
    ImageView taobaoIV;

    /**
     * 登录接口
     */
    private LoginInfo loginInfo = new LoginInfo();
    /** 第三方登陆接口 */
    private PartyLoginInfo mPartyLoginInfo = new PartyLoginInfo();
    /** 第三方登陆绑定接口 */
    private OtherLoginBandInfo mOtherLoginBandInfo = new OtherLoginBandInfo();
    /** 记录第三方数据用户id */
    public static String webid;
    /** 记录第三方数据用户昵称 */
    public static String webname;
    /** 记录第三方数据用户头像 */
    public static String webpic;
    /** 记录第三方类型 */
    public static String type;
    /** 获取我的信息接口 */
    private MyInfo mMyInfo = new MyInfo();
    private UserItem mUserItem;

    private AreaDBManage mAreaDBManage;

    private SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

    private AMapLocationClient locationClient = null;
    private AMapLocationClientOption locationOption = null;

    private static final int MSG_TOAST = 1;
    private static final int MSG_ACTION_CCALLBACK = 2;
    private static final int MSG_CANCEL_NOTIFY = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);


    }

    @Override
    protected void onResume() {
        super.onResume();
//        UserItem userItem = getMyApp().getUserItem();
//        if (userItem != null) {
        String userName = myAccount.getString(Constant.Spf.USERNAME, "18248792600");
        userNameET.setText(userName);// "139 1299 9583"
        String rembPassWord = myAccount.getString(Constant.Spf.PASSWORD, "123456");
        passwordET.setText(rembPassWord);
        userNameET.setSelection(userNameET.getText().length());
        passwordET.setSelection(rembPassWord.length());
//        } else {
//            userNameET.setText("");
//            passwordET.setText("");
//        }
    }

    @AfterViews
    public void initView() {
        initTitleBar(R.id.al_tb_title, "登录", null, "注册");
        findViewById(R.id.it_ibn_left).setVisibility(View.GONE);

        //initial ShareSDK
        ShareSDK.initSDK(this);

        landBN.setOnClickListener(this);
        forgetPasswordTV.setOnClickListener(this);
        qqIV.setOnClickListener(this);
        wechatIV.setOnClickListener(this);
        taobaoIV.setOnClickListener(this);

        //初始化定位对象
        locationClient = new AMapLocationClient(this.getApplicationContext());
        locationOption = new AMapLocationClientOption();
        // 设置定位模式为低功耗模式
        locationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        // 设置定位监听
        locationClient.setLocationListener(this);
        // 设置为单次定位
        locationOption.setOnceLocation(true);
        locationOption.setNeedAddress(true);
        // 设置定位参数
        locationClient.setLocationOption(locationOption);
        // 启动定位
        locationClient.startLocation();

        // 如果用户名改变，清空密码
        userNameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordET.setText(null);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //初始化数据库
        mAreaDBManage = PandLifeApp.getInstance().getAreaDBManage();

//        AlibabaSDK.turnOnDebug();
//        //初始化淘宝第三方登录
//        AlibabaSDK.asyncInit(this, new InitResultCallback() {
//            @Override
//            public void onSuccess() {
//                Toast.makeText(LoginActivity.this, "初始化成功", Toast.LENGTH_SHORT).show();
//                JKLog.i(TAG, "初始化淘宝第三方登录初始化成功-------------------->");
//            }
//
//            @Override
//            public void onFailure(int code, String message) {
//                Toast.makeText(LoginActivity.this, "初始化异常", Toast.LENGTH_SHORT).show();
//                JKLog.i(TAG, "初始化淘宝第三方登录初始化异常-------------------->");
//            }
//        });

        //判断是否跳过登录和绑定站点小区
        isJumpLoginAndBindSite();



//        String registerStr = registerTV.getText().toString();
//        SpannableString spannableRegister = new SpannableString(registerStr);
//        spannableRegister.setSpan(new ClickableSpan() {
//            @Override
//            public void onClick(View widget) {
////                Intent intentRegister = new Intent(LoginActivity.this, RegisterGetCodeActivity_.class);
////                startActivity(intentRegister);
//            }
//        }, registerStr.length() - 6, registerStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        spannableRegister.setSpan(new ForegroundColorSpan(Color.GREEN), registerStr.length() - 6, registerStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

    }

    /**
     * 是否跳过登录，是否进入绑定小区、站点方法
     */
    private void isJumpLoginAndBindSite(){
        //是否跳过登录
        String token = myAccount.getString(Constant.Spf.TOKEN, "");
        if (!TextUtils.isEmpty(token)){
            BaseInfo.token = token;
            mUserItem = getMyApp().getUserItem();
            int siteId = 0;
            if (mUserItem != null){
                siteId = mUserItem.getStation_id();
                if (siteId != 0){
                    Intent intent = new Intent(mContext, MainActivity_.class);
                    startActivity(intent);
                    finish();
                }else{
                    Intent intentSelectLocationOrManual = new Intent(mContext, SelectLocationOrManualActivity_.class);
                    intentSelectLocationOrManual.putExtra(SelectLocationOrManualActivity.SITE_OR_COMMUNITY , "community_and_site");
                    startActivityForResult(intentSelectLocationOrManual, Constant.StaticCode.REQUSET_BIND_SITE);
                }
            }else{
                ApiManager apiManager = ApiManager.getInstance();
                apiManager.request(mMyInfo, new AbsOnRequestListener(mContext){
                    @Override
                    public void onRequestSuccess(int result, JSONObject jsonObject) {
                        super.onRequestSuccess(result, jsonObject);
                        try {
                            mUserItem = mMyInfo.getUserItem();
//                            if (mUserItem == null){
//                                CustomToast.showToast(mContext, "获取信息失败，请稍后重试");
//                                return;
//                            }
                            //如果获取的用户实体不为空那么就会掉该方法
                            isJumpLoginAndBindSite();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
//            Intent intent = new Intent(mContext, MainActivity_.class);
//            startActivity(intent);
        }
    }

    /**
     * 登录方法
     */
    private void login() {
//        Intent intent = new Intent(mContext, MainActivity_.class);
//        startActivity(intent);

        String userName = userNameET.getText().toString();
        String password = passwordET.getText().toString();
        if (TextUtils.isEmpty(userName)) {
            CustomToast.showToast(mContext, "用户名不能为空");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            CustomToast.showToast(mContext, "密码不能为空");
            return;
        }

        //将用户登录密码缓存
        final SharedPreferences.Editor editor = myAccount.edit();
        editor.putString(Constant.Spf.USERNAME, userName);
        editor.putString(Constant.Spf.PASSWORD, password);
        editor.commit();

        ApiManager apiManager = ApiManager.getInstance();
        loginInfo.setmUserName(userName);
        loginInfo.setmPassword(password);
        apiManager.request(loginInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
//                    if (loginInfo.getUserItem() != null){

                        CustomToast.showToast(mContext, "登录成功");
                        //当登录成功后做调用判断方法
                        isJumpLoginAndBindSite();

//                        Intent intentSelectProvince = new Intent(mContext, SelectProvinceActivity_.class);
//                        startActivityForResult(intentSelectProvince, Constant.StaticCode.REQUSET_BIND_SITE);
//                        Intent intent = new Intent(mContext, MainActivity_.class);
//                        startActivity(intent);
//                        startActivity(LoginActivity.this, MainActivity.class);
//                        finish();
//                    }
                    } else {
                        JKLog.i(TAG, "登录失败");
                    }
                } catch (Exception e) {

                }
            }
        });

    }

    /**
     * 忘记密码方法
     */
    private void forgetPassword() {
        Intent intentForgetPwd = new Intent(this, GetPasswdActy_.class);
        startActivity(intentForgetPwd);

    }

    /**
     * 进入第三方登录方法（QQ、weixin）
     */
    private void otherLogin(String type){
        if (("qq").equals(type)) {
            //初始化QQ平台
            //Initialize SinaWeibo object
            Platform pf = ShareSDK.getPlatform(LoginActivity.this, QQ.NAME);
            pf.SSOSetting(false);
            //设置监听
            //Setting listener
            pf.setPlatformActionListener(LoginActivity.this);
            //获取登陆用户的信息，如果没有授权，会先授权，然后获取用户信息
            //Perform showUser action,in order to get user info;
            pf.showUser(null);
        }else if(("weixin").equals(type)){
            //初始化微信平台
            Platform pf = ShareSDK.getPlatform(LoginActivity.this, Wechat.NAME);
            pf.SSOSetting(true);
            //设置监听
            //Setting listener
            pf.setPlatformActionListener(LoginActivity.this);
            //获取登陆用户的信息，如果没有授权，会先授权，然后获取用户信息
            //Perform showUser action,in order to get user info;
            pf.showUser(null);
        }
    }

    public void showAliLogin() {
        //调用getService方法来获取服务
        LoginService loginService = AlibabaSDK.getService(LoginService.class);
        loginService.showLogin(LoginActivity.this, new LoginCallback() {
            @Override
            public void onSuccess(Session session) {
                // 当前是否登录状态           boolean isLogin();
                // 登录授权时间           long getLoginTime();
                // 当前用户ID           String getUserId();
                // 用户其他属性           User getUser();
                //User中：淘宝用户名  淘宝用户ID   淘宝用户头像地址
                Toast.makeText(LoginActivity.this, "-isLogin-"+session.isLogin()+"-UserId-"+session.getUserId() + "-LoginTime-"+ session.getLoginTime()+"[user]:nick="+session.getUser().nick + "头像"+ session.getUser().avatarUrl,Toast.LENGTH_SHORT).show();
                webid = session.getUserId();
                webname = session.getUser().nick;
                webpic = session.getUser().avatarUrl;
                if ((TextUtils.isEmpty(webid)) || (TextUtils.isEmpty(webname))) {
                    CustomToast.showToast(mContext, "第三方获取的信息缺少，请使用其他登录方式登录");
                } else {
                    //进入第三方登录方法
                    partyLogin();
                }
            }
            @Override
            public void onFailure(int code, String message) {
                Toast.makeText(LoginActivity.this, "授权取消"+code+message,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 第三方登录方法
     */
    private void partyLogin(){
        mPartyLoginInfo.setWebid(webid);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mPartyLoginInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    if (!TextUtils.isEmpty(mPartyLoginInfo.getToken())){
                        BaseInfo.token = mPartyLoginInfo.getToken();
                        //将用户登录token缓存
                        final SharedPreferences.Editor editor = myAccount.edit();
                        editor.putString(Constant.Spf.TOKEN, "");
                        editor.commit();
                        //当第三方登录成功后判断是否绑定过小区和站点
                        isJumpLoginAndBindSite();
                    }else{
                        Intent intentOthterLoginBandSelect = new Intent(mContext, OtherLoginBandSelectActivity_.class);
                        intentOthterLoginBandSelect.putExtra("webid", webid);
                        intentOthterLoginBandSelect.putExtra("webname", webname);
                        intentOthterLoginBandSelect.putExtra("webpic", webpic);
                        intentOthterLoginBandSelect.putExtra("type", type);
                        startActivityForResult(intentOthterLoginBandSelect, Constant.StaticCode.REQUEST_SELECT_OTHER_BAND);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestFail(int result, JSONObject jsonObject) {
                super.onRequestFail(result, jsonObject);
                try {
                    Intent intentOthterLoginBandSelect = new Intent(mContext, OtherLoginBandSelectActivity_.class);
                    intentOthterLoginBandSelect.putExtra("webid", webid);
                    intentOthterLoginBandSelect.putExtra("webname", webname);
                    intentOthterLoginBandSelect.putExtra("webpic", webpic);
                    intentOthterLoginBandSelect.putExtra("type", type);
                    startActivityForResult(intentOthterLoginBandSelect, Constant.StaticCode.REQUEST_SELECT_OTHER_BAND);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        CallbackContext.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUSET_BIND_SITE) && (resultCode == RESULT_OK)){
            Intent intent = new Intent(mContext, MainActivity_.class);
            startActivity(intent);
        }
        if ((requestCode == Constant.StaticCode.REQUEST_SELECT_OTHER_BAND) && (resultCode == RESULT_OK)){
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.it_btn_right://点击进入注册页面
                Intent intent = new Intent(this, RegisterGetCodeActivity_.class);
                startActivity(intent);
                break;
            case R.id.al_btn_land://点击进行登录
                login();
                break;
            case R.id.al_tv_forgetpassword://点击进入忘记密码
                forgetPassword();
                break;
            case R.id.al_iv_qq://点击进行qq第三方登录
                Platform pfQQ = ShareSDK.getPlatform(LoginActivity.this, QQ.NAME);
                pfQQ.getDb().removeAccount();
                type = "qq";
                otherLogin(type);
                break;
            case R.id.al_iv_wechat://点击进行微信第三方登录
                Platform pfWechat = ShareSDK.getPlatform(LoginActivity.this, Wechat.NAME);
                pfWechat.getDb().removeAccount();
                type = "weixin";
                otherLogin(type);
                break;
            case R.id.al_iv_taobao://点击进行淘宝第三方登录
                type = "taobao";
//                showAliLogin();
                break;
            default:
                break;
        }
    }

    private long firstTime;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                long secondTime = System.currentTimeMillis();
                if (secondTime - firstTime > 1500) {// 如果两次按键时间间隔大于1500毫秒，则退出
                    CustomToast.showToast(mContext, "双击退出");
                    firstTime = secondTime;// 更新firstTime

                    return true;
                } else {
                    // 退出
//				stopService();
                    //退出后再进入是刚刚登录
//                    IMData.isJustNowLogin = true;
                    PandLifeApp.getInstance().exitApp(mContext, false, false, false);
//				JobHuntingApp.getInstance().exitApp(mContext);
                }
                try {
//                    unregisterReceiver(receiver);
                } catch (Exception e) {
                }
                return true;
            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != locationClient) {
            /**
             * 如果AMapLocationClient是在当前Activity实例化的，
             * 在Activity的onDestroy中一定要执行AMapLocationClient的onDestroy
             */
            locationClient.onDestroy();
            locationClient = null;
            locationOption = null;
        }
        ShareSDK.stopSDK(this);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        //定位回调
        JKLog.i(TAG, aMapLocation.toString());
    }


    //设置监听http://sharesdk.cn/androidDoc/cn/sharesdk/framework/PlatformActionListener.html
    //监听是子线程，不能Toast，要用handler处理，不要犯这么二的错误
    //Setting listener, http://sharesdk.cn/androidDoc/cn/sharesdk/framework/PlatformActionListener.html
    //The listener is the child-thread that can not handle ui
    @Override
    public void onCancel(Platform platform, int action) {
        JKLog.i(TAG, "登录和授权关闭------------------->" + action);
        Message msg = new Message();
        msg.what = MSG_ACTION_CCALLBACK;
        msg.arg1 = 3;
        msg.arg2 = action;
        msg.obj = platform;
        UIHandler.sendMessage(msg, this);
    }

    @Override
    public void onComplete(Platform platform, int action, HashMap<String, Object> res) {
        /** res是返回的数据，例如showUser(null),返回用户信息，对其解析就行
         *   http://sharesdk.cn/androidDoc/cn/sharesdk/framework/PlatformActionListener.html
         *   1、不懂如何解析hashMap的，可以上网搜索一下
         *   2、可以参考官网例子中的GetInforPage这个类解析用户信息
         *   3、相关的key-value,可以看看对应的开放平台的api
         *     如新浪的：http://open.weibo.com/wiki/2/users/show
         *     腾讯微博：http://wiki.open.t.qq.com/index.php/API%E6%96%87%E6%A1%A3/%E5%B8%90%E6%88%B7%E6%8E%A5%E5%8F%A3/%E8%8E%B7%E5%8F%96%E5%BD%93%E5%89%8D%E7%99%BB%E5%BD%95%E7%94%A8%E6%88%B7%E7%9A%84%E4%B8%AA%E4%BA%BA%E8%B5%84%E6%96%99
         *
         *	The param of res is the showUser(null) method action return data.
         *   You should analyse the return data of res by yourself
         *   1、Also,you can refer our GetInforPage.java in our official sample
         *   2、If you want to see the official document of each Weibo ,you can see the ShareSDK.xml this file
         *   For example, SinaWeibo Development Website: http://open.weibo.com/wiki/2/users/show
         *   TecentWeibo Development Website:http://wiki.open.t.qq.com/index.php/API%E6%96%87%E6%A1%A3/%E5%B8%90%E6%88%B7%E6%8E%A5%E5%8F%A3/%E8%8E%B7%E5%8F%96%E5%BD%93%E5%89%8D%E7%99%BB%E5%BD%95%E7%94%A8%E6%88%B7%E7%9A%84%E4%B8%AA%E4%BA%BA%E8%B5%84%E6%96%99
         */

        Message msg = new Message();
        msg.what = MSG_ACTION_CCALLBACK;
        msg.arg1 = 1;
        msg.arg2 = action;
        msg.obj = platform;
        UIHandler.sendMessage(msg, this);

        //hashMap object transform into json object
        //JsonUtils ju = new JsonUtils();
        //String json = ju.fromHashMap(res);
        JKLog.i(TAG, "登录和授权成功！获取到的用户信息------------------->" + res.toString());
        if (("qq").equals(type)) {
            Platform pf = ShareSDK.getPlatform(LoginActivity.this, QQ.NAME);
            webid = pf.getDb().getUserId();
            webname = pf.getDb().getUserName();
            webpic = pf.getDb().getUserIcon();
        }else if (("weixin").equals(type)){
            Platform pf = ShareSDK.getPlatform(LoginActivity.this, Wechat.NAME);
            webid = pf.getDb().getUserId();
            webname = pf.getDb().getUserName();
            webpic = pf.getDb().getUserIcon();
        }
        JKLog.e("sharesdk use_id", webid); //获取用户id
        JKLog.e("sharesdk use_name", webname);//获取用户名称
        JKLog.e("sharesdk use_icon", webpic);//获取用户头像
        if ((TextUtils.isEmpty(webid)) || (TextUtils.isEmpty(webname))) {
            CustomToast.showToast(mContext, "第三方获取的信息缺少，请使用其他登录方式登录");
            return;
        } else {
            //进入第三方登录方法
            partyLogin();
        }
    }

    @Override
    public void onError(Platform platform, int action, Throwable t) {
        t.printStackTrace();
        t.getMessage();
        Message msg = new Message();
        msg.what = MSG_ACTION_CCALLBACK;
        msg.arg1 = 2;
        msg.arg2 = action;
        msg.obj = t;
        UIHandler.sendMessage(msg, this);
        JKLog.i(TAG, "登录和授权失败------------------->" + t);
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch(msg.what) {
            case MSG_TOAST: {
                String text = String.valueOf(msg.obj);
                Toast.makeText(LoginActivity.this, text, Toast.LENGTH_SHORT).show();
            }
            break;
            case MSG_ACTION_CCALLBACK: {
                switch (msg.arg1) {
                    case 1: { // 成功, successful notification
                        //授权成功后,获取用户信息，要自己解析，看看oncomplete里面的注释
                        //ShareSDK只保存以下这几个通用值
                        if (("qq").equals(type)) {
                            Platform pf = ShareSDK.getPlatform(LoginActivity.this, QQ.NAME);
                            webid = pf.getDb().getUserId();
                            webname = pf.getDb().getUserName();
                            webpic = pf.getDb().getUserIcon();
                        }else if (("weixin").equals(type)){
                            Platform pf = ShareSDK.getPlatform(LoginActivity.this, Wechat.NAME);
                            webid = pf.getDb().getUserId();
                            webname = pf.getDb().getUserName();
                            webpic = pf.getDb().getUserIcon();
                        }
                        JKLog.e("sharesdk use_id", webid); //获取用户id
                        JKLog.e("sharesdk use_name", webname);//获取用户名称
                        JKLog.e("sharesdk use_icon", webpic);//获取用户头像
                        if ((TextUtils.isEmpty(webid)) || (TextUtils.isEmpty(webname))) {
                            CustomToast.showToast(mContext, "第三方获取的信息缺少，请使用其他登录方式登录");
                        } else {
                            //进入第三方登录方法
                            partyLogin();
                        }

                        //pf.author()这个方法每一次都会调用授权，出现授权界面
                        //如果要删除授权信息，重新授权
                        //pf.getDb().removeAccount();
                        //调用后，用户就得重新授权，否则下一次就不用授权
                    }
                    break;
                    case 2: { // 失败, fail notification
                        String expName = msg.obj.getClass().getSimpleName();
                        if ("WechatClientNotExistException".equals(expName)
                                || "WechatTimelineNotSupportedException".equals(expName)) {
                            CustomToast.showToast(mContext, "目前您的微信版本过低或未安装微信，需要安装微信才能使用");
                        }
                        else if ("GooglePlusClientNotExistException".equals(expName)) {
                            CustomToast.showToast(mContext, "Google+ 版本过低或者没有安装，需要升级或安装Google+才能使用！");
                        }
                        else if ("QQClientNotExistException".equals(expName)) {
                            CustomToast.showToast(mContext, "QQ 版本过低或者没有安装，需要升级或安装QQ才能使用！");
                        }
                        else {
                            CustomToast.showToast(mContext, "Auth unsuccessfully");
                        }
                    }
                    break;
                    case 3: { // 取消, cancel notification
                        CustomToast.showToast(mContext, "Cancel authorization");
                    }
                    break;
                }
            }
            break;
            case MSG_CANCEL_NOTIFY: {
                NotificationManager nm = (NotificationManager) msg.obj;
                if (nm != null) {
                    nm.cancel(msg.arg1);
                }
            }
            break;
        }
        return false;
    }

}
