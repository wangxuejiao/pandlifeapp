package com.jiakang.pandlife.item;

/**
 * Created by play on 2016/1/12.
 */
public class ToolRentDetail {


    /**
     * data : {"explain":"建议租借说明，请相信问问租借人，","detailed":"注意：产品版本升级为钢琴烤漆面，手感光滑。  网络最新版本均为牛皮纸盒包装。  配件：充电宝自身充电充电线（30厘米左右）*1+说明书*1","deposit_detail":"押金说明一定要写清楚，不然到时候很麻烦的事情哦。不退押金就麻烦了","station_id":"12530","add_title":"让您在旅途中永远不断电","deposit":"100.00","create":"1452481850","id":"1","pic":"//img.alicdn.com/bao/uploaded/i7/TB1xygEHXXXXXaaXXXXIJbp8pXX_023724.jpg_b.jpg","title":"移动电源","nums":"10"}
     * status : 1
     * info : 详细信息
     */
    private DataEntity data;
    private int status;
    private String info;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public DataEntity getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }

    public class DataEntity {
        /**
         * explain : 建议租借说明，请相信问问租借人，
         * detailed : 注意：产品版本升级为钢琴烤漆面，手感光滑。  网络最新版本均为牛皮纸盒包装。  配件：充电宝自身充电充电线（30厘米左右）*1+说明书*1
         * deposit_detail : 押金说明一定要写清楚，不然到时候很麻烦的事情哦。不退押金就麻烦了
         * station_id : 12530
         * add_title : 让您在旅途中永远不断电
         * deposit : 100.00
         * create : 1452481850
         * id : 1
         * pic : //img.alicdn.com/bao/uploaded/i7/TB1xygEHXXXXXaaXXXXIJbp8pXX_023724.jpg_b.jpg
         * title : 移动电源
         * nums : 10
         */
        private String explain;
        private String detailed;
        private String deposit_detail;
        private String station_id;
        private String add_title;
        private String deposit;
        private String create;
        private String id;
        private String pic;
        private String title;
        private String nums;

        public void setExplain(String explain) {
            this.explain = explain;
        }

        public void setDetailed(String detailed) {
            this.detailed = detailed;
        }

        public void setDeposit_detail(String deposit_detail) {
            this.deposit_detail = deposit_detail;
        }

        public void setStation_id(String station_id) {
            this.station_id = station_id;
        }

        public void setAdd_title(String add_title) {
            this.add_title = add_title;
        }

        public void setDeposit(String deposit) {
            this.deposit = deposit;
        }

        public void setCreate(String create) {
            this.create = create;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setNums(String nums) {
            this.nums = nums;
        }

        public String getExplain() {
            return explain;
        }

        public String getDetailed() {
            return detailed;
        }

        public String getDeposit_detail() {
            return deposit_detail;
        }

        public String getStation_id() {
            return station_id;
        }

        public String getAdd_title() {
            return add_title;
        }

        public String getDeposit() {
            return deposit;
        }

        public String getCreate() {
            return create;
        }

        public String getId() {
            return id;
        }

        public String getPic() {
            return pic;
        }

        public String getTitle() {
            return title;
        }

        public String getNums() {
            return nums;
        }
    }
}
