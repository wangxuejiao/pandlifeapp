package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.BannerItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by play on 2016/1/30.
 */
public class NeighbourBannerInfo  extends BaseAbsInfo{


    private BannerItem bannerItem;
    private List<BannerItem.DataEntityBanner> dataEntityBannerList = new ArrayList<>();

    public BannerItem getBannerItem() {
        return bannerItem;
    }

    public void setBannerItem(BannerItem bannerItem) {
        this.bannerItem = bannerItem;
    }

    public List<BannerItem.DataEntityBanner> getDataEntityBannerList() {
        return dataEntityBannerList;
    }

    public void setDataEntityBannerList(List<BannerItem.DataEntityBanner> dataEntityBannerList) {
        this.dataEntityBannerList = dataEntityBannerList;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Index&a=Getxqphhpic&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        bannerItem = BaseInfo.gson.fromJson(jsonObject.toString(),BannerItem.class);
        dataEntityBannerList = bannerItem.getData();
    }
}
