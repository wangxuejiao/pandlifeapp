package com.jiakang.pandlife.utils;

import android.content.Context;

/**
 * Created by play on 2016/1/11.
 */
public class DimensionUtil {

    /**
     * 将dp转换成px
     * @param context
     * @param dipValue
     * @return
     */
    public static float dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return dipValue * scale + 0.5f;
    }

    /**
     * 将px转换成dp
     * @param context
     * @param pxValue
     * @return
     */
    public static float px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return pxValue / scale + 0.5f;
    }
}
