//package com.jiakang.pandlife.acty.homepage;
//
//import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.TextView;
//
//import com.jiakang.pandlife.Constant;
//import com.jiakang.pandlife.R;
//import com.jiakang.pandlife.acty.BaseActy;
//import com.jiakang.pandlife.adapter.ItemAdapter;
//import com.jiakang.pandlife.item.AddressItem;
//import com.jiakang.pandlife.item.GoodsItem;
//import com.jiakang.pandlife.item.GroupPurchaseDetailItem;
//import com.jiakang.pandlife.utils.Util;
//import com.jiakang.pandlife.widget.CustomDialog;
//import com.jiakang.pandlife.widget.CustomListView;
//
//import org.androidannotations.annotations.AfterViews;
//import org.androidannotations.annotations.EActivity;
//import org.androidannotations.annotations.ViewById;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * 填写订单
// */
//@EActivity(R.layout.activity_write_indent)
//public class WriteIndentActivity extends BaseActy {
//    protected static final String TAG = "WriteIndentActivity";
//
//    @ViewById(R.id.agpi_tv_receiver_name)
//    TextView receiverNameTV;
//    @ViewById(R.id.agpi_tv_receiver_phone)
//    TextView receiverPhoneTV;
//    @ViewById(R.id.agpi_tv_receiver_address)
//    TextView receiverAddressTV;
//    @ViewById(R.id.agpi_clv_goods)
//    CustomListView goodsCLV;
//    @ViewById(R.id.agpi_tv_site_name)
//    TextView siteNameTV;
//    @ViewById(R.id.include_iip_rgp_pay_type)
//    RadioGroup payTypeRGP;
//    @ViewById(R.id.include_iip_rbn_balance)
//    RadioButton balanceRBN;
//    @ViewById(R.id.include_iip_rbn_alipay)
//    RadioButton alipayRBN;
//    @ViewById(R.id.include_iip_tv_distribution_date_select)
//    TextView distributionDateTV;
//    @ViewById(R.id.include_iip_et_distribution_note)
//    EditText distributionRemarksET;
//    @ViewById(R.id.include_iip_tv_red_package_number)
//    TextView redPackageNumberTV;
//    @ViewById(R.id.include_iip_tv_red_package_content)
//    TextView redPackageContentTV;
//    @ViewById(R.id.include_iip_tv_sum_content)
//    TextView sumTV;
//    @ViewById(R.id.include_iip_tv_freight_content)
//    TextView freightTV;
//    @ViewById(R.id.agpi_tv_count)
//    TextView countTV;
//    @ViewById(R.id.agpi_btn_confirm)
//    Button confirmBN;
//
//    private ItemAdapter mItemAdapter;
//
//    private List<GoodsItem> allItems = new ArrayList<GoodsItem>();
//
//    private String PAY_WAY = "1";
//    private AddressItem senderAddressItem;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }
//
//    @AfterViews
//    protected void initVar(){
//        mItemAdapter = new ItemAdapter(mContext);
//        goodsCLV.setAdapter(mItemAdapter);
//
//        findViewById(R.id.agpi_rl_address).setOnClickListener(this);
//        distributionDateTV.setOnClickListener(this);
//        redPackageContentTV.setOnClickListener(this);
//        confirmBN.setOnClickListener(this);
//
//
//        payTypeRGP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                switch (checkedId) {
//                    case R.id.include_iip_rbn_balance:
//                        PAY_WAY = "1";
//                        break;
//                    case R.id.include_iip_rbn_alipay:
//                        PAY_WAY = "2";
//                        break;
////                    case R.id.include_iip_rbn_wechat:
////                        break;
//                }
//            }
//        });
//
//
//        bindView();
//    }
//
//    private void bindView(){
//        Intent intent = getIntent();
//        Bundle bundle = intent.getExtras();
//        ArrayList list = bundle.getParcelableArrayList("list");
//        if ((list != null) && (list.size() != 0)) {
//            initTitleBar(R.id.agpi_tb_title, "订单确认");
//            GoodsItem goodsItem;
//            for (int i = 0; i < list.size(); i++) {
//                goodsItem = (GoodsItem) list.get(i);
//                allItems.add(goodsItem);
//            }
//            mItemAdapter.clear();
//            mItemAdapter.addItems((List) allItems);
//            mItemAdapter.notifyDataSetChanged();
//            //当为团购时有配送时间选择，显示之
//            findViewById(R.id.include_iip_rl_distribution_date).setVisibility(View.VISIBLE);
//
////            float countPrice = goodsItem.getNumber() * goodsItem.getPrice();
////            countPriceTV.setText("共" + goodsItem.getNumber() + "件商品 合计：" + countPrice + "元（含运费：￥0.00）");
//        }
//    }
//
//    /**
//     * 选择使用哪个红包的方法
//     */
//    private void selectRedPackage(){
//
//    }
//
//    /**
//     * 确认团购方法
//     */
//    private void confirm(){
//        if(("1").equals(PAY_WAY)){
//            final CustomDialog customDialog = Util.getDialog(mContext, "提示", "确定支付吗？", "确定", "取消");
//            customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    customDialog.dismiss();
//                    //调用支付接口
//
//                }
//            });
//            customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    customDialog.dismiss();
//                }
//            });
//            customDialog.show();
//        }else if(("2").equals(PAY_WAY)){
//            Intent intentPurchaseSuccess = new Intent(mContext, PurchaseSuccessActivity_.class);
//            startActivityForResult(intentPurchaseSuccess, Constant.StaticCode.REQUEST_GROUP_PURCHASE);
//        }
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if ((requestCode == Constant.StaticCode.REQUEST_GROUP_PURCHASE) && (resultCode == RESULT_CANCELED)){
//            setResult(RESULT_CANCELED);
//            finish();
//        }
//        if ((requestCode == Constant.StaticCode.REQUEST_SENDER_ADDRESS) && (resultCode == RESULT_OK)){
//            senderAddressItem = (AddressItem)data.getExtras().get("senderAddressItem");
//            receiverNameTV.setText(senderAddressItem.getName());
//            receiverPhoneTV.setText(senderAddressItem.getPhone());
//            receiverAddressTV.setText(senderAddressItem.getAddress());
//        }
//    }
//
//    @Override
//    public void onClick(View v) {
//        super.onClick(v);
//        switch (v.getId()){
//            case R.id.it_ibn_left:
//                finish();
//                break;
//            case R.id.agpi_rl_address://点击选择收件人
//                Intent intentSendAddress = new Intent(this, AddressBookManagerActivity_.class);
//                intentSendAddress.putExtra("sendOrObtain", "sender");
//                startActivityForResult(intentSendAddress, Constant.StaticCode.REQUEST_SENDER_ADDRESS);
//                break;
//            case R.id.include_iip_tv_red_package_content://点击选择使用红包
//                selectRedPackage();
//                break;
//            case R.id.agpi_btn_confirm://点击确认
//                confirm();
//                break;
//        }
//    }
//}
