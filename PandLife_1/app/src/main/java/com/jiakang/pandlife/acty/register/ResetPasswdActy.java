package com.jiakang.pandlife.acty.register;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;


import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.info.ResetpasswordInfo;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.CustomDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;


/**
 * @title 找回密码-设置
 * 
 *        {@link com.xinye.matchmake.info.GetPassWordInfo}
 * 
 * @author CQ
 */
@EActivity(R.layout.activity_forget_password2)
public class ResetPasswdActy extends BaseActy {
	protected final String TAG = "ResetPasswdActy";

	@ViewById(R.id.afp2_et_inputpwd)
	EditText pass1ET;
	@ViewById(R.id.afp2_et_reInputpwd)
	EditText pass2ET;
	@ViewById(R.id.afp2_btn_finish)
	Button finishBN;

	/** 重置密码接口 */
	private ResetpasswordInfo mResetpasswordInfo = new ResetpasswordInfo();

	private SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

//	private String phoneNum;
//	private LosePassWordInfo losePassWordInfo = new LosePassWordInfo();
//	private LoginInfo loginInfo = new LoginInfo();
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

//		phoneNum = getIntent().getStringExtra("phoneNum");
//		mId = getIntent().getStringExtra("userId");
		
	}

	@AfterViews
	protected void initVar(){
		initTitleBar(R.id.afp2_tb_title, "找回密码");

		finishBN.setOnClickListener(this);
	}


	/**
	 *
	 */
	private void finishResetPwd(){
		String passwordStr1 = pass1ET.getText().toString();
		String passwordStr2 = pass2ET.getText().toString();
		if (TextUtils.isEmpty(passwordStr1) || TextUtils.isEmpty(passwordStr2)){
			CustomToast.showToast(this, "请填写密码");
			return;
		}
		if (!passwordStr1.equals(passwordStr2)){
			CustomToast.showToast(this, "两次密码不一致，请重新填写");
			pass1ET.setText("");
			pass2ET.setText("");
			return;
		}

		ApiManager apiManager = ApiManager.getInstance();
		String mobile = myAccount.getString(Constant.Spf.MOBILE, "");
		mResetpasswordInfo.setmUserName(mobile);
		mResetpasswordInfo.setmPassword(passwordStr1);
		apiManager.request(mResetpasswordInfo, new AbsOnRequestListener(mContext){
			@Override
			public void onRequestSuccess(int stat, JSONObject jsonObject) {
				super.onRequestSuccess(stat, jsonObject);
				try {
					if (stat == 1) {
						CustomToast.showToast(ResetPasswdActy.this, "密码修改成功，下次登录请使用新密码登录");
						setResult(RESULT_OK);
						finish();
					} else {
					}
				} catch (Exception e) {

				}
			}
		});

	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			showExitDlg();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	
	/**
	 * 取消注册对话框
	 */
	private void showExitDlg() {
		final CustomDialog customDialog = Util.getDialog(mContext, "提示", "确定放弃修改吗？", "确定", "取消");
		customDialog.setOnClickListener(R.id.ok, new OnClickListener() {
			@Override
			public void onClick(View v) {
				customDialog.dismiss();
				finish();
			}
		});
		customDialog.setOnClickListener(R.id.cancel, new OnClickListener() {
			@Override
			public void onClick(View v) {
				customDialog.dismiss();
			}
		});
		customDialog.show();
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()){
			case R.id.it_ibn_left:
				finish();
				break;
			case R.id.afp2_btn_finish:
				finishResetPwd();
				break;
			default:
				break;
		}
	}
}
