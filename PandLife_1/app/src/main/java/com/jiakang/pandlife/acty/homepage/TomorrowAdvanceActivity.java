package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.GetTuanListInfo;
import com.jiakang.pandlife.info.TomorrowTuanInfo;
import com.jiakang.pandlife.item.GroupPurchaseItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.widget.PullToRefreshListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * 明日预告
 */

@EActivity(R.layout.activity_tomorrow_advance)
public class TomorrowAdvanceActivity extends BaseActy {
    protected static final String TAG = "TomorrowAdvanceActivity";

    @ViewById(R.id.ata_prlv_group_purchase)
    PullToRefreshListView groupPurchasePRLV;

    private ItemAdapter mItemAdapter;

    /** 每页个数，BaseInfo.pageSize = 20 为默认值 */
    private int pageSize = 20;
    /** 第几页 */
    private int pageIndex = 1;

    private final static int GET_NEW_OK = 0x12;
    private final static int GET_MORE_OK = 0x13;
    private final static int IS_GET_NO = 0;
    private final static int IS_GET_NEW = 1;
    private final static int IS_GET_MORE = 2;
    private static int isPullRefresh = 0;
    private boolean noUpLoadFinish = true;

    private List<GroupPurchaseItem> allItems = new ArrayList<GroupPurchaseItem>();

    /** 团购列表接口 */
    private TomorrowTuanInfo mTomorrowTuanInfo = new TomorrowTuanInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.ata_tb_title, "明日预告");

        bindView();
        bindInfo(IS_GET_NO);
    }

    private void bindView(){
        mItemAdapter = new ItemAdapter(mContext);
        groupPurchasePRLV.setShowFootView(true);
        groupPurchasePRLV.setAdapter(mItemAdapter);

        mItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {
                GroupPurchaseItem groupPurchaseItem = (GroupPurchaseItem) view.getTag();
                Intent intentGroupPurchaseDetail = new Intent(mContext, TomorrowAdvanceDetailActivity_.class);
                intentGroupPurchaseDetail.putExtra("groupPurchaseItem", groupPurchaseItem);
                startActivityForResult(intentGroupPurchaseDetail, Constant.StaticCode.REQUEST_GROUP_PURCHASE);
            }
        });
//        groupPurchasePRLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ListView listView = (ListView) parent;
//                GroupPurchaseItem groupPurchaseItem = (GroupPurchaseItem) view.getTag();
//                Intent intentGroupPurchaseDetail = new Intent(mContext, GroupPurchaseDetailActivity_.class);
//                intentGroupPurchaseDetail.putExtra("groupPurchaseItem", groupPurchaseItem);
//                startActivityForResult(intentGroupPurchaseDetail, Constant.StaticCode.REQUEST_GROUP_PURCHASE);
//            }
//        });
    }

    private void bindInfo(int isPullToRefresh){
        if (isPullToRefresh == 1){
            pageIndex = 1;
        }else if (isPullToRefresh == 2){
            pageIndex ++;
        }
        allItems.clear();
        mItemAdapter.clear();
//        BaseInfo.token = "ff0a6c74e7b4d08b659f9e2737a4ac5c";
        mTomorrowTuanInfo.setPageIndex(pageIndex);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mTomorrowTuanInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    allItems = mTomorrowTuanInfo.getAllItems();
                    if (allItems.size() == 0) {
                        CustomToast.showToast(mContext, "获取数据失败，请重试");
                        groupPurchasePRLV.setBackgroundColor(getResources().getColor(R.color.gray_bg));
                        groupPurchasePRLV.onRefreshComplete();
                        groupPurchasePRLV.onMoreComplete(false);
                        return;
                    }
                    groupPurchasePRLV.setBackgroundColor(getResources().getColor(R.color.reddish_orange));
                    if (allItems.size() < BaseInfo.pageSize){
                        noUpLoadFinish = false;
                    }else{
                        noUpLoadFinish = true;
                    }
                    mItemAdapter.addItems((List) allItems);
                    mItemAdapter.notifyDataSetChanged();
                    groupPurchasePRLV.onRefreshComplete();
                    groupPurchasePRLV.onMoreComplete(noUpLoadFinish);

                    groupPurchasePRLV.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {

                        @Override
                        public void onRefresh() {
                            // 线程获取更新数据
                            isPullRefresh = 1;
                            bindInfo(IS_GET_NEW);

                        }
                    });

                    groupPurchasePRLV.setOnMoreListener(new PullToRefreshListView.OnMoreListener() {

                        @Override
                        public void onMore() {
                            // 线程获取更多数据
                            isPullRefresh = 2;
                            bindInfo(IS_GET_MORE);

                        }
                    });

                    groupPurchasePRLV.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            if (position == mItemAdapter.getCount() + 1) {
                                return;
                            }
                            ListView listView = (ListView) parent;
                            GroupPurchaseItem groupPurchaseItem = (GroupPurchaseItem)listView.getItemAtPosition(position);
                            Intent intentGroupPurchaseDetail = new Intent(mContext, TomorrowAdvanceDetailActivity_.class);
                            intentGroupPurchaseDetail.putExtra("groupPurchaseItem", groupPurchaseItem);
                            startActivityForResult(intentGroupPurchaseDetail, Constant.StaticCode.REQUEST_GROUP_PURCHASE);
                        }
                    });

                } catch (Exception e) {

                }
            }

            @Override
            public void onRequestFail(int result, JSONObject jsonObject) {
                super.onRequestFail(result, jsonObject);
                groupPurchasePRLV.setBackgroundColor(getResources().getColor(R.color.gray_bg));
                groupPurchasePRLV.onRefreshComplete();
                groupPurchasePRLV.onMoreComplete(false);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_GROUP_PURCHASE) && (resultCode == RESULT_OK)){
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
