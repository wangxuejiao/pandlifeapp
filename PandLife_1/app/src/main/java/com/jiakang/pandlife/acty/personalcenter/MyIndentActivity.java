package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.amap.api.location.AMapLocation;
//import com.example.hjk.amap_android_navi.BasicNaviActivity;
import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.homepage.SelectPayWayActivity_;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.db.DataCacheDBHelper;
import com.jiakang.pandlife.db.DataCacheDBManage;
import com.jiakang.pandlife.info.AbolishOrderInfo;
import com.jiakang.pandlife.info.ApplyRefundInfo;
import com.jiakang.pandlife.info.MyApplyOrderInfo;
import com.jiakang.pandlife.info.MyNoPayOrderInfo;
import com.jiakang.pandlife.info.MyOrderInfo;
import com.jiakang.pandlife.info.MyPayOrderInfo;
import com.jiakang.pandlife.info.MyTuanOrderDetailInfo;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.item.RedPackageItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.DataCache;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.CustomDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的订单
 * Created by Administrator on 2016/1/14.
 */
@EActivity(R.layout.activity_my_indent)
public class MyIndentActivity extends BaseActy{
    protected static final String TAG = "MyIndentActivity";

    @ViewById(R.id.ami_rgp_label)
    RadioGroup labelRGP;
    @ViewById(R.id.ami_rbn_non_payment)
    RadioButton nonPaymentRBN;
    @ViewById(R.id.ami_rbn_stay_receipt)
    RadioButton stayReceiptRBN;
    @ViewById(R.id.ami_rbn_already_finish)
    RadioButton alreadyFinishRBN;
    @ViewById(R.id.ami_rbn_after_service)
    RadioButton afterServiceRBN;
    @ViewById(R.id.ami_lv_non_payment)
    ListView nonPaymentLV;
    @ViewById(R.id.ami_lv_stay_receipt)
    ListView stayReceiptLV;
    @ViewById(R.id.ami_lv_already_finish)
    ListView alreadyFinishLV;
    @ViewById(R.id.ami_lv_after_service)
    ListView afterServiceLV;
    @ViewById(R.id.ami_rl_no_data)
    RelativeLayout nodataRL;

    private ItemAdapter nonPaymentItemAdapter;
    private ItemAdapter stayReceiptItemAdapter;
    private ItemAdapter alreadyFinishItemAdapter;
    private ItemAdapter afterServiceItemAdapter;

    private static final int MY_INDENT_NONPAYMENT = 1;
    private static final int MY_INDENT_STAYRECEIPT = 2;
    private static final int MY_INDENT_ALREADYFINISH = 3;
    private static final int MY_INDENT_AFTERSERVICE = 4;

    /** 我的未付款订单接口 */
    private MyNoPayOrderInfo mMyNoPayOrderInfo = new MyNoPayOrderInfo();
    /** 我的已付款订单接口 */
    private MyPayOrderInfo mMyPayOrderInfo = new MyPayOrderInfo();
    /** 我的已完成订单接口 */
    private MyOrderInfo mMyOrderInfo = new MyOrderInfo();
    /** 我的退款订单接口 */
    private MyApplyOrderInfo mMyApplyOrderInfo = new MyApplyOrderInfo();
    /** 我的订单详情接口 */
    private MyTuanOrderDetailInfo mMyTuanOrderDetailInfo = new MyTuanOrderDetailInfo();
    /** 我的订单取消接口 */
    private AbolishOrderInfo mAbolishOrderInfo = new AbolishOrderInfo();
    /** 申请退款接口 */
    private ApplyRefundInfo mApplyRefundInfo = new ApplyRefundInfo();

    //保存未付款订单集合
    private List<MyIndentItem> noPayAllitems = new ArrayList<MyIndentItem>();
    //保存退款订单集合
    private List<MyIndentItem> alreadyPayAllitems = new ArrayList<MyIndentItem>();
    //保存已完成订单集合
    private List<MyIndentItem> alreadyFinishAllitems = new ArrayList<MyIndentItem>();
    //保存已付款订单集合
    private List<MyIndentItem> refundAllitems = new ArrayList<MyIndentItem>();

    private DataCacheDBManage dataCacheDBManage;

    private MyIndentItem mMyIndentItem;
    //订单编号id
    private int id;
    //订单id
    private String orid;
    //需导航的站点或商家经度
    private String longitude;
    //需导航的站点或商家经度
    private String latitude;
    //记录支付方式
    private String payWay;
    //记录支付金额
    private int paySum;
    //记录电话号码
    private String telephone;
    //记录点击的item中实体id
    private int indentId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.ami_tb_title, "我的订单");
        dataCacheDBManage = getMyApp().getCacheDataDBManage();
        bindView();
        getCacheData();
//        bindInfo(MY_INDENT_NONPAYMENT);
        getNoPayCacheData();
    }

    private void bindView(){
        nonPaymentItemAdapter = new ItemAdapter(mContext);
        stayReceiptItemAdapter = new ItemAdapter(mContext);
        alreadyFinishItemAdapter = new ItemAdapter(mContext);
        afterServiceItemAdapter = new ItemAdapter(mContext);
        nonPaymentLV.setAdapter(nonPaymentItemAdapter);
        stayReceiptLV.setAdapter(stayReceiptItemAdapter);
        alreadyFinishLV.setAdapter(alreadyFinishItemAdapter);
        afterServiceLV.setAdapter(afterServiceItemAdapter);

        labelRGP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.ami_rbn_non_payment:
                        //设置显示的listview和标题字体颜色
                        nonPaymentLV.setVisibility(View.VISIBLE);
                        stayReceiptLV.setVisibility(View.GONE);
                        alreadyFinishLV.setVisibility(View.GONE);
                        afterServiceLV.setVisibility(View.GONE);
                        nodataRL.setVisibility(View.GONE);
                        nonPaymentRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        stayReceiptRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        alreadyFinishRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        afterServiceRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        if (nonPaymentItemAdapter.getCount() == 0) {
//                            bindInfo(MY_INDENT_NONPAYMENT);
                            getNoPayCacheData();
                        }
                        break;
                    case R.id.ami_rbn_stay_receipt:
                        nonPaymentLV.setVisibility(View.GONE);
                        stayReceiptLV.setVisibility(View.VISIBLE);
                        alreadyFinishLV.setVisibility(View.GONE);
                        afterServiceLV.setVisibility(View.GONE);
                        nodataRL.setVisibility(View.GONE);
                        nonPaymentRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        stayReceiptRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        alreadyFinishRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        afterServiceRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        if (stayReceiptItemAdapter.getCount() == 0) {
//                            bindInfo(MY_INDENT_STAYRECEIPT);
                            getAlreadyPayCacheData();
                        }
                        break;
                    case R.id.ami_rbn_already_finish:
                        nonPaymentLV.setVisibility(View.GONE);
                        stayReceiptLV.setVisibility(View.GONE);
                        alreadyFinishLV.setVisibility(View.VISIBLE);
                        afterServiceLV.setVisibility(View.GONE);
                        nodataRL.setVisibility(View.GONE);
                        nonPaymentRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        stayReceiptRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        alreadyFinishRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        afterServiceRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        if (alreadyFinishItemAdapter.getCount() == 0) {
//                            bindInfo(MY_INDENT_ALREADYFINISH);
                            getAlreadyFinishCacheData();
                        }
                        break;
                    case R.id.ami_rbn_after_service:
                        nonPaymentLV.setVisibility(View.GONE);
                        stayReceiptLV.setVisibility(View.GONE);
                        alreadyFinishLV.setVisibility(View.GONE);
                        afterServiceLV.setVisibility(View.VISIBLE);
                        nodataRL.setVisibility(View.GONE);
                        nonPaymentRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        stayReceiptRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        alreadyFinishRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        afterServiceRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        if (afterServiceItemAdapter.getCount() == 0) {
//                            bindInfo(MY_INDENT_AFTERSERVICE);
                            getRefundCacheData();
                        }
                        break;
                }
            }
        });

        //未付款的条目点击和按钮点击
        nonPaymentItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {
                MyIndentItem myIndentItem = (MyIndentItem) view.getTag();
                String clickStr = myIndentItem.getClickStr();
                payWay = myIndentItem.getPay_kind();
                paySum = myIndentItem.getPay_money();
                longitude = myIndentItem.getLongitude();
                latitude = myIndentItem.getLatitude();
                indentId = myIndentItem.getId();
                orid = myIndentItem.getOrid();
                id = myIndentItem.getId();
                responseClick(clickStr, "nonPay");
            }
        });
        nonPaymentLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listView = (ListView) parent;
                MyIndentItem myIndentItem = (MyIndentItem) listView.getItemAtPosition(position);
                int indentId = myIndentItem.getId();
                orid = myIndentItem.getOrid();
                getMyIndentDetail(indentId);
//                Intent intentMyIndentDetail = new Intent(mContext, MyIndentNoPayDetailActivity_.class);
//                intentMyIndentDetail.putExtra("myIndentItem", myIndentItem);
//                startActivity(intentMyIndentDetail);
            }
        });
        //已付款的条目点击和按钮点击
        stayReceiptItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {
                MyIndentItem myIndentItem = (MyIndentItem) view.getTag();
                String clickStr = myIndentItem.getClickStr();
                payWay = myIndentItem.getPay_kind();
                paySum = myIndentItem.getPay_money();
                longitude = myIndentItem.getLongitude();
                latitude = myIndentItem.getLatitude();
                indentId = myIndentItem.getId();
                orid = myIndentItem.getOrid();
                id = myIndentItem.getId();
                responseClick(clickStr, "alreadPay");
            }
        });
        stayReceiptLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listView = (ListView) parent;
                MyIndentItem myIndentItem = (MyIndentItem) listView.getItemAtPosition(position);
                int indentId = myIndentItem.getId();
                orid = myIndentItem.getOrid();
                getMyIndentDetail(indentId);
//                Intent intentMyIndentDetail = new Intent(mContext, MyIndentNoPayDetailActivity_.class);
//                intentMyIndentDetail.putExtra("myIndentItem", myIndentItem);
//                startActivity(intentMyIndentDetail);
            }
        });
        //已完成的条目点击和按钮点击
        alreadyFinishItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {
                MyIndentItem myIndentItem = (MyIndentItem) view.getTag();
                String clickStr = myIndentItem.getClickStr();
                payWay = myIndentItem.getPay_kind();
                paySum = myIndentItem.getPay_money();
                longitude = myIndentItem.getLongitude();
                latitude = myIndentItem.getLatitude();
                indentId = myIndentItem.getId();
                orid = myIndentItem.getOrid();
                id = myIndentItem.getId();
                telephone = myIndentItem.getStationtel();
                responseClick(clickStr, "alreadyFinish");
            }
        });
        alreadyFinishLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listView = (ListView) parent;
                MyIndentItem myIndentItem = (MyIndentItem) listView.getItemAtPosition(position);
                int indentId = myIndentItem.getId();
                orid = myIndentItem.getOrid();
                getMyIndentDetail(indentId);
//                Intent intentMyIndentDetail = new Intent(mContext, MyIndentNoPayDetailActivity_.class);
//                intentMyIndentDetail.putExtra("myIndentItem", myIndentItem);
//                startActivity(intentMyIndentDetail);
            }
        });
        //已退款的条目点击和按钮点击
        afterServiceItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {
                MyIndentItem myIndentItem = (MyIndentItem) view.getTag();
                String clickStr = myIndentItem.getClickStr();
                payWay = myIndentItem.getPay_kind();
                paySum = myIndentItem.getPay_money();
                longitude = myIndentItem.getLongitude();
                latitude = myIndentItem.getLatitude();
                indentId = myIndentItem.getId();
                orid = myIndentItem.getOrid();
                id = myIndentItem.getId();
                telephone = myIndentItem.getStationtel();
                responseClick(clickStr, "alreadyFinish");
            }
        });
        //退款订单点击条目点击
        afterServiceLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listView = (ListView) parent;
                MyIndentItem myIndentItem = (MyIndentItem) listView.getItemAtPosition(position);
                int indentId = myIndentItem.getId();
                orid = myIndentItem.getOrid();
                getMyIndentDetail(indentId);
//                Intent intentMyIndentDetail = new Intent(mContext, MyIndentNoPayDetailActivity_.class);
//                intentMyIndentDetail.putExtra("myIndentItem", myIndentItem);
//                startActivity(intentMyIndentDetail);
            }
        });
    }

    /**
     * 通过数据库获取我的订单列表信息
     */
    private void getCacheData(){
        if (dataCacheDBManage.getGetMyIndentList().size() != 0){
            for (int i=0; i < dataCacheDBManage.getGetMyIndentList().size(); i ++){
                if (("1").equals(dataCacheDBManage.getGetMyIndentList().get(i).getOrder_type())){
                    noPayAllitems.add(dataCacheDBManage.getGetMyIndentList().get(i));
                }else if (("2").equals(dataCacheDBManage.getGetMyIndentList().get(i).getOrder_type()) || ("3").equals(dataCacheDBManage.getGetMyIndentList().get(i).getOrder_type())){
                    alreadyPayAllitems.add(dataCacheDBManage.getGetMyIndentList().get(i));
                }else if (("4").equals(dataCacheDBManage.getGetMyIndentList().get(i).getOrder_type())){
                    alreadyFinishAllitems.add(dataCacheDBManage.getGetMyIndentList().get(i));
                }else if (("6").equals(dataCacheDBManage.getGetMyIndentList().get(i).getOrder_type()) || ("7").equals(dataCacheDBManage.getGetMyIndentList().get(i).getOrder_type())){
                    refundAllitems.add(dataCacheDBManage.getGetMyIndentList().get(i));
                }
            }
        }

    }

    /**
     * 初始化未付款列表
     */
    private void getNoPayCacheData(){
        if (noPayAllitems.size() != 0){
            nonPaymentItemAdapter.clear();
            nonPaymentItemAdapter.addItems((List) noPayAllitems);
            nonPaymentItemAdapter.notifyDataSetChanged();

            MyIndentItem myIndentItem = noPayAllitems.get(0);
            if ((myIndentItem.getCurrentTime() + myIndentItem.getSaveTime()) < System.currentTimeMillis()) {
                //清除数据库
                dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYINDENT_CACHE);
                //获取网络请求数据
                bindInfo(MY_INDENT_NONPAYMENT);
            } else {
            }
        }else{
            bindInfo(MY_INDENT_NONPAYMENT);
        }
    }
    /**
     * 初始化已付款列表
     */
    private void getAlreadyPayCacheData(){
        if (alreadyPayAllitems.size() != 0){
            stayReceiptItemAdapter.clear();
            stayReceiptItemAdapter.addItems((List) alreadyPayAllitems);
            stayReceiptItemAdapter.notifyDataSetChanged();

            MyIndentItem myIndentItem = alreadyPayAllitems.get(0);
            if ((myIndentItem.getCurrentTime() + myIndentItem.getSaveTime()) < System.currentTimeMillis()) {
                //清除数据库
                dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYINDENT_CACHE);
                //获取网络请求数据
                bindInfo(MY_INDENT_STAYRECEIPT);
            } else {
            }
        }else{
            bindInfo(MY_INDENT_STAYRECEIPT);
        }
    }
    /**
     * 初始化已完成列表
     */
    private void getAlreadyFinishCacheData(){
        if (alreadyFinishAllitems.size() != 0){
            alreadyFinishItemAdapter.clear();
            alreadyFinishItemAdapter.addItems((List) alreadyFinishAllitems);
            alreadyFinishItemAdapter.notifyDataSetChanged();

            MyIndentItem myIndentItem = alreadyFinishAllitems.get(0);
            if ((myIndentItem.getCurrentTime() + myIndentItem.getSaveTime()) < System.currentTimeMillis()) {
                //清除数据库
                dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYINDENT_CACHE);
                //获取网络请求数据
                bindInfo(MY_INDENT_ALREADYFINISH);
            } else {
            }
        }else{
            bindInfo(MY_INDENT_ALREADYFINISH);
        }
    }
    /**
     * 初始化退款列表
     */
    private void getRefundCacheData(){
        if (refundAllitems.size() != 0){
            afterServiceItemAdapter.clear();
            afterServiceItemAdapter.addItems((List) refundAllitems);
            afterServiceItemAdapter.notifyDataSetChanged();

            MyIndentItem myIndentItem = refundAllitems.get(0);
            if ((myIndentItem.getCurrentTime() + myIndentItem.getSaveTime()) < System.currentTimeMillis()) {
                //清除数据库
                dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYINDENT_CACHE);
                //获取网络请求数据
                bindInfo(MY_INDENT_AFTERSERVICE);
            } else {
            }
        }else{
            bindInfo(MY_INDENT_AFTERSERVICE);
        }
    }

    /**
     * 获取网络数据
     * @param my_indent
     */
    private void bindInfo(int my_indent){
        //获取显示未付款
        if (MY_INDENT_NONPAYMENT == my_indent){
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mMyNoPayOrderInfo, new AbsOnRequestListener(mContext) {
                @Override
                public void onRequestSuccess(int result, JSONObject jsonObject) {
                    super.onRequestSuccess(result, jsonObject);
                    try {
                        nonPaymentItemAdapter.clear();
                        if (mMyNoPayOrderInfo.getAllItems().size() == 0) {
                            CustomToast.showToast(mContext, "当前没有未付款订单");
                            nonPaymentLV.setVisibility(View.GONE);
                            stayReceiptLV.setVisibility(View.GONE);
                            alreadyFinishLV.setVisibility(View.GONE);
                            afterServiceLV.setVisibility(View.GONE);
                            nodataRL.setVisibility(View.VISIBLE);
                            return;
                        }
                        nonPaymentItemAdapter.clear();
                        nonPaymentItemAdapter.addItems((List) mMyNoPayOrderInfo.getAllItems());
                        nonPaymentItemAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        //获取显示待收货
        else if (MY_INDENT_STAYRECEIPT == my_indent){
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mMyPayOrderInfo, new AbsOnRequestListener(mContext){
                @Override
                public void onRequestSuccess(int result, JSONObject jsonObject) {
                    super.onRequestSuccess(result, jsonObject);
                    try{
                        stayReceiptItemAdapter.clear();
                        if (mMyPayOrderInfo.getAllItems().size() == 0){
                            CustomToast.showToast(mContext, "当前没有已付款订单");
                            nonPaymentLV.setVisibility(View.GONE);
                            stayReceiptLV.setVisibility(View.GONE);
                            alreadyFinishLV.setVisibility(View.GONE);
                            afterServiceLV.setVisibility(View.GONE);
                            nodataRL.setVisibility(View.VISIBLE);
                            return;
                        }
                        stayReceiptItemAdapter.clear();
                        stayReceiptItemAdapter.addItems((List)mMyPayOrderInfo.getAllItems());
                        stayReceiptItemAdapter.notifyDataSetChanged();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
        //获取显示已完成
        else if (MY_INDENT_ALREADYFINISH == my_indent){
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mMyOrderInfo, new AbsOnRequestListener(mContext){
                @Override
                public void onRequestSuccess(int result, JSONObject jsonObject) {
                    super.onRequestSuccess(result, jsonObject);
                    try{
                        alreadyFinishItemAdapter.clear();
                        if (mMyOrderInfo.getAllItems().size() == 0){
                            CustomToast.showToast(mContext, "当前没有已完成订单");
                            nonPaymentLV.setVisibility(View.GONE);
                            stayReceiptLV.setVisibility(View.GONE);
                            alreadyFinishLV.setVisibility(View.GONE);
                            afterServiceLV.setVisibility(View.GONE);
                            nodataRL.setVisibility(View.VISIBLE);
                            return;
                        }
                        alreadyFinishItemAdapter.clear();
                        alreadyFinishItemAdapter.addItems((List)mMyOrderInfo.getAllItems());
                        alreadyFinishItemAdapter.notifyDataSetChanged();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
        //获取显示退款订单
        else if (MY_INDENT_AFTERSERVICE == my_indent){
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mMyApplyOrderInfo, new AbsOnRequestListener(mContext){
                @Override
                public void onRequestSuccess(int result, JSONObject jsonObject) {
                    super.onRequestSuccess(result, jsonObject);
                    try{
                        afterServiceItemAdapter.clear();
                        if (mMyApplyOrderInfo.getAllItems().size() == 0){
                            CustomToast.showToast(mContext, "当前没有已完成订单");
                            nonPaymentLV.setVisibility(View.GONE);
                            stayReceiptLV.setVisibility(View.GONE);
                            alreadyFinishLV.setVisibility(View.GONE);
                            afterServiceLV.setVisibility(View.GONE);
                            nodataRL.setVisibility(View.VISIBLE);
                            return;
                        }
                        afterServiceItemAdapter.clear();
                        afterServiceItemAdapter.addItems((List)mMyApplyOrderInfo.getAllItems());
                        afterServiceItemAdapter.notifyDataSetChanged();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * 获取我的订单详情缓存中
     * @param intentId
     */
    private void getMyIndentDetail(int intentId){
        //判断缓存是否存在
        DataCache dataCache = DataCache.get(mContext);
        String key = "myIndentDetail" + intentId;
        JSONObject jsonObject = dataCache.getAsJSONObject(key);
        if(jsonObject != null){
            try {
                mMyIndentItem = BaseInfo.gson.fromJson(jsonObject.get("data").toString(), MyIndentItem.class);
                String orderType = mMyIndentItem.getOrder_type();
                if (("6").equals(orderType) || ("7").equals(orderType)){
                    Intent intentMyIndentDetailRefund = new Intent(mContext, MyIndentRefundDetailActivity_.class);
                    intentMyIndentDetailRefund.putExtra("myIndentItem", mMyIndentItem);
                    intentMyIndentDetailRefund.putExtra("orid", orid);
                    startActivity(intentMyIndentDetailRefund);
                }else {
                    Intent intentMyIndentDetail = new Intent(mContext, MyIndentNoPayDetailActivity_.class);
                    intentMyIndentDetail.putExtra("myIndentItem", mMyIndentItem);
                    intentMyIndentDetail.putExtra("orid", orid);
                    startActivity(intentMyIndentDetail);
                }
            } catch (Exception e) {
                getMyIndentDetailByHttp(intentId);
                e.printStackTrace();
            }
        }else {
            getMyIndentDetailByHttp(intentId);
        }
    }
    /**
     * 获取我的订单详情网络中
     * @param intentId
     */
    private void getMyIndentDetailByHttp(final int intentId){
        mMyTuanOrderDetailInfo.setId(intentId);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mMyTuanOrderDetailInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    mMyIndentItem = mMyTuanOrderDetailInfo.getMyIndentItem();
                    //缓存数据
                    DataCache aCache = DataCache.get(PandLifeApp.getInstance());
                    String key = "myIndentDetail" + intentId;
                    aCache.put(key,jsonObject, 3600 * 12);

                    String orderType = mMyIndentItem.getOrder_type();
                    if (("6").equals(orderType) || ("7").equals(orderType)){
                        Intent intentMyIndentDetailRefund = new Intent(mContext, MyIndentRefundDetailActivity_.class);
                        intentMyIndentDetailRefund.putExtra("myIndentItem", mMyIndentItem);
                        intentMyIndentDetailRefund.putExtra("orid", orid);
                        startActivity(intentMyIndentDetailRefund);
                    }else {
                        Intent intentMyIndentDetail = new Intent(mContext, MyIndentNoPayDetailActivity_.class);
                        intentMyIndentDetail.putExtra("myIndentItem", mMyIndentItem);
                        intentMyIndentDetail.putExtra("orid", orid);
                        startActivity(intentMyIndentDetail);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }



    /**
     * 响应点击的方法
     */
    private void responseClick(String clickStr, String type){
        //点击站点或商家导航
        if (("navigation").equals(clickStr)){
            final CustomDialog customDialog = Util.getDialog(mContext, "提示", "进入导航吗？", "确定", "取消");
            customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customDialog.dismiss();
                startLocation();
                }
            });
            customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customDialog.dismiss();
                }
            });
            customDialog.show();

        }
        //点击button1
        else if(("button1").equals(clickStr)) {
            //未支付点击button1是去付款
            if (("nonPay").equals(type)) {
                //进入选择支付方式
                Intent intentSelectRechargeWay = new Intent(mContext, SelectPayWayActivity_.class);
                intentSelectRechargeWay.putExtra("rechargeWay", payWay);
                intentSelectRechargeWay.putExtra("paySum", paySum);
                startActivityForResult(intentSelectRechargeWay, Constant.StaticCode.REQUEST_RECHARGE_WAY);
            }else if(("alreadyPay").equals(type)){
                //调用申请退款方法
                mApplyRefundInfo.setOrid(orid);
                mApplyRefundInfo.setOther("");
                ApiManager apiManager = ApiManager.getInstance();
                apiManager.request(mApplyRefundInfo, new AbsOnRequestListener(mContext){
                    @Override
                    public void onRequestSuccess(int result, JSONObject jsonObject) {
                        super.onRequestSuccess(result, jsonObject);
                        try {
                            CustomToast.showToast(mContext, "申请已提交，请等待审核");
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }else if (("alreadyFinish").equals(type)){
                //拨打电话方法
                clickCallPhone(telephone);
            }
        }
        //点击button2
        else if(("button2").equals(clickStr)){
            //未支付点击button2
            if (("nonPay").equals(type)){
                //调用取消订单方法
                mAbolishOrderInfo.setId(id);
                ApiManager apiManager = ApiManager.getInstance();
                apiManager.request(mAbolishOrderInfo, new AbsOnRequestListener(mContext){
                    @Override
                    public void onRequestSuccess(int result, JSONObject jsonObject) {
                        super.onRequestSuccess(result, jsonObject);
                        try {
                            CustomToast.showToast(mContext, "订单取消成功");
                            bindInfo(MY_INDENT_NONPAYMENT);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }else if(("alreadyPay").equals(type)){

            }else if (("alreadyFinish").equals(type)){

            }
        }
        //点击button3
        else if(("button3").equals(clickStr)){
            //未支付点击button3
            if (("nonPay").equals(type)){
            }else if(("alreadyPay").equals(type)){
            }else if (("alreadyFinish").equals(type)){
                //删除订单方法
            }
        }
        //点击了商品列表
        else if (("item").equals(clickStr)){
            getMyIndentDetail(indentId);
        }
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }

    /**
     * 拨打电话方法
     */
    private void clickCallPhone(String telephoneStr){
        Intent intentCall = new Intent();
        intentCall.setAction(Intent.ACTION_CALL);
        intentCall.setData(Uri.parse("tel:" + telephoneStr));
        startActivity(intentCall);
    }

    /**
     * 进入导航的方法
     */
    private void toNavigation(double locationLongitude, double locationLatitude){
//        Intent intentNavigation = new Intent(this, BasicNaviActivity.class);
//
//        intentNavigation.putExtra(BasicNaviActivity.END_LONGITUDE, Double.parseDouble(longitude));
//        intentNavigation.putExtra(BasicNaviActivity.END_LATITUDE, Double.parseDouble(latitude));
//        intentNavigation.putExtra(BasicNaviActivity.START_LONGITUDE, locationLongitude);
//        intentNavigation.putExtra(BasicNaviActivity.START_LATITUDE, locationLatitude);
//        startActivityForResult(intentNavigation, Constant.StaticCode.REQUEST_SENDER_ADDRESS);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        //定位回调
        JKLog.i(TAG, aMapLocation.toString());
        double locationLongitude = aMapLocation.getLongitude();
        double locationLatitude = aMapLocation.getLatitude();
        toNavigation(locationLongitude, locationLatitude);
    }
}
