package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.homepage.SelectPayWayActivity_;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.AbolishOrderInfo;
import com.jiakang.pandlife.info.ApplyRefundInfo;
import com.jiakang.pandlife.item.GoodsItem;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

/**
 * 我的待付款订单详情
 * Created by Administrator on 2016/1/21.
 */
@EActivity(R.layout.activity_my_indent_detail)
public class MyIndentNoPayDetailActivity extends BaseActy{
    protected static final String TAG = "MyIndentNoPayDetailActivity";

    @ViewById(R.id.it_ibn_right)
    ImageButton rigthIBN;
    //头部两个条目的
    @ViewById(R.id.amid_rl_two)
    RelativeLayout twoRL;
    @ViewById(R.id.amid_iv_two_one_logo)
    ImageView twoOneLogoIV;
    @ViewById(R.id.amid_tv_two_one_content)
    TextView twoOneContentTV;
    @ViewById(R.id.amid_tv_two_one_date)
    TextView twoOneDateTV;
    @ViewById(R.id.amid_iv_two_two_logo)
    ImageView twoTwoLogoIV;
    @ViewById(R.id.amid_tv_two_two_content)
    TextView twoTwoContentTV;
    @ViewById(R.id.amid_tv_two_two_date)
    TextView twoTwoDateTV;
    //头部三个条目的
    @ViewById(R.id.amid_rl_three)
    RelativeLayout threeRL;
    @ViewById(R.id.amid_iv_three_one_logo)
    ImageView threeOneLogoIV;
    @ViewById(R.id.amid_tv_three_one_content)
    TextView threeOneContentTV;
    @ViewById(R.id.amid_tv_three_one_date)
    TextView threeOneDateTV;
    @ViewById(R.id.amid_iv_three_two_logo)
    ImageView threeTwoLogoIV;
    @ViewById(R.id.amid_tv_three_two_content)
    TextView threeTwoContentTV;
    @ViewById(R.id.amid_tv_three_two_date)
    TextView threeTwoDateTV;
    @ViewById(R.id.amid_iv_three_three_logo)
    ImageView threeThreeLogoIV;
    @ViewById(R.id.amid_tv_three_three_content)
    TextView threeThreeContentTV;
    @ViewById(R.id.amid_tv_three_three_date)
    TextView threeThreeDateTV;
    //中间详情部分
    @ViewById(R.id.iidc_tv_site_name)
    TextView siteNameTV;
    @ViewById(R.id.iidc_iv_call_telephone)
    ImageView callTelephoneIV;
    @ViewById(R.id.iidc_clv_goods)
    ListView listLV;
    @ViewById(R.id.iidc_tv_price_content)
    TextView priceTV;
    @ViewById(R.id.iidc_tv_freight_content)
    TextView freightTV;
    @ViewById(R.id.iidc_tv_red_package_content)
    TextView redPackageTV;
    @ViewById(R.id.iidc_tv_real_pay_content)
    TextView realPayTV;
    @ViewById(R.id.iidc_tv_indnet_number_content)
    TextView indnetNumberTV;
    @ViewById(R.id.iidc_tv_indnet_date_content)
    TextView indnetDateTV;
    @ViewById(R.id.iidc_tv_pay_way_content)
    TextView payWayTV;
    @ViewById(R.id.iidc_tv_receiver_name_content)
    TextView receiverNameTV;
    @ViewById(R.id.iidc_tv_receiver_phone_content)
    TextView receiverPhoneTV;
    @ViewById(R.id.iidc_tv_receiver_address_content)
    TextView receiverAddressTV;
    //底部的按钮
    @ViewById(R.id.amid_btn_button1)
    Button button1BN;
    @ViewById(R.id.amid_btn_button2)
    Button button2BN;
    @ViewById(R.id.amid_btn_button3)
    Button button3BN;

    //我的订单实体
    private MyIndentItem mMyIndentItem;
    //订单号
    private String orid;
    /** 我的订单取消接口 */
    private AbolishOrderInfo mAbolishOrderInfo = new AbolishOrderInfo();
    /** 申请退款接口 */
    private ApplyRefundInfo mApplyRefundInfo = new ApplyRefundInfo();

    private ItemAdapter mItemAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.amid_tb_title, "订单详情");
        rigthIBN.setImageDrawable(getResources().getDrawable(R.mipmap.icon_iv_call_telephone));
        rigthIBN.setOnClickListener(this);
        callTelephoneIV.setOnClickListener(this);
        button1BN.setOnClickListener(this);
        button2BN.setOnClickListener(this);
        button3BN.setOnClickListener(this);
        mItemAdapter = new ItemAdapter(mContext);
        listLV.setAdapter(mItemAdapter);

        initView();
    }

    private void initView(){
        Intent intent = getIntent();
        mMyIndentItem = (MyIndentItem)intent.getExtras().get("myIndentItem");
        orid = intent.getExtras().getString("orid");
        GoodsItem goodsItem = new GoodsItem();
        goodsItem.setName(mMyIndentItem.getGoodtitle());
        goodsItem.setContent(mMyIndentItem.getRemarks());
        goodsItem.setNumber(mMyIndentItem.getNumber());
        goodsItem.setPrice(mMyIndentItem.getDanjia());
        goodsItem.setPic(mMyIndentItem.getGpic());
        mItemAdapter.add(goodsItem);
        mItemAdapter.notifyDataSetChanged();

        int count = mMyIndentItem.getDanjia() * mMyIndentItem.getNumber();
        int freight = mMyIndentItem.getPs_money();
        int redPackets = mMyIndentItem.getRed_packets();
        int realPay = count + freight - redPackets;
        priceTV.setText("￥" + count + ".0");
        freightTV.setText("￥" + freight + ".0");
        redPackageTV.setText("-￥" + redPackets + ".0");
        realPayTV.setText("￥" + realPay + ".0");

        String indentDateStr = mMyIndentItem.getCreatetime();
        long indentDate = 0;
        if (!TextUtils.isEmpty(indentDateStr)){
            try{
                indentDate = Long.parseLong(indentDateStr) * 1000;
            }catch (Exception e){
            }
        }
        String payKind = mMyIndentItem.getPay_kind();
//        String payWay = "未支付";
//
//        if (("account").equals(payKind)){
//            payWay = "账户余额支付";
//        }else if (("Alipay").equals(payKind)){
//            payWay = "支付宝支付";
//        }else if (("Wx").equals(payKind)){
//            payWay = "微信支付";
//        }else if (("UnionPay").equals(payKind)){
//            payWay = "银联支付";
//        }

        indnetNumberTV.setText(orid);
        indnetDateTV.setText(Util.getFormatedDateTime("yyyy-MM-dd HH:mm:ss", indentDate));
        payWayTV.setText(payKind);
        receiverNameTV.setText(mMyIndentItem.getShname());
        receiverPhoneTV.setText(mMyIndentItem.getShphone());
        receiverAddressTV.setText(mMyIndentItem.getShaddress());

        String orderType = mMyIndentItem.getOrder_type();
        //待付款
        if (("1").equals(orderType)){
            twoRL.setVisibility(View.VISIBLE);
            threeRL.setVisibility(View.GONE);

            twoOneContentTV.setText("下单成功");
            twoTwoContentTV.setText("待付款");

            button1BN.setText("去付款");
            button1BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg_reddish_orange));
            button1BN.setTextColor(getResources().getColor(R.color.reddish_orange));

            button2BN.setText("取消订单");
            button2BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
            button2BN.setTextColor(getResources().getColor(R.color.gray_content));

            button3BN.setVisibility(View.GONE);

        }
        //已付款待发货
        else if (("2").equals(orderType)){
            threeRL.setVisibility(View.VISIBLE);
            twoRL.setVisibility(View.GONE);

            threeOneContentTV.setText("下单成功");
            threeTwoContentTV.setText("已付款");
            threeThreeContentTV.setText("待成团");

            button1BN.setText("申请退款");
            button1BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg_reddish_orange));
            button1BN.setTextColor(getResources().getColor(R.color.reddish_orange));

            button2BN.setText("提醒发货");
            button2BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
            button2BN.setTextColor(getResources().getColor(R.color.gray_content));

            button3BN.setVisibility(View.GONE);

        }
        //已发货，待收货
        else if (("3").equals(orderType)){
            threeRL.setVisibility(View.VISIBLE);
            twoRL.setVisibility(View.GONE);

            threeOneContentTV.setText("下单成功");
            threeTwoContentTV.setText("已成团");
            threeThreeContentTV.setText("待取货");

            button1BN.setText("确认收货");
            button1BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg_reddish_orange));
            button1BN.setTextColor(getResources().getColor(R.color.reddish_orange));

            button2BN.setText("物流追踪");
            button2BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
            button2BN.setTextColor(getResources().getColor(R.color.gray_content));

            button3BN.setVisibility(View.GONE);

        }
        //已完成
        else if (("4").equals(orderType)){
            threeRL.setVisibility(View.VISIBLE);
            twoRL.setVisibility(View.GONE);

            threeOneContentTV.setText("下单成功");
            threeTwoContentTV.setText("已成团");
            threeThreeContentTV.setText("已取货");

            button1BN.setText("售后电话");
            button1BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg_reddish_orange));
            button1BN.setTextColor(getResources().getColor(R.color.reddish_orange));

            button2BN.setText("物流追踪");
            button2BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
            button2BN.setTextColor(getResources().getColor(R.color.gray_content));

            button3BN.setVisibility(View.VISIBLE);
            button3BN.setText("删除订单");
            button3BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
            button3BN.setTextColor(getResources().getColor(R.color.gray_content));
        }
    }

    /**
     * 点击不同的button不同的方法
     */
    private void clickButton(String buttonStr){
        if(("去付款").equals(buttonStr)){
            //进入选择支付方式
            Intent intentSelectRechargeWay = new Intent(mContext, SelectPayWayActivity_.class);
            intentSelectRechargeWay.putExtra("rechargeWay", mMyIndentItem.getPay_kind());
            intentSelectRechargeWay.putExtra("paySum", mMyIndentItem.getPay_money());
            startActivityForResult(intentSelectRechargeWay, Constant.StaticCode.REQUEST_RECHARGE_WAY);
        }else if(("取消订单").equals(buttonStr)){
            //调用取消订单方法
            mAbolishOrderInfo.setId(mMyIndentItem.getId());
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mAbolishOrderInfo, new AbsOnRequestListener(mContext){
                @Override
                public void onRequestSuccess(int result, JSONObject jsonObject) {
                    super.onRequestSuccess(result, jsonObject);
                    try {
                        CustomToast.showToast(mContext, "订单取消成功");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }else if(("申请退款").equals(buttonStr)){
            //调用申请退款方法
            mApplyRefundInfo.setOrid(orid);
            mApplyRefundInfo.setOther("");
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mApplyRefundInfo, new AbsOnRequestListener(mContext){
                @Override
                public void onRequestSuccess(int result, JSONObject jsonObject) {
                    super.onRequestSuccess(result, jsonObject);
                    try {
                        CustomToast.showToast(mContext, "申请已提交，请等待审核");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }else if(("售后电话").equals(buttonStr)){
            //拨打电话方法
            clickCallPhone(mMyIndentItem.getStationtel());
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.iidc_iv_call_telephone:
                String telephone = mMyIndentItem.getShphone();
                clickCallPhone(telephone);
                break;
            case R.id.amid_btn_button1:
                String buttonStr1 = button1BN.getText().toString();
                clickButton(buttonStr1);
                break;
            case R.id.amid_btn_button2:
                String buttonStr2 = button2BN.getText().toString();
                clickButton(buttonStr2);
                break;
            case R.id.amid_btn_button3:
                String buttonStr3 = button3BN.getText().toString();
                clickButton(buttonStr3);
                break;
        }
    }


    /**
     * 拨打电话方法
     */
    private void clickCallPhone(String telephoneStr){
        Intent intentCall = new Intent();
        intentCall.setAction(Intent.ACTION_CALL);
        intentCall.setData(Uri.parse("tel:" + telephoneStr));
        startActivity(intentCall);
    }
}
