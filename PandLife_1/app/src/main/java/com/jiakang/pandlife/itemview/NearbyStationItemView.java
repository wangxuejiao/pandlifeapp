package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.media.Image;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.info.SiteInfo;
import com.jiakang.pandlife.item.ConveniencePhoneItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.NearbyStationItem;
import com.jiakang.pandlife.item.SiteItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

import java.util.List;

/**
 * Created by play on 2016/1/6.
 */
public class NearbyStationItemView extends AbsLinearLayout {

    private TextView tvStationName;
    private TextView tvAddress;
    private TextView tvPhone;
    private ImageView imageView;

    public NearbyStationItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NearbyStationItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        tvStationName = (TextView) findViewById(R.id.tv_station_nearby_station);
        tvAddress = (TextView) findViewById(R.id.tv_address_nearby_station);
        tvPhone = (TextView) findViewById(R.id.tv_phone_nearby_station);
        imageView = (ImageView) findViewById(R.id.iv_nearby_station);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        if(item instanceof NearbyStationItem.DataEntity) {
            NearbyStationItem.DataEntity dataEntity = (NearbyStationItem.DataEntity) item;
            tvStationName.setText(dataEntity.getNice_name());
            tvAddress.setText(dataEntity.getAddress());
            tvPhone.setText(dataEntity.getPhone());
            ImageLoaderUtil.displayImage(dataEntity.getPic(), imageView);

        }else if (item instanceof SiteItem){
            SiteItem siteItem = (SiteItem) item;
            tvStationName.setText(siteItem.getNice_name());
            tvAddress.setText(siteItem.getAddress());
            tvPhone.setText(siteItem.getMobiphone());
            ImageLoaderUtil.displayImage(siteItem.getPic(), imageView);
        }
    }
}
