package com.jiakang.pandlife.itemview;


import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;

public interface ItemView {

	/**
     */
	void findViewsByIds();

	/**
     */
	void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener);
	
	

}
