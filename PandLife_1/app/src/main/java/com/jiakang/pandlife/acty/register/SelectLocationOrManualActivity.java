package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.CommunityInfo;
import com.jiakang.pandlife.info.NearCommunityInfo;
import com.jiakang.pandlife.item.CommunityItem;
import com.jiakang.pandlife.item.RedPackageItem;
import com.jiakang.pandlife.item.SiteItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 通过定位或者手动选择绑定小区和站点
 */
@EActivity(R.layout.activity_select_location_or_manual)
public class SelectLocationOrManualActivity extends BaseActy {
    protected static final String TAG = "SelectLocationOrManualActivity";

    @ViewById(R.id.aslom_rl_default)
    RelativeLayout defaultRL;
    @ViewById(R.id.aslom_tv_default)
    TextView defaultTV;
    @ViewById(R.id.aslom_tv_location)
    TextView locationTV;
    @ViewById(R.id.aslom_tv_manual)
    TextView manualTV;

//    private AMapLocationClient locationClient = null;
//    private AMapLocationClientOption locationOption = null;

    /** 获取附近小区接口 */
    private NearCommunityInfo mNearCommunityInfo = new NearCommunityInfo();

    //记录经度
    private double longitude = 0;
    //记录纬度
    private double latitude = 0;

    //选择站点或者小区
    public static final String SITE_OR_COMMUNITY = "site_or_community";

    private String siteOrCommunity = "community_and_site";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.aslom_tb_title, "选择方式");

        Intent intent = getIntent();
        siteOrCommunity = intent.getExtras().getString(SelectLocationOrManualActivity.SITE_OR_COMMUNITY);
        if (("community").equals(siteOrCommunity)){
            CommunityItem communityItem = (CommunityItem)intent.getExtras().get("communityItem");
            if (communityItem != null) {
                defaultRL.setVisibility(View.VISIBLE);
                defaultTV.setText("当前小区：" + communityItem.getName());
            }
        }else if(("site").equals(siteOrCommunity)){
            SiteItem siteItem = (SiteItem)intent.getExtras().get("siteItem");
            if (siteItem != null){
                defaultRL.setVisibility(View.VISIBLE);
                defaultTV.setText("当前站点：" + siteItem.getNice_name());
            }
        }
        bindView();

    }

    private void bindView(){
        locationTV.setOnClickListener(this);
        manualTV.setOnClickListener(this);
    }

//    /**
//     *初始化定位对象且开始定位
//     */
//    public void startLocationForOne(){
//        //初始化定位对象
//        locationClient = new AMapLocationClient(this.getApplicationContext());
//        locationOption = new AMapLocationClientOption();
//        // 设置定位模式为低功耗模式
//        locationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
//        // 设置定位监听
//        locationClient.setLocationListener(this);
//        // 设置为单次定位
//        locationOption.setOnceLocation(true);
//        locationOption.setNeedAddress(true);
//        // 设置定位参数
//        locationClient.setLocationOption(locationOption);
//        // 启动定位
//        locationClient.startLocation();
//    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        //定位回调
        JKLog.i(TAG, aMapLocation.toString());
        longitude = aMapLocation.getLongitude();
        latitude = aMapLocation.getLatitude();
        locationGetCommunity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUSET_BIND_SITE) && (resultCode == RESULT_OK)){
            if (("community").equals(siteOrCommunity)){
                setResult(RESULT_FIRST_USER);
            }else {
                setResult(RESULT_OK);
            }
            finish();
        }
    }

    /**
     * 定位选择小区方法
     * 进行定位选择，如果定位成功就获取小区信息，小区信息不为空就跳转小区选择
     */
    private void locationGetCommunity(){
        if ((longitude == 0) || (latitude == 0)){
            CustomToast.showToast(mContext, "定位失败，请手动选择");
            return;
        }

        mNearCommunityInfo.setLatitude(latitude + "");
        mNearCommunityInfo.setLongitude(longitude + "");
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mNearCommunityInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try{
                    if (mNearCommunityInfo.getAllItems().size() != 0){
                        Intent intentCommunity = new Intent(mContext, SelectCommunityActivity_.class);
                        Bundle bundle=new Bundle();
                        bundle.putParcelableArrayList("list", (ArrayList)mNearCommunityInfo.getAllItems());
                        intentCommunity.putExtras(bundle);
                        intentCommunity.putExtra(SelectLocationOrManualActivity.SITE_OR_COMMUNITY, siteOrCommunity);
                        startActivityForResult(intentCommunity, Constant.StaticCode.REQUSET_BIND_SITE);
                    }else{
                        CustomToast.showToast(mContext, "附近没有推广的小区,请手动选择");
                        return;
                    }
                }catch (Exception e){
                    JKLog.e(TAG, "异常------------->" + e);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.aslom_tv_location://点击进行定位选择，如果定位成功就获取小区信息，小区信息不为空就跳转小区选择
                startLocation();
                break;
            case R.id.aslom_tv_manual:
                Intent intentSelectProvince = new Intent(mContext, SelectProvinceActivity_.class);
                intentSelectProvince.putExtra(SelectLocationOrManualActivity.SITE_OR_COMMUNITY, siteOrCommunity);
                startActivityForResult(intentSelectProvince, Constant.StaticCode.REQUSET_BIND_SITE);
                break;
        }
    }
}
