package com.jiakang.pandlife.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.nfc.Tag;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.acty.homepage.TomorrowAdvanceActivity;
import com.jiakang.pandlife.db.FileCacheHelper;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class FileCache {


    private final String TAG = "FileCache";
    // public SharedPreferences imgCacheMapSpf = null;
    private File cacheDir;
    private static FileCache instance;
    public final String cachePath = "/mnt/sdcard/yilinke/cache/";
    // 机型适配
    public final String cachePath2 = "/sdcard/yilinke/cache/";
    public final String imgCacheDir = "imgCacheDir/";
    private FileCacheHelper fileCacheHelper;

    public static FileCache getInstance() {
        return getInstance(PandLifeApp.getInstance());
    }

    public static FileCache getInstance(Context context) {
        if (null == instance)
            instance = new FileCache(context);
        return instance;
    }

    /**
     * 优先存放SD卡揯目录 其次系统默认缓存
     *
     * @param context
     */
    private FileCache(Context context) {
        if (hasSDCard()) {
            cacheDir = createFilePath(Environment.getExternalStorageDirectory() + "/" + imgCacheDir);
            if (!cacheDir.exists()) {
                cacheDir = createFilePath(cachePath2 + imgCacheDir);
            }
        } else {
            cacheDir = createFilePath(context.getCacheDir() + imgCacheDir);
        }
        fileCacheHelper = new FileCacheHelper(context);
    }

    /**
     * 初始化检查SD卡
     */
    public boolean hasSDCard() {
        return android.os.Environment.getExternalStorageState()
                .equals(android.os.Environment.MEDIA_MOUNTED);
    }

    /**
     * 初始化路径
     *
     * @param filePath
     * @return boolean
     */
    private File createFilePath(String filePath) {

        return createFilePath(new File(filePath));
    }

    private File createFilePath(File filejia) {
        if (!filejia.exists()) {
            filejia.mkdirs();// 按照文件夹路径创建文件夹
        }
        return filejia;
    }

    /**
     * 保存Bitmap到SdCard，返回本地文件路径，不插入记录到数据库
     *
     * @param bitmap
     * @return String
     */
    public String addBitmapToSdCard(InputStream is) {
        if (is == null) {
            return null;
        }
        Bitmap bitmap = BitmapFactory.decodeStream(is);
        return addBitmapToSdCard(bitmap);
    }

    /**
     * 保存Bitmap到SdCard，返回本地文件路径，不插入记录到数据库
     *
     * @param bitmap
     * @return String
     */
    public String addBitmapToSdCard(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }

        try {
            String fileName = System.currentTimeMillis() + ".zy";
            // 创建文件夹
            createFilePath(cacheDir);
            File bitmapFile = new File(cacheDir, fileName);
            bitmapFile.createNewFile();

            JKLog.e(TAG, "FileCache中：bitmap:Width为：" + bitmap.getWidth());
            JKLog.e(TAG, "FileCache中：bitmap:Height为：" + bitmap.getHeight());
            JKLog.e(TAG, "FileCache中：bitmap:ByteCount为：" + bitmap.getByteCount());

            // float width = bitmap.getWidth()/1632.0f;
            // float height = bitmap.getHeight()/2176.0f;
            // int zoom = (int)(100.0/(width > height? height:width));
            // if (zoom > 100) {
            // zoom = 100;
            // }
            // if (zoom < 50) {
            // zoom = 50;
            // }
            // XYLog.e(TAG, "addBitmapToSdCard方法中-------->：zoom为：" + zoom);

            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(bitmapFile));
            bitmap.compress(CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();

            String filePath = bitmapFile.getAbsolutePath();
            return filePath;
        } catch (IOException e) {
            JKLog.e("", "FileCache中：保存图片错误-------为：" + e);
        }
        return null;
    }

    /**
     * @param url
     * @param bitmap
     * @return String
     */
    public String addBitmapCache(String url, Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }

        // 文件已经存在
        String path = getPathByUrl(url);
        if (null != path) {
            File file = new File(path);
            if (file.exists())
                return path;
        }

        try {
            String fileName = System.currentTimeMillis() + ".zy";
            // 创建文件夹
            createFilePath(cacheDir);
            File bitmapFile = new File(cacheDir, fileName);
            bitmapFile.createNewFile();
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(bitmapFile));
            bitmap.compress(CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();

            String filePath = bitmapFile.getAbsolutePath();
            // 插入数据库
            insert(url, filePath);
            return filePath;
        } catch (IOException e) {
            JKLog.e("", "FileCache中：保存图片错误-------为：" + e);
        }
        return null;
    }

    /**
     * 添加文件缓存 返回文件路径
     *
     * @param url 网络路径
     * @return String 文件路径
     */
    public String addBitmapCache(String url) {
        InputStream inputStream = getInputSteamFromNet(url);
        return addBitmapCache(url, inputStream);
    }

    /**
     * 添加文件缓存 返回文件路径
     *
     * @param url 网络路径
     * @return String 文件路径
     */
    public String addBitmapCache(String url, InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }

        // 文件已经存在
        String path = getPathByUrl(url);
        if (null != path) {
            File file = new File(path);
            if (file.exists())
                return path;
        }

        // 系统时间作为图片名称
        String fileName = System.currentTimeMillis() + ".zy";
        // 创建文件夹
        createFilePath(cacheDir);
        File createFile = new File(cacheDir, fileName);
        if (!createFile.exists())
            try {
                createFile.createNewFile();
                byte[] data = convertInputStream(inputStream);
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                bitmap.compress(CompressFormat.JPEG, 100, new FileOutputStream(createFile));
                // 让内存缓存管理bitmap的回收
                MemoryCache.getInstance().addBitmapToCache(url, bitmap);
                String filePath = createFile.getAbsolutePath();
                // 插入数据库
                insert(url, filePath);
                return filePath;

            } catch (Exception e) {
                JKLog.e(TAG, " create fileIOException ----------------->" + e);
            }
        return null;
    }

    /**
     * 添加文件缓存 返回文件路径
     *
     * @param url 网络路径
     * @return String 文件路径
     */
    public synchronized String addFileCache(String url, InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }

        // 文件已经存在
        String path = getPathByUrl(url);
        if (null != path) {
            File file = new File(path);
            if (file.exists())
                return path;
        }
        // 文件不存在
        OutputStream outputStream = null;
        try {
            // 系统时间作为图片名称
            String fileName = System.currentTimeMillis() + ".png";
            File createFile = new File(cacheDir, fileName);
            if (!createFile.exists())
                createFile.createNewFile();
            outputStream = new FileOutputStream(createFile);
            int data = inputStream.read();
            while (data != -1) {
                outputStream.write(data);
                data = inputStream.read();
            }
            inputStream.close();
            outputStream.close();

            String filePath = createFile.getAbsolutePath();
            // 插入数据库
            insert(url, filePath);
            return filePath;
        } catch (Exception e) {
            JKLog.e(TAG, " write to file filed----------------->" + e);
        } finally {
            if (outputStream != null) {
                try {
                    inputStream.close();
                    outputStream.close();
                } catch (IOException e) {
                    JKLog.e(TAG, " close stream filed----------------->" + e);
                }
            }
        }
        return null;
    }

    /**
     * 其实在android里面，不建议直接把网络图片原样写人sd里面，毕竟手机的sd空间是有限的。 最常用的方法是把网络图片压缩成jpg格式保存：
     *
     * @param inStream
     * @return byte[]
     * @throws Exception
     */
    private byte[] convertInputStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();
        return outSteam.toByteArray();
    }

    /**
     * 从网络获取InputStream
     *
     * @param urlStr
     * @return InputStream
     * @throws MalformedURLException
     * @throws IOException
     */
    public InputStream getInputSteamFromNet(String urlStr) {
        try {
            // XYLog.i(TAG, "getInputSteamFromUrl方法中-------->：urlStr为：" +
            // urlStr);
            URL url = null;
            url = new URL(urlStr);
            HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
            InputStream inputStream = urlConn.getInputStream();
            return inputStream;
        } catch (Exception e) {
            JKLog.e(TAG, "getInputSteamFromNet方法中-------->：e为：" + e);
            return null;
        }
    }

    /**
     * 查询url对应的本地path，如果本地文件存在返回path，否则返回null
     *
     * @param url 网络文件url
     * @return String
     */
    public String getCacheFilePath(String url) {
        String fileName = getPathByUrl(url);
        if (fileName != null) {
            File file = new File(fileName);
            if (file.exists()) {
                return fileName;
            } else {
                // 删除记录
                delete(url);
                return null;
            }
        } else {
            // 删除记录
            delete(url);
            return null;
        }
    }

    /**
     * 根据url获取本地bitmap
     *
     * @param url
     * @param height
     * @param width
     * @param crop
     * @return Bitmap
     */
    public Bitmap getcacheBitmap(String url, int height, int width, boolean crop) {
        String path = getCacheFilePath(url);
        if (path == null) {
            return null;
        }
        return BitmapUtil.decodePathAsBitmap(path, height, width, crop);
    }

    /**
     * 查询url对应的本地path，如果本地文件存在返回path，否则返回null
     *
     * @param url 网络文件url
     * @return String
     */
    public File getCacheFile(String url) {
        String fileName = getPathByUrl(url);
        if (fileName != null) {
            File file = new File(fileName);
            if (file.exists()) {
                return file;
            } else {
                // 删除记录
                delete(url);
                return null;
            }
        } else {
            // 删除记录
            delete(url);
            return null;
        }
    }

    /**
     * 清理缓存 缓存目录：cacheDir
     */
    public void clearCache() {
        File[] files = cacheDir.listFiles();
        if (files == null)
            return;
        for (File f : files) {
            if (null != f) {
                f.delete();
            }
        }
    }

    public File getCacheDir() {
        return cacheDir;
    }

    /**
     * 插入数据库中含有http头部
     *
     * @param url  含有http或不含http都行
     * @param path
     * @return void
     */
    public void insert(String url, String path) {
        if (!url.contains("http")) {
            url = Constant.downalImageUrl + url;
        }

        String sql = "insert or replace into " + FileCacheHelper.TAB_FILES + " ([url], [filePath]) values ('" + url + "', '" + path + "')";
        doSql(sql);
    }

    public void delete(String url) {
        String sql = "delete from " + FileCacheHelper.TAB_FILES + " where url = '" + url + "'";
        doSql(sql);
    }

    /**
     * @param url
     * @return String 没有返回null
     */
    private String getPathByUrl(String url) {
        if (TextUtils.isEmpty(url)) {
            return null;
        }

        if (!url.contains("http")) {
            url = Constant.downalImageUrl + url;
        }
        String result = null;
        String sql = "select * from " + FileCacheHelper.TAB_FILES + " where url = '" + url + "'";
        Cursor cursor = fileCacheHelper.getReadableDatabase().rawQuery(sql, null);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                result = cursor.getString(cursor.getColumnIndex("filePath"));
            }
        }
        cursor.close();
        return result;
    }

    private void doSql(String sql) {
        fileCacheHelper.getWritableDatabase().execSQL(sql);
    }


    /**
     * 将Asset中的图片写入sdcard
     * @param context
     * @param fileName
     * @return
     *//*
    public static Bitmap getImageFromAssetsFile(Context context, String fileName) {
        //获取应用的包名
        String packageName = context.getPackageName();
        //定义存放这些图片的内存路径
        String path="/data/data/"+packageName;
        //如果这个路径不存在则新建
        File file = new File(path);
        Bitmap image = null;
        boolean isExist = file.exists();
        if(!isExist){
            file.mkdirs();
        }
        //获取assets下的资源
        AssetManager am = context.getAssets();
        try {
            //图片放在img文件夹下
            InputStream is = am.open("img/"+fileName);
            image = BitmapFactory.decodeStream(is);
            FileOutputStream out = new FileOutputStream(path+"/"+fileName);
            //这个方法非常赞
            image.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }*/

    public void writeAssetsImageToSdcard(Context context)
    {
        InputStream in = null;
        FileOutputStream fos = null;
        try
        {
            in = context.getAssets().open("img/share_image.png");
            //获取应用的包名
            String packageName = context.getPackageName();
            //定义存放这些图片的内存路径
            String path="/data/data/"+packageName + "/share_image.png";
            File f = new File(path);
            if (!f.exists())
            {
                f.createNewFile();
            }
            fos = new FileOutputStream(f);
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = in.read(buffer)) > 0)
            {
                fos.write(buffer);
                fos.flush();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (fos != null)
            {
                try
                {
                    fos.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            if (in != null)
            {
                try
                {
                    in.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}