package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.CityItem;
import com.jiakang.pandlife.item.SendBagItem;
import com.jiakang.pandlife.utils.JKLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 15.寄件信息列表
 *
 * @author ww
 *
 */
public class SendBagListInfo extends BaseAbsInfo {

    private static final String TAG = "SendBagListInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<SendBagItem> allItems = new ArrayList<SendBagItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Express&a=Sendbaglist" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
//            json.put("token", "ff0a6c74e7b4d08b659f9e2737a4ac5c");
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();

                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                SendBagItem sendBagItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    sendBagItem = BaseInfo.gson.fromJson(itemStr, SendBagItem.class);
                    sendBagItem.insert();
                    allItems.add(sendBagItem);
                }
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<SendBagItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<SendBagItem> allItems) {
        this.allItems = allItems;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
