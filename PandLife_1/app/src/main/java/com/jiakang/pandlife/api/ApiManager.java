package com.jiakang.pandlife.api;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.form.FormFile;
import com.jiakang.pandlife.form.FormImage;
import com.jiakang.pandlife.info.BaseInterfaceInfo;
import com.jiakang.pandlife.utils.BitmapCache;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.utils.ProgressDialogUtil;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.utils.VolleyUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2015/12/8.
 */
public class ApiManager {

    protected static final String TAG = "ApiManager";

    private static ApiManager apiManager;

    public static boolean isShowProgressBar = true;

    private Context mContext;

    public static ApiManager getInstance(){
        if (apiManager == null) {
            apiManager = new ApiManager();
        }
        return apiManager;
    }

    // 线程池
    private static final ExecutorService THREAD_WORKER = Executors.newFixedThreadPool(5);

    /**
     * 关闭线程池
     */
    public void shutdown() {
        THREAD_WORKER.shutdown();
    }

    private final int code_success = 0x12;
    private final int code_fail = 0x13;
    private final int code_timeOut = 0x14;
    private RequestQueue requestQueue = BaseActy.getRequestQueue(PandLifeApp.getInstance());
//            Volley.newRequestQueue(PandLifeApp.getInstance());

    private ImageLoader imageLoader;;
    private ImageLoader.ImageListener imageListener;

    private ApiManager() {
    }

//    public void request(final RequestMap requestMap, final AbsOnRequestListener onRequestListener){
//        request(requestMap, 10000, onRequestListener);
//    }



    public void request( final BaseInterfaceInfo baseInterfaceInfo, final AbsOnRequestListener onRequestListener){
//        mContext = context;
        if (baseInterfaceInfo.requestDate() == BaseInfo.DATE_MAX){
            request(baseInterfaceInfo, 10000, onRequestListener);
        }
    }

    /**
     * 通过info接口请求网络
     * @param baseInterfaceInfo
     * @param timeOut
     * @param onRequestListener
     */
    public void request(final BaseInterfaceInfo baseInterfaceInfo, final int timeOut, final AbsOnRequestListener onRequestListener){
//        requestQueue = Volley.newRequestQueue(mContext);
        final Handler mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case code_success:
                        onRequestListener.onRequestSuccess(msg.arg1, (JSONObject)msg.obj);
                        break;
                    case code_fail:
                        onRequestListener.onRequestFail(msg.arg1, (JSONObject)msg.obj);
                        break;
                    case code_timeOut:
                        onRequestListener.onRequestTimeOut(msg.arg1, (JSONObject)msg.obj);
                        break;
                    default:
                        break;
                }
                ProgressDialogUtil.stopProgressBar();
            }

        };

        if (isShowProgressBar){
            ProgressDialogUtil.startProgressBar(onRequestListener.getCtx(), "加载中...");
        }
        THREAD_WORKER.execute(new Runnable() {
            public void run() {
//                try {
                if (mHandler != null) {
                    Message msg = Message.obtain();
                    // 兼容设计，超时、失败都发msgWhat
                    msg.what = code_timeOut;
                    msg.arg1 = -1;
                    mHandler.sendMessageDelayed(msg, timeOut);
                }
                StringRequest stringRequest = new StringRequest(Request.Method.POST, baseInterfaceInfo.requestUrl(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                JKLog.i(TAG, "网络获取数据------------->" + response);
//                                    Gson gson = new Gson();
                                try {
                                    JSONObject dataJson = new JSONObject(response);
                                    baseInterfaceInfo.responseData(dataJson);

                                    JKLog.i(TAG, "网络获取数据------------->" + dataJson.toString());
                                    // 请求成功 && baseInterfaceInfo.requestResult().equals(BaseInterfaceInfo.CODE_SUCCESS)
                                    if (1 == dataJson.getInt("status")) {
                                        mHandler.removeMessages(code_timeOut);

                                        Message msg = Message.obtain();
                                        msg.what = code_success;
                                        msg.arg1 = dataJson.getInt("status");
                                        msg.obj = dataJson;
                                        mHandler.sendMessage(msg);
                                    } else {
                                        mHandler.removeMessages(code_timeOut);

                                        Message msg = Message.obtain();
                                        msg.what = code_fail;
                                        msg.arg1 = dataJson.getInt("status");
                                        msg.obj = dataJson;
                                        mHandler.sendMessage(msg);
                                    }

                                } catch (JSONException e) {
                                    JKLog.i(TAG, "请求失败，失败原因------------->" + e);
                                    mHandler.removeMessages(code_timeOut);
                                    Message msg = Message.obtain();
                                    msg.what = code_fail;
                                    msg.arg1 = 1;
                                    msg.obj = new JSONObject();
                                    mHandler.sendMessage(msg);
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        JKLog.i(TAG, "请求失败，失败原因------------->" + error);
                        mHandler.removeMessages(code_timeOut);
                        Message msg = Message.obtain();
                        msg.what = code_fail;
                        msg.arg1 = 1;
                        msg.obj = new JSONObject();
                        mHandler.sendMessage(msg);
                    }
                }) {
                    @Override
//                    protected Map<String, String> getParams() {
                    protected Map<String, String> getParams() {
                        Map<String, String> map = new HashMap<String, String>();
//                        JKLog.i(TAG, "请求参数----------------->" + baseInterfaceInfo.requestParams().toString());
                        JKLog.i(TAG, "-------->sendJson含文件的http请求地址：" + baseInterfaceInfo.requestUrl() + "\n　　请求参数：---> " + baseInterfaceInfo.requestParams().toString());
//                        map.put(baseInterfaceInfo.requestStr(), baseInterfaceInfo.requestParams().toString());
                        //这里将json格式数据转成map格式，直接传表单
                        map = Util.getMapForJson(baseInterfaceInfo.requestParams().toString());
                        return map;
                    }
                };
                requestQueue.add(stringRequest);


//                } catch (Exception e) {
//                    mHandler.removeMessages(code_timeOut);
//                    Message msg = Message.obtain();
//                    msg.what = code_fail;
//                    msg.arg1 = 1;
//                    msg.obj = new JSONObject();
//                    mHandler.sendMessage(msg);
//                }
            }
        });
    }

    /**
     * 通过RequestMap请求网络
     * @param requestMap
     * @param timeOut
     * @param onRequestListener
     */
    public void request(final RequestMap requestMap, final int timeOut, final AbsOnRequestListener onRequestListener){

        final Handler mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case code_success:
                        onRequestListener.onRequestSuccess(msg.arg1, (JSONObject)msg.obj);
                        break;
                    case code_fail:
                        onRequestListener.onRequestFail(msg.arg1, (JSONObject)msg.obj);
                        break;
                    case code_timeOut:
                        onRequestListener.onRequestTimeOut(msg.arg1, (JSONObject)msg.obj);
                        break;
                    default:
                        break;
                }
                ProgressDialogUtil.stopProgressBar();
            }

        };

        ProgressDialogUtil.startProgressBar(onRequestListener.getCtx(), "加载中...");
        THREAD_WORKER.execute(new Runnable() {
            public void run() {
                if (mHandler != null) {
                    Message msg = Message.obtain();
                    // 兼容设计，超时、失败都发msgWhat
                    msg.what = code_timeOut;
                    msg.arg1 = -1;
                    mHandler.sendMessageDelayed(msg, timeOut);
                }
                StringRequest stringRequest = new StringRequest(Request.Method.POST, requestMap.getUrl(),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                JKLog.i(TAG, "网络获取数据------------->" + response);
//                                    Gson gson = new Gson();
                                try {
                                    JSONObject dataJson = new JSONObject(response);
                                    JKLog.i(TAG, "网络获取数据------------->" + dataJson.toString());
                                    // 请求成功 && info.requestResult().equals(BaseInterfaceInfo.CODE_SUCCESS)
                                    if (1 == dataJson.getInt("status")) {
                                        mHandler.removeMessages(code_timeOut);

                                        Message msg = Message.obtain();
                                        msg.what = code_success;
                                        msg.arg1 = dataJson.getInt("status");
                                        msg.obj = dataJson;
                                        mHandler.sendMessage(msg);
                                    }else {
                                        mHandler.removeMessages(code_timeOut);

                                        Message msg = Message.obtain();
                                        msg.what = code_fail;
                                        msg.arg1 = dataJson.getInt("status");
                                        msg.obj = dataJson;
                                        mHandler.sendMessage(msg);
                                    }

                                } catch (JSONException e) {
                                    JKLog.i(TAG, "请求失败，失败原因------------->" + e);
                                    mHandler.removeMessages(code_timeOut);
                                    Message msg = Message.obtain();
                                    msg.what = code_fail;
                                    msg.arg1 = 1;
                                    msg.obj = new JSONObject();
                                    mHandler.sendMessage(msg);
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        JKLog.i(TAG, "请求失败，失败原因------------->" + error);
                        mHandler.removeMessages(code_timeOut);
                        Message msg = Message.obtain();
                        msg.what = code_fail;
                        msg.arg1 = 1;
                        msg.obj = new JSONObject();
                        mHandler.sendMessage(msg);
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> map = new HashMap<String, String>();
                        JKLog.i(TAG, "请求参数----------------->" + requestMap.toString());
                        map.put(requestMap.getRequestStr(), requestMap.toString());
                        return map;
                    }
                };
                requestQueue.add(stringRequest);


            }
        });
    }


    /**
     * 上传图片接口--只有图片
     * @param bitmap 需要上传的图片
     * @param listener 请求回调
     */
    public void uploadImg(Bitmap bitmap,AbsOnRequestListener onRequestListener){

        List<FormImage> imageList = new ArrayList<FormImage>() ;
        imageList.add(new FormImage(bitmap)) ;
        Request request = new PostUploadRequest(Constant.downalImageUrl, imageList, onRequestListener) ;
        VolleyUtil.getRequestQueue().add(request) ;
    }

    /**
     * 上传图片接口--且有实体或其他内容
     * @param bitmap 需要上传的图片
     * @param listener 请求回调
     */
    public void uploadImg(BaseInterfaceInfo baseInterfaceInfo, AbsOnRequestListener onRequestListener){
        try {
            JSONObject jsonObject = baseInterfaceInfo.requestParams();
            JKLog.i(TAG, "-------->sendJson含文件的http请求地址：" + baseInterfaceInfo.requestUrl() + "\n　　请求参数：---> " + baseInterfaceInfo.requestParams().toString());
            Iterator iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                String key = iterator.next().toString();
                Object object = jsonObject.get(key);
                // 参数传输
                if (object instanceof String) {
                    request(baseInterfaceInfo, onRequestListener);
                }
                // 文件传输
                else if (object instanceof File) {
                    List<FormFile> fileList = new ArrayList<FormFile>() ;
                    fileList.add(new FormFile((File) object)) ;
                    Request request = new PostUploadRequest(fileList, Constant.downalImageUrl, onRequestListener) ;
                    VolleyUtil.getRequestQueue().add(request) ;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //获取图片方法
    public void getImage(ImageView view, String url,ImageLoader.ImageListener imageListener){
        imageLoader = new ImageLoader(requestQueue, new BitmapCache());
        imageListener = ImageLoader.getImageListener(view, R.mipmap.public_img_default, R.mipmap.public_img_default);
        imageLoader.get(url, imageListener);
    }

    //获取图片方法、并且指定大小
    public void getImage(ImageView view, String url,ImageLoader.ImageListener imageListener, int width, int height){
        imageLoader = new ImageLoader(requestQueue, new BitmapCache());
        imageListener = ImageLoader.getImageListener(view, R.mipmap.public_img_default, R.mipmap.public_img_default);
        imageLoader.get(url, imageListener , width, height);
    }

    /**
     * 回调接口
     */
    public interface OnRequestListener{
        void onRequestSuccess(int result, JSONObject jsonObject);
        void onRequestFail(int result, JSONObject jsonObject);
        void onRequestTimeOut(int result, JSONObject jsonObject);
    }

}
