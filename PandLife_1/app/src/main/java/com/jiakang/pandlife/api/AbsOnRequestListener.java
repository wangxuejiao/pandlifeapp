package com.jiakang.pandlife.api;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.jiakang.pandlife.utils.CustomToast;

import org.json.JSONObject;


/** 
 * @author：xuwangwang E-mail: 541765907@qq.com
 * @date：2014年7月1日 下午2:56:10
 * @version 1.0   
 * String Tag = "AbsOnRequestListener中：";
 */

/**
 * 抽象类
 */
public class AbsOnRequestListener implements ApiManager.OnRequestListener, Response.ErrorListener ,Response.Listener{

	private Context ctx;

	public AbsOnRequestListener(Context mContext) {
		this.ctx = mContext;
	}

	public Context getCtx() {
		return ctx;
	}

	public void setCtx(Context ctx) {
		this.ctx = ctx;
	}

	@Override
	public void onRequestSuccess(int result, JSONObject jsonObject) {
		// CustomImageToast.showToast(ctx, "请求成功");
	}

	@Override
	public void onRequestFail(int result, JSONObject jsonObject) {
		try {
			String message = jsonObject.getString("info");
			if (TextUtils.isEmpty(message)) {
				message = "请求失败";
			}
			CustomToast.showToast(ctx, message);
		} catch (Exception e) {
		}
	}

	@Override
	public void onRequestTimeOut(int result, JSONObject jsonObject) {
		CustomToast.showToast(ctx, "超时...");
	}

	@Override
	public void onErrorResponse(VolleyError error) {

	}

	@Override
	public void onResponse(Object response) {

	}
}
