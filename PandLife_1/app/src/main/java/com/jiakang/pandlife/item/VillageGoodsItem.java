package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

import java.util.List;

/**
 * Created by play on 2016/1/6.
 */
public class VillageGoodsItem{
    /**
     * data : [{"uid":"13","img":"https://img.alicdn.com/bao/uploaded/i2/2137121108/TB2c5i1hXXXXXaaXpXXXXXXXXXX_!!2137121108.jpg_80x80.jpg","des":"星期天天气不错，大家一起去郊游","replynum":"4","id":"303","time":"1452936215","title":"小区活动","type":"1","userinfo":{"nick":"栋楼","head":"1453534671065.jpg","mobile":"18248792600","mid":"13"},"cid":"1","sid":"1","supportnum":"0"}]
     * page : {"pageIndex":1,"totalPage":0}
     * status : 1
     * info : 拼好货列表
     */
    private List<DataEntityVillageGoods> data;
    private PageEntity page;
    private int status;
    private String info;

    public void setData(List<DataEntityVillageGoods> data) {
        this.data = data;
    }

    public void setPage(PageEntity page) {
        this.page = page;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<DataEntityVillageGoods> getData() {
        return data;
    }

    public PageEntity getPage() {
        return page;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }

    public class DataEntityVillageGoods extends Item{
        /**
         * uid : 13
         * img : https://img.alicdn.com/bao/uploaded/i2/2137121108/TB2c5i1hXXXXXaaXpXXXXXXXXXX_!!2137121108.jpg_80x80.jpg
         * des : 星期天天气不错，大家一起去郊游
         * replynum : 4
         * id : 303
         * time : 1452936215
         * title : 小区活动
         * type : 1
         * userinfo : {"nick":"栋楼","head":"1453534671065.jpg","mobile":"18248792600","mid":"13"}
         * cid : 1
         * sid : 1
         * supportnum : 0
         */
        private String uid;
        private String img;
        private String des;
        private String replynum;
        private String id;
        private String time;
        private String title;
        private String type;
        private UserinfoEntity userinfo;
        private String cid;
        private String sid;
        private String supportnum;

        private int layout = R.layout.item_village_goods;

        @Override
        public int getItemLayoutId() {
            return layout;
        }
        public void setLayout(int layout) {
            this.layout = layout;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public void setDes(String des) {
            this.des = des;
        }

        public void setReplynum(String replynum) {
            this.replynum = replynum;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setUserinfo(UserinfoEntity userinfo) {
            this.userinfo = userinfo;
        }

        public void setCid(String cid) {
            this.cid = cid;
        }

        public void setSid(String sid) {
            this.sid = sid;
        }

        public void setSupportnum(String supportnum) {
            this.supportnum = supportnum;
        }

        public String getUid() {
            return uid;
        }

        public String getImg() {
            return img;
        }

        public String getDes() {
            return des;
        }

        public String getReplynum() {
            return replynum;
        }

        public String getId() {
            return id;
        }

        public String getTime() {
            return time;
        }

        public String getTitle() {
            return title;
        }

        public String getType() {
            return type;
        }

        public UserinfoEntity getUserinfo() {
            return userinfo;
        }

        public String getCid() {
            return cid;
        }

        public String getSid() {
            return sid;
        }

        public String getSupportnum() {
            return supportnum;
        }

        public class UserinfoEntity {
            /**
             * nick : 栋楼
             * head : 1453534671065.jpg
             * mobile : 18248792600
             * mid : 13
             */
            private String nick;
            private String head;
            private String mobile;
            private String mid;

            public void setNick(String nick) {
                this.nick = nick;
            }

            public void setHead(String head) {
                this.head = head;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public void setMid(String mid) {
                this.mid = mid;
            }

            public String getNick() {
                return nick;
            }

            public String getHead() {
                return head;
            }

            public String getMobile() {
                return mobile;
            }

            public String getMid() {
                return mid;
            }
        }
    }

    public class PageEntity {
        /**
         * pageIndex : 1
         * totalPage : 0
         */
        private int pageIndex;
        private int totalPage;

        public void setPageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
        }

        public void setTotalPage(int totalPage) {
            this.totalPage = totalPage;
        }

        public int getPageIndex() {
            return pageIndex;
        }

        public int getTotalPage() {
            return totalPage;
        }
    }

}
