package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.item.ConsumptionDetailItem;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.item.UserItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 7.消费明细列表获取接口
 *
 * @author ww
 *
 */
public class ConsumptionDetailInfo extends BaseAbsInfo {

    private static final String TAG = "ConsumptionDetailInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<ConsumptionDetailItem> allItems = new ArrayList<ConsumptionDetailItem>();

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=User&a=Consumptiondetial" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                ConsumptionDetailItem consumptionDetailItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    consumptionDetailItem = BaseInfo.gson.fromJson(itemStr, ConsumptionDetailItem.class);
                    consumptionDetailItem.insert();
                    allItems.add(consumptionDetailItem);
                }
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<ConsumptionDetailItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<ConsumptionDetailItem> allItems) {
        this.allItems = allItems;
    }
}
