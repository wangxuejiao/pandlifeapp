package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.ConsumptionDetailItem;
import com.jiakang.pandlife.item.ConveniencePhoneItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.Util;

/**
 * Created by play on 2016/1/6.
 */
public class ConsumptionDetailItemView extends AbsRelativeLayout {

    private TextView titleTV;
    private TextView dateTV;
    private TextView balanceTV;
    private TextView contentTV;

    public ConsumptionDetailItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ConsumptionDetailItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        titleTV = (TextView) findViewById(R.id.icd_tv_title);
        dateTV = (TextView) findViewById(R.id.icd_tv_date);
        balanceTV = (TextView) findViewById(R.id.icd_tv_balance);
        contentTV = (TextView) findViewById(R.id.icd_tv_content);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        ConsumptionDetailItem consumptionDetailItem = (ConsumptionDetailItem) item;
        String createTimeStr = consumptionDetailItem.getCreatetime();
        long createTime = 0;
        if (!TextUtils.isEmpty(createTimeStr)) {
            createTime = Long.parseLong(createTimeStr) * 1000;
        }

        titleTV.setText(consumptionDetailItem.getTitle());
        dateTV.setText(Util.getFormatedDateTime("yyyy-MM-dd", createTime));
        balanceTV.setText("余额" + consumptionDetailItem.getEndmoney());
        contentTV.setText("-" + consumptionDetailItem.getMoney());
    }
}
