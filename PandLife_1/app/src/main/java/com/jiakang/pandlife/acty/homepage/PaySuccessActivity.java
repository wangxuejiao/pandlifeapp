package com.jiakang.pandlife.acty.homepage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 支付详情（成功）
 */
@EActivity(R.layout.activity_pay_success)
public class PaySuccessActivity extends BaseActy {
    protected static final String TAG = "PaySuccessActivity";

    @ViewById(R.id.apsu_btn_return_homepage)
    Button returnHomepageBN;
    @ViewById(R.id.apsu_btn_indent_detail)
    Button indentDetailBN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.apsu_tb_title, "支付详情");

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.apsu_btn_return_homepage:
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.apsu_btn_indent_detail:
                break;
        }
    }
}
