package com.jiakang.pandlife.acty.neighbour;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.VillageGoodsInfo;
import com.jiakang.pandlife.item.VillageGoodsItem;
import com.jiakang.pandlife.widget.PullToRefreshListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


@EActivity(R.layout.activity_village_goods)
public class VillageGoodsActivity extends BaseActy {

    @ViewById(R.id.lv_village_goods)
    PullToRefreshListView lvVillageGoods;

    private static final int FIRST_REQUEST = 0;
    private static final int REFRESH = 1;
    private static final int MORE = 2;
    private boolean isMore = true;

    /**
     * 每页个数，BaseInfo.pageSize = 20 为默认值
     */
    private int pageSize = 20;
    /**
     * 当前请求的页
     */
    private int pageIndexGet = 1;


    private List<VillageGoodsItem.DataEntityVillageGoods> mDataEntityVillageGoodsList = new
            ArrayList<>();
    private ItemAdapter mItemAdapter;

    private VillageGoodsInfo mVillageGoodsInfo = new VillageGoodsInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
    }

    @AfterViews
    protected void afterCreateView(){

        initTitleBar(R.id.al_tb_title, "小区拼好货", null, null);

        lvVillageGoods.setShowFootView(true);
        mItemAdapter = new ItemAdapter(mContext);
        lvVillageGoods.setAdapter(mItemAdapter);

        lvVillageGoods.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(VillageGoodsActivity.this, VillageGoodsDetailActivity_.class);
                String replyid = ((VillageGoodsItem.DataEntityVillageGoods)lvVillageGoods.getItemAtPosition(position)).getId();
                intent.putExtra("id",replyid);
                startActivity(intent);
            }
        });

        lvVillageGoods.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            @Override
            public void onRefresh() {

                villageGoodsRequest(REFRESH);
            }
        });

        lvVillageGoods.setOnMoreListener(new PullToRefreshListView.OnMoreListener() {
            @Override
            public void onMore() {
                villageGoodsRequest(MORE);
            }
        });
    }

    private void initData() {

        villageGoodsRequest(FIRST_REQUEST);

    }

    private void villageGoodsRequest(final int listType){

        if(listType == REFRESH || listType == FIRST_REQUEST){
            pageIndexGet = 1;
        }else if(listType == MORE){
            pageIndexGet ++;
        }else {
            pageIndexGet = 1;
        }

        mVillageGoodsInfo.setPageSize(pageSize);
        mVillageGoodsInfo.setPageIndex(pageIndexGet);

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mVillageGoodsInfo,new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                mDataEntityVillageGoodsList = mVillageGoodsInfo.getDataEntityVillageGoodsList();

                if(pageIndexGet < mVillageGoodsInfo.getVillageGoodsItem().getPage().getTotalPage()){
                    isMore = true;
                }else {
                    isMore = false;
                }

                if(listType != MORE){

                    mItemAdapter.clear();
                }

                mItemAdapter.addItems((List) mDataEntityVillageGoodsList);
                mItemAdapter.notifyDataSetChanged();

                lvVillageGoods.onRefreshComplete();
                lvVillageGoods.onMoreComplete(isMore);
            }
        });
    }

    public static void startVillageGoods(Context context){

        Intent intent = new Intent(context,VillageGoodsActivity_.class);
        context.startActivity(intent);
    }
}
