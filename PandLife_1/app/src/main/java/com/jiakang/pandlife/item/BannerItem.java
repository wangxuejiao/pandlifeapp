package com.jiakang.pandlife.item;

import java.util.List;

/**
 * Created by play on 2016/1/25.
 */
public class BannerItem {


    /**
     * data : [{"createtime":"1453542489","kind":"xqphh","pic":"http://m.360buyimg.com/mobilecms/s720x350_jfs/t2092/153/2067194550/267558/b39292a0/569888abN6330465f.jpg","id":"1","bid":"4","type":"1","updatetime":"1453542402","url":null},{"createtime":"1453542457","kind":"linli","pic":"http://m.360buyimg.com/mobilecms/s720x350_jfs/t1855/130/1414784410/46282/e01f19b2/56a04a99Nbb8fa2ec.jpg","id":null,"bid":"6","type":"1","updatetime":"1453542402","url":null},{"createtime":"1453542456","kind":"h5","pic":"http://m.360buyimg.com/mobilecms/s720x350_jfs/t2149/197/1463885523/62798/f2848ecc/56a0dc4cNd4421b42.jpg","id":null,"bid":"1","type":"1","updatetime":"1453542402","url":"http://m.jd.com"},{"createtime":"1453542402","kind":"tuan","pic":"http://m.360buyimg.com/mobilecms/s720x350_jfs/t2269/220/2068072932/203015/1eab57bc/56a0b2aaN57636bad.jpg","id":null,"bid":"2","type":"1","updatetime":"1453542439","url":null},{"createtime":"1453542402","kind":"linli","pic":"http://m.360buyimg.com/mobilecms/s720x350_jfs/t2323/199/1954192461/117790/54dbd089/56976484Ne47e7b07.jpg","id":"305","bid":"3","type":"1","updatetime":"1453542439","url":null},{"createtime":"1453542402","kind":"tuan","pic":"http://m.360buyimg.com/mobilecms/s720x350_jfs/t2239/295/1384687205/88675/b213baa2/569ef416Nbc21061e.jpg","id":"2","bid":"5","type":"1","updatetime":"1453542402","url":null}]
     * status : 1
     * info : banner图片地址
     */
    private List<DataEntityBanner> data;
    private int status;
    private String info;

    public void setData(List<DataEntityBanner> data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<DataEntityBanner> getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }

    public class DataEntityBanner {
        /**
         * createtime : 1453542489
         * kind : xqphh
         * pic : http://m.360buyimg.com/mobilecms/s720x350_jfs/t2092/153/2067194550/267558/b39292a0/569888abN6330465f.jpg
         * id : 1
         * bid : 4
         * type : 1
         * updatetime : 1453542402
         * url : null
         */
        private String createtime;
        private String kind;
        private String pic;
        private String id;
        private String bid;
        private String type;
        private String updatetime;
        private String url;

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setBid(String bid) {
            this.bid = bid;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setUpdatetime(String updatetime) {
            this.updatetime = updatetime;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getCreatetime() {
            return createtime;
        }

        public String getKind() {
            return kind;
        }

        public String getPic() {
            return pic;
        }

        public String getId() {
            return id;
        }

        public String getBid() {
            return bid;
        }

        public String getType() {
            return type;
        }

        public String getUpdatetime() {
            return updatetime;
        }

        public String getUrl() {
            return url;
        }
    }
}
