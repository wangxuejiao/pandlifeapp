package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.VoteItem;

/**
 * Created by play on 2016/1/27.
 */
public class VoteItemView extends AbsLinearLayout {

    private ImageView ivVote;
    private TextView tvVoteOption;
    private ProgressBar pbVote;
    private TextView tvVoteCount;

    public VoteItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VoteItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        ivVote = (ImageView) findViewById(R.id.iv_vote_goods_detail);
        tvVoteOption = (TextView) findViewById(R.id.tv_vote_potion);
        pbVote = (ProgressBar) findViewById(R.id.pb_vote);
        tvVoteCount = (TextView) findViewById(R.id.tv_count_vote);
    }

    @Override
    public void setObject(Item item, final int position, final ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        final VoteItem voteItem = (VoteItem) item;
        tvVoteOption.setText(voteItem.getOption());
        tvVoteCount.setText(voteItem.getCount() + "票");
        pbVote.setMax(voteItem.getTotalCount());
        if(!TextUtils.isEmpty(voteItem.getCount())){
            pbVote.setProgress(Integer.valueOf(voteItem.getCount()));
        }
        if(voteItem.isCheck()){
            ivVote.setImageResource(R.mipmap.ic_checked);
        }else {
            ivVote.setImageResource(R.mipmap.ic_unchecked);
        }

        ivVote.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setTag(voteItem);
                onViewClickListener.onViewClick(v, position);
            }
        });
    }
}
