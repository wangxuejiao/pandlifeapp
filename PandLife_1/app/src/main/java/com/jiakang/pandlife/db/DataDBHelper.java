package com.jiakang.pandlife.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jiakang.pandlife.annotation.utils.SqlBuilder;
import com.jiakang.pandlife.item.GroupPurchaseItem;
import com.jiakang.pandlife.utils.JKLog;


/**
 * 默认就在数据库里创建5张表
 * 
 * @FriendItem
 */
public class DataDBHelper extends SQLiteOpenHelper {
	private static final String TAG = "DataDBHelper";

	private String dbName;// 数据库名称
	private static final int version = 10;// 数据库版本

	/** 大类表 */
	// public final static String TAB_CATEGORY = "t_category";


	public DataDBHelper(Context context, String name) {
		super(context, name, null, version);
		dbName = name;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		JKLog.i("DBOpenHelper", "onCreate------->数据库创建了----");

		// 大类缓存表
		 //db.execSQL(SqlBuilder.getCreatTableSQL(CategoryItem.class));


		/**
		 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ +++ 缓存部分 +++
		 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		 */

		// db.execSQL("create table if not exists " + TAB_CACHE_FRIENDS +
		// " (id varchar PRIMARY KEY, data text) ");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		JKLog.e("DBOpenHelper", "onUpgrade-----> 数据库更新删除表了----oldVersion:" + oldVersion + ";---newVersion:" + newVersion + ";--Dbname:" + dbName);

		switch (oldVersion) {
		case 75:
			// try {
			// // 缓存表加入排序字段
			// db.execSQL("alter table "+TAB_CACHE_MSG+" add sortTop varchar(100) default '0' ");
			// } catch (Exception e) {
			// db.execSQL("DROP TABLE IF EXISTS " + TAB_CACHE_MSG);
			// }
			// 添加兴趣吧好友粉丝列表缓存
			// db.execSQL("DROP TABLE IF EXISTS " + CACHE_INTEREST_FRIENDFANS);

		case 99:

			break;
		default:
			db.execSQL("DROP TABLE IF EXISTS config");
			break;
		}

		// deleteIndex(db, "idx_cachemsg");
		onCreate(db);
	}

	/**
	 * 删除引所值
	 * 
	 * @param db
	 * @param index
	 * @return void
	 */
	public void deleteIndex(SQLiteDatabase db, String index) {
		try {
			db.execSQL("drop index " + index);
		} catch (Exception e) {
			JKLog.e(TAG, "deleteIndex方法中-------->：e为：" + e);
		}
	}
}