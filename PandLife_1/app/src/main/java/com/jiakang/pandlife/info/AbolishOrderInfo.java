package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.utils.JKLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 12.订单取消接口
 *
 * @author ww
 *
 */
public class AbolishOrderInfo extends BaseAbsInfo {

    private static final String TAG = "AbolishOrderInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;
    /** 订单的编号id	int（10） */
    private int id;


    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Order&a=Abolishorder" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("id", id);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");


            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
