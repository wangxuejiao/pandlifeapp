package com.jiakang.pandlife.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiakang.pandlife.R;

/**
 * Created by play on 2016/1/7.
 */
public class VoteComponent extends RelativeLayout{

    private RadioButton radioButton;
    private ProgressBar progressbar;
    private TextView tvCount;

    private int clickIndex;

    private onVoteRadioButtonClickListener onVoteRadioButtonClickListener;


    public VoteComponent(Context context) {
        super(context);
        initView(context);
    }

    public VoteComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public VoteComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public void initView(Context context){

        View view = View.inflate(context, R.layout.item_vote,this);//这里一定要加this，添加服布局，否则显示不出来
        radioButton = (RadioButton) view.findViewById(R.id.rb_vote);
        progressbar = (ProgressBar) view.findViewById(R.id.pb_vote);
        tvCount = (TextView) view.findViewById(R.id.tv_count_vote);

        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(null != onVoteRadioButtonClickListener){
                    onVoteRadioButtonClickListener.onRadioClick();
                }
            }
        });
    }

    /**
     * 判断是否选中
     */
    public boolean isChecked(){

        return radioButton.isChecked();
    }

    /**
     * 设置radioButton状态
     * @param isChecked
     */
    public void setChecked(boolean isChecked){
        radioButton.setChecked(isChecked);
    }

    /**
     * 设置选项内容
     * @param content
     */
    public void setRadioContent(String content){

        radioButton.setText(content);
    }

    /**
     * 设置radiobtn的Id，用于标记当前点击的是哪个radiobtn
     * @param id
     */
    public void setRadioId(int id){

        radioButton.setId(id);
    }

    public RadioButton getRadioButton(){
        return radioButton;
    }

    /**
     * 设置投票的数
     * @param count
     */
    public void setVoteCount(int count){

        tvCount.setText(count + "票");
    }

    /**
     * 设置progress
     * @param count
     */
    public void setProgress(int count){

        progressbar.setProgress(count);
    }

    public interface onVoteRadioButtonClickListener{

        void onRadioClick();

    }

    public void setRadioButtonClickListener(onVoteRadioButtonClickListener onVoteRadioButtonClickListener){

        this.onVoteRadioButtonClickListener = onVoteRadioButtonClickListener;

    }

}
