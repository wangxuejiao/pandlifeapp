package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.QiNiuToken;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by play on 2016/1/14.
 */
public class QinNiuTokenInfo extends BaseAbsInfo {

    private QiNiuToken qiNiuToken;
    private QiNiuToken.DataEntityQiNiuToken dataEntityQiNiuToken;

    public QiNiuToken getQiNiuToken() {
        return qiNiuToken;
    }

    public void setQiNiuToken(QiNiuToken qiNiuToken) {
        this.qiNiuToken = qiNiuToken;
    }

    public QiNiuToken.DataEntityQiNiuToken getDataEntityQiNiuToken() {
        return dataEntityQiNiuToken;
    }

    public void setDataEntityQiNiuToken(QiNiuToken.DataEntityQiNiuToken dataEntityQiNiuToken) {
        this.dataEntityQiNiuToken = dataEntityQiNiuToken;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Upload&a=GetqiniuToken&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        try {
            if(jsonObject.getInt("status") == 1){
                qiNiuToken = BaseInfo.gson.fromJson(jsonObject.toString(), QiNiuToken.class);
                dataEntityQiNiuToken = qiNiuToken.getData();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
