package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.StationNotificationDetail;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by play on 2016/1/13.
 */
public class StationNotificationDetailInfo extends BaseAbsInfo {

    private int id;
    private StationNotificationDetail stationNotificationDetail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StationNotificationDetail getStationNotificationDetail() {
        return stationNotificationDetail;
    }

    public void setStationNotificationDetail(StationNotificationDetail stationNotificationDetail) {
        this.stationNotificationDetail = stationNotificationDetail;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Server&a=Getnoticedetail&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",id);
            jsonObject.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        stationNotificationDetail = BaseInfo.gson.fromJson(jsonObject.toString(),StationNotificationDetail.class);

    }
}
