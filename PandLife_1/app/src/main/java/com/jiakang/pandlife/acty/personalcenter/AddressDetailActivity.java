package com.jiakang.pandlife.acty.personalcenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.DeleteAddressInfo;
import com.jiakang.pandlife.info.MakedefaultAddressInfo;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.service.DownloadService;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.CustomDialog;
import com.jiakang.pandlife.widget.TitleBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

/**
 * 修改地址
 */
@EActivity(R.layout.activity_address_detail)
public class AddressDetailActivity extends BaseActy {
    protected static final String TAG = "AddressDetailActivity";

//    @ViewById(R.id.it_ibn_right)
//    ImageButton redactAddressIBN;
    @ViewById(R.id.aad_tb_title)
    TitleBar mTitleBar;
    @ViewById(R.id.aad_tv_name_content)
    TextView nameTV;
    @ViewById(R.id.aad_tv_phone_content)
    TextView phoneTV;
    @ViewById(R.id.aad_tv_province_select)
    TextView provinceTV;
    @ViewById(R.id.aad_tv_city_select)
    TextView cityTV;
    @ViewById(R.id.aad_tv_area_select)
    TextView areaTV;
    @ViewById(R.id.aad_tv_detail_address_content)
    TextView detailAddressTV;
    @ViewById(R.id.aad_tv_delete_address)
    TextView deleteAddressTV;
    @ViewById(R.id.aad_btn_setting_default)
    Button setDefaultBN;

    private AddressItem mAddressItem;

    /**设置默认收件人/寄件人地址jiek*/
    private MakedefaultAddressInfo mMakedefaultAddressInfo = new MakedefaultAddressInfo();
    /**删除收件人/寄件人地址*/
    private DeleteAddressInfo mDeleteAddressInfo = new DeleteAddressInfo();

    //记录地址类型
    private String addressType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.aad_tb_title, "地址详情", null, "修改");

        initView();
        bindView();
    }

    private void initView(){
        provinceTV.setOnClickListener(this);
        cityTV.setOnClickListener(this);
        areaTV.setOnClickListener(this);
        deleteAddressTV.setOnClickListener(this);
        setDefaultBN.setOnClickListener(this);
        mTitleBar.rightBN.setOnClickListener(this);

//        redactAddressIBN.setImageDrawable(getResources().getDrawable(R.mipmap.icon_iv_scanning));
//        redactAddressIBN.setVisibility(View.VISIBLE);
//        redactAddressIBN.setOnClickListener(this);
    }

    private void bindView(){
        Intent intent = getIntent();
        mAddressItem = (AddressItem)intent.getExtras().get("addressItem");
        addressType = intent.getExtras().getString(MyAddressBookActivity.ADDRESS_TYPE);
        if (mAddressItem == null){
            CustomToast.showToast(mContext, "获取数据异常，稍后请重试");
            return;
        }
        nameTV.setText(mAddressItem.getName());
        phoneTV.setText(mAddressItem.getPhone());
        provinceTV.setText(mAddressItem.getProvince());
        cityTV.setText(mAddressItem.getCity());
        areaTV.setText(mAddressItem.getZone());
        detailAddressTV.setText(mAddressItem.getAddress());
    }

    /**
     * 删除地址方法
     */
    private void deleteAddress(){
        mDeleteAddressInfo.setId(mAddressItem.getId());
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mDeleteAddressInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        CustomToast.showToast(mContext, "删除成功");
                        setResult(RESULT_CANCELED);
                        finish();
                    } else {
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    /**
     * 设置为默认地址方法
     */
    private void setDefaultAddress(){
        mMakedefaultAddressInfo.setId(mAddressItem.getId());
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mMakedefaultAddressInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        CustomToast.showToast(mContext, "默认地址设置成功");
                        finish();
                    } else {
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_REDACT_ADDRESS) && (resultCode == RESULT_OK)){
            mAddressItem = (AddressItem)data.getExtras().get("addressItem");
            nameTV.setText(mAddressItem.getName());
            phoneTV.setText(mAddressItem.getPhone());
            provinceTV.setText(mAddressItem.getProvince());
            cityTV.setText(mAddressItem.getCity());
            areaTV.setText(mAddressItem.getZone());
            detailAddressTV.setText(mAddressItem.getAddress());
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left://点击返回
                finish();
                break;
            case R.id.it_btn_right://点击编辑
                Intent intentRedactAddress = new Intent(this, AddressRedactActivity_.class);
                intentRedactAddress.putExtra("addressItem", mAddressItem);
                intentRedactAddress.putExtra(MyAddressBookActivity.ADDRESS_TYPE, addressType);
                startActivityForResult(intentRedactAddress, Constant.StaticCode.REQUEST_REDACT_ADDRESS);
//                redactAddress();
                break;
            case R.id.aad_tv_delete_address://点击删除地址
                final CustomDialog customDialog = Util.getDialog(mContext, "提示", "确定删除吗？", "确定", "取消");
                customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                        deleteAddress();
                    }
                });
                customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                    }
                });
                customDialog.show();

                break;
            case R.id.aad_btn_setting_default://点击设置为默认地址
                setDefaultAddress();
                break;
        }
    }
}
