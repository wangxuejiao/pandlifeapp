package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 3.团购商品订单添加接口
 *
 * @author ww
 *
 */
public class AddOrderInfo extends BaseAbsInfo {

    private static final String TAG = "AddOrderInfo";

    /** 商品id */
    private int gid;
    /** 收件人地址id */
    private int aid;
    /** 站点的shopid */
    private int station_id;
    /** 团购产品数量 */
    private int number;
    /** 支付方式（alipay，微信支付..） */
    private String pay_kind;
    /** 红包id */
    private int hid;
    /** 配送费 */
    private int ps_money;
    /** 产品总金额 */
    private int total_money;
    /** 支付总金额 */
    private int pay_money;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;
    /** 配送备注	Varchar(32) */
    private String remarks;

    /** 请求成功后返回的订单号 */
    private String mailno;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Torder&a=Addorder" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("gid", gid);
            json.put("aid", aid);
            json.put("station_id", station_id);
            json.put("number", number);
            json.put("pay_kind", pay_kind);
            json.put("hid", hid);
            json.put("ps_money", ps_money);
            json.put("total_money", total_money);
            json.put("pay_money", pay_money);
            json.put("remarks", remarks);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                mailno = jsonObject.get("data").toString();

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPay_kind() {
        return pay_kind;
    }

    public void setPay_kind(String pay_kind) {
        this.pay_kind = pay_kind;
    }

    public int getHid() {
        return hid;
    }

    public void setHid(int hid) {
        this.hid = hid;
    }

    public int getPs_money() {
        return ps_money;
    }

    public void setPs_money(int ps_money) {
        this.ps_money = ps_money;
    }

    public int getTotal_money() {
        return total_money;
    }

    public void setTotal_money(int total_money) {
        this.total_money = total_money;
    }

    public int getPay_money() {
        return pay_money;
    }

    public void setPay_money(int pay_money) {
        this.pay_money = pay_money;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMailno() {
        return mailno;
    }

    public void setMailno(String mailno) {
        this.mailno = mailno;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
