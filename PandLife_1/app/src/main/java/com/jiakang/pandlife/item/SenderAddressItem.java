//package com.jiakang.pandlife.item;
//
//import com.jiakang.pandlife.R;
//
///**
// * Created by Administrator on 2016/1/11.
// */
//public class SenderAddressItem extends Item{
//
//    /** 姓名	Varchar(32) */
//    private String name;
//    /** 省份id	Int(10) */
//    private int province_id;
//    /** 市id	Int(10) */
//    private int city_id;
//    /** 区/县id	Int(10) */
//    private int zone_id;
//    /** 省份名称	Varchar(32) */
//    private String province;
//    /** 市名称	Varchar(32) */
//    private String city;
//    /** 区/县名称	Varchar(32) */
//    private String zone;
//    /** 详细地址	Varchar(32) */
//    private String address;
//    /** 手机号码	Varchar(11) */
//    private String phone;
//    /** 登录时返回的凭证	Varchar(32) */
//
//    private int layout = R.layout.item_my_address;
//
//    @Override
//    public int getItemLayoutId() {
//        return layout;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public int getProvince_id() {
//        return province_id;
//    }
//
//    public void setProvince_id(int province_id) {
//        this.province_id = province_id;
//    }
//
//    public int getCity_id() {
//        return city_id;
//    }
//
//    public void setCity_id(int city_id) {
//        this.city_id = city_id;
//    }
//
//    public int getZone_id() {
//        return zone_id;
//    }
//
//    public void setZone_id(int zone_id) {
//        this.zone_id = zone_id;
//    }
//
//    public String getProvince() {
//        return province;
//    }
//
//    public void setProvince(String province) {
//        this.province = province;
//    }
//
//    public String getCity() {
//        return city;
//    }
//
//    public void setCity(String city) {
//        this.city = city;
//    }
//
//    public String getZone() {
//        return zone;
//    }
//
//    public void setZone(String zone) {
//        this.zone = zone;
//    }
//
//    public String getAddress() {
//        return address;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }
//
//    public String getPhone() {
//        return phone;
//    }
//
//    public void setPhone(String phone) {
//        this.phone = phone;
//    }
//
//    public int getLayout() {
//        return layout;
//    }
//
//    public void setLayout(int layout) {
//        this.layout = layout;
//    }
//}
