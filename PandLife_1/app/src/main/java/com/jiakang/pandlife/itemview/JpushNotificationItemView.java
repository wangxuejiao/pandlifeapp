package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.JpushNotificationItem;
import com.jiakang.pandlife.item.StationNotificationItem;
import com.jiakang.pandlife.utils.DateUtils;

/**
 * Created by play on 2016/1/4.
 */
public class JpushNotificationItemView extends AbsLinearLayout{
    private String TAG = "JpushNotificationItemView";
	private TextView tvTitle;
    private TextView tvDate;
	private TextView tvContent;
	private TextView readFlag;

	public JpushNotificationItemView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public JpushNotificationItemView(Context context) {
		super(context);
	}

	@Override
	public void findViewsByIds() {
		tvTitle = (TextView) findViewById(R.id.tv_title_jpush_notification);
		tvDate = (TextView) findViewById(R.id.tv_date_jpush_notification);
		tvContent = (TextView) findViewById(R.id.tv_content_jpush_notification);
		readFlag = (TextView) findViewById(R.id.tv_read_flag_jpush_notification);

	}

	@Override
	public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
		super.setObject(item, position, onViewClickListener);

		JpushNotificationItem jpushNotificationItem = (JpushNotificationItem) item;
		tvTitle.setText(jpushNotificationItem.getTitle());
		tvDate.setText(DateUtils.getDateStringyyyyMMddHH(jpushNotificationItem.getTime()));
		tvContent.setText(jpushNotificationItem.getMessage());
		readFlag.setText(jpushNotificationItem.getFlag());
    }

}
