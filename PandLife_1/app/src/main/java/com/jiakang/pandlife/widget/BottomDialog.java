package com.jiakang.pandlife.widget;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.jiakang.pandlife.R;


/**
 * 自定义Dialog
 * 
 * @author ww
 */
public class BottomDialog extends Dialog {
	private String TAG = "BottomDialog";

	public BottomDialog(Activity ctx, int layout, int width, int height) {
		this(ctx.getLayoutInflater().inflate(layout, null), width, height);
	}

	public BottomDialog(View view, int width, int height) {
		this(view, width, height, R.style.BottomDialog);
	}

	public BottomDialog(View view, int width, int height, int style) {
		super(view.getContext(), style);

		// // 透明背景
		// Drawable myDrawable =
		// context.getResources().getDrawable(R.drawable.dialog_root_bg);
		// myDrawable.setAlpha(150);
		// view.setBackgroundDrawable(myDrawable);
		setContentView(view);

		Window window = getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		params.width = width;
		params.height = height;
		// params.height = (int) (default_height * density);
		// view.measure(0, 0);
		// params.width = view.getMeasuredWidth();
		// params.height = view.getMeasuredHeight();
		params.gravity = Gravity.CENTER;
		window.setAttributes(params);

		// 此处可以设置dialog显示的位置
		window.setGravity(Gravity.BOTTOM);
		// 添加动画
		// window.setWindowAnimations(R.style.popup_in_out);
	}

	/**
	 * 为指定id控件设置点击监听
	 * 
	 * @param id
	 * @param listener
	 */
	public BottomDialog setOnClickListener(int id, View.OnClickListener listener) {
		View view = findViewById(id);
		if (view != null && listener != null)
			view.setOnClickListener(listener);
		return this;
	}

	public void setOutsideTouchable(boolean touchable) {
	}

	public void setBackgroundDrawable(Drawable background) {
	}

	public void setAnimationStyle(int animationStyle) {
	}

	// public void setOnDismissListener(final
	// android.widget.PopupWindow.OnDismissListener onDismissListener){
	// this.setOnDismissListener(new OnDismissListener() {
	// @Override
	// public void onDismiss(DialogInterface dialog) {
	// onDismissListener.onDismiss();
	// }
	// });
	// }
	public void showAtLocation(View parent, int gravity, int x, int y) {
		show();
	}

}