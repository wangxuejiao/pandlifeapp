package com.jiakang.pandlife.acty.neighbour;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.MainPostSupportInfo;
import com.jiakang.pandlife.info.ReplayCommentPostInfo;
import com.jiakang.pandlife.info.ReplayPostInfo;
import com.jiakang.pandlife.info.ReplyPostSupportInfo;
import com.jiakang.pandlife.info.VillageGoodsDetailInfo;
import com.jiakang.pandlife.info.VillageGoodsVoteInfo;
import com.jiakang.pandlife.item.PostDetail;
import com.jiakang.pandlife.item.ReplayItem;
import com.jiakang.pandlife.item.VillageGoodsDetail;
import com.jiakang.pandlife.item.VoteItem;
import com.jiakang.pandlife.utils.DataCache;
import com.jiakang.pandlife.utils.DateUtils;
import com.jiakang.pandlife.utils.DimensionUtil;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.widget.PullToRefreshListView;
import com.jiakang.pandlife.widget.PullToRefreshListViewInScrollView;
import com.jiakang.pandlife.widget.ScrollGridView;
import com.jiakang.pandlife.widget.XScrollView;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import me.iwf.photopicker.widget.TouchImageView;

@EActivity(R.layout.activity_goods_detail)
public class VillageGoodsDetailActivity extends BaseActy implements XScrollView.IXScrollViewListener {

    private static final String TAG = "GoodsDetailActivity";

    private TextView tvType;
    private TextView tvTitle;
    private TextView tvDate;
    private TextView tvContent;
    private TextView tvSupportCount;
    private ImageButton ivSupport;
    private TextView tvCommentCount;
    private ImageView ivComment;
    private ScrollGridView gvImage;
    private TextView tvDeadlineVote;
    private Button btnVote;
    private PullToRefreshListView lvVote;
    private PullToRefreshListViewInScrollView lvReplay;
    private EditText etReply;
    private TextView btnReply;//点击回复
    private XScrollView scrollView;

    private List<VillageGoodsDetail.ImglistEntity> mImglistEntityList = new ArrayList<>();
    private List<ReplayItem> mReplayItemList = new ArrayList<>();
    private List<VoteItem> mVoteItemList = new ArrayList<>();

    private ItemAdapter mVoteItemAdapter;
    private ItemAdapter mReplyItemAdapter;

    private String goodsID;
    private int replyFlag;//1代表回复主贴，2代表楼层回帖

    InputMethodManager mInputMethodManager;
    DataCache mACache; //数据缓存

    private ApiManager mApiManager = ApiManager.getInstance();

    //主贴点赞
    private MainPostSupportInfo mMainPostSupportInfo = new MainPostSupportInfo();

    //回复帖子点赞
    private ReplyPostSupportInfo mReplyPostSupportInfo = new ReplyPostSupportInfo();

    //回复主贴
    private ReplayPostInfo mReplayPostInfo = new ReplayPostInfo();

    //回复楼层评论
    private ReplayCommentPostInfo mReplayCommentPostInfo = new ReplayCommentPostInfo();

    //小区拼好货详情
    private VillageGoodsDetailInfo mVillageGoodsDetailInfo = new VillageGoodsDetailInfo();
    private VillageGoodsDetail mVillageGoodsDetail;

    //投票
    private VillageGoodsVoteInfo mVillageGoodsVoteInfo = new VillageGoodsVoteInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @AfterViews
    protected void afterCreateView() {

        initView();
        initData();
    }

    /**
     * 初始化控件
     */
    private void initView(){

        initTitleBar(R.id.al_tb_title, "拼好货详情", null, null);
        scrollView = (XScrollView) findViewById(R.id.xscorllview_goods_detail);
        View scrollview_content = LayoutInflater.from(this).inflate(R.layout.village_goods_detail_scrollview_content,null);
        if(null != scrollview_content){

            tvType = (TextView) scrollview_content.findViewById(R.id.tv_type_goods_detail);
            tvTitle = (TextView) scrollview_content.findViewById(R.id.tv_title_goods_detail);
            tvDate = (TextView) scrollview_content.findViewById(R.id.tv_date_goods_detail);
            tvContent = (TextView) scrollview_content.findViewById(R.id.tv_content_goods_detail);
            tvSupportCount = (TextView) scrollview_content.findViewById(R.id.tv_support_count_goods_detail);
            tvCommentCount = (TextView) scrollview_content.findViewById(R.id.tv_comment_count_goods_detail);
            ivSupport = (ImageButton) scrollview_content.findViewById(R.id.iv_support_goods_detail);
            ivComment = (ImageView) scrollview_content.findViewById(R.id.iv_comment_goods_detail);
            tvDeadlineVote = (TextView) scrollview_content.findViewById(R.id.tv_deadline_goods_detail);
            btnVote = (Button) scrollview_content.findViewById(R.id.btn_vote_goods_detail);
            gvImage = (ScrollGridView) scrollview_content.findViewById(R.id.gv_image_goods_detail);
            lvVote = (PullToRefreshListView) scrollview_content.findViewById(R.id.lv_vote_goods_detail);
            lvReplay = (PullToRefreshListViewInScrollView) scrollview_content.findViewById(R.id.lv_replay_goods_detail);
            etReply = (EditText) findViewById(R.id.et_reply_goods_detail);
            btnReply = (TextView) findViewById(R.id.btn_reply_goods_detail);
        }

        scrollView.setView(scrollview_content);

        scrollView.setPullRefreshEnable(true);
        scrollView.setPullLoadEnable(false);
        scrollView.setIXScrollViewListener(this);
        scrollView.setRefreshTime(scrollView.getTime());

        ivSupport.setOnClickListener(this);
        lvReplay.setFocusable(false);
        lvReplay.setFocusableInTouchMode(false);

        ivSupport.setOnClickListener(this);
        ivComment.setOnClickListener(this);
        btnVote.setOnClickListener(this);
        btnReply.setOnClickListener(this);


        mReplyItemAdapter = new ItemAdapter(mContext);
        lvReplay.setAdapter(mReplyItemAdapter);

        mVoteItemAdapter = new ItemAdapter(mContext);
        lvVote.setAdapter(mVoteItemAdapter);

    }

    /**
     * 初始化数据
     */
    private void initData() {

        goodsID = getIntent().getExtras().getString("id");
        if(TextUtils.isEmpty(goodsID)){
            return;
        }

        //判断缓存是否存在
        mACache = DataCache.get(mContext);
        String key = "dataEntityGoodsDetail" + goodsID;
        JSONObject jsonObject = mACache.getAsJSONObject(key);
        if(jsonObject != null){

            JSONObject dataJsonObject = null;
            try {
                dataJsonObject = jsonObject.getJSONObject("data");
                mVillageGoodsDetail = BaseInfo.gson.fromJson(dataJsonObject.toString(), VillageGoodsDetail.class);
                setView();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else {
            villageGoodsDetailRequest();
        }

        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
    }

    /**
     * 获取数据后给控件赋值
     */
    public void setView() {

        tvType.setText(mVillageGoodsDetail.getTypename());
        tvTitle.setText(mVillageGoodsDetail.getTitle());
        long tempDate = Long.parseLong(mVillageGoodsDetail.getTime()) * 1000;
        tvDate.setText(DateUtils.getDateStringyyyyMMddHHmm(tempDate));
        tvContent.setText(mVillageGoodsDetail.getContent());
        if (mVillageGoodsDetail.getSupport() == 1) {
            ivSupport.setImageResource(R.mipmap.ic_support_press_replay);
        } else {
            ivSupport.setImageResource(R.mipmap.ic_support_normal_replay);
        }
        tvSupportCount.setText(mVillageGoodsDetail.getSupportnum());
        tvCommentCount.setText(mVillageGoodsDetail.getReplynum());

        //拼好货详情照片
        if (mVillageGoodsDetail.getImglist() != null && mVillageGoodsDetail.getImglist().size() > 0) {
            mImglistEntityList = mVillageGoodsDetail.getImglist();
            gvImage.setAdapter(new PictureGridViewAdapter());
        }

        long tempDeadlineDate = Long.parseLong(mVillageGoodsDetail.getTime()) * 1000;
        tvDeadlineVote.setText("投票截止时间：" + DateUtils.getDateStringyyyyMMddHH(tempDeadlineDate));
        //投票
        mVoteItemList = mVillageGoodsDetail.getOptionlist();
        int totalCount = 0;
        if (mVoteItemList != null && mVoteItemList.size() > 0) {

            //计算投票总数
            for (int i = 0; i < mVoteItemList.size(); i++) {

                if (!TextUtils.isEmpty(mVoteItemList.get(i).getCount())) {
                    totalCount += Integer.valueOf(mVoteItemList.get(i).getCount());
                }
            }

            //给每一个选项设置投票总数
            for (int i = 0; i < mVoteItemList.size(); i++) {
                mVoteItemList.get(i).setTotalCount(totalCount);
            }
            mVoteItemAdapter.clear();
            mVoteItemAdapter.addItems((List) mVoteItemList);
            mVoteItemAdapter.notifyDataSetChanged();
        }


        //回复列表
        mReplayItemList = mVillageGoodsDetail.getDialoglist();
        if (mReplayItemList != null && mReplayItemList.size() > 0) {

            for (int i = 0; i < mReplayItemList.size(); i++) {//设置标识符，表明是小区拼好货的回复列表
                mReplayItemList.get(i).setFlag(2);
            }
            mReplyItemAdapter.clear();
            mReplyItemAdapter.addItems((List) mReplayItemList);
            mReplyItemAdapter.notifyDataSetChanged();
        }

        //listView投票点击事件
        mVoteItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {

                String currentId = ((VoteItem) view.getTag()).getId();
                for (int i = 0; i < mVoteItemList.size(); i++) {

                    VoteItem voteItem = mVoteItemList.get(i);
                    if (currentId.equals(voteItem.getId())) {
                        voteItem.setCheck(true);
                    } else {
                        voteItem.setCheck(false);
                    }
                }
                mVoteItemAdapter.notifyDataSetChanged();

                mVillageGoodsVoteInfo.setId(Integer.parseInt(currentId));
            }
        });

        mReplyItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {

                switch (view.getId()) {

                    case R.id.iv_support_replay:  //回复帖子点赞


                        ReplayItem replayItem = (ReplayItem) view.getTag();
                        if(replayItem.getSupport() == 1){
                            Toast.makeText(mContext,"您已点赞，不能重复点赞",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        int id = Integer.parseInt(replayItem.getId());
                        mReplyPostSupportInfo.setForumid(id);
                        mReplyPostSupportInfo.setType("r");
                        supportReplyPostRequest();
                        break;

                    case R.id.iv_comment_replay: //回复楼层帖子

                        mInputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

                        replyFlag = 2;
                        int replyid = Integer.parseInt(((ReplayItem) view.getTag()).getId());
                        mReplayCommentPostInfo.setReplyid(replyid);

                        String nick = ((ReplayItem) view.getTag()).getNick();
                        if (TextUtils.isEmpty(nick)) {
                            mReplayCommentPostInfo.setContent("回复匿名用户：");
                        } else {
                            mReplayCommentPostInfo.setContent("回复" + nick + "：");
                        }
                        etReply.setHint(mReplayCommentPostInfo.getContent());
                }
            }
        });

    }

    /**
     * 小区拼好货详情
     */
    private void villageGoodsDetailRequest() {

        mVillageGoodsDetailInfo.setId(Integer.parseInt(goodsID));
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mVillageGoodsDetailInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mVillageGoodsDetail = mVillageGoodsDetailInfo.getVillageGoodsDetail();
                setView();
            }
        });
    }

    /**
     * 小区拼好货投票
     */
    private void voteRequest(){

        if(mVillageGoodsVoteInfo.getId() == -1){
            Toast.makeText(mContext,"请选择投票选项",Toast.LENGTH_SHORT).show();
            return;
        }

        if(mVillageGoodsDetail.getIsoption().equals("1")){
            Toast.makeText(mContext,"已投票，不能重复投票",Toast.LENGTH_SHORT).show();
            return;
        }

        long tempDeadlineDate = Long.parseLong(mVillageGoodsDetail.getTime()) * 1000;
        String deadlineTime = DateUtils.getDateStringyyyyMMddHH(tempDeadlineDate);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        if(!DateUtils.compareAfterDate(format,deadlineTime)){
            Toast.makeText(mContext,"投票时间已截止，不能再投票！",Toast.LENGTH_SHORT).show();
            return;
        }

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mVillageGoodsVoteInfo,new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                Toast.makeText(mContext,"投票成功！",Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 主帖点赞
     */
    private void supportMainPostRequest(){

        if(TextUtils.isEmpty(goodsID)){
            return;
        }
        mMainPostSupportInfo.setForumid(Integer.parseInt(goodsID));
        mMainPostSupportInfo.setType("s");
        mApiManager.request(mMainPostSupportInfo,new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                villageGoodsDetailRequest();
            }
        });
    }


    /**
     * 回复主贴
     */
    private void replyPostRequest(){

        if(TextUtils.isEmpty(goodsID)){
            return;
        }
        mReplayPostInfo.setReplyid(Integer.parseInt(goodsID));
        if(TextUtils.isEmpty(etReply.getText().toString())){
            Toast.makeText(mContext,"请输入回复内容！",Toast.LENGTH_SHORT).show();
            return;
        }
        mReplayPostInfo.setContent(etReply.getText().toString());

        mApiManager.request(mReplayPostInfo,new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                Toast.makeText(mContext,"回复主题成功！",Toast.LENGTH_SHORT).show();
                villageGoodsDetailRequest();
                etReply.setText("");
                etReply.setHint("回复主题：");
                mReplayPostInfo.setContent("");
                mInputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });
    }

    /**
     * 回复帖子点赞
     */
    private void supportReplyPostRequest(){

        mApiManager.request(mReplyPostSupportInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                villageGoodsDetailRequest();
               // Toast.makeText(mContext,"楼层点赞成功！",Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 回复楼层帖子
     */
    private void replyCommentPostRequest(){


        if(TextUtils.isEmpty(etReply.getText())){//用户没有输入内容

            Toast.makeText(mContext,"请输入回复内容！",Toast.LENGTH_SHORT).show();
            return;
        }

        mReplayCommentPostInfo.setContent(mReplayCommentPostInfo.getContent() + etReply.getText().toString());

        mApiManager.request(mReplayCommentPostInfo,new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                villageGoodsDetailRequest();
                Toast.makeText(mContext,"回复楼层评论成功！",Toast.LENGTH_SHORT).show();
                etReply.setText("");
                etReply.setHint("回复主题：");
                replyFlag = 1;
                mReplayPostInfo.setContent("");
                mInputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });
    }


    @Override
    public void onRefresh() {

        villageGoodsDetailRequest();
        scrollView.finishLoad();
    }

    @Override
    public void onLoadMore() {

    }


    //帖子详情中gridview图片适配器
    class PictureGridViewAdapter extends BaseAdapter {

        @Override
        public int getCount() {

            return mImglistEntityList.size();
        }

        @Override
        public Object getItem(int position) {

            return mImglistEntityList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            WindowManager wm = VillageGoodsDetailActivity.this.getWindowManager();

            int imageSize = (int) ((wm.getDefaultDisplay().getWidth() - DimensionUtil.dip2px
                    (mContext, 26)) / 3);
            TouchImageView imageView;
            if (convertView == null) {

                imageView = new TouchImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(imageSize, imageSize));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } else {
                imageView = (TouchImageView) convertView;
            }

            ImageLoaderUtil.displayImage(mImglistEntityList.get(position).getImgurl(), imageView, R.mipmap.default_image);
            return imageView;
        }
    }

    @Click(R.id.btn_vote_goods_detail)
    protected void voteClick(){
        voteRequest();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){

            case R.id.iv_support_goods_detail:
                if(mVillageGoodsDetail.getSupport() == 1){
                    Toast.makeText(mContext,"您已点赞，不能重复点赞",Toast.LENGTH_SHORT).show();
                    return;
                }
                supportMainPostRequest();
                break;

            case R.id.iv_comment_goods_detail:
                mInputMethodManager.toggleSoftInput(0, InputMethodManager.RESULT_SHOWN);
                replyFlag = 1;
                etReply.setHint("回复主题：");

                break;
            case R.id.btn_vote_goods_detail:
                voteRequest();
                break;
            case R.id.btn_reply_goods_detail:

                if(replyFlag == 2){ //回复楼层
                    replyCommentPostRequest();
                }else {
                    replyPostRequest();
                }
                break;

        }
    }
}
