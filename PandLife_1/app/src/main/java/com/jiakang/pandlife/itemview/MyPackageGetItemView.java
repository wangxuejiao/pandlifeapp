package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.NearbyStationItem;

/**
 * Created by play on 2016/1/6.
 */
public class MyPackageGetItemView extends AbsLinearLayout {

    private TextView waybillNumberTV;
    private TextView expressCompanyTV;
    private TextView obtainDateTV;

    public MyPackageGetItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyPackageGetItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        waybillNumberTV = (TextView) findViewById(R.id.impg_tv_waybill_number_content);
        expressCompanyTV = (TextView) findViewById(R.id.impg_tv_express_company_content);
        obtainDateTV = (TextView) findViewById(R.id.impg_tv_obtain_date_content);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        ExpressListItem expressListItem = (ExpressListItem) item;
        waybillNumberTV.setText(expressListItem.getMailno());
        expressCompanyTV.setText(expressListItem.getCompanyname());
        obtainDateTV.setText(expressListItem.getGmtcreate());

    }
}
