//package com.example.cxl.cxlframe.itemview;
//
//import android.content.Context;
//import android.util.AttributeSet;
//import android.view.View;
//import android.widget.TextView;
//
//import com.example.cxl.cxlframe.R;
//import com.example.cxl.cxlframe.adapter.ItemAdapter;
//import com.example.cxl.cxlframe.item.Item;
//import com.example.cxl.cxlframe.utils.JKLog;
//import com.example.cxl.cxlframe.utils.PinyinUtil;
//
//
//public class CountryItemView extends AbsRelativeLayout {
//
//	private String TAG = "CountryItemView";
//	private TextView titleTV;
//	private TextView contentTV;
//
//	public CountryItemView(Context context, AttributeSet attrs) {
//		super(context, attrs);
//	}
//
//	public CountryItemView(Context context) {
//		super(context);
//	}
//
//	@Override
//	public void findViewsByIds() {
//		titleTV = (TextView) findViewById(R.id.ic_tv_alpha);
//		contentTV = (TextView) findViewById(R.id.ic_tv_name);
//
//	}
//
//	@Override
//	public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
//		super.setObject(item, position, onViewClickListener);
//		JKLog.i(TAG, "CountryItemView中：setObject方法-------------->" + position);
//
//		String currentStr = "";
//		String previewStr = "";
//		String name = "";
//
//		if (item instanceof CountryItem) {
//			CountryItem countryItem = (CountryItem) item;
//			name = countryItem.getName();
//			currentStr = PinyinUtil.getFirstBig(countryItem.getSortName());
//			previewStr = PinyinUtil.getFirstBig(countryItem.getPreSortName());
//		} else if (item instanceof ProvinceItem) {
//			ProvinceItem provinceItem = (ProvinceItem) item;
//			name = provinceItem.getProvince();
//			currentStr = PinyinUtil.getFirstBig(provinceItem.getSortProvince());
//			previewStr = PinyinUtil.getFirstBig(provinceItem.getPreSortName());
//		} else if (item instanceof CityItem) {
//			CityItem cityItem = (CityItem) item;
//			name = cityItem.getCity();
//			currentStr = PinyinUtil.getFirstBig(cityItem.getSortCity());
//			previewStr = PinyinUtil.getFirstBig(cityItem.getPreSortName());
//		} else if (item instanceof AreaItem) {
//			AreaItem areaItem = (AreaItem) item;
//			name = areaItem.getArea();
//			currentStr = PinyinUtil.getFirstBig(areaItem.getSortArea());
//			previewStr = PinyinUtil.getFirstBig(areaItem.getPreSortName());
//		}
//		if (!previewStr.equals(currentStr)) {
//			titleTV.setVisibility(View.VISIBLE);
//			titleTV.setText(currentStr);
//		} else {
//			titleTV.setVisibility(View.GONE);
//		}
//		contentTV.setText(name);
//	}
//}
