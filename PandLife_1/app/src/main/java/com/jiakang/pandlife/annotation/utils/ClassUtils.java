package com.jiakang.pandlife.annotation.utils;

import com.jiakang.pandlife.annotation.info.TableField;
import com.jiakang.pandlife.annotation.sqlite.Id;
import com.jiakang.pandlife.annotation.sqlite.Table;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ClassUtils {

	/**
	 * 根据实体类 获得 实体类对应的表名
	 * 
	 * @param entity
	 * @return
	 */
	public static String getTableName(Class<?> clazz) {
		Table table = clazz.getAnnotation(Table.class);
		// 没有注释时
		if (table == null || table.name().trim().length() == 0) {
			// 当没有注解的时候默认用类的名称作为表名,并把点（.）替换为下划线(_)
			return clazz.getName().replace('.', '_');
		}
		// 返回注释值
		return table.name();
	}

	/**
	 * 获取实体对象的主键字段的值
	 * 
	 * @param entity
	 * @return
	 */
	public static Object getPrimaryKeyValue(Object entity) {
		return FieldUtils.getFieldValue(entity, ClassUtils.getPrimaryKeyField(entity.getClass()));
	}

	/**
	 * 为建表做准备<br>
	 * 根据实体类 获得 建表的主键名，获得注释Id.column()属性返回<br>
	 * 未注释则先找_id,再找id返回<br>
	 * 还未找到，则返回null<br>
	 * 
	 * @param clazz
	 * <br>
	 * @return<br>
	 * 
	 *             如：
	 * @Id(column="userId")<br> private int user_Id;<br>
	 *                          此处返回userId<br>
	 * 
	 */
	public static String getPrimaryKeyColumn(Class<?> clazz) {
		String primaryKey = null;
		Field[] fields = clazz.getDeclaredFields();
		if (fields != null) {
			Id idAnnotation = null;
			Field idField = null;

			for (Field field : fields) { // 获取ID注解
				idAnnotation = field.getAnnotation(Id.class);
				if (idAnnotation != null) {
					idField = field;
					break;
				}
			}

			if (idAnnotation != null) { // 有ID注解
				primaryKey = idAnnotation.column();
				if (primaryKey == null || primaryKey.trim().length() == 0)
					primaryKey = idField.getName();
			} else { // 没有ID注解,默认去找 _id 和 id 为主键，优先寻找 _id
				for (Field field : fields) {
					if ("_id".equals(field.getName()))
						return "_id";
				}

				for (Field field : fields) {
					if ("id".equals(field.getName()))
						return "id";
				}
			}
		} else {
			throw new RuntimeException("this model[" + clazz + "] has no field");
		}
		return primaryKey;
	}

	/**
	 * 根据实体类类型获得注释Id的原字段Field<br>
	 * 未注释则先找_id,再找id对象Field返回<br>
	 * 还未找到，则返回null<br>
	 * 
	 * @param clazz
	 * <br>
	 * @return<br> 如：
	 * @Id(column="userId")<br> private int user_Id;<br>
	 *                          此处返回user_Id的包装类Field<br>
	 */
	public static Field getPrimaryKeyField(Class<?> clazz) {
		Field primaryKeyField = null;
		Field[] fields = clazz.getDeclaredFields();
		if (fields != null) {

			for (Field field : fields) { // 获取ID注解
				if (field.getAnnotation(Id.class) != null) {
					primaryKeyField = field;
					break;
				}
			}

			if (primaryKeyField == null) { // 没有ID注解
				for (Field field : fields) {
					if ("_id".equals(field.getName())) {
						primaryKeyField = field;
						break;
					}
				}
			}

			if (primaryKeyField == null) { // 如果没有_id的字段
				for (Field field : fields) {
					if ("id".equals(field.getName())) {
						primaryKeyField = field;
						break;
					}
				}
			}

		} else {
			throw new RuntimeException("this model[" + clazz + "] has no field");
		}
		return primaryKeyField;
	}

	/**
	 * 根据实体类 获得 实体类对应的表名
	 * 
	 * @param entity
	 * @return
	 */
	public static String getPrimaryKeyFieldName(Class<?> clazz) {
		Field f = getPrimaryKeyField(clazz);
		return f == null ? null : f.getName();
	}

	/**
	 * 将对象转换为ContentValues
	 * 
	 * @param entity
	 * @param selective
	 *            是否忽略 值为null的字段
	 * @return
	 */
	public static List<TableField> getPropertyList(Class<?> clazz) {

		List<TableField> plist = new ArrayList<TableField>();
		try {
			Field[] fs = clazz.getDeclaredFields();
			String primaryKeyFieldName = getPrimaryKeyFieldName(clazz);
			for (Field f : fs) {
				// 必须是基本数据类型和没有标瞬时态的字段
				if (!FieldUtils.isTransient(f)) {
					if (FieldUtils.isBaseDateType(f)) {

						if (f.getName().equals(primaryKeyFieldName)) // 过滤主键
							continue;

						TableField tableField = new TableField();
						// varchar(60)
						tableField.setType(FieldUtils.getPropertyTypeByField(f));
						// 表列名
						tableField.setColumn(FieldUtils.getPropertyColumnByField(f));
						// DEFAULT(20)
						tableField.setDefaultValue(FieldUtils.getPropertyDefaultValue(f));

						tableField.setDataType(f.getType());
						tableField.setFieldName(f.getName());
						tableField.setSet(FieldUtils.getFieldSetMethod(clazz, f));
						tableField.setGet(FieldUtils.getFieldGetMethod(clazz, f));
						tableField.setField(f);
						plist.add(tableField);
					}
				}
			}
			return plist;
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

}
