package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

/**
 * Created by Administrator on 2016/1/12.
 */
public class SiteItem extends Item{

    /** 站点id	Int(10) */
    private int shopid;
    /** 站点名称	Varchar(32) */
    private String nice_name;
    /** 站点图片	Varchar(128) */
    private String pic;
    /** 站点地址	Varchar(32) */
    private String address;
    /** 经度	Varchar(32) */
    private String longitude;
    /** 纬度	Varchar(32) */
    private String latitude;
    /** 店铺开放时间	Varchar(32) */
    private String opentime;
    /** 站点联系方式(电话/手机号)	Varchar(32) */
    private String mobiphone;
    /** 城市id	Int(10) */
    private int city_id;
    /** 区/县id	Int(10) */
    private int zone_id;
    /** 距离（单位：米）	Varchar(32) */
    private String juli;


    private int layout = R.layout.item_nearby_station;
    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public int getShopid() {
        return shopid;
    }

    public void setShopid(int shopid) {
        this.shopid = shopid;
    }

    public String getNice_name() {
        return nice_name;
    }

    public void setNice_name(String nice_name) {
        this.nice_name = nice_name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getOpentime() {
        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public String getMobiphone() {
        return mobiphone;
    }

    public void setMobiphone(String mobiphone) {
        this.mobiphone = mobiphone;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getZone_id() {
        return zone_id;
    }

    public void setZone_id(int zone_id) {
        this.zone_id = zone_id;
    }

    public String getJuli() {
        return juli;
    }

    public void setJuli(String juli) {
        this.juli = juli;
    }
}
