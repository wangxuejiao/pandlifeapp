package com.jiakang.pandlife.acty.service;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.NearbyStationInfo;
import com.jiakang.pandlife.item.NearbyStationItem;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.widget.PullToRefreshListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;



@EActivity(R.layout.activity_nearby_station)
public class NearbyStationActivity extends BaseActy {

    @ViewById(R.id.lv_nearby_station)
    PullToRefreshListView lvNearbyStation;

    private ItemAdapter mItemAdapter;

    private LocationManager mLocationManager;
    private Location mLocation;

    private NearbyStationInfo mNearbyStationInfo = new NearbyStationInfo();
    private List<NearbyStationItem.DataEntity> mDataEntityList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
        startLocation();
    }

    @AfterViews
    protected void afterCreateView() {

        initTitleBar(R.id.al_tb_title, "附近站点", null, null);
        lvNearbyStation.setAdapter(mItemAdapter);

        lvNearbyStation.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                nearbyStationRequest();
            }
        });
    }

    private void initData() {

        mDataEntityList = new ArrayList<>();
//      getLocation();
        mItemAdapter = new ItemAdapter(mContext);
        //nearbyStationRequest();
        updateWithNewLocation(null);
    }

    /**
     * 获取定位信息
     */
    public void getLocation() {

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        String provider = mLocationManager.getBestProvider(criteria, true);

        //检查权限是否开启，系统自动生成的代码
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(provider, 2000, (float)0.1, mLocationListener);
        mLocation = mLocationManager.getLastKnownLocation(provider);
        long startTime = System.currentTimeMillis();
        while (mLocation == null){//超过4秒定位失败

            mLocationManager.getLastKnownLocation(provider);
            if(System.currentTimeMillis() - startTime > 4000){

                Toast.makeText(mContext,"定位失败",Toast.LENGTH_SHORT).show();
                return;
            }
        }
        updateWithNewLocation(mLocation);
        mLocationManager.removeUpdates(mLocationListener);//定位完成后，解除监听

    }

    public void updateWithNewLocation(Location location) {

        if(location != null){
            System.out.println("----->getLongitude:" + location.getLongitude());
            System.out.println("----->getLatitude:" + location.getLatitude());
            mNearbyStationInfo.setLongitude(String.valueOf(location.getLongitude()));
            mNearbyStationInfo.setLatitude(String.valueOf(location.getLatitude()));
        }
    }

    public final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            updateWithNewLocation(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            updateWithNewLocation(null);
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    /**
     * 获取附近站点请求
     */
    public void nearbyStationRequest() {

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mNearbyStationInfo, new AbsOnRequestListener(mContext) {

            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mDataEntityList = mNearbyStationInfo.getDataEntitieList();
                if(null != mDataEntityList && mDataEntityList.size() > 0){

                    mItemAdapter.clear();
                    mItemAdapter.addItems((List)mDataEntityList);
                    mItemAdapter.notifyDataSetChanged();
                }

                lvNearbyStation.onRefreshComplete();
            }
        });
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        //定位回调
        JKLog.i(TAG, aMapLocation.toString());
        double locationLongitude = aMapLocation.getLongitude();
        double locationLatitude = aMapLocation.getLatitude();

        mNearbyStationInfo.setLongitude(String.valueOf(locationLongitude));
        mNearbyStationInfo.setLatitude(String.valueOf(locationLatitude));
        nearbyStationRequest();
    }
}
