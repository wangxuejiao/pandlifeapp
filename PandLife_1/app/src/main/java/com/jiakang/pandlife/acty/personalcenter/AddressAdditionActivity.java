package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.share.SelectProCityAreaActivity_;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.db.AreaDBManage;
import com.jiakang.pandlife.info.AddRecipientsInfo;
import com.jiakang.pandlife.info.AddSenderInfo;
import com.jiakang.pandlife.info.EditinfoAddressInfo;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.CityItem;
import com.jiakang.pandlife.item.ProvinceItem;
import com.jiakang.pandlife.utils.CustomToast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

/**
 * 添加地址
 */
@EActivity(R.layout.activity_address_addition)
public class AddressAdditionActivity extends BaseActy {
    protected static final String TAG = "AddressAdditionActivity";

    @ViewById(R.id.aaa_et_name)
    EditText nameET;
    @ViewById(R.id.aaa_et_phone)
    EditText phoneET;
    @ViewById(R.id.aaa_tv_province_select)
    TextView provinceSelectTV;
    @ViewById(R.id.aaa_tv_city_select)
    TextView citySelectTV;
    @ViewById(R.id.aaa_tv_area_select)
    TextView areaSelectTV;
    @ViewById(R.id.aaa_et_detail_address)
    EditText detailAddressET;
    @ViewById(R.id.aaa_btn_finish)
    Button finishBN;

    private AddressItem mAddressItem;

    //记录地址类型
    private String addressType;
    /** 选择省市区的类型 */
    public static final String SELECT_TYPE = "select_type";
    /** 选择省市区的传过去的值 */
    public static final String SELECT_DATA = "select_data";
    //被选中的省id
    private int province_id = 0;
    //被选中的市id
    private int city_id = 0;
    //被选中的区id
    private int zone_id = 0;

    /**添加寄件人信息接口*/
    private AddSenderInfo mAddSenderInfo = new AddSenderInfo();
    /**添加收件人信息接口*/
    private AddRecipientsInfo mAddRecipientsInfo = new AddRecipientsInfo();

    private AreaDBManage mAreaDBManage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.aaa_tb_title, "添加地址");
        mAreaDBManage = getMyApp().getAreaDBManage();
        initView();
        bindView();
    }

    private void initView(){
        provinceSelectTV.setOnClickListener(this);
        citySelectTV.setOnClickListener(this);
        areaSelectTV.setOnClickListener(this);
        finishBN.setOnClickListener(this);

    }

    private void bindView(){
        Intent intent = getIntent();
        addressType = intent.getExtras().getString(MyAddressBookActivity.ADDRESS_TYPE);

    }


    /**
     * 保存地址方法
     */
    private void saveAddress(){
        final String name = nameET.getText().toString();
        final String phone = phoneET.getText().toString();
        final String detailAddress = detailAddressET.getText().toString();
        final String province = provinceSelectTV.getText().toString();
        final String city = citySelectTV.getText().toString();
        final String area = areaSelectTV.getText().toString();
        if (TextUtils.isEmpty(name)){
            CustomToast.showToast(mContext, "姓名不能为空");
            return;
        }
        if (TextUtils.isEmpty(phone)){
            CustomToast.showToast(mContext, "手机号码不能为空");
            return;
        }
        if (("请选择所在省份").equals(province)){
            CustomToast.showToast(mContext, "省份不能为空");
            return;
        }
        if (("请选择所在城市").equals(city)){
            CustomToast.showToast(mContext, "城市不能为空");
            return;
        }
        if (("请选择所在地区").equals(area)){
            CustomToast.showToast(mContext, "地区不能为空");
            return;
        }
        if (TextUtils.isEmpty(detailAddress)){
            CustomToast.showToast(mContext, "详细地址不能为空");
            return;
        }

        if ((MyAddressBookActivity.SENDER_ADDRESS).equals(addressType)){
            mAddSenderInfo.setName(name);
            mAddSenderInfo.setPhone(phone);
            mAddSenderInfo.setProvince_id(province_id);
            mAddSenderInfo.setCity_id(city_id);
            mAddSenderInfo.setZone_id(zone_id);
            mAddSenderInfo.setAddress(detailAddress);
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mAddSenderInfo,new AbsOnRequestListener(mContext) {
                @Override
                public void onRequestSuccess(int stat, JSONObject jsonObject) {
                    super.onRequestSuccess(stat, jsonObject);
                    try {
                        if (stat == 1) {
                            CustomToast.showToast(mContext, "添加地址成功");
                            setResult(RESULT_OK);
                            finish();
                        } else {
                        }
                    } catch (Exception e) {

                    }
                }
            });
        }else if((MyAddressBookActivity.RECIPIENTS_ADDRESS).equals(addressType)){
            mAddRecipientsInfo.setName(name);
            mAddRecipientsInfo.setPhone(phone);
            mAddRecipientsInfo.setProvince_id(province_id);
            mAddRecipientsInfo.setCity_id(city_id);
            mAddRecipientsInfo.setZone_id(zone_id);
            mAddRecipientsInfo.setAddress(detailAddress);
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mAddRecipientsInfo,new AbsOnRequestListener(mContext) {
                @Override
                public void onRequestSuccess(int stat, JSONObject jsonObject) {
                    super.onRequestSuccess(stat, jsonObject);
                    try {
                        if (stat == 1) {
                            CustomToast.showToast(mContext, "添加地址成功");
                            setResult(RESULT_OK);
                            finish();
                        } else {
                        }
                    } catch (Exception e) {

                    }
                }
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUSET_SELECT_PROVINCE_SUCCEE) && (resultCode == RESULT_OK)){
            ProvinceItem provinceItem = (ProvinceItem)data.getExtras().get("item");
            province_id = provinceItem.getCode();
            provinceSelectTV.setText(provinceItem.getName());
            //清空市和区
            citySelectTV.setText("");
            areaSelectTV.setText("");
//            provinceCode = provinceItem.getCode();
        }
        if ((requestCode == Constant.StaticCode.REQUSET_SELECT_CITY_SUCCEE) && (resultCode == RESULT_OK)){
            CityItem cityItem = (CityItem)data.getExtras().get("item");
            city_id = cityItem.getCode();
            citySelectTV.setText(cityItem.getName());
            //清空区
            areaSelectTV.setText("");
//            cityCode = cityItem.getCode();
        }
        if ((requestCode == Constant.StaticCode.REQUSET_SELECT_AREA_SUCCEE) && (resultCode == RESULT_OK)){
            AreaItem areaItem = (AreaItem)data.getExtras().get("item");
            zone_id = areaItem.getCode();
            areaSelectTV.setText(areaItem.getName());
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left://点击返回
                finish();
                break;
            case R.id.aaa_tv_province_select://点击选择所在省份
                Intent intentProvinceSelect = new Intent(this, SelectProCityAreaActivity_.class);
                intentProvinceSelect.putExtra(SELECT_TYPE, "选择省");
                intentProvinceSelect.putExtra(SELECT_DATA, "");
                startActivityForResult(intentProvinceSelect, Constant.StaticCode.REQUSET_SELECT_PROVINCE_SUCCEE);
                break;
            case R.id.aaa_tv_city_select://点击选择所在城市
                if(province_id == 0){
                    CustomToast.showToast(mContext, "请先选择省份");
                    return;
                }else{
                    Intent intentCitySelect = new Intent(this, SelectProCityAreaActivity_.class);
                    intentCitySelect.putExtra(SELECT_TYPE, "选择市");
                    intentCitySelect.putExtra(SELECT_DATA, province_id);
                    startActivityForResult(intentCitySelect, Constant.StaticCode.REQUSET_SELECT_CITY_SUCCEE);
                }
                break;
            case R.id.aaa_tv_area_select://点击选择所在地区
                if(city_id == 0){
                    CustomToast.showToast(mContext, "请先选择城市");
                    return;
                }else {
                    Intent intentAreaSelect = new Intent(this, SelectProCityAreaActivity_.class);
                    intentAreaSelect.putExtra(SELECT_TYPE, "选择区");
                    intentAreaSelect.putExtra(SELECT_DATA, city_id);
                    startActivityForResult(intentAreaSelect, Constant.StaticCode.REQUSET_SELECT_AREA_SUCCEE);
                }
                break;
            case R.id.aaa_btn_finish://点击保存
                saveAddress();
                break;
        }
    }
}
