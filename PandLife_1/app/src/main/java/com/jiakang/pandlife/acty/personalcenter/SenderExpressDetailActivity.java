package com.jiakang.pandlife.acty.personalcenter;

import android.app.ActionBar.LayoutParams;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
//import com.example.hjk.amap_android_navi.BasicNaviActivity;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.express.ExpressDetailActivity;
import com.jiakang.pandlife.acty.homepage.AddressBookManagerActivity_;
import com.jiakang.pandlife.acty.homepage.SelectSingleDataActivity_;
import com.jiakang.pandlife.acty.service.NearbyStationActivity_;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.BagisenableInfo;
import com.jiakang.pandlife.info.ExpressDeltailInfo;
import com.jiakang.pandlife.info.GetdeliveryInfo;
import com.jiakang.pandlife.info.SendbagInfo;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.DeliveryItem;
import com.jiakang.pandlife.item.ExpressDetailItem;
import com.jiakang.pandlife.item.SendBagItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.BottomDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import greendroid.widgetww.PullToRefreshListView;

/**
 * 寄快递
 */
@EActivity(R.layout.activity_sender_express_detail)
public class SenderExpressDetailActivity extends BaseActy {

    @ViewById(R.id.it_ibn_right)
    ImageButton deleteIBN;
    @ViewById(R.id.ased_tv_express_status)
    TextView expressStatusTV;
    @ViewById(R.id.ased_tv_site_navigation)
    TextView siteNavigationTV;
    @ViewById(R.id.ased_iv_nearby_station)
    ImageView nearbyStieIV;
//    @ViewById(R.id.ased_tv_nearby_station_name)
//    TextView nearbyStieNameTV;
//    @ViewById(R.id.ased_tv_nearby_station_address)
//    TextView nearbyStieAddressTV;
//    @ViewById(R.id.ased_tv_nearby_station_phone)
//    TextView nearbyStiePhoneTV;
    @ViewById(R.id.ased_tv_send_info)
    TextView sendInfoTV;
    @ViewById(R.id.ased_rl_sender)
    RelativeLayout senderRL;
    @ViewById(R.id.ased_tv_sender_name)
    TextView senderNameTV;
    @ViewById(R.id.ased_tv_sender_phone)
    TextView senderPhoneTV;
    @ViewById(R.id.ased_tv_sender_address)
    TextView senderAddressTV;
    @ViewById(R.id.ased_tv_obtain_info)
    TextView obtainInfoTV;
    @ViewById(R.id.ased_rl_receiver)
    RelativeLayout receiverRL;
    @ViewById(R.id.ased_tv_receiver_name)
    TextView receiverNameTV;
    @ViewById(R.id.ased_tv_receiver_phone)
    TextView receiverPhoneTV;
    @ViewById(R.id.ased_tv_receiver_address)
    TextView receiverAddressTV;
    @ViewById(R.id.ased_tv_select_pay_type)
    TextView payTypeTV;
    @ViewById(R.id.ased_tv_express_type_select)
    TextView expressTypeTV;
    @ViewById(R.id.ased_tv_goods_kind_select)
    TextView goodsKindTV;
    @ViewById(R.id.ased_tv_goods_number_content)
    TextView goodsNumberTV;
    @ViewById(R.id.ased_tv_support_value_content)
    TextView supportValueTV;
    @ViewById(R.id.ased_tv_remarks_info_content)
    TextView remarksTV;
//    @ViewById(R.id.ased_iv_question)
//    ImageView questionIV;
//    @ViewById(R.id.ased_et_remarks_info)
//    EditText remarkInfoET;
//    @ViewById(R.id.ased_btn_submit)
//    Button submitBN;

    /** 记录商品数目的 */
//    private int goodsNumber = 0;
    private String payType = "f";
    private AddressItem senderAddressItem;
    private AddressItem recipientsAddressItem;

    //上一个页面传递过来的列表中寄件信息
    private SendBagItem mSendBagItem;
    //寄件信息的id
    private int id;
    //寄件详情的实体
    private ExpressDetailItem mExpressDetailItem;

    private int delivery_id;

    /**获取快递信息接口*/
    private ExpressDeltailInfo mExpressDeltailInfo = new ExpressDeltailInfo();
    /**取消寄件接口*/
    private BagisenableInfo mBagisenableInfo = new BagisenableInfo();

    public static final String SELECT_SITE = "select_site";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        Intent intent = getIntent();
        mSendBagItem = (SendBagItem)intent.getExtras().get("sendBagItem");
        id = mSendBagItem.getId();

        initTitleBar(R.id.ased_tb_title, "寄件详情");
        if (("y").equals(mSendBagItem.getIs_enable())) {
            deleteIBN.setImageDrawable(getResources().getDrawable(R.mipmap.ic_more));
            deleteIBN.setVisibility(View.VISIBLE);
            deleteIBN.setOnClickListener(this);
        }

        siteNavigationTV.setOnClickListener(this);

        initView();
    }

    private void initView(){


        mExpressDeltailInfo.setId(id);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mExpressDeltailInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    mExpressDetailItem = mExpressDeltailInfo.getmExpressDetailItem();

                    //填充数据
                    //通过是否正常和是否处理判断寄件状态并赋值
                    String bag_type = mExpressDetailItem.getBag_type();
                    String is_enable = mExpressDetailItem.getIs_enable();
                    if (("y").equals(is_enable)) {
                        if (("y").equals(bag_type)) {
                            expressStatusTV.setText("已投递");

                            siteNavigationTV.setText("查看物流");
                            siteNavigationTV.setTextColor(getResources().getColor(R.color.gray_content));
                            siteNavigationTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            siteNavigationTV.setCompoundDrawablePadding((int) Util.dip2px(mContext, 5.0f));
                        } else {
                            expressStatusTV.setText("等待投递");

                            siteNavigationTV.setText("站点导航");
                            siteNavigationTV.setTextColor(getResources().getColor(R.color.reddish_orange));
                            siteNavigationTV.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_iv_site_navigation, 0, 0, 0);
                            siteNavigationTV.setCompoundDrawablePadding((int) Util.dip2px(mContext, 5.0f));
                        }
                    } else {
                        expressStatusTV.setText("已取消");

                        siteNavigationTV.setText("站点导航");
                        siteNavigationTV.setTextColor(getResources().getColor(R.color.reddish_orange));
                        siteNavigationTV.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_iv_site_navigation, 0, 0, 0);
                        siteNavigationTV.setCompoundDrawablePadding((int) Util.dip2px(mContext, 5.0f));
                    }
                    //填充寄件人和收件人信息
                    if (mExpressDetailItem.getSinfo() != null) {
                        receiverRL.setVisibility(View.VISIBLE);
                        receiverNameTV.setText(mExpressDetailItem.getSinfo().getName());
                        receiverPhoneTV.setText(mExpressDetailItem.getSinfo().getPhone());
                        receiverAddressTV.setText(mExpressDetailItem.getSinfo().getAddress());
                    }
                    if (mExpressDetailItem.getJinfo() != null) {
                        senderRL.setVisibility(View.VISIBLE);
                        senderNameTV.setText(mExpressDetailItem.getJinfo().getName());
                        senderPhoneTV.setText(mExpressDetailItem.getJinfo().getPhone());
                        senderAddressTV.setText(mExpressDetailItem.getJinfo().getAddress());
                    }
                    //填充付款方式
                    if (("d").equals(mExpressDetailItem)) {
                        payTypeTV.setText("到付");
                    } else {
                        payTypeTV.setText("寄付");
                    }
                    //填充物品种类
                    goodsKindTV.setText(mExpressDetailItem.getBag_kind());
                    //填充寄件类型
                    expressTypeTV.setText(mExpressDetailItem.getDelivery_name());
                    //填充物品数量
                    goodsNumberTV.setText(mExpressDetailItem.getNums() + "");
                    //填充保价金额
                    supportValueTV.setText(mExpressDetailItem.getSupvalue() + "");
                    //填充备注
                    remarksTV.setText(mExpressDetailItem.getRemarks());
                } catch (Exception e) {
                    JKLog.e(TAG, "onRequestSuccess方法中-------->：");
                }
            }
        });
    }


    /**
     * 删除寄件的方法
     */
    private void deleteExpress(){
        View view = LayoutInflater.from(this).inflate(R.layout.dlg_delete_cancel, null);
        TextView titleTV = (TextView)view.findViewById(R.id.dsirj_tv_title);
        Button deleteBN = (Button)view.findViewById(R.id.dsirj_btn_yes);
        Button cancelBN = (Button)view.findViewById(R.id.dsirj_btn_no);
        titleTV.setText("包裹尚未投递，删除将取消寄件");
        deleteBN.setText("删除");
        deleteBN.setTextColor(getResources().getColor(R.color.reddish_orange));
        cancelBN.setText("取消");
        cancelBN.setTextColor(getResources().getColor(R.color.blue));
        final BottomDialog bottomDialog = new BottomDialog(view, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        bottomDialog.show();
        deleteBN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
                mBagisenableInfo.setId(mExpressDetailItem.getId());
                ApiManager apiManager = ApiManager.getInstance();
                apiManager.request(mBagisenableInfo, new AbsOnRequestListener(mContext) {
                    @Override
                    public void onRequestSuccess(int result, JSONObject jsonObject) {
                        super.onRequestSuccess(result, jsonObject);
                        try {
                            CustomToast.showToast(mContext, "删除成功");
                            setResult(RESULT_CANCELED);
                            finish();
                        } catch (Exception e) {
                            JKLog.e(TAG, "onRequestSuccess方法中-------->：");
                        }
                    }
                });
            }
        });
        cancelBN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomDialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.it_ibn_right://点击可删除寄件
                deleteExpress();
                break;
            case R.id.ased_tv_site_navigation://点击站点导航或者查看物流
                if (("查看物流").equals(siteNavigationTV.getText().toString())){
                    //跳转物流页
                    Intent intentNavigation = new Intent(mContext, CheckLogisticsActivity_.class);
                    intentNavigation.putExtra("sendBagItem", mSendBagItem);
                    startActivity(intentNavigation);
                }else {
                    //跳转导航页，先调用定位方法
                    startLocation();
                }
                break;
        }
    }

    /**
     * 进入导航的方法
     */
    private void toNavigation(double locationLongitude, double locationLatitude){
//        Intent intentNavigation = new Intent(this, BasicNaviActivity.class);
//
//        intentNavigation.putExtra(BasicNaviActivity.END_LONGITUDE, Double.parseDouble(mSendBagItem.getLongitude()));
//        intentNavigation.putExtra(BasicNaviActivity.END_LATITUDE, Double.parseDouble(mSendBagItem.getLatitude()));
//        intentNavigation.putExtra(BasicNaviActivity.START_LONGITUDE, locationLongitude);
//        intentNavigation.putExtra(BasicNaviActivity.START_LATITUDE, locationLatitude);
//        startActivityForResult(intentNavigation, Constant.StaticCode.REQUEST_SENDER_ADDRESS);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        //定位回调
        JKLog.i(TAG, aMapLocation.toString());
        double locationLongitude = aMapLocation.getLongitude();
        double locationLatitude = aMapLocation.getLatitude();
        toNavigation(locationLongitude, locationLatitude);
    }
}
