package com.jiakang.pandlife.info;

import android.util.Log;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.item.StationDetail;
import com.jiakang.pandlife.utils.DataCache;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by play on 2016/1/13.
 */
public class StationDetailInfo extends BaseAbsInfo {


    private int id;
    private StationDetail stationDetail;
    private StationDetail.DataEntityStationDetail dataEntityStationDetail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StationDetail getStationDetail() {
        return stationDetail;
    }

    public void setStationDetail(StationDetail stationDetail) {
        this.stationDetail = stationDetail;
    }

    public StationDetail.DataEntityStationDetail getDataEntityStationDetail() {
        return dataEntityStationDetail;
    }

    public void setDataEntityStationDetail(StationDetail.DataEntityStationDetail dataEntityStationDetail) {
        this.dataEntityStationDetail = dataEntityStationDetail;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Server&a=Getstationinfo&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        Log.i("---->jsonObject:", jsonObject.toString());
        stationDetail = BaseInfo.gson.fromJson(jsonObject.toString(), StationDetail.class);
        dataEntityStationDetail = stationDetail.getData();
        DataCache dataCache = DataCache.get(PandLifeApp.getInstance());
        String key = "dataEntityStationDetail";
        dataCache.put(key,jsonObject,10 * 60);
    }
}
