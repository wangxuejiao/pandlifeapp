package com.jiakang.pandlife.acty;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.Post.PostTypeActivity;
import com.jiakang.pandlife.acty.fragment.HomePageFragment;
import com.jiakang.pandlife.acty.fragment.NeighbourhoodFragment;
import com.jiakang.pandlife.acty.fragment.PersonalCenterFragment;
import com.jiakang.pandlife.acty.fragment.ServiceFragment;
import com.jiakang.pandlife.receiver.JpushReceiver;
import com.jiakang.pandlife.service.DownloadService;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.CustomDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import cn.jpush.android.api.JPushInterface;

/**
 * 主页 ： 四个标签+fragment
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActy {
    protected static final String TAG = "MainActivity";

    @ViewById(R.id.am_rgp_tab)
    RadioGroup labelRG;
    @ViewById(R.id.am_rbn_homepage)
    RadioButton homePageRBN;
    @ViewById(R.id.am_rbn_service)
    RadioButton serviceRBN;
    @ViewById(R.id.am_rbn_neighbourhood)
    RadioButton neighbourhoodRBN;
    @ViewById(R.id.am_rbn_personalcenter)
    RadioButton personalCenterRBN;
    @ViewById(R.id.am_framelayout)
    FrameLayout frameLayoutFL;

    private HomePageFragment mHomePageFragment;
    private ServiceFragment mServiceFragment;
    private NeighbourhoodFragment mNeighbourhoodFragment;
    private PersonalCenterFragment mPersonalCenterFragment;
    private FragmentManager fragmentManager;

    public static MainActivity mMainActivity;
    /**
     * 是否已退出
     */
    public static boolean isExit = false;
    private String newVersionPath;
    private DownloadService service;
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder iBinder) {
            DownloadService.DownloadBinder serviceBinder = (DownloadService.DownloadBinder) iBinder;
            service = (DownloadService) serviceBinder.getService();
            // 下载apk
            service.downNewFile(newVersionPath, "熊猫生活");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            service = null;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        JPushInterface.onResume(mContext);
    }

    @Override
    protected void onPause() {
        super.onPause();
        JPushInterface.onPause(mContext);
    }

    @AfterViews
    protected void initVar() {
        initView();
    }

    private void initView() {
        //默认首页是找工作
        Resources resource = (Resources) getBaseContext().getResources();
        final ColorStateList blue = (ColorStateList) resource.getColorStateList(R.color.reddish_orange);
        final ColorStateList gray = (ColorStateList) resource.getColorStateList(R.color.gray);
        this.fragmentManager = this.getSupportFragmentManager();
        labelRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.am_rbn_homepage://选中首页
                        homePageRBN.setTextColor(blue);
                        serviceRBN.setTextColor(gray);
                        neighbourhoodRBN.setTextColor(gray);
                        personalCenterRBN.setTextColor(gray);
                        setTabSelection(0);
                        break;
                    case R.id.am_rbn_service://选中服务
                        serviceRBN.setTextColor(blue);
                        homePageRBN.setTextColor(gray);
                        neighbourhoodRBN.setTextColor(gray);
                        personalCenterRBN.setTextColor(gray);
                        setTabSelection(1);
                        break;
                    case R.id.am_rbn_neighbourhood://选中邻里
                        neighbourhoodRBN.setTextColor(blue);
                        homePageRBN.setTextColor(gray);
                        serviceRBN.setTextColor(gray);
                        personalCenterRBN.setTextColor(gray);
                        setTabSelection(2);
                        break;
                    case R.id.am_rbn_personalcenter://选中我的
                        personalCenterRBN.setTextColor(blue);
                        homePageRBN.setTextColor(gray);
                        serviceRBN.setTextColor(gray);
                        neighbourhoodRBN.setTextColor(gray);
                        setTabSelection(3);
                        break;
                }
            }
        });

        setTabSelection(0);
    }

    void setTabSelection(int index) {
        FragmentTransaction transaction = this.fragmentManager.beginTransaction();
        hideFragment(transaction);
        JKLog.e(TAG, "transaction-------------->" + transaction);
        switch (index) {
            case 0:
                if (this.mHomePageFragment == null) {
                    this.mHomePageFragment = new HomePageFragment();
                    transaction.add(R.id.am_framelayout, this.mHomePageFragment);
                } else {
                    transaction.show(this.mHomePageFragment);
                }
                break;
            case 1:
                if (this.mServiceFragment == null) {
                    this.mServiceFragment = new ServiceFragment();
                    transaction.add(R.id.am_framelayout, this.mServiceFragment);
                } else {
                    transaction.show(this.mServiceFragment);
                }
                break;
            case 2:
                if (this.mNeighbourhoodFragment == null) {
                    this.mNeighbourhoodFragment = new NeighbourhoodFragment();
                    transaction.add(R.id.am_framelayout, this.mNeighbourhoodFragment);
                } else {
                    transaction.show(this.mNeighbourhoodFragment);
                }
                break;
            case 3:
                if (this.mPersonalCenterFragment == null) {
                    this.mPersonalCenterFragment = new PersonalCenterFragment();
                    transaction.add(R.id.am_framelayout, this.mPersonalCenterFragment);
                } else {
                    transaction.show(this.mPersonalCenterFragment);
                }

                //暂不知产品如何设计，先注释掉
//                if(LogoActivity.isOrNotLogin == false){
//                    if(this.myCentreUnLoginFragment == null){
//                        this.myCentreUnLoginFragment = new MyCentreUnLoginFragment();
//                        transaction.add(R.id.am_framelayout, this.myCentreUnLoginFragment);
//                    }else{
//                        transaction.show(this.myCentreUnLoginFragment);
//                    }
//                }else{
//                    if(this.myCentreLoginFragment == null){
//                        this.myCentreLoginFragment = new MyCentreLoginFragment();
//                        transaction.add(R.id.am_framelayout, this.myCentreLoginFragment);
//                    }else{
//                        transaction.show(this.myCentreLoginFragment);
//                    }
//                }
                break;

        }
        transaction.commit();
    }

    void hideFragment(FragmentTransaction transaction) {
        if (this.mHomePageFragment != null) {
            transaction.hide(this.mHomePageFragment);
        }
        if (this.mServiceFragment != null) {
            transaction.hide(this.mServiceFragment);
        }
        if (this.mNeighbourhoodFragment != null) {
            transaction.hide(this.mNeighbourhoodFragment);
        }
        if (this.mPersonalCenterFragment != null) {
            transaction.hide(this.mPersonalCenterFragment);
        }


        //暂不知产品如何设计，先注释掉
//        if(this.myCentreUnLoginFragment!=null){
//            transaction.hide(this.myCentreUnLoginFragment);
//        }

//        if((LogoActivity.isOrNotLogin == false) && (this.myCentreLoginFragment!=null)){
//            transaction.remove(this.myCentreLoginFragment);
//        }else{
//            if(this.myCentreLoginFragment!=null){
//                transaction.hide(this.myCentreLoginFragment);
//            }
//        }

    }

    private long firstTime;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                long secondTime = System.currentTimeMillis();
                if (secondTime - firstTime > 1500) {// 如果两次按键时间间隔大于1500毫秒，则退出
                    CustomToast.showToast(mContext, "双击退出");
                    firstTime = secondTime;// 更新firstTime

                    return true;
                } else {
                    // 退出
//				stopService();
                    //退出后再进入是刚刚登录
//                    IMData.isJustNowLogin = true;
                    PandLifeApp.getInstance().exitApp(mContext, false, false, false);
//				JobHuntingApp.getInstance().exitApp(mContext);
                }
                try {
//                    unregisterReceiver(receiver);
                } catch (Exception e) {
                }
                return true;
            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 取消注册对话框
     */
    private void showExitDlg() {
        final CustomDialog customDialog = Util.getDialog(mContext, "提示", "确定退出吗？", "确定", "取消");
        customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
//                UserItem userItem = PandLifeApp.getInstance().getUserItem();
//                // 是否退出：0不退出，1已经退出（需要重新登录）
//                userItem.setIsExit("1");
//                // 需要重新登录
//                PandLifeApp.getInstance().setUserItem(userItem);
                PandLifeApp.getInstance().exitApp(mContext);
            }
        });
        customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.show();
    }

    /**
     * 手动显示软件更新对话框
     */
    public void showNoticeDialog(String description) {
        final CustomDialog customDialog = Util.getDialog(mContext, "软件更新", description, "立即升级", "暂不升级");
        TextView contentTV = (TextView) customDialog.findViewById(R.id.message);
        contentTV.setGravity(Gravity.LEFT);
        customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
                // 显示下载对话框
                Intent intent = new Intent(mContext, DownloadService.class);
                startService(intent);
                // 绑定下载服务
                bindService(intent, connection, Context.BIND_AUTO_CREATE);
            }
        });
        customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.show();

    }

    /**
     * 强制更新对话框
     */
    private CustomDialog strongUpdateDlg;

    public void showStrongUpdate(String description) {
        strongUpdateDlg = Util.getDialog(mContext, "软件更新", description, "立即升级", "退出程序");
        strongUpdateDlg.setCancelable(false);
        TextView contentTV = (TextView) strongUpdateDlg.findViewById(R.id.message);
        contentTV.setGravity(Gravity.LEFT);
        strongUpdateDlg.setOnClickListener(R.id.ok, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // strongUpdateDlg.dismiss();
                // 显示下载对话框
                Intent intent = new Intent(mContext, DownloadService.class);
                startService(intent);
                // 绑定下载服务
                bindService(intent, connection, Context.BIND_AUTO_CREATE);
            }
        });
        strongUpdateDlg.setOnClickListener(R.id.cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strongUpdateDlg.dismiss();
                // 退出程序
                PandLifeApp.getInstance().exitApp(mContext);
            }
        });
        strongUpdateDlg.show();

    }

    public void jumpToNeighbourFragement(){

        Resources resource = (Resources) getBaseContext().getResources();
        ColorStateList blue = (ColorStateList) resource.getColorStateList(R.color.reddish_orange);
        ColorStateList gray = (ColorStateList) resource.getColorStateList(R.color.gray);
        this.fragmentManager = this.getSupportFragmentManager();

        neighbourhoodRBN.setTextColor(blue);
        homePageRBN.setTextColor(gray);
        serviceRBN.setTextColor(gray);
        personalCenterRBN.setTextColor(gray);

        homePageRBN.setChecked(false);
        neighbourhoodRBN.setChecked(true);
        setTabSelection(2);
    }
}
