package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import android.content.SharedPreferences;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.MD5Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 1.1登陆接口
 *
 * @author ww
 *
 */
public class LoginInfo extends BaseAbsInfo {

    private static final String TAG = "LoginInfo";
    private String mUserName = null;
    private String mPassword = null;
    /** 1表示andriod,2表示iphone */
    protected String loginType = "0";

    private UserItem userItem;

    private SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Auth&a=Login" + "&subtime=" + System.currentTimeMillis();
//        return "http://192.168.1.2/proj/api/user/login";
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("mobile", mUserName);
            json.put("password", MD5Util.encodeByMD5(mPassword));
//            json.put("system", loginType);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            data = (JSONObject) jsonObject.get("data");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                BaseInfo.token = data.getString("token");

                //将用户登录密码缓存
                final SharedPreferences.Editor editor = myAccount.edit();
                editor.putString(Constant.Spf.TOKEN, BaseInfo.token);
                editor.commit();
//                CustomImageToast.showToast(PandLifeApp.getInstance(), info);
//                userItem = BaseInfo.gson.fromJson(jsonObject.toString(), UserItem.class);
                // 保存用户实体
//                MyJobApp.getInstance().setMemberItem(memberItem);
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getmUserName() {
        return mUserName;
    }

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public UserItem getUserItem() {
        return userItem;
    }

    public void setUserItem(UserItem userItem) {
        this.userItem = userItem;
    }
}
