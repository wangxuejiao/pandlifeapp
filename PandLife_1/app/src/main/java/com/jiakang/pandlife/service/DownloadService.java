package com.jiakang.pandlife.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.utils.JKLog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class DownloadService extends Service {

	private final int NOTIFICATIONID = 1024;

	private NotificationManager nm;
	private Notification notification;
	private boolean update = true;
	private ExecutorService executorService = Executors.newFixedThreadPool(5); // 固定五个线程来执行任务
	public Map<Integer, Integer> download = new HashMap<Integer, Integer>();
	public Context mContext;
	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			PendingIntent contentIntent = null;
			super.handleMessage(msg);
			if (msg != null) {
				switch (msg.what) {
				case 0:
					Toast.makeText(mContext, msg.obj.toString(), Toast.LENGTH_SHORT).show();
					download.remove(msg.arg1);
					break;
				case 2:
					Notification.Builder builder = new Notification.Builder(mContext).setTicker(msg.getData().getString("name") + "下载完成")
							.setSmallIcon(R.mipmap.ic_launcher);
					contentIntent = PendingIntent.getService(DownloadService.this, msg.arg1, new Intent(DownloadService.this, DownloadService.class), 0);
//					notification.setLatestEventInfo(DownloadService.this, msg.getData().getString("name") + "下载完成", "100%", contentIntent); // 该方法已弃用
					notification = builder.setContentIntent(contentIntent).setContentTitle( msg.getData().getString("name") + "下载完成").setContentText("100%").build();

					nm.notify(msg.arg1, notification);
					// 下载完成后清除所有下载信息，执行安装提示
					download.remove(msg.arg1);
					nm.cancel(msg.arg1);
					// 安装APK文件
					installApk();
					break;
				case 3:
					contentIntent = PendingIntent.getService(DownloadService.this, msg.arg1, new Intent(DownloadService.this, DownloadService.class), 0);
					notification.contentView.setTextViewText(R.id.nd_tv_name, msg.getData().getString("name") + "正在下载");
					notification.contentView.setTextViewText(R.id.nd_tv_rate, download.get(msg.arg1) + "%");
					notification.contentView.setProgressBar(R.id.nd_progress_rate, 100, download.get(msg.arg1), false);
					// 更新
					nm.notify(msg.arg1, notification);
					break;
				case 4:
					Toast.makeText(mContext, msg.obj.toString(), Toast.LENGTH_SHORT).show();
					download.remove(msg.arg1);
					nm.cancel(msg.arg1);
					break;
				}
			}
		}
	};

	public class DownloadBinder extends Binder {
		public Service getService() {
			return DownloadService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return new DownloadBinder();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		JKLog.e("DownloadService", "DownloadService--------->创建了");
		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		mContext = this;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		JKLog.e("DownloadService", "onStartCommand--------->");
		return START_REDELIVER_INTENT;
	}

	@Override
	public void onDestroy() {
		JKLog.e("DownloadService", "onDestroy--------->");
		super.onDestroy();
	}

	public void downNewFile(final String url, final String name) {
		if (download.containsKey(NOTIFICATIONID))
			return;
		notification = new Notification();
		notification.icon = android.R.drawable.stat_sys_download;
		// notification.icon=android.R.drawable.stat_sys_download_done;
		notification.tickerText = name + "开始下载";
		notification.when = System.currentTimeMillis();
		notification.defaults = Notification.DEFAULT_LIGHTS;
		// 显示在“正在进行中”
		notification.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
		// PendingIntent contentIntent = PendingIntent.getActivity(context,
		// NOTIFICATIONID, new Intent(context, DownloadTest.class), 0);
		PendingIntent contentIntent = PendingIntent.getService(mContext, NOTIFICATIONID, new Intent(mContext, DownloadService.class), 0);
		// 布局
		notification.contentView = new RemoteViews(mContext.getPackageName(), R.layout.notification_download);
		// notification.setLatestEventInfo(mContext, name, "0%", contentIntent);
		download.put(NOTIFICATIONID, 0);
		// notification.contentView.setImageViewResource(R.id.nd_iv_logo,
		// R.drawable.logo_marthmake);
		notification.contentIntent = contentIntent;
		// 将下载任务添加到任务栏中
		nm.notify(NOTIFICATIONID, notification);
		// 启动线程开始执行下载任务
		downFile(url, NOTIFICATIONID, name);
	}

	/* 下载保存路径 */
	private String mSavePath;

	/* 记录进度条数量 */
	// private int progress;

	// 下载更新文件
	private void downFile(final String fileUrl, final int NOTIFICATIONID, final String name) {
		executorService.execute(new Runnable() {
			@Override
			public void run() {
				// 判断SD卡是否存在，并且是否具有读写权限
				if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
					// 获得存储卡的路径
					String sdpath = Environment.getExternalStorageDirectory() + "/";
					mSavePath = sdpath + "download";
				} else {
					mSavePath = mContext.getFilesDir().getAbsolutePath();
				}
				try {

					URL url = new URL(fileUrl);
					// 创建连接
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.connect();
					// 获取文件大小
					int length = conn.getContentLength();
					// 创建输入流
					InputStream is = conn.getInputStream();

					File file = new File(mSavePath);
					// 判断文件目录是否存在
					if (!file.exists()) {
						file.mkdir();
					}
					File apkFile = new File(mSavePath, BaseInfo.newVersionID + ".apk");
					FileOutputStream fos = new FileOutputStream(apkFile);
					int count = 0;
					int precent = 0;
					// 缓存
					byte buf[] = new byte[1024];
					// 写入到文件中
					do {
						int numread = is.read(buf);
						count += numread;
						// 计算进度条位置
						// progress = (int) (((float) count / length) * 100);
						// 更新进度

						precent = (int) (((double) count / length) * 100);
						// 每下载完成1%就通知任务栏进行修改下载进度
						if (precent - download.get(NOTIFICATIONID) >= 1) {
							download.put(NOTIFICATIONID, precent);
							Message message = mHandler.obtainMessage(3, precent);
							Bundle bundle = new Bundle();
							bundle.putString("name", name);
							message.setData(bundle);
							message.arg1 = NOTIFICATIONID;
							mHandler.sendMessage(message);
						}
						// 写入文件
						if (numread > -1) {
							fos.write(buf, 0, numread);
						} else {
							break;
						}
					} while (update);// 点击取消就停止下载.
					fos.flush();
					fos.close();
					is.close();

					if (update) {
						Message message = mHandler.obtainMessage(2, "医林客");
						message.arg1 = NOTIFICATIONID;
						Bundle bundle = new Bundle();
						bundle.putString("name", name);
						message.setData(bundle);
						mHandler.sendMessage(message);
					}
					//ClientProtocolException 该类已弃用
//				} catch (ClientProtocolException e) {
//					Message message = mHandler.obtainMessage(4, name + "下载失败：网络异常！");
//					message.arg1 = NOTIFICATIONID;
//					mHandler.sendMessage(message);
				} catch (IOException e) {
					Message message = mHandler.obtainMessage(4, name + "下载失败：文件传输异常");
					message.arg1 = NOTIFICATIONID;
					mHandler.sendMessage(message);
				}
			}
		});
	}

	// 安装下载后的apk文件
	@Deprecated
	private void Instanll(File file, Context context) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
		context.startActivity(intent);
	}

	/**
	 * 安装APK文件
	 */
	private void installApk() {
		File apkfile = new File(mSavePath, BaseInfo.newVersionID + ".apk");
		if (!apkfile.exists()) {
			return;
		}
		// 通过Intent安装APK文件
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.setAction(Intent.ACTION_VIEW);
		i.setDataAndType(Uri.parse("file://" + apkfile.toString()), "application/vnd.android.package-archive");
		mContext.startActivity(i);
	}

}
