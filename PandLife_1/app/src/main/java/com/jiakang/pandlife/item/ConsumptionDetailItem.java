package com.jiakang.pandlife.item;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.annotation.sqlite.Id;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.db.DataCacheDBHelper;

/**
 * 消费明细实体
 * Created by Administrator on 2016/1/11.
 */
@Table(name = DataCacheDBHelper.TAB_CONSUMPTION_DETAIL_CACHE)
public class ConsumptionDetailItem extends Item{

    /** 数据库id	Int(10) */
    @Id
    private int _id;
    /** 编号id	Int(10) */
    private int id;
    /** 订单编号	Varchar(32) */
    private String orderid;
    /** 用户id	Int(10) */
    private int mid;
    /** 消费名称	Varchar(32) */
    private String title;
    /** 消费金额	Int(10) */
    private float money;
    /** 支付类型（account，weixin，alipay，bank）	Varchar */
    private String type;
    /** 余额账户剩余金额	Int(10) */
    private float endmoney;
    /** 订单创建时间	Varchar(32) */
    private String createtime;
    /** 是否成功（失败：n  成功：y）	Varchar(32) */
    private String is_success;
    /** 更新时间	Varchar(32) */
    private String updatetime;

    /** 插入数据库时的时间 */
    private long currentTime = System.currentTimeMillis();
    /** 保存的时间 */
    private long saveTime = 86400000;

    private int layout = R.layout.item_consumption_detail;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    @Override
    public int insert() {
        PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
        return 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getEndmoney() {
        return endmoney;
    }

    public void setEndmoney(float endmoney) {
        this.endmoney = endmoney;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getIs_success() {
        return is_success;
    }

    public void setIs_success(String is_success) {
        this.is_success = is_success;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public long getSaveTime() {
        return saveTime;
    }

    public void setSaveTime(long saveTime) {
        this.saveTime = saveTime;
    }
}
