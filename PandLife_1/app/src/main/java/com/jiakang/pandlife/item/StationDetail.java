package com.jiakang.pandlife.item;

/**
 * Created by play on 2016/1/13.
 */
public class StationDetail {


    /**
     * data : {"address":"U谷5#102室熊猫快收12530","phone":"15156097071","latitude":"31.868279","nice_name":"熊猫快收12530U谷未来谷店铺","longitude":"118.829785"}
     * status : 1
     * info : 站点信息获取成功！
     */
    private DataEntityStationDetail data;
    private int status;
    private String info;

    public void setData(DataEntityStationDetail data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public DataEntityStationDetail getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }

    public class DataEntityStationDetail {
        /**
         * address : U谷5#102室熊猫快收12530
         * phone : 15156097071
         * latitude : 31.868279
         * nice_name : 熊猫快收12530U谷未来谷店铺
         * longitude : 118.829785
         */
        private String address;
        private String phone;
        private String latitude;
        private String nice_name;
        private String longitude;

        public void setAddress(String address) {
            this.address = address;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public void setNice_name(String nice_name) {
            this.nice_name = nice_name;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getAddress() {
            return address;
        }

        public String getPhone() {
            return phone;
        }

        public String getLatitude() {
            return latitude;
        }

        public String getNice_name() {
            return nice_name;
        }

        public String getLongitude() {
            return longitude;
        }
    }
}
