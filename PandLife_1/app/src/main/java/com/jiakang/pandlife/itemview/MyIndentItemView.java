package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.GoodsItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.MyIndentItem;

/**
 * Created by play on 2016/1/6.
 */
public class MyIndentItemView extends AbsRelativeLayout {

    private ImageView iconIV;
    private TextView titleTV;
    private TextView statusTV;
    private LinearLayout listLL;
    private ListView listLV;
    private TextView countTV;
    private TextView remarksTV;
    private Button button1BN;
    private Button button2BN;
    private Button button3BN;

    private ItemAdapter mItemAdapter;

    public MyIndentItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyIndentItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();
        iconIV = (ImageView) findViewById(R.id.imi_iv_logo);
        titleTV = (TextView) findViewById(R.id.imi_tv_site_name);
        statusTV = (TextView) findViewById(R.id.imi_tv_status);
        listLL = (LinearLayout) findViewById(R.id.imi_ll_goods);
        listLV = (ListView) findViewById(R.id.imi_clv_goods);
        countTV = (TextView) findViewById(R.id.imi_tv_count);
        remarksTV = (TextView) findViewById(R.id.imi_tv_remarks);
        button1BN = (Button) findViewById(R.id.imi_btn_button1);
        button2BN = (Button) findViewById(R.id.imi_btn_button2);
        button3BN = (Button) findViewById(R.id.imi_btn_button3);

        mItemAdapter = new ItemAdapter(PandLifeApp.getInstance());
        listLV.setAdapter(mItemAdapter);
    }

    @Override
    public void setObject(final Item item, final int position,final ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        if(item instanceof MyIndentItem){
            final MyIndentItem myIndentItem = (MyIndentItem) item;
            titleTV.setText(myIndentItem.getStationname());
            String order_type = myIndentItem.getOrder_type();
            if (("1").equals(order_type)){
                statusTV.setText("待付款");

                button1BN.setText("去付款");
                button1BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg_reddish_orange));
                button1BN.setTextColor(getResources().getColor(R.color.reddish_orange));

                button2BN.setText("取消订单");
                button2BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
                button2BN.setTextColor(getResources().getColor(R.color.gray_content));

                button3BN.setVisibility(View.GONE);
//                button3BN.setText("去付款");
//                button3BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
//                button3BN.setTextColor(getResources().getColor(R.color.gray_content));

                remarksTV.setVisibility(View.GONE);
            }else if(("2").equals(order_type)){
                statusTV.setText("已付款");

                button1BN.setVisibility(View.VISIBLE);
                button1BN.setText("申请退款");
                button1BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg_reddish_orange));
                button1BN.setTextColor(getResources().getColor(R.color.reddish_orange));

                button2BN.setVisibility(View.GONE);
                button2BN.setText("提醒发货");
                button2BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
                button2BN.setTextColor(getResources().getColor(R.color.gray_content));

                button3BN.setVisibility(View.GONE);
                remarksTV.setVisibility(View.GONE);
            }else if(("3").equals(order_type)){
                statusTV.setText("已成团");

                button1BN.setVisibility(View.VISIBLE);
                button1BN.setText("申请退款");
                button1BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg_reddish_orange));
                button1BN.setTextColor(getResources().getColor(R.color.reddish_orange));

                button2BN.setVisibility(View.GONE);
                button2BN.setText("物流追踪");
                button2BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
                button2BN.setTextColor(getResources().getColor(R.color.gray_content));

                button3BN.setVisibility(View.GONE);
                remarksTV.setVisibility(View.GONE);
            }else if(("4").equals(order_type)){
                statusTV.setText("交易成功");
                button1BN.setVisibility(View.VISIBLE);
                button1BN.setText("售后电话");
                button1BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg_reddish_orange));
                button1BN.setTextColor(getResources().getColor(R.color.reddish_orange));

                button2BN.setVisibility(View.GONE);
                button2BN.setText("物流追踪");
                button2BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
                button2BN.setTextColor(getResources().getColor(R.color.gray_content));

                button3BN.setVisibility(View.VISIBLE);
                button3BN.setText("删除订单");
                button3BN.setBackground(getResources().getDrawable(R.drawable.shape_tv_border_bg));
                button3BN.setTextColor(getResources().getColor(R.color.gray_content));

                remarksTV.setVisibility(View.GONE);
            }else if (("6").equals(order_type)){
                statusTV.setText("退款中");
                remarksTV.setVisibility(View.VISIBLE);
                remarksTV.setText(myIndentItem.getRemarks());
                button1BN.setVisibility(View.GONE);
                button2BN.setVisibility(View.GONE);
                button3BN.setVisibility(View.GONE);
            }else if (("7").equals(order_type)){
                statusTV.setText("退款成功");
                remarksTV.setVisibility(View.VISIBLE);
                remarksTV.setText(myIndentItem.getRemarks());
                button1BN.setVisibility(View.GONE);
                button2BN.setVisibility(View.GONE);
                button3BN.setVisibility(View.GONE);
            }
            int count = myIndentItem.getNumber() * myIndentItem.getDanjia() + myIndentItem.getPs_money();
            countTV.setText("共" + myIndentItem.getNumber() + "件 合计￥" + count + " （含运费￥" + myIndentItem.getPs_money() + "）");

            //点击站点或商家进行导航
            titleTV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    myIndentItem.setClickStr("navigation");
                    v.setTag(myIndentItem);
                    onViewClickListener.onViewClick(v, position);
                }
            });
            //点击button1
            button1BN.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    myIndentItem.setClickStr("button1");
                    v.setTag(myIndentItem);
                    onViewClickListener.onViewClick(v, position);
                }
            });
            //点击button2
            button2BN.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    myIndentItem.setClickStr("button2");
                    v.setTag(myIndentItem);
                    onViewClickListener.onViewClick(v, position);
                }
            });
            //点击button3
            button3BN.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    myIndentItem.setClickStr("button3");
                    v.setTag(myIndentItem);
                    onViewClickListener.onViewClick(v, position);
                }
            });
            //点击商品列表表示点击了item条目
            listLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    myIndentItem.setClickStr("item");
                    view.setTag(myIndentItem);
                    onViewClickListener.onViewClick(view, position);
                }
            });

            mItemAdapter.clear();
            GoodsItem goodsItem = new GoodsItem();
            goodsItem.setName(myIndentItem.getGoodtitle());
            goodsItem.setContent(myIndentItem.getRemarks());
            goodsItem.setNumber(myIndentItem.getNumber());
            goodsItem.setPrice(myIndentItem.getDanjia());
            if (!TextUtils.isEmpty(myIndentItem.getGoodpic())){
                goodsItem.setPic(myIndentItem.getGoodpic());
            }else if (!TextUtils.isEmpty(myIndentItem.getGpic())){
                goodsItem.setPic(myIndentItem.getGpic());
            }else if (!TextUtils.isEmpty(myIndentItem.getPic())){
                goodsItem.setPic(myIndentItem.getPic());
            }
            mItemAdapter.add(goodsItem);
            mItemAdapter.notifyDataSetChanged();
        }
    }
}
