package com.jiakang.pandlife;

/**
 * @author：xuwangwang E-mail: 541765907@qq.com
 * @date：2014-5-11 下午9:30:40
 * @version 1.0 接口测试页： http://app.hair8.net/app.php
 */
public class Constant {

	public static boolean mDebug = true;

//	public static String mWebAddress = "http://192.168.0.22:8080/";
	public static String mWebAddress = "http://api.panda-life.cn/index.php?";
	public static String downalImageUrl = "";
	public static int MSG_TIME_OUT_Length = 30000;
	
	
//	public static String mWebAddress = "http://192.168.0.22:8080/";
//	public static String downalImageUrl = "";
//	public static int MSG_TIME_OUT_Length = 30000;

	public static final class ActyForResultID {
		/** 选择国家，非中国 */
		public static final int ACTIVITY_SELECT_COUNTRY = 0x00001114;
		/** 选择国家、省、城市 */
		public static final int ACTIVITY_SELECT_CITY = 0x00001115;
	}

	public static final class BroadAction {
		/** 登陆成功 */
		public static final String BA_LOGIN_OK = "ba_login_ok";
		/** */

	}

	public static final class StaticCode{
		/** 登录的静态值 */
		public static int REQUSET_LOGIN_SUCCEE = 1000;
		/** 注册的静态值 */
		public static int REQUSET_REGISTER_SUCCEE = 990;
		/** 注册后绑定小区的静态值 */
		public static int REQUSET_BIND_COMMUNITY = 987;
		/** 注册后绑定站点的静态值 */
		public static int REQUSET_BIND_SITE = 985;
		/** 选择省份的静态值 */
		public static int REQUSET_SELECT_PROVINCE_SUCCEE = 980;
		/** 选择城市的静态值 */
		public static int REQUSET_SELECT_CITY_SUCCEE = 950;
		/** 选择地区的静态值 */
		public static int REQUSET_SELECT_AREA_SUCCEE = 970;
		/** 修改个人资料的静态值 */
		public static int REQUEST_CHANGED_DATA = 960;
		/** 提现的静态值 */
		public static int REQUEST_TRANSFER = 950;
		/** 选择充值方式的静态值 */
		public static int REQUEST_RECHARGE_WAY = 945;
		/** 充值的静态值 */
		public static int REQUEST_RECHARGE = 940;
		/** 消息的静态值 */
		public static int REQUEST_MESSAGE = 930;
		/** 系统消息的静态值 */
		public static int REQUEST_PERSONAL_MESSAGE = 920;
		/** 重设密码的静态值 */
		public static int REQUEST_RESET_PASSWORD = 910;
		/** 选择寄件人地址的静态值 */
		public static int REQUEST_SENDER_ADDRESS = 900;
		/** 选择收人地址的静态值 */
		public static int REQUEST_RECEIVER_ADDRESS = 890;
		/** 选择站点的静态值 */
		public static int REQUEST_SELECT_SITE = 880;
		/** 选择商品种类的静态值 */
		public static int REQUEST_SELECT_GOODS_KIND = 870;
		/** 选择商品类型的静态值 */
		public static int REQUEST_SELECT_GOODS_TYPE = 865;
		/** 进入扫描二维码的静态值 */
		public static int REQUEST_SCANNING_BARCODE = 860;
		/** 今日团购的的静态值 */
		public static int REQUEST_GROUP_PURCHASE = 850;
		/** 编辑地址的静态值 */
		public static int REQUEST_REDACT_ADDRESS = 840;
		/** 我的寄件详情的静态值 */
		public static int REQUEST_SENDER_EXPRESS_DETAIL = 830;
		/** 订单中选择红包的静态值 */
		public static int REQUEST_SELECT_RED_PACKAGE = 820;
		/** 修改用户名或昵称的静态值 */
		public static int REQUEST_CHANGE_NAME = 810;
		/** 获取相册中的图片的静态值 */
		public static int REQUEST_CODE_CAPTURE_ALBUM = 800;
		/** 获取相机中的图片的静态值 */
		public static int REQUEST_CODE_CAPTURE_CAMEIA = 790;
		/** 选择绑定第三方的静态值 */
		public static int REQUEST_SELECT_OTHER_BAND = 780;
		/** 进入老用户绑定的静态值 */
		public static int REQUEST_OLDUSER_BAND = 770;
		/** 进入设置的静态值 */
		public static int REQUEST_SETTING_LOGOUT = 760;
		/** 商家订单的静态值 */
		public static int REQUEST_MERCHANT_INDENT = 750;
	}

	public static final class Spf {
		/** MemberItem实体的字符串 */
		public static final String MEMBER = "member";
		/** 登录标记，hafa、qq、sina分别代表掌缘在线平台用户登录、腾讯平台用户、新浪平台 */
		public static final String LOGIN_FLAG = "login_flag";
		/** 登陆账号 */
		 public static final String USERNAME = "userName";
		/** 获取验证码的手机号 */
		 public static final String MOBILE = "mobile";
		/** 登陆密码 */
		 public static final String PASSWORD = "password";
		/** 登录后返回的认证码 */
		 public static final String TOKEN = "token";
		/** 第三方openId */
		public static final String OPENID = "openid";
		/** 第三方登陆是否绑定了账号 */
		public static final String hasBind = "hasbind";

		/** home页缓存 */
		public static final String HOMECACHE = "homeCache";
		/** 意见反馈缓存 */
		public static final String CACHE_FEEDBACK = "cache_feedback";
		/** 评论缓存 */
		public static final String CACHE_COMMENT = "cache_comment";

		public static final String CACHE_FROM_EMAIL = "cache_from_email";
		public static final String CACHE_FROM_PASS = "cache_from_pass";
		public static final String CACHE_TO_EMAIL = "cache_to_email";

		/** 用户头像url,不含conststing中download头地址 */
		public static final String HEAD_PIC_URL = "head_pic_url";
		/** 旧的版本号 */
		public static final String OLD_VISON = "oldVison";

		/** 聊天背景图片 */
		public static final String CHAT_BG = "bg_res";
		/** 设置中聊天背景face */
		public static final String CHAT_BG_FACE = "chat_bg_face";
		/** 高级搜索记录，搜索条件 */
		public static final String Search_MaterequestInfo = "search_materequestinfo";
		/** 发缘分信记录，缘分信要求 */
		public static final String LOVELETTER_REQUEST = "loveletter_request";

		/** 获取基础数据版本号的接口地址最新的数据 */
		public static final String BASIC_DATA_SERVER = "basicDataServer2";
		/** http服务器地址最新的数据 */
		public static final String HTTP_SERVER = "httpServer2";
		/** 文件服务器地址最新的数据 */
		public static final String FILE_SERVER = "fileServer2";
		/** 即时消息通信地址和端口号的分发接口地址最新的数据 */
		// public static final String SWITCH_IM_SERVER = "switchImServer2";
		/** 开放城市最新的数据 */
		public static final String OPEN_CITY = "openCity2";
		/** 欢迎页背景图最新的数据 */
		public static final String WELCOME_BG = "welcomeBg2";
		/** 主题背景图最新的数据 */
		public static final String THEME_BG = "themeBg2";
		/** 左侧菜单背景图最新的数据 */
		public static final String LEFT_BG = "leftBg2";
		/** 个人资料背景图最新的数据 */
		public static final String MYINFO_BG = "myInfoBg2";
		/** 他人资料背景图最新的数据 */
		public static final String OTHERINFO_BG = "otherInfoBg2";
		/** 保存设置中通知是否开启的值 true:开启，false：关闭 */
		public static final String MESSAGE_NOTIFY = "message_notify";

	}
}
