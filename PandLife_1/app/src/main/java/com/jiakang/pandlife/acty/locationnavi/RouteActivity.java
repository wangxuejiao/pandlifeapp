//package com.jiakang.pandlife.acty.locationnavi;
//
//import java.util.List;
//
//import android.app.Activity;
//import android.app.ProgressDialog;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageButton;
//
//import com.amap.api.location.AMapLocation;
//import com.amap.api.location.AMapLocationClient;
//import com.amap.api.location.AMapLocationClientOption;
//import com.amap.api.location.AMapLocationListener;
//import com.amap.api.maps.AMap;
//import com.amap.api.maps.AMap.InfoWindowAdapter;
//import com.amap.api.maps.AMap.OnInfoWindowClickListener;
//import com.amap.api.maps.AMap.OnMapClickListener;
//import com.amap.api.maps.AMap.OnMarkerClickListener;
//import com.amap.api.maps.LocationSource;
//import com.amap.api.maps.MapView;
//import com.amap.api.maps.MapsInitializer;
//import com.amap.api.maps.model.BitmapDescriptorFactory;
//import com.amap.api.maps.model.LatLng;
//import com.amap.api.maps.model.Marker;
//import com.amap.api.maps.model.MarkerOptions;
//import com.amap.api.maps.overlay.BusRouteOverlay;
//import com.amap.api.maps.overlay.DrivingRouteOverlay;
//import com.amap.api.maps.overlay.WalkRouteOverlay;
//import com.amap.api.services.core.LatLonPoint;
//import com.amap.api.services.core.PoiItem;
//import com.amap.api.services.poisearch.PoiResult;
//import com.amap.api.services.poisearch.PoiSearch;
//import com.amap.api.services.route.BusPath;
//import com.amap.api.services.route.BusRouteResult;
//import com.amap.api.services.route.DrivePath;
//import com.amap.api.services.route.DriveRouteResult;
//import com.amap.api.services.route.RouteSearch;
//import com.amap.api.services.route.WalkPath;
//import com.amap.api.services.route.WalkRouteResult;
//import com.jiakang.pandlife.R;
//import com.jiakang.pandlife.acty.BaseActy;
//import com.jiakang.pandlife.utils.CustomToast;
//
//import static com.amap.api.services.route.RouteSearch.*;
//
///**
// * AMapV2地图中简单介绍route搜索
// */
//public class RouteActivity extends BaseActy implements OnMarkerClickListener,AMapLocationListener,
//		OnMapClickListener, OnInfoWindowClickListener, InfoWindowAdapter, OnRouteSearchListener, PoiSearch.OnPoiSearchListener {
//	private AMap aMap;
//	private MapView mapView;
//
//	private AMapLocationClient mlocationClient;
//	private AMapLocationClientOption mLocationOption;
//
//	private ProgressDialog progDialog = null;// 搜索时进度条
//	private int drivingMode = DrivingDefault;// 驾车默认模式
//	private int walkMode = WalkDefault;// 步行默认模式
//	private DriveRouteResult driveRouteResult;// 驾车模式查询结果
//	private WalkRouteResult walkRouteResult;// 步行模式查询结果
//	private int routeType = 1;// 1代表公交模式，2代表驾车模式，3代表步行模式
//	private String strStart;
//	private String strEnd;
//	private LatLonPoint startPoint = null;
//	private LatLonPoint endPoint = null;
//	private PoiSearch.Query startSearchQuery;
//	private PoiSearch.Query endSearchQuery;
//
//	private boolean isClickStart = false;
//	private boolean isClickTarget = false;
//	private Marker startMk, targetMk;
//	private RouteSearch routeSearch;
//	public ArrayAdapter<String> aAdapter;
//
//	@Override
//	protected void onCreate(Bundle bundle) {
//		super.onCreate(bundle);
//		setContentView(R.layout.activity_navigation);
//        /*
//         * 设置离线地图存储目录，在下载离线地图或初始化地图设置;
//         * 使用过程中可自行设置, 若自行设置了离线地图存储的路径，
//         * 则需要在离线地图下载和使用地图页面都进行路径设置
//         * */
//	    //Demo中为了其他界面可以使用下载的离线地图，使用默认位置存储，屏蔽了自定义设置
//        MapsInitializer.sdcardDir =OffLineMapUtils.getSdCacheDir(this);
//		mapView = (MapView) findViewById(R.id.ana_amnv_navigation);
//		mapView.onCreate(bundle);// 此方法必须重写
//		init();
//		bindView();
//	}
//
//	/**
//	 * 初始化AMap对象
//	 */
//	private void init() {
//		if (aMap == null) {
//			aMap = mapView.getMap();
//			setUpMap();
//			registerListener();
//		}
//		routeSearch = new RouteSearch(this);
//		routeSearch.setRouteSearchListener(this);
//	}
//
//	/**
//	 * 设置一些amap的属性
//	 */
//	private void setUpMap() {
////		aMap.setLocationSource(this);// 设置定位监听
//		aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
//		aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
//		// 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
//		aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
//	}
//
//	private void bindView(){
//		mlocationClient = new AMapLocationClient(this);
//		mLocationOption = new AMapLocationClientOption();
//		//设置定位监听
//		mlocationClient.setLocationListener(this);
//		//设置为高精度定位模式
//		mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
//		//设置定位参数
//		mlocationClient.setLocationOption(mLocationOption);
//		// 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
//		// 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用stopLocation()方法来取消定位请求
//		// 在定位结束后，在合适的生命周期调用onDestroy()方法
//		// 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
//		mlocationClient.startLocation();
//	}
//
//	/**
//	 * 方法必须重写
//	 */
//	@Override
//	protected void onResume() {
//		super.onResume();
//		mapView.onResume();
//	}
//
//	/**
//	 * 方法必须重写
//	 */
//	@Override
//	protected void onPause() {
//		super.onPause();
//		mapView.onPause();
//	}
//
//	/**
//	 * 方法必须重写
//	 */
//	@Override
//	protected void onSaveInstanceState(Bundle outState) {
//		super.onSaveInstanceState(outState);
//		mapView.onSaveInstanceState(outState);
//	}
//
//	/**
//	 * 方法必须重写
//	 */
//	@Override
//	protected void onDestroy() {
//		super.onDestroy();
//		mapView.onDestroy();
//		if (mlocationClient != null) {
//			mlocationClient.stopLocation();
//			mlocationClient.onDestroy();
//		}
//		mlocationClient = null;
//	}
//
//	/**
//	 * 选择驾车模式
//	 */
//	private void drivingRoute() {
//		routeType = 2;// 标识为驾车模式
//	}
//
//	/**
//	 * 选择步行模式
//	 */
//	private void walkRoute() {
//		routeType = 3;// 标识为步行模式
//		walkMode = WalkMultipath;
//	}
//
//	/**
//	 * 在地图上选取起点
//	 */
//	private void startImagePoint() {
//		CustomToast.showToast(mContext, "在地图上点击您的起点");
//		isClickStart = true;
//		isClickTarget = false;
//		registerListener();
//	}
//
//	/**
//	 * 在地图上选取终点
//	 */
//	private void endImagePoint() {
//		CustomToast.showToast(mContext, "在地图上点击您的终点");
//		isClickTarget = true;
//		isClickStart = false;
//		registerListener();
//	}
//
//	/**
//	 * 点击搜索按钮开始Route搜索
//	 */
//	public void searchRoute() {
//		if (strStart == null || strStart.length() == 0) {
//			CustomToast.showToast(mContext, "请选择起点");
//			return;
//		}
//		if (strEnd == null || strEnd.length() == 0) {
//			CustomToast.showToast(mContext, "请选择终点");
//			return;
//		}
//		if (strStart.equals(strEnd)) {
//			CustomToast.showToast(mContext, "起点与终点距离很近，您可以步行前往");
//			return;
//		}
//
//		startSearchResult();// 开始搜终点
//	}
//
//	@Override
//	public void onInfoWindowClick(Marker marker) {
//		isClickStart = false;
//		isClickTarget = false;
////		if (marker.equals(startMk)) {
////			startPoint = AMapUtil.convertToLatLonPoint(startMk.getPosition());
////			startMk.hideInfoWindow();
////			startMk.remove();
////		} else if (marker.equals(targetMk)) {
////			endTextView.setText("地图上的终点");
////			endPoint = AMapUtil.convertToLatLonPoint(targetMk.getPosition());
////			targetMk.hideInfoWindow();
////			targetMk.remove();
////		}
//	}
//
//	@Override
//	public boolean onMarkerClick(Marker marker) {
//		if (marker.isInfoWindowShown()) {
//			marker.hideInfoWindow();
//		} else {
//			marker.showInfoWindow();
//		}
//		return false;
//	}
//
//	@Override
//	public void onMapClick(LatLng latng) {
//		if (isClickStart) {
//			startMk = aMap.addMarker(new MarkerOptions()
//					.anchor(0.5f, 1)
//					.icon(BitmapDescriptorFactory
//							.fromResource(R.mipmap.arrow_forward)).position(latng)
//					.title("点击选择为起点"));
//			startMk.showInfoWindow();
//		} else if (isClickTarget) {
//			targetMk = aMap.addMarker(new MarkerOptions()
//					.anchor(0.5f, 1)
//					.icon(BitmapDescriptorFactory
//							.fromResource(R.mipmap.arrow_forward)).position(latng)
//					.title("点击选择为目的地"));
//			targetMk.showInfoWindow();
//		}
//	}
//
//	@Override
//	public View getInfoContents(Marker marker) {
//		return null;
//	}
//
//	@Override
//	public View getInfoWindow(Marker marker) {
//		return null;
//	}
//
//	/**
//	 * 注册监听
//	 */
//	private void registerListener() {
//		aMap.setOnMapClickListener(RouteActivity.this);
//		aMap.setOnMarkerClickListener(RouteActivity.this);
//		aMap.setOnInfoWindowClickListener(RouteActivity.this);
//		aMap.setInfoWindowAdapter(RouteActivity.this);
//	}
//
//	/**
//	 * 显示进度框
//	 */
//	private void showProgressDialog() {
//		if (progDialog == null)
//			progDialog = new ProgressDialog(this);
//		progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//		progDialog.setIndeterminate(false);
//		progDialog.setCancelable(true);
//		progDialog.setMessage("正在搜索");
//		progDialog.show();
//	}
//
//	/**
//	 * 隐藏进度框
//	 */
//	private void dissmissProgressDialog() {
//		if (progDialog != null) {
//			progDialog.dismiss();
//		}
//	}
//
//	/**
//	 * 查询路径规划起点
//	 */
//	public void startSearchResult() {
//		if (startPoint != null && strStart.equals("地图上的起点")) {
//			endSearchResult();
//		} else {
//			showProgressDialog();
//			startSearchQuery = new PoiSearch.Query(strStart, "", "010"); // 第一个参数表示查询关键字，第二参数表示poi搜索类型，第三个参数表示城市区号或者城市名
//			startSearchQuery.setPageNum(0);// 设置查询第几页，第一页从0开始
//			startSearchQuery.setPageSize(20);// 设置每页返回多少条数据
//			PoiSearch poiSearch = new PoiSearch(RouteActivity.this,
//					startSearchQuery);
//			poiSearch.setOnPoiSearchListener(this);
//			poiSearch.searchPOIAsyn();// 异步poi查询
//		}
//	}
//
//	/**
//	 * 查询路径规划终点
//	 */
//	public void endSearchResult() {
//		if (endPoint != null && strEnd.equals("地图上的终点")) {
//			searchRouteResult(startPoint, endPoint);
//		} else {
//			showProgressDialog();
//			endSearchQuery = new PoiSearch.Query(strEnd, "", "010"); // 第一个参数表示查询关键字，第二参数表示poi搜索类型，第三个参数表示城市区号或者城市名
//			endSearchQuery.setPageNum(0);// 设置查询第几页，第一页从0开始
//			endSearchQuery.setPageSize(20);// 设置每页返回多少条数据
//
//			PoiSearch poiSearch = new PoiSearch(RouteActivity.this,
//					endSearchQuery);
//			poiSearch.setOnPoiSearchListener(this);
//			poiSearch.searchPOIAsyn(); // 异步poi查询
//		}
//	}
//
//	/**
//	 * 开始搜索路径规划方案
//	 */
//	public void searchRouteResult(LatLonPoint startPoint, LatLonPoint endPoint) {
//		showProgressDialog();
//		final FromAndTo fromAndTo = new FromAndTo(
//				startPoint, endPoint);
//			DriveRouteQuery query = new DriveRouteQuery(fromAndTo, drivingMode,
//					null, null, "");// 第一个参数表示路径规划的起点和终点，第二个参数表示驾车模式，第三个参数表示途经点，第四个参数表示避让区域，第五个参数表示避让道路
//			routeSearch.calculateDriveRouteAsyn(query);// 异步路径规划驾车模式查询
//	}
//
//
//	/**
//	 * POI搜索结果回调
//	 */
//	@Override
//	public void onPoiSearched(PoiResult result, int rCode) {
//		dissmissProgressDialog();
//		if (rCode == 0) {// 返回成功
//			if (result != null && result.getQuery() != null
//					&& result.getPois() != null && result.getPois().size() > 0) {// 搜索poi的结果
//				if (result.getQuery().equals(startSearchQuery)) {
//					List<PoiItem> poiItems = result.getPois();// 取得poiitem数据
////					RouteSearchPoiDialog dialog = new RouteSearchPoiDialog(
////							RouteActivity.this, poiItems);
////					dialog.setTitle("您要找的起点是:");
////					dialog.show();
////					dialog.setOnListClickListener(new OnListItemClick() {
////						@Override
////						public void onListItemClick(
////								RouteSearchPoiDialog dialog,
////								PoiItem startpoiItem) {
////							startPoint = startpoiItem.getLatLonPoint();
////							strStart = startpoiItem.getTitle();
////							startTextView.setText(strStart);
////							endSearchResult();// 开始搜终点
////						}
////
////					});
//				} else if (result.getQuery().equals(endSearchQuery)) {
//					List<PoiItem> poiItems = result.getPois();// 取得poiitem数据
////					RouteSearchPoiDialog dialog = new RouteSearchPoiDialog(
////							RouteActivity.this, poiItems);
////					dialog.setTitle("您要找的终点是:");
////					dialog.show();
////					dialog.setOnListClickListener(new OnListItemClick() {
////						@Override
////						public void onListItemClick(
////								RouteSearchPoiDialog dialog, PoiItem endpoiItem) {
////							endPoint = endpoiItem.getLatLonPoint();
////							strEnd = endpoiItem.getTitle();
////							endTextView.setText(strEnd);
////							searchRouteResult(startPoint, endPoint);// 进行路径规划搜索
////						}
////
////					});
//				}
//			} else {
//				CustomToast.showToast(mContext, R.string.no_result);
//			}
//		} else if (rCode == 27) {
//			CustomToast.showToast(mContext, R.string.error_network);
//		} else if (rCode == 32) {
//			CustomToast.showToast(mContext, R.string.error_key);
//		} else {
//			CustomToast.showToast(mContext, getString(R.string.error_other)
//					+ rCode);
//		}
//	}
//
//
//	@Override
//	public void onBusRouteSearched(BusRouteResult busRouteResult, int i) {
//
//	}
//
//	/**
//	 * 驾车结果回调
//	 */
//	@Override
//	public void onDriveRouteSearched(DriveRouteResult result, int rCode) {
//		dissmissProgressDialog();
//		if (rCode == 0) {
//			if (result != null && result.getPaths() != null
//					&& result.getPaths().size() > 0) {
//				driveRouteResult = result;
//				DrivePath drivePath = driveRouteResult.getPaths().get(0);
//				aMap.clear();// 清理地图上的所有覆盖物
//				DrivingRouteOverlay drivingRouteOverlay = new DrivingRouteOverlay(
//						this, aMap, drivePath, driveRouteResult.getStartPos(),
//						driveRouteResult.getTargetPos());
//				drivingRouteOverlay.removeFromMap();
//				drivingRouteOverlay.addToMap();
//				drivingRouteOverlay.zoomToSpan();
//			} else {
//				CustomToast.showToast(mContext, R.string.no_result);
//			}
//		} else if (rCode == 27) {
//			CustomToast.showToast(mContext, R.string.error_network);
//		} else if (rCode == 32) {
//			CustomToast.showToast(mContext, R.string.error_key);
//		} else {
//			CustomToast.showToast(mContext, getString(R.string.error_other)
//					+ rCode);
//		}
//	}
//
//	/**
//	 * 定位成功后回调函数
//	 */
//	@Override
//	public void onLocationChanged(AMapLocation amapLocation) {
//		if ( amapLocation != null) {
//			if (amapLocation != null
//					&& amapLocation.getErrorCode() == 0) {
////				mLocationErrText.setVisibility(View.GONE);
////				mListener.onLocationChanged(amapLocation);// 显示系统小蓝点
//			} else {
//				String errText = "定位失败," + amapLocation.getErrorCode()+ ": " + amapLocation.getErrorInfo();
//				Log.e("AmapErr", errText);
//				CustomToast.showToast(mContext, errText);
////				mLocationErrText.setVisibility(View.VISIBLE);
////				mLocationErrText.setText(errText);
//			}
//		}
//	}
//
//	@Override
//	public void onWalkRouteSearched(WalkRouteResult walkRouteResult, int i) {
//
//	}
//
//
//	@Override
//	public void onClick(View v) {
//		switch (v.getId()) {
//		default:
//			break;
//		}
//	}
//
//	@Override
//	public void onPoiItemSearched(PoiItem arg0, int arg1) {
//		// TODO Auto-generated method stub
//
//	}
//}
