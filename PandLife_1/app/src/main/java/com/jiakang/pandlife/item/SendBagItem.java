package com.jiakang.pandlife.item;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.annotation.sqlite.Id;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.db.DataCacheDBHelper;

/**
 * 寄件信息列表中实体
 * Created by Administrator on 2016/1/14.
 */
@Table(name = DataCacheDBHelper.TAB_MY_SENDEXPRESS_CACHE)
public class SendBagItem extends Item{

    /** 数据库id	Int(10) */
    @Id
    private int _id;
    /** 寄件记录id	Int(10) */
    private int id;
    /** 收件人地址id	Int(10) */
    private int sid;
    /** 快递id	Int(10) */
    private int delivery_id;
    /** 快递名称	String(10) */
    private String delivery_name;
    /** 运单号	String(10) */
    private String mailno;
    /** 是否处理（是：y，否：n）	Varchar(32) */
    private String bag_type;
    /** 站点id	Int(10) */
    private int station;
    /** 是否正常（正常：y   取消：n）	Varchar(32) */
    private String is_enable;
    /** 寄件提交时间	Varchar(32) */
    private String reatetime;
    /** 收件人姓名	Varchar(32) */
    private String sname;
    /** 收件人电话 string */
    private String phone;
    /** 收件人详细地址 string */
    private String address;
    /** 经度 string */
    private String longitude;
    /** 纬度 string */
    private String latitude;

    /** 插入数据库时的时间 */
    private long currentTime = System.currentTimeMillis();
    /** 保存的时间 */
    private long saveTime = 600000;

    @Override
    public int insert() {
        PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
        return 1;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public long getSaveTime() {
        return saveTime;
    }

    public void setSaveTime(long saveTime) {
        this.saveTime = saveTime;
    }


    private int layout = R.layout.item_send_bag;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getDelivery_name() {
        return delivery_name;
    }

    public void setDelivery_name(String delivery_name) {
        this.delivery_name = delivery_name;
    }

    public String getBag_type() {
        return bag_type;
    }

    public void setBag_type(String bag_type) {
        this.bag_type = bag_type;
    }

    public int getStation() {
        return station;
    }

    public void setStation(int station) {
        this.station = station;
    }

    public String getIs_enable() {
        return is_enable;
    }

    public void setIs_enable(String is_enable) {
        this.is_enable = is_enable;
    }

    public String getReatetime() {
        return reatetime;
    }

    public void setReatetime(String reatetime) {
        this.reatetime = reatetime;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getDelivery_id() {
        return delivery_id;
    }

    public void setDelivery_id(int delivery_id) {
        this.delivery_id = delivery_id;
    }

    public String getMailno() {
        return mailno;
    }

    public void setMailno(String mailno) {
        this.mailno = mailno;
    }
}
