package com.jiakang.pandlife.api;

import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.jiakang.pandlife.form.FormFile;
import com.jiakang.pandlife.form.FormImage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;


/**
 * Created by gyzhong on 15/3/1.
 */
public class PostUploadRequest extends Request<String> {

    /**
     * ��ȷ���ݵ�ʱ��ص���
     */
    private AbsOnRequestListener mListener ;
    /*���� ����ͨ����������ʽ����*/
    private List<FormImage> mListItem ;
    /*���� ����ͨ����������ʽ����*/
    private Bitmap mBitmap ;
    /*���� ����ͨ����������ʽ����*/
    private List<FormFile> mListFile ;

    private ByteArrayOutputStream bos;

    private String BOUNDARY = "--------------520-13-14"; //���ݷָ���
    private String MULTIPART_FORM_DATA = "multipart/form-data";

    public  PostUploadRequest(List<FormFile> listFileItem, String url, AbsOnRequestListener onRequestListener) {
        super(Method.POST, url, onRequestListener);
        this.mListener = onRequestListener ;
        setShouldCache(false);
        mListFile = listFileItem ;
        setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public  PostUploadRequest(String url, List<FormImage> listItem, AbsOnRequestListener onRequestListener) {
        super(Method.POST, url, onRequestListener);
        this.mListener = onRequestListener ;
        setShouldCache(false);
        mListItem = listItem ;
        setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public PostUploadRequest(String url, Bitmap bitmap, AbsOnRequestListener onRequestListener) {
        super(Method.POST, url, onRequestListener);
        this.mListener = onRequestListener ;
        this.mBitmap = bitmap;
        setShouldCache(false);
        mBitmap = bitmap ;
        setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    /**
     * ���￪ʼ��������
     * @param response Response from the network
     * @return
     */
    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            String mString =
                    new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Log.v("zgy", "====mString===" + mString);

            return Response.success(mString,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    /**
     * �ص���ȷ������
     * @param response The parsed response returned by
     */
    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (mListItem != null && mListItem.size() != 0){
            bos = new ByteArrayOutputStream();
            int N = mListItem.size();
            FormImage formImage;
            for (int i = 0; i < N; i++) {
                formImage = mListItem.get(i);
                StringBuffer sb = new StringBuffer();
            /*��һ��*/
                sb.append("--" + BOUNDARY);
                sb.append("\r\n");
            /*�ڶ���*/
                sb.append("Content-Disposition: form-data;");
                sb.append(" name=\"");
                sb.append(formImage.getmName());
                sb.append("\"");
                sb.append("; filename=\"");
                sb.append(formImage.getFileName());
                sb.append("\"");
                sb.append("\r\n");
            /*������*/
                sb.append("Content-Type: ");
                sb.append(formImage.getmMime());
                sb.append("\r\n");
            /*������*/
                sb.append("\r\n");
                try {
                    bos.write(sb.toString().getBytes("utf-8"));
                /*������*/
                    bos.write(formImage.getValue());
                    bos.write("\r\n".getBytes("utf-8"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        /*��β��*/
            String endLine = "--" + BOUNDARY + "--" + "\r\n";
            try {
                bos.write(endLine.toString().getBytes("utf-8"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("zgy", "=====formImage====\n" + bos.toString());
            return bos.toByteArray();
        }else if (mListFile != null && mListFile.size() != 0){
            bos = new ByteArrayOutputStream();
            int N = mListFile.size();
            FormFile formFile;
            for (int i = 0; i < N; i++) {
                formFile = mListFile.get(i);
                StringBuffer sb = new StringBuffer();
            /*��һ��*/
                sb.append("--" + BOUNDARY);
                sb.append("\r\n");
            /*�ڶ���*/
                sb.append("Content-Disposition: form-data;");
                sb.append(" name=\"");
                sb.append(formFile.getmName());
                sb.append("\"");
                sb.append("; filename=\"");
                sb.append(formFile.getmFileName());
                sb.append("\"");
                sb.append("\r\n");
            /*������*/
                sb.append("Content-Type: ");
                sb.append(formFile.getmMime());
                sb.append("\r\n");
            /*������*/
                sb.append("\r\n");
                try {
                    bos.write(sb.toString().getBytes("utf-8"));
                /*������*/
//                    bos.write(formFile.getmFile(), 0, 0);
                    bos.write("\r\n".getBytes("utf-8"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        /*��β��*/
            String endLine = "--" + BOUNDARY + "--" + "\r\n";
            try {
                bos.write(endLine.toString().getBytes("utf-8"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.v("zgy", "=====formImage====\n" + bos.toString());
            return bos.toByteArray();
        }else{

            return super.getBody();
        }
    }

    @Override
    public String getBodyContentType() {
        return MULTIPART_FORM_DATA+"; boundary="+BOUNDARY;
    }
}
