package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

/**
 * Created by Administrator on 2016/1/18.
 */
public class GroupDetailStationItem extends Item{
    /** 站点id	Int(10) */
    private int station_id;
    /** 站点库存	Int(10) */
    private int number;
    /** 站点名称	Varchar(32) */
    private String station_name;

    private int layout = R.layout.item_select_single_left;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getStation_name() {
        return station_name;
    }

    public void setStation_name(String station_name) {
            this.station_name = station_name;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }
}
