package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.MainPostSupport;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by play on 2016/1/20.
 */
public class MainPostSupportInfo extends BaseAbsInfo {

    private int forumid;
    private MainPostSupport mainPostSupport;
    private String type;

    public int getForumid() {
        return forumid;
    }

    public void setForumid(int forumid) {
        this.forumid = forumid;
    }

    public MainPostSupport getMainPostSupport() {
        return mainPostSupport;
    }

    public void setMainPostSupport(MainPostSupport mainPostSupport) {
        this.mainPostSupport = mainPostSupport;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Forum&a=Support&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("forumid",forumid);
            json.put("type",type);
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        try {
            if(jsonObject.getInt("status") == 1){

                mainPostSupport = BaseInfo.gson.fromJson(jsonObject.toString(),MainPostSupport.class);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
