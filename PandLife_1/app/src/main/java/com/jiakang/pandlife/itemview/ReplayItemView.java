package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.ReplayItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

import java.security.interfaces.ECKey;
import java.util.List;

/**
 * Created by play on 2016/1/8.
 */
public class ReplayItemView extends AbsLinearLayout {

    private ImageView ivHead;
    private TextView tvUser;
    private TextView tvTime;
    private TextView tvFloorNumber;
    private TextView tvContent;
    private TextView tvSupportCount;
    private ImageView ivSupport;
    private ImageView icComment;

    public ReplayItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ReplayItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        ivHead = (ImageView) findViewById(R.id.iv_head_replay);
        tvUser = (TextView) findViewById(R.id.tv_user_replay);
        tvTime = (TextView) findViewById(R.id.tv_time_replay);
        tvFloorNumber = (TextView) findViewById(R.id.tv_floor);
        tvContent = (TextView) findViewById(R.id.tv_content_replay);
        tvSupportCount = (TextView) findViewById(R.id.tv_support_count_replay);
        ivSupport = (ImageView) findViewById(R.id.iv_support_replay);
        icComment = (ImageView) findViewById(R.id.iv_comment_replay);
    }

    @Override
    public void setObject(Item item, final int position, final ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        final ReplayItem replayItem = (ReplayItem) item;
        ImageLoaderUtil.displayImageCircleCache(replayItem.getHead(), ivHead, R.mipmap.ic_head);
        if(TextUtils.isEmpty(replayItem.getNick())){
            tvUser.setText("匿名用户");
        }else {
            tvUser.setText(replayItem.getNick());
        }
        tvTime.setText(replayItem.getTime());
        tvFloorNumber.setText((position + 1) + "楼");
        tvContent.setText(replayItem.getContent());

        //判断该回复是帖子详情的还是小区拼好货的，帖子详情是 1，小区拼好货是2
        //如果是小区拼好货，则不需要显示点赞和评论个数
        if(replayItem.getFlag() == 1){

            tvSupportCount.setVisibility(VISIBLE);
            tvSupportCount.setText(replayItem.getSupportnum() + "");

            if(replayItem.getIssupport() == 1){
                ivSupport.setImageResource(R.mipmap.ic_support_press_replay);
            }else {
                ivSupport.setImageResource(R.mipmap.ic_support_normal_replay);
            }

        }else {//小区拼好货不需要显示点赞和评论

            tvSupportCount.setVisibility(GONE);
            if(replayItem.getSupport() == 1){
                ivSupport.setImageResource(R.mipmap.ic_support_press_replay);
            }else {
                ivSupport.setImageResource(R.mipmap.ic_support_normal_replay);
            }
        }

        ivSupport.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                v.setTag(replayItem);
                onViewClickListener.onViewClick(v, position);
            }
        });

        icComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setTag(replayItem);
                onViewClickListener.onViewClick(v,position);
            }
        });
    }
}
