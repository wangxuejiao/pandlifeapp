package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;

/**
 * Created by play on 2016/1/14.
 */
public class TwoImagesItemView extends AbsLinearLayout  {

    ImageView ivFirst;
    ImageView ivTwo;

    public TwoImagesItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TwoImagesItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {

        ivFirst = (ImageView) findViewById(R.id.iv_first_two_image);
        ivTwo = (ImageView) findViewById(R.id.iv_two_two_image);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);


    }
}
