package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.media.Image;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.VillageGoodsItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

/**
 * Created by play on 2016/1/6.
 */
public class VillageGoodsItemView extends AbsLinearLayout {

    private TextView tvTitle;
    private TextView tvContent;
    private ImageView imageView;

    public VillageGoodsItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VillageGoodsItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        tvTitle = (TextView) findViewById(R.id.tv_title_village_goods);
        tvContent = (TextView) findViewById(R.id.tv_content_village_goods);
        imageView = (ImageView) findViewById(R.id.iv_village_goods);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        VillageGoodsItem.DataEntityVillageGoods villageGoodsItem = (VillageGoodsItem.DataEntityVillageGoods) item;
        tvTitle.setText(villageGoodsItem.getTitle());
        tvContent.setText(villageGoodsItem.getDes());
        ImageLoaderUtil.displayImage(villageGoodsItem.getImg(),imageView,R.mipmap.default_image);
    }
}
