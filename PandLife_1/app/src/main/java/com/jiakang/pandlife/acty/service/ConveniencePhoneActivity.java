package com.jiakang.pandlife.acty.service;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AdapterView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.ConveniencePhoneInfo;
import com.jiakang.pandlife.item.ConveniencePhoneItem;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.CustomDialog;
import com.jiakang.pandlife.widget.PullToRefreshListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


@EActivity(R.layout.activity_convenience_phone)
public class ConveniencePhoneActivity extends BaseActy {

    @ViewById(R.id.lv_convenience_phone)
    PullToRefreshListView lvConveniencePhone;

    private ItemAdapter mItemAdapter;
    private List<ConveniencePhoneItem.DataEntity> mDataEntitieList = new ArrayList<>();

    /*便民电话请求类*/
    private ConveniencePhoneInfo mConveniencePhoneInfo = new ConveniencePhoneInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
    }

    @AfterViews
    protected void afterCreateView() {

        initTitleBar(R.id.al_tb_title, "便民电话", null, null);
        mItemAdapter = new ItemAdapter(mContext);
        lvConveniencePhone.setAdapter(mItemAdapter);

        lvConveniencePhone.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            @Override
            public void onRefresh() {

                conveniencePhoneRequest();
            }
        });

        lvConveniencePhone.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                final CustomDialog customDialog = Util.getDialog(mContext, "提示", "确定要拨打该号码？", "确定", "取消");
                customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                        //用intent启动拨打电话
                        String phone = ((ConveniencePhoneItem.DataEntity) lvConveniencePhone.getItemAtPosition(position)).getTel();
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
                        if (ActivityCompat.checkSelfPermission(ConveniencePhoneActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                });
                customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        customDialog.dismiss();
                    }
                });
                customDialog.show();
            }
        });
    }

    private void initData() {

        conveniencePhoneRequest();
    }

    public void conveniencePhoneRequest() {

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mConveniencePhoneInfo, new AbsOnRequestListener(mContext) {

            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mDataEntitieList = mConveniencePhoneInfo.getDataEntityConveniencePhoneList();
                if(mDataEntitieList != null && mDataEntitieList.size() > 0){

                    mItemAdapter.clear();
                    mItemAdapter.addItems((List)mDataEntitieList);
                    mItemAdapter.notifyDataSetChanged();
                }
                lvConveniencePhone.onRefreshComplete();
            }
        });

    }
}
