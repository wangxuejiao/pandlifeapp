package com.jiakang.pandlife.item;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.annotation.sqlite.Id;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.annotation.sqlite.Transient;
import com.jiakang.pandlife.db.DataCacheDBHelper;

import java.util.List;

/**
 * Created by play on 2016/1/19.
 */
public class PostDetail {


    /**
     * data : {"nick":null,"head":null,"replyid":"303","dialoglist":[{"replyuser":{"nick":null,"uid":"13","id":"17","floor":"4"},"pic":null,"content":"再来一个","isshow":"1","nick":null,"head":null,"uid":"13","replyid":"18","dialog_id":"303","id":"18","time":"17:40","floor":"5","support":0},{"replyuser":{"nick":null,"uid":"13","id":"17","floor":"4"},"pic":null,"content":"再来一个冯绍峰的说法","isshow":"1","nick":null,"head":null,"uid":"13","replyid":"19","dialog_id":"303","id":"19","time":"17:40","floor":"6","support":0}],"time":"1452936215","title":"小区活动","content":"星期天天气不错，大家一起去郊游"}
     * status : 1
     * info : 成功
     */
    private DataEntityPostDetail data;
    private int status;
    private String info;

    public void setData(DataEntityPostDetail data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public DataEntityPostDetail getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }

    @Table(name = DataCacheDBHelper.TAB_POST_DETAIL_CACHE)
    public class DataEntityPostDetail extends Item{
        /**
         *
         * nick : null
         * head : null
         * replyid : 333
         * replynum : 0
         * dialoglist : []
         * time : 1453276154
         * title : 魔法
         * imglist : [{"imgurl":"http://7xpw5g.com1.z0.glb.clouddn.com/1453276141160.jpg"},{"imgurl":"http://7xpw5g.com1.z0.glb.clouddn.com/1453276141225.jpg"}]
         * content : 接口
         * issupport : 0
         * supportnum : 0
         */

        @Id(column="postDetailId")
        private int id;
        private String type;
        private String typename;
        private String typepic;
        private String nick;
        private String head;
        private String replyid;
        private String replynum;
        @Transient
        private List<ReplayItem> dialoglist;
        private String time;
        private String title;
        @Transient
        private List<ImglistEntity> imglist;
        private String content;
        private int issupport;
        private String supportnum;


        public DataEntityPostDetail() {
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTypename() {
            return typename;
        }

        public void setTypename(String typename) {
            this.typename = typename;
        }

        public String getTypepic() {
            return typepic;
        }

        public void setTypepic(String typepic) {
            this.typepic = typepic;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public void setHead(String head) {
            this.head = head;
        }

        public void setReplyid(String replyid) {
            this.replyid = replyid;
        }

        public void setReplynum(String replynum) {
            this.replynum = replynum;
        }

        public void setDialoglist(List<ReplayItem> dialoglist) {
            this.dialoglist = dialoglist;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setImglist(List<ImglistEntity> imglist) {
            this.imglist = imglist;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public void setIssupport(int issupport) {
            this.issupport = issupport;
        }

        public void setSupportnum(String supportnum) {
            this.supportnum = supportnum;
        }

        public String getNick() {
            return nick;
        }

        public String getHead() {
            return head;
        }

        public String getReplyid() {
            return replyid;
        }

        public String getReplynum() {
            return replynum;
        }

        public List<ReplayItem> getDialoglist() {
            return dialoglist;
        }

        public String getTime() {
            return time;
        }

        public String getTitle() {
            return title;
        }

        public List<ImglistEntity> getImglist() {
            return imglist;
        }

        public String getContent() {
            return content;
        }

        public int getIssupport() {
            return issupport;
        }

        public String getSupportnum() {
            return supportnum;
        }

        public class ImglistEntity {
            /**
             * imgurl : http://7xpw5g.com1.z0.glb.clouddn.com/1453276141160.jpg
             */
            private String imgurl;

            public void setImgurl(String imgurl) {
                this.imgurl = imgurl;
            }

            public String getImgurl() {
                return imgurl;
            }
        }

        @Override
        public int getItemLayoutId() {
            return 0;
        }

        @Override
        public int insert() {
            PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
            return 1;
        }
    }
}
