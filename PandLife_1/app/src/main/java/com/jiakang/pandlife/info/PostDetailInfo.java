package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.item.PostDetail;
import com.jiakang.pandlife.item.ReplayItem;
import com.jiakang.pandlife.utils.DataCache;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by play on 2016/1/19.
 */
public class PostDetailInfo extends BaseAbsInfo {

    private int id;
    private PostDetail postDetail;
    private PostDetail.DataEntityPostDetail dataEntityPostDetail;
    private List<ReplayItem> replayItemList = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PostDetail getPostDetail() {
        return postDetail;
    }

    public void setPostDetail(PostDetail postDetail) {
        this.postDetail = postDetail;
    }

    public PostDetail.DataEntityPostDetail getDataEntityPostDetail() {
        return dataEntityPostDetail;
    }

    public void setDataEntityPostDetail(PostDetail.DataEntityPostDetail dataEntityPostDetail) {
        this.dataEntityPostDetail = dataEntityPostDetail;
    }

    public List<ReplayItem> getReplayItemList() {
        return replayItemList;
    }

    public void setReplayItemList(List<ReplayItem> replayItemList) {
        replayItemList = replayItemList;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Forum&a=ForumDetail&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("id",id);
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        try {
            if(jsonObject.getInt("status")==1){

                postDetail = BaseInfo.gson.fromJson(jsonObject.toString(), PostDetail.class);
                dataEntityPostDetail = postDetail.getData();

                //缓存数据
                DataCache aCache = DataCache.get(PandLifeApp.getInstance());
                String key = "dataEntityPostDetail" + dataEntityPostDetail.getReplyid();
                aCache.put(key,jsonObject,10 * 60);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
