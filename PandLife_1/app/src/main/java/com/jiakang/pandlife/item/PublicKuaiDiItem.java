package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

/**
 * 物流信息实体
 * Created by Administrator on 2016/1/14.
 */
public class PublicKuaiDiItem extends Item{

    /** 信息提交时间 String */
    private String time;
    /** 物流信息内容 String */
    private String context;
    /**添加一个字段记录是否是第一个或者最后一个*/
    private String ordinal;

    private int layout = R.layout.item_public_kuaidi;
    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public String getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(String ordinal) {
        this.ordinal = ordinal;
    }
}
