package com.jiakang.pandlife.acty.share;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.amap.api.location.AMapLocation;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.personalcenter.SenderExpressDetailActivity_;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.NearbyStationInfo;
import com.jiakang.pandlife.info.SiteInfo;
import com.jiakang.pandlife.item.SendBagItem;
import com.jiakang.pandlife.item.SiteItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.CustomDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.List;

import greendroid.widgetww.PullToRefreshListView;

/**
 * 选择附近站点（所有选择站点的都进这里）
 */
@EActivity(R.layout.activity_select_nearby_site)
public class SelectNearbySiteActivity extends BaseActy {
    protected static final String TAG = "SelectNearbySiteActivity";

    @ViewById(R.id.asns_lv_site)
    ListView siteLV;

    private ItemAdapter mItemAdapter;

    /** 附近站点接口 */
    private SiteInfo mSiteInfo = new SiteInfo();

    public static final String SELECT_NEARBY_SITE = "select_nearby_site";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.asns_tb_title, "附近站点");
        startLocation();
        bindView();
//        bindInfo();
    }

    private void bindView(){
        mItemAdapter = new ItemAdapter(mContext);
        siteLV.setAdapter(mItemAdapter);
    }

    private void bindInfo(double longitude, double latitude){
//        if ((returnLocationLongitude() == 0) || (returnLocationLatitude() == 0)){
//            final CustomDialog customDialog = Util.getDialog(mContext, "提示", "定位失败，确定重新定位获取附近站点吗？", "确定", "取消");
//            customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    customDialog.dismiss();
//                    initLocation();
//                    bindInfo();
//                }
//            });
//            customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    customDialog.dismiss();
//                }
//            });
//            customDialog.show();
//        }else{
            mSiteInfo.setLongitude(longitude+"");
            mSiteInfo.setLatitude(latitude + "");
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mSiteInfo, new AbsOnRequestListener(mContext){
                @Override
                public void onRequestSuccess(int result, JSONObject jsonObject) {
                    super.onRequestSuccess(result, jsonObject);
                    try{
                        if (mSiteInfo.getAllItems().size() == 0){
                            CustomToast.showToast(mContext, "附近没有站点");
                            return;
                        }
                        mItemAdapter.clear();
                        mItemAdapter.addItems((List) mSiteInfo.getAllItems());
                        mItemAdapter.notifyDataSetChanged();

                        siteLV.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    int position, long id) {
                                ListView listView = (ListView) parent;
                                SiteItem siteItem = (SiteItem) listView.getItemAtPosition(position);
                                Intent intentReturn = new Intent();
                                intentReturn.putExtra(SELECT_NEARBY_SITE, siteItem);
                                setResult(RESULT_OK, intentReturn);
                                finish();
                            }
                        });

                    }catch (Exception e){

                    }
                }
            });
//        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        //定位回调
        JKLog.i(TAG, aMapLocation.toString());
        double locationLongitude = aMapLocation.getLongitude();
        double locationLatitude = aMapLocation.getLatitude();
        bindInfo(locationLongitude, locationLatitude);
    }
}
