package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.GoodsItem;
import com.jiakang.pandlife.item.GroupPurchaseDetailItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.RecommendGoodsItem;
import com.jiakang.pandlife.item.RedPackageItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

/**
 * Created by play on 2016/1/6.
 */
public class GroupPurchaseIndentItemView extends AbsRelativeLayout {

    private ImageView imageIV;
    private TextView goodsNameTV;
//    private TextView contentTV;
    private TextView sumTV;
    private TextView goodsNumberTV;

    public GroupPurchaseIndentItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GroupPurchaseIndentItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        imageIV = (ImageView)findViewById(R.id.igpi_iv_image);
        goodsNameTV = (TextView) findViewById(R.id.igpi_tv_goods_name);
//        contentTV = (TextView) findViewById(R.id.igpi_tv_content);
        sumTV = (TextView) findViewById(R.id.igpi_tv_sum);
        goodsNumberTV = (TextView) findViewById(R.id.igpi_tv_goods_number);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);
        if (item instanceof GroupPurchaseDetailItem) {
            GroupPurchaseDetailItem groupPurchaseDetailItem = (GroupPurchaseDetailItem) item;
            goodsNameTV.setText(groupPurchaseDetailItem.getTitle());
//        contentTV.setText(goodsItem.getContent());
            sumTV.setText("￥" + groupPurchaseDetailItem.getTprice());
            goodsNumberTV.setText("*" + groupPurchaseDetailItem.getTotal());
            ImageLoaderUtil.displayImage(groupPurchaseDetailItem.getPic(), imageIV);
        }else if (item instanceof GoodsItem){
            GoodsItem goodsItem = (GoodsItem) item;
            goodsNameTV.setText(goodsItem.getName());
            sumTV.setText("￥" + goodsItem.getPrice());
            goodsNumberTV.setText("*" + goodsItem.getNumber());
            ImageLoaderUtil.displayImage(goodsItem.getPic(), imageIV);
        }else if (item instanceof RecommendGoodsItem){
            RecommendGoodsItem recommendGoodsItem = (RecommendGoodsItem) item;
            goodsNameTV.setText(recommendGoodsItem.getTitle());
            sumTV.setText("￥" + recommendGoodsItem.getTprice());
            goodsNumberTV.setText("*" + recommendGoodsItem.getClickNum());
            ImageLoaderUtil.displayImage(recommendGoodsItem.getPic(), imageIV);
        }
    }
}
