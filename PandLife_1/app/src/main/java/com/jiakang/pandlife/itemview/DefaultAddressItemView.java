package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.Item;

/**
 * Created by play on 2016/1/6.
 */
public class DefaultAddressItemView extends AbsRelativeLayout {

    private TextView nameTV;
    private TextView phoneTV;
    private TextView addressTV;

    public DefaultAddressItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DefaultAddressItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();
        nameTV = (TextView) findViewById(R.id.ida_tv_name);
        phoneTV = (TextView) findViewById(R.id.ida_tv_phone);
        addressTV = (TextView) findViewById(R.id.ida_tv_address);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        if(item instanceof AddressItem){
            AddressItem addressItem = (AddressItem) item;
            nameTV.setText(addressItem.getName());
            phoneTV.setText(addressItem.getPhone());
            addressTV.setText(addressItem.getAddress());

        }
//        else if(item instanceof SenderAddressItem){
//            SenderAddressItem senderAddressItem = (SenderAddressItem) item;
//            nameTV.setText(senderAddressItem.getName());
//            phoneTV.setText(senderAddressItem.getPhone());
//            addressTV.setText(senderAddressItem.getAddress());
//        }
    }
}
