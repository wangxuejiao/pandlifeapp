package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

import java.util.List;

/**
 * Created by play on 2016/1/6.
 */
public class ConveniencePhoneItem{

    /**
     * data : [{"station_id":"-1","icon":"http://panda-life.cn/statics/images/4.jpg","tel":"025-84185858","id":"2","title":"百米需电话"}]
     * status : 1
     * info : 便民服务列表
     */
    private List<DataEntity> data;
    private int status;
    private String info;




    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }



    public class DataEntity extends Item {
        /**
         * station_id : -1
         * icon : http://panda-life.cn/statics/images/4.jpg
         * tel : 025-84185858
         * id : 2
         * title : 百米需电话
         */
        private String station_id;
        private String icon;
        private String tel;
        private String id;
        private String title;
        private int layout = R.layout.item_convenience_phone;

        @Override
        public int getItemLayoutId() {
            return layout;
        }
        public void setLayout(int layout){
            this.layout = layout;
        }

        public void setStation_id(String station_id) {
            this.station_id = station_id;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getStation_id() {
            return station_id;
        }

        public String getIcon() {
            return icon;
        }

        public String getTel() {
            return tel;
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }
    }
}
