package com.jiakang.pandlife.utils;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class DesPlus {

	static String src = "test source";

	// static {
	// Security.addProvider(new com.sun.crypto.provider.SunJCE());
	// }

	public static byte[] encrypt(byte[] arrB, byte[] key) throws Exception {
		Key secretKey = null;
		Cipher encryptCipher = null;
		byte[] bs = null;
		try {
			secretKey = getKey(key);
			encryptCipher = Cipher.getInstance("DES");
			encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (arrB != null) {
			bs = encryptCipher.doFinal(arrB);
		}
		if (bs != null) {
			return bs;
		} else {
			return null;
		}
	}

	public static byte[] decrypt(byte[] arrB, byte[] key) throws Exception {
		Key secretKey = null;
		Cipher decryptCipher = null;
		byte[] bs = null;
		try {
			secretKey = getKey(key);
			decryptCipher = Cipher.getInstance("DES");
			decryptCipher.init(Cipher.DECRYPT_MODE, secretKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (arrB != null) {
			bs = decryptCipher.doFinal(arrB);
		}
		if (bs != null) {
			return bs;
		} else {
			return null;
		}
	}

	private static Key getKey(byte[] arrBTmp) throws Exception {
		byte[] arrB = new byte[8];
		for (int i = 0; i < arrBTmp.length && i < arrB.length; i++) {
			arrB[i] = arrBTmp[i];
		}
		Key key = new SecretKeySpec(arrB, "DES");
		return key;
	}

}