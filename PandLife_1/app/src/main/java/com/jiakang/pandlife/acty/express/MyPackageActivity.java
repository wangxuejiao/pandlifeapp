package com.jiakang.pandlife.acty.express;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;


import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.db.DataCacheDBHelper;
import com.jiakang.pandlife.db.DataCacheDBManage;
import com.jiakang.pandlife.info.AlexpresslistInfo;
import com.jiakang.pandlife.info.ExpresslistInfo;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.widget.PullToRefreshListView;
import com.jiakang.pandlife.widget.TitleBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * 我的包裹
 */
@EActivity(R.layout.activity_my_package)
public class MyPackageActivity extends BaseActy {
    protected static final String TAG = "MyPackageActivity";

    @ViewById(R.id.amp_tb_title)
    TitleBar mTitleBar;
    @ViewById(R.id.amp_rgp_tab)
    RadioGroup tabRGP;
    @ViewById(R.id.amp_rbn_tableOne)
    RadioButton tableOneRBN;
    @ViewById(R.id.amp_rbn_tableTwo)
    RadioButton tableTwoRBN;
    @ViewById(R.id.amp_prlv_express_get)
    PullToRefreshListView getExpressPRLV;
    @ViewById(R.id.amp_prlv_express_unget)
    PullToRefreshListView ungetExpressPRLV;
    @ViewById(R.id.amp_rl_no_data)
    RelativeLayout noDataRL;

    private ItemAdapter getExpressListItemAdapter;
    private ItemAdapter ungetExpressListItemAdapter;

    /** 每页个数，BaseInfo.pageSize = 20 为默认值 */
    private int pageSize = 20;
    /** 第几页 */
    private int pageIndexUnget = 1;
    private int pageIndexGet = 1;

    private final static int GET_NEW_OK = 0x12;
    private final static int GET_MORE_OK = 0x13;
    private final static int IS_GET_NO = 0;
    private final static int IS_GET_NEW = 1;
    private final static int IS_GET_MORE = 2;
    private static int isPullRefresh = 0;
    private boolean noUpLoadFinishGet = true;
    private boolean noUpLoadFinishUnget = true;

    private DataCacheDBManage dataCacheDBManage;
    //记录数据库中数据保存是否过期
    private boolean cacheDataDue = false;
    //保存未去包裹集合
    private List<ExpressListItem> unGetAllItems = new ArrayList<ExpressListItem>();
    //保存已去包裹集合
    private List<ExpressListItem> getAllItems = new ArrayList<ExpressListItem>();

    /** 我的未取包裹接口 */
    private ExpresslistInfo mExpresslistInfo = new ExpresslistInfo();
    /** 我的已取包裹接口 */
    private AlexpresslistInfo mAlexpresslistInfo = new AlexpresslistInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.amp_tb_title, "我的包裹");
        mTitleBar.rightIBN.setImageDrawable(getResources().getDrawable(R.mipmap.icon_iv_scanning));
        mTitleBar.rightBN.setVisibility(View.GONE);
        mTitleBar.rightIBN.setVisibility(View.VISIBLE);
        mTitleBar.rightIBN.setOnClickListener(this);
        dataCacheDBManage = getMyApp().getCacheDataDBManage();
        initView();
        getCacheDataUnget();

    }

    private void initView(){
        getExpressListItemAdapter = new ItemAdapter(mContext);
        ungetExpressListItemAdapter = new ItemAdapter(mContext);
        getExpressPRLV.setShowFootView(true);
        ungetExpressPRLV.setShowFootView(true);
        getExpressPRLV.setAdapter(getExpressListItemAdapter);
        ungetExpressPRLV.setAdapter(ungetExpressListItemAdapter);
        ungetExpressPRLV.setVisibility(View.VISIBLE);
        getExpressPRLV.setVisibility(View.GONE);

        tabRGP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.amp_rbn_tableOne:
                        tableOneRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        tableTwoRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        ungetExpressPRLV.setVisibility(View.VISIBLE);
                        getExpressPRLV.setVisibility(View.GONE);
                        if (ungetExpressListItemAdapter.getCount() == 0) {
                            getCacheDataGet();
//                            getUngetExpress(IS_GET_NO);
                        }
                        break;
                    case R.id.amp_rbn_tableTwo:
                        tableTwoRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        tableOneRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        getExpressPRLV.setVisibility(View.VISIBLE);
                        ungetExpressPRLV.setVisibility(View.GONE);
                        if (getExpressListItemAdapter.getCount() == 0) {
                            getCacheDataGet();
//                            getGetExpress(IS_GET_NO);
                        }
                        break;
                }
            }
        });
    }

    /**
     * 获取缓存数据库中的未取包裹信息
     */
    private void getCacheDataUnget(){

        if (dataCacheDBManage.getUnGetExpressList().size() != 0){

            for (int i=0; i < dataCacheDBManage.getUnGetExpressList().size(); i ++){
                if (("1").equals(dataCacheDBManage.getUnGetExpressList().get(i).getIsget())){
                    unGetAllItems.add(dataCacheDBManage.getUnGetExpressList().get(i));
                }else{
                    getAllItems.add(dataCacheDBManage.getUnGetExpressList().get(i));
                }
            }
            if (unGetAllItems.size() != 0) {
                ungetExpressListItemAdapter.clear();
                ungetExpressListItemAdapter.addItems((List) unGetAllItems);
                ungetExpressListItemAdapter.notifyDataSetChanged();

                //判断是否过期，过期重新网络请求一次
                ExpressListItem expressListItem = unGetAllItems.get(0);
                if ((expressListItem.getCurrentTime() + expressListItem.getSaveTime()) < System.currentTimeMillis()) {
                    //清除数据库
                    dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYEXPRESS_CACHE);
                    //获取网络请求数据
                    getUngetExpress(IS_GET_NO);
                } else {
                    ungetExpressPRLV.onRefreshComplete();
                    ungetExpressPRLV.onMoreComplete(false);
                }
            }else{
                //清除数据库
//                dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYEXPRESS_CACHE);
                //获取网络请求数据
                getUngetExpress(IS_GET_NO);
            }
        }else{
            //清除数据库
            dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYEXPRESS_CACHE);
            //获取网络请求数据
            getUngetExpress(IS_GET_NO);
        }
    }
    /**
     * 获取缓存数据库中的已取包裹信息
     */
    private void getCacheDataGet(){
        if (getAllItems.size() != 0) {
            getExpressListItemAdapter.addItems((List) getAllItems);
            getExpressListItemAdapter.notifyDataSetChanged();

            //判断是否过期，过期重新网络请求一次
            ExpressListItem expressListItem = getAllItems.get(0);
            if ((expressListItem.getCurrentTime() + expressListItem.getSaveTime()) < System.currentTimeMillis()) {
                //清除数据库
                dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYEXPRESS_CACHE);
                //获取网络请求数据
                getGetExpress(IS_GET_NO);
            }else{
                getExpressPRLV.onRefreshComplete();
                getExpressPRLV.onMoreComplete(false);
            }
        }else {
            //清除数据库
//            dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYEXPRESS_CACHE);
            //获取网络请求数据
            getGetExpress(IS_GET_NO);
        }
    }

    /**
     * 获取待取快递数据的方法
     */
    private void getUngetExpress(int isPullToRefresh){

        if (isPullToRefresh == 1){
            pageIndexUnget = 1;
        }else if (isPullToRefresh == 2){
            pageIndexUnget ++;
        }else{
            pageIndexUnget = 1;
        }
        mExpresslistInfo.setPageIndex(pageIndexUnget);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mExpresslistInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    if (mExpresslistInfo.getAllItems().size() == 0){
                        ungetExpressPRLV.setVisibility(View.GONE);
                        getExpressPRLV.setVisibility(View.GONE);
                        noDataRL.setVisibility(View.VISIBLE);
                        return;
                    }
                    ungetExpressListItemAdapter.clear();
                    if(mExpresslistInfo.getAllItems().size() < pageSize){
                        noUpLoadFinishUnget = false;
                    }else{
                        noUpLoadFinishUnget = true;
                    }
//                    if (mExpresslistInfo.getPageIndexStr().equals(mExpresslistInfo.getTotalPage())){
//                        noUpLoadFinishUnget = true;
//                    }else{
//                        noUpLoadFinishUnget = false;
//                    }
                    pageIndexUnget = mExpresslistInfo.getPageIndex();
                    //将有重复的信息去除
                    for(int i=0; i < mExpresslistInfo.getAllItems().size(); i++){
                        String mailno = mExpresslistInfo.getAllItems().get(i).getMailno();
                        for(int j=0; j < ungetExpressListItemAdapter.getCount(); j++){
                            String mailnoItemAdapter = ((ExpressListItem)ungetExpressListItemAdapter.getItem(j)).getMailno();
                            if(mailno.equals(mailnoItemAdapter)){
                                ungetExpressListItemAdapter.remove(j);
                            }
                        }
                    }
//                    if(isPullRefresh == 2){
                        ungetExpressListItemAdapter.addItems((List)mExpresslistInfo.getAllItems());
                        ungetExpressPRLV.onRefreshComplete();
                        ungetExpressPRLV.onMoreComplete(noUpLoadFinishUnget);
//                    }
                    ungetExpressListItemAdapter.notifyDataSetChanged();
                    ungetExpressPRLV.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {

                        @Override
                        public void onRefresh() {
                            // 线程获取更新数据
                            isPullRefresh = 1;
                            dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYEXPRESS_CACHE);
                            getUngetExpress(IS_GET_NEW);

                        }
                    });

                    ungetExpressPRLV.setOnMoreListener(new PullToRefreshListView.OnMoreListener() {

                        @Override
                        public void onMore() {
                            // 线程获取更多数据
                            isPullRefresh = 2;
                            getUngetExpress(IS_GET_MORE);

                        }
                    });

                    ungetExpressPRLV.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
//							Intent intent = new Intent(mContext, CompanyDetailsActy.class);
//							startActivity(intent);
                            if (position == ungetExpressListItemAdapter.getCount() + 1) {
                                return;
                            }
//                            ListView listView = (ListView) parent;
//                            ExpressListItem expressListItem = (ExpressListItem) listView.getItemAtPosition(position);
//                            String mailno = expressListItem.getMailno();
//                            Intent intentExpressDetail = new Intent(mContext, ExpressDetailActivity.class);
//                            intentExpressDetail.putExtra("expressListItem", expressListItem);
//                            intentExpressDetail.putExtra("mailno", mailno);
//                            startActivityForResult(intentExpressDetail, 0);

                        }
                    });
//					RecruitItem recruitItem = BaseInfo.gson.fromJson(jsonObject.getJSONObject("listArr").toString(), RecruitItem.class);
//					HWLog.e(TAG, "recruitItem----------------->"+allItems);
                } catch (Exception e) {
                    JKLog.e(TAG, "onRequestSuccess方法中-------->：");
                }
            }
        });

    }

    /**
     * 获取已取快递数据的方法
     */
    private void getGetExpress(int isPullToRefresh){

        if (isPullToRefresh == 1){
            pageIndexGet = 1;
        }else if (isPullToRefresh == 2){
            pageIndexGet ++;
        }else {
            pageIndexGet = 1;
        }
        mAlexpresslistInfo.setPageIndex(pageIndexGet);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mAlexpresslistInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    if (mAlexpresslistInfo.getAllItems().size() == 0){
                        ungetExpressPRLV.setVisibility(View.GONE);
                        getExpressPRLV.setVisibility(View.GONE);
                        noDataRL.setVisibility(View.VISIBLE);
                        return;
                    }
                    getExpressListItemAdapter.clear();
                    getExpressListItemAdapter.notifyDataSetChanged();
                    if(mExpresslistInfo.getAllItems().size() < pageSize){
                        noUpLoadFinishGet = false;
                    }else{
                        noUpLoadFinishGet = true;
                    }
//                    if (mAlexpresslistInfo.getPageIndexStr().equals(mAlexpresslistInfo.getTotalPage())){
//                        noUpLoadFinishGet = true;
//                    }else{
//                        noUpLoadFinishGet = false;
//                    }
                    pageIndexGet = mAlexpresslistInfo.getPageIndex();
                    //将有重复的信息去除
                    for(int i=0; i < mAlexpresslistInfo.getAllItems().size(); i++){
                        String mailno = mAlexpresslistInfo.getAllItems().get(i).getMailno();
                        for(int j=0; j < getExpressListItemAdapter.getCount(); j++){
                            String mailnoItemAdapter = ((ExpressListItem)getExpressListItemAdapter.getItem(j)).getMailno();
                            if(mailno.equals(mailnoItemAdapter)){
                                getExpressListItemAdapter.remove(j);
                            }
                        }
                    }
//                    if(isPullRefresh == 2){
                    getExpressListItemAdapter.addItems((List) mAlexpresslistInfo.getAllItems());
                    getExpressPRLV.onRefreshComplete();
                    getExpressPRLV.onMoreComplete(noUpLoadFinishGet);
//                    }
                    getExpressListItemAdapter.notifyDataSetChanged();
                    getExpressPRLV.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {

                        @Override
                        public void onRefresh() {
                            // 线程获取更新数据
                            isPullRefresh = 1;
                            dataCacheDBManage.clear(DataCacheDBHelper.TAB_MYEXPRESS_CACHE);
                            getGetExpress(IS_GET_NEW);

                        }
                    });

                    getExpressPRLV.setOnMoreListener(new PullToRefreshListView.OnMoreListener() {

                        @Override
                        public void onMore() {
                            // 线程获取更多数据
                            isPullRefresh = 2;
                            getGetExpress(IS_GET_MORE);

                        }
                    });

                    getExpressPRLV.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
//							Intent intent = new Intent(mContext, CompanyDetailsActy.class);
//							startActivity(intent);
                            if (position == getExpressListItemAdapter.getCount() + 1) {
                                return;
                            }
                            ListView listView = (ListView) parent;
//                            ExpressListItem expressListItem = (ExpressListItem) listView.getItemAtPosition(position);
//                            String mailno = expressListItem.getMailno();
//                            Intent intentExpressDetail = new Intent(mContext, ExpressDetailActivity_.class);
//                            intentExpressDetail.putExtra("expressListItem", expressListItem);
//                            intentExpressDetail.putExtra("mailno", mailno);
//                            startActivityForResult(intentExpressDetail, 0);
                        }
                    });
//					RecruitItem recruitItem = BaseInfo.gson.fromJson(jsonObject.getJSONObject("listArr").toString(), RecruitItem.class);
//					HWLog.e(TAG, "recruitItem----------------->"+allItems);
                } catch (Exception e) {
                    JKLog.e(TAG, "onRequestSuccess方法中-------->：");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //处理扫描结果（在界面上显示）
        if ((requestCode == Constant.StaticCode.REQUEST_SCANNING_BARCODE) && (resultCode == RESULT_OK)) {
            getUngetExpress(IS_GET_NO);
            getGetExpress(IS_GET_NO);
        }
        //根据点击DeliverySuccessActivity中返回列表进入该方法
//        if ((requestCode == Constant.StaticCode.REQUEST_SCANNING_BARCODE) && (resultCode == RESULT_CANCELED)) {
//            setResult(RESULT_CANCELED);
//            finish();
//        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.it_ibn_right:
//                //点击进入扫码取件界面——中间过度页去掉
//                Intent intentScanCodeDelivery = new Intent(this, ScanCodeDeliveryActivity_.class);
//                startActivityForResult(intentScanCodeDelivery, Constant.StaticCode.REQUEST_SCANNING_BARCODE);
                //打开扫描界面扫描条形码或二维码
                Intent openCameraIntent = new Intent(this, CaptureActivity.class);
                startActivityForResult(openCameraIntent, Constant.StaticCode.REQUEST_SCANNING_BARCODE);
                break;
        }
    }
}
