package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.AddOrderInfo;
import com.jiakang.pandlife.info.GetDefaultSenderAddressInfo;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.GoodsItem;
import com.jiakang.pandlife.item.GroupDetailStationItem;
import com.jiakang.pandlife.item.GroupPurchaseDetailItem;
import com.jiakang.pandlife.item.RedPackageItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.CustomDialog;
import com.jiakang.pandlife.widget.CustomListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.qqtheme.framework.picker.DatePicker;

/**
 * 团购订单
 */
@EActivity(R.layout.activity_group_purchase_indent)
public class GroupPurchaseIndentActivity extends BaseActy {
    protected static final String TAG = "GroupPurchaseIndentActivity";

    @ViewById(R.id.agpi_tv_receiver_name)
    TextView receiverNameTV;
    @ViewById(R.id.agpi_tv_receiver_phone)
    TextView receiverPhoneTV;
    @ViewById(R.id.agpi_tv_receiver_address)
    TextView receiverAddressTV;
    @ViewById(R.id.agpi_clv_goods)
    CustomListView goodsCLV;
    @ViewById(R.id.agpi_tv_site_name)
    TextView siteNameTV;
    @ViewById(R.id.include_iip_rgp_pay_type)
    RadioGroup payTypeRGP;
    @ViewById(R.id.include_iip_rbn_balance)
    RadioButton balanceRBN;
    @ViewById(R.id.include_iip_rbn_alipay)
    RadioButton alipayRBN;
    @ViewById(R.id.include_iip_tv_distribution_date_select)
    TextView distributionDateTV;
    @ViewById(R.id.include_iip_et_distribution_note)
    EditText distributionRemarksET;
    @ViewById(R.id.include_iip_tv_red_package_number)
    TextView redPackageNumberTV;
    @ViewById(R.id.include_iip_tv_red_package_content)
    TextView redPackageContentTV;
    @ViewById(R.id.include_iip_tv_sum_content)
    TextView sumTV;
    @ViewById(R.id.include_iip_tv_freight_content)
    TextView freightTV;
    @ViewById(R.id.agpi_tv_count)
    TextView countTV;
    @ViewById(R.id.agpi_btn_confirm)
    Button confirmBN;

    private ItemAdapter mItemAdapter;

    private List<GoodsItem> allItems = new ArrayList<GoodsItem>();
    private GroupPurchaseDetailItem mGroupPurchaseDetailItem;
    private GroupDetailStationItem mGroupDetailStationItem;

    private String PAY_WAY = "1";
    private AddressItem senderAddressItem;

    //如果选择支付宝等支付，默认为支付宝
    private String currentRechargeWay = "1";

    //记录实际需要付款金额
    private float realPaySum;
    //红包实体
    private RedPackageItem mRedPackageItem;
    //订单总金额
    private float countPrice = 0;
    //红包优惠金额
    private int redPackageSum = 0;
    //运费金额
    private int freightSum = 0;

    /** 提交团购订单接口 */
    private AddOrderInfo mAddOrderInfo = new AddOrderInfo();

    /**获取我的默认地址接口*/
    private GetDefaultSenderAddressInfo mGetDefaultSenderAddressInfo = new GetDefaultSenderAddressInfo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        mItemAdapter = new ItemAdapter(mContext);
        goodsCLV.setAdapter(mItemAdapter);

        findViewById(R.id.agpi_rl_address).setOnClickListener(this);
        distributionDateTV.setOnClickListener(this);
        redPackageContentTV.setOnClickListener(this);
        confirmBN.setOnClickListener(this);


        payTypeRGP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.include_iip_rbn_balance:
                        PAY_WAY = "account";
                        break;
                    case R.id.include_iip_rbn_alipay:
                        PAY_WAY = "Alipay";
                        break;
                    case R.id.include_iip_rbn_wechat:
                        PAY_WAY = "Wx";
                        break;
                    case R.id.include_iip_rbn_unionpay:
                        PAY_WAY = "UnionPay";
                        break;
                }
            }
        });


        bindView();
        initDefaultAddress();
    }

    private void bindView(){
        Intent intent = getIntent();
        mGroupPurchaseDetailItem = (GroupPurchaseDetailItem)intent.getExtras().get("groupPurchaseDetailItem");
        mGroupDetailStationItem = (GroupDetailStationItem)intent.getExtras().get("groupDetailStationItem");
        if (mGroupPurchaseDetailItem != null){
            initTitleBar(R.id.agpi_tb_title, "团购订单");
            mItemAdapter.add(mGroupPurchaseDetailItem);
            mItemAdapter.notifyDataSetChanged();

            countPrice = mGroupPurchaseDetailItem.getTotal() * mGroupPurchaseDetailItem.getTprice();
            sumTV.setText("￥" + countPrice);
            freightTV.setText("免运费");
            notifyCountSum();


            //当为团购时没有配送时间选择，隐藏之
            findViewById(R.id.include_iip_rl_distribution_date).setVisibility(View.GONE);
//            float countPrice = goodsItem.getNumber() * goodsItem.getPrice();
//            countPriceTV.setText("共" + goodsItem.getNumber() + "件商品 合计：" + countPrice + "元（含运费：￥0.00）");
        }
    }

    /**
     * 如果有默认地址就添加默认地址
     */
    private void initDefaultAddress(){
        ApiManager apiManagerDefault = ApiManager.getInstance();
        apiManagerDefault.request(mGetDefaultSenderAddressInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        AddressItem addressItem =  mGetDefaultSenderAddressInfo.getAddressItem();
                        receiverNameTV.setText(addressItem.getName());
                        receiverPhoneTV.setText(addressItem.getPhone());
                        receiverAddressTV.setText(addressItem.getAddress());

                    } else {
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    /**
     * 选择归还时间
     */
    public void onYearMonthDayPicker() {

        DatePicker picker = new DatePicker(this);
        picker.setRange(2000, 2016);
        picker.setSelectedItem(2015, 10, 10);
        picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
            @Override
            public void onDatePicked(String year, String month, String day) {

                distributionDateTV.setText(year + "-" + month + "-" + day);

//                Date date = null;
//                try {
//                    date = new SimpleDateFormat("yyyy-MM-dd").parse(year + "-" + month + "-" + day);
//
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
            }
        });
        picker.show();
    }

    /**
     * 选择使用哪个红包的方法
     */
    private void selectRedPackage(){
        Intent intentSelectRedPackage = new Intent(mContext, MyRedPackageManagerActivity_.class);
        intentSelectRedPackage.putExtra("gid", mGroupPurchaseDetailItem.getId());
        intentSelectRedPackage.putExtra("money", countPrice);
        intentSelectRedPackage.putExtra("type", "t");
        startActivityForResult(intentSelectRedPackage, Constant.StaticCode.REQUEST_SELECT_RED_PACKAGE);
    }

    /**
     * 确认团购方法
     */
    private void confirm(){
            final CustomDialog customDialog = Util.getDialog(mContext, "提示", "确定支付吗？", "确定", "取消");
            customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customDialog.dismiss();
                    //调用支付接口
                    addGroupPurchaseIndent();

                }
            });
            customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customDialog.dismiss();
                }
            });
            customDialog.show();

    }


    /**
     * 添加团购订单方法
     */
    private void addGroupPurchaseIndent(){
        mAddOrderInfo.setGid(mGroupPurchaseDetailItem.getId());
        mAddOrderInfo.setAid(senderAddressItem.getId());
        mAddOrderInfo.setStation_id(mGroupDetailStationItem.getStation_id());
        mAddOrderInfo.setNumber(mGroupPurchaseDetailItem.getTotal());
        mAddOrderInfo.setPay_kind(PAY_WAY);
        if(mRedPackageItem != null) {
            mAddOrderInfo.setHid(mRedPackageItem.getId());
        }
        mAddOrderInfo.setRemarks(distributionRemarksET.getText().toString());
        mAddOrderInfo.setPs_money(freightSum);
        mAddOrderInfo.setTotal_money((int) countPrice);
        mAddOrderInfo.setPay_money((int) (countPrice + freightSum - redPackageSum));
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mAddOrderInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try{
                    CustomToast.showToast(mContext, "提交成功");
                    String mailno = mAddOrderInfo.getMailno();
                    //这里调用ping++接口

//                    Intent intentSelectRechargeWay = new Intent(mContext, SelectPayWayActivity_.class);
//                    intentSelectRechargeWay.putExtra("rechargeWay", currentRechargeWay);
//                    intentSelectRechargeWay.putExtra("paySum", realPaySum);
//                    startActivityForResult(intentSelectRechargeWay, Constant.StaticCode.REQUEST_RECHARGE_WAY);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     *刷新实际支付金额
     */
    private void notifyCountSum(){
//            realPaySum = countPrice + 运费 - 红包；
        //计算实付金额
        realPaySum = countPrice + freightSum - redPackageSum;
        countTV.setText("需付款：￥" + realPaySum );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_GROUP_PURCHASE) && (resultCode == RESULT_OK)){
            setResult(RESULT_OK);
            finish();
        }
        if ((requestCode == Constant.StaticCode.REQUEST_SENDER_ADDRESS) && (resultCode == RESULT_OK)){
            senderAddressItem = (AddressItem)data.getExtras().get("senderAddressItem");
            receiverNameTV.setText(senderAddressItem.getName());
            receiverPhoneTV.setText(senderAddressItem.getPhone());
            receiverAddressTV.setText(senderAddressItem.getAddress());
        }
        if ((requestCode == Constant.StaticCode.REQUEST_SELECT_RED_PACKAGE) && (resultCode == RESULT_OK)){
            mRedPackageItem = (RedPackageItem)data.getExtras().get("redPackageItem");
            redPackageSum = mRedPackageItem.getMoney();
            redPackageContentTV.setText("优惠" + redPackageSum + "元");
            notifyCountSum();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.agpi_rl_address://点击选择收件人
                Intent intentSendAddress = new Intent(this, AddressBookManagerActivity_.class);
                intentSendAddress.putExtra("sendOrObtain", "sender");
                startActivityForResult(intentSendAddress, Constant.StaticCode.REQUEST_SENDER_ADDRESS);
                break;
            case R.id.include_iip_rl_distribution_date:
                onYearMonthDayPicker();
                break;
            case R.id.include_iip_tv_red_package_content://点击选择使用红包
                selectRedPackage();
                break;
            case R.id.agpi_btn_confirm://点击确认
                confirm();
                break;
        }
    }

}
