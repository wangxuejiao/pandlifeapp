package com.jiakang.pandlife.info;

import com.google.gson.Gson;
import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.Post;
import com.jiakang.pandlife.item.StationNotificationItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/1/12.
 */
public class StationNotificationInfo extends BaseAbsInfo {

    private StationNotificationItem stationNotificationItem;
    private List<StationNotificationItem.DataEntity> dataEntityStationNotificationList = new ArrayList<>();

    public StationNotificationItem getStationNotificationItem() {
        return stationNotificationItem;
    }

    public void setStationNotificationItem(StationNotificationItem stationNotificationItem) {
        this.stationNotificationItem = stationNotificationItem;
    }

    public List<StationNotificationItem.DataEntity> getDataEntityStationNotificationList() {
        return dataEntityStationNotificationList;
    }

    public void setDataEntityStationNotificationList(List<StationNotificationItem.DataEntity> dataEntityStationNotificationList) {
        this.dataEntityStationNotificationList = dataEntityStationNotificationList;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Server&a=Getnotice&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token", BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        stationNotificationItem = BaseInfo.gson.fromJson(jsonObject.toString(), StationNotificationItem.class);
        dataEntityStationNotificationList.clear();
        dataEntityStationNotificationList = stationNotificationItem.getData();
        for (int i = 0; i < dataEntityStationNotificationList.size(); i++) {
            dataEntityStationNotificationList.get(i).setLayout(R.layout.item_station_notification);
        }
    }
}
