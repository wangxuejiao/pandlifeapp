package com.jiakang.pandlife.acty.personalcenter;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.db.DataCacheDBHelper;
import com.jiakang.pandlife.db.DataCacheDBManage;
import com.jiakang.pandlife.info.MyRedPackInfo;
import com.jiakang.pandlife.item.ConsumptionDetailItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.RedPackageItem;
import com.jiakang.pandlife.utils.CustomToast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的红包
 * Created by Administrator on 2016/1/11.
 */
@EActivity(R.layout.activity_my_red_package)
public class MyRedPackageActivity extends BaseActy{
    protected static final String TAG = "MyRedPackageActivity";

//    @ViewById(R.id.amrp_rgp_label)
//    RadioGroup labelRGP;
//    @ViewById(R.id.amrp_rbn_no_past_due)
//    RadioButton noPastDueRBN;
//    @ViewById(R.id.amrp_rbn_already_past_due)
//    RadioButton alreadyPastDueRBN;
    @ViewById(R.id.amrp_lv_no_past_due)
    ListView noPastDueLV;
//    @ViewById(R.id.amrp_lv_already_past_due)
//    ListView alreadyPastDueLV;
    @ViewById(R.id.amrp_rl_no_data)
    RelativeLayout noDataRL;

    private ItemAdapter noPastDueItemAdapter;
//    private ItemAdapter alreadyPastDueItemAdapter;

    /** 我的红包接口 */
    private MyRedPackInfo mMyRedPackInfo = new MyRedPackInfo();

    private List<RedPackageItem> allItems = new ArrayList<RedPackageItem>();

    private DataCacheDBManage dataCacheDBManage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.amrp_tb_title, "我的红包");
        dataCacheDBManage = getMyApp().getCacheDataDBManage();
        initView();
        bindView();
    }

    private void initView(){
        noPastDueItemAdapter = new ItemAdapter(mContext);
        noPastDueLV.setAdapter(noPastDueItemAdapter);

        getCacheDataMyRedPackage();

//        alreadyPastDueItemAdapter = new ItemAdapter(mContext);
//        alreadyPastDueLV.setAdapter(alreadyPastDueItemAdapter);

//        labelRGP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                switch (checkedId){
//                    case R.id.amrp_rbn_no_past_due:
//                        noPastDueRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
//                        alreadyPastDueRBN.setTextColor(getResources().getColor(R.color.gray_dark));
//                        noPastDueLV.setVisibility(View.VISIBLE);
//                        alreadyPastDueLV.setVisibility(View.GONE);
//                        if (noPastDueItemAdapter.getCount() == 0){
//                            getNoPastDueRedPackage();
//                        }
//                        break;
//                    case R.id.amrp_rbn_already_past_due:
//                        alreadyPastDueRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
//                        noPastDueRBN.setTextColor(getResources().getColor(R.color.gray_dark));
//                        alreadyPastDueLV.setVisibility(View.VISIBLE);
//                        noPastDueLV.setVisibility(View.GONE);
//                        if (alreadyPastDueItemAdapter.getCount() == 0){
//                            getAlreadyPastDueRedPackage();
//                        }
//                        break;
//                }
//            }
//        });

    }
    private void bindView(){

    }

    /**
     * 通过数据库获取我的红包列表信息
     */
    private void getCacheDataMyRedPackage(){
        allItems = dataCacheDBManage.getGetMyRedPackageList();
        if (allItems.size() != 0){
            noPastDueItemAdapter.clear();
            noPastDueItemAdapter.addItems((List) allItems);
            noPastDueItemAdapter.notifyDataSetChanged();

            RedPackageItem redPackageItem = allItems.get(0);
            if ((redPackageItem.getCurrentTime() + redPackageItem.getSaveTime()) < System.currentTimeMillis()) {
                //清除数据库
                dataCacheDBManage.clear(DataCacheDBHelper.TAB_MY_REDPACKAGE_CACHE);
                //获取网络请求数据
                getNoPastDueRedPackage();
            } else {
            }
        }else{
            getNoPastDueRedPackage();
        }
    }

    /**
     * 获取未过期红包数据
     */
    private void getNoPastDueRedPackage(){
        allItems.clear();
        noPastDueItemAdapter.clear();
//        BaseInfo.token = "ff0a6c74e7b4d08b659f9e2737a4ac5c";
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mMyRedPackInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try{
                    allItems = mMyRedPackInfo.getAllItems();
                    if (allItems.size() == 0){
                        CustomToast.showToast(mContext, "您当前没有红包");
                        noPastDueLV.setVisibility(View.GONE);
                        noDataRL.setVisibility(View.VISIBLE);
                        return;
                    }

                    noPastDueItemAdapter.addItems((List)allItems);
                    noPastDueItemAdapter.notifyDataSetChanged();
                }catch (Exception e){

                }
            }
        });
    }

    /**
     * 获取已过期红包数据
     */
//    private void getAlreadyPastDueRedPackage(){
//
//    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
