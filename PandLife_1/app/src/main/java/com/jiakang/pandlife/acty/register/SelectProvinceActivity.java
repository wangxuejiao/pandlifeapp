package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.express.ExpressDetailActivity;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.ProvinceInfo;
import com.jiakang.pandlife.item.NearbyMerchantItem;
import com.jiakang.pandlife.item.ProvinceItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.List;

/**
 * 选择省份
 */
@EActivity(R.layout.activity_select_province)
public class SelectProvinceActivity extends BaseActy {
    protected static final String TAG = "SelectProvinceActivity";

    @ViewById(R.id.asp_lv_province)
    ListView provinceLV;

    private ItemAdapter mItemAdapter;
    /** 选择省接口 */
    private ProvinceInfo mProvinceInfo = new ProvinceInfo();

    private String siteOrCommunity = "community_and_site";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.asp_tb_title, "选择省");
        bindView();
        bindInfo();
    }

    private void bindView(){
        mItemAdapter = new ItemAdapter(mContext);
        provinceLV.setAdapter(mItemAdapter);

        Intent intent = getIntent();
        siteOrCommunity = intent.getExtras().getString(SelectLocationOrManualActivity.SITE_OR_COMMUNITY);
    }

    private void bindInfo(){
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mProvinceInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        if (mProvinceInfo.getAllItems().size() == 0) {
                            CustomToast.showToast(mContext, "未获取到省信息，请重新获取");
                            return;
                        }
                        mItemAdapter.addItems((List) mProvinceInfo.getAllItems());
                        mItemAdapter.notifyDataSetChanged();

                        provinceLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                ListView listView = (ListView) parent;
                                ProvinceItem provinceItem = (ProvinceItem) listView.getItemAtPosition(position);
                                Intent intentCity = new Intent(mContext, SelectCityActivity_.class);
                                intentCity.putExtra("provinceItem", provinceItem);
                                intentCity.putExtra(SelectLocationOrManualActivity.SITE_OR_COMMUNITY, siteOrCommunity);
                                startActivityForResult(intentCity, Constant.StaticCode.REQUSET_BIND_SITE);
                            }
                        });

                    } else {
                        JKLog.i(TAG, "失败");
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUSET_BIND_SITE) && (resultCode == RESULT_OK)){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
