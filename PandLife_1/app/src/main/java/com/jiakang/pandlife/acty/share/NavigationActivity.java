//package com.jiakang.pandlife.acty.share;
//
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.View;
//
//import com.baidu.location.BDLocation;
//import com.baidu.location.BDLocationListener;
//import com.baidu.location.LocationClient;
//import com.baidu.location.LocationClientOption;
//import com.baidu.mapapi.map.BaiduMap;
//import com.baidu.mapapi.map.BitmapDescriptor;
//import com.baidu.mapapi.map.MapStatusUpdate;
//import com.baidu.mapapi.map.MapStatusUpdateFactory;
//import com.baidu.mapapi.map.MapView;
//import com.baidu.mapapi.map.MyLocationConfiguration;
//import com.baidu.mapapi.map.MyLocationData;
//import com.baidu.mapapi.model.LatLng;
//import com.jiakang.pandlife.R;
//import com.jiakang.pandlife.acty.BaseActy;
//
//
///**
// * 导航
// */
//public class NavigationActivity extends BaseActy{
//    protected static final String TAG = "NavigationActivity";
//
//    private MapView mapMV = null;
//    private BaiduMap mBaiduMap = null;
//    // 定位相关
//    LocationClient mLocClient;
//    private MyLocationConfiguration.LocationMode mCurrentMode;
//    BitmapDescriptor mCurrentMarker;
//    public MyLocationListenner myListener = new MyLocationListenner();
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_navigation);
//
//        initTitleBar(R.id.ana_tb_title, "导航");
//        mapMV = (MapView)findViewById(R.id.fjcs_mv_bmapView);
//        mBaiduMap = mapMV.getMap();
//        // 开启定位图层
//        mBaiduMap.setMyLocationEnabled(true);
//        // 定位初始化
//        mLocClient = new LocationClient(this);
//        mLocClient.registerLocationListener(myListener);
//        mCurrentMode = MyLocationConfiguration.LocationMode.NORMAL;
//        mBaiduMap
//                .setMyLocationConfigeration(new MyLocationConfiguration(
//                        mCurrentMode, true, mCurrentMarker));
//        LocationClientOption option = new LocationClientOption();
//        option.setOpenGps(true);// 打开gps
//        option.setCoorType("bd09ll"); // 设置坐标类型
//        option.setScanSpan(1000);
//        mLocClient.setLocOption(option);
//        mLocClient.start();
//        LatLng ll = new LatLng(11.4646613131,
//                11.4646613131);
//        MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
//        mBaiduMap.animateMapStatus(u);
//    }
//    /**
//     * 定位SDK监听函数
//     */
//    public class MyLocationListenner implements BDLocationListener {
//
//        @Override
//        public void onReceiveLocation(BDLocation location) {
//            // map view 销毁后不在处理新接收的位置
//            if (location == null || mapMV == null)
//                return;
//            MyLocationData locData = new MyLocationData.Builder()
//                    .accuracy(location.getRadius())
//                            // 此处设置开发者获取到的方向信息，顺时针0-360
//                    .direction(100).latitude(location.getLatitude())
//                    .longitude(location.getLongitude()).build();
//            mBaiduMap.setMyLocationData(locData);
//                LatLng ll = new LatLng(location.getLatitude(),
//                        location.getLongitude());
//                MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
//                mBaiduMap.animateMapStatus(u);
//        }
//
//        public void onReceivePoi(BDLocation poiLocation) {
//        }
//    }
//
//    @Override
//    protected void onPause() {
//        mapMV.onPause();
//        super.onPause();
//    }
//
//    @Override
//    protected void onResume() {
//        mapMV.onResume();
//        super.onResume();
//    }
//
//    @Override
//    protected void onDestroy() {
//        // 退出时销毁定位
//        mLocClient.stop();
//        // 关闭定位图层
//        mBaiduMap.setMyLocationEnabled(false);
//        mapMV.onDestroy();
//        mapMV = null;
//        super.onDestroy();
//    }
//
//    @Override
//    public void onClick(View v) {
//        super.onClick(v);
//        switch (v.getId()){
//            case R.id.it_ibn_left:
//                finish();
//                break;
//        }
//    }
//}
