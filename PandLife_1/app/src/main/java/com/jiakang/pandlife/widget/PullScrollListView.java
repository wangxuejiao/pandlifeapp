package com.jiakang.pandlife.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AbsListView;
import android.widget.Adapter;

import com.jiakang.pandlife.utils.JKLog;

import greendroid.widgetww.PullToRefreshListView;


/**
 * @author：xuwangwang E-mail: 541765907@qq.com
 * @date：2013-12-25 下午3:14:22
 * @version 1.0 String Tag = "PullToRefreshSoundListView中：";
 */
public class PullScrollListView extends PullToRefreshListView {
	private String TAG = "PullScrollListView";
	private Context mContext;

	public PullScrollListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.mContext = context;
	}

	public PullScrollListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.mContext = context;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisiableItem, int visibleItemCount, int totalItemCount) {
		super.onScroll(view, firstVisiableItem, visibleItemCount, totalItemCount);
		JKLog.e(TAG, "onScroll方法中-------->：firstVisiableItem为：" + firstVisiableItem);
		JKLog.e(TAG, "onScroll方法中-------->：visibleItemCount为：" + visibleItemCount);
		JKLog.e(TAG, "onScroll方法中-------->：totalItemCount为：" + totalItemCount);
		int visibleLastIndex = firstVisiableItem + visibleItemCount - 1;
		Adapter adapter = getAdapter();
		if (adapter == null)
			return;
		if (adapter.getCount() - 1 == visibleLastIndex) {
			if (onScrollListener != null) {
				onScrollListener.onScroll(firstVisiableItem, visibleItemCount, totalItemCount);
			}
		} else {

		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		super.onScrollStateChanged(view, scrollState);
	}

	private OnScrollListener onScrollListener;

	public OnScrollListener getOnScrollListener() {
		return onScrollListener;
	}

	public void setOnScrollListener(OnScrollListener onScrollListener) {
		this.onScrollListener = onScrollListener;
	}

	public interface OnScrollListener {
		void onScroll(int firstVisiableItem, int visibleItemCount, int totalItemCount);
	}

}
