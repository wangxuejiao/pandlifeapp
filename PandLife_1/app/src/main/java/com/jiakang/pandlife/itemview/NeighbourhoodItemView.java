package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.NeibourhoodItem;
import com.jiakang.pandlife.item.NeighborhoodItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

/**
 * Created by play on 2016/1/7.
 */
public class NeighbourhoodItemView extends AbsLinearLayout {

    private TextView tvType;
    private TextView tvTitle;
    private TextView tvContent;
    private ImageView ivHead;
    private TextView tvUser;
    private ImageView imageView;
    private TextView tvSupportCount;
    private TextView tvCommentCount;

    public NeighbourhoodItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NeighbourhoodItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        tvType = (TextView) findViewById(R.id.tv_type_neighbourhood);
        tvTitle = (TextView) findViewById(R.id.tv_title_neighbourhood);
        tvContent = (TextView) findViewById(R.id.tv_content_neighbourhood);
        ivHead = (ImageView) findViewById(R.id.iv_head_neighbourhood);
        tvUser = (TextView) findViewById(R.id.tv_user_neighbourhood);
        imageView = (ImageView) findViewById(R.id.iv_neighbourhood);
        tvSupportCount = (TextView) findViewById(R.id.tv_support_count_neighbourhood);
        tvCommentCount = (TextView) findViewById(R.id.tv_comment_count_neighbourhood);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        NeibourhoodItem.DataEntityNeibourhood neighbourhoodItem = (NeibourhoodItem.DataEntityNeibourhood )item;
        tvType.setText(neighbourhoodItem.getType());
        tvTitle.setText(neighbourhoodItem.getTitle());
        tvContent.setText(neighbourhoodItem.getDes());
        ImageLoaderUtil.displayImageCircleCache(neighbourhoodItem.getUserinfo().getHead(), ivHead, R.mipmap.ic_head);
        if(TextUtils.isEmpty(neighbourhoodItem.getUserinfo().getNick())){
            tvUser.setText("匿名用户");
        }else {
            tvUser.setText(neighbourhoodItem.getUserinfo().getNick());
        }
        if(TextUtils.isEmpty(neighbourhoodItem.getImg())){
            imageView.setVisibility(GONE);
        }else {
            imageView.setVisibility(VISIBLE);
            ImageLoaderUtil.displayImage(neighbourhoodItem.getImg(), imageView,R.mipmap.default_image);
        }
        tvSupportCount.setText(neighbourhoodItem.getSupportnum());
        tvCommentCount.setText(neighbourhoodItem.getReplynum());
    }
}
