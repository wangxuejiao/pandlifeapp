package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 团购商品详情实体
 * Created by Administrator on 2016/1/11.
 */
public class GroupPurchaseDetailItem extends Item{

    /** 产品id	Int(10) */
    private int id;
    /** 分类id(预留)	Int(10) */
    private int cid;
    /** 产品名称	Varchar(255) */
    private String title;
    /** 图片	Varchar(255) */
    private String pic;
    /** 规格	textarea */
    private String format;
    /** 商品详细信息	textarea */
    private String detailed;
    /** 总数	Int(10) */
    private int total;
    /** 售价	Int(10) */
    private int price;
    /** 团购价	Int(10) */
    private int tprice;
    /** 团购商品添加时间	Varchar(32) */
    private String createtime;
    /** 开团时间	Varchar(32) */
    private String starttime;
    /** 团购结束时间	Varchar(32) */
    private String endtime;

    private List<GroupDetailStationItem> station = new ArrayList<GroupDetailStationItem>();

    private int layout = R.layout.item_group_purchase_indent;

    @Override
    public int getItemLayoutId() {
        return layout;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getDetailed() {
        return detailed;
    }

    public void setDetailed(String detailed) {
        this.detailed = detailed;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getTprice() {
        return tprice;
    }

    public void setTprice(int tprice) {
        this.tprice = tprice;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public List<GroupDetailStationItem> getStation() {
        return station;
    }

    public void setStation(List<GroupDetailStationItem> station) {
        this.station = station;
    }
}
