package com.jiakang.pandlife.acty;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.jiakang.pandlife.R;

public class WebViewActivity extends BaseActy {

    private WebView webView;

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        initView();
        initData();
        setView();
    }

    private void initView() {

        initTitleBar(R.id.al_tb_title,"熊猫快收");

        webView = (WebView) findViewById(R.id.web_view);
        //设置WebView属性，能够执行Javascript脚本
        webView.getSettings().setJavaScriptEnabled(true);
    }

    private void initData(){

        url = getIntent().getExtras().getString("url");
    }

    private void setView(){

        webView.loadUrl(url);

        //覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                return true;
            }
        });

    }

    public static void startWebView(Context context,String url){

        Intent intent = new Intent(context,WebViewActivity.class);
        intent.putExtra("url",url);
        context.startActivity(intent);
    }


}
