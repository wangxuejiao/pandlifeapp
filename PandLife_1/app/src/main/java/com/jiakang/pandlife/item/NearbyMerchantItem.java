package com.jiakang.pandlife.item;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.annotation.sqlite.Id;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.db.DataCacheDBHelper;

/**
 * Created by Administrator on 2016/1/12.
 */
@Table(name = DataCacheDBHelper.TAB_NEARBY_MERCHANT_CACHE)
public class NearbyMerchantItem extends Item{

    /** 数据库id	Int(10) */
    @Id
    private int _id;
    /** 编号id	Int(10) */
    private int id;
    /** 商家昵称	Varchar(32) */
    private String nick;
    /** 商家图片	Varchar(255) */
    private String head;
    /** 商家简介	Varchar(255) */
    private String description;
    /** 营业时间	Varchar(32) */
    private String opentime;
    /** 销量	Int(10) */
    private int count;
    /** 配送费	Int(10) */
    private int ps_money;
    /**  */
    private String manageKind;

    /** 插入数据库时的时间 */
    private long currentTime = System.currentTimeMillis();
    /** 保存的时间 */
    private long saveTime = 86400000;

    private int layout = R.layout.item_nearby_merchant;
    @Override
    public int getItemLayoutId() {
        return layout;
    }

    @Override
    public int insert() {
        PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
        return 1;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOpentime() {
        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getManageKind() {
        return manageKind;
    }

    public void setManageKind(String manageKind) {
        this.manageKind = manageKind;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public long getSaveTime() {
        return saveTime;
    }

    public void setSaveTime(long saveTime) {
        this.saveTime = saveTime;
    }

    public int getPs_money() {
        return ps_money;
    }

    public void setPs_money(int ps_money) {
        this.ps_money = ps_money;
    }
}
