package com.jiakang.pandlife.acty.homepage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jiakang.pandlife.R;

import org.androidannotations.annotations.EActivity;

/**
 *好货详情
 */
@EActivity(R.layout.activity_goods_detail)
public class GoodsDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
