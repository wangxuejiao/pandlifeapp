package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.SiteItem;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 获取我的站点接口
 *
 * @author ww
 *
 */
public class MyStationInfo extends BaseAbsInfo {

    private static final String TAG = "MyStationInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private SiteItem mSiteItem;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=User&a=MyStation" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                data = (JSONObject)jsonObject.get("data");
                mSiteItem = BaseInfo.gson.fromJson(data.toString(), SiteItem.class);
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }



    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public SiteItem getSiteItem() {
        return mSiteItem;
    }

    public void setSiteItem(SiteItem siteItem) {
        mSiteItem = siteItem;
    }
}
