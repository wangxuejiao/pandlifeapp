package com.jiakang.pandlife.acty.express;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.app.ActionBar.LayoutParams;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.homepage.AddressBookManagerActivity_;
import com.jiakang.pandlife.acty.homepage.SelectSingleDataActivity;
import com.jiakang.pandlife.acty.homepage.SelectSingleDataActivity_;
import com.jiakang.pandlife.acty.service.NearbyStationActivity;
import com.jiakang.pandlife.acty.service.NearbyStationActivity_;
import com.jiakang.pandlife.acty.share.SelectNearbySiteActivity;
import com.jiakang.pandlife.acty.share.SelectNearbySiteActivity_;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.GetdeliveryInfo;
import com.jiakang.pandlife.info.NearbyStationInfo;
import com.jiakang.pandlife.info.SendbagInfo;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.DeliveryItem;
import com.jiakang.pandlife.item.NearbyStationItem;
import com.jiakang.pandlife.item.SiteItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.widget.BottomDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 寄快递
 */
@EActivity(R.layout.activity_sender_express)
public class SenderExpressActivity extends BaseActy {

    @ViewById(R.id.ase_tv_selet_site)
    TextView selectSiteTV;
    @ViewById(R.id.ase_ll_nearby_site)
    LinearLayout nearbyStieLL;
    @ViewById(R.id.ase_iv_nearby_station)
    ImageView nearbyStieIV;
    @ViewById(R.id.ase_tv_nearby_station_name)
    TextView nearbyStieNameTV;
    @ViewById(R.id.ase_tv_nearby_station_address)
    TextView nearbyStieAddressTV;
    @ViewById(R.id.ase_tv_nearby_station_phone)
    TextView nearbyStiePhoneTV;
    @ViewById(R.id.ase_tv_send_info)
    TextView sendInfoTV;
    @ViewById(R.id.ase_rl_sender)
    RelativeLayout senderRL;
    @ViewById(R.id.ase_tv_sender_name)
    TextView senderNameTV;
    @ViewById(R.id.ase_tv_sender_phone)
    TextView senderPhoneTV;
    @ViewById(R.id.ase_tv_sender_address)
    TextView senderAddressTV;
    @ViewById(R.id.ase_tv_obtain_info)
    TextView obtainInfoTV;
    @ViewById(R.id.ase_rl_receiver)
    RelativeLayout receiverRL;
    @ViewById(R.id.ase_tv_receiver_name)
    TextView receiverNameTV;
    @ViewById(R.id.ase_tv_receiver_phone)
    TextView receiverPhoneTV;
    @ViewById(R.id.ase_tv_receiver_address)
    TextView receiverAddressTV;
    @ViewById(R.id.ase_tv_select_pay_type)
    TextView payTypeTV;
    @ViewById(R.id.ase_tv_express_type_select)
    TextView expressTypeTV;
    @ViewById(R.id.ase_tv_goods_kind_select)
    TextView goodsKindTV;
    @ViewById(R.id.ase_et_goods_number)
    EditText goodsNumberET;
    @ViewById(R.id.ase_et_support_value)
    EditText supportValueET;
    @ViewById(R.id.ase_iv_question)
    ImageView questionIV;
    @ViewById(R.id.ase_et_remarks_info)
    EditText remarkInfoET;
    @ViewById(R.id.ase_btn_submit)
    Button submitBN;

    /** 记录商品数目的 */
//    private int goodsNumber = 0;
    private String payType = "f";
    private AddressItem senderAddressItem;
    private AddressItem recipientsAddressItem;

    private DeliveryItem mDeliveryItem;
    private List<DeliveryItem> mDeliveryItems = new ArrayList<DeliveryItem>();
    private List<String> delivery_type;
    private int delivery_id;
    private int sid = 0;
    private int jid = 0;
    private int station = 0;

    /**获取快递信息接口*/
    private GetdeliveryInfo mGetdeliveryInfo = new GetdeliveryInfo();
    /**寄快递接口*/
    private SendbagInfo mSendbagInfo = new SendbagInfo();

    public static final String SELECT_SITE = "select_site";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.ase_tb_title, "寄快递");

        initView();
        getExpressInfo();
    }

    private void initView(){
        selectSiteTV.setOnClickListener(this);
        sendInfoTV.setOnClickListener(this);
        obtainInfoTV.setOnClickListener(this);
        payTypeTV.setOnClickListener(this);
        expressTypeTV.setOnClickListener(this);
        goodsKindTV.setOnClickListener(this);
        questionIV.setOnClickListener(this);
        submitBN.setOnClickListener(this);
        //默认寄付
        payTypeTV.setText("寄付");
    }

    private void getExpressInfo(){
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mGetdeliveryInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        mDeliveryItems = mGetdeliveryInfo.getAllItems();
                        delivery_type = mGetdeliveryInfo.getDelivery_type();
                    } else {
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    /**
     * 选择付款方式方法
     */
    private void selectPayType(){
        View view = LayoutInflater.from(this).inflate(R.layout.dlg_select_two, null);
        Button manBN = (Button)view.findViewById(R.id.dsirj_btn_yes);
        Button womanBN = (Button)view.findViewById(R.id.dsirj_btn_no);
        manBN.setText("寄付");
        womanBN.setText("到付");
        final BottomDialog bottomDialog = new BottomDialog(view, LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        bottomDialog.show();
        manBN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payTypeTV.setText("寄付");
                payType = "f";
                bottomDialog.dismiss();
            }
        });
        womanBN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payTypeTV.setText("到付");
                payType = "d";
                bottomDialog.dismiss();
            }
        });
    }

    /**
     * 选择快递类型方法
     */
    private void selectExpressType(){
        Intent intentSelectGoodsKind = new Intent(mContext, SelectSingleDataActivity_.class);
        intentSelectGoodsKind.putExtra("title", "选择产品类型");
        intentSelectGoodsKind.putExtra("list", (Serializable) mDeliveryItems);
        startActivityForResult(intentSelectGoodsKind, Constant.StaticCode.REQUEST_SELECT_GOODS_TYPE);
    }

    /**
     * 选择产品种类方法
     */
    private void selectGoodsKind(){
        Intent intentSelectGoodsKind = new Intent(mContext, SelectSingleDataActivity_.class);
        intentSelectGoodsKind.putExtra("title", "选择物品种类");
        intentSelectGoodsKind.putExtra("data", delivery_type.toString());
        startActivityForResult(intentSelectGoodsKind, Constant.StaticCode.REQUEST_SELECT_GOODS_KIND);
    }

    /**
     * 提交寄件
     */
    private void submitExpress(){

        String bag_kind = goodsKindTV.getText().toString();
        int nums = 0;
        int supvalue = 0;
        String numsStr = goodsNumberET.getText().toString();
        if (!TextUtils.isEmpty(numsStr)){
            nums = Integer.parseInt(numsStr);
        }
        String supvalueStr = supportValueET.getText().toString();
        if (!TextUtils.isEmpty(supvalueStr)){
            supvalue = Integer.parseInt(supvalueStr);
        }
        String remarks = remarkInfoET.getText().toString();

        if (station == 0){
            CustomToast.showToast(mContext, "请选择站点");
            return;
        }
        if (sid == 0){
            CustomToast.showToast(mContext, "请选择收件人地址");
            return;
        }
        if (jid == 0){
            CustomToast.showToast(mContext, "请选择寄件人地址");
            return;
        }
        if (delivery_id == 0){
            CustomToast.showToast(mContext, "请选择快递类型");
            return;
        }
        if (TextUtils.isEmpty(bag_kind)){
            CustomToast.showToast(mContext, "请选择物品类型");
            return;
        }
        if (TextUtils.isEmpty(numsStr)){
            CustomToast.showToast(mContext, "请填写商品数量");
            return;
        }
        if (TextUtils.isEmpty(supvalueStr)){
            CustomToast.showToast(mContext, "请填写保价金额");
            return;
        }
        if (TextUtils.isEmpty(remarks)){
            CustomToast.showToast(mContext, "请填写备注");
            return;
        }

        if (senderAddressItem != null){
//            jid = senderAddressItem.get
        }

        mSendbagInfo.setJid(jid);
        mSendbagInfo.setSid(sid);
        mSendbagInfo.setStation(station);
        mSendbagInfo.setDelivery_id(delivery_id);
        mSendbagInfo.setBag_kind(bag_kind);
        mSendbagInfo.setNums(nums);
        mSendbagInfo.setPaytype(payType);
        mSendbagInfo.setSupvalue(supvalue);
        mSendbagInfo.setRemarks(remarks);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mSendbagInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        CustomToast.showToast(mContext,"添加成功");
                        finish();
                    } else {
                    }
                } catch (Exception e) {

                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_SENDER_ADDRESS) && (resultCode == RESULT_OK)){
            senderAddressItem = (AddressItem)data.getExtras().get("senderAddressItem");
            senderRL.setVisibility(View.VISIBLE);
            senderNameTV.setText(senderAddressItem.getName());
            senderPhoneTV.setText(senderAddressItem.getPhone());
            senderAddressTV.setText(senderAddressItem.getAddress());
            jid = senderAddressItem.getId();
        }
        if ((requestCode == Constant.StaticCode.REQUEST_RECEIVER_ADDRESS) && (resultCode == RESULT_OK)){
            recipientsAddressItem = (AddressItem)data.getExtras().get("recipientsAddressItem");
            receiverRL.setVisibility(View.VISIBLE);
            receiverNameTV.setText(recipientsAddressItem.getName());
            receiverPhoneTV.setText(recipientsAddressItem.getPhone());
            receiverAddressTV.setText(recipientsAddressItem.getAddress());
            sid = recipientsAddressItem.getId();
        }
        if ((requestCode == Constant.StaticCode.REQUEST_SELECT_GOODS_KIND) && (resultCode == RESULT_OK)){
            goodsKindTV.setText(data.getExtras().getString("data"));
        }
        if ((requestCode == Constant.StaticCode.REQUEST_SELECT_GOODS_TYPE) && (resultCode == RESULT_OK)){
            DeliveryItem deliveryItem = (DeliveryItem)data.getExtras().get("deliveryItem");
            expressTypeTV.setText(deliveryItem.getName());
            delivery_id = deliveryItem.getTaobao_code();
        }
        if ((requestCode == Constant.StaticCode.REQUEST_SELECT_SITE) && (resultCode == RESULT_OK)){
            SiteItem siteItem = (SiteItem)data.getExtras().get(SelectNearbySiteActivity.SELECT_NEARBY_SITE);
            nearbyStieLL.setVisibility(View.VISIBLE);
            ImageLoaderUtil.displayImage(siteItem.getPic(), nearbyStieIV);
            nearbyStieNameTV.setText(siteItem.getNice_name());
            nearbyStieAddressTV.setText(siteItem.getAddress());
            nearbyStiePhoneTV.setText(siteItem.getMobiphone());
            station = siteItem.getShopid();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.ase_tv_send_info://点击选择寄件人地址
                Intent intentSendAddress = new Intent(this, AddressBookManagerActivity_.class);
                intentSendAddress.putExtra("sendOrObtain", "sender");
                startActivityForResult(intentSendAddress, Constant.StaticCode.REQUEST_SENDER_ADDRESS);
                break;
            case R.id.ase_tv_obtain_info://点击选择收件人地址
                Intent intentObtainAddress = new Intent(this, AddressBookManagerActivity_.class);
                intentObtainAddress.putExtra("sendOrObtain", "recipients");
                startActivityForResult(intentObtainAddress, Constant.StaticCode.REQUEST_RECEIVER_ADDRESS);
                break;
            case R.id.ase_tv_selet_site://点击选择站点
                Intent intentSelectSite = new Intent(this, SelectNearbySiteActivity_.class);
                startActivityForResult(intentSelectSite, Constant.StaticCode.REQUEST_SELECT_SITE);
                break;
            case R.id.ase_tv_select_pay_type://点击选择付款方式
                selectPayType();
                break;
            case R.id.ase_tv_express_type_select://点击选择快递类型
                selectExpressType();
                break;
            case R.id.ase_tv_goods_kind_select://点击选择商品种类
                selectGoodsKind();
                break;
            case R.id.ase_iv_question://点击疑问
                break;
            case R.id.ase_btn_submit://点击提交
                submitExpress();
                break;
        }
    }

}
