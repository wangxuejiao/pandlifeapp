package com.jiakang.pandlife.acty;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.utils.DateUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_jpush_notification_detail)
public class JpushNotificationDetailActivity extends BaseActy {

    @ViewById(R.id.tv_title_jpush_notification_detail)
    TextView tvTitle;
    @ViewById(R.id.tv_date_jpush_notification_detail)
    TextView tvDate;
    @ViewById(R.id.tv_content_jpush_notification_detail)
    TextView tvContent;

    private String title;
    private String message;
    private Long time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void afterViewCreate() {

        initTitleBar(R.id.al_tb_title,"消息详情");
        initData();
        setView();

    }

    private void setView() {
        tvTitle.setText(title);
        tvContent.setText(message);
        tvDate.setText(DateUtils.getDateStringyyyyMMddHH(time));
    }

    private void initData() {

        title = getIntent().getStringExtra("title");
        message = getIntent().getStringExtra("message");
        time = getIntent().getExtras().getLong("time");
    }



}
