package com.jiakang.pandlife.item;


import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.db.DataCacheDBHelper;

@Table(name = DataCacheDBHelper.TAB_ADVERTISEMENT_CACHE)
public class AdvertisementItem extends Item{

	/** String	id */
	private String id;
	/** String	图片地址 */
	private String img_path;
	/** String	排序 */
	private String ordernum;
	/** String	跳转链接 */
	private String jump_url;
	/** String	是否跳转 */
	private String is_jump;
	/** String	保存在手机上的图片地址 */
	private String picPath;
	
//	private int layoutId = R.layout.item_array_salary;
	
	@Override
	public int getItemLayoutId() {
		return 0;
	}
	
	@Override
	public int insert() {
		PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
		return 1;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImg_path() {
		return img_path;
	}

	public void setImg_path(String img_path) {
		this.img_path = img_path;
	}

	public String getOrdernum() {
		return ordernum;
	}

	public void setOrdernum(String ordernum) {
		this.ordernum = ordernum;
	}

	public String getJump_url() {
		return jump_url;
	}

	public void setJump_url(String jump_url) {
		this.jump_url = jump_url;
	}

	public String getIs_jump() {
		return is_jump;
	}

	public void setIs_jump(String is_jump) {
		this.is_jump = is_jump;
	}

//	public int getLayoutId() {
//		return layoutId;
//	}
//
//	public void setLayoutId(int layoutId) {
//		this.layoutId = layoutId;
//	}

	public String getPicPath() {
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}
	
}
