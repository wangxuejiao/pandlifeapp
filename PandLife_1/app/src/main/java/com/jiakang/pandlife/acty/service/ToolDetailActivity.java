package com.jiakang.pandlife.acty.service;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.StationDetailInfo;
import com.jiakang.pandlife.info.ToolRentDetailInfo;
import com.jiakang.pandlife.item.PostDetail;
import com.jiakang.pandlife.item.StationDetail;
import com.jiakang.pandlife.item.ToolRentDetail;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.DataCache;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.CustomDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

@EActivity(R.layout.activity_tool_detail)
public class ToolDetailActivity extends BaseActy {

    private static final String TAG = "ToolDetailActivity";

    @ViewById(R.id.iv_tool_detail)
    ImageView ivToolDetail;
    @ViewById(R.id.tv_available_count_tool_detail)
    TextView tvAvailableCount;
    //子标题
    @ViewById(R.id.tv_name_tool_detail)
    TextView tvToolName;
    @ViewById(R.id.tv_feature_tool_detail)
    TextView tvFeature;
    @ViewById(R.id.tv_content_tool_detail)
    TextView tvContent;
    //租借信息
    @ViewById(R.id.tv_rent_tool_detail)
    TextView tvRent;
    //押金详情
    @ViewById(R.id.tv_deposit_detail_tool_detail)
    TextView tvDepositDetail;
    //押金
    @ViewById(R.id.tv_deposit_tool_detail)
    TextView tvDeposit;
    @ViewById(R.id.btn_rent_tool_detail)
    Button btnRent;

    @ViewById(R.id.tv_station_rent_tool_detail)
    TextView tvStation;
    @ViewById(R.id.tv_address_tool_rent_detail)
    TextView tvAddress;
    @ViewById(R.id.phone_tool_rent_detail)
    TextView tvPhone;

    private String toolId;

    //获取工具详情类
    private ToolRentDetailInfo mToolRentDetailInfo = new ToolRentDetailInfo();
    private ToolRentDetail.DataEntity mToolRentDetailEntity;


    //站点详情类
    private StationDetailInfo mStationDetailInfo = new StationDetailInfo();
    private StationDetail.DataEntityStationDetail mDataEntityStationDetail;

    ApiManager apiManager = ApiManager.getInstance();

    DataCache mDataCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @AfterViews
    protected void afterCreateView() {

        initTitleBar(R.id.al_tb_title, "工具详情", null, null);

        initData();
    }

    /**
     * 请求到数据后填充控件数据
     */
    public void setViewData() {

        if (mToolRentDetailEntity != null) {

            ImageLoaderUtil.displayImage(mToolRentDetailEntity.getPic(), ivToolDetail,R.mipmap.ic_banner_bg);
            tvAvailableCount.setText(mToolRentDetailEntity.getNums() + "件可借");
            tvToolName.setText(mToolRentDetailEntity.getTitle());
            tvFeature.setText(mToolRentDetailEntity.getAdd_title());
            tvContent.setText(mToolRentDetailEntity.getDetailed());
            tvRent.setText(mToolRentDetailEntity.getExplain());
            tvDepositDetail.setText(mToolRentDetailEntity.getDeposit_detail());
            tvDeposit.setText("押金:￥" + mToolRentDetailEntity.getDeposit() + "元");
        }
    }

    private void initData() {

        toolId = getIntent().getStringExtra("id");
        mDataCache = DataCache.get(mContext);

        //获取工具详情缓存
        String key = "toolRentDetailEntity" + toolId;
        JSONObject jsonObject = mDataCache.getAsJSONObject(key);
        if (jsonObject != null) {

            ToolRentDetail toolRentDetail = BaseInfo.gson.fromJson(jsonObject.toString(), ToolRentDetail.class);
            mToolRentDetailEntity = toolRentDetail.getData();
            setViewData();
        } else {
            toolDetailRequest();
        }

        //获取站点信息缓存
        String stationKey = "dataEntityStationDetail";
        JSONObject stationJsonObject = mDataCache.getAsJSONObject(stationKey);
        if (stationJsonObject != null) {

            StationDetail stationDetail = BaseInfo.gson.fromJson(stationJsonObject.toString(), StationDetail.class);
            mDataEntityStationDetail = stationDetail.getData();
            tvStation.setText(mDataEntityStationDetail.getNice_name());
            tvAddress.setText(mDataEntityStationDetail.getAddress());
            tvPhone.setText(mDataEntityStationDetail.getPhone());
        } else {
            stationDetailRequest();
        }
    }

    /**
     * 请求工具详情
     */
    private void toolDetailRequest() {

        if (TextUtils.isEmpty(toolId)) {
            return;
        }
        mToolRentDetailInfo.setId(Integer.valueOf(toolId));

        apiManager.request(mToolRentDetailInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mToolRentDetailEntity = mToolRentDetailInfo.getToolRentDetailEntity();
                setViewData();

                stationDetailRequest();
            }
        });

    }

    /**
     * 根据站点id获取站点详情
     */
    private void stationDetailRequest() {

        if(mToolRentDetailEntity == null){
            return;
        }
        mStationDetailInfo.setId(Integer.parseInt(mToolRentDetailEntity.getStation_id()));
        apiManager.isShowProgressBar = false;
        apiManager.request(mStationDetailInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                mDataEntityStationDetail = mStationDetailInfo.getDataEntityStationDetail();
                tvStation.setText(mDataEntityStationDetail.getNice_name());
                tvAddress.setText(mDataEntityStationDetail.getAddress());
                tvPhone.setText(mDataEntityStationDetail.getPhone());
            }
        });
    }

    @Click(R.id.btn_rent_tool_detail)
    protected void rentToolClick() {

        Intent intent = new Intent(this, RentToolActivity_.class);
        intent.putExtra("imgUrl", mToolRentDetailEntity.getPic());
        intent.putExtra("title", mToolRentDetailEntity.getTitle());
        intent.putExtra("addTitle", mToolRentDetailEntity.getAdd_title());
        intent.putExtra("deposit", mToolRentDetailEntity.getDeposit());
        intent.putExtra("availableCount", mToolRentDetailEntity.getNums());
        intent.putExtra("id", mToolRentDetailEntity.getId());
        startActivity(intent);
    }

    @Click(R.id.rl_call_tool_detail)
    protected void callClick() {

        final CustomDialog customDialog = Util.getDialog(mContext, "提示", "确定要拨打该号码？", "确定", "取消");
        customDialog.setOnClickListener(R.id.ok, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
                //用intent启动拨打电话
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mDataEntityStationDetail.getPhone()));
                if (ActivityCompat.checkSelfPermission(ToolDetailActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
            }
        });
        customDialog.setOnClickListener(R.id.cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.dismiss();
            }
        });
        customDialog.show();


    }
}
