package com.jiakang.pandlife.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiakang.pandlife.R;


public class BottomBar extends RelativeLayout {

	public TextView textTV;
	public ImageView iconIV;

	public BottomBar(final Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.include_bottom, this);

		iconIV = (ImageView) findViewById(R.id.tb_iv_icon);
		textTV = (TextView) findViewById(R.id.tb_tv_text);
		
	}

}
