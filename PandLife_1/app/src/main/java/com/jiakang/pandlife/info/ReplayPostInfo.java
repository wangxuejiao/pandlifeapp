package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.ReplayItem;
import com.jiakang.pandlife.item.ReplyPost;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by play on 2016/1/19.
 */
public class ReplayPostInfo extends BaseAbsInfo {

    private int replyid;
    private String content;
    private ReplyPost replyPost;

    public int getReplyid() {
        return replyid;
    }

    public void setReplyid(int replyid) {
        this.replyid = replyid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ReplyPost getReplyPost() {
        return replyPost;
    }

    public void setReplyPost(ReplyPost replyPost) {
        this.replyPost = replyPost;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Forum&a=Reply&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("replyid",replyid);
            json.put("content",content);
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        try {
            if (jsonObject.getInt("status") == 1){

                replyPost = BaseInfo.gson.fromJson(jsonObject.toString(),ReplyPost.class);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
