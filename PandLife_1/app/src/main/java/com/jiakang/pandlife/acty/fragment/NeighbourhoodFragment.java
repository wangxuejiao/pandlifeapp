package com.jiakang.pandlife.acty.fragment;

import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.bigkoo.convenientbanner.listener.OnItemClickListener;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.Post.PostActivity_;
import com.jiakang.pandlife.acty.Post.PostDetailActivity_;
import com.jiakang.pandlife.acty.WebViewActivity;
import com.jiakang.pandlife.acty.homepage.GroupPurchaseActivity_;
import com.jiakang.pandlife.acty.homepage.GroupPurchaseDetailActivity_;
import com.jiakang.pandlife.acty.neighbour.VillageGoodsActivity_;
import com.jiakang.pandlife.acty.personalcenter.ChangePersonalDataActivity_;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.BannerInfo;
import com.jiakang.pandlife.info.NeibourhoodInfo;
import com.jiakang.pandlife.info.NeighbourBannerInfo;
import com.jiakang.pandlife.info.PostTypeInfo;
import com.jiakang.pandlife.item.BannerItem;
import com.jiakang.pandlife.item.NeibourhoodItem;
import com.jiakang.pandlife.item.PostTypeItem;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.itemview.NetworkImageHolderView;
import com.jiakang.pandlife.widget.PullToRefreshListView;
import com.jiakang.pandlife.widget.TitleBar;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * 邻里Fragment
 */
public class NeighbourhoodFragment extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener, OnItemClickListener {

    private static final String TAG = "NeighbourhoodFragment";

    private static final int POST_DETAIL_REQUEST_CODE = 1;
    private static final int POST_REQUST_CODE = 2;

    private static final int FIRST_REQUEST = 0;
    private static final int REFRESH = 1; //下拉刷新
    private static final int MORE = 2;//加载更多
    private boolean isMore = true; //判断有下一页

    /**
     * 每页个数，BaseInfo.pageSize = 20 为默认值
     */
    private int pageSize = 10;
    /**
     * 当前请求的页
     */
    private int pageIndexGet = 1;

    private TitleBar mTitleBar;
    //  Banner banner;
    private ConvenientBanner convenientBanner;
    private View view;
    private PullToRefreshListView lvNeighbourhood;
    private ItemAdapter mItemAdapter;

    //banner获取类
    private NeighbourBannerInfo mBannerInfo = new NeighbourBannerInfo();
    private List<BannerItem.DataEntityBanner> mDataEntityBannerList;


    //帖子列表类
    private NeibourhoodInfo mNeibourhoodInfo = new NeibourhoodInfo();
    private List<NeibourhoodItem.DataEntityNeibourhood> mDataEntityNeibourhoodList = new ArrayList<>();

    //帖子类型类
    private PostTypeInfo mPostTypeInfo = new PostTypeInfo();
    private List<PostTypeItem.DataEntityPostType> mDataEntityPostTypeList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_neighbourhood, container, false);
        initData();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();

    }

    @Override
    public void onResume() {
        super.onResume();
        //开始自动翻页
        convenientBanner.startTurning(5000);
    }

    @Override
    public void onPause() {
        super.onPause();
        //停止翻页
        convenientBanner.stopTurning();
    }

    private void initView() {

        mTitleBar = (TitleBar) view.findViewById(R.id.al_tb_title);
        mTitleBar.titleTV.setText("邻里");
        mTitleBar.leftBN.setVisibility(View.GONE);
        mTitleBar.leftIBN.setVisibility(View.GONE);
        mTitleBar.rightBN.setText("发布");
        mTitleBar.rightBN.setOnClickListener(this);

        convenientBanner = (ConvenientBanner) view.findViewById(R.id.convenientBanner);

        lvNeighbourhood = (PullToRefreshListView) view.findViewById(R.id.lv_neighbourhood);
        lvNeighbourhood.setShowFootView(true);
        mItemAdapter = new ItemAdapter(getActivity());
        lvNeighbourhood.setAdapter(mItemAdapter);

        lvNeighbourhood.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (position ==  mItemAdapter.getCount() + 1) {
                    return;
                }
                Intent intent = new Intent(getActivity(), PostDetailActivity_.class);
                int postId = Integer.valueOf(((NeibourhoodItem.DataEntityNeibourhood) lvNeighbourhood.getItemAtPosition
                        (position)).getId());
                intent.putExtra("postId", postId);
                intent.putExtra("postType", ((NeibourhoodItem.DataEntityNeibourhood) lvNeighbourhood.getItemAtPosition(position)).getType());
                intent.putExtra("delete", "no");
                startActivityForResult(intent, POST_DETAIL_REQUEST_CODE);
            }
        });

        lvNeighbourhood.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            @Override
            public void onRefresh() {

                neibourhoodRequest(REFRESH);
            }
        });

        lvNeighbourhood.setOnMoreListener(new PullToRefreshListView.OnMoreListener() {
            @Override
            public void onMore() {

                if (isMore) {
                    neibourhoodRequest(MORE);
                } else {
                    Toast.makeText(getActivity(), "没有更多了！", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initData() {

        bannerRequest();


        neibourhoodRequest(FIRST_REQUEST);
    }

    /**
     * banner请求
     */
    private void bannerRequest() {

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mBannerInfo, new AbsOnRequestListener(getActivity()) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mDataEntityBannerList = mBannerInfo.getDataEntityBannerList();

                convenientBanner.setPages(new CBViewHolderCreator<NetworkImageHolderView>() {
                    @Override
                    public NetworkImageHolderView createHolder() {
                        return new NetworkImageHolderView();
                    }
                }, mDataEntityBannerList)
                        //设置两个点图片作为翻页指示器，不设置则没有指示器，可以根据自己需求自行配合自己的指示器
                        .setPageIndicator(new int[]{R.mipmap.dot_blur, R.mipmap.dot_focus})
                                //设置指示器的方向
//                .setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.ALIGN_PARENT_RIGHT)
//                .setOnPageChangeListener(this)//监听翻页事件
                        .setOnItemClickListener(NeighbourhoodFragment.this);
            }
        });
    }

    /**
     * 帖子列表请求
     */
    private void neibourhoodRequest(final int listType) {

        if (listType == REFRESH || listType == FIRST_REQUEST) {
            pageIndexGet = 1;
        } else if (listType == MORE) {
            pageIndexGet++;
        } else {
            pageIndexGet = 1;
        }

        mNeibourhoodInfo.setPageIndex(pageIndexGet);
        mNeibourhoodInfo.setPageSize(pageSize);

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mNeibourhoodInfo, new AbsOnRequestListener(getActivity()) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mDataEntityNeibourhoodList = mNeibourhoodInfo.getDataEntityNeibourhoodList();

                if (mDataEntityNeibourhoodList != null && mDataEntityNeibourhoodList.size() > 0) {

                    onPostTypeRequest();
                }

                //判断是否有下一页
                if (pageIndexGet < mNeibourhoodInfo.getNeibourhoodItem().getPage().getTotalPage()) {
                    isMore = true;
                } else {
                    isMore = false;
                }

                if (listType != MORE) {

                    mItemAdapter.clear();
                }

                lvNeighbourhood.onRefreshComplete();
                lvNeighbourhood.onMoreComplete(isMore);
            }
        });
    }

    /**
     * 帖子类型请求
     */
    private void onPostTypeRequest() {

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mPostTypeInfo, new AbsOnRequestListener(getActivity()) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);


                mDataEntityPostTypeList = mPostTypeInfo.getDataEntityPostTypeList();
                for (int i = 0; i < mDataEntityNeibourhoodList.size(); i++) {

                    String postType = mDataEntityNeibourhoodList.get(i).getType();

                    for (int j = 0; j < mDataEntityPostTypeList.size(); j++) {

                        if (postType.equals(mDataEntityPostTypeList.get(j).getId())) {
                            mDataEntityNeibourhoodList.get(i).setType(mDataEntityPostTypeList.get(j).getName());
                        }
                    }
                }
                mItemAdapter.addItems((List) mDataEntityNeibourhoodList);
                mItemAdapter.notifyDataSetChanged();
            }
        });
    }


    @Override
    public void onItemClick(int position) {

        //控制是否循环
//        convenientBanner.setCanLoop(!convenientBanner.isCanLoop());

        BannerItem.DataEntityBanner dataEntityBanner = mDataEntityBannerList.get(position);
        switch (dataEntityBanner.getKind()) {
            case "tuan":
                if (!TextUtils.isEmpty(dataEntityBanner.getId())) {

                    Intent intent = new Intent(getActivity(), GroupPurchaseDetailActivity_.class);
                    intent.putExtra("id", dataEntityBanner.getId());
                    startActivity(intent);
                } else {

                    Intent intent = new Intent(getActivity(), GroupPurchaseActivity_.class);
                    startActivity(intent);
                }
                break;
            case "xqphh":
                if (!TextUtils.isEmpty(dataEntityBanner.getId())) {

                    Intent intentGoodsDetail = new Intent(getActivity(), VillageGoodsActivity_.class);
                    intentGoodsDetail.putExtra("id", dataEntityBanner.getId());
                    startActivity(intentGoodsDetail);
                } else {

                    Intent intentXqphh = new Intent(getActivity(), VillageGoodsActivity_.class);
                    startActivity(intentXqphh);
                }

                break;
            case "linli":

                if (!TextUtils.isEmpty(dataEntityBanner.getId())) {

                    Intent intentLinli = new Intent(getActivity(), PostDetailActivity_.class);
                    intentLinli.putExtra("postId", Integer.valueOf(dataEntityBanner.getId()));
                    intentLinli.putExtra("delete", "no");
                    startActivity(intentLinli);
                }
                break;

            case "h5":
                WebViewActivity.startWebView(getActivity(), dataEntityBanner.getUrl());
                break;
            default:
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        //Toast.makeText(getActivity(),"监听到翻到第"+position+"了",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

        //Toast.makeText(this,"点击了第"+state+"个",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_OK) {

           /* switch (requestCode){
                case POST_DETAIL_REQUEST_CODE :
                    neibourhoodRequest(FIRST_REQUEST);
                    break;
                case POST_REQUST_CODE :
                    neibourhoodRequest(FIRST_REQUEST);
                    break;
            }*/

            neibourhoodRequest(FIRST_REQUEST);
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.it_btn_right:

                UserItem userItem = PandLifeApp.getInstance().getUserItem();
                String nick = userItem.getNick();
                if (TextUtils.isEmpty(nick)) {

                    Toast.makeText(getActivity(), "您还没有昵称，请先完善信息", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), ChangePersonalDataActivity_.class);
                    startActivity(intent);
                } else {

                    Intent intent = new Intent(getActivity(), PostActivity_.class);
                    startActivityForResult(intent, POST_REQUST_CODE);
                }

                break;
        }
    }
}
