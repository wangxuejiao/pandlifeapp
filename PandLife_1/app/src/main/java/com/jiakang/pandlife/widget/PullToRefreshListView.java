package com.jiakang.pandlife.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiakang.pandlife.R;

import java.util.Date;

import greendroid.util.Util;

/**
 * Created by Administrator on 2016/1/26.
 */
public class PullToRefreshListView extends ListView implements AbsListView.OnScrollListener {
    private static final String TAG = "PullToRefreshListView";
    private static final int PULL_To_REFRESH = 0;
    private static final int RELEASE_To_REFRESH = 1;
    private static final int REFRESHING = 2;
    private static final int DONE = 3;
    private int state;
    private static final int LV_STATE_ALLMORE = 545;
    private static final int LV_STATE_HASMORE = 546;
    private static final int LV_STATE_MORE_LOADING = 547;
    private float spaceValue;
    private LayoutInflater inflater;
    private View footView;
    private View footProgressBar;
    private TextView footMoreTV;
    private LinearLayout headView;
    RelativeLayout headRLayout;
    private TextView tipsTextview;
    private TextView lastUpdatedTextView;
    private ImageView arrowImageView;
    private ProgressBar progressBar;
    private RotateAnimation animation;
    private RotateAnimation reverseAnimation;
    private boolean isRecoredWhenLastVisible = false;
    private boolean isRecoredWhenFirstVisible = false;
    private int headContentWidth;
    private int headContentHeight;
    private int headContentOriginalTopPadding;
    private float startY;
    private int currentScrollState;
    private Context context;
    private boolean isBack;
    private boolean isLastItemVisible = true;
    private boolean isFirstItemVisible = true;
    private int heightScreen;
    private int firstVisiableIndex = 0;
    private ActionBarDrawable themeDrawable;
    public PullToRefreshListView.OnRefreshListener refreshListener;
    public PullToRefreshListView.OnMoreListener moreListener;
    private boolean showFootView = false;
    private boolean showHeaderView = true;
    private int scrollTop = 1;

    public PullToRefreshListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    public PullToRefreshListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.init(context);
    }

    private void init(Context c) {
        this.context = c;
        this.animation = new RotateAnimation(0.0F, -180.0F, 1, 0.5F, 1, 0.5F);
        this.animation.setInterpolator(new LinearInterpolator());
        this.animation.setDuration(100L);
        this.animation.setFillAfter(true);
        this.reverseAnimation = new RotateAnimation(-180.0F, 0.0F, 1, 0.5F, 1, 0.5F);
        this.reverseAnimation.setInterpolator(new LinearInterpolator());
        this.reverseAnimation.setDuration(100L);
        this.reverseAnimation.setFillAfter(true);
        this.inflater = LayoutInflater.from(this.context);
        this.headView = (LinearLayout)this.inflater.inflate(R.layout.pull_to_refresh_head, (ViewGroup)null);
        this.headRLayout = (RelativeLayout)this.headView.findViewById(R.id.head_contentLayout);
        this.arrowImageView = (ImageView)this.headView.findViewById(R.id.head_arrowImageView);
        this.arrowImageView.setMinimumWidth(50);
        this.arrowImageView.setMinimumHeight(50);
        this.themeDrawable = new ActionBarDrawable(this.inflater.getContext(), R.mipmap.ic_pulltorefresh_arrow);
        this.arrowImageView.setImageDrawable(this.themeDrawable);
        this.progressBar = (ProgressBar)this.headView.findViewById(R.id.head_progressBar);
        this.tipsTextview = (TextView)this.headView.findViewById(R.id.head_tipsTextView);
        this.lastUpdatedTextView = (TextView)this.headView.findViewById(R.id.head_lastUpdatedTextView);
        this.headContentOriginalTopPadding = this.headView.getPaddingTop();
        this.measureView(this.headView);
        this.headContentHeight = this.headView.getMeasuredHeight();
        this.headContentWidth = this.headView.getMeasuredWidth();
        this.headView.setPadding(this.headView.getPaddingLeft(), -1 * this.headContentHeight, this.headView.getPaddingRight(), this.headView.getPaddingBottom());
        this.spaceValue = Util.dip2px(this.context, 30.0F);
        System.out.println("初始高度：" + this.headContentHeight);
        System.out.println("初始TopPad：" + this.headContentOriginalTopPadding);
        System.out.println("初始spaceValue：" + this.spaceValue);
        this.addHeaderView(this.headView);
        this.setOnScrollListener(this);
        this.footView = ((Activity)this.context).getLayoutInflater().inflate(R.layout.ww_listview_footer, (ViewGroup)null);
        this.footMoreTV = (TextView)this.footView.findViewById(R.id.listview_foot_more);
        this.footProgressBar = (ProgressBar)this.footView.findViewById(R.id.listview_foot_progress);
        this.addFooterView(this.footView);
        this.setShowFootView(false);
        this.setShowHeaderView(true);
    }

    public void setShowFootView(boolean show) {
        this.showFootView = show;
        this.footMoreTV.setVisibility(this.showFootView ? 0 : 8);
        this.footProgressBar.setVisibility(this.showFootView ? 0 : 8);
    }

    public void setShowHeaderView(boolean show) {
        this.showHeaderView = show;
        this.headRLayout.setVisibility(show ? 0 : 8);
        this.headContentHeight = show?this.headView.getMeasuredHeight():0;
    }

    public void onScroll(AbsListView view, int firstVisiableItem, int visibleItemCount, int totalItemCount) {
        this.firstVisiableIndex = firstVisiableItem;
        this.isFirstItemVisible = firstVisiableItem == 0;
        int visibleLastIndex = firstVisiableItem + visibleItemCount - 1;
        ListAdapter adapter = this.getAdapter();
        if(adapter != null) {
            if(adapter.getCount() - 1 == visibleLastIndex) {
                this.isLastItemVisible = true;
            } else {
                this.isLastItemVisible = false;
            }

        }
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        if(view.getChildCount() > 0) {
            View scrollEnd = view.getChildAt(0);
            this.scrollTop = scrollEnd == null?0:scrollEnd.getTop();
        }

        if(this.getAdapter().getCount() != 0 && this.footView != null) {
            boolean scrollEnd1 = false;

            try {
                if(view.getPositionForView(this.footView) == view.getLastVisiblePosition()) {
                    ;
                }

                scrollEnd1 = true;
            } catch (Exception var5) {
                ;
            }

            boolean hasMore = this.toInt(this.getTag()) == 546;
            if(scrollEnd1 && hasMore) {
                this.setTag(Integer.valueOf(547));
                this.footMoreTV.setText("加载中...");
                this.footProgressBar.setVisibility(this.showFootView ? 0 : 8);
                if(this.moreListener != null && this.showFootView) {
                    this.moreListener.onMore();
                }
            }

        }
    }

    private int toInt(Object tag) {
        try {
            return Integer.parseInt(tag.toString());
        } catch (Exception var3) {
            return 546;
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()) {
            case 0:
                if(!this.isRecoredWhenLastVisible && this.isLastItemVisible) {
                    this.isRecoredWhenLastVisible = true;
                    this.startY = event.getY();
                }

                if(!this.isRecoredWhenFirstVisible && this.isFirstItemVisible) {
                    this.isRecoredWhenFirstVisible = true;
                    this.startY = event.getY();
                }
                break;
            case 1:
            case 3:
                if(this.state != 2 && this.state != 3) {
                    if(this.state == 0) {
                        this.state = 3;
                        this.setSelection(0);
                        this.changeHeaderViewByState();
                    } else if(this.state == 1) {
                        if(this.refreshListener != null && this.showHeaderView) {
                            this.state = 2;
                            this.changeHeaderViewByState();
                            this.refreshListener.onRefresh();
                        } else {
                            this.state = 3;
                            this.changeHeaderViewByState();
                        }
                    }
                }

                if(this.isLastItemVisible && this.isRecoredWhenLastVisible) {
                    if(this.footView != null) {
                        this.footView.setPadding(this.footView.getPaddingLeft(), this.footView.getPaddingTop(), this.footView.getPaddingRight(), 0);
                    } else {
                        this.setPadding(0, 0, 0, 0);
                    }
                }

                this.isRecoredWhenLastVisible = false;
                this.isRecoredWhenFirstVisible = false;
                this.isBack = false;
                break;
            case 2:
                float tempY = event.getY();
                if(!this.isRecoredWhenLastVisible && this.isLastItemVisible) {
                    this.isRecoredWhenLastVisible = true;
                    this.startY = tempY;
                }

                if(!this.isRecoredWhenFirstVisible && this.isFirstItemVisible) {
                    this.isRecoredWhenFirstVisible = true;
                    this.startY = tempY;
                    System.out.println("PullToRefreshListView中：startY为：" + this.startY);
                }

                int topPadding;
                if(this.isLastItemVisible && this.isRecoredWhenLastVisible && this.footView != null) {
                    topPadding = (int)((double)(this.startY - tempY) / 1.5D);
                    this.footView.setPadding(this.footView.getPaddingLeft(), this.footView.getPaddingTop(), this.footView.getPaddingRight(), topPadding);
                }

                if(this.isFirstItemVisible && this.isRecoredWhenFirstVisible && this.state != 2) {
                    if(this.state == 1) {
                        this.setSelection(0);
                        if(tempY - this.startY < (float)this.headContentHeight + this.spaceValue && tempY - this.startY > 0.0F) {
                            this.state = 0;
                            this.changeHeaderViewByState();
                        } else if(tempY - this.startY <= 0.0F) {
                            this.state = 3;
                            this.changeHeaderViewByState();
                        }
                    } else if(this.state == 0) {
                        this.setSelection(0);
                        if(tempY - this.startY >= (float)this.headContentHeight + this.spaceValue && this.currentScrollState == 1) {
                            this.isBack = true;
                            this.state = 1;
                            this.changeHeaderViewByState();
                        } else if(tempY - this.startY <= 0.0F) {
                            this.state = 3;
                            this.changeHeaderViewByState();
                        }
                    } else if(this.state == 3 && tempY - this.startY > 0.0F) {
                        this.state = 0;
                        this.changeHeaderViewByState();
                    }

                    if(this.state == 0 || this.state == 1) {
                        topPadding = (int)((double)(-this.headContentHeight) + (double)(tempY - this.startY) / 1.5D);
                        this.headView.setPadding(this.headView.getPaddingLeft(), topPadding, this.headView.getPaddingRight(), this.headView.getPaddingBottom());
                    }
                }
        }

        return super.onTouchEvent(event);
    }

    private void changeHeaderViewByState() {
        switch(this.state) {
            case 0:
                this.progressBar.setVisibility(8);
                this.tipsTextview.setVisibility(0);
                this.lastUpdatedTextView.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setVisibility(0);
                if(this.isBack) {
                    this.isBack = false;
                    this.arrowImageView.clearAnimation();
                    this.arrowImageView.startAnimation(this.reverseAnimation);
                }

                this.tipsTextview.setText(R.string.pull_to_refresh_pull_label);
                break;
            case 1:
                this.arrowImageView.setVisibility(0);
                this.progressBar.setVisibility(8);
                this.tipsTextview.setVisibility(0);
                this.lastUpdatedTextView.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.startAnimation(this.animation);
                this.tipsTextview.setText(R.string.pull_to_refresh_release_label);
                break;
            case 2:
                this.headView.setPadding(this.headView.getPaddingLeft(), this.headContentOriginalTopPadding, this.headView.getPaddingRight(), this.headView.getPaddingBottom());
                this.progressBar.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setVisibility(8);
                this.tipsTextview.setText(R.string.pull_to_refresh_refreshing_label);
                break;
            case 3:
                this.headView.setPadding(this.headView.getPaddingLeft(), -this.headContentHeight, this.headView.getPaddingRight(), this.headView.getPaddingBottom());
                this.progressBar.setVisibility(8);
                this.arrowImageView.clearAnimation();
                this.tipsTextview.setText(R.string.pull_to_refresh_pull_label);
                this.lastUpdatedTextView.setVisibility(0);
        }

    }

    public void setOnRefreshListener(PullToRefreshListView.OnRefreshListener refreshListener) {
        this.refreshListener = refreshListener;
    }

    public void setOnMoreListener(PullToRefreshListView.OnMoreListener moreListener) {
        this.moreListener = moreListener;
    }

    public void onRefreshComplete(String update) {
        this.lastUpdatedTextView.setText(update + (new Date()).toLocaleString());
        this.state = 3;
        this.changeHeaderViewByState();
    }

    public void onRefreshComplete() {
        this.onRefreshComplete("最近更新:");
    }

    public void onMoreComplete(boolean hasMore) {
//        this.onMoreComplete(hasMore, "");
        this.onMoreComplete(hasMore, "已加载全部内容");
    }

    public void onMoreComplete(boolean hasMore, String noMoreTip) {
        if(this.showFootView) {
            this.setTag(Integer.valueOf(hasMore?546:545));
            this.footMoreTV.setText(hasMore?"加载中...":noMoreTip);
            this.footProgressBar.setVisibility(hasMore ? 0 : 8);
        } else {
            this.footMoreTV.setVisibility(8);
            this.footProgressBar.setVisibility(8);
        }

    }

    public void addFooterView(View v, Object data, boolean isSelectable) {
        super.addFooterView(v, data, isSelectable);
    }

    private void measureView(View view) {
        LayoutParams p = (LayoutParams) view.getLayoutParams();
        if(p == null) {
            p = new LayoutParams(-1, -2);
        }

        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        int lpHeight = p.height;
        int childHeightSpec;
        if(lpHeight > 0) {
            childHeightSpec = MeasureSpec.makeMeasureSpec(lpHeight, 1073741824);
        } else {
            childHeightSpec = MeasureSpec.makeMeasureSpec(0, 0);
        }

        view.measure(childWidthSpec, childHeightSpec);
    }

    public int getFirstVisiableIndex() {
        return this.firstVisiableIndex;
    }

    public int getScrollTop() {
        return this.scrollTop;
    }

    public interface OnMoreListener {
        void onMore();
    }

    public interface OnRefreshListener {
        void onRefresh();
    }
}
