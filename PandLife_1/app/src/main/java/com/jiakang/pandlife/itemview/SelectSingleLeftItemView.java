package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.CityItem;
import com.jiakang.pandlife.item.CommunityItem;
import com.jiakang.pandlife.item.DeliveryItem;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.GroupDetailStationItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.ProvinceItem;

/**
 * Created by play on 2016/1/6.
 */
public class SelectSingleLeftItemView extends AbsRelativeLayout {

    private TextView leftTV;

    public SelectSingleLeftItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectSingleLeftItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        leftTV = (TextView) findViewById(R.id.issl_tv_left);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        if (item instanceof ProvinceItem){
            ProvinceItem provinceItem = (ProvinceItem) item;
            leftTV.setText(provinceItem.getName());
        }else if (item instanceof CityItem){
            CityItem cityItem = (CityItem)item;
            leftTV.setText(cityItem.getName());
        }else if (item instanceof AreaItem){
            AreaItem areaItem = (AreaItem)item;
            leftTV.setText(areaItem.getName());
        }else if (item instanceof CommunityItem){
            CommunityItem communityItem = (CommunityItem)item;
            leftTV.setText(communityItem.getName());
        }else if (item instanceof DeliveryItem){
            DeliveryItem deliveryItem = (DeliveryItem)item;
            leftTV.setText(deliveryItem.getName());
        }else if(item instanceof GroupDetailStationItem){
            GroupDetailStationItem groupDetailStationItem = (GroupDetailStationItem)item;
            leftTV.setText(groupDetailStationItem.getStation_name() + "  库存" + groupDetailStationItem.getNumber() + "件");
        }

    }
}
