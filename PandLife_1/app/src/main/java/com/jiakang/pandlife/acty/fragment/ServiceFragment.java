package com.jiakang.pandlife.acty.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.service.ConveniencePhoneActivity_;
import com.jiakang.pandlife.acty.service.NearbyStationActivity_;
import com.jiakang.pandlife.acty.service.StationNotificationActivity_;
import com.jiakang.pandlife.acty.service.ToolRentActivity_;
import com.jiakang.pandlife.widget.TitleBar;


/**
 * 服务Fragment
 */
public class ServiceFragment extends Fragment implements View.OnClickListener {
    protected static final String TAG = "ServiceFragment";

    private TitleBar mTitleBar;
    private RelativeLayout noticeRL;
    private RelativeLayout rentToolsRL;
    private RelativeLayout conveniencePhoneRL;
    private RelativeLayout nearbySiteRL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initVar();
        initView();
        bindView();
    }

    private void initVar() {

    }

    private void initView() {
        mTitleBar = (TitleBar) getView().findViewById(R.id.fs_tb_title);
        noticeRL = (RelativeLayout) getView().findViewById(R.id.fs_rl_notice);
        rentToolsRL = (RelativeLayout) getView().findViewById(R.id.fs_rl_rent_tools);
        conveniencePhoneRL = (RelativeLayout) getView().findViewById(R.id.fs_rl_convenience_phone);
        nearbySiteRL = (RelativeLayout) getView().findViewById(R.id.fs_rl_nearby_site);


        mTitleBar.titleTV.setText("服务");
        mTitleBar.leftBN.setVisibility(View.GONE);
        mTitleBar.leftIBN.setVisibility(View.GONE);
        mTitleBar.rightBN.setVisibility(View.GONE);
    }

    private void bindView() {
        noticeRL.setOnClickListener(this);
        rentToolsRL.setOnClickListener(this);
        conveniencePhoneRL.setOnClickListener(this);
        nearbySiteRL.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fs_rl_notice:
                Intent intentNotice = new Intent(getActivity(), StationNotificationActivity_.class);
                startActivity(intentNotice);
                break;
            case R.id.fs_rl_rent_tools:
                Intent intentTools = new Intent(getActivity(), ToolRentActivity_.class);
                startActivity(intentTools);
                break;
            case R.id.fs_rl_convenience_phone:
                Intent intentPhone = new Intent(getActivity(), ConveniencePhoneActivity_.class);
                startActivity(intentPhone);
                break;
            case R.id.fs_rl_nearby_site:
                Intent intentSite = new Intent(getActivity(), NearbyStationActivity_.class);
                startActivity(intentSite);
                break;

            default:
                break;
        }
    }
}
