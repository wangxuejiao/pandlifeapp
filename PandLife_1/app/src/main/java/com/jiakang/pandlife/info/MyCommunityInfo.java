package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.CommunityItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 获取我的绑定小区信息
 *
 * @author ww
 *
 */
public class MyCommunityInfo extends BaseAbsInfo {

    private static final String TAG = "MyCommunityInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private CommunityItem mCommunityItem;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=User&a=MyCommunity" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                JSONArray taskJA = jsonObject.getJSONArray("data");
                mCommunityItem = BaseInfo.gson.fromJson(taskJA.getJSONObject(0).toString(), CommunityItem.class);
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public CommunityItem getCommunityItem() {
        return mCommunityItem;
    }

    public void setCommunityItem(CommunityItem communityItem) {
        mCommunityItem = communityItem;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
