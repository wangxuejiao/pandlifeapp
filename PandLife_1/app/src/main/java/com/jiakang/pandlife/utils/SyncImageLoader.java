package com.jiakang.pandlife.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.jiakang.pandlife.Constant;

import java.io.InputStream;
import java.io.Serializable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class SyncImageLoader {
	/**
	 * 
	 */
	private static final long serialVersionUID = 349378295065915626L;
	private static final String TAG = "SyncImageLoader";
	private MemoryCache memoryCache;
	private FileCache fileCache;
	private Context mContext;
	private static SyncImageLoader instance;
	// private BitmapFactory.Options opts;
	private boolean isRound = false;
	private Bitmap cache = null;
	// 工人数量
	private static final int THREAD_WORKER_COUNT = 3;
	/**
	 * Java通过Executors提供四种线程池，分别为：
	 * newCachedThreadPool创建一个可缓存线程池，如果线程池长度超过处理需要，可灵活回收空闲线程，若无可回收，则新建线程。
	 * newFixedThreadPool 创建一个定长线程池，可控制线程最大并发数，超出的线程会在队列中等待。
	 * newScheduledThreadPool 创建一个定长线程池，支持定时及周期性任务执行。 newSingleThreadExecutor
	 * 创建一个单线程化的线程池，它只会用唯一的工作线程来执行任务，保证所有任务按照指定顺序(FIFO, LIFO, 优先级)执行。
	 */
	private static final ExecutorService THREAD_WORKER = Executors.newFixedThreadPool(THREAD_WORKER_COUNT);

	/** 处理加载（下载、内存缓存、文件缓存）图片成功与失败 */
	// private Handler mHandler = new Handler() {
	// @Override
	// public void handleMessage(Message msg) {
	// OnloadImage onloadImage = (OnloadImage) msg.getData().get("onloadImage");
	// switch (msg.what) {
	// case 1:
	// onloadImage.loadFinish((Bitmap) msg.obj);
	// break;
	// case 2:
	// onloadImage.loadFail(Util.hasNet(mContext, false));
	// break;
	// default:
	// break;
	// }
	// super.handleMessage(msg);
	// }
	//
	// };

	/**
	 * 加载图片回调
	 * 
	 * @author ww
	 */
	public interface OnloadImage extends Serializable {
		/** 加载完成 */
		void loadFinish(Bitmap bitmap);

		/** 加载失败 */
		void loadFail(boolean hasNet);
	}

	public static SyncImageLoader getInstance(Context context) {
		if (instance == null) {
			instance = new SyncImageLoader(null, context);
		}
		return instance;
	}

	/**
	 * 私有构造方法
	 * 
	 * @param handler
	 * @param context
	 */
	private SyncImageLoader(Handler handler, Context context) {
		this.mContext = context;
		memoryCache = MemoryCache.getInstance();
		fileCache = FileCache.getInstance(mContext);
		// opts = new BitmapFactory.Options();
		// opts.inJustDecodeBounds = true;

	}

	/**
	 * 关闭线程池
	 */
	public static void shutdown() {
		THREAD_WORKER.shutdown();
	}

	/**
	 * 默认缩小倍数加载图片（bitmap不带圆角）
	 * 
	 * @param imageUrl
	 * @param onloadImage
	 */
	public void loadZoomImage(String imageUrl, OnloadImage onloadImage, int width, int height) {
		loadImage(imageUrl, onloadImage, true, width, height);
	}

	/**
	 * 加载缩放图片（bitmap不带圆角）
	 * 
	 * @param imageUrl
	 * @param onloadImage
	 * @param view
	 *            根据ImageView进行缩放
	 * @return void
	 */
	public void loadZoomImage(String imageUrl, OnloadImage onloadImage, View view) {
		int width = getComputeWidth(view);
		int height = getComputeHeight(view);
		loadImage(imageUrl, onloadImage, true, width, height);
	}

	/**
	 * 加载原图片（bitmap不带圆角）
	 * 
	 * @param imageUrl
	 * @param onloadImage
	 */
	public void loadOriginalImage(final String imageUrl, final OnloadImage onloadImage) {
		loadImage(imageUrl, onloadImage, false, -1, -1);
	}

	/**
	 * 加载原图片（bitmap不带圆角），传入isRoom是否缩小倍数
	 * 
	 * @param imageUrl
	 * @param onloadImage
	 * @param isZoom
	 *            是否缩小倍数
	 */
	public void loadImage(final String imageUrl, final OnloadImage onloadImage, final boolean isZoom, final int width, final int height) {
		// 非法url
		if (TextUtils.isEmpty(imageUrl) || Constant.downalImageUrl.equals(imageUrl) || (Constant.downalImageUrl + "null").equals(imageUrl)) {
			// Message msg = Message.obtain(handler, 2);
			// Bundle bundle = new Bundle();
			// bundle.putSerializable("onloadImage", onloadImage);
			// msg.setData(bundle);
			// handler.sendMessage(msg);
			onloadImage.loadFail(Util.hasNet(mContext, false));
			return;
		}

		// 内存缓存
		if (isZoom) {
			// 如果缩减倍数，内存中能取到缩减倍数后的bitmap
			Bitmap bit = memoryCache.getBitmapFromCache(imageUrl);
			if (null != bit) {
				// Message msg = Message.obtain(handler, 1, bit);
				// Bundle bundle = new Bundle();
				// bundle.putSerializable("onloadImage", onloadImage);
				// msg.setData(bundle);
				// handler.sendMessage(msg);
				onloadImage.loadFinish(bit);
				return;
			}
		}

		// 文件缓存
		String file = fileCache.getCacheFilePath(imageUrl);
		if (null != file) {
			// 不缩减图片倍数
			Bitmap bit2;
			if (isZoom) {
				bit2 = decodeUriAsBitmap(file, width, height);
			} else {
				bit2 = decodeUriAsBitmap(file, -1, -1);
			}

			if (null != bit2) {
				// 缩减倍数加入内存，如果不缩减倍数，原始图片很大，不加入内存
				if (isZoom) {
					memoryCache.addBitmapToCache(imageUrl, bit2);
				}
				// Message msg = Message.obtain(handler, 1, bit2);
				// Bundle bundle = new Bundle();
				// bundle.putSerializable("onloadImage", onloadImage);
				// msg.setData(bundle);
				// handler.sendMessage(msg);
				onloadImage.loadFinish(bit2);
				return;
			}
		}

		// 没有网络
		if (!Util.hasNet(mContext, false)) {
			// Message msg = Message.obtain(handler, 2);
			// Bundle bundle = new Bundle();
			// bundle.putSerializable("onloadImage", onloadImage);
			// msg.setData(bundle);
			// handler.sendMessage(msg);
			onloadImage.loadFail(Util.hasNet(mContext, false));
			return;
		}

		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 1:
					onloadImage.loadFinish((Bitmap) msg.obj);
					break;
				case 2:
					onloadImage.loadFail(Util.hasNet(mContext, false));
					break;
				default:
					break;
				}
				super.handleMessage(msg);
			}
		};

		// 下载
		THREAD_WORKER.execute(new Runnable() {
			@Override
			public void run() {
				Bitmap bitmap;
				if (isZoom) {
					// 本身带有缓存成文件功能
					bitmap = downloadBitmap(imageUrl, width, height);
				} else {
					// 本身带有缓存成文件功能
					bitmap = downloadBitmap(imageUrl, -1, -1);
				}
				Message msg;
				if (bitmap != null) {
					// 缩减倍数加入内存，如果不缩减倍数，原始图片很大，不加入内存
					if (isZoom) {
						memoryCache.addBitmapToCache(imageUrl, bitmap);
					}
					msg = Message.obtain(handler, 1, bitmap);
					handler.sendMessage(msg);
				} else {
					msg = Message.obtain(handler, 2, null);
					handler.sendMessage(msg);
				}
			}
		});
	}

	/**
	 * 默认设置图片到Src上，bitmap不带圆角
	 * 
	 * @param imageUrl
	 * @param onloadImage
	 */
	public void loadImageOnSrc(final String imageUrl, final ImageView iView, final int defImgeRes) {
		loadImageOnSrcOrBack(imageUrl, iView, defImgeRes, true);
	}

	/**
	 * 是否设置图片到Src上，bitmap不带圆角
	 * 
	 * @param imageUrl
	 * @param onloadImage
	 *            是否设置图片到Src上
	 */
	public void loadImageOnSrcOrBack(final String imageUrl, final ImageView iView, final int defImgeRes, final boolean isSrc) {
		// 非法url
		if (TextUtils.isEmpty(imageUrl) || Constant.downalImageUrl.equals(imageUrl) || (Constant.downalImageUrl + "null").equals(imageUrl)) {
			if (isSrc)
				iView.setImageResource(defImgeRes);
			else
				iView.setBackgroundResource(defImgeRes);
			return;
		}

		// 内存缓存
		Bitmap bit = memoryCache.getBitmapFromCache(imageUrl);
		if (null != bit) {
			if (isSrc)
				iView.setImageBitmap(bit);
			else
				iView.setBackgroundDrawable(new BitmapDrawable(bit));
			return;
		}

		// 文件缓存
		String file = fileCache.getCacheFilePath(imageUrl);
		if (null != file) {
			// 测量
			int width = getComputeWidth(iView);
			int height = getComputeHeight(iView);
			JKLog.i("SyncImageLoader", "SyncImageLoader类文件缓存-------->：width为：" + width + "--" + height);
			Bitmap bit2 = decodeUriAsBitmap(file, width, height);
			if (null != bit2) {
				// 非圆角bitmap加入内存，下载完，自动缓存成文件，放到内存中容易oom
				memoryCache.addBitmapToCache(imageUrl, bit2);
				if (isSrc)
					iView.setImageBitmap(bit2);
				else
					iView.setBackgroundDrawable(new BitmapDrawable(bit2));
				return;
			}
		}

		// 没有网络
		if (!Util.hasNet(mContext, false)) {
			if (isSrc)
				iView.setImageResource(defImgeRes);
			else
				iView.setBackgroundResource(defImgeRes);
			return;
		}

		final Handler mHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				if (msg.obj != null) {
					Bitmap bitmap = (Bitmap) msg.obj;
					// 非圆角bitmap加入内存，下载完，自动缓存成文件，放到内存中容易oom
					memoryCache.addBitmapToCache(imageUrl, bitmap);
					if (isSrc)
						iView.setImageBitmap(bitmap);
					else
						iView.setBackgroundDrawable(new BitmapDrawable(bitmap));
				} else {
					if (isSrc)
						iView.setImageResource(defImgeRes);
					else
						iView.setBackgroundResource(defImgeRes);
				}
			}
		};

		// 下载
		THREAD_WORKER.execute(new Runnable() {
			@Override
			public void run() {
				// 测量
				int width = getComputeWidth(iView);
				int height = getComputeHeight(iView);
				// 本身带有缓存成文件功能
				Bitmap bitmap = downloadBitmap(imageUrl, width, height);
				Message msg = Message.obtain(mHandler, 0x12, bitmap);
				mHandler.sendMessage(msg);
			}
		});
	}

	/**
	 * 默认是圆角，设置到src上，缩小图片倍数
	 * 
	 * @param imageUrl
	 * @param iView
	 * @param defImgeRes
	 */
	public void loadImageRound(final String imageUrl, final ImageView iView, final int defImgeRes) {
		loadImageRoundOrnot(imageUrl, iView, defImgeRes, true);
	}

	/** 圆角半径 */
	private int roundCorner = 5;

	/**
	 * 是否圆角，设置到src上，缩小图片倍数
	 * 
	 * @param imageUrl
	 * @param iView
	 * @param defImgeRes
	 * @param isRoundCorner
	 */
	public void loadImageRoundOrnot(final String imageUrl, final ImageView iView, final int defImgeRes, final boolean isRoundCorner) {
		// 非法url
		if (TextUtils.isEmpty(imageUrl) || Constant.downalImageUrl.equals(imageUrl) || (Constant.downalImageUrl + "null").equals(imageUrl)) {
			iView.setImageResource(defImgeRes);
			return;
		}

		// 内存缓存
		Bitmap bit = memoryCache.getBitmapFromCache(imageUrl);
		if (null != bit) {
			if (isRoundCorner)
				bit = ImageUtil.toRoundCorner(bit, roundCorner);
			iView.setImageBitmap(bit);
			return;
		}

		// 文件缓存
		String file = fileCache.getCacheFilePath(imageUrl);
		if (null != file) {
			// 测量
			int width = getComputeWidth(iView);
			int height = getComputeHeight(iView);
			JKLog.i("SyncImageLoader", "SyncImageLoader类文件缓存-------->：width为：" + width + "--" + height);
			Bitmap bit2 = decodeUriAsBitmap(file, width, height);
			// 下载完，非圆角bitmap加入内存内存
			memoryCache.addBitmapToCache(imageUrl, bit2);

			if (null != bit2) {
				if (isRoundCorner)
					bit2 = ImageUtil.toRoundCorner(bit2, roundCorner);
				iView.setImageBitmap(bit2);
				return;
			}
		}

		// 没有网络
		if (!Util.hasNet(mContext, false)) {
			iView.setImageResource(defImgeRes);
			return;
		}

		final Handler mHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				if (msg.obj != null) {
					Bitmap bitmap = (Bitmap) msg.obj;
					// 非圆角bitmap加入内存
					memoryCache.addBitmapToCache(imageUrl, bitmap);
					if (isRoundCorner)
						bitmap = ImageUtil.toRoundCorner(bitmap, roundCorner);
					iView.setImageBitmap(bitmap);
				} else {
					if (isRoundCorner)
						iView.setImageResource(defImgeRes);
					else
						iView.setBackgroundResource(defImgeRes);
				}
			}
		};

		// 下载
		THREAD_WORKER.execute(new Runnable() {
			@Override
			public void run() {
				int width = getComputeWidth(iView);
				int height = getComputeHeight(iView);
				// 本身带有缓存成文件功能
				Bitmap bitmap = downloadBitmap(imageUrl, width, height);
				Message msg = Message.obtain(mHandler, 0x12, bitmap);
				mHandler.sendMessage(msg);
			}
		});
	}

	/**
	 * 
	 * @param view
	 * @return void
	 */
	public static int getComputeWidth(View view) {
		try {
			int width = view.getWidth();
			if (width == 0) {
				view.measure(0, 0);
				width = view.getMeasuredWidth();
			}
			return width;
		} catch (Exception e) {
		}
		return 0;
	}

	/**
	 * 
	 * @param view
	 * @return int
	 */
	public static int getComputeHeight(View view) {
		try {
			int height = view.getHeight();
			if (height == 0) {
				view.measure(0, 0);
				height = view.getMeasuredHeight();
			}
			return height;
		} catch (Exception e) {
		}
		return 0;
	}

	/**
	 * 
	 * 下载网络图片；缓存到文件目录中，即data/data/com.emww.yilinke/cache/listHeadDir中
	 * 在SharedPreferences中保存图片url(key)和对应的文件路径(path)，
	 * 拿到path根据width和height值决定是否缩减倍数生成bitmap返回
	 * 
	 * @param imageUrl
	 * @param width
	 *            为-1时不缩减比例
	 * @param height
	 *            为-1时不缩减比例
	 * @return
	 */
	public Bitmap downloadBitmap(String imageUrl, int width, int height) {
		Bitmap bitmap = null;
		String realUrl = null;
		InputStream inputStream;
		try {
			// 添加文件缓存 返回文件路径
			realUrl = fileCache.addBitmapCache(imageUrl);
			if (width == -1 && height == -1) {
				bitmap = memoryCache.getBitmapFromCache(imageUrl);
			}
			if (bitmap == null) {
				if (!TextUtils.isEmpty(realUrl)) {
					bitmap = decodeUriAsBitmap(realUrl, width, height);
				}
			}
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param filePath
	 * @param width
	 *            为-1时不缩减比例
	 * @param height
	 *            为-1时不缩减比例
	 * @return
	 */
	public static Bitmap decodeUriAsBitmap(String filePath, int width, int height) {
		JKLog.i(TAG, "decodeUriAsBitmap方法中-------->：width:height为-->" + width + ":" + height);
		if (TextUtils.isEmpty(filePath))
			return null;

		if (width == 0)
			width = 80;
		if (height == 0)
			height = 80;

		try {
			// 主动回收内存
			// System.gc();
			if (width == -1 && height == -1) {
				try {
					Bitmap bit = BitmapFactory.decodeFile(filePath);
					return bit;
				} catch (OutOfMemoryError e) {
					return decodeUriAsBitmap(filePath, 0, 0);
				}
			} else {
				BitmapFactory.Options options = new BitmapFactory.Options();
				// 仅仅解码边框，并没有在内存中生成bitmap，但能读取options.outWidth和options.outHeight
				options.inJustDecodeBounds = true;
				JKLog.i("file--path--->", filePath);
				// 并不分配内存
				BitmapFactory.decodeFile(filePath, options);
				// int scalWidth = options.outWidth / width;
				// int scalHeight = options.outHeight / height;
				// if (scalWidth <= 0) {
				// scalWidth = 1;
				// }
				// if (scalHeight <= 0) {
				// scalHeight = 1;
				// }
				// // 缩略倍数：为2，则返回1/2大小
				// options.inSampleSize = scalWidth > scalHeight ? scalWidth :
				// scalHeight;
				options.inSampleSize = calculateInSampleSize(options, width, height);
				options.inJustDecodeBounds = false;
				JKLog.i("file--path--->", filePath);
				return BitmapFactory.decodeFile(filePath, options);
			}
		} catch (Exception e) {
			JKLog.i("uri-:", filePath);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * 按尺寸裁剪Bitmap的方法（来自google官方），这个方法会返回 BitmapFactory.Options 的inSampleSize参数值
	 * Calculate an inSampleSize for use in a {@link BitmapFactory.Options}
	 * object when decoding bitmaps using the decode* methods from
	 * {@link BitmapFactory}. This implementation calculates the closest
	 * inSampleSize that will result in the final decoded bitmap having a width
	 * and height equal to or larger than the requested width and height. This
	 * implementation does not ensure a power of 2 is returned for inSampleSize
	 * which can be faster when decoding but results in a larger bitmap which
	 * isn't as useful for caching purposes.
	 * 
	 * @param options
	 *            An options object with out* params already populated (run
	 *            through a decode* method with inJustDecodeBounds==true
	 * @param reqWidth
	 *            The requested width of the resulting bitmap
	 * @param reqHeight
	 *            The requested height of the resulting bitmap
	 * @return The value to be used for inSampleSize
	 */
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}

			// This offers some additional logic in case the image has a strange
			// aspect ratio. For example, a panorama may have a much larger
			// width than height. In these cases the total pixels might still
			// end up being too large to fit comfortably in memory, so we should
			// be more aggressive with sample down the image (=larger
			// inSampleSize).

			final float totalPixels = width * height;

			// Anything more than 2x the requested pixels we'll sample down
			// further.
			// 任何超过2倍的请求像素，我们将品尝简化
			// 更多。
			final float totalReqPixelsCap = reqWidth * reqHeight * 2;

			while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
				inSampleSize++;
			}
		}
		return inSampleSize;
	}
}