package com.jiakang.pandlife.utils;

import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.widget.Toast;

import greendroid.util.Util;

/**
 * ȫ�ֵ�Toast
 * 
 * @author ������
 */
public class CustomToast {
	private static Toast mToast;
	private static Handler mHandler = new Handler();
	private static Runnable run = new Runnable() {
		public void run() {
			mToast.cancel();
		}
	};

	public static void showToast(Context mContext, String text) {
		showToast(mContext, text, 1000);
	}

	public static void showToast(Context mContext, String text, int duration) {
		if (mContext == null && text != null) {
			return;
		}
		mHandler.removeCallbacks(run);
		if (mToast == null)
			mToast = Toast.makeText(mContext, text, Toast.LENGTH_LONG);
		mToast.setText(text);
		if (duration == Toast.LENGTH_SHORT) {
			duration = 1000;
		} else if (duration == Toast.LENGTH_LONG) {
			duration = 2000;
		} else {
			duration = 2000;
		}
		mHandler.postDelayed(run, duration);
		mToast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, (int) Util.dip2px(mContext, 100));
		mToast.show();
	}

	public static void showToast(Context mContext, int resId) {
		showToast(mContext, mContext.getResources().getString(resId), 1000);
	}

	public static void showToast(Context mContext, int resId, int duration) {
		showToast(mContext, mContext.getResources().getString(resId), duration);
	}

}
