package com.jiakang.pandlife.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.StateSet;
import android.util.TypedValue;

import com.jiakang.pandlife.R;

import greendroid.graphics.drawable.DrawableStateSet;

/**
 * Created by Administrator on 2016/1/26.
 */
public class ActionBarDrawable extends BitmapDrawable {
    private static final TypedValue sTypedValue = new TypedValue();
    private ColorFilter mNormalCf;
    private ColorFilter mAltCf;

    /** @deprecated */
    @Deprecated
    public ActionBarDrawable(Resources res, int resId) {
        this(res, res.getDrawable(resId));
    }

    /** @deprecated */
    @Deprecated
    public ActionBarDrawable(Resources res, Drawable d) {
        this((Resources)res, d, -1, -16777216);
    }

    /** @deprecated */
    @Deprecated
    public ActionBarDrawable(Resources res, int resId, int normalColor, int altColor) {
        this(res, res.getDrawable(resId), normalColor, altColor);
    }

    /** @deprecated */
    @Deprecated
    public ActionBarDrawable(Resources res, Drawable d, int normalColor, int altColor) {
        super(res, d instanceof BitmapDrawable?((BitmapDrawable)d).getBitmap():null);
        this.mNormalCf = new LightingColorFilter(-16777216, normalColor);
        this.mAltCf = new LightingColorFilter(-16777216, altColor);
    }

    public ActionBarDrawable(Context context, int resId) {
        this(context, context.getResources().getDrawable(resId));
    }

    public ActionBarDrawable(Context context, int resId, int normalAttr, int defNormalColor, int altAttr, int defAltColor) {
        this(context, context.getResources().getDrawable(resId), getColorFromTheme(context, normalAttr, defNormalColor), getColorFromTheme(context, altAttr, defAltColor));
    }

    public ActionBarDrawable(Context context, Drawable d) {
        this(context, d, getColorFromTheme(context, R.attr.gdActionBarItemColorNormal, -1), getColorFromTheme(context, R.attr.gdActionBarItemColorAlt, -16777216));
    }

    public ActionBarDrawable(Context context, Drawable d, int normalAttr, int defNormalColor, int altAttr, int defAltColor) {
        this(context, d, getColorFromTheme(context, normalAttr, defNormalColor), getColorFromTheme(context, altAttr, defAltColor));
    }

    public ActionBarDrawable(Context context, int resId, int normalColor, int altColor) {
        this(context, context.getResources().getDrawable(resId), normalColor, altColor);
    }

    public ActionBarDrawable(Context context, Drawable d, int normalColor, int altColor) {
        super(context.getResources(), d instanceof BitmapDrawable?((BitmapDrawable)d).getBitmap():null);
        this.mNormalCf = new LightingColorFilter(-16777216, normalColor);
        this.mAltCf = new LightingColorFilter(-16777216, altColor);
    }

    private static int getColorFromTheme(Context context, int attr, int defaultColor) {
        TypedValue var3 = sTypedValue;
        synchronized(sTypedValue) {
            TypedValue value = sTypedValue;
            Resources.Theme theme = context.getTheme();
            if(theme != null) {
                theme.resolveAttribute(attr, value, true);
                if(value.type >= 16 && value.type <= 31) {
                    return value.data;
                }
            }

            return defaultColor;
        }
    }

    public boolean isStateful() {
        return true;
    }

    protected boolean onStateChange(int[] stateSet) {
        boolean useAlt = StateSet.stateSetMatches(DrawableStateSet.ENABLED_PRESSED_STATE_SET, stateSet) || StateSet.stateSetMatches(DrawableStateSet.ENABLED_FOCUSED_STATE_SET, stateSet);
        this.setColorFilter(useAlt?this.mAltCf:this.mNormalCf);
        return true;
    }
}
