package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.MainActivity_;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.AreaInfo;
import com.jiakang.pandlife.info.CommunityInfo;
import com.jiakang.pandlife.info.MyInfo;
import com.jiakang.pandlife.info.NearbyStationInfo;
import com.jiakang.pandlife.info.SetMyStationInfo;
import com.jiakang.pandlife.info.SiteInfo;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.CityItem;
import com.jiakang.pandlife.item.CommunityItem;
import com.jiakang.pandlife.item.NearbyStationItem;
import com.jiakang.pandlife.item.ProvinceItem;
import com.jiakang.pandlife.item.SiteItem;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.List;

/**
 * 选择站点
 */
@EActivity(R.layout.activity_select_site)
public class SelectSiteActivity extends BaseActy {
    protected static final String TAG = "SelectSiteActivity";

    @ViewById(R.id.assi_lv_site)
    ListView siteLV;

    private ItemAdapter mItemAdapter;
    /**
     * 选择站点接口
     */
    private SiteInfo mSiteInfo = new SiteInfo();
    /** 绑定站点接口 */
    private SetMyStationInfo mSetMyStationInfo = new SetMyStationInfo();
    /** 获取我的信息接口 */
    private MyInfo mMyInfo = new MyInfo();
    private UserItem mUserItem;

    private CommunityItem mCommunityItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar() {
        initTitleBar(R.id.assi_tb_title, "选择站点");
        bindView();
        bindInfo();
    }

    private void bindView() {
        mItemAdapter = new ItemAdapter(mContext);
        siteLV.setAdapter(mItemAdapter);

        Intent intent = getIntent();
        mCommunityItem = (CommunityItem)intent.getExtras().get("communityItem");
    }

    private void bindInfo() {
        ApiManager apiManager = ApiManager.getInstance();
        mSiteInfo.setLatitude(mCommunityItem.getLatitude());
        mSiteInfo.setLongitude(mCommunityItem.getLongitude());
        apiManager.request(mSiteInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        if (mSiteInfo.getAllItems().size() == 0) {
                            CustomToast.showToast(mContext, "未获取到站点信息，请重新获取");
                            return;
                        }
                        mItemAdapter.addItems((List) mSiteInfo.getAllItems());
                        mItemAdapter.notifyDataSetChanged();

                        siteLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                ListView listView = (ListView) parent;
                                SiteItem siteItem = (SiteItem) listView.getItemAtPosition(position);
                                ApiManager apiManagerSetSite = ApiManager.getInstance();
                                mSetMyStationInfo.setId(siteItem.getShopid());
                                apiManagerSetSite.request(mSetMyStationInfo, new AbsOnRequestListener(mContext) {
                                    @Override
                                    public void onRequestSuccess(int stat, JSONObject jsonObject) {
                                        super.onRequestSuccess(stat, jsonObject);
                                        try {
                                            if (stat == 1) {
                                                CustomToast.showToast(mContext, "站点绑定成功");
//                                                Intent intent = new Intent(mContext, MainActivity_.class);
//                                                startActivity(intent);
                                                //绑定成功后获取一遍我的信息
                                                ApiManager apiManager = ApiManager.getInstance();
                                                apiManager.request(mMyInfo, new AbsOnRequestListener(mContext) {
                                                    @Override
                                                    public void onRequestSuccess(int result, JSONObject jsonObject) {
                                                        super.onRequestSuccess(result, jsonObject);
                                                        try {
                                                            mUserItem = mMyInfo.getUserItem();
                                                            setResult(RESULT_OK);
                                                            finish();
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });

                                            } else {
                                                JKLog.i(TAG, "失败");
                                            }
                                        } catch (Exception e) {

                                        }
                                    }
                                });

                            }
                        });

                    } else {
                        JKLog.i(TAG, "失败");
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
