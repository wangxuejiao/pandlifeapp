package com.jiakang.pandlife.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.jiakang.pandlife.annotation.utils.ClassUtils;
import com.jiakang.pandlife.annotation.utils.CursorUtil;
import com.jiakang.pandlife.annotation.utils.SqlBuilder;
import com.jiakang.pandlife.item.AdvertisementItem;
import com.jiakang.pandlife.item.ConsumptionDetailItem;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.GroupPurchaseItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.JpushNotificationItem;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.item.NearbyMerchantItem;
import com.jiakang.pandlife.item.PostDetail;
import com.jiakang.pandlife.item.RedPackageItem;
import com.jiakang.pandlife.item.ReplayItem;
import com.jiakang.pandlife.item.SendBagItem;


/**
 * 数据库方法封装，创建表，删除表，数据(增删改查)...
 */
public class DataCacheDBManage {
	private String TAG = "DataCacheDBManage";
	private DataCacheDBHelper dbOpenHelper;

	public DataCacheDBManage(Context context, String dbName) {
		dbOpenHelper = new DataCacheDBHelper(context, dbName);
	}

	public void dropTable(String taleName) {
		dbOpenHelper.getWritableDatabase().execSQL(
				"DROP TABLE IF EXISTS " + taleName);
	}

	public void doSQL(String sql) {
		dbOpenHelper.getWritableDatabase().execSQL(sql);
	}

	public void clear(String table) {
		doSQL("delete from " + table);
	}

	/**
	 * 查询
	 * 
	 * @param sql
	 * @return Cursor
	 */
	public Cursor queryBySQL(String sql) {
		return dbOpenHelper.getWritableDatabase().rawQuery(sql, null);
	}

	public void createTable(Item item) {

	}

	public void close() {
		dbOpenHelper.close();
	}

	public SQLiteDatabase getWritableDatabase() {
		return dbOpenHelper.getWritableDatabase();
	}

	public SQLiteDatabase getReadableDatabase() {
		return dbOpenHelper.getReadableDatabase();
	}

	/**
	 * 查询所有RecommendRecruit
	 * 
	 * @return
	 */
//	public List<Item> getAllRecommendRecruit() {
//		return getRecommendRecruitBySql("select * from "
//				+ DataDBHelper.TAB_RECOMMEND_RECRUIT);
//	}

	/**
	 * 查询所有RecommendRecruit
	 * 
	 * @return
	 */
//	public List<Item> getAllRecommendRecruit(String orderBy) {
//		return getRecommendRecruitBySql("select * from "
//				+ DataDBHelper.TAB_RECOMMEND_RECRUIT + " where pcode like"
//				+ orderBy);
//	}

	/**
	 * 根据sql筛选RecommendRecruit，目前只有根据输入RecommendRecruit名称模糊查询用到
	 * 
	 * @param sql
	 * @return
	 */
//	public List<Item> getRecommendRecruitBySql(String sql) {
//		List<Item> itemList = new ArrayList<Item>();
//		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
//		if (cursor != null && cursor.getCount() > 0) {
//			while (cursor.moveToNext()) {
//				RecommendRecruitItem recommendRecruitItem = CursorUtil
//						.fromJson(cursor, RecommendRecruitItem.class);
//				if (recommendRecruitItem != null) {
//					HWLog.i("recommendRecruitItem",
//							"recommendRecruitItem------------------------------------------>"
//									+ recommendRecruitItem);
//					recommendRecruitItem
//							.setLayoutId(R.layout.item_reward_recommend);
//					itemList.add(recommendRecruitItem);
//				}
//			}
//		}
//		HWLog.i("AreaDBManage",
//				"getAllRecommendRecruit方法中-------->：cursor.getCount()为："
//						+ cursor.getCount());
//		cursor.close();
//		return itemList;
//	}

	/**
	 * -------------------------------通用----------------------------------------
	 */

	/**
	 * 通用
	 * 
	 * @param sql
	 * @param cls
	 * @return
	 */
	public List<Item> getList(String sql, Class<?> cls) {
		List<Item> itemList = new ArrayList<Item>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object sysItem = CursorUtil.fromJson(cursor, cls);
				if (sysItem != null)
					itemList.add((Item) sysItem);
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * 通用
	 * 
	 * @param sql
	 * @param cls
	 * @return
	 */
	public <T> List<T> getTList(String sql, Class<T> cls) {
		List<T> itemList = new ArrayList<T>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object item = CursorUtil.fromJson(cursor, cls);
				if (item != null)
					itemList.add((T) item);
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * 通用通过sql和class返回实体
	 * 
	 * @param sql
	 * @param cls
	 * @return T
	 */
	public <T> T getTByClass(String sql, Class<T> cls) {
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			Object item = CursorUtil.fromJson(cursor, cls);
			if (item != null)
				return (T) item;
		}
		cursor.close();
		return null;
	}

	/**
	 * 通用插入
	 * 
	 * @param item
	 */
	public void insert(Item item) {
		insert(item, ClassUtils.getTableName(item.getClass()));
	}

	/**
	 * 通用插入
	 * 
	 * @param item
	 * @param table
	 * @return void
	 */
	public void insert(Item item, String table) {
		if (TextUtils.isEmpty(table)) {
			table = ClassUtils.getTableName(item.getClass());
		}
		Map<String, Object> maps = CursorUtil.getMaps(item);
		String sql = SqlBuilder.getInsertSql(table, maps, null, true, false,
				ClassUtils.getPrimaryKeyColumn(item.getClass()));
		// XYLog.e(TAG, "insert方法中--插入语句-------->：sql为：" + sql);
		doSQL(sql);
	}

	/**
	 * 通用插入
	 * 
	 * @param str
	 * @return void
	 */
	public void insert(String str) {
		insert(str, "t_data");
	}

	/**
	 * 通用插入String数据
	 * 
	 * @param str
	 * @param table
	 * @return void
	 */
	public void insert(String str, String table) {
		doSQL("insert into " + table + " (`searchRecord`) values ('" + str
				+ "')");
	}

	// public List<Item> getSearchRecord() {
	// return getSearchRecordBySql("select * from t_data");
	// }

	// public List<Item> getSearchRecordBySql(String sql){
	// List<Item> itemList = new ArrayList<Item>();
	// Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
	// if (cursor != null && cursor.getCount() > 0) {
	// while (cursor.moveToNext()) {
	// SearchRecordItem searchRecordItem = CursorUtil.fromJson(cursor,
	// SearchRecordItem.class);
	// if (searchRecordItem != null) {
	// HWLog.i("searchRecordItem",
	// "searchRecordItem------------------------------------------>"+searchRecordItem);
	// // searchRecordItem.setLayoutId(R.layout.item_reward_recommend);
	// itemList.add(searchRecordItem);
	// }
	// }
	// }
	// HWLog.i("AreaDBManage",
	// "getAllRecommendRecruit方法中-------->：cursor.getCount()为：" +
	// cursor.getCount());
	// cursor.close();
	// return itemList;
	// }


	/**
	 * -------------------------------获取广告模块----------------------------------------
	 * -------------------------------TAB_ADVERTISEMENT_CACHE表----------------------------------------
	 */
	public List<AdvertisementItem> getAdvertisementList(){
		String sql = "select * from " + DataCacheDBHelper.TAB_ADVERTISEMENT_CACHE + " order by id ";

		List<AdvertisementItem> itemList = new ArrayList<AdvertisementItem>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object object = CursorUtil.fromJson(cursor, AdvertisementItem.class);
				if (object != null) {
					AdvertisementItem advertisementItem = (AdvertisementItem) object;
					itemList.add(advertisementItem);
				}
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * -------------------------------获取团购模块----------------------------------------
	 * -------------------------------TAB_GROUPPURCHASE_CACHE表----------------------------------------
	 */
	public List<GroupPurchaseItem> getGroupPurchaseList(){
		String sql = "select * from " + DataCacheDBHelper.TAB_GROUPPURCHASE_CACHE + " order by id ";

		List<GroupPurchaseItem> itemList = new ArrayList<GroupPurchaseItem>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object object = CursorUtil.fromJson(cursor, GroupPurchaseItem.class);
				if (object != null) {
					GroupPurchaseItem groupPurchaseItem = (GroupPurchaseItem) object;
					itemList.add(groupPurchaseItem);
				}
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * -------------------------------获取我的未取包裹模块----------------------------------------
	 * -------------------------------TAB_GROUPPURCHASE_CACHE表----------------------------------------
	 */
	public List<ExpressListItem> getUnGetExpressList(){
		String sql = "select * from " + DataCacheDBHelper.TAB_MYEXPRESS_CACHE;

		List<ExpressListItem> itemList = new ArrayList<ExpressListItem>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object object = CursorUtil.fromJson(cursor, ExpressListItem.class);
				if (object != null) {
					ExpressListItem expressListItem = (ExpressListItem) object;
					itemList.add(expressListItem);
				}
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * -------------------------------获取附近商家裹模块----------------------------------------
	 * -------------------------------TAB_NEARBY_MERCHANT_CACHE表----------------------------------------
	 */
	public List<NearbyMerchantItem> getGetNearbyMerchantList(){
		String sql = "select * from " + DataCacheDBHelper.TAB_NEARBY_MERCHANT_CACHE;

		List<NearbyMerchantItem> itemList = new ArrayList<NearbyMerchantItem>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object object = CursorUtil.fromJson(cursor, NearbyMerchantItem.class);
				if (object != null) {
					NearbyMerchantItem nearbyMerchantItem = (NearbyMerchantItem) object;
					itemList.add(nearbyMerchantItem);
				}
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * -------------------------------获取收支明细裹模块----------------------------------------
	 * -------------------------------TAB_CONSUMPTION_DETAIL_CACHE表----------------------------------------
	 */
	public List<ConsumptionDetailItem> getGetConsumptionDetailList(){
		String sql = "select * from " + DataCacheDBHelper.TAB_CONSUMPTION_DETAIL_CACHE;

		List<ConsumptionDetailItem> itemList = new ArrayList<ConsumptionDetailItem>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object object = CursorUtil.fromJson(cursor, ConsumptionDetailItem.class);
				if (object != null) {
					ConsumptionDetailItem consumptionDetailItem = (ConsumptionDetailItem) object;
					itemList.add(consumptionDetailItem);
				}
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * -------------------------------获取我的红包裹模块----------------------------------------
	 * -------------------------------TAB_MY_REDPACKAGE_CACHE表----------------------------------------
	 */
	public List<RedPackageItem> getGetMyRedPackageList(){
		String sql = "select * from " + DataCacheDBHelper.TAB_MY_REDPACKAGE_CACHE;

		List<RedPackageItem> itemList = new ArrayList<RedPackageItem>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object object = CursorUtil.fromJson(cursor, RedPackageItem.class);
				if (object != null) {
					RedPackageItem redPackageItem = (RedPackageItem) object;
					itemList.add(redPackageItem);
				}
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * -------------------------------获取我的订单裹模块----------------------------------------
	 * -------------------------------TAB_MYINDENT_CACHE表----------------------------------------
	 */
	public List<MyIndentItem> getGetMyIndentList(){
		String sql = "select * from " + DataCacheDBHelper.TAB_MYINDENT_CACHE;

		List<MyIndentItem> itemList = new ArrayList<MyIndentItem>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object object = CursorUtil.fromJson(cursor, MyIndentItem.class);
				if (object != null) {
					MyIndentItem myIndentItem = (MyIndentItem) object;
					itemList.add(myIndentItem);
				}
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * -------------------------------获取我的寄件裹模块----------------------------------------
	 * -------------------------------TAB_MY_SENDEXPRESS_CACHE表----------------------------------------
	 */
	public List<SendBagItem> getGetMySendExpressList(){
		String sql = "select * from " + DataCacheDBHelper.TAB_MY_SENDEXPRESS_CACHE;

		List<SendBagItem> itemList = new ArrayList<SendBagItem>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object object = CursorUtil.fromJson(cursor, SendBagItem.class);
				if (object != null) {
					SendBagItem sendBagItem = (SendBagItem) object;
					itemList.add(sendBagItem);
				}
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * 获取极光推送列表
	 * @return
	 */
	public List<JpushNotificationItem> getJpushNotificationList(){

		String sql = "select * from " + DataCacheDBHelper.TAB_JPUSH_NOTIFICATION_CACHE;
		List<JpushNotificationItem> jpushNotificationItemList = new ArrayList<>();
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql,null);
		if(cursor != null && cursor.getCount() > 0){
			while (cursor.moveToNext()){
				Object object = CursorUtil.fromJson(cursor,JpushNotificationItem.class);
				if(object != null){
					JpushNotificationItem jpushNotificationItem = (JpushNotificationItem) object;
					jpushNotificationItemList.add(jpushNotificationItem);
				}
			}
		}
		if (cursor != null) {
			cursor.close();
		}
		return jpushNotificationItemList;
	}

	/**
	 * 修改极光推送未读标记
	 * @param id
	 */
	public void updateJpushNotificationItem(int id){

		String sql = "update " + DataCacheDBHelper.TAB_JPUSH_NOTIFICATION_CACHE + " set flag = " + "'" + "已读" + "'" + " where id = " + id;
		dbOpenHelper.getWritableDatabase().execSQL(sql);
	}


	//获取回复列表
	public List<ReplayItem> getReplayList(String dialog_id){

		List<ReplayItem> replayItemList = new ArrayList<>();
		String sql = "select * from " + DataCacheDBHelper.TAB_REPLY_CACHE + " where dialog_id = " + dialog_id;
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql,null);
		if(cursor != null && cursor.getCount() > 0){
			while (cursor.moveToNext()){
				Object object = CursorUtil.fromJson(cursor, ReplayItem.class);
				if(object != null){
					ReplayItem replayItem = (ReplayItem) object;
					replayItemList.add(replayItem);
				}
			}
		}

		if (cursor != null) {
			cursor.close();
		}

		return replayItemList;
	}

	/**
	 * 获取贴子详细列表
	 * @param replyid
	 * @return
	 */
	public PostDetail.DataEntityPostDetail getPostDetail(String replyid){

		PostDetail.DataEntityPostDetail dataEntityPostDetail = null;

		String sql = "select * from " + DataCacheDBHelper.TAB_POST_DETAIL_CACHE + " where replyid = " + "'" + replyid + "'";
		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql,null);
		if(cursor != null && cursor.getCount() > 0){
			if(cursor.moveToNext()){

				Object object = CursorUtil.fromJson(cursor, PostDetail.DataEntityPostDetail.class);
				if(object != null){

					dataEntityPostDetail = (PostDetail.DataEntityPostDetail) object;
				}
			}
		}

		if (cursor != null) {
			cursor.close();
		}
		return dataEntityPostDetail;
	}


	
	/**
	 * -------------------------------获取热门城市----------------------------------------
	 * -------------------------------TAB_HOTCITY_CACHE表----------------------------------------
	 */
//	public List<HotCityItem> getHotCityList(){
//		String sql = "select * from " + DataCacheDBHelper.TAB_HOTCITY_CACHE + " order by id ";
//
//		List<HotCityItem> itemList = new ArrayList<HotCityItem>();
//		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
//		if (cursor != null && cursor.getCount() > 0) {
//			while (cursor.moveToNext()) {
//				Object object = CursorUtil.fromJson(cursor, HotCityItem.class);
//				if (object != null) {
//					HotCityItem hotCityItem = (HotCityItem) object;
//					itemList.add(hotCityItem);
//				}
//			}
//		}
//		cursor.close();
//
//		return itemList;
//
//	}
//	/**
//	 * 删除热门职位
//	 * @param item
//	 */
//	public void deleteHotCityItem(HotCityItem item) {
//		String sql = "delete from " + DataCacheDBHelper.TAB_HOTCITY_CACHE;
//		doSQL(sql);
//	}
//	
	/**
	 * -------------------------------获取热门职位----------------------------------------
	 * -------------------------------TAB_HOTJOB_CACHE表----------------------------------------
	 */
//	public List<HotJobItem> getHotJobList(){
//		String sql = "select * from " + DataCacheDBHelper.TAB_HOTJOB_CACHE + " order by id ";
//
//		List<HotJobItem> itemList = new ArrayList<HotJobItem>();
//		Cursor cursor = dbOpenHelper.getReadableDatabase().rawQuery(sql, null);
//		if (cursor != null && cursor.getCount() > 0) {
//			while (cursor.moveToNext()) {
//				Object object = CursorUtil.fromJson(cursor, HotJobItem.class);
//				if (object != null) {
//					HotJobItem hotJobItem = (HotJobItem) object;
//					itemList.add(hotJobItem);
//				}
//			}
//		}
//		cursor.close();
//
//		return itemList;
//
//	}
//	/**
//	 * 删除热门职位
//	 * @param item
//	 */
//	public void deleteHotJobItem(HotJobItem item) {
//		String sql = "delete from " + DataCacheDBHelper.TAB_HOTJOB_CACHE;
//		doSQL(sql);
//	}
	
}