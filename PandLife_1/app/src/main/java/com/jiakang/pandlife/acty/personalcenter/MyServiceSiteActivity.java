package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
//import com.example.hjk.amap_android_navi.BasicNaviActivity;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
//import com.jiakang.pandlife.acty.locationnavi.RouteActivity;
import com.jiakang.pandlife.acty.register.SelectLocationOrManualActivity;
import com.jiakang.pandlife.acty.register.SelectLocationOrManualActivity_;
import com.jiakang.pandlife.acty.service.NearbyStationActivity_;
//import com.jiakang.pandlife.acty.share.RoutePlanDemo;
import com.jiakang.pandlife.acty.share.SelectNearbySiteActivity;
import com.jiakang.pandlife.acty.share.SelectNearbySiteActivity_;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.MyStationInfo;
import com.jiakang.pandlife.item.SiteItem;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

/**
 * 我的服务站点
 */
@EActivity(R.layout.activity_my_service_site)
public class MyServiceSiteActivity extends BaseActy {
    protected static final String TAG = "MyServiceSiteActivity";

    @ViewById(R.id.amss_iv_image)
    ImageView imageIV;
    @ViewById(R.id.amss_iv_navigation)
    ImageView navigationIV;
    @ViewById(R.id.amss_iv_call_telephone)
    ImageView callTelephoneIV;
    @ViewById(R.id.amss_tv_name)
    TextView nameTV;
    @ViewById(R.id.amss_tv_address)
    TextView addressTV;
    @ViewById(R.id.amss_tv_telephone)
    TextView phoneTV;
    @ViewById(R.id.amss_tv_open_date)
    TextView openDateTV;
    @ViewById(R.id.amss_btn_change_service_site)
    Button changeServiceSiteBN;

    /** 获取我的默认服务站点 */
    private MyStationInfo mMyStationInfo = new MyStationInfo();
    /** 我的默认站点实体 */
    private SiteItem mSiteItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.amss_tb_title, "我的服务站");

        changeServiceSiteBN.setOnClickListener(this);
        navigationIV.setOnClickListener(this);
        callTelephoneIV.setOnClickListener(this);

        bindView();
    }

    private void bindView(){
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mMyStationInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    mSiteItem = mMyStationInfo.getSiteItem();
                    nameTV.setText(mSiteItem.getNice_name());
                    addressTV.setText(mSiteItem.getAddress());
                    phoneTV.setText(mSiteItem.getMobiphone());
                    openDateTV.setText(mSiteItem.getOpentime());
                } catch (Exception e) {
                    JKLog.e(TAG, "onRequestSuccess方法中-------->：");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_SELECT_SITE) && (resultCode == RESULT_OK)){
//            mSiteItem = (SiteItem)data.getExtras().get(SelectNearbySiteActivity.SELECT_NEARBY_SITE);
//            nameTV.setText(mSiteItem.getNice_name());
//            addressTV.setText(mSiteItem.getAddress());
//            phoneTV.setText(mSiteItem.getMobiphone());
//            openDateTV.setText(mSiteItem.getOpentime());
            bindView();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.amss_iv_navigation:
                //跳转导航页，先调用定位方法

                startLocation();
                break;
            case R.id.amss_iv_call_telephone:
                clickCallPhone(phoneTV.getText().toString());
                break;
            case R.id.amss_btn_change_service_site:
                Intent intentSelectLocationOrManual = new Intent(mContext, SelectLocationOrManualActivity_.class);
                intentSelectLocationOrManual.putExtra(SelectLocationOrManualActivity.SITE_OR_COMMUNITY , "site");
                intentSelectLocationOrManual.putExtra("siteItem", mSiteItem);
                startActivityForResult(intentSelectLocationOrManual, Constant.StaticCode.REQUEST_SELECT_SITE);
                break;
        }
    }

    /**
     * 拨打电话方法
     */
    private void clickCallPhone(String telephoneStr){
        Intent intentCall = new Intent();
        intentCall.setAction(Intent.ACTION_CALL);
        intentCall.setData(Uri.parse("tel:" + telephoneStr));
        startActivity(intentCall);
    }

    /**
     * 进入导航的方法
     */
    private void toNavigation(double locationLongitude, double locationLatitude){
//        Intent intentNavigation = new Intent(this, NavigateArrowOverlayActivity.class);
//        Intent intentNavigation = new Intent(this, RoutePlanDemo.class);

//        intentNavigation.putExtra(BasicNaviActivity.END_LONGITUDE, Double.parseDouble(mSiteItem.getLongitude()));
//        intentNavigation.putExtra(BasicNaviActivity.END_LATITUDE, Double.parseDouble(mSiteItem.getLatitude()));
//        intentNavigation.putExtra(BasicNaviActivity.START_LONGITUDE, locationLongitude);
//        intentNavigation.putExtra(BasicNaviActivity.START_LATITUDE, locationLatitude);
//        startActivityForResult(intentNavigation, Constant.StaticCode.REQUEST_SENDER_ADDRESS);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        //定位回调
        JKLog.i(TAG, aMapLocation.toString());
        double locationLongitude = aMapLocation.getLongitude();
        double locationLatitude = aMapLocation.getLatitude();
        toNavigation(locationLongitude, locationLatitude);
    }
}
