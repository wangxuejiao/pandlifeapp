package com.jiakang.pandlife.acty.personalcenter;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.db.DataCacheDBHelper;
import com.jiakang.pandlife.db.DataCacheDBManage;
import com.jiakang.pandlife.info.ConsumptionDetailInfo;
import com.jiakang.pandlife.item.ConsumptionDetailItem;
import com.jiakang.pandlife.item.NearbyMerchantItem;
import com.jiakang.pandlife.utils.CustomToast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 账户明细
 * Created by Administrator on 2016/1/14.
 */
@EActivity(R.layout.activity_account_detail)
public class AccountDetailActivity extends BaseActy{
    protected static final String TAG = "AccountDetailActivity";

//    @ViewById(R.id.aad_tv_account_sum)
//    TextView accountSumTV;
    @ViewById(R.id.aad_lv_account_detail)
    ListView accountDetailLV;

    private ItemAdapter mItemAdapter;

    /** 获取消费明细接口 */
    private ConsumptionDetailInfo mConsumptionDetailInfo = new ConsumptionDetailInfo();

    private List<ConsumptionDetailItem> allItems = new ArrayList<ConsumptionDetailItem>();

    private DataCacheDBManage dataCacheDBManage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.aad_tb_title, "账户明细");
        dataCacheDBManage = getMyApp().getCacheDataDBManage();
        bindView();
        getCacheDataConsumptionDetail();
    }

    private void bindView(){
        mItemAdapter = new ItemAdapter(mContext);
        accountDetailLV.setAdapter(mItemAdapter);

    }

    /**
     * 通过数据库获取收支明细列表信息
     */
    private void getCacheDataConsumptionDetail(){
        allItems = dataCacheDBManage.getGetConsumptionDetailList();
        if (allItems.size() != 0){
            mItemAdapter.clear();
            mItemAdapter.addItems((List) allItems);
            mItemAdapter.notifyDataSetChanged();

            ConsumptionDetailItem consumptionDetailItem = allItems.get(0);
            if ((consumptionDetailItem.getCurrentTime() + consumptionDetailItem.getSaveTime()) < System.currentTimeMillis()) {
                //清除数据库
                dataCacheDBManage.clear(DataCacheDBHelper.TAB_CONSUMPTION_DETAIL_CACHE);
                //获取网络请求数据
                bindInfo();
            } else {
            }
        }else{
            bindInfo();
        }
    }

    private void bindInfo(){
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mConsumptionDetailInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try{
                    if(mConsumptionDetailInfo.getAllItems().size() == 0){
                        CustomToast.showToast(mContext, "当前没有消费明细");
                        return;
                    }
                    mItemAdapter.addItems((List)mConsumptionDetailInfo.getAllItems());
                    mItemAdapter.notifyDataSetChanged();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
