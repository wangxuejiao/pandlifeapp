package com.jiakang.pandlife.info;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author：xuwangwang E-mail: 541765907@qq.com
 * @date：2014年5月20日 上午10:45:48
 * @version 1.0 String Tag = "QueryProductTypeInfo中：";
 */
public abstract class BaseAbsInfo implements BaseInterfaceInfo {

	protected String TAG = "BaseInfo";
	protected String mResult = BaseInterfaceInfo.CODE_FAIL;
	protected JSONObject data;
	protected String message = "请求失败";

//	public static String data = "data";

	public BaseAbsInfo() {
		TAG = getClass().getSimpleName();
	}

	@Override
	public String requestResult() {
		return mResult;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void getFromDb(JSONArray jsonArray) {

	}

	@Override
	public void setRequestResult(String result) {

	}

}
