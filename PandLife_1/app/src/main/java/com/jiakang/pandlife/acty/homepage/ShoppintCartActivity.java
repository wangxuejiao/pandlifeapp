package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.GoodsItem;
import com.jiakang.pandlife.widget.CustomListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 购物车
 */
@EActivity(R.layout.activity_shopping_cart)
public class ShoppintCartActivity extends BaseActy {
    protected static final String TAG = "ShoppintCartActivity";

    @ViewById(R.id.asc_tv_select_date)
    TextView selectDateTV;
    @ViewById(R.id.asc_btn_select)
    TextView selectBN;
    @ViewById(R.id.asc_clv_goods)
    CustomListView goodsCLV;
    @ViewById(R.id.asc_tv_remarks_content)
    TextView remarksTV;
    @ViewById(R.id.asc_tv_count)
    TextView countTV;
    @ViewById(R.id.asc_btn_confirm)
    Button confirmBN;

    private ItemAdapter mItemAdapter;

    private List<GoodsItem> selectItems = new ArrayList<GoodsItem>();

    //购买总数
    private int countNumber;
    //总价格
    private float countPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.asc_tb_title, "购物车");
        Intent intent = getIntent();
        selectItems = (List<GoodsItem>)intent.getSerializableExtra("list");
        bindView();
        countNumberAndPrice();
    }

    private void bindView(){
        mItemAdapter = new ItemAdapter(mContext);
        goodsCLV.setAdapter(mItemAdapter);
        GoodsItem goodsItem;
        for (int i=0;i<selectItems.size();i++){
            goodsItem = selectItems.get(i);
            goodsItem.setLayout(R.layout.item_shopping_cart);
            goodsItem.setNumber(1);
            mItemAdapter.add(goodsItem);
        }
        mItemAdapter.notifyDataSetChanged();

        mItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {
                int number = Integer.parseInt(view.getTag().toString());
                ((GoodsItem) mItemAdapter.getItem(position)).setNumber(number);

                countNumberAndPrice();
            }
        });
    }

    /**
     * 确认方法
     */
    private void confirm(){
//        Intent intentIndent = new Intent(mContext, WriteIndentActivity_.class);
//        intentIndent.putExtra("list", (Serializable)selectItems);
//        startActivity(intentIndent);
    }

    /**
     * 计算总数和总价格方法
     */
    private void countNumberAndPrice(){
        countNumber = 0;
        countPrice = 0;
        for (int i=0;i<mItemAdapter.getCount();i++){
            countNumber += ((GoodsItem)mItemAdapter.getItem(i)).getNumber();
            countPrice += ((GoodsItem)mItemAdapter.getItem(i)).getNumber() * ((GoodsItem)mItemAdapter.getItem(i)).getPrice();
        }
        countTV.setText("共" + countNumber + "件 合计：￥" + countPrice);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.asc_tv_select_date:
                break;
            case R.id.asc_btn_confirm:
                confirm();
                break;
        }
    }
}
