package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.ProvinceItem;
import com.jiakang.pandlife.item.RedPackageItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的红包接口
 *
 * @author ww
 *
 */
public class MyRedPackInfo extends BaseAbsInfo {

    private static final String TAG = "MyRedPackInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private List<RedPackageItem> allItems = new ArrayList<RedPackageItem>();
    /** 返回红包数量（可用红包）	Int(10) */
    private int usable;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Redpack&a=Myredpack" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", "ff0a6c74e7b4d08b659f9e2737a4ac5c");
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                allItems.clear();
                JSONArray taskJA = jsonObject.getJSONArray("data");
                String itemStr;
                RedPackageItem redPackageItem;
                for(int i=0;i<taskJA.length();i++){
                    itemStr = taskJA.getJSONObject(i).toString();
                    redPackageItem = BaseInfo.gson.fromJson(itemStr, RedPackageItem.class);
                    allItems.add(redPackageItem);
                }

                usable = jsonObject.getInt("usable");
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public List<RedPackageItem> getAllItems() {
        return allItems;
    }

    public void setAllItems(List<RedPackageItem> allItems) {
        this.allItems = allItems;
    }

    public int getUsable() {
        return usable;
    }

    public void setUsable(int usable) {
        this.usable = usable;
    }
}
