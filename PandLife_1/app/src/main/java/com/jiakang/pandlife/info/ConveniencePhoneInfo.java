package com.jiakang.pandlife.info;

import android.util.Log;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.ConveniencePhoneItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by play on 2016/1/11.
 */
public class ConveniencePhoneInfo extends BaseAbsInfo {

    private static final String TAG = "ConveniencePhoneInfo";
    private ConveniencePhoneItem conveniencePhoneItem;
    private List<ConveniencePhoneItem.DataEntity> dataEntityConveniencePhoneList = new ArrayList<>();

    public ConveniencePhoneItem getConveniencePhoneItem() {
        return conveniencePhoneItem;
    }

    public void setConveniencePhoneItem(ConveniencePhoneItem conveniencePhoneItem) {
        this.conveniencePhoneItem = conveniencePhoneItem;
    }

    public List<ConveniencePhoneItem.DataEntity> getDataEntityConveniencePhoneList() {
        return dataEntityConveniencePhoneList;
    }

    public void setDataEntityConveniencePhoneList(List<ConveniencePhoneItem.DataEntity> dataEntityConveniencePhoneList) {
        this.dataEntityConveniencePhoneList = dataEntityConveniencePhoneList;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {

        return Constant.mWebAddress + "m=Api&c=Server&a=Getfacilitatetel&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        conveniencePhoneItem = BaseInfo.gson.fromJson(jsonObject.toString(),ConveniencePhoneItem.class);
        dataEntityConveniencePhoneList.clear();
        dataEntityConveniencePhoneList = conveniencePhoneItem.getData();
        for (int i = 0; i < dataEntityConveniencePhoneList.size(); i++) {
            dataEntityConveniencePhoneList.get(i).setLayout(R.layout.item_convenience_phone);
        }
    }
}
