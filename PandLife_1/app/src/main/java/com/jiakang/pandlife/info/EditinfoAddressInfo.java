package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 4.添加寄件人信息接口
 *
 * @author ww
 *
 */
public class EditinfoAddressInfo extends BaseAbsInfo {

    private static final String TAG = "EditinfoAddressInfo";

    /**收件人/寄件人id	Int(10)*/
    private int id;
    /** 姓名	Varchar(32) */
    private String name;
    /**信息类型	（s：寄件人；r：收件人）*/
    private String type;
    /** 省份id	Int(10) */
    private int province_id;
    /** 市id	Int(10) */
    private int city_id;
    /** 区/县id	Int(10) */
    private int zone_id;
    /** 详细地址	Varchar(32) */
    private String address;
    /** 手机号码	Varchar(11) */
    private String phone;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=ExpressUsers&a=Editinfo" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("id", id);
            json.put("name", name);
            json.put("type", type);
            json.put("province_id", province_id);
            json.put("city_id", city_id);
            json.put("zone_id", zone_id);
            json.put("address", address);
            json.put("phone", phone);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");


            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProvince_id() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getZone_id() {
        return zone_id;
    }

    public void setZone_id(int zone_id) {
        this.zone_id = zone_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
