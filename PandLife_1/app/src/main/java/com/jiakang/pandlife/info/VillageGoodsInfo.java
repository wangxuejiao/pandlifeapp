package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.VillageGoodsItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by play on 2016/1/25.
 */
public class VillageGoodsInfo extends BaseAbsInfo{

    private int pageIndex;
    private int pageSize;
    private VillageGoodsItem villageGoodsItem;
    private List<VillageGoodsItem.DataEntityVillageGoods> dataEntityVillageGoodsList = new
            ArrayList<>();

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public VillageGoodsItem getVillageGoodsItem() {
        return villageGoodsItem;
    }

    public void setVillageGoodsItem(VillageGoodsItem villageGoodsItem) {
        this.villageGoodsItem = villageGoodsItem;
    }

    public List<VillageGoodsItem.DataEntityVillageGoods> getDataEntityVillageGoodsList() {
        return dataEntityVillageGoodsList;
    }

    public void setDataEntityVillageGoodsList(List<VillageGoodsItem.DataEntityVillageGoods> dataEntityVillageGoodsList) {
        this.dataEntityVillageGoodsList = dataEntityVillageGoodsList;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Forum&a=HardgoodsList&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        villageGoodsItem = BaseInfo.gson.fromJson(jsonObject.toString(),VillageGoodsItem.class);
        dataEntityVillageGoodsList.clear();
        dataEntityVillageGoodsList = villageGoodsItem.getData();
        for (int i = 0; i < dataEntityVillageGoodsList.size(); i++) {
            dataEntityVillageGoodsList.get(i).setLayout(R.layout.item_village_goods);
        }
    }
}
