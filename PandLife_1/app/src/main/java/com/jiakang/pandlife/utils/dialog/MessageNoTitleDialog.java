package com.jiakang.pandlife.utils.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.jiakang.pandlife.R;


/**
 * Create custom Dialog windows for your application
 * Custom dialogs rely on custom layouts wich allow you to
 * create and use your own look & feel.
 *
 * Under GPL v3 : http://www.gnu.org/licenses/gpl-3.0.html
 *
 * <a href="http://my.oschina.net/arthor" target="_blank" rel="nofollow">@author</a> antoine vianey
 */
public class MessageNoTitleDialog extends Dialog {
    public static MessageDialogListener messageDialogListener;
    private MessageNoTitleDialog dialog;

    public MessageNoTitleDialog(Context context, int theme) {
        super(context, theme);
    }

    public MessageNoTitleDialog(Context context) {
        super(context);

    }

    public MessageNoTitleDialog getDialog(Context context,String content) {
        Builder b = new Builder(context);
        b.setMessage(content);
        dialog = b.create();
        dialog.show();
        return dialog;
    }
    public MessageNoTitleDialog getDialog(Context context,String content,String left,String right) {
        Builder b = new Builder(context);
        b.setMessage(content);
        b.setLeft(left);
        b.setRight(right);
        
        dialog = b.create();
        
        dialog.show();
        return dialog;
    }
    /**
     * Helper class for creating a custom dialog
     */
    public static class Builder {
        private Context context;
        private TextView txtName;
        private TextView txtContent,txtLeft,txtRight;
        private String title;
        private String content;
        private String left, Right;

        public void setLeft(String left) {
            this.left = left;
        }

        public void setRight(String right) {
            Right = right;
        }

        public Builder(Context context) {
            this.context = context;
        }

        public void setMessage(String content) {
            this.content = content;
        }
        /**
         * Create the custom dialog
         */
        @SuppressLint("Override")
        public MessageNoTitleDialog create() {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            final MessageNoTitleDialog dialog = new MessageNoTitleDialog(context,
                    R.style.CustomDialog);
            dialog.setCanceledOnTouchOutside(false);
            View layout = inflater.inflate(R.layout.view_message_no_titile_dialog, null);
            txtContent = (TextView) layout.findViewById(R.id.txt_content);
            txtLeft = (TextView) layout.findViewById(R.id.txt_cancel);
            txtRight = (TextView) layout.findViewById(R.id.txt_sure);
            txtLeft.setText(left);
            txtRight.setText(Right);
            txtContent.setText(content);
            layout.findViewById(R.id.txt_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (messageDialogListener != null) {
                        messageDialogListener.cancel();
                    }
                    dialog.dismiss();
                }
            });
            layout.findViewById(R.id.txt_sure).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    
             
                    if (messageDialogListener != null) {
                        messageDialogListener.sure();

                    }
                    dialog.dismiss();
                }
            });
            dialog.addContentView(layout, new LayoutParams(
                    LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
            dialog.setContentView(layout);
            return dialog;
        }

    }

    public interface MessageDialogListener {
        void sure();

        void cancel();
    }


    public MessageDialogListener getClearDataListener() {
        return messageDialogListener;
    }

    public void seteditDialogListener(MessageDialogListener messageDialogListener) {
        this.messageDialogListener = messageDialogListener;
    }
}