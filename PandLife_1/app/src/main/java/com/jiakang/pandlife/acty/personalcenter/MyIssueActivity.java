package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.Post.PostActivity_;
import com.jiakang.pandlife.acty.Post.PostDetailActivity_;
import com.jiakang.pandlife.acty.neighbour.VillageGoodsActivity;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.MyForumInfo;
import com.jiakang.pandlife.info.NeibourhoodInfo;
import com.jiakang.pandlife.info.PostTypeInfo;
import com.jiakang.pandlife.item.NeibourhoodItem;
import com.jiakang.pandlife.item.PostTypeItem;
import com.jiakang.pandlife.widget.Banner;
import com.jiakang.pandlife.widget.PullToRefreshListView;
import com.jiakang.pandlife.widget.TitleBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.List;


/**
 * 我的发布
 */
@EActivity(R.layout.activity_my_issue)
public class MyIssueActivity extends BaseActy {
    protected static final String TAG = "MyIssueActivity";

    @ViewById(R.id.amis_lv_issue)
    PullToRefreshListView myIssueLV;

    private ItemAdapter mItemAdapter;

    private static final int FIRST_REQUEST = 0;
    private static final int REFRESH = 1; //下拉刷新
    private static final int MORE = 2;//加载更多
    private boolean isMore = true; //判断有下一页

    /**
     * 每页个数，BaseInfo.pageSize = 20 为默认值
     */
    private int pageSize = 20;
    /**
     * 当前请求的页
     */
    private int pageIndexGet = 1;

    //帖子列表类
    private MyForumInfo mMyForumInfo = new MyForumInfo();
    private List<NeibourhoodItem.DataEntityNeibourhood> mDataEntityNeibourhoodList;

    //帖子类型类
    private PostTypeInfo mPostTypeInfo = new PostTypeInfo();
    private List<PostTypeItem.DataEntityPostType> mDataEntityPostTypeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.amis_tb_title, "我的发布");

        mItemAdapter = new ItemAdapter(mContext);
        myIssueLV.setShowFootView(true);
        myIssueLV.setAdapter(mItemAdapter);

        //进入页面初始化数据
        neibourhoodRequest(REFRESH);

        myIssueLV.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position == mDataEntityNeibourhoodList.size() + 1){
                    return;
                }
                Intent intent = new Intent(mContext, PostDetailActivity_.class);
                int postId = Integer.valueOf(mDataEntityNeibourhoodList.get(position - 1).getId());
                intent.putExtra("delete", "yes");
                intent.putExtra("postId", postId);
                intent.putExtra("postType", mDataEntityNeibourhoodList.get(position - 1).getType());
                startActivity(intent);
            }
        });

        myIssueLV.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            @Override
            public void onRefresh() {

                neibourhoodRequest(REFRESH);
            }
        });

        myIssueLV.setOnMoreListener(new PullToRefreshListView.OnMoreListener() {
            @Override
            public void onMore() {

                Log.i(TAG, "------>进入了加载更多");
                if (isMore) {
                    neibourhoodRequest(MORE);
                } else {
                    Toast.makeText(mContext, "没有更多了！", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * 帖子列表请求
     */
    private void neibourhoodRequest(final int listType) {

        if (listType == REFRESH || listType == FIRST_REQUEST) {
            pageIndexGet = 1;
        } else if (listType == MORE) {
            pageIndexGet++;
        } else {
            pageIndexGet = 1;
        }

        mMyForumInfo.setPageIndex(pageIndexGet);
        mMyForumInfo.setPageSize(pageSize);

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mMyForumInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mDataEntityNeibourhoodList = mMyForumInfo.getDataEntityNeibourhoodList();

                if (mDataEntityNeibourhoodList != null && mDataEntityNeibourhoodList.size() > 0) {
                    onPostTypeRequest();
                }

                //判断是否有下一页
                if (pageIndexGet < mMyForumInfo.getNeibourhoodItem().getPage().getTotalPage()) {
                    isMore = true;
                } else {
                    isMore = false;
                }

                if (listType != MORE) {

                    mItemAdapter.clear();
                }

                myIssueLV.onRefreshComplete();
                myIssueLV.onMoreComplete(isMore);
            }


        });
    }

    /**
     * 帖子类型请求
     */
    private void onPostTypeRequest() {

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mPostTypeInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);


                mDataEntityPostTypeList = mPostTypeInfo.getDataEntityPostTypeList();
                for (int i = 0; i < mDataEntityNeibourhoodList.size(); i++) {

                    String postType = mDataEntityNeibourhoodList.get(i).getType();

                    for (int j = 0; j < mDataEntityPostTypeList.size(); j++) {

                        if (postType.equals(mDataEntityPostTypeList.get(j).getId())) {
                            mDataEntityNeibourhoodList.get(i).setType(mDataEntityPostTypeList.get(j).getName());
                        }
                    }
                }
                mItemAdapter.addItems((List) mDataEntityNeibourhoodList);
                mItemAdapter.notifyDataSetChanged();

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
