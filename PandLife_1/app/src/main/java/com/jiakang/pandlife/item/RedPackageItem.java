package com.jiakang.pandlife.item;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.annotation.sqlite.Id;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.db.DataCacheDBHelper;

/**
 * Created by Administrator on 2016/1/11.
 */
@Table(name = DataCacheDBHelper.TAB_MY_REDPACKAGE_CACHE)
public class RedPackageItem extends Item{

    /** 数据库id	Int(10) */
    @Id
    private int _id;
    /** 红包id	Int(10) */
    private int id;
    /** 红包标题	Varchar(32) */
    private String title;
    /** 红包金额	Int(10) */
    private int money;
    /** 过期时间	Varchar(32) */
    private String etime;
    /** 是否可用（是：y 否：n） */
    private String is_enable;

    /** 插入数据库时的时间 */
    private long currentTime = System.currentTimeMillis();
    /** 保存的时间 */
    private long saveTime = 600000;

    @Override
    public int insert() {
        PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
        return 1;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public long getSaveTime() {
        return saveTime;
    }

    public void setSaveTime(long saveTime) {
        this.saveTime = saveTime;
    }

    private int layout = R.layout.item_my_red_package;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getEtime() {
        return etime;
    }

    public void setEtime(String etime) {
        this.etime = etime;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public String getIs_enable() {
        return is_enable;
    }

    public void setIs_enable(String is_enable) {
        this.is_enable = is_enable;
    }

}
