package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 选择绑定第三方
 */
@EActivity(R.layout.activity_other_login_band_select)
public class OtherLoginBandSelectActivity extends BaseActy {
    protected static final String TAG = "OtherLoginBandSelectActivity";

    @ViewById(R.id.aolbs_tv_newuser_register)
    TextView newUserRegisterTV;
    @ViewById(R.id.aolbs_tv_olduser_band)
    TextView oldUserBandTV;

//    /** 记录第三方数据用户id */
//    public static String webid;
//    /** 记录第三方数据用户昵称 */
//    public static String webname;
//    /** 记录第三方数据用户头像 */
//    public static String webpic;
//    /** 记录第三方类型 */
//    public static String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.aolbs_tb_title, "选择绑定");

        Intent intent = getIntent();
//        webid = intent.getExtras().getString("webid");
//        webname = intent.getExtras().getString("webname");
//        webpic = intent.getExtras().getString("webpic");
//        type = intent.getExtras().getString("type");

        bindView();
    }

    private void bindView(){
        newUserRegisterTV.setOnClickListener(this);
        oldUserBandTV.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_OLDUSER_BAND ) && (resultCode == RESULT_OK)){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.aolbs_tv_newuser_register:
                Intent intentRegister = new Intent(this, RegisterGetCodeActivity_.class);
                startActivity(intentRegister);
                break;
            case R.id.aolbs_tv_olduser_band:
                Intent intentBand = new Intent(mContext, BandLoginActivity_.class);
                startActivityForResult(intentBand, Constant.StaticCode.REQUEST_OLDUSER_BAND);
                break;
        }
    }
}
