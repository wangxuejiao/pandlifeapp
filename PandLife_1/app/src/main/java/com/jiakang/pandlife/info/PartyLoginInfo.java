package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.utils.JKLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;


/**
 * 10.第三方登陆接口
 *
 * @author ww
 *
 */
public class PartyLoginInfo extends BaseAbsInfo {

    private static final String TAG = "PartyLoginInfo";

    /** 第三方返回的用户id */
    private String webid;
    /** 用户昵称 */
    private String webname;
    /** 用户头像地址 */
    private String webpic;
    /** 第三方类型'qq','weixin','taobao' */
    private String type;

    /** 接收的数据token 登录凭证	Varchar(32) */
    private String token;
    /** 第三方登陆记录表id */
    private String id;


    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Partylogin&a=Partylogin" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("webid", webid);
//            json.put("webname", webname);
//            json.put("webpic", webpic);
//            json.put("type", type);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
//                id = ((JSONObject)jsonObject.get("data")).getString("id");
                try {
                    token = ((JSONObject)jsonObject.get("data")).getString("token");
                }catch (Exception e){
                    e.printStackTrace();
                }

            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getWebid() {
        return webid;
    }

    public void setWebid(String webid) {
        this.webid = webid;
    }

    public String getWebname() {
        return webname;
    }

    public void setWebname(String webname) {
        this.webname = webname;
    }

    public String getWebpic() {
        return webpic;
    }

    public void setWebpic(String webpic) {
        this.webpic = webpic;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
