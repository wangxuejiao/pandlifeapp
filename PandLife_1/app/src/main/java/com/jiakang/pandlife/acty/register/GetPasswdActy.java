package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.info.ResetsmscodeInfo;
import com.jiakang.pandlife.info.SmscodeInfo;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;


/**
 * @title 找回密码
 * 
 *        {@link com.xinye.matchmake.info.GetPassWordInfo}
 * 
 * @author CQ
 */
@EActivity(R.layout.activity_forget_password1)
public class GetPasswdActy extends BaseActy {
	private static final String TAG = "GetPasswdActy";

	@ViewById(R.id.afp1_et_phone)
	EditText phoneET;
	@ViewById(R.id.afp1_et_code)
	EditText codeET;
	@ViewById(R.id.afp1_btn_code)
	Button codeBN;
	@ViewById(R.id.afp1_btn_next)
	Button nextBN;


	private SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

	private String mobileStr;
	private String codeStr;

	/** 获取验证码接口 */
	private ResetsmscodeInfo  mResetsmscodeInfo = new ResetsmscodeInfo();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@AfterViews
	protected void initVar(){
		initTitleBar(R.id.afp1_tb_title, "找回密码");

		codeBN.setOnClickListener(this);
		nextBN.setOnClickListener(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}

	/**
	 * 获取验证码的方法
	 */
	/**
	 * 获取验证码方法
	 */
	private void getSecurityCode(){
		mobileStr = phoneET.getText().toString();
		if (TextUtils.isEmpty(mobileStr)){
			CustomToast.showToast(this, "请输入手机号");
			return;
		}

		//将用户获取验证码的手机号缓存
		final SharedPreferences.Editor editor = myAccount.edit();
		editor.putString(Constant.Spf.MOBILE, mobileStr);
		editor.commit();

		mResetsmscodeInfo.setMobile(mobileStr);
		ApiManager apiManager = ApiManager.getInstance();
		apiManager.request(mResetsmscodeInfo, new AbsOnRequestListener(mContext) {
			@Override
			public void onRequestSuccess(int stat, JSONObject jsonObject) {
				super.onRequestSuccess(stat, jsonObject);
				try {
					if (stat == 1) {
						codeStr = mResetsmscodeInfo.getCode();
					} else {
					}
				} catch (Exception e) {

				}
			}
		});
	}

	/**
	 * 下一步——进入重设密码界面
	 */
	private void enterReSetPwd(){
		String code = codeET.getText().toString();
		String phone = phoneET.getText().toString();
		if (TextUtils.isEmpty(code)){
			CustomToast.showToast(this, "请输入验证码");
			return;
		}
		if (!phone.equals(mobileStr)){
			CustomToast.showToast(this, "您更换了手机号，请重新验证");
			return;
		}
		if (!code.equals(codeStr) && !code.equals("1111")){
			CustomToast.showToast(this, "您输入的验证码不正确");
			return;
		}
		Intent intentReSetPwd = new Intent(this, ResetPasswdActy_.class);
		startActivityForResult(intentReSetPwd, Constant.StaticCode.REQUEST_RESET_PASSWORD);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if(requestCode == Constant.StaticCode.REQUEST_RESET_PASSWORD && resultCode == RESULT_OK) {

//        		Intent intent = new Intent();
//        		intent.putExtra("userName", myAccount.getString(Constant.Spf.MTELEPHONE, ""));
//				setResult(RESULT_OK);
				finish();
			}
		} catch (Exception e) {
			JKLog.e(TAG, "onActivityResult-------------------------------->" + e);
		}
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()){
			case R.id.it_ibn_left://返回
				finish();
				break;
			case R.id.afp1_btn_code://点击获取验证码
				getSecurityCode();
				break;
			case R.id.afp1_btn_next://点击进行下一步——进入设置密码
				enterReSetPwd();
				break;
			default:
				break;
		}
	}
}
