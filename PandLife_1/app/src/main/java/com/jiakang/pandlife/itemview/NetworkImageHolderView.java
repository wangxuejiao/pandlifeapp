package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bigkoo.convenientbanner.holder.Holder;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.BannerItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

/**
 * Created by play on 2016/1/25.
 */
public class NetworkImageHolderView implements Holder<BannerItem.DataEntityBanner> {

    private ImageView imageView;

    @Override
    public View createView(Context context) {
        //你可以通过layout文件来创建，也可以像我一样用代码创建，不一定是Image，任何控件都可以进行翻页
        imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        return imageView;
    }

    @Override
    public void UpdateUI(Context context, int position, BannerItem.DataEntityBanner dataEntityBanner) {

        ImageLoaderUtil.displayImage(dataEntityBanner.getPic(), imageView,R.mipmap.ic_banner_bg);
    }
}
