package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.AddressItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 9.获取默认寄件人信息
 *
 * @author ww
 *
 */
public class GetDefaultSenderAddressInfo extends BaseAbsInfo {

    private static final String TAG = "GetDefaultSenderAddressInfo";

    /** 登录时返回的凭证	Varchar(32) */
    private String token;

//    private List<AddressItem> allItems = new ArrayList<AddressItem>();

    private AddressItem mAddressItem;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=ExpressUsers&a=Getdefaultsender" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                mAddressItem = BaseInfo.gson.fromJson(jsonObject.get("data").toString(), AddressItem.class);
                mAddressItem.setLayout(R.layout.item_default_address);
                mAddressItem.setDefaultTag(true);
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public AddressItem getAddressItem() {
        return mAddressItem;
    }

    public void setAddressItem(AddressItem addressItem) {
        mAddressItem = addressItem;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
