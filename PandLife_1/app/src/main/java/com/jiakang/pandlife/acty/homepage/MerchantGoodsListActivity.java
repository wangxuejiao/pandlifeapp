package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.CallSuper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
//import com.example.hjk.amap_android_navi.BasicNaviActivity;
import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.express.MyPackageActivity_;
import com.jiakang.pandlife.acty.express.SenderExpressActivity_;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.db.DataCacheDBHelper;
import com.jiakang.pandlife.info.nearmerchase.AllProductInfo;
import com.jiakang.pandlife.info.nearmerchase.GetCategoryInfo;
import com.jiakang.pandlife.info.nearmerchase.GetTraderInfo;
import com.jiakang.pandlife.info.nearmerchase.OneCateProductInfo;
import com.jiakang.pandlife.item.GoodsDetail;
import com.jiakang.pandlife.item.GoodsItem;
import com.jiakang.pandlife.item.GoodsKindItem;
import com.jiakang.pandlife.item.MerchantInfoItem;
import com.jiakang.pandlife.item.NearbyMerchantItem;
import com.jiakang.pandlife.item.RecommendGoodsItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.widget.PullToRefreshListView;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 商家产品列表
 */
@EActivity(R.layout.activity_merchant_goods_list)
public class MerchantGoodsListActivity extends BaseActy {
    protected static final String TAG = "MerchantGoodsListActivity";


    @ViewById(R.id.amgl_iv_return)
    ImageView returnIV;
    @ViewById(R.id.amgl_iv_merchant_icon)
    ImageView merchantIconIV;
    @ViewById(R.id.amgl_tv_merchant_name)
    TextView merchantNameTV;
    @ViewById(R.id.amgl_tv_merchant_content)
    TextView merchantContentTV;
    @ViewById(R.id.amgl_rgp_label)
    RadioGroup labelRGP;
    @ViewById(R.id.amgl_rbn_goods_list)
    RadioButton goodsListRBN;
    @ViewById(R.id.amgl_rbn_merchant_intro)
    RadioButton merchantIntroRBN;
    //商品列表模块
    @ViewById(R.id.amgl_rl_goods_list)
    RelativeLayout goodsListRL;
    @ViewById(R.id.amgl_lv_label)
    ListView labelLV;
    @ViewById(R.id.amgl_lv_goods)
    PullToRefreshListView goodsLV;
    @ViewById(R.id.amgl_tv_cart_number)
    TextView cartNumberTV;
    @ViewById(R.id.amgl_tv_count_sum)
    TextView countSumTV;
    @ViewById(R.id.amgl_btn_selected_yet)
    Button selectedYetBN;
    //商家简介模块
    @ViewById(R.id.amgl_rl_merchant_intro)
    RelativeLayout merchantIntroRL;
    @ViewById(R.id.amgl_tv_address)
    TextView addressTV;
    @ViewById(R.id.amgl_tv_site)
    TextView siteTV;
    @ViewById(R.id.amgl_iv_navigation)
    ImageView navigationIV;
    @ViewById(R.id.amgl_tv_telephone)
    TextView telephoneTV;
    @ViewById(R.id.amgl_iv_call_telephone)
    ImageView callTelephoneIV;
    @ViewById(R.id.amgl_tv_intro_content)
    TextView introTV;

    /** 获取商品种类接口 */
    private GetCategoryInfo mGetCategoryInfo = new GetCategoryInfo();
    /** 获取商品全部接口 */
    private AllProductInfo mAllProductInfo = new AllProductInfo();
    /** 获取分类商品接口 */
    private OneCateProductInfo mOneCateProductInfo = new OneCateProductInfo();
    /** 获取商家信息接口 */
    private GetTraderInfo mGetTraderInfo = new GetTraderInfo();

    private NearbyMerchantItem mNearbyMerchantItem;

    private ItemAdapter goodsItemAdapter;

    //所有商品
    private List<RecommendGoodsItem> allItems = new ArrayList<RecommendGoodsItem>();
    //推荐商品
    private List<RecommendGoodsItem> recommendAllItems = new ArrayList<RecommendGoodsItem>();
    //分类商品
    private List<RecommendGoodsItem> sortAllItems = new ArrayList<RecommendGoodsItem>();

    private ItemAdapter labelItemAdapter;
    private GoodsKindItem mGoodsKindItem;
    /**记录刷新或加载的商品种类*/
    private int goodsKindId = 0;
    /**记录选中的商品总金额*/
    private float checkCountSum = 0;
    /**记录选中的商品*/
    private int checkNum = 0;;
    /**记录配送费用*/
    private int psMoney = 0;
    //记录被选中的item
    private List<RecommendGoodsItem> selectItems = new ArrayList<RecommendGoodsItem>();

    /** 每页个数，BaseInfo.pageSize = 20 为默认值 */
    private int pageSize = 20;
    /** 第几页 */
    private int pageIndex = 1;

    private final static int GET_NEW_OK = 0x12;
    private final static int GET_MORE_OK = 0x13;
    private final static int IS_GET_NO = 0;
    private final static int IS_GET_NEW = 1;
    private final static int IS_GET_MORE = 2;
    private static int isPullRefresh = 0;
    private boolean noUpLoadFinish = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        Intent intent = getIntent();
        mNearbyMerchantItem = (NearbyMerchantItem)intent.getExtras().get("nearbyMerchantItem");


        if (mNearbyMerchantItem != null){
            merchantNameTV.setText(mNearbyMerchantItem.getNick());
            merchantContentTV.setText("营业时间" + mNearbyMerchantItem.getOpentime() + "|月售" + mNearbyMerchantItem.getCount() + "份");
            ImageLoaderUtil.displayImageCircle(mNearbyMerchantItem.getHead(), merchantIconIV);

            psMoney = mNearbyMerchantItem.getPs_money();
        }

        bindView();
        bindInfoLabel(IS_GET_NO);
        getMerchantInfo();
        notifyCartNum();
    }

    private void bindView(){
        goodsItemAdapter = new ItemAdapter(mContext);
        labelItemAdapter = new ItemAdapter(mContext);
        labelLV.setAdapter(labelItemAdapter);
        goodsLV.setAdapter(goodsItemAdapter);
        goodsLV.setShowFootView(true);

        returnIV.setOnClickListener(this);
        selectedYetBN.setOnClickListener(this);
        navigationIV.setOnClickListener(this);
        callTelephoneIV.setOnClickListener(this);
        labelRGP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.amgl_rbn_goods_list:
                        goodsListRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        merchantIntroRBN.setTextColor(getResources().getColor(R.color.gray_prompt));
                        goodsListRL.setVisibility(View.VISIBLE);
                        merchantIntroRL.setVisibility(View.GONE);
                        break;
                    case R.id.amgl_rbn_merchant_intro:
                        merchantIntroRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        goodsListRBN.setTextColor(getResources().getColor(R.color.gray_prompt));
                        merchantIntroRL.setVisibility(View.VISIBLE);
                        goodsListRL.setVisibility(View.GONE);
                        break;
                }
            }
        });

        goodsItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {
                RecommendGoodsItem recommendGoodsItem = (RecommendGoodsItem)goodsItemAdapter.getItem(position);
                String addSub = view.getTag().toString();
                if (("add").equals(addSub)){
                    recommendGoodsItem.setClickNum(recommendGoodsItem.getClickNum() + 1);
                    goodsItemAdapter.addItem(position, recommendGoodsItem);
                    goodsItemAdapter.remove(position);
                    goodsItemAdapter.notifyDataSetChanged();

                    if (selectItems.size() == 0){
                        selectItems.add(recommendGoodsItem);
                    }else{
                        boolean exist = false;//判断是否存在
                        for (int i=0; i < selectItems.size() ; i ++){
                            if ((selectItems.get(i).getId()) == recommendGoodsItem.getId()){
                                selectItems.get(i).setClickNum(recommendGoodsItem.getClickNum());
                                exist = true;
                            }
                        }
                        if (!exist){
                            selectItems.add(recommendGoodsItem);
                        }
                    }
                }else if (("sub").equals(addSub)){
                    recommendGoodsItem.setClickNum(recommendGoodsItem.getClickNum() - 1);
                    goodsItemAdapter.addItem(position, recommendGoodsItem);
                    goodsItemAdapter.remove(position);
                    goodsItemAdapter.notifyDataSetChanged();

                    if (selectItems.size() == 0){
                        selectItems.add(recommendGoodsItem);
                    }else{
                        for (int i=0; i < selectItems.size() ; i ++){
                            if ((selectItems.get(i).getId()) == recommendGoodsItem.getId()){
                                if (recommendGoodsItem.getClickNum() == 0){
                                    selectItems.remove(i);
                                }else {
                                    selectItems.get(i).setClickNum(recommendGoodsItem.getClickNum());
                                }
                            }
                        }
                    }
                }
                notifyCartNum();

//                checkCountSum = 0;
//                checkNum = 0;
//                selectItems.clear();

            }
        });

        labelLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                ListView listView = (ListView) parent;
                if (goodsKindId != position) {
                    goodsKindId = position;
                    goodsItemAdapter.clear();
                    goodsItemAdapter.notifyDataSetChanged();
                    isPullRefresh = 0;
                }
                mGoodsKindItem = (GoodsKindItem) listView.getItemAtPosition(position);
                for (int j = 0; j < labelItemAdapter.getCount(); j++) {
                    if (j == position) {
                        ((GoodsKindItem) labelItemAdapter.getItem(j)).setCheck(true);
                    } else {
                        ((GoodsKindItem) labelItemAdapter.getItem(j)).setCheck(false);
                    }
                }
                labelItemAdapter.notifyDataSetChanged();

                //获取分类信息的方法
                if (position == 0) {
//                    goodsItemAdapter.clear();
//                    goodsItemAdapter.addItems((List) allItems);
//                    goodsItemAdapter.notifyDataSetChanged();
                    bindInfoAllGoods(IS_GET_NEW);
                } else {
                    bindInfoSortGoods(IS_GET_NO);
                }
            }
        });

        //下拉刷新
        goodsLV.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {

            @Override
            public void onRefresh() {
                // 线程获取更新数据
                isPullRefresh = 1;
                //判断是分类刷新还是全部刷新
                if (goodsKindId == 0) {
                    bindInfoAllGoods(IS_GET_NEW);
                } else {
                    bindInfoSortGoods(IS_GET_NEW);
                }

            }
        });
        //上拉加载
        goodsLV.setOnMoreListener(new PullToRefreshListView.OnMoreListener() {

            @Override
            public void onMore() {
                // 线程获取更多数据
                isPullRefresh = 2;
                //判断是分类加载还是全部加载
                if (goodsKindId == 0) {
                    bindInfoAllGoods(IS_GET_MORE);
                } else {
                    bindInfoSortGoods(IS_GET_MORE);
                }

            }
        });
    }


    /**
     * 获取商品种类
     * @param isPullToRefresh
     */
    private void bindInfoLabel(int isPullToRefresh){
        if (isPullToRefresh == 2){
            pageIndex ++;
        }else{
            pageIndex = 1;
            labelItemAdapter.clear();
        }
        mGetCategoryInfo.setPageIndex(pageIndex);
        mGetCategoryInfo.setPageSize(BaseInfo.pageSize);
        mGetCategoryInfo.setSid(mNearbyMerchantItem.getId());
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mGetCategoryInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    if (mGetCategoryInfo.getAllItems().size() == 0){
                        CustomToast.showToast(mContext, "当前没有商品种类");
                        return;
                    }
                    //添加全部
                    GoodsKindItem goodsKindItem = new GoodsKindItem();
                    goodsKindItem.setCname("全部");
                    goodsKindItem.setCheck(true);
                    goodsKindItem.setLayout(R.layout.item_select_single_center);
                    labelItemAdapter.add(goodsKindItem);
                    labelItemAdapter.addItems((List) mGetCategoryInfo.getAllItems());
                    labelItemAdapter.notifyDataSetChanged();
                    bindInfoAllGoods(IS_GET_NO);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestFail(int result, JSONObject jsonObject) {
                super.onRequestFail(result, jsonObject);
                //添加全部
                GoodsKindItem goodsKindItem = new GoodsKindItem();
                goodsKindItem.setCname("全部");
                goodsKindItem.setCheck(true);
                goodsKindItem.setLayout(R.layout.item_select_single_center);
                labelItemAdapter.add(goodsKindItem);
                labelItemAdapter.addItems((List) mGetCategoryInfo.getAllItems());
                labelItemAdapter.notifyDataSetChanged();
            }
        });


//        final String[] goodsKindArr = getResources().getStringArray(R.array.goods_kind);
//        for (int i=0; i < goodsKindArr.length; i ++){
//            mGoodsKindItem = new GoodsKindItem();
//            mGoodsKindItem.setCname(goodsKindArr[i]);
//            mGoodsKindItem.setLayout(R.layout.item_select_single_center);
//            if (i == 0){
//                mGoodsKindItem.setCheck(true);
//            }
//            labelItemAdapter.add(mGoodsKindItem);
//        }
//        labelItemAdapter.notifyDataSetChanged();
//        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(mContext,R.layout.item_array_select_center, goodsKindArr);
//        labelLV.setAdapter(arrayAdapter);


//        GoodsItem goodsItem;
//        for (int i = 0; i < 5; i++) {
//            goodsItem = new GoodsItem();
//            goodsItem.setName("全部");
//            goodsItem.setPrice(19);
//            goodsItem.setLayout(R.layout.item_goods_list);
//            allItems.add(goodsItem);
//        }
//        goodsItemAdapter.addItems((List) allItems);
//        goodsItemAdapter.notifyDataSetChanged();
    }

    /**
     * 获取所有商品信息方法
     */
    private void bindInfoAllGoods(int isPullToRefresh){
        if (isPullToRefresh == 2){
            pageIndex ++;
        }else{
            pageIndex = 1;
            allItems.clear();
        }
        mAllProductInfo.setPageIndex(pageIndex);
        mAllProductInfo.setPageSize(BaseInfo.pageSize);
        mAllProductInfo.setSid(mNearbyMerchantItem.getId());
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mAllProductInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {


                    if (mAllProductInfo.getRecommendAllItems().size() != 0){
//                        allItems.clear();
//                        goodsItemAdapter.clear();
                        allItems.addAll(mAllProductInfo.getRecommendAllItems());
                    }
                    if (mAllProductInfo.getAllItems().size() != 0){
                        allItems.addAll(mAllProductInfo.getAllItems());
                    }
                    //遍历集合，判断是否有加入购物车的商品
                    if(selectItems.size() != 0){
                        for (int i=0;i < selectItems.size();i ++){
                            for (int j=0; j < allItems.size(); j ++){
                                if (selectItems.get(i).getId() == allItems.get(j).getId()){
                                    allItems.get(j).setClickNum(selectItems.get(i).getClickNum());
                                }
                            }
                        }
                    }

                    goodsItemAdapter.clear();
                    goodsItemAdapter.addItems((List) allItems);
                    goodsItemAdapter.notifyDataSetChanged();
                    goodsLV.onRefreshComplete();
                    if (mAllProductInfo.getAllItems().size() < BaseInfo.pageSize){
                        goodsLV.onMoreComplete(false);
                    }else{
                        goodsLV.onMoreComplete(true);
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestFail(int result, JSONObject jsonObject) {
                super.onRequestFail(result, jsonObject);
                if (isPullRefresh == 2){
                    goodsItemAdapter.clear();
                    goodsItemAdapter.notifyDataSetChanged();
                }
                goodsLV.onRefreshComplete();
                goodsLV.onMoreComplete(false);
            }
        });
    }

    /**
     * 获取分类商品信息方法
     */
    private void bindInfoSortGoods(int isPullToRefresh){
        if (isPullToRefresh == 2){
            pageIndex ++;
        }else{
            pageIndex = 1;
            sortAllItems.clear();
        }
        mOneCateProductInfo.setPageIndex(pageIndex);
        mOneCateProductInfo.setPageSize(BaseInfo.pageSize);
        mOneCateProductInfo.setSid(mNearbyMerchantItem.getId());
        mOneCateProductInfo.setCid(mGoodsKindItem.getId());
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mOneCateProductInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    if (mOneCateProductInfo.getAllItems().size() == 0) {
                        CustomToast.showToast(mContext, "当前没有" + mGoodsKindItem.getCname() + "类商品");
                        return;
                    }
                    if (isPullRefresh == 2) {
                        sortAllItems.addAll(mOneCateProductInfo.getAllItems());
                    } else {
                        sortAllItems = mOneCateProductInfo.getAllItems();
                    }

                    //遍历集合，判断是否有加入购物车的商品
                    if(selectItems.size() != 0){
                        for (int i=0;i < selectItems.size();i ++){
                            for (int j=0; j < sortAllItems.size(); j ++){
                                if (selectItems.get(i).getId() == sortAllItems.get(j).getId()){
                                    sortAllItems.get(j).setClickNum(selectItems.get(i).getClickNum());
                                }
                            }
                        }
                    }
                    goodsItemAdapter.clear();
                    goodsItemAdapter.addItems((List) sortAllItems);
                    goodsItemAdapter.notifyDataSetChanged();

                    goodsLV.onRefreshComplete();
                    if (mOneCateProductInfo.getAllItems().size() < BaseInfo.pageSize) {
                        goodsLV.onMoreComplete(false);
                    } else {
                        goodsLV.onMoreComplete(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestFail(int result, JSONObject jsonObject) {
                super.onRequestFail(result, jsonObject);
                if (isPullRefresh != 2) {
                    goodsItemAdapter.clear();
                    goodsItemAdapter.notifyDataSetChanged();
                }
                goodsLV.onRefreshComplete();
                goodsLV.onMoreComplete(false);
            }
        });
    }

    /**
     * 获取商家信息
     */
    private void getMerchantInfo(){
        mGetTraderInfo.setSid(mNearbyMerchantItem.getId());
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mGetTraderInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try{
                    MerchantInfoItem merchantInfoItem = mGetTraderInfo.getMerchantInfoItem();
//                    merchantNameTV.setText(merchantInfoItem.getNick());
//                    merchantContentTV.setText("营业时间" + merchantInfoItem.getOpentime() + "|月售" + merchantInfoItem.getCount() + "份");
//                    ImageLoaderUtil.displayImageCircle(merchantInfoItem.getHead(), merchantIconIV);

                    addressTV.setText(merchantInfoItem.getAddress());
                    siteTV.setText(merchantInfoItem.getStation_name());
                    telephoneTV.setText(merchantInfoItem.getTelnumber());
                    introTV.setText(merchantInfoItem.getDescription());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     *  选好了进入购物车方法
     */
    private void selectedYetGoods(){
        Intent intentShopCart = new Intent(mContext, MerchantIndentActivity_.class);
        intentShopCart.putExtra("list", (Serializable) selectItems);
        intentShopCart.putExtra("station_id", mNearbyMerchantItem.getId());
        intentShopCart.putExtra("pa_money", psMoney);
        startActivityForResult(intentShopCart, Constant.StaticCode.REQUEST_MERCHANT_INDENT);
    }

    /**
     * 更新购物车中商品数目且判断按钮状态
     */
    private void notifyCartNum(){
        checkCountSum = 0;
        checkNum = 0;
        RecommendGoodsItem recommendGoodsItem;
        for (int i=0; i < selectItems.size(); i ++){
            recommendGoodsItem = selectItems.get(i);
            checkNum = checkNum + recommendGoodsItem.getClickNum();
            checkCountSum = checkCountSum + recommendGoodsItem.getClickNum() * recommendGoodsItem.getTprice();
        }
        if(checkNum > 0){
            countSumTV.setText("￥" + checkCountSum);
            cartNumberTV.setText(checkNum+"");
            cartNumberTV.setVisibility(View.VISIBLE);
            selectedYetBN.setTextColor(getResources().getColor(R.color.white));
            selectedYetBN.setEnabled(true);
            selectedYetBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_yellow_bg));
        }else{
            countSumTV.setText("￥0.0");
            cartNumberTV.setVisibility(View.GONE);
            selectedYetBN.setTextColor(getResources().getColor(R.color.gray_content));
            selectedYetBN.setEnabled(false);
            selectedYetBN.setBackground(getResources().getDrawable(R.drawable.shape_btn_border_bg));
        }
    }

    /**
     * 拨打电话方法
     */
    private void clickCallPhone(String telephoneStr){
        Intent intentCall = new Intent();
        intentCall.setAction(Intent.ACTION_CALL);
        intentCall.setData(Uri.parse("tel:" + telephoneStr));
        startActivity(intentCall);
    }

    /**
     * 进入导航的方法
     */
    private void toNavigation(double locationLongitude, double locationLatitude){
//        Intent intentNavigation = new Intent(this, BasicNaviActivity.class);
//
//        intentNavigation.putExtra(BasicNaviActivity.END_LONGITUDE, "Double.parseDouble(mSiteItem.getLongitude())");
//        intentNavigation.putExtra(BasicNaviActivity.END_LATITUDE, "Double.parseDouble(mSiteItem.getLatitude())");
//        intentNavigation.putExtra(BasicNaviActivity.START_LONGITUDE, locationLongitude);
//        intentNavigation.putExtra(BasicNaviActivity.START_LATITUDE, locationLatitude);
//        startActivityForResult(intentNavigation, Constant.StaticCode.REQUEST_SENDER_ADDRESS);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        //定位回调
        JKLog.i(TAG, aMapLocation.toString());
        double locationLongitude = aMapLocation.getLongitude();
        double locationLatitude = aMapLocation.getLatitude();
        toNavigation(locationLongitude, locationLatitude);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.amgl_iv_return:
                finish();
                break;
            case R.id.amgl_btn_selected_yet://点击选好了进入购物车
                selectedYetGoods();
                break;
            case R.id.amgl_iv_call_telephone:
                String telephone = telephoneTV.getText().toString();
                clickCallPhone(telephone);
                break;
            case R.id.amgl_iv_navigation:
                startLocation();
                break;
            default:
                break;
        }
    }
}
