package com.jiakang.pandlife.utils;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

/**
 * 
 * @author qiu
 * 
 */
public class MemoryCache {

	private static MemoryCache instance;
	private static final String TAG = "ImageCache";
	// private final int HARD_CACHE_CAPACITY = memClass*3/10; //1张全副图3.5M //相册
	// +拍照头像+预览
	// 可以指定缓存大小为总内存的1/n
	private final int HARD_CACHE_CAPACITY = 5 * 1024 * 1024; // 1张全副图3.5M //相册
																// +拍照头像+预览
	private LruCache<String, Bitmap> sHardBitmapCache; // galaxy note
														// 1024*768*4/1024*1024/裁剪比例

	/**
	 * private final HashMap<String, Bitmap> sHardBitmapCache = new
	 * LinkedHashMap<String, Bitmap>(HARD_CACHE_CAPACITY, 0.75f, true) {
	 * 
	 * @Override protected boolean removeEldestEntry(LinkedHashMap.Entry<String,
	 *           Bitmap> eldest) { if (size() > HARD_CACHE_CAPACITY) { //
	 *           Entries push-out of hard reference cache are transferred to
	 *           soft reference cache Log.e("hard full",
	 *           "remove to soft!!!!!!!!!!!!!!!!!!!!!!!!");
	 *           sSoftBitmapCache.put(eldest.getKey(), new
	 *           SoftReference<Bitmap>(eldest.getValue())); return true; } else
	 *           return false; } }; 小图 极 限情况+一张被溢出的大图 galaxy note
	 *           1024*768*4/1024*1024/裁剪比例 90*101*4*50/1024*1024 = 1.8M
	 *           1.8+600*900*4/1024/1024=2M
	 * 
	 *           WeakReference是弱引用，其中保存的对象实例可以被GC回收掉。
	 *           SoftReference是强引用，它保存的对象实例，除非JVM即将OutOfMemory，否则不会被GC回收。
	 * 
	 * 
	 *           SoftReference比WeakReference生命力更强，当JVM的内存不吃紧时，即使引用的对象被置为空了，
	 *           Soft还可以保留对该对象的引用，此时的JVM内存池实际上还保有原来对象，
	 *           只有当内存吃紧的情况下JVM才会清除Soft的引用对象，并且会在未来重新加载该引用的对象。
	 * 
	 *           而WeakReference则当清理内存池时会自动清理掉引用的对象。
	 */
	// private static final int SOFT_CACHE_CAPACITY = 50; //
	// Soft cache for bitmaps kicked out of hard cache
	// private final static ConcurrentHashMap<String, SoftReference<Bitmap>>
	// sSoftBitmapCache = new ConcurrentHashMap<String, SoftReference<Bitmap>>(
	// SOFT_CACHE_CAPACITY);

	public static MemoryCache getInstance() {
		if (null == instance)
			instance = new MemoryCache();
		return instance;
	}

	private MemoryCache() {
		// 获取到可用内存的最大值，使用内存超出这个值会引起OutOfMemory异常.
		// LruCache通过构造函数传入缓存值，以KB为单位。
		int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		// 使用最大可用内存值的1/8作为缓存的大小。
		int cacheSize = maxMemory / 8;

		sHardBitmapCache = new LruCache<String, Bitmap>(HARD_CACHE_CAPACITY) {

			@Override
			public int sizeOf(String key, Bitmap bitmap) {
				// 重写此方法来衡量每张图片的大小，默认返回图片数量
				// return bitmap.getByteCount() / 1024;
				return bitmap.getRowBytes() * bitmap.getHeight();
			}

			// cache full call remove/put key --->call entryRemove
			@Override
			protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
				// 硬引用缓存区满，将一个最不经常使用的oldvalue推入到软引用缓存区
				JKLog.e(TAG, "sHardBitmapCache  已经被回收");
				// sSoftBitmapCache.put(key, new
				// SoftReference<Bitmap>(oldValue));
				sHardBitmapCache.remove(key);
			}

		};
	}

	/**
	 * 添加缓存 sHardBitmapCache sSoftBitmapCache
	 * 
	 * @param bitmap
	 */
	public void addBitmapToCache(String url, Bitmap bitmap) {
		if (bitmap != null) {
			synchronized (sHardBitmapCache) {
				if (null != sHardBitmapCache.get(url)) {
					return;
				}
				sHardBitmapCache.put(url, bitmap);
			}
			//
			/*
			 * synchronized (sSoftBitmapCache) {
			 * if(null!=sSoftBitmapCache.get(url)){ XYLog.e(TAG,
			 * "save sSoftBitmapCache"); return; } sSoftBitmapCache.put(url, new
			 * SoftReference<Bitmap>(bitmap)); }
			 */
		}
	}

	/**
	 * 从缓存提取bitmap
	 * 
	 * @param url
	 * @return
	 */
	public Bitmap getBitmapFromCache(String url) {
		// 先hardCache
		synchronized (sHardBitmapCache) {
			final Bitmap bitmap = sHardBitmapCache.get(url);
			if (bitmap != null) {
				// // LRU has
				// sHardBitmapCache.remove(url);
				// sHardBitmapCache.put(url, bitmap);
				// XYLog.d(TAG, "hard cache get !!!!!!!!!");
				return bitmap;
			} else {// 如果key为url，图片为null，说明图片被GC回收了，此处清空key键
				sHardBitmapCache.remove(url);
			}
		}
		// synchronized (sSoftBitmapCache) {
		// // softCache
		// SoftReference<Bitmap> bitmapReference = sSoftBitmapCache.get(url);
		// if (bitmapReference != null) {
		// final Bitmap bitmap = bitmapReference.get();
		// if (bitmap != null) {
		// XYLog.e(TAG, "soft cache get !!!!!!!!!");
		// return bitmap;
		// } else {
		// // Soft reference has been Garbage Collected
		// sSoftBitmapCache.remove(url);
		// }
		// } else {// 如果key为url，图片为null，说明图片被GC回收了，此处清空key键
		// sSoftBitmapCache.remove(url);
		// }
		// }
		// file:
		return null;
	}

	/**
	 * 清除缓存.
	 */
	public void clearCache() {
		sHardBitmapCache.evictAll();
		// sSoftBitmapCache.clear();
	}

}
