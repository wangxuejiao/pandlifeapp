package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.ToolRentItem;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

import java.util.List;

/**
 * Created by play on 2016/1/5.
 */
public class ToolRentItemView extends AbsLinearLayout {

    private TextView tvName;
    private TextView tvContent;
    private TextView tvDeposit;
    private ImageView imageView;

    private String TAG = "toolRentItemView";

    public ToolRentItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void findViewsByIds() {

        tvName = (TextView) findViewById(R.id.tv_title_tool_rent);
        tvContent = (TextView) findViewById(R.id.tv_content_tool_rent);
        tvDeposit = (TextView) findViewById(R.id.tv_deposit_tool_rent);
        imageView = (ImageView) findViewById(R.id.iv_rent_tool);
    }

    @Override
    public void setObject(Item item, final int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        ToolRentItem.DataEntity dataEntity = (ToolRentItem.DataEntity) item;
        tvName.setText(dataEntity.getTitle());
        tvContent.setText(dataEntity.getAdd_title());
        tvDeposit.setText("押金: ￥" + dataEntity.getDeposit() + "元");
        ImageLoaderUtil.displayImage(dataEntity.getPic(),imageView);

    }
}
