package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.LoginActivity;
import com.jiakang.pandlife.acty.MainActivity_;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.info.LoginInfo;
import com.jiakang.pandlife.info.MyInfo;
import com.jiakang.pandlife.info.OtherLoginBandInfo;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

/**
 * 老用户登录并绑定
 */
@EActivity(R.layout.activity_band_login)
public class BandLoginActivity extends BaseActy {
    protected static final String TAG = "BandLoginActivity";

    @ViewById(R.id.abl_et_account)
    AutoCompleteTextView userNameET;
    @ViewById(R.id.abl_et_password)
    EditText passwordET;
    @ViewById(R.id.abl_btn_land)
    Button landBN;

    /** 登录接口 */
    private LoginInfo loginInfo = new LoginInfo();
    /** 第三方绑定接口 */
    private OtherLoginBandInfo mOtherLoginBandInfo = new OtherLoginBandInfo();
    /** 获取我的信息接口 */
    private MyInfo mMyInfo = new MyInfo();
    private UserItem mUserItem;

    private SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.abl_tb_title, "老用户绑定");

        bindView();
    }

    private void bindView(){
        landBN.setOnClickListener(this);

        // 如果用户名改变，清空密码
        userNameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordET.setText(null);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /**
     * 登录方法
     */
    private void login() {
        String userName = userNameET.getText().toString();
        String password = passwordET.getText().toString();
        if (TextUtils.isEmpty(userName)) {
            CustomToast.showToast(mContext, "用户名不能为空");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            CustomToast.showToast(mContext, "密码不能为空");
            return;
        }
        //将用户登录密码缓存
        final SharedPreferences.Editor editor = myAccount.edit();
        editor.putString(Constant.Spf.USERNAME, userName);
        editor.putString(Constant.Spf.PASSWORD, password);
        editor.commit();

        final ApiManager apiManager = ApiManager.getInstance();
        loginInfo.setmUserName(userName);
        loginInfo.setmPassword(password);
        apiManager.request(loginInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
//                        CustomToast.showToast(mContext, "登录成功");
                        //当登录成功后进行绑定
                        bandLogin();

                    } else {
                        JKLog.i(TAG, "登录失败");
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    /**
     * 登录成后进行绑定
     */
    private void bandLogin(){
        mOtherLoginBandInfo.setWebid(LoginActivity.webid);
        mOtherLoginBandInfo.setWebname(LoginActivity.webname);
        mOtherLoginBandInfo.setWebpic(LoginActivity.webpic);
        mOtherLoginBandInfo.setType(LoginActivity.type);
        ApiManager apiManagerBand = ApiManager.getInstance();
        apiManagerBand.request(mOtherLoginBandInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    CustomToast.showToast(mContext, "绑定成功");
                    //绑定成功后获取用户信息，判断是否有绑定小区和站点
                    getUserInfo();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 获取用户信息方法
     */
    private void getUserInfo(){
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mMyInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    mUserItem = mMyInfo.getUserItem();
                    int siteId = 0;
                    siteId = mUserItem.getStation_id();
                    //如果站点id不为空，那么跳转主页，为空就进入绑定小区和站点页面
                    if (siteId != 0) {
                        Intent intent = new Intent(mContext, MainActivity_.class);
                        startActivity(intent);
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        Intent intentSelectLocationOrManual = new Intent(mContext, SelectLocationOrManualActivity_.class);
                        intentSelectLocationOrManual.putExtra(SelectLocationOrManualActivity.SITE_OR_COMMUNITY, "community_and_site");
                        startActivityForResult(intentSelectLocationOrManual, Constant.StaticCode.REQUSET_BIND_SITE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUSET_BIND_SITE) && (resultCode == RESULT_OK)){
            Intent intent = new Intent(mContext, MainActivity_.class);
            startActivity(intent);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
            case R.id.abl_btn_land:
                login();
                break;
        }
    }
}
