//package com.example.cxl.cxlframe.utils;
//
//import java.util.Properties;
//
//
//import android.os.Message;
//import android.util.Log;
//
///**
// * @author：xuwangwang E-mail: 541765907@qq.com
// * @date：2014年6月9日 下午8:01:41
// * @version 1.0 String Tag = "EmailUtils中：";
// */
//public class EmailUtils {
//	/**
//	 * 邮件发送程序
//	 *
//	 * @param to
//	 *            接受人
//	 * @param subject
//	 *            邮件主题
//	 * @param content
//	 *            邮件内容
//	 * @throws Exception
//	 * @throws MessagingException
//	 */
//	public static void sendEmail(String to, String subject, String content) throws Exception, MessagingException {
//		String host = "smtp.qq.com";
//		String address = "435320085@qq.com";
//		String from = "435320085@qq.com";
//		String password = "19880";
//		if ("".equals(to) || to == null) {
//			to = "541765907@qq.com";
//		}
//		String port = "25";
//		sendEmail(host, address, from, password, to, port, subject, content);
//	}
//
//	/**
//	 *
//	 * http://hi.baidu.com/vowyu/item/6239787df49c9e3a6f29f6fd
//	 *
//	 * <pre>
//	 * 网易@163.com邮箱：　   pop.163.com; 　　           smtp.163.com
//	 * 网易@yeah.net邮箱：    pop.yeah.net; 　　          smtp.yeah.net
//	 * 网易@netease.com邮箱： pop.netease.com;            smtp.netease.com
//	 * 网易@126.com邮箱：     pop.126.com       　        smtp.126.com
//	 * 搜狐@sohu.com：        pop3.sohu.com；             smtp.sohu.com
//	 * 信网@eyou.com：        pop3.eyou.com；             mx.eyou.com
//	 * 新飞网@tom.com：　     pop.tom.com；               smtp.tom.com
//	 * 亿唐@etang.com：　     pop.free.etang.com          smtp.free.etang.com
//	 * 21世纪@21cn.com： 　   pop.21cn.com； 　　　　　   smtp.21cn.com
//	 * </pre>
//	 *
//	 * @param from
//	 * @param password
//	 * @param to
//	 * @param subject
//	 * @param content
//	 * @throws Exception
//	 * @return void
//	 */
//	public static void sendEmail(String from, String password, String to, String subject, String content) throws Exception {
//		// String host = "smtp.qq.com";
//		String host = "smtp." + from.substring(from.indexOf("@") + 1);
//		String address = from;
//		String port = "25";
//		JKLog.e("", "sendEmail方法中-------->：from为：" + from);
//		JKLog.e("", "sendEmail方法中-------->：password为：" + password);
//		JKLog.e("", "sendEmail方法中-------->：to为：" + to);
//		JKLog.e("", "sendEmail方法中-------->：subject为：" + subject);
//		JKLog.e("", "sendEmail方法中-------->：content为：" + content);
//		sendEmail(host, address, from, password, to, port, subject, content);
//	}
//
//	/**
//	 * 邮件发送程序
//	 *
//	 * @param host
//	 *            邮件服务器 如：smtp.qq.com
//	 * @param address
//	 *            发送邮件的地址 如：545099227@qq.com
//	 * @param from
//	 *            来自： wsx2miao@qq.com
//	 * @param password
//	 *            您的邮箱密码
//	 * @param to
//	 *            接收人
//	 * @param port
//	 *            端口（QQ:25）
//	 * @param subject
//	 *            邮件主题
//	 * @param content
//	 *            邮件内容
//	 * @throws Exception
//	 */
//	public static void sendEmail(String host, String address, String from, String password, String to, String port, String subject, String content) throws Exception {
//		Multipart multiPart;
//		String finalString = "";
//
//		Properties props = System.getProperties();
//		props.put("mail.smtp.host", host);
//		props.put("mail.smtp.starttls.enable", "true");
//		props.put("mail.smtp.user", address);
//		props.put("mail.smtp.password", password);
//		props.put("mail.smtp.port", port);
//		props.put("mail.smtp.auth", "true");
//		Log.i("Check", "done pops");
//		Session session = Session.getDefaultInstance(props, null);
//		DataHandler handler = new DataHandler(new ByteArrayDataSource(finalString.getBytes(), "text/plain"));
//		MimeMessage message = new MimeMessage(session);
//		message.setFrom(new InternetAddress(from));
//		message.setDataHandler(handler);
//		Log.i("Check", "done sessions");
//
//		multiPart = new MimeMultipart();
//		InternetAddress toAddress;
//		toAddress = new InternetAddress(to);
//		message.addRecipient(Message.RecipientType.TO, toAddress);
//		Log.i("Check", "added recipient");
//		message.setSubject(subject);
//		message.setContent(multiPart);
//		message.setText(content);
//
//		Log.i("check", "transport");
//		Transport transport = session.getTransport("smtp");
//		Log.i("check", "connecting");
//		transport.connect(host, address, password);
//		Log.i("check", "wana send");
//		transport.sendMessage(message, message.getAllRecipients());
//		transport.close();
//		Log.i("check", "sent");
//	}
//}