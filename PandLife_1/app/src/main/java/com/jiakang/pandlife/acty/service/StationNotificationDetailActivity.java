package com.jiakang.pandlife.acty.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.StationNotificationDetailInfo;
import com.jiakang.pandlife.item.StationNotificationDetail;
import com.jiakang.pandlife.utils.DateUtils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

@EActivity(R.layout.activity_station_notification_detail)
public class StationNotificationDetailActivity extends BaseActy {

    @ViewById(R.id.tv_title_station_notification_detail)
    TextView tvTitle;
    @ViewById(R.id.tv_date_station_notification_detail)
    TextView tvDate;
    @ViewById(R.id.tv_content_station_notification_detail)
    TextView tvContent;
    @ViewById(R.id.view_station_notification_detail)
    View viewline;

    private StationNotificationDetailInfo mStationNotificationDetailInfo = new StationNotificationDetailInfo();
    private StationNotificationDetail mStationNotificationDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
    }

    @AfterViews
    protected void afterCreateView(){

        initTitleBar(R.id.al_tb_title, "站点通知详情", null, null);
    }

    private void initData() {

        int id = getIntent().getIntExtra("id",-1);
        mStationNotificationDetailInfo.setId(id);

        stationNotificationDetailRequest();

    }

    /**
     * 请求到数据后给界面填充数据
     */
    private void setViewData(){

        tvTitle.setText(mStationNotificationDetail.getData().getTitle());
        Long tempDate = Long.parseLong(mStationNotificationDetail.getData().getCreatetime()) * 1000 ;
        tvDate.setText(DateUtils.getDateStringyyyyMMddHH(tempDate));
        tvContent.setText(mStationNotificationDetail.getData().getContent());
        viewline.setVisibility(View.VISIBLE);

    }

    /**
     * 站点详情请求
     */
    private void stationNotificationDetailRequest() {

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mStationNotificationDetailInfo,new AbsOnRequestListener(mContext){

            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mStationNotificationDetail = mStationNotificationDetailInfo.getStationNotificationDetail();
                setViewData();
            }
        });

    }


    /**
     * 启动站点详情界面
     * @param context
     * @param id 详情的id
     */
    public static void startStationNotificationDetail(Context context,int id){

        Intent intent = new Intent(context,StationNotificationDetailActivity_.class);
        intent.putExtra("id",id);
        context.startActivity(intent);

    }

}
