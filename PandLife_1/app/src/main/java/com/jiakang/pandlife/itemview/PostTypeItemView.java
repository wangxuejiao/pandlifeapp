package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.PostTypeItem;
import com.jiakang.pandlife.item.Item;

/**
 * Created by play on 2016/1/18.
 */
public class PostTypeItemView extends AbsLinearLayout {

    private TextView textView;
    private ImageView ivArrowSelect;

    public PostTypeItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PostTypeItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        textView = (TextView) findViewById(R.id.tv_name_post_type);
        ivArrowSelect = (ImageView) findViewById(R.id.iv_arrow_select);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        PostTypeItem.DataEntityPostType dataEntityPostType = (PostTypeItem.DataEntityPostType) item;
        if(dataEntityPostType.isChecked()){
            ivArrowSelect.setVisibility(VISIBLE);
        }else {
            ivArrowSelect.setVisibility(INVISIBLE);
        }
        textView.setText(dataEntityPostType.getName());

    }
}
