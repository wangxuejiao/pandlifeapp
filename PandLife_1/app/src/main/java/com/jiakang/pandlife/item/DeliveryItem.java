package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

import java.io.Serializable;

/**
 * 我的地址实体
 * Created by Administrator on 2016/1/11.
 */
public class DeliveryItem extends Item implements Serializable {

    /** 快递类型名称	Varchar(32) */
    private String name;
    /** 快递类型id	Int(10) */
    private int taobao_code;
    /**标记是否选中过*/
    private String tag;

    private int layout = R.layout.item_select_single_left;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTaobao_code() {
        return taobao_code;
    }

    public void setTaobao_code(int taobao_code) {
        this.taobao_code = taobao_code;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    @Override
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
