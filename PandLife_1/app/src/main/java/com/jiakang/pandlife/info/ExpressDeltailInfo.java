package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.ExpressDetailItem;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 16.寄件详情信息
 *
 * @author ww
 *
 */
public class ExpressDeltailInfo extends BaseAbsInfo {

    private static final String TAG = "ExpressDeltailInfo";

    /** 寄件记录id	Int(10) */
    private int id;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;

    private ExpressDetailItem mExpressDetailItem;

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Express&a=Expressdeltail" + "&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("id", id);
//            json.put("token", token);
            json.put("token", BaseInfo.token);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");

                mExpressDetailItem = BaseInfo.gson.fromJson(jsonObject.get("data").toString(), ExpressDetailItem.class);
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ExpressDetailItem getmExpressDetailItem() {
        return mExpressDetailItem;
    }

    public void setmExpressDetailItem(ExpressDetailItem mExpressDetailItem) {
        this.mExpressDetailItem = mExpressDetailItem;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
