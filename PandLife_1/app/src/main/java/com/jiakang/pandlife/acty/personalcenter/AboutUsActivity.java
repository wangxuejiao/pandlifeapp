package com.jiakang.pandlife.acty.personalcenter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 关于我们
 */
@EActivity(R.layout.activity_about_us)
public class AboutUsActivity extends BaseActy {
    protected static final String TAG = "AboutUsActivity";

    @ViewById(R.id.aau_tv_about_us)
    TextView aboutUsTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.aau_tb_title, "关于我们");

//        aboutUsTV.setText("");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
