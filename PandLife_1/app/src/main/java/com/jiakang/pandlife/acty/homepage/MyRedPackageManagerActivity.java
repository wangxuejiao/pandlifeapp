package com.jiakang.pandlife.acty.homepage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.UsedRedPackInfo;
import com.jiakang.pandlife.item.RedPackageItem;
import com.jiakang.pandlife.utils.CustomToast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的红包管理器
 */
@EActivity(R.layout.activity_my_red_package_manager)
public class MyRedPackageManagerActivity extends BaseActy {
    protected static final String TAG = "MyRedPackageManagerActivity";

    @ViewById(R.id.amrpm_lv_red_package)
    ListView redPackageLV;

    private ItemAdapter mItemAdapter;

    //商品id
    private int gid;
    //商品类型：团购为t
    private String type;
    //订单金额
    private int money;

    /** 订单可用红包接口 */
    private UsedRedPackInfo mUsedRedPackInfo = new UsedRedPackInfo();

    private List<RedPackageItem> allItems = new ArrayList<RedPackageItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.amrpm_tb_title, "我的红包");

        bindView();
        bindInfo();
    }

    private void bindView(){
        mItemAdapter = new ItemAdapter(mContext);
        redPackageLV.setAdapter(mItemAdapter);

        final Intent intent = getIntent();
        gid = intent.getExtras().getInt("gid");
        money = intent.getExtras().getInt("money");
        type = intent.getExtras().getString("type");

        redPackageLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListView listView = (ListView)parent;
                RedPackageItem redPackageItem = (RedPackageItem)listView.getItemAtPosition(position);
                Intent intentReturn = new Intent();
                intentReturn.putExtra("redPackageItem", redPackageItem);
                setResult(RESULT_OK, intentReturn);
                finish();
            }
        });

    }

    private void bindInfo(){
        allItems.clear();
        mItemAdapter.clear();
//        BaseInfo.token = "ff0a6c74e7b4d08b659f9e2737a4ac5c";
//        mUsedRedPackInfo.setToken("ff0a6c74e7b4d08b659f9e2737a4ac5c");
        mUsedRedPackInfo.setGid(gid);
        mUsedRedPackInfo.setMoney(money);
        mUsedRedPackInfo.setType(type);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mUsedRedPackInfo, new AbsOnRequestListener(mContext){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try{
                    allItems = mUsedRedPackInfo.getAllItems();
                    if (allItems.size() == 0){
                        CustomToast.showToast(mContext, "没有可用红包");
                        return;
                    }
                    mItemAdapter.addItems((List)allItems);
                    mItemAdapter.notifyDataSetChanged();


                }catch (Exception e){

                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
