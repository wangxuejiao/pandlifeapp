package com.jiakang.pandlife.widget;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jiakang.pandlife.R;


/**
 * 图片浏览指示标
 * @author sunday
 *
 */
public class MarkView extends LinearLayout {
	private ImageView[] mImageView;
	private Context context;

	public MarkView(Context context){
		super(context);
		this.context = context;
	}

	public MarkView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	public void setMarkCount(int iCount) {
		mImageView = new ImageView[iCount];
		LayoutParams p = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		for (int i = 0; i < iCount; i++) {
			ImageView image = new ImageView(context);
			image.setImageResource(R.mipmap.unselected_dot);
			image.setLayoutParams(p);
			mImageView[i] = image;
			image.setId(i);
			addView(image);
			
		}
	}

	public void setMark(int position) {
		for (int i = 0; i < mImageView.length; i++) {
			if (i == position) {
				mImageView[i].setImageResource(R.mipmap.selected_dot);
			} else {
				mImageView[i].setImageResource(R.mipmap.unselected_dot);
			}
		}
	}

}
