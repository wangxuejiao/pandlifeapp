package com.jiakang.pandlife.form;

import java.io.File;

/**
 * Created by moon.zhong on 2015/3/2.
 */
public class FormFile {

    private String mName ;

    private String mFileName ;

    private String mValue ;

    private String mMime ;

    private File mFile;

    public FormFile(File file) {
        this.mFile = file;
    }


    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmValue() {
        return mValue;
    }

    public void setmValue(String mValue) {
        this.mValue = mValue;
    }

    public String getmMime() {
        return mMime;
    }

    public void setmMime(String mMime) {
        this.mMime = mMime;
    }

    public File getmFile() {
        return mFile;
    }

    public void setmFile(File mFile) {
        this.mFile = mFile;
    }

    public String getmFileName() {
        return mFileName;
    }

    public void setmFileName(String mFileName) {
        this.mFileName = mFileName;
    }
}
