package com.jiakang.pandlife.item;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.annotation.sqlite.Transient;
import com.jiakang.pandlife.db.DataCacheDBHelper;

/**
 * Created by play on 2016/1/8.
 */
public class ReplayItem extends Item {
    /**
     * replyuser : {"nick":null,"uid":"13","id":"22","floor":"2"}
     * pic : null
     * content : 回复匿名用户：发送
     * issupport : 0
     * isshow : 1
     * nick : null
     * head : null
     * uid : 13
     * replyid : 35
     * dialog_id : 324
     * id : 35
     * time : 13:57
     * floor : 9
     * supportnum : 0
     * flag: 1 表示帖子详情的回复，2 表示小区拼好货的回复,默认是帖子列表的回复（自己添加的）
     */
    private ReplyuserEntity replyuser;
    private String pic;
    private String content;
    private int issupport;
    private int support;
    private String isshow;
    private String nick;
    private String head;
    private String uid;
    private String replyid;
    private String dialog_id;
    private String id;
    private String time;
    private String floor;
    private String supportnum;
    private int flag = 1;

    private int layout = R.layout.item_replay;

    public void setLayout(int layout) {
        this.layout = layout;
    }

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public void setReplyuser(ReplyuserEntity replyuser) {
        this.replyuser = replyuser;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setIssupport(int issupport) {
        this.issupport = issupport;
    }

    public void setIsshow(String isshow) {
        this.isshow = isshow;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setReplyid(String replyid) {
        this.replyid = replyid;
    }

    public void setDialog_id(String dialog_id) {
        this.dialog_id = dialog_id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public void setSupportnum(String supportnum) {
        this.supportnum = supportnum;
    }

    public ReplyuserEntity getReplyuser() {
        return replyuser;
    }

    public String getPic() {
        return pic;
    }

    public String getContent() {
        return content;
    }

    public int getIssupport() {
        return issupport;
    }

    public int getSupport() {
        return support;
    }

    public void setSupport(int support) {
        this.support = support;
    }

    public String getIsshow() {
        return isshow;
    }

    public String getNick() {
        return nick;
    }

    public String getHead() {
        return head;
    }

    public String getUid() {
        return uid;
    }

    public String getReplyid() {
        return replyid;
    }

    public String getDialog_id() {
        return dialog_id;
    }

    public String getId() {
        return id;
    }

    public String getTime() {
        return time;
    }

    public String getFloor() {
        return floor;
    }

    public String getSupportnum() {
        return supportnum;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getLayout() {
        return layout;
    }

    public class ReplyuserEntity {
        /**
         * nick : null
         * uid : 13
         * id : 22
         * floor : 2
         */
        private String nick;
        private String uid;
        private String id;
        private String floor;

        public void setNick(String nick) {
            this.nick = nick;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setFloor(String floor) {
            this.floor = floor;
        }

        public String getNick() {
            return nick;
        }

        public String getUid() {
            return uid;
        }

        public String getId() {
            return id;
        }

        public String getFloor() {
            return floor;
        }
    }

    @Override
    public int insert() {
        PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
        return 1;
    }
}
