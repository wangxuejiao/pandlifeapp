package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

/**
 * Created by play on 2016/1/18.
 */
public class NeighborhoodItem extends Item {


    /**
     * uid : 13
     * img : https://img.alicdn.com/bao/uploaded/i2/2137121108/TB2c5i1hXXXXXaaXpXXXXXXXXXX_!!2137121108.jpg_80x80.jpg
     * des : 星期天天23213123气不错，大家一起去郊游
     * id : 305
     * time : 1452936223
     * title : 小区活动23
     * type : 1
     * userinfo : {"nick":null,"head":null,"mobile":"18248792600","mid":"13"}
     */
    private String uid;
    private String img;
    private String des;
    private String id;
    private String time;
    private String title;
    private String type;
    private UserinfoEntity userinfo;

    private int layout = R.layout.item_neighbourhood;

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUserinfo(UserinfoEntity userinfo) {
        this.userinfo = userinfo;
    }

    public String getUid() {
        return uid;
    }

    public String getImg() {
        return img;
    }

    public String getDes() {
        return des;
    }

    public String getId() {
        return id;
    }

    public String getTime() {
        return time;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public UserinfoEntity getUserinfo() {
        return userinfo;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public class UserinfoEntity {
        /**
         * nick : null
         * head : null
         * mobile : 18248792600
         * mid : 13
         */
        private String nick;
        private String head;
        private String mobile;
        private String mid;

        public void setNick(String nick) {
            this.nick = nick;
        }

        public void setHead(String head) {
            this.head = head;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public void setMid(String mid) {
            this.mid = mid;
        }

        public String getNick() {
            return nick;
        }

        public String getHead() {
            return head;
        }

        public String getMobile() {
            return mobile;
        }

        public String getMid() {
            return mid;
        }
    }
}
