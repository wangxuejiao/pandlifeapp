package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import android.content.SharedPreferences;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.config.ConfigSharedPreferences;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.MD5Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 1.1注册接口
 *
 * @author ww
 *
 */
public class RegisterInfo extends BaseAbsInfo {

    private static final String TAG = "RegisterInfo";
    private String mobile = null;
    private String password = null;
    /** 第三方id 可不传 */
    private String webid = null;
    /** 第三方昵称 可不传 */
    private String webname = null;
    /** 第三方头像 可不传 */
    private String webpic = null;
    /** 第三方类型 可不传 */
    private String type = null;
    /** 1表示andriod,2表示iphone */
    protected String loginType = "0";

//    private UserItem userItem;

    private SharedPreferences myAccount = ConfigSharedPreferences.getInstance().getConfigSharedPreferences(ConfigSharedPreferences.NAME_ACCOUNT);

    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Auth&a=Register" + "&subtime=" + System.currentTimeMillis();
//        return "http://192.168.1.2/proj/api/user/login";
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("mobile", mobile);
            json.put("password", MD5Util.encodeByMD5(password));
            json.put("webid", webid);
            json.put("webname", webname);
            json.put("webpic", webpic);
            json.put("type", type);
//            json.put("system", loginType);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            data = (JSONObject)jsonObject.get("data");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                BaseInfo.token = data.getString("token");

                //将用户登录密码缓存
                final SharedPreferences.Editor editor = myAccount.edit();
                editor.putString(Constant.Spf.TOKEN, BaseInfo.token);
                editor.commit();
//                userItem = BaseInfo.gson.fromJson(jsonObject.toString(), UserItem.class);
                // 保存用户实体
//                MyJobApp.getInstance().setMemberItem(memberItem);
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getWebid() {
        return webid;
    }

    public void setWebid(String webid) {
        this.webid = webid;
    }

    public String getWebname() {
        return webname;
    }

    public void setWebname(String webname) {
        this.webname = webname;
    }

    public String getWebpic() {
        return webpic;
    }

    public void setWebpic(String webpic) {
        this.webpic = webpic;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
//    public UserItem getUserItem() {
//        return userItem;
//    }
//
//    public void setUserItem(UserItem userItem) {
//        this.userItem = userItem;
//    }
}
