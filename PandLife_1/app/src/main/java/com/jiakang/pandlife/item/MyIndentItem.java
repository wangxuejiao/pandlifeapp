package com.jiakang.pandlife.item;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.annotation.sqlite.Id;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.db.DataCacheDBHelper;

/**
 * 我的订单实体
 * Created by Administrator on 2016/1/11.
 */
@Table(name = DataCacheDBHelper.TAB_MYINDENT_CACHE)
public class MyIndentItem extends Item{

    /** 数据库id	Int(10) */
    @Id
    private int _id;
    /** 编号id	Int(10) */
    private int id;
    /** 订单号	Varchar(32) */
    private String orid;
    /** 用户id */
    private int mid;
    /** 商品id	Int(10) */
    private int gid;
    /** 团购收件人地址id	Int(10) */
    private int aid;
    /** 站点id	Int(10) */
    private int station_id;
    /** 数量	Int(10) */
    private int number;
    /** 支付类型	 */
    private String pay_kind;
    /** 单价	Int(10) */
    private int danjia;
    /** 总价格	Int(10) */
    private int total_money;
    /** 支付金额	Int(10) */
    private int pay_money;
    /** 配送费	Int(10) */
    private int ps_money;
    /** 是否使用红包是：y  否：n	Varchar */
    private String is_redpackets;
    /** 红包金额	Int(10) */
    private int red_packets;
    /** 备注信息	Varchar */
    private String remarks;
    /** 订单创建时间	Varchar(32) */
    private String createtime;
    /** 订单状态：1未付款，2已付款，3已配送，4已完成	Varchar */
    private String order_type;
    /** 支付时间	Date */
    private String paytime;
    /** 配送时间	Varchar(32) */
    private String pstime;
    /** 收货时间	Varchar(32) */
    private String shtime;
    /** 未付款 商品图片地址	Varchar(255) */
    private String goodpic;
    /** 商品标题	Varchar(255) */
    private String goodtitle;
    /** 收货人姓名	Varchar(32) */
    private String shname;
    /** 收货人手机号	Varchar(11) */
    private String shphone;
    /** 收货地址	Varchar(255) */
    private String shaddress;
    /** 产品图片	Varchar(255) */
    private String gpic;
    /** 已完成 产品图片	Varchar(255) */
    private String pic;
    /** 产品名称	Varchar(255) */
    private String gtitle;
    /** 站点经度 Varchar(32) */
    private String longitude;
    /** 站点纬度 Varchar(32) */
    private String latitude;
    /** 站点名称	Varchar(32) */
    private String stationname;
    /** 站点电话 */
    private String stationtel;
    /** 订单状态：未付款，已付款，已配送，已完成	Varchar(32) */
    private String type;
    /** 退款时间	Int（10） */
    private int ttime;
    /** 退款金额	Float（10,2） */
    private float Money;
    /** 退款备注	text */
    private String other;
    /** 商家回复	text */
    private String reother;

    /** 额外字段，判断点击了什么 */
    private String clickStr;

    /** 插入数据库时的时间 */
    private long currentTime = System.currentTimeMillis();
    /** 保存的时间 */
    private long saveTime = 600000;

    @Override
    public int insert() {
        PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
        return 1;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public long getSaveTime() {
        return saveTime;
    }

    public void setSaveTime(long saveTime) {
        this.saveTime = saveTime;
    }

    private int layout = R.layout.item_my_indent;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrid() {
        return orid;
    }

    public void setOrid(String orid) {
        this.orid = orid;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getGid() {
        return gid;
    }

    public void setGid(int gid) {
        this.gid = gid;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPay_kind() {
        return pay_kind;
    }

    public void setPay_kind(String pay_kind) {
        this.pay_kind = pay_kind;
    }

    public int getDanjia() {
        return danjia;
    }

    public void setDanjia(int danjia) {
        this.danjia = danjia;
    }

    public int getTotal_money() {
        return total_money;
    }

    public void setTotal_money(int total_money) {
        this.total_money = total_money;
    }

    public int getPay_money() {
        return pay_money;
    }

    public void setPay_money(int pay_money) {
        this.pay_money = pay_money;
    }

    public int getPs_money() {
        return ps_money;
    }

    public void setPs_money(int ps_money) {
        this.ps_money = ps_money;
    }

    public String getIs_redpackets() {
        return is_redpackets;
    }

    public void setIs_redpackets(String is_redpackets) {
        this.is_redpackets = is_redpackets;
    }

    public int getRed_packets() {
        return red_packets;
    }

    public void setRed_packets(int red_packets) {
        this.red_packets = red_packets;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getPaytime() {
        return paytime;
    }

    public void setPaytime(String paytime) {
        this.paytime = paytime;
    }

    public String getPstime() {
        return pstime;
    }

    public void setPstime(String pstime) {
        this.pstime = pstime;
    }

    public String getShtime() {
        return shtime;
    }

    public void setShtime(String shtime) {
        this.shtime = shtime;
    }

    public String getGoodpic() {
        return goodpic;
    }

    public void setGoodpic(String goodpic) {
        this.goodpic = goodpic;
    }

    public String getGoodtitle() {
        return goodtitle;
    }

    public void setGoodtitle(String goodtitle) {
        this.goodtitle = goodtitle;
    }

    public String getShname() {
        return shname;
    }

    public void setShname(String shname) {
        this.shname = shname;
    }

    public String getShphone() {
        return shphone;
    }

    public void setShphone(String shphone) {
        this.shphone = shphone;
    }

    public String getShaddress() {
        return shaddress;
    }

    public void setShaddress(String shaddress) {
        this.shaddress = shaddress;
    }

    public String getGpic() {
        return gpic;
    }

    public void setGpic(String gpic) {
        this.gpic = gpic;
    }

    public String getGtitle() {
        return gtitle;
    }

    public void setGtitle(String gtitle) {
        this.gtitle = gtitle;
    }

    public String getClickStr() {
        return clickStr;
    }

    public void setClickStr(String clickStr) {
        this.clickStr = clickStr;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getStationname() {
        return stationname;
    }

    public void setStationname(String stationname) {
        this.stationname = stationname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getStationtel() {
        return stationtel;
    }

    public void setStationtel(String stationtel) {
        this.stationtel = stationtel;
    }

    public int getTtime() {
        return ttime;
    }

    public void setTtime(int ttime) {
        this.ttime = ttime;
    }

    public float getMoney() {
        return Money;
    }

    public void setMoney(float money) {
        Money = money;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getReother() {
        return reother;
    }

    public void setReother(String reother) {
        this.reother = reother;
    }
}
