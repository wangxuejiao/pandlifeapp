package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.PublicKuaiDiItem;
import com.jiakang.pandlife.item.SendBagItem;
import com.jiakang.pandlife.utils.Util;

/**
 * Created by play on 2016/1/6.
 */
public class PublicKuaiDiItemView extends AbsRelativeLayout {

    private TextView contentTV;
    private TextView dateTV;
    private ImageView circleIV;
    private ImageView circleGreenIV;
    private View line1V;
    private View line2V;

    public PublicKuaiDiItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PublicKuaiDiItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();
        contentTV = (TextView) findViewById(R.id.ipk_tv_content);
        dateTV = (TextView) findViewById(R.id.ipk_tv_date);
        circleIV = (ImageView) findViewById(R.id.ipk_iv_circle);
        circleGreenIV = (ImageView) findViewById(R.id.ipk_iv_circle_green);
        line1V = (View) findViewById(R.id.ipk_view_line1);
        line2V = (View) findViewById(R.id.ipk_view_line2);
    }

    @Override
    public void setObject(final Item item, final int position, final ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        if(item instanceof PublicKuaiDiItem) {
            PublicKuaiDiItem publicKuaiDiItem = (PublicKuaiDiItem) item;
            String ordinal = publicKuaiDiItem.getOrdinal();
            if (("first").equals(ordinal)){
//                circleIV.setImageDrawable(getResources().getDrawable(R.mipmap.icon_iv_logistics_dot_green));
                contentTV.setTextColor(getResources().getColor(R.color.green));
                dateTV.setTextColor(getResources().getColor(R.color.green));
                line1V.setVisibility(View.INVISIBLE);
                line2V.setVisibility(View.VISIBLE);
                circleGreenIV.setVisibility(View.VISIBLE);
            }else if (("last").equals(ordinal)){
//                circleIV.setImageDrawable(getResources().getDrawable(R.mipmap.icon_iv_logistics_dot_gray));
                contentTV.setTextColor(getResources().getColor(R.color.gray_content));
                dateTV.setTextColor(getResources().getColor(R.color.gray_content));
                line1V.setVisibility(View.VISIBLE);
                line2V.setVisibility(View.INVISIBLE);
                circleGreenIV.setVisibility(View.GONE);
            }else{
//                circleIV.setImageDrawable(getResources().getDrawable(R.mipmap.icon_iv_logistics_dot_gray));
                contentTV.setTextColor(getResources().getColor(R.color.gray_content));
                dateTV.setTextColor(getResources().getColor(R.color.gray_content));
                line1V.setVisibility(View.VISIBLE);
                line2V.setVisibility(View.VISIBLE);
                circleGreenIV.setVisibility(View.GONE);
            }

            contentTV.setText(publicKuaiDiItem.getContext());
            dateTV.setText(publicKuaiDiItem.getTime());
        }
    }
}
