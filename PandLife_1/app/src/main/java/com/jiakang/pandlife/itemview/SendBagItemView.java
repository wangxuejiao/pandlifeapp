package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.personalcenter.MySendExpressActivity;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.AddressItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.SendBagItem;
import com.jiakang.pandlife.utils.Util;

/**
 * Created by play on 2016/1/6.
 */
public class SendBagItemView extends AbsRelativeLayout {

    private TextView statusTV;
    private TextView navigationTV;
    private TextView nameTV;
    private TextView phoneTV;
    private TextView addressTV;

    public SendBagItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SendBagItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();
        statusTV = (TextView) findViewById(R.id.isb_tv_status);
        navigationTV = (TextView) findViewById(R.id.isb_tv_navigation);
        nameTV = (TextView) findViewById(R.id.isb_tv_name);
        phoneTV = (TextView) findViewById(R.id.isb_tv_phone);
        addressTV = (TextView) findViewById(R.id.isb_tv_address);
    }

    @Override
    public void setObject(final Item item, final int position, final ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        if(item instanceof SendBagItem){
            SendBagItem sendBagItem = (SendBagItem) item;
            final String is_enable = sendBagItem.getIs_enable();
            final String bag_type = sendBagItem.getBag_type();
            if (("n").equals(is_enable)){
                statusTV.setText("已取消");
                statusTV.setTextColor(getResources().getColor(R.color.gray_content));

                navigationTV.setText("查看物流");
                navigationTV.setTextColor(getResources().getColor(R.color.gray_content));
                navigationTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                navigationTV.setCompoundDrawablePadding((int) Util.dip2px(getContext(), 5.0f));
            }else {
                if (("y").equals(bag_type)){
                    statusTV.setText("已投递");
                    statusTV.setTextColor(getResources().getColor(R.color.gray_content));

                    navigationTV.setText("查看物流");
                    navigationTV.setTextColor(getResources().getColor(R.color.gray_content));
                    navigationTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    navigationTV.setCompoundDrawablePadding((int) Util.dip2px(getContext(), 5.0f));
                }else{
                    statusTV.setText("等待投递");
                    statusTV.setTextColor(getResources().getColor(R.color.gray_dark));

                    navigationTV.setText("站点导航");
                    navigationTV.setTextColor(getResources().getColor(R.color.reddish_orange));
                    navigationTV.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.icon_iv_site_navigation, 0, 0, 0);
                    navigationTV.setCompoundDrawablePadding((int) Util.dip2px(getContext(), 5.0f));
                }
            }
            nameTV.setText(sendBagItem.getSname());
            phoneTV.setText(sendBagItem.getPhone());
            addressTV.setText(sendBagItem.getAddress());

            navigationTV.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setTag(item);
                    onViewClickListener.onViewClick(v, position);

//                    if (("y").equals(is_enable) && ("n").equals(bag_type)){
//                        Intent intent = new Intent(PandLifeApp.getInstance().getBaseContext(), MySendExpressActivity.class);
//                    }
                }
            });

        }
//        else if(item instanceof SenderAddressItem){
//            SenderAddressItem senderAddressItem = (SenderAddressItem) item;
//            nameTV.setText(senderAddressItem.getName());
//            phoneTV.setText(senderAddressItem.getPhone());
//            addressTV.setText(senderAddressItem.getAddress());
//        }
    }
}
