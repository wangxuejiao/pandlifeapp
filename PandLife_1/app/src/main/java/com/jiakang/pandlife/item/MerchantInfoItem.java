package com.jiakang.pandlife.item;

/**
 * 商家信息实体
 * Created by Administrator on 2016/1/29.
 */
public class MerchantInfoItem extends Item{

    /** 商家头像 Varchar(255) */
    private String head;
    /** 商家昵称 Varchar(255) */
    private String nick;
    /** 商家电话 Varchar(255) */
    private String telnumber;
    /** 站点id Int(10) */
    private int station_id;
    /** 商家详细地址 Varchar(255) */
    private String address;
    /** 商家简介 Varchar */
    private String description;
    /** 商家营业时间 */
    private String opentime;
    /** 经度 */
    private String longitude;
    /** 纬度 */
    private String latitude;
    /** 站点名称 Varchar */
    private String station_name;

    @Override
    public int getItemLayoutId() {
        return 0;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getTelnumber() {
        return telnumber;
    }

    public void setTelnumber(String telnumber) {
        this.telnumber = telnumber;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOpentime() {
        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getStation_name() {
        return station_name;
    }

    public void setStation_name(String station_name) {
        this.station_name = station_name;
    }
}
