package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.PostDelete;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by play on 2016/1/22.
 */
public class PostDeleteInfo extends BaseAbsInfo {

    private int id;
    private PostDelete postDelete;

    public PostDelete getPostDelete() {
        return postDelete;
    }

    public void setPostDelete(PostDelete postDelete) {
        this.postDelete = postDelete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Forum&a=Forumdel&subtime=" + System
                .currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("id",id);
            json.put("token",BaseInfo.token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        postDelete = BaseInfo.gson.fromJson(jsonObject.toString(),PostDelete.class);
    }
}
