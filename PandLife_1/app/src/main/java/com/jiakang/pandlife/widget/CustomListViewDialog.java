package com.jiakang.pandlife.widget;

import android.app.Dialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.utils.Util;

import java.util.Arrays;
import java.util.List;


/**
 * 自定义Dialog
 * 
 * @author
 */
public class CustomListViewDialog extends Dialog implements OnClickListener, OnItemClickListener {

	private ListView listView;
	private Button closeBtn;
	private TextView titleTV;
	// private List<String> listStr;
	private CustomDialogAdapter customDialogAdapter;

	public CustomListViewDialog(Context context, List<String> list, OnDlgItemOnClickListener onDlgItemOnClickListener) {
		this(context, "请选择", R.style.MyDialog, list, onDlgItemOnClickListener);
	}

	public CustomListViewDialog(Context context, String title, List list, OnDlgItemOnClickListener onDlgItemOnClickListener) {
		this(context, title, R.style.MyDialog, list, onDlgItemOnClickListener);
	}

	public CustomListViewDialog(Context context, String title, String[] array, OnDlgItemOnClickListener onDlgItemOnClickListener) {
		this(context, title, R.style.MyDialog, Arrays.asList(array), onDlgItemOnClickListener);
	}

	public CustomListViewDialog(Context context, String title, int style, List list, OnDlgItemOnClickListener onDlgItemOnClickListener) {
		super(context, style);

		View view = getLayoutInflater().inflate(R.layout.dlg_custom_listview, null);
		// 透明背景
		// Drawable myDrawable =
		// context.getResources().getDrawable(R.drawable.dialog_root_bg);
		// myDrawable.setAlpha(150);
		// view.setBackgroundDrawable(myDrawable);
		setContentView(view);

		titleTV = (TextView) view.findViewById(R.id.dcl_tv_title);
		listView = (ListView) view.findViewById(R.id.dcl_lv_select);
		closeBtn = (Button) view.findViewById(R.id.dcl_btn_close);

		DisplayMetrics display = context.getResources().getDisplayMetrics();
		if (list.size() > 6) {
			LayoutParams params = listView.getLayoutParams();
			params.height = display.heightPixels / 2;
			listView.setLayoutParams(params);
		}

		Window window = getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		params.width = (int) (280 * display.density);
		params.gravity = Gravity.CENTER;
		window.setAttributes(params);

		titleTV.setText(title);
		customDialogAdapter = new CustomDialogAdapter(context, list);
		listView.setAdapter(customDialogAdapter);
		listView.setOnItemClickListener(this);
		closeBtn.setOnClickListener(this);
		this.onDlgItemOnClickListener = onDlgItemOnClickListener;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		if (onDlgItemOnClickListener != null) {
			this.dismiss();
			onDlgItemOnClickListener.onDlgItemOnClick(position, customDialogAdapter.getItemString(position));
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dcl_btn_close:
			CustomListViewDialog.this.dismiss();
			break;
		default:
			break;
		}
	}

	private OnDlgItemOnClickListener onDlgItemOnClickListener;

	public interface OnDlgItemOnClickListener {
		void onDlgItemOnClick(int position, String itemStr);
	}

	public CustomDialogAdapter getCustomDialogAdapter() {
		return customDialogAdapter;
	}

	public void setCustomDialogAdapter(CustomDialogAdapter customDialogAdapter) {
		this.customDialogAdapter = customDialogAdapter;
	}

	public void setTitle(String title) {
		titleTV.setText(title);
	}

	/** 适配器 */
	public class CustomDialogAdapter extends BaseAdapter {
		private List listStr;
		private Context context;

		public CustomDialogAdapter(Context context, List strs) {
			this.context = context;
			this.listStr = strs;
		}

		public List<String> getListStr() {
			return listStr;
		}

		public void setListStr(List<String> listStr) {
			this.listStr = listStr;
		}

		@Override
		public int getCount() {
			return listStr.size();
		}

		@Override
		public Object getItem(int position) {
			return listStr.get(position);
		}

		public String getItemString(int position) {
			Object object = listStr.get(position);
			if (object instanceof String) {
				return (String) object;
			} else if (object instanceof Item) {
				return object.toString();
			}
			return object.toString();
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView textView;
			if (convertView != null) {
				textView = (TextView) convertView;
			} else {
				textView = new TextView(context);
				textView.setBackgroundResource(R.drawable.xml_lv_dlg_itembg);// public_lbg_rounded_middle
				// float size =
				// context.getResources().getDimension(R.dimen.letter_font_text);
				textView.setTextSize(16);//
				textView.setSingleLine(true);
				//textView.setTextColor(context.getResources().getColorStateList(R.drawable.xml_color_dialog));// R.drawable.xml_color_dialog
				textView.setGravity(Gravity.CENTER_VERTICAL);
				int padding = (int) Util.dip2px(context, 10);
				textView.setPadding(padding, padding, padding, padding);
			}
			textView.setText(getItemString(position));
			return textView;
		}

	}

}