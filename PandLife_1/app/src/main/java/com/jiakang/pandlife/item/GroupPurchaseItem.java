package com.jiakang.pandlife.item;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.annotation.sqlite.Id;
import com.jiakang.pandlife.annotation.sqlite.Table;
import com.jiakang.pandlife.db.DataCacheDBHelper;
import com.jiakang.pandlife.db.DataDBHelper;

/**
 * 团购列表实体
 * Created by Administrator on 2016/1/12.
 */
@Table(name = DataCacheDBHelper.TAB_GROUPPURCHASE_CACHE)
public class GroupPurchaseItem extends Item{

    /** 数据库id	Int(10) */
    @Id
    private int _id;
    /** 产品id	Int(10) */
    private int id;
    /** 产品名称	Varchar(255) */
    private String title;
    /** 图片	Varchar(255) */
    private String pic;
    /** 总库存数量	Int(10) */
    private int total;
    /** 售价	Int(10) */
    private float price;
    /** 团购价格	Int(10) */
    private float tprice;
    /** 团购开始时间	Varchar(32) */
    private String starttime;
    /** 结束时间	Varchar(32) */
    private String endtime;

    /** 插入数据库时的时间 */
    private long currentTime = System.currentTimeMillis();
    /** 保存的时间 */
    private long saveTime = 600000;

    private int layout = R.layout.item_group_purchase;
    @Override
    public int getItemLayoutId() {
        return layout;
    }

    @Override
    public int insert() {
        PandLifeApp.getInstance().getCacheDataDBManage().insert(this);
        return 1;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getTprice() {
        return tprice;
    }

    public void setTprice(float tprice) {
        this.tprice = tprice;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }
}
