package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.RedPackageItem;
import com.jiakang.pandlife.utils.Util;

/**
 * Created by play on 2016/1/6.
 */
public class MyRedPackageItemView extends AbsRelativeLayout {

    private TextView sumTV;
//    private TextView sumNoteTV;
    private TextView redPackageTypeTV;
    private TextView periodValidityTV;
//    private TextView redPackageConditionTV;
//    private ImageView fastPastDueIV;

    public MyRedPackageItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyRedPackageItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        sumTV = (TextView) findViewById(R.id.imrp_tv_sum);
        redPackageTypeTV = (TextView) findViewById(R.id.imrp_tv_red_paceage_type);
        periodValidityTV = (TextView) findViewById(R.id.imrp_tv_period_validity);
//        sumNoteTV = (TextView) findViewById(R.id.imrp_tv_sum_note);
//        redPackageConditionTV = (TextView) findViewById(R.id.imrp_tv_redPackage_condition);
//        fastPastDueIV = (ImageView)findViewById(R.id.imrp_iv_fast_past_due);
    }

    @Override
    public void setObject(Item item, int position, ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);

        RedPackageItem redPackageItem = (RedPackageItem) item;
        sumTV.setText("￥" + redPackageItem.getMoney());
        redPackageTypeTV.setText(redPackageItem.getTitle());
        long etime = Long.parseLong(redPackageItem.getEtime()) * 1000;
        periodValidityTV.setText("有效期至" + Util.getFormatedDateTime("yyyy-MM-dd", etime));
    }
}
