package com.jiakang.pandlife.acty.service;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.StationNotificationInfo;
import com.jiakang.pandlife.item.StationNotificationItem;
import com.jiakang.pandlife.widget.PullToRefreshListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@EActivity(R.layout.activity_station_notification)
public class StationNotificationActivity extends BaseActy {

    @ViewById(R.id.lv_station_notification)
    PullToRefreshListView listView;
    ItemAdapter mItemAdapter;

    private StationNotificationInfo mStationNotificationInfo = new StationNotificationInfo();
    private List<StationNotificationItem.DataEntity> mDataEntityList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
    }

    @AfterViews
    protected void onCreateViewAfter() {

        initTitleBar(R.id.al_tb_title, "站点通知", null, null);

        listView.setAdapter(mItemAdapter);
        listView.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int stationNotificationId = Integer.parseInt(mDataEntityList.get(position - 1).getId());
                StationNotificationDetailActivity.startStationNotificationDetail(mContext, stationNotificationId);
            }
        });

        listView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            @Override
            public void onRefresh() {
                stationNotificationRequest();
            }
        });

    }

    public void initData() {

        mItemAdapter = new ItemAdapter(this);
        stationNotificationRequest();
    }

    private void stationNotificationRequest() {
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mStationNotificationInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);

                mDataEntityList = mStationNotificationInfo.getDataEntityStationNotificationList();
                mItemAdapter.clear();
                mItemAdapter.addItems((List) mDataEntityList);
                mItemAdapter.notifyDataSetChanged();
                listView.onRefreshComplete();
            }
        });
    }

}
