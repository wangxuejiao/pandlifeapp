package com.jiakang.pandlife.acty.express;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jiakang.pandlife.R;

import org.androidannotations.annotations.EActivity;

/**
 * 选择站点
 */
@EActivity(R.layout.activity_selected_site)
public class SelectedSiteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
