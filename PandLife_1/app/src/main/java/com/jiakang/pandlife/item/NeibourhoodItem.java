package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

import java.util.List;

/**
 * Created by play on 2016/1/18.
 */
public class NeibourhoodItem{


    /**
     * data : [{"uid":"13","img":"https://img.alicdn.com/bao/uploaded/i2/2137121108/TB2c5i1hXXXXXaaXpXXXXXXXXXX_!!2137121108.jpg_80x80.jpg","des":"星期天天23213123气不错，大家一起去郊游","id":"305","time":"1452936223","title":"小区活动23","type":"1","userinfo":{"nick":null,"head":null,"mobile":"18248792600","mid":"13"}},{"uid":"13","img":"https://img.alicdn.com/bao/uploaded/i2/2137121108/TB2c5i1hXXXXXaaXpXXXXXXXXXX_!!2137121108.jpg_80x80.jpg","des":"星期天天气不错，大家一起去郊游","id":"304","time":"1452936220","title":"小区活动23","type":"1","userinfo":{"nick":null,"head":null,"mobile":"18248792600","mid":"13"}}]
     * page : {"pageIndex":1,"totalPage":2}
     * status : 1
     * info : 帖子列表
     */
    private List<DataEntityNeibourhood> data;
    private PageEntity page;
    private int status;
    private String info;

    public void setData(List<DataEntityNeibourhood> data) {
        this.data = data;
    }

    public void setPage(PageEntity page) {
        this.page = page;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<DataEntityNeibourhood> getData() {
        return data;
    }

    public PageEntity getPage() {
        return page;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }

    public class DataEntityNeibourhood extends Item {
        /**
         * uid : 13   //发帖用户id
         * img : https://img.alicdn.com/bao/uploaded/i2/2137121108/TB2c5i1hXXXXXaaXpXXXXXXXXXX_!!2137121108.jpg_80x80.jpg
         * des : 星期天天23213123气不错，大家一起去郊游
         * id : 305     //帖子id
         * time : 1452936223
         * title : 小区活动23
         * type : 1   //帖子类型
         * userinfo : {"nick":null,"head":null,"mobile":"18248792600","mid":"13"}
         * supportnum：赞的数量
         * replynum：回复数量
         */
        private String uid;
        private String img;
        private String des;
        private String id;
        private String time;
        private String title;
        private String type;
        private UserinfoEntity userinfo;
        private String supportnum;
        private String replynum;

        private int layout = R.layout.item_neighbourhood;

        public void setUid(String uid) {
            this.uid = uid;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public void setDes(String des) {
            this.des = des;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setUserinfo(UserinfoEntity userinfo) {
            this.userinfo = userinfo;
        }

        public String getUid() {
            return uid;
        }

        public String getImg() {
            return img;
        }

        public String getDes() {
            return des;
        }

        public String getId() {
            return id;
        }

        public String getTime() {
            return time;
        }

        public String getTitle() {
            return title;
        }

        public String getType() {
            return type;
        }

        public String getSupportnum() {
            return supportnum;
        }

        public void setSupportnum(String supportnum) {
            this.supportnum = supportnum;
        }

        public String getReplynum() {
            return replynum;
        }

        public void setReplynum(String replynum) {
            this.replynum = replynum;
        }

        public UserinfoEntity getUserinfo() {
            return userinfo;
        }

        public void setLayout(int layout) {
            this.layout = layout;
        }

        @Override
        public int getItemLayoutId() {
            return layout;
        }


        public class UserinfoEntity {
            /**
             * nick : null
             * head : null
             * mobile : 18248792600
             * mid : 13
             */
            private String nick;
            private String head;
            private String mobile;
            private String mid;

            public void setNick(String nick) {
                this.nick = nick;
            }

            public void setHead(String head) {
                this.head = head;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public void setMid(String mid) {
                this.mid = mid;
            }

            public String getNick() {
                return nick;
            }

            public String getHead() {
                return head;
            }

            public String getMobile() {
                return mobile;
            }

            public String getMid() {
                return mid;
            }
        }
    }

    public class PageEntity {
        /**
         * pageIndex : 1
         * totalPage : 2
         */
        private int pageIndex;
        private int totalPage;

        public void setPageIndex(int pageIndex) {
            this.pageIndex = pageIndex;
        }

        public void setTotalPage(int totalPage) {
            this.totalPage = totalPage;
        }

        public int getPageIndex() {
            return pageIndex;
        }

        public int getTotalPage() {
            return totalPage;
        }
    }
}
