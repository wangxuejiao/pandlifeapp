package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.RentTool;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by play on 2016/1/12.
 * 租借工具类
 */
public class RentTooInfo extends BaseAbsInfo {

    private int lid;
    private int backTime;
    private int count;

    private RentTool mRentTool;


    public int getLid() {
        return lid;
    }

    public void setLid(int lid) {
        this.lid = lid;
    }

    public int getBackTime() {
        return backTime;
    }

    public void setBackTime(int backTime) {
        this.backTime = backTime;
    }

    public RentTool getRentTool() {
        return mRentTool;
    }

    public void setRentTool(RentTool rentTool) {
        mRentTool = rentTool;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Server&a=Addleaserecord&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("token",BaseInfo.token);
            jsonObject.put("lid",lid);
            jsonObject.put("back_time",backTime);
            jsonObject.put("nums",count);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        mRentTool = BaseInfo.gson.fromJson(jsonObject.toString(),RentTool.class);
    }
}
