package com.jiakang.pandlife.item;

/**
 * Created by play on 2016/1/13.
 */
public class StationNotificationDetail {


    /**
     * data : {"createtime":"1452480789","station_id":"-1","id":"1","title":"春节放假通知","content":"关于春节放假通知的测试信息，大家可以添加测试信息看看相关效果"}
     * status : 1
     * info : 通知详细信息
     */
    private DataEntity data;
    private int status;
    private String info;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public DataEntity getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }

    public class DataEntity {
        /**
         * createtime : 1452480789
         * station_id : -1
         * id : 1
         * title : 春节放假通知
         * content : 关于春节放假通知的测试信息，大家可以添加测试信息看看相关效果
         */
        private String createtime;
        private String station_id;
        private String id;
        private String title;
        private String content;

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public void setStation_id(String station_id) {
            this.station_id = station_id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCreatetime() {
            return createtime;
        }

        public String getStation_id() {
            return station_id;
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getContent() {
            return content;
        }
    }
}
