package com.jiakang.pandlife.acty.personalcenter;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.amap.api.location.AMapLocation;
//import com.example.hjk.amap_android_navi.BasicNaviActivity;
import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.acty.express.ExpressDetailActivity;
import com.jiakang.pandlife.acty.express.ScanCodeDeliveryActivity_;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.db.DataCacheDBHelper;
import com.jiakang.pandlife.db.DataCacheDBManage;
import com.jiakang.pandlife.info.AlexpresslistInfo;
import com.jiakang.pandlife.info.ExpresslistInfo;
import com.jiakang.pandlife.info.SendBagListInfo;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.item.MyIndentItem;
import com.jiakang.pandlife.item.SendBagItem;
import com.jiakang.pandlife.utils.JKLog;
import com.jiakang.pandlife.widget.PullToRefreshListView;
import com.jiakang.pandlife.widget.TitleBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * 我的寄件
 * Created by Administrator on 2016/1/14.
 */
@EActivity(R.layout.activity_my_send_express)
public class MySendExpressActivity extends BaseActy{
    protected static final String TAG = "MySendExpressActivity";

//    @ViewById(R.id.amp_tb_title)
//    TitleBar mTitleBar;
    @ViewById(R.id.amse_rgp_tab)
    RadioGroup tabRGP;
    @ViewById(R.id.amse_rbn_tableOne)
    RadioButton tableOneRBN;
    @ViewById(R.id.amse_rbn_tableTwo)
    RadioButton tableTwoRBN;
    @ViewById(R.id.amse_lv_express_unfinish)
    PullToRefreshListView unfinishExpressLV;
    @ViewById(R.id.amse_lv_express_finish)
    PullToRefreshListView finishExpressLV;
    @ViewById(R.id.amse_rl_no_data)
    RelativeLayout noDataRL;

    private ItemAdapter unFinishExpressListItemAdapter;
    private ItemAdapter finishExpressListItemAdapter;

    /** 每页个数，BaseInfo.pageSize = 20 为默认值 */
    private int pageSize = 20;
    /** 第几页 */
    private int pageIndexUnget = 1;
    private int pageIndexGet = 1;

    private final static int GET_NEW_OK = 0x12;
    private final static int GET_MORE_OK = 0x13;
    private final static int IS_GET_NO = 0;
    private final static int IS_GET_NEW = 1;
    private final static int IS_GET_MORE = 2;
    private static int isPullRefresh = 0;
    private boolean noUpLoadFinishFinish = true;
    private boolean noUpLoadFinishUnfinish = true;

    /** 我的寄件列表接口 */
    private SendBagListInfo mSendBagListInfo = new SendBagListInfo();

    //未完成集合
    private List<SendBagItem> unfinishAllItems = new ArrayList<SendBagItem>();
    //已完成集合
    private List<SendBagItem> finishAllItems = new ArrayList<SendBagItem>();

    private SendBagItem mSendBagItem;

    private DataCacheDBManage dataCacheDBManage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.amse_tb_title, "我的寄件");
        dataCacheDBManage = getMyApp().getCacheDataDBManage();
        initView();
        getCacheData();
        initUnFinish();
    }

    private void initView(){
        unFinishExpressListItemAdapter = new ItemAdapter(mContext);
        finishExpressListItemAdapter = new ItemAdapter(mContext);
//        unfinishExpressLV.setShowFootView(true);
//        finishExpressLV.setShowFootView(true);
        unfinishExpressLV.setShowHeaderView(false);
        finishExpressLV.setShowHeaderView(false);
        finishExpressLV.setAdapter(finishExpressListItemAdapter);
        unfinishExpressLV.setAdapter(unFinishExpressListItemAdapter);
        unfinishExpressLV.setVisibility(View.VISIBLE);
        finishExpressLV.setVisibility(View.GONE);

        tabRGP.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.amse_rbn_tableOne:
                        tableOneRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        tableTwoRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        unfinishExpressLV.setVisibility(View.VISIBLE);
                        finishExpressLV.setVisibility(View.GONE);
                        if (unFinishExpressListItemAdapter.getCount() == 0) {
//                            getUnfinishExpress(IS_GET_NO);
                            initUnFinish();
                        }
                        break;
                    case R.id.amse_rbn_tableTwo:
                        tableTwoRBN.setTextColor(getResources().getColor(R.color.reddish_orange));
                        tableOneRBN.setTextColor(getResources().getColor(R.color.gray_dark));
                        finishExpressLV.setVisibility(View.VISIBLE);
                        unfinishExpressLV.setVisibility(View.GONE);
                        if (finishExpressListItemAdapter.getCount() == 0) {
//                            getUnfinishExpress(IS_GET_NO);
                            initFinish();
                        }
                        break;
                }
            }
        });

        unFinishExpressListItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {
                mSendBagItem = (SendBagItem) view.getTag();
                //跳转导航页，先调用定位方法
                startLocation();
            }
        });

        finishExpressListItemAdapter.setOnViewClickListener(new ItemAdapter.OnViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {
                SendBagItem sendBagItem = (SendBagItem) view.getTag();
                //跳转物流页
                Intent intentNavigation = new Intent(mContext, CheckLogisticsActivity_.class);
                intentNavigation.putExtra("sendBagItem", sendBagItem);
                startActivity(intentNavigation);
            }
        });
    }

    /**
     * 通过数据库获取我的订单列表信息
     */
    private void getCacheData(){
        if (dataCacheDBManage.getGetMySendExpressList().size() != 0){
            for (int i=0; i < dataCacheDBManage.getGetMySendExpressList().size(); i ++){
                String is_enable = dataCacheDBManage.getGetMySendExpressList().get(i).getIs_enable();
                String bag_type = dataCacheDBManage.getGetMySendExpressList().get(i).getBag_type();
                if (("n").equals(is_enable)) {
                    finishAllItems.add(dataCacheDBManage.getGetMySendExpressList().get(i));
                } else {
                    if (("y").equals(bag_type)) {
                        finishAllItems.add(dataCacheDBManage.getGetMySendExpressList().get(i));
                    } else {
                        unfinishAllItems.add(dataCacheDBManage.getGetMySendExpressList().get(i));
                    }
                }
            }
        }
    }

    /**
     * 初始化未完成列表
     */
    private void initUnFinish(){
        if (unfinishAllItems.size() != 0){
            unFinishExpressListItemAdapter.clear();
            unFinishExpressListItemAdapter.addItems((List) unfinishAllItems);
            unFinishExpressListItemAdapter.notifyDataSetChanged();

            SendBagItem sendBagItem = unfinishAllItems.get(0);
            if ((sendBagItem.getCurrentTime() + sendBagItem.getSaveTime()) < System.currentTimeMillis()) {
                //清除数据库
                dataCacheDBManage.clear(DataCacheDBHelper.TAB_MY_SENDEXPRESS_CACHE);
                //获取网络请求数据
                getUnfinishExpress(IS_GET_NO);
            } else {
            }
        }else{
            getUnfinishExpress(IS_GET_NO);
        }
    }

    /**
     * 初始化已完成列表
     */
    private void initFinish(){
        if (finishAllItems.size() != 0){
            finishExpressListItemAdapter.clear();
            finishExpressListItemAdapter.addItems((List) finishAllItems);
            finishExpressListItemAdapter.notifyDataSetChanged();

            SendBagItem sendBagItem = finishAllItems.get(0);
            if ((sendBagItem.getCurrentTime() + sendBagItem.getSaveTime()) < System.currentTimeMillis()) {
                //清除数据库
                dataCacheDBManage.clear(DataCacheDBHelper.TAB_MY_SENDEXPRESS_CACHE);
                //获取网络请求数据
                getUnfinishExpress(IS_GET_NO);
            } else {
            }
        }else{
            getUnfinishExpress(IS_GET_NO);
        }
    }

    /**
     * 获取待取快递数据的方法
     */
    private void getUnfinishExpress(int isPullToRefresh){

//        if (isPullToRefresh == 1){
//            pageIndexUnget = 1;
//        }else if (isPullToRefresh == 2){
//            pageIndexUnget ++;
//        }else{
//            pageIndexUnget = 1;
//        }
//        mExpresslistInfo.setPageIndex(pageIndexUnget);
        //方便测试使用
//        BaseInfo.token = "ff0a6c74e7b4d08b659f9e2737a4ac5c";
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mSendBagListInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try {
                    if (mSendBagListInfo.getAllItems().size() == 0){
                        unfinishExpressLV.setVisibility(View.GONE);
                        finishExpressLV.setVisibility(View.GONE);
                        noDataRL.setVisibility(View.VISIBLE);
                        return;
                    }
//                    if (mSendBagListInfo.getAllItems().size() < pageSize) {
//                        noUpLoadFinishUnget = false;
//                    } else {
//                        noUpLoadFinishUnget = true;
//                    }
                    unFinishExpressListItemAdapter.clear();
                    finishExpressListItemAdapter.clear();
                    unfinishAllItems.clear();
                    finishAllItems.clear();
                    //将未完成和已完成分离
                    for (int i = 0; i < mSendBagListInfo.getAllItems().size(); i++) {
                        String is_enable = mSendBagListInfo.getAllItems().get(i).getIs_enable();
                        String bag_type = mSendBagListInfo.getAllItems().get(i).getBag_type();
                        if (("n").equals(is_enable)) {
                            finishAllItems.add(mSendBagListInfo.getAllItems().get(i));
                        } else {
                            if (("y").equals(bag_type)) {
                                finishAllItems.add(mSendBagListInfo.getAllItems().get(i));
                            } else {
                                unfinishAllItems.add(mSendBagListInfo.getAllItems().get(i));
                            }
                        }
                    }
                    unFinishExpressListItemAdapter.addItems((List) unfinishAllItems);
                    finishExpressListItemAdapter.addItems((List) finishAllItems);

                    finishExpressLV.onRefreshComplete();
                    finishExpressLV.onMoreComplete(true);
                    unFinishExpressListItemAdapter.notifyDataSetChanged();
                    finishExpressListItemAdapter.notifyDataSetChanged();

//                    finishExpressLV.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
//
//                        @Override
//                        public void onRefresh() {
//                            // 线程获取更新数据
//                            isPullRefresh = 1;
//                            getUnfinisExpress(IS_GET_NEW);
//
//                        }
//                    });
//
//                    finishExpressLV.setOnMoreListener(new PullToRefreshListView.OnMoreListener() {
//
//                        @Override
//                        public void onMore() {
//                            // 线程获取更多数据
//                            isPullRefresh = 2;
//                            getUnfinisExpress(IS_GET_MORE);
//
//                        }
//                    });

                    unfinishExpressLV.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
//							Intent intent = new Intent(mContext, CompanyDetailsActy.class);
//							startActivity(intent);
                            if (position == unFinishExpressListItemAdapter.getCount() + 1) {
                                return;
                            }
                            ListView listView = (ListView) parent;
                            SendBagItem sendBagItem = (SendBagItem) listView.getItemAtPosition(position);
                            Intent intentExpressDetail = new Intent(mContext, SenderExpressDetailActivity_.class);
                            intentExpressDetail.putExtra("sendBagItem", sendBagItem);
//                            intentExpressDetail.putExtra("mailno", mailno);
                            startActivityForResult(intentExpressDetail, Constant.StaticCode.REQUEST_SENDER_EXPRESS_DETAIL);

                        }
                    });

                    finishExpressLV.setOnItemClickListener(new PullToRefreshListView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
//							Intent intent = new Intent(mContext, CompanyDetailsActy.class);
//							startActivity(intent);
                            if (position == finishExpressListItemAdapter.getCount() + 1) {
                                return;
                            }
                            ListView listView = (ListView) parent;
                            SendBagItem sendBagItem = (SendBagItem) listView.getItemAtPosition(position);
                            Intent intentExpressDetail = new Intent(mContext, SenderExpressDetailActivity_.class);
                            intentExpressDetail.putExtra("sendBagItem", sendBagItem);
//                            intentExpressDetail.putExtra("mailno", mailno);
                            startActivityForResult(intentExpressDetail, Constant.StaticCode.REQUEST_SENDER_EXPRESS_DETAIL);

                        }
                    });
//					RecruitItem recruitItem = BaseInfo.gson.fromJson(jsonObject.getJSONObject("listArr").toString(), RecruitItem.class);
//					HWLog.e(TAG, "recruitItem----------------->"+allItems);
                } catch (Exception e) {
                    JKLog.e(TAG, "onRequestSuccess方法中-------->：");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_SENDER_EXPRESS_DETAIL) && (resultCode == RESULT_CANCELED)){
            getUnfinishExpress(IS_GET_NO);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }

    /**
     * 进入导航的方法
     */
    private void toNavigation(double locationLongitude, double locationLatitude){
//        Intent intentNavigation = new Intent(this, BasicNaviActivity.class);
//
//        intentNavigation.putExtra(BasicNaviActivity.END_LONGITUDE, Double.parseDouble(mSendBagItem.getLongitude()));
//        intentNavigation.putExtra(BasicNaviActivity.END_LATITUDE, Double.parseDouble(mSendBagItem.getLatitude()));
//        intentNavigation.putExtra(BasicNaviActivity.START_LONGITUDE, locationLongitude);
//        intentNavigation.putExtra(BasicNaviActivity.START_LATITUDE, locationLatitude);
//        startActivityForResult(intentNavigation, Constant.StaticCode.REQUEST_SENDER_ADDRESS);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        //定位回调
        JKLog.i(TAG, aMapLocation.toString());
        double locationLongitude = aMapLocation.getLongitude();
        double locationLatitude = aMapLocation.getLatitude();
        toNavigation(locationLongitude, locationLatitude);
    }
}
