package com.jiakang.pandlife.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiakang.pandlife.R;


public class TitleBar extends RelativeLayout {

    public Button leftBN;
    public ImageButton leftIBN;
    public TextView titleTV;
    public Button rightBN;
    public ImageButton rightIBN;


    public TitleBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public TitleBar(Context context) {
        super(context);
        init(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.include_titlebar, this);
        leftBN = (Button) this.findViewById(R.id.it_btn_left);
        leftIBN = (ImageButton) this.findViewById(R.id.it_ibn_left);
        titleTV = (TextView) this.findViewById(R.id.it_tv_title);
        rightBN = (Button) this.findViewById(R.id.it_btn_right);
        rightIBN = (ImageButton)this.findViewById(R.id.it_ibn_right);
    }


    public void setRightBg(int resId) {
        rightBN.setBackgroundResource(resId);
    }

    public void setBackBg(int resId) {
        leftIBN.setBackgroundResource(resId);
    }
}
