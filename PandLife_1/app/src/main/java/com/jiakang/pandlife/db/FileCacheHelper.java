package com.jiakang.pandlife.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jiakang.pandlife.utils.JKLog;


/**
 * 默认就在数据库里创建5张表
 * 
 * @FriendItem
 */
public class FileCacheHelper extends SQLiteOpenHelper {
	private static final String TAG = "FileCacheHelper";
	private static final int version = 16;// 数据库版本
	public final static String TAB_FILES = "t_files";
	public static String dbName = "fileCache.db";

	/** 大类表 */
	public final static String TAB_CATEGORY = "t_category";

	public FileCacheHelper(Context context) {
		super(context, dbName, null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		JKLog.i(TAG, "onCreate------->数据库创建了----");

		// 大类缓存表
		// db.execSQL(SqlBuilder.getCreatTableSQL(CategoryItem.class));
		// url<-->filePath
		db.execSQL("create table if not exists " + TAB_FILES + " (url varchar PRIMARY KEY, filePath text) ");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		JKLog.e(TAG, "onUpgrade-----> 数据库更新删除表了----oldVersion:" + oldVersion + ";---newVersion:" + newVersion);

		if (oldVersion == 15) {
			db.execSQL("DROP TABLE IF EXISTS " + TAB_CATEGORY);
		} else if (oldVersion == 23) {
		}
		onCreate(db);
	}
}