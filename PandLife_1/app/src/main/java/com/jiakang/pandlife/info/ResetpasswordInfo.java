package com.jiakang.pandlife.info;

/**
 * Created by Administrator on 2015/12/8.
 */


import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.MD5Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 1.1重设密码接口
 *
 * @author ww
 *
 */
public class ResetpasswordInfo extends BaseAbsInfo {

    private static final String TAG = "ResetpasswordInfo";
    private String mUserName = null;
    private String mPassword = null;
    /** 登录时返回的凭证	Varchar(32) */
    private String token;


    @Override
    public String requestStr() {
        return BaseInfo.data;
    }
    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress + "m=Api&c=Auth&a=Resetpassword" + "&subtime=" + System.currentTimeMillis();
//        return "http://192.168.1.2/proj/api/user/login";
    }

    @Override
    public JSONObject requestParams() {
        JSONObject json = new JSONObject();
        try {
            json.put("token", BaseInfo.token);
            json.put("newpassword", MD5Util.encodeByMD5(mPassword));
//            json.put("system", loginType);
        } catch (JSONException e) {
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {
        try {
            mResult = jsonObject.getString("status");
            data = (JSONObject)jsonObject.get("data");
            if (mResult.equals("1")) {
                String info = jsonObject.getString("info");
                BaseInfo.token = data.getString("token");
            } else {
                message = jsonObject.getString("message");
            }

        } catch (Exception e) {
//            JKLog.e(TAG, "getData方法中-------->：e为：" + e);
        }
    }

    public String getmUserName() {
        return mUserName;
    }

    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

}
