package com.jiakang.pandlife.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.item.GoodsItem;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.item.RecommendGoodsItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.ImageLoaderUtil;

/**
 * Created by play on 2016/1/6.
 */
public class GoodsListItemView extends AbsRelativeLayout {

    private ImageView imageIV;
    private ImageView addIV;
    private ImageView subtractIV;
    private EditText bugNumberET;
    private TextView goodsNameTV;
    private TextView contentTV;
    private TextView sellNumberTV;
    private TextView sumTV;
//    private CheckBox addCB;

    private int buyNumber = 0;

    public GoodsListItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GoodsListItemView(Context context) {
        super(context);
    }

    @Override
    public void findViewsByIds() {
        super.findViewsByIds();

        imageIV = (ImageView)findViewById(R.id.igl_iv_image);
        addIV = (ImageView)findViewById(R.id.igl_tv_goods_number_add);
        subtractIV = (ImageView)findViewById(R.id.igl_tv_goods_number_subtract);
        bugNumberET = (EditText) findViewById(R.id.igl_et_goods_number);
        goodsNameTV = (TextView) findViewById(R.id.igl_tv_goods_name);
        contentTV = (TextView) findViewById(R.id.igl_tv_content);
        sellNumberTV = (TextView) findViewById(R.id.igl_tv_number);
        sumTV = (TextView) findViewById(R.id.igl_tv_sum);
//        addCB = (CheckBox)findViewById(R.id.igl_cb_select_goods);

        bugNumberET.setEnabled(false);
    }

    @Override
    public void setObject(final Item item,final int position, final ItemAdapter.OnViewClickListener onViewClickListener) {
        super.setObject(item, position, onViewClickListener);
        final RecommendGoodsItem recommendGoodsItem = (RecommendGoodsItem)item;
        ImageLoaderUtil.displayImage(recommendGoodsItem.getPic(), imageIV, R.mipmap.default_image);
        goodsNameTV.setText(recommendGoodsItem.getTitle());
        sumTV.setText("￥" + recommendGoodsItem.getTprice() + "/份");
        sellNumberTV.setText("月售" + recommendGoodsItem.getSales() + "份");
        bugNumberET.setText(recommendGoodsItem.getClickNum() +"");
//        sumTV.setText("￥" + recommendGoodsItem.getTprice()+"/份");
//        sumTV.setText("￥" + recommendGoodsItem.getTprice()+"/份");


        addIV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                buyNumber ++;
//                recommendGoodsItem.setClickNum(buyNumber);
                v.setTag("add");
                onViewClickListener.onViewClick(v, position);
            }
        });
        subtractIV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recommendGoodsItem.getClickNum() == 0){
                    return;
                }
//                buyNumber --;
//                recommendGoodsItem.setClickNum(buyNumber);
                v.setTag("sub");
                onViewClickListener.onViewClick(v, position);
            }
        });

//        addCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                buttonView.setTag(isChecked);
//                onViewClickListener.onViewClick(buttonView, position);
//            }
//        });
    }
}
