package com.jiakang.pandlife.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.jiakang.pandlife.annotation.utils.ClassUtils;
import com.jiakang.pandlife.annotation.utils.CursorUtil;
import com.jiakang.pandlife.annotation.utils.SqlBuilder;
import com.jiakang.pandlife.item.Item;
import com.jiakang.pandlife.utils.JKLog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 数据库方法封装，创建表，删除表，数据(增删改查)...
 */
public class FileDBManage {
	private String TAG = "FileDBManage";
	private FileCacheHelper fileOpenHelper;

	public FileDBManage(Context context) {
		fileOpenHelper = new FileCacheHelper(context);
	}

	public void dropTable(String taleName) {
		fileOpenHelper.getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + taleName);
	}

	public void doSQL(String sql) {
		fileOpenHelper.getWritableDatabase().execSQL(sql);
	}

	public void clear(String table) {
		doSQL("delete from " + table);
	}

	/**
	 * 查询
	 * 
	 * @param sql8
	 * @return Cursor
	 */
	public Cursor queryBySQL(String sql) {
		return fileOpenHelper.getWritableDatabase().rawQuery(sql, null);
	}

	public void createTable(Item item) {

	}

	public void close() {
		fileOpenHelper.close();
	}

	public SQLiteDatabase getWritableDatabase() {
		return fileOpenHelper.getWritableDatabase();
	}

	public SQLiteDatabase getReadableDatabase() {
		return fileOpenHelper.getReadableDatabase();
	}

	/**
	 * -------------------------------通用----------------------------------------
	 */

	/**
	 * 通用
	 * 
	 * @param sql
	 * @param cls
	 * @return
	 */
	public List<Item> getList(String sql, Class<?> cls) {
		List<Item> itemList = new ArrayList<Item>();
		Cursor cursor = fileOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object sysItem = CursorUtil.fromJson(cursor, cls);
				if (sysItem != null)
					itemList.add((Item) sysItem);
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * 通用
	 * 
	 * @param sql
	 * @param cls
	 * @return
	 */
	public <T> List<T> getTList(String sql, Class<T> cls) {
		List<T> itemList = new ArrayList<T>();
		Cursor cursor = fileOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				Object item = CursorUtil.fromJson(cursor, cls);
				if (item != null)
					itemList.add((T) item);
			}
		}
		cursor.close();
		return itemList;
	}

	/**
	 * 通用通过sql和class返回实体
	 * 
	 * @param sql
	 * @param cls
	 * @return T
	 */
	public <T> T getTByClass(String sql, Class<T> cls) {
		Cursor cursor = fileOpenHelper.getReadableDatabase().rawQuery(sql, null);
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			Object item = CursorUtil.fromJson(cursor, cls);
			if (item != null)
				return (T) item;
		}
		cursor.close();
		return null;
	}

	/**
	 * 通用插入
	 * 
	 * @param item
	 * @param table
	 * @return void
	 */
	public void insert(Item item) {
		insert(item, ClassUtils.getTableName(item.getClass()));
	}

	/**
	 * 通用插入
	 * 
	 * @param item
	 * @param table
	 * @return void
	 */
	public void insert(Item item, String table) {
		if (TextUtils.isEmpty(table)) {
			table = ClassUtils.getTableName(item.getClass());
		}
		Map<String, Object> maps = CursorUtil.getMaps(item);
		String sql = SqlBuilder.getInsertSql(table, maps, null, true, true, ClassUtils.getPrimaryKeyColumn(item.getClass()));
		JKLog.e(TAG, "insert方法中--插入语句-------->：sql为：" + sql);
		doSQL(sql);
	}

	// public void insert(CategoryItem item) {
	// Map<String, Object> maps = CursorUtil.getMaps(item);
	// String sql = SqlBuilder.getInsertSql(FileCacheHelper.TAB_CATEGORY, maps,
	// null, true, true, "classId");
	// XYLog.e(TAG, "insert方法中--插入语句-------->：sql为：" + sql);
	// doSQL(sql);
	// }

	/**
	 * 筛选所有大类
	 * 
	 * @return List<CategoryItem>
	 */
	// public List<CategoryItem> getCategoryItemList(){
	// String sql = "select * from " + FileCacheHelper.TAB_CATEGORY +
	// " order by classId asc ";
	// return getTList(sql, CategoryItem.class);
	// }

	/**
	 * 删除所有大类
	 * 
	 * @return void
	 */
	public void clearCategoryTable() {
		clear(FileCacheHelper.TAB_CATEGORY);
	}

}