package com.jiakang.pandlife.info;

import com.jiakang.pandlife.BaseInfo;
import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.item.ToolRentItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by play on 2016/1/12.
 *工具列表
 */
public class ToolRentInfo extends BaseAbsInfo {

    private ToolRentItem toolRentItem;

    private List<ToolRentItem.DataEntity> dataEntityList = new ArrayList<>();

    public ToolRentItem getToolRentItem() {
        return toolRentItem;
    }

    public void setToolRentItem(ToolRentItem toolRentItem) {
        this.toolRentItem = toolRentItem;
    }

    public List<ToolRentItem.DataEntity> getDataEntityList() {
        return dataEntityList;
    }

    public void setDataEntityList(List<ToolRentItem.DataEntity> dataEntityList) {
        this.dataEntityList = dataEntityList;
    }

    @Override
    public String requestStr() {
        return null;
    }

    @Override
    public int requestDate() {
        return BaseInfo.DATE_MAX;
    }

    @Override
    public String requestUrl() {
        return Constant.mWebAddress +"m=Api&c=Server&a=Getleaselist&subtime=" + System.currentTimeMillis();
    }

    @Override
    public JSONObject requestParams() {

        JSONObject json = new JSONObject();
        try {
            json.put("token",BaseInfo.token);//ff0a6c74e7b4d08b659f9e2737a4ac5c
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    @Override
    public void responseData(JSONObject jsonObject) {

        try {
            if(jsonObject.getInt("status") == 1){

                toolRentItem = BaseInfo.gson.fromJson(jsonObject.toString(), ToolRentItem.class);
                dataEntityList = toolRentItem.getData();
                for (int i = 0; i < dataEntityList.size(); i++) {
                    dataEntityList.get(i).setLayout(R.layout.item_tool_rent);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
