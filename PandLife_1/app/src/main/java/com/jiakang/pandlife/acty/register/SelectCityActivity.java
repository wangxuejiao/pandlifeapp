package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.CityInfo;
import com.jiakang.pandlife.info.ProvinceInfo;
import com.jiakang.pandlife.item.CityItem;
import com.jiakang.pandlife.item.ProvinceItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.List;

/**
 * 选择城市
 */
@EActivity(R.layout.activity_select_city)
public class SelectCityActivity extends BaseActy {
    protected static final String TAG = "SelectCityActivity";

    @ViewById(R.id.asci_lv_city)
    ListView cityLV;

    private ItemAdapter mItemAdapter;
    /**
     * 选择城市接口
     */
    private CityInfo mCityInfo = new CityInfo();

    private ProvinceItem mProvinceItem;

    private String siteOrCommunity = "community_and_site";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar() {
        initTitleBar(R.id.asci_tb_title, "选择城市");
        bindView();
        bindInfo();
    }

    private void bindView() {
        mItemAdapter = new ItemAdapter(mContext);
        cityLV.setAdapter(mItemAdapter);

        Intent intent = getIntent();
        mProvinceItem = (ProvinceItem)intent.getExtras().get("provinceItem");
        siteOrCommunity = intent.getExtras().getString(SelectLocationOrManualActivity.SITE_OR_COMMUNITY);
    }

    private void bindInfo() {
        ApiManager apiManager = ApiManager.getInstance();
        mCityInfo.setId(mProvinceItem.getCode());
        apiManager.request(mCityInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        if (mCityInfo.getAllItems().size() == 0) {
                            CustomToast.showToast(mContext, "未获取到城市信息，请重新获取");
                            return;
                        }
                        mItemAdapter.addItems((List) mCityInfo.getAllItems());
                        mItemAdapter.notifyDataSetChanged();

                        cityLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                ListView listView = (ListView) parent;
                                CityItem cityItem = (CityItem) listView.getItemAtPosition(position);
                                Intent intentArea = new Intent(mContext, SelectAreaActivity_.class);
                                intentArea.putExtra("cityItem", cityItem);
                                intentArea.putExtra("provinceItem",mProvinceItem);
                                intentArea.putExtra(SelectLocationOrManualActivity.SITE_OR_COMMUNITY, siteOrCommunity);
                                startActivityForResult(intentArea, Constant.StaticCode.REQUSET_BIND_SITE);
                            }
                        });

                    } else {
                        JKLog.i(TAG, "失败");
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUSET_BIND_SITE) && (resultCode == RESULT_OK)){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
