package com.jiakang.pandlife.item;

import java.util.List;

/**
 * Created by play on 2016/1/20.
 */
public class ReplayCommentPost {


    /**
     * data : []
     * status : 1
     * info : 回复成功
     */
    private List<?> data;
    private int status;
    private String info;

    public void setData(List<?> data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<?> getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }
}
