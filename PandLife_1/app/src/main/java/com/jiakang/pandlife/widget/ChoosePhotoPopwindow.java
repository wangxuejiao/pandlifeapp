package com.jiakang.pandlife.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;

import com.jiakang.pandlife.R;

/**
 * Created by play on 2016/1/8.
 */
public class ChoosePhotoPopwindow extends PopupWindow{

    private View view;
    private Button btnPhotograph;
    private Button btnPhotoAlbum;
    private Button btnCancel;

    public ChoosePhotoPopwindow(Context context,View.OnClickListener itemsOnclickListener) {

        super(context);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.choose_image_popwindow, null);

        btnPhotograph = (Button) view.findViewById(R.id.btn_photograph_popwindow);
        btnPhotoAlbum = (Button) view.findViewById(R.id.btn_album_popwindow);
        btnCancel = (Button) view.findViewById(R.id.btn_cancel_popwindow);

        /*点击取消按钮*/
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        /*设置按钮监听*/
        btnPhotograph.setOnClickListener(itemsOnclickListener);
        btnPhotoAlbum.setOnClickListener(itemsOnclickListener);
        //设置SelectPicPopupWindow的View
        this.setContentView(view);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
        this.setAnimationStyle(R.style.popup_down_in_out);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);

        //mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
        view.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                int height = view.findViewById(R.id.ll_choose_popwindow).getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });

    }

}
