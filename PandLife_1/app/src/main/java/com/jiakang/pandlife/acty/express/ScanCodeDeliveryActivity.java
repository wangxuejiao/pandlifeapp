package com.jiakang.pandlife.acty.express;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 扫码取件
 */
@EActivity(R.layout.activity_scan_code_delivery)
public class ScanCodeDeliveryActivity extends BaseActy {
    protected static final String TAG = "ScanCodeDeliveryActivity";

    @ViewById(R.id.ascd_btn_start_scanning)
    Button scanningBN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        scanningBN.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //处理扫描结果（在界面上显示）
        if ((requestCode == Constant.StaticCode.REQUEST_SCANNING_BARCODE) && (resultCode == RESULT_OK)) {
            Bundle bundle = data.getExtras();
            String scanResult = bundle.getString("result");
//            resultTextView.setText(scanResult);
            Intent intent = new Intent(this, DeliverySuccessActivity_.class);
            startActivityForResult(intent, Constant.StaticCode.REQUEST_SCANNING_BARCODE);
            finish();
        }
        //根据点击DeliverySuccessActivity中返回列表进入该方法
        if ((requestCode == Constant.StaticCode.REQUEST_SCANNING_BARCODE) && (resultCode == RESULT_CANCELED)) {
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.ascd_btn_start_scanning:
                //打开扫描界面扫描条形码或二维码
               /* Intent openCameraIntent = new Intent(this, CaptureActivity.class);
                startActivityForResult(openCameraIntent, Constant.StaticCode.REQUEST_SCANNING_BARCODE);
                break;*/
        }
    }
}
