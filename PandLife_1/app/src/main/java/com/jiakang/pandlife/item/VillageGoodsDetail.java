package com.jiakang.pandlife.item;

import java.util.List;

/**
 * Created by play on 2016/1/27.
 */
public class VillageGoodsDetail {


    /**
     * nick : 栋楼
     * head : http://7xpw5g.com1.z0.glb.clouddn.com/http://7xpw5g.com1.z0.glb.clouddn.com/http://7xpw5g.com1.z0.glb.clouddn.com/1453688071038.jpg
     * replyid : 303
     * replynum : 4
     * optionlist : [{"count":"0","id":"1","option":"强烈需要"}]
     * dialoglist : [{"replyuser":[],"pic":null,"content":"bucuo","isshow":"1","nick":"栋楼","head":"http://7xpw5g.com1.z0.glb.clouddn.com/http://7xpw5g.com1.z0.glb.clouddn.com/http://7xpw5g.com1.z0.glb.clouddn.com/1453688071038.jpg","uid":"13","replyid":"16","dialog_id":"303","id":"16","time":"17:39","floor":"3","support":0},{"replyuser":[],"pic":null,"content":"再来一个","isshow":"1","nick":"栋楼","head":"http://7xpw5g.com1.z0.glb.clouddn.com/http://7xpw5g.com1.z0.glb.clouddn.com/http://7xpw5g.com1.z0.glb.clouddn.com/1453688071038.jpg","uid":"13","replyid":"17","dialog_id":"303","id":"17","time":"17:39","floor":"4","support":0},{"replyuser":{"nick":"栋楼","uid":"13","id":"17","floor":"4"},"pic":null,"content":"再来一个","isshow":"1","nick":"栋楼","head":"http://7xpw5g.com1.z0.glb.clouddn.com/http://7xpw5g.com1.z0.glb.clouddn.com/http://7xpw5g.com1.z0.glb.clouddn.com/1453688071038.jpg","uid":"13","replyid":"18","dialog_id":"303","id":"18","time":"17:40","floor":"5","support":0},{"replyuser":{"nick":"栋楼","uid":"13","id":"17","floor":"4"},"pic":null,"content":"再来一个冯绍峰的说法","isshow":"1","nick":"栋楼","head":"http://7xpw5g.com1.z0.glb.clouddn.com/http://7xpw5g.com1.z0.glb.clouddn.com/http://7xpw5g.com1.z0.glb.clouddn.com/1453688071038.jpg","uid":"13","replyid":"19","dialog_id":"303","id":"19","time":"17:40","floor":"6","support":0}]
     * time : 1452936215
     * title : 小区活动
     * support : 0
     * imglist : [{"imgurl":"https://img.alicdn.com/bao/uploaded/i2/2137121108/TB2c5i1hXXXXXaaXpXXXXXXXXXX_!!2137121108.jpg_80x80.jpg"},{"imgurl":"https://img.alicdn.com/imgextra/i3/2203822923/TB22EFEcpXXXXXqXpXXXXXXXXXX_!!2203822923.jpg"}]
     * content : 星期天天气不错，大家一起去郊游
     * supportnum : 0
     * isoption 1 投票 0未投票
     */
    private String type;
    private String typepic;
    private String typename;
    private String nick;
    private String head;
    private String replyid;
    private String replynum;
    private List<VoteItem> optionlist;
    private List<ReplayItem> dialoglist;
    private String time;
    private String title;
    private int support;
    private List<ImglistEntity> imglist;
    private String content;
    private String supportnum;
    private String isoption;
    private String limittime;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypepic() {
        return typepic;
    }

    public void setTypepic(String typepic) {
        this.typepic = typepic;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public void setReplyid(String replyid) {
        this.replyid = replyid;
    }

    public void setReplynum(String replynum) {
        this.replynum = replynum;
    }

    public void setOptionlist(List<VoteItem> optionlist) {
        this.optionlist = optionlist;
    }

    public void setDialoglist(List<ReplayItem> dialoglist) {
        this.dialoglist = dialoglist;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSupport(int support) {
        this.support = support;
    }

    public void setImglist(List<ImglistEntity> imglist) {
        this.imglist = imglist;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setSupportnum(String supportnum) {
        this.supportnum = supportnum;
    }

    public String getNick() {
        return nick;
    }

    public String getHead() {
        return head;
    }

    public String getReplyid() {
        return replyid;
    }

    public String getReplynum() {
        return replynum;
    }

    public List<VoteItem> getOptionlist() {
        return optionlist;
    }

    public List<ReplayItem> getDialoglist() {
        return dialoglist;
    }

    public String getTime() {
        return time;
    }

    public String getTitle() {
        return title;
    }

    public int getSupport() {
        return support;
    }

    public List<ImglistEntity> getImglist() {
        return imglist;
    }

    public String getContent() {
        return content;
    }

    public String getSupportnum() {
        return supportnum;
    }

    public String getIsoption() {
        return isoption;
    }

    public void setIsoption(String isoption) {
        this.isoption = isoption;
    }

    public String getLimittime() {
        return limittime;
    }

    public void setLimittime(String limittime) {
        this.limittime = limittime;
    }

    public class ImglistEntity {
        /**
         * imgurl : https://img.alicdn.com/bao/uploaded/i2/2137121108/TB2c5i1hXXXXXaaXpXXXXXXXXXX_!!2137121108.jpg_80x80.jpg
         */
        private String imgurl;

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        public String getImgurl() {
            return imgurl;
        }
    }
}
