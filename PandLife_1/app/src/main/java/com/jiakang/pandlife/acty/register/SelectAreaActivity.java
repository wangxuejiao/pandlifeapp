package com.jiakang.pandlife.acty.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.adapter.ItemAdapter;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.AreaInfo;
import com.jiakang.pandlife.info.CityInfo;
import com.jiakang.pandlife.item.AreaItem;
import com.jiakang.pandlife.item.CityItem;
import com.jiakang.pandlife.item.ProvinceItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.JKLog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import java.util.List;

/**
 * 选择地区
 */
@EActivity(R.layout.activity_select_area)
public class SelectAreaActivity extends BaseActy {
    protected static final String TAG = "SelectCityActivity";

    @ViewById(R.id.asar_lv_area)
    ListView areaLV;

    private ItemAdapter mItemAdapter;
    /**
     * 选择地区接口
     */
    private AreaInfo mAreaInfo = new AreaInfo();

    private CityItem mCityItem;
    private ProvinceItem mProvinceItem;

    private String siteOrCommunity = "community_and_site";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar() {
        initTitleBar(R.id.asar_tb_title, "选择地区");
        bindView();
        bindInfo();
    }

    private void bindView() {
        mItemAdapter = new ItemAdapter(mContext);
        areaLV.setAdapter(mItemAdapter);

        Intent intent = getIntent();
        mCityItem = (CityItem)intent.getExtras().get("cityItem");
        mProvinceItem = (ProvinceItem)intent.getExtras().get("provinceItem");
        siteOrCommunity = intent.getExtras().getString(SelectLocationOrManualActivity.SITE_OR_COMMUNITY);
    }

    private void bindInfo() {
        ApiManager apiManager = ApiManager.getInstance();
        mAreaInfo.setId(mCityItem.getCode());
        apiManager.request(mAreaInfo, new AbsOnRequestListener(mContext) {
            @Override
            public void onRequestSuccess(int stat, JSONObject jsonObject) {
                super.onRequestSuccess(stat, jsonObject);
                try {
                    if (stat == 1) {
                        if (mAreaInfo.getAllItems().size() == 0) {
                            CustomToast.showToast(mContext, "未获取到地区信息，请重新获取");
                            return;
                        }
                        mItemAdapter.addItems((List) mAreaInfo.getAllItems());
                        mItemAdapter.notifyDataSetChanged();

                        areaLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                ListView listView = (ListView) parent;
                                AreaItem areaItem = (AreaItem) listView.getItemAtPosition(position);
                                Intent intentCommunity = new Intent(mContext, SelectCommunityActivity_.class);
                                intentCommunity.putExtra("areaItem", areaItem);
                                intentCommunity.putExtra("provinceItem", mProvinceItem);
                                intentCommunity.putExtra("cityItem", mCityItem);
                                intentCommunity.putExtra(SelectLocationOrManualActivity.SITE_OR_COMMUNITY, siteOrCommunity);
                                startActivityForResult(intentCommunity, Constant.StaticCode.REQUSET_BIND_SITE);
                            }
                        });

                    } else {
                        JKLog.i(TAG, "失败");
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUSET_BIND_SITE) && (resultCode == RESULT_OK)){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.it_ibn_left:
                finish();
                break;
        }
    }
}
