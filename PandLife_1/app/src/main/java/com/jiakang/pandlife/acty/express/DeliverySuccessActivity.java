package com.jiakang.pandlife.acty.express;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
//import com.zxing.activity.CaptureActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 扫描成功
 */
@EActivity(R.layout.activity_delivery_success)
public class DeliverySuccessActivity extends BaseActy {
    protected static final String TAG = "DeliverySuccessActivity";

    @ViewById(R.id.ads_btn_check_my_bill)
    Button checkMyBillBN;
    @ViewById(R.id.ads_btn_continue_delivery)
    Button continueDeliveryBN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        checkMyBillBN.setOnClickListener(this);
        continueDeliveryBN.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //处理扫描结果（在界面上显示）
        if ((requestCode == Constant.StaticCode.REQUEST_SCANNING_BARCODE) && (resultCode == RESULT_OK)) {
            Bundle bundle = data.getExtras();
            String scanResult = bundle.getString("result");
//            resultTextView.setText(scanResult);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.ads_btn_check_my_bill:
                setResult(RESULT_CANCELED);
                break;
            case R.id.ads_btn_continue_delivery:
                //打开扫描界面扫描条形码或二维码
//                Intent openCameraIntent = new Intent(this, CaptureActivity.class);
//                startActivityForResult(openCameraIntent, Constant.StaticCode.REQUEST_SCANNING_BARCODE);
                break;
        }
    }
}
