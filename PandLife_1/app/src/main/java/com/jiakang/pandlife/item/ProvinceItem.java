package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

/**
 * Created by Administrator on 2016/1/11.
 */
public class ProvinceItem extends Item{

    /** 省份id	Int（10） */
    private int id;
    /** 地区编码	Int（10） */
    private int code;
    /** 上级id，省级默认为0	Int（10） */
    private int parentid;
    /** 省份名称	Varchar(32) */
    private String name;
    /** 省市区类型,省级默认为1	Int（10） */
    private int level;

    private int layout = R.layout.item_select_single_left;

    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getParentid() {
        return parentid;
    }

    public void setParentid(int parentid) {
        this.parentid = parentid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }
}
