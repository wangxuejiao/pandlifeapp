package com.jiakang.pandlife.acty.express;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.item.ExpressListItem;
import com.jiakang.pandlife.widget.CustomListView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * 快递详情
 */
@EActivity(R.layout.activity_express_detail)
public class ExpressDetailActivity extends BaseActy {

    @ViewById(R.id.aed_clv_logistics)
    CustomListView logisticsCLV;
    @ViewById(R.id.aed_tv_waybill_number_content)
    TextView wayBillNumberTV;
    @ViewById(R.id.aed_tv_waybill_company_content)
    TextView wayBillCompanyTV;
    @ViewById(R.id.aed_tv_waybill_date_content)
    TextView wayBillDateTV;
    @ViewById(R.id.aed_tv_sender_name_content)
    TextView senderNameTV;
    @ViewById(R.id.aed_tv_sender_phone_content)
    TextView senderPhoneTV;
    @ViewById(R.id.aed_tv_sender_address_content)
    TextView senderAddressTV;
    @ViewById(R.id.aed_tv_receiver_name_content)
    TextView receiverNameTV;
    @ViewById(R.id.aed_tv_receiver_phone_content)
    TextView receiverPhoneTV;
    @ViewById(R.id.aed_tv_receiver_address_content)
    TextView receiverAddressTV;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    protected void initVar(){
        initTitleBar(R.id.aed_tb_title, "快递详情");

        initView();
    }

    private void initView(){
        Intent intent = getIntent();
        if (intent != null){
            String mailno = intent.getExtras().getString("mailno");
            ExpressListItem expressListItem = (ExpressListItem)intent.getExtras().get("expressListItem");
        }
    }
}
