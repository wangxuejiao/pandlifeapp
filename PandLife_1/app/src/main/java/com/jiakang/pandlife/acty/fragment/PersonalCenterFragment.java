package com.jiakang.pandlife.acty.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.PandLifeApp;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.express.MyPackageActivity_;
import com.jiakang.pandlife.acty.personalcenter.AccountBalanceActivity_;
import com.jiakang.pandlife.acty.personalcenter.MyAddressBookActivity_;
import com.jiakang.pandlife.acty.personalcenter.MyIndentActivity_;
import com.jiakang.pandlife.acty.personalcenter.MyIssueActivity_;
import com.jiakang.pandlife.acty.personalcenter.MyMerchantIndentActivity_;
import com.jiakang.pandlife.acty.personalcenter.MyRedPackageActivity_;
import com.jiakang.pandlife.acty.personalcenter.MyRentActivity_;
import com.jiakang.pandlife.acty.personalcenter.MySendExpressActivity_;
import com.jiakang.pandlife.acty.personalcenter.MyServiceSiteActivity_;
import com.jiakang.pandlife.acty.personalcenter.PersonalDataActivity_;
import com.jiakang.pandlife.acty.personalcenter.RecipientsAddressBookActivity_;
import com.jiakang.pandlife.acty.personalcenter.SettingActivity_;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.info.EditHeadInfo;
import com.jiakang.pandlife.info.EditMyInfo;
import com.jiakang.pandlife.info.MyInfo;
import com.jiakang.pandlife.info.MyRedPackInfo;
import com.jiakang.pandlife.item.ConsumptionDetailItem;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.QiNiuUtil;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.ChoosePhotoPopwindow;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

/**
 * 我的Fragment
 */
public class PersonalCenterFragment extends Fragment implements View.OnClickListener{
    protected static final String TAG = "PersonalCenterFragment";

    private ImageView headIV;
    private TextView userNameTV;
    private TextView balanceTV;
    private TextView myGroupBillTV;
    private TextView myMerchantBillTV;
    private TextView myRedPacketTV;
    private TextView mySendExpressTV;
    private TextView myObtainExpressTV;
    private TextView myRentTV;
    private TextView myAddressTV;
    private TextView recipientsAddressTV;
    private TextView myReviceSiteTV;
    private TextView myIssueTV;
    private ImageView messageCenterIV;
    private ImageView settingIV;

//    private final static int RESULT_LOAD_IMAGE = 4;
//    private final static int REQUEST_CODE_CAPTURE_CAMEIA = 5;
    public static final String IMAGE_UNSPECIFIED = "image/*";
    public static final int PhotoResult = 3;
    private static String savePicPath;
    private String saveBase = "/sdcard/header/";
    private File headerFile;

    private PopupWindow popupWindow;
    //记录头像的uri
    private String headUri;

    private boolean isChangeImage = false;

    /** 获取我的信息接口 */
    private MyInfo mMyInfo = new MyInfo();
    /** 设置提交我的信息接口 */
    private EditMyInfo mEditMyInfo = new EditMyInfo();
    /** 我的红包接口 */
    private MyRedPackInfo mMyRedPackInfo = new MyRedPackInfo();
    /** 单独修改头像信息接口 */
    private EditHeadInfo mEditHeadInfo = new EditHeadInfo();

    private UserItem mUserItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_personal_center, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initVar();
        initView();
        bindView();
        bindData();
    }

    private void initVar(){
        //初始化七牛
        QiNiuUtil.requestQiNiuToken(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("TakePhotos中：onResume方法-------------->");

        if (Util.hasSDCard()) {
            savePicPath = saveBase + Util.getFolderName("yyyyMMdd");
            File pathFile = new File(savePicPath);
            if (!pathFile.exists()) {
                pathFile.mkdirs();
            }
        }
    }

    private void initView(){
        headIV = (ImageView)getView().findViewById(R.id.fpc_iv_head);
        userNameTV = (TextView)getView().findViewById(R.id.fpc_tv_userName);
        balanceTV = (TextView)getView().findViewById(R.id.fpc_tv_balance_content);
        myGroupBillTV = (TextView)getView().findViewById(R.id.fpc_tv_my_group_bill);
        myMerchantBillTV = (TextView)getView().findViewById(R.id.fpc_tv_my_merchant_bill);
        myRedPacketTV = (TextView)getView().findViewById(R.id.fpc_tv_red_package_content);
        mySendExpressTV = (TextView)getView().findViewById(R.id.fpc_tv_my_send_express);
        myObtainExpressTV = (TextView)getView().findViewById(R.id.fpc_tv_my_obtain_express);
        myRentTV = (TextView)getView().findViewById(R.id.fpc_tv_my_rent);
        myAddressTV = (TextView)getView().findViewById(R.id.fpc_tv_my_address);
        recipientsAddressTV = (TextView)getView().findViewById(R.id.fpc_tv_recipients_address);
        myReviceSiteTV = (TextView)getView().findViewById(R.id.fpc_tv_my_service_site);
        myIssueTV = (TextView)getView().findViewById(R.id.fpc_tv_my_issue);
        messageCenterIV = (ImageView)getView().findViewById(R.id.fpc_iv_message);
        settingIV = (ImageView)getView().findViewById(R.id.fpc_iv_setting);
    }

    private void bindView(){
        headIV.setOnClickListener(this);
        userNameTV.setOnClickListener(this);
//        phoneTV.setOnClickListener(this);
        getView().findViewById(R.id.fpc_rl_user).setOnClickListener(this);
        getView().findViewById(R.id.fpc_rl_my_balance).setOnClickListener(this);
        getView().findViewById(R.id.fpc_rl_my_red_package).setOnClickListener(this);
        myGroupBillTV.setOnClickListener(this);
        myMerchantBillTV.setOnClickListener(this);
        mySendExpressTV.setOnClickListener(this);
        myObtainExpressTV.setOnClickListener(this);
        myRentTV.setOnClickListener(this);
        myAddressTV.setOnClickListener(this);
        recipientsAddressTV.setOnClickListener(this);
        myReviceSiteTV.setOnClickListener(this);
        myIssueTV.setOnClickListener(this);
        messageCenterIV.setOnClickListener(this);
        settingIV.setOnClickListener(this);

    }

    private void bindData(){
        mUserItem = PandLifeApp.getInstance().getUserItem();
        if (mUserItem != null){
            if (!TextUtils.isEmpty(mUserItem.getHead())){
                ImageLoaderUtil.displayImage(mUserItem.getHead(), headIV, R.mipmap.default_image);
            }
            if (!TextUtils.isEmpty(mUserItem.getNick())){
                userNameTV.setText(mUserItem.getNick());
            }
            float money = 0;
            money = mUserItem.getMoney();
            balanceTV.setText("￥" + money);
        }else{
            ApiManager apiManager = ApiManager.getInstance();
            apiManager.request(mMyInfo, new AbsOnRequestListener(getActivity()){
                @Override
                public void onRequestSuccess(int result, JSONObject jsonObject) {
                    super.onRequestSuccess(result, jsonObject);
                    try {
                        mUserItem = mMyInfo.getUserItem();
                        if (mUserItem == null){
                            CustomToast.showToast(getActivity(), "获取信息失败，请稍后重试");
                            return;
                        }
                        if (!TextUtils.isEmpty(mUserItem.getHead())){
                            ImageLoaderUtil.displayImage(mUserItem.getHead(), headIV);
                        }
                        if (!TextUtils.isEmpty(mUserItem.getNick())){
                            userNameTV.setText(mUserItem.getNick());
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });
        }

        ApiManager apiManagerRedPackage = ApiManager.getInstance();
        apiManagerRedPackage.request(mMyRedPackInfo, new AbsOnRequestListener(getActivity()){
            @Override
            public void onRequestSuccess(int result, JSONObject jsonObject) {
                super.onRequestSuccess(result, jsonObject);
                try{
                    int usable = mMyRedPackInfo.getUsable();
                    myRedPacketTV.setText(""+usable);
                }catch (Exception e){

                }
            }
        });
    }

    /**
     * 单独修改头像的方法
     */
    private void changeHead(){
        mEditHeadInfo.setHead(headUri);
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.request(mEditHeadInfo, new AbsOnRequestListener(getActivity()){
            @Override
            public void onRequestFail(int result, JSONObject jsonObject) {
                super.onRequestFail(result, jsonObject);
                try {
                    CustomToast.showToast(getActivity(), "修改成功");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == Constant.StaticCode.REQUEST_CHANGED_DATA) && (resultCode == getActivity().RESULT_OK)){
            bindData();
        }
        if ((requestCode == Constant.StaticCode.REQUEST_SETTING_LOGOUT) && (resultCode == getActivity().RESULT_OK)){
            getActivity().finish();
        }
        //requestCode标示请求的标示   resultCode表示有数据
        if(requestCode == Constant.StaticCode.REQUEST_CODE_CAPTURE_ALBUM){
            Intent intent = Util.startPhotoZoom(data.getData());
            startActivityForResult(intent, PhotoResult);
        }else if (requestCode == Constant.StaticCode.REQUEST_CODE_CAPTURE_CAMEIA ) {
            Intent intent = Util.startPhotoZoom(Uri.fromFile(tempFile));
            startActivityForResult(intent, PhotoResult);
        }else if (requestCode == PhotoResult) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                // Parcel包裹，打包；
                Bitmap photo = extras.getParcelable("data");
                // 设置图片
                headIV.setImageBitmap(photo);
                //完成后关闭悬浮框
                popupWindow.dismiss();
                isChangeImage = true;
                try {
                    String picName = Util.getPicName();
                    // 保存图片到本地
                    //上传图片到七牛并且网络请求更换头像
                    headerFile = Util.saveFile(photo, savePicPath, picName);

                    String key = System.currentTimeMillis() + ".jpg";;
                    headUri = key;
//					mEditMyInfo.setHead(key);
                    QiNiuUtil.uploadNoConfig(savePicPath + "/" + picName, key, QiNiuUtil.qiNiuToken);
                    //修改头像
                    changeHead();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private File tempFile = new File(Environment.getExternalStorageDirectory(),  Util.getPicName());
    /**
     * 弹出选择头像的悬浮框
     */
    private void showPopWindown() {

        //设置popwindownAcitivity中的位置（底部居中）
        popupWindow = new ChoosePhotoPopwindow(getActivity(), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (v.getId()) {

                    case R.id.btn_photograph_popwindow:
                        String state = Environment.getExternalStorageState();
                        if (state.equals(Environment.MEDIA_MOUNTED)) {

                            Intent getImageByCamera = new Intent("android.media.action.IMAGE_CAPTURE");
                            // 指定调用相机拍照后照片的储存路径
                            getImageByCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
                            startActivityForResult(getImageByCamera, Constant.StaticCode.REQUEST_CODE_CAPTURE_CAMEIA);
                        }
                        else {
                            CustomToast.showToast(getActivity(), "请确认已经插入SD卡");
//                            Toast.makeText(getActivity(), "请确认已经插入SD卡", Toast.LENGTH_LONG).show();
                        }
                        break;

                    case R.id.btn_album_popwindow:

                        Intent intentAlbum = new Intent(Intent.ACTION_PICK, null);
                        intentAlbum.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
                        startActivityForResult(intentAlbum, Constant.StaticCode.REQUEST_CODE_CAPTURE_ALBUM);

                        break;

                    default:
                        break;
                }

            }
        });
        popupWindow.showAtLocation(getActivity().findViewById(R.id.fpc_ll_personal_center), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fpc_rl_user:
                Intent intentPersonalData = new Intent(getActivity(), PersonalDataActivity_.class);
                startActivityForResult(intentPersonalData, Constant.StaticCode.REQUEST_CHANGED_DATA);
                break;
            case R.id.fpc_iv_head:
                showPopWindown();
                break;
//            case R.id.fpc_tv_userName:
//                Intent intentChangeName = new Intent(getActivity(), ChangeUserNameActivity_.class);
//                startActivityForResult(intentChangeName, Constant.StaticCode.REQUEST_CHANGE_NAME);
//                break;
            case R.id.fpc_rl_my_balance:
                Intent intentMyBalance = new Intent(getActivity(), AccountBalanceActivity_.class);
                startActivity(intentMyBalance);
                break;
            case R.id.fpc_rl_my_red_package:
                Intent intentMyRedPackage = new Intent(getActivity(), MyRedPackageActivity_.class);
                startActivity(intentMyRedPackage);
                break;
            case R.id.fpc_tv_my_group_bill:
                Intent intentMyGroupIndent = new Intent(getActivity(), MyIndentActivity_.class);
                startActivity(intentMyGroupIndent);
                break;
            case R.id.fpc_tv_my_merchant_bill:
                Intent intentMyMerchantIndent = new Intent(getActivity(), MyMerchantIndentActivity_.class);
                startActivity(intentMyMerchantIndent);
                break;
            case R.id.fpc_tv_my_send_express:
                Intent intentMySendExpress = new Intent(getActivity(), MySendExpressActivity_.class);
                startActivity(intentMySendExpress);
                break;
            case R.id.fpc_tv_my_obtain_express:
                Intent intentMyPackage = new Intent(getActivity(), MyPackageActivity_.class);
                startActivity(intentMyPackage);
                break;
            case R.id.fpc_tv_my_rent:
                Intent intentMyRent = new Intent(getActivity(), MyRentActivity_.class);
                startActivity(intentMyRent);
                break;
            case R.id.fpc_tv_my_address:
                Intent intentMyAddress = new Intent(getActivity(), MyAddressBookActivity_.class);
                startActivity(intentMyAddress);
                break;
            case R.id.fpc_tv_recipients_address:
                Intent intentRecipientsAddress = new Intent(getActivity(), RecipientsAddressBookActivity_.class);
                startActivity(intentRecipientsAddress);
                break;
            case R.id.fpc_tv_my_service_site:
                Intent intentMyServiceSite = new Intent(getActivity(), MyServiceSiteActivity_.class);
                startActivity(intentMyServiceSite);
                break;
            case R.id.fpc_tv_my_issue:
                Intent intentIssue = new Intent(getActivity(), MyIssueActivity_.class);
                startActivity(intentIssue);
                break;
            case R.id.fpc_iv_message:
//                Intent intentMessage = new Intent(getActivity(), MessageCenterActivity.class);
//                startActivity(intentMessage);
                break;
            case R.id.fpc_iv_setting:
                Intent intentSetting = new Intent(getActivity(), SettingActivity_.class);
                startActivityForResult(intentSetting, Constant.StaticCode.REQUEST_SETTING_LOGOUT);
                break;
        }
    }
}
