package com.jiakang.pandlife.acty.personalcenter;

import java.io.File;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jiakang.pandlife.Constant;
import com.jiakang.pandlife.R;
import com.jiakang.pandlife.acty.BaseActy;
import com.jiakang.pandlife.api.AbsOnRequestListener;
import com.jiakang.pandlife.api.ApiManager;
import com.jiakang.pandlife.db.AreaDBManage;
import com.jiakang.pandlife.info.EditHeadInfo;
import com.jiakang.pandlife.info.EditMyInfo;
import com.jiakang.pandlife.item.UserItem;
import com.jiakang.pandlife.utils.CustomToast;
import com.jiakang.pandlife.utils.ImageLoaderUtil;
import com.jiakang.pandlife.utils.QiNiuUtil;
import com.jiakang.pandlife.utils.Util;
import com.jiakang.pandlife.widget.ChoosePhotoPopwindow;
import com.jiakang.pandlife.widget.CustomDialog;
import com.jiakang.pandlife.widget.TitleBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.json.JSONObject;

import cn.qqtheme.framework.picker.DatePicker;

/**
 * 修改个人资料
 */
@EActivity(R.layout.activity_change_personal_data)
public class ChangePersonalDataActivity extends BaseActy {
	protected final String TAG = "ChangePersonalDataActivity";

	@ViewById(R.id.acpd_tb_title)
	TitleBar mTitleBar;
	@ViewById(R.id.acpd_iv_head)
	ImageView headerIV;
	@ViewById(R.id.acpd_tv_nick_name_content)
	EditText nickNameET;
	@ViewById(R.id.acpd_tv_real_name_content)
	EditText realNameET;
	@ViewById(R.id.acpd_tv_identity_card_content)
	EditText identityCardET;
	@ViewById(R.id.acpd_tv_birthday_content)
	TextView birthDayTV;
	@ViewById(R.id.acpd_tv_phone_content)
	EditText telephoneET;
	@ViewById(R.id.acpd_tv_home_address_content)
	EditText addressET;

	private AreaDBManage areaDBManage;
	private final int MSG_UPDATE_FILE = 0x124;
	
	private String cityCode;
	private String areaCode;
	private String postCode;
	
	private final static int RESULT_LOAD_IMAGE = 4;
	public static final String IMAGE_UNSPECIFIED = "image/*";
	public static final int PhotoResult = 3;
	private static String savePicPath;
	private String saveBase = "/sdcard/header/";
	private File headerFile;

	private UserItem mUserItem;

	/** 设置提交我的信息接口 */
	private EditMyInfo mEditMyInfo = new EditMyInfo();
	/** 单独修改头像信息接口 */
	private EditHeadInfo mEditHeadInfo = new EditHeadInfo();

	//记录头像的uri
	private String headUri;
	
	private boolean isChangeImage = false;

	private PopupWindow popupWindow;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


	}

	@Override
	protected void onResume() {
		super.onResume();
		System.out.println("TakePhotos中：onResume方法-------------->");

		if (Util.hasSDCard()) {
			savePicPath = saveBase + Util.getFolderName("yyyyMMdd");
			File pathFile = new File(savePicPath);
			if (!pathFile.exists()) {
				pathFile.mkdirs();
			}
		}
	}

	@AfterViews
	protected void initVar() {
		QiNiuUtil.requestQiNiuToken(mContext);
		//设置光标，优化
		nickNameET.setSelection(nickNameET.getText().length());
//		nickNameET.setGravity(Gravity.RIGHT);

		initView();
		bindView();
	}
	
	private void initView() {
		initTitleBar(R.id.acpd_tb_title, "修改个人资料", null, "保存");
		mTitleBar.rightBN.setOnClickListener(this);
	}

	private void bindView() {
		birthDayTV.setOnClickListener(this);

		telephoneET.setEnabled(false);
		findViewById(R.id.acpd_rl_head).setOnClickListener(this);
		
		mUserItem = getMyApp().getUserItem();

		if (!TextUtils.isEmpty(mUserItem.getHead())){
			ImageLoaderUtil.displayImage(mUserItem.getHead(), headerIV, R.mipmap.default_image);
		}
		if (!TextUtils.isEmpty(mUserItem.getNick())){
			nickNameET.setText(mUserItem.getNick());
		}
		String userNameStr = mUserItem.getRealname();
		String telephoneStr = mUserItem.getMobile();


		realNameET.setText(userNameStr);
		identityCardET.setText(mUserItem.getCard_no());
		birthDayTV.setText(mUserItem.getBirthday());
		telephoneET.setText(telephoneStr);
		addressET.setText(mUserItem.getAddress());
	}
	
//	/**
//	 * 选择用户头像
//	 */
//	private void selectUserHead(){
//		Intent intent = new Intent(Intent.ACTION_PICK, null);
//		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
//		startActivityForResult(intent, RESULT_LOAD_IMAGE);
//	}
	

	/**
	 * 保存修改
	 */
	private void saveChange(){

		if (isChangePersonalDate()) {
			String realNameStr = realNameET.getText().toString();
			String nickNameStr = nickNameET.getText().toString();
			String identityCardStr = identityCardET.getText().toString();
			String birthDayStr = birthDayTV.getText().toString();
			String addressStr = addressET.getText().toString();
			if (TextUtils.isEmpty(realNameStr)) {
				CustomToast.showToast(mContext, "姓名为空");
				return;
			}
			if (TextUtils.isEmpty(nickNameStr)) {
				CustomToast.showToast(mContext, "昵称为空");
				return;
			}
			if (!Util.checkName(realNameStr)) {
				CustomToast.showToast(mContext, "请输入2-8个汉字！");
				return;
			}
			if (TextUtils.isEmpty(identityCardStr)) {
				CustomToast.showToast(mContext, "身份证为空");
				return;
			}
			if (TextUtils.isEmpty(birthDayStr)) {
				CustomToast.showToast(mContext, "请选择生日");
				return;
			}
			if (TextUtils.isEmpty(addressStr)) {
				CustomToast.showToast(mContext, "家庭住址为空");
				return;
			}


			mEditMyInfo.setNick(nickNameStr);
			mEditMyInfo.setRealname(realNameStr);
			mEditMyInfo.setCard_no(identityCardStr);
			mEditMyInfo.setBirthday(birthDayStr);
			mEditMyInfo.setAddress(addressStr);
			ApiManager apiManager = ApiManager.getInstance();
			apiManager.request(mEditMyInfo, new AbsOnRequestListener(mContext) {
				@Override
				public void onRequestSuccess(int result, JSONObject jsonObject) {
					super.onRequestSuccess(result, jsonObject);
					try {
						CustomToast.showToast(mContext, "修改成功");

						setResult(RESULT_OK);
						finish();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}

		if (!isChangeImage && !TextUtils.isEmpty(headUri)) {
			changeHead();
		}
	}

	/**
	 * 单独修改头像的方法
	 */
	private void changeHead(){
		mEditHeadInfo.setHead(headUri);
		ApiManager apiManager = ApiManager.getInstance();
		apiManager.request(mEditHeadInfo, new AbsOnRequestListener(mContext){
			@Override
			public void onRequestFail(int result, JSONObject jsonObject) {
				super.onRequestFail(result, jsonObject);
				try {
					CustomToast.showToast(mContext, "修改成功");
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		});
	}
	
	@Override  
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        super.onActivityResult(requestCode, resultCode, data);  
        //requestCode标示请求的标示   resultCode表示有数据  
		if(requestCode == Constant.StaticCode.REQUEST_CODE_CAPTURE_ALBUM){
			Intent intent = Util.startPhotoZoom(data.getData());
			startActivityForResult(intent, PhotoResult);
		}else if (requestCode == Constant.StaticCode.REQUEST_CODE_CAPTURE_CAMEIA ) {
			Intent intent = Util.startPhotoZoom(Uri.fromFile(tempFile));
			startActivityForResult(intent, PhotoResult);
		}else if (requestCode == PhotoResult) {
			Bundle extras = data.getExtras();
			if (extras != null) {
				// Parcel包裹，打包；
				Bitmap photo = extras.getParcelable("data");
				// 设置图片
				headerIV.setImageBitmap(photo);
				//完成后关闭悬浮框
				popupWindow.dismiss();
                isChangeImage = true;
				try {
					String picName = Util.getPicName();
					// 保存图片到本地
					//上传图片到七牛并且网络请求更换头像
					headerFile = Util.saveFile(photo, savePicPath, picName);

					String key = System.currentTimeMillis() + ".jpg";;
					headUri = key;
//					mEditMyInfo.setHead(key);
					QiNiuUtil.uploadNoConfig(savePicPath + "/" + picName, key, QiNiuUtil.qiNiuToken);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
    }  
	
	/**
	 * 是否修改了个人资料
	 */
	private boolean isChangePersonalDate(){
		String realNameStr = realNameET.getText().toString();
		String nickNameStr = nickNameET.getText().toString();
		String identityCardStr = identityCardET.getText().toString();
		String birthDayStr = birthDayTV.getText().toString();
		String addressStr = addressET.getText().toString();

		UserItem userItem = getMyApp().getUserItem();
		if(!TextUtils.isEmpty(realNameStr) && !realNameStr.equals(userItem.getRealname())){
			return true;
		}else if(!TextUtils.isEmpty(nickNameStr) && !nickNameStr.equals(userItem.getNick())){
			return true;
		}else if(!TextUtils.isEmpty(identityCardStr) && !identityCardStr.equals(userItem.getCard_no())){
			return true;
		}else if(!TextUtils.isEmpty(birthDayStr) && !birthDayStr.equals(userItem.getBirthday())){
			return true;
		}else if(!TextUtils.isEmpty(addressStr) && !addressStr.equals(userItem.getAddress())){
			return true;
		}else{
			return false;
		}
	}

	private File tempFile = new File(Environment.getExternalStorageDirectory(),  Util.getPicName());
	/**
	 * 弹出选择头像的悬浮框
	 */
	private void showPopWindown() {

		//设置popwindownAcitivity中的位置（底部居中）
		popupWindow = new ChoosePhotoPopwindow(mContext, new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				switch (v.getId()) {

					case R.id.btn_photograph_popwindow:
						String state = Environment.getExternalStorageState();
						if (state.equals(Environment.MEDIA_MOUNTED)) {

							Intent getImageByCamera = new Intent("android.media.action.IMAGE_CAPTURE");
							// 指定调用相机拍照后照片的储存路径
							getImageByCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
							startActivityForResult(getImageByCamera, Constant.StaticCode.REQUEST_CODE_CAPTURE_CAMEIA);
						}
						else {
							CustomToast.showToast(mContext, "请确认已经插入SD卡");
//                            Toast.makeText(getActivity(), "请确认已经插入SD卡", Toast.LENGTH_LONG).show();
						}
						break;

					case R.id.btn_album_popwindow:

						Intent intentAlbum = new Intent(Intent.ACTION_PICK, null);
						intentAlbum.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
						startActivityForResult(intentAlbum, Constant.StaticCode.REQUEST_CODE_CAPTURE_ALBUM);

						break;

					default:
						break;
				}

			}
		});
		popupWindow.showAtLocation(this.findViewById(R.id.acpd_ll_change_personal_data), Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
	}

	/**
	 * 选择生日时间
	 */
	public void onYearMonthDayPicker() {

		DatePicker picker = new DatePicker(this);
		picker.setRange(2000, 2016);
		picker.setSelectedItem(2015, 10, 10);
		picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
			@Override
			public void onDatePicked(String year, String month, String day) {

				birthDayTV.setText(year + "-" + month + "-" + day);

//                Date date = null;
//                try {
//                    date = new SimpleDateFormat("yyyy-MM-dd").parse(year + "-" + month + "-" + day);
//
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
			}
		});
		picker.show();
	}

	@Override
	public void onClick(View v){
		switch(v.getId()){
		case R.id.it_ibn_left://返回
			boolean is = isChangePersonalDate();
			if((is == true) || isChangeImage){
				final CustomDialog customDialog = Util.getDialog(mContext, null, "您修改了个人资料，确定保存吗？", "是", "否");
				customDialog.setOnClickListener(R.id.ok, new OnClickListener() {
					@Override
					public void onClick(View v) {
						customDialog.dismiss();
						saveChange();
					}
				});
				customDialog.setOnClickListener(R.id.cancel, new OnClickListener() {
					@Override
					public void onClick(View v) {
						customDialog.dismiss();
						ChangePersonalDataActivity.this.finish();
					}
				});
				customDialog.show();
			}else{
				ChangePersonalDataActivity.this.finish();
			}
			break;
		case R.id.it_btn_right:
			saveChange();
			break;
		case R.id.acpd_rl_head://选择头像
			showPopWindown();
			break;
		case R.id.acpd_tv_birthday_content://选择生日
			onYearMonthDayPicker();
			break;
		}
	}

}
