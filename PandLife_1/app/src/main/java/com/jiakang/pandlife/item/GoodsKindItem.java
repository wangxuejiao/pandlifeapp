package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/1/12.
 */
public class GoodsKindItem extends Item implements Serializable{

    /** 分类id	Int(10) */
    private int id;
    /** 分类上级id	Int(10) */
    private int pid;
    /** 商家id	Int(10) */
    private int sid;
    /** 分类排序编号	Int(10) */
    private int sort;
    /** 创建时间	Int(10) */
    private int createtime;
    /** 分类名称	Varchar(255) */
    private String cname;
    /** 记录是否被选中 */
    private boolean check;

    /**  */
    private String content;
    private int number;
    private float price;

    private int layout = R.layout.item_group_purchase_indent;
    @Override
    public int getItemLayoutId() {
        return layout;
    }

    public int getLayout() {
        return layout;
    }

    public void setLayout(int layout) {
        this.layout = layout;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getCreatetime() {
        return createtime;
    }

    public void setCreatetime(int createtime) {
        this.createtime = createtime;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
