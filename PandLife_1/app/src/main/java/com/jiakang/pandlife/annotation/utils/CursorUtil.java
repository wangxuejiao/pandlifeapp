package com.jiakang.pandlife.annotation.utils;

import android.content.ContentValues;
import android.database.Cursor;

import com.jiakang.pandlife.utils.JKLog;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * http://www.cnblogs.com/rollenholt/archive/2011/09/02/2163758.html
 * 
 */
public class CursorUtil {

	/**
	 * cursor生成对应的实体对象
	 * 
	 * @param cursor
	 * @param cls
	 * @return
	 */
	public static <T> T fromJson(Cursor cursor, Class<T> cls) {
		// Class<?> class1;
		try {
			// class1 = Class.forName(cls);
			T instance = cls.newInstance();
			// 取得本类的全部属性
			Field[] field = cls.getDeclaredFields();
			// 取得实现的接口或者父类的属性
			Field[] field1 = cls.getSuperclass().getDeclaredFields();
			// System.out.println("CursorUtil中：类型：" + cls + "子类属性数：" +
			// field.length + "---父类属性数：" + field1.length);
			setData(field, instance, cursor);
			setData(field1, instance, cursor);
			return instance;
		} catch (Exception e) {
			return null;
		}

	}

	private static void setData(Field[] field, Object instance, Cursor cursor) {
		for (int i = 0; i < field.length; i++) {
			field[i].setAccessible(true);
			try {
				// field[i].getName()
				int index = cursor.getColumnIndex(FieldUtils.getPropertyColumnByField(field[i]));
				Class<?> cls = field[i].getType();
				// if(cls == int.class || cls == integer.class){
				if (cls.equals(Integer.class) || cls.equals(int.class)) {
					if (index != -1)
						field[i].set(instance, cursor.getInt(index));
				} else if (cls.equals(Float.class) || cls.equals(float.class)) {
					if (index != -1) {
						field[i].set(instance, cursor.getFloat(index));
					}
				} else {
					if (index != -1)
						field[i].set(instance, cursor.getString(index));
				}
			} catch (Exception e) {
				JKLog.e("", "CursorUtil中：e为：" + e);
			}
		}
	}

	/**
	 * 根据实体对象，转成Map
	 * 
	 * 使用<T>来声明类型持有者名称，自定义泛型类时，类持有者名称可以使用T(Type)，
	 * 如果是容器的元素可以使用E(Element)，若键值匹配可以用K(Key)和V(Value)等，
	 * 若是<?>，则是默认是允许Object及其下的子类，也就是java的所有对象了。
	 * 
	 * @param instance
	 * @return
	 */
	public static <T> ContentValues getConValues(T instance) {
		Class<T> clss = (Class<T>) instance.getClass();
		return getConValues(instance, clss);
	}

	/**
	 * 根据实体对象，转成ContentValues
	 * 
	 * 使用<T>来声明类型持有者名称，自定义泛型类时，类持有者名称可以使用T(Type)，
	 * 如果是容器的元素可以使用E(Element)，若键值匹配可以用K(Key)和V(Value)等，
	 * 若是<?>，则是默认是允许Object及其下的子类，也就是java的所有对象了。
	 * 
	 * @param instance
	 * @param cls
	 * @return
	 */
	public static <T> ContentValues getConValues(T instance, Class<T> cls) {
		ContentValues contentValues = new ContentValues();
		// 取得本类的全部属性
		Field[] field = cls.getDeclaredFields();
		// 取得实现的接口或者父类的属性
		// Field[] field1 = cls.getSuperclass().getDeclaredFields();
		setvalues(field, instance, contentValues);
		// setvalues(field1, instance, contentValues);
		return contentValues;
	}

	private static void setvalues(Field[] fields, Object instance, ContentValues contentValues) {
		for (int i = 0; i < fields.length; i++) {
			fields[i].setAccessible(true);
			try {
				if (fields[i].get(instance) != null && !FieldUtils.isTransient(fields[i]))
					contentValues.put(FieldUtils.getPropertyColumnByField(fields[i]), (String) fields[i].get(instance));
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * 根据实体对象，转成Map
	 * 
	 * 使用<T>来声明类型持有者名称，自定义泛型类时，类持有者名称可以使用T(Type)，
	 * 如果是容器的元素可以使用E(Element)，若键值匹配可以用K(Key)和V(Value)等，
	 * 若是<?>，则是默认是允许Object及其下的子类，也就是java的所有对象了。
	 * 
	 * @param instance
	 * @return
	 */
	public static <T> Map<String, Object> getMaps(T instance) {
		Class<T> clss = (Class<T>) instance.getClass();
		return getMaps(instance, clss);
	}

	/**
	 * 根据实体对象，转成Map
	 * 
	 * 使用<T>来声明类型持有者名称，自定义泛型类时，类持有者名称可以使用T(Type)，
	 * 如果是容器的元素可以使用E(Element)，若键值匹配可以用K(Key)和V(Value)等，
	 * 若是<?>，则是默认是允许Object及其下的子类，也就是java的所有对象了。
	 * 
	 * @param instance
	 * @param cls
	 * @return
	 */
	public static <T> Map<String, Object> getMaps(T instance, Class<T> cls) {
		Map<String, Object> maps = new HashMap<String, Object>();
		// 取得本类的全部属性
		Field[] field = cls.getDeclaredFields();
		// 取得实现的接口或者父类的属性
		// Field[] field1 = cls.getSuperclass().getDeclaredFields();
		setvalues(field, instance, maps);
		// setvalues(field1, instance, contentValues);
		return maps;
	}

	private static void setvalues(Field[] fields, Object instance, Map<String, Object> maps) {
		for (int i = 0; i < fields.length; i++) {
			fields[i].setAccessible(true);
			try {

				if (fields[i].get(instance) != null && !FieldUtils.isTransient(fields[i]))
					maps.put(FieldUtils.getPropertyColumnByField(fields[i]), fields[i].get(instance));
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

}
