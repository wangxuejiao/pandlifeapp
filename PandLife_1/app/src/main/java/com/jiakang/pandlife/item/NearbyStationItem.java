package com.jiakang.pandlife.item;

import com.jiakang.pandlife.R;

import java.util.List;

/**
 * Created by play on 2016/1/6.
 */
public class NearbyStationItem{
    /**
     * data : [{"zone_id":"320115","address":"南京市江宁区U谷5#102室熊猫快收12530","mobiphone":"15156097071","phone":"15156097071","latitude":"31.868279","juli":"1","shopid":"12530","pic":"http://panda-life.cn/statics/images/4.jpg","nice_name":"熊猫快收12530U谷未来谷店铺","city_id":"3201","longitude":"118.829785"},{"zone_id":"320115","address":"南京市江宁区秣周路U谷3号楼5楼P532（熊猫快收11458）菜鸟驿站店","mobiphone":"18951825833","phone":"18951825833","latitude":"31.870682","juli":"2439","shopid":"11458","pic":"http://panda-life.cn/statics/images/4.jpg","nice_name":"熊猫快收11458悠谷店","city_id":"3201","longitude":"118.804144"}]
     * status : 1
     * info : 附近10个站点列表
     */
    private List<DataEntity> data;
    private int status;
    private String info;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public int getStatus() {
        return status;
    }

    public String getInfo() {
        return info;
    }


    public class DataEntity extends Item  {
        /**
         * zone_id : 320115
         * address : 南京市江宁区U谷5#102室熊猫快收12530
         * mobiphone : 15156097071
         * phone : 15156097071
         * latitude : 31.868279
         * juli : 1
         * shopid : 12530
         * pic : http://panda-life.cn/statics/images/4.jpg
         * nice_name : 熊猫快收12530U谷未来谷店铺
         * city_id : 3201
         * longitude : 118.829785
         */
        private String zone_id;
        private String address;
        private String mobiphone;
        private String phone;
        private String latitude;
        private String juli;
        private String shopid;
        private String pic;
        private String nice_name;
        private String city_id;
        private String longitude;
        private int layout = R.layout.item_nearby_station;

        public void setZone_id(String zone_id) {
            this.zone_id = zone_id;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public void setMobiphone(String mobiphone) {
            this.mobiphone = mobiphone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public void setJuli(String juli) {
            this.juli = juli;
        }

        public void setShopid(String shopid) {
            this.shopid = shopid;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public void setNice_name(String nice_name) {
            this.nice_name = nice_name;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getZone_id() {
            return zone_id;
        }

        public String getAddress() {
            return address;
        }

        public String getMobiphone() {
            return mobiphone;
        }

        public String getPhone() {
            return phone;
        }

        public String getLatitude() {
            return latitude;
        }

        public String getJuli() {
            return juli;
        }

        public String getShopid() {
            return shopid;
        }

        public String getPic() {
            return pic;
        }

        public String getNice_name() {
            return nice_name;
        }

        public String getCity_id() {
            return city_id;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLayout(int layout) {
            this.layout = layout;
        }

        @Override
        public int getItemLayoutId() {
            return layout;
        }
    }

}
