package com.example.hjk.amap_android_navi;

import android.content.Intent;
import android.os.Bundle;

import com.amap.api.navi.AMapNaviView;
import com.amap.api.navi.model.NaviLatLng;

/**
 * 创建时间：11/10/15 15:08
 * 项目名称：newNaviDemo
 *
 * @author lingxiang.wang
 * @email lingxiang.wang@alibaba-inc.com
 * 类说明：
 * <p/>
 * 最普通的导航页面，如果你想处理一些诸如菜单点击，停止导航按钮点击的事件处理
 * 请implement AMapNaviViewListener
 */

public class BasicNaviActivity extends BaseActivity {

    public static final String START_LONGITUDE = "startLongitude";
    public static final String START_LATITUDE = "startLatitude";
    public static final String END_LONGITUDE = "endLongitude";
    public static final String END_LATITUDE = "endLatitude";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_basic_navi);

        Intent intent = getIntent();
        double startLongitude = intent.getExtras().getDouble(START_LONGITUDE);
        double startLatitude = intent.getExtras().getDouble(START_LATITUDE);
        double endLongitude = intent.getExtras().getDouble(END_LONGITUDE);
        double endLatitude = intent.getExtras().getDouble(END_LATITUDE);

        mStartLatlng = new NaviLatLng(startLatitude, startLongitude);
        mEndLatlng = new  NaviLatLng(endLatitude, endLongitude);
        mAMapNaviView = (AMapNaviView) findViewById(R.id.navi_view);
        mAMapNaviView.onCreate(savedInstanceState);
        mAMapNaviView.setAMapNaviViewListener(this);
    }


}
